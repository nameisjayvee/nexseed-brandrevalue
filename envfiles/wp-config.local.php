<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpress' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          '2(o[5U!2EaU0q8u%GauS_wmVvE/=Z(Cz3C(8g|H*DF`[q[]L!`WKnQ%dyNbO;.;C' );
define( 'SECURE_AUTH_KEY',   '~]XjJ?*S|5fbr<bZQ7RHBqzn:Bih(V-S)~<J{d!-M}X06o^I?yhPJ^QjB=!gz~hT' );
define( 'LOGGED_IN_KEY',     '@xo^_3}jzfKlt^K;bc3MED|[.*3EqBCT4}/FfSSOUcTl|8>O1;<XX~a{L[ed3{iJ' );
define( 'NONCE_KEY',         'kRQa*ZS{)R$k `w+q=Zo|YZ@1UA&wvCwv0~~Dz;j.`47zNv4_r6k[*Pip7CNXwiu' );
define( 'AUTH_SALT',         'Z&j<}$`G0X:[qh>o*-K[fcOhaor6(&}q:tKlFOywtdGeI`5]7y/hj.2 ]eCD%jan' );
define( 'SECURE_AUTH_SALT',  '`}9`DM9smg1s}%G f|fL{k2Q/YEv1dV`x`_q/ZOYX%(el)Jc2[VvrbEe_u8eN&zN' );
define( 'LOGGED_IN_SALT',    '8$}~aa0Izsv1x@ti)_.Ka1#@MKB7at<qEb_I[|hh6.9FXtB9uk<3[5u.;o6SOrG-' );
define( 'NONCE_SALT',        'mc&$RyuXr/DjXKjn,,1#m=v||vB.muQUE,DijBVby;bWAC3u{5K:q()P{lvm[y4T' );
define( 'WP_CACHE_KEY_SALT', '![{Z[r!G>R!!iw2sP54^BMH#OZarVq!YkO#V0j[pr?]7kGc?`cP?S$?{X=Tna#h^' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


define( 'JETPACK_DEV_DEBUG', True );
define( 'WP_DEBUG', True );
define( 'FORCE_SSL_ADMIN', False );
define( 'SAVEQUERIES', False );

// Additional PHP code in the wp-config.php
// These lines are inserted by VCCW.
// You can place additional PHP code here!


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
