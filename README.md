##説明
VCCWを使用したローカル開発環境です

#####事前準備

Install Virtual Box
```
http://www.oracle.com/technetwork/server-storage/virtualbox/downloads/index.html?ssSourceSiteId=otnjp
```

Install Vagrant
```
https://www.vagrantup.com/downloads.html
```

install plugin

```
vagrant plugin install vagrant-hostsupdater
```



##開発環境構築手順
1,

```
ssh-add -K ~/.ssh/staygold-wp.pem
```

2, vagrantの起動 ※ルートディレクトリで実行してください
※vagrantの停止は vagrant halt
```
vagrant up
```
3,ssh vm
```
vagrant ssh
```
4,
```
cd /vagrant
```
5,テスト環境からファイル一式を持ってきます
```
wordmove pull -e staging --all
```


6,ローカル用の設定ファイルを作成します
/rootdir/envfilesにある.htaccess.localとwp-config.local.phpを 
.htaccessとwp-config.phpにrenameしてwordpressディレクトリに配置してください。

7,kaitorisatei.info.local.br/brandrevalue/　でブラウザからアクセスしてください

※ローカル環境では存在しないurlについては本番のurlを設定している場合もあります。画面での操作は本番に遷移していないことに気をつけて行ってください。

##word moveについて
rubyを使用したword press用デプロイツールです
https://github.com/welaika/wordmove

基本的な開発・デプロイの流れ

```
1,本番環境からthemes配下をpull ※事前にdry run(-s)で必ず確認してください
wordmove pull -e production -t
```
```
2,git(master)に修正が反映されていない場合はmasterに差分を取り込む。(修正の差分がわからなくなるので必ず実施)
```
```
3,ローカルでソースコードを更新
```
```
4,gitに反映
```

```
5,テスト環境にデプロイ ※事前にdry run(-s)で必ず確認してください
wordmove push -e staging -t
```

```
6,画面での動作の確認
```

```
7, 1,の後に本番が直接更新されているかを確認するためpull
wordmove pull -e production -s
```
```
8,本番から新たに持ってきたファイルがgitのソースと差分がないことを確認 (差分があった場合はgitに取り込む)
```
```
9,本番デプロイ ※事前にdry run(-s)で必ず確認してください
wordmove push -e production -t
```


###オプションについて
```
:wordpress | -w
Movefileのexclude部分で指定した以外のソースの同期をする。

:uploads | -u
uploads フォルダを同期する。

:theme | -t
wp-content/theme フォルダを同期する。

:plugins | -p
wp-content/plugins フォルダを同期する。
:language | -l
wp-content/languages の*.mo, *.po だけ同期する。

:db | -d
DBのデータを mysqlsump でダンプ -> mysqlコマンドでリストアする。

:verbose | -v
メッセージの詳細表示。

:simulate | -s
dry runモード 接続チェックなどに利用する。 コマンドを実行する前に必ず一度実施すること。

:environment | -e
環境を指定。テスト環境: staging 本番環境: production

:config | -c
wp-congig.php も同期させる。
```