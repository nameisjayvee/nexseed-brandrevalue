<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  '001.jpg::::ベルルッティ　アニバーサリー・カリグラフ・アレッサンドロ::142,000::136,000::150,000::14,000',
  '002.jpg::::シャネル　ココマーク　レザースニーカー::38,000::32,000::41,000::9,000',
  '003.jpg::::グッチ　モンクストラップレザーシューズ::8,500::8,000::10,000::2,000',
  '004.jpg::::グッチ　ホースビットローファー::13,500::12,800::14,000::1,200',
  '005.jpg::::サルバトーレフェラガモ　ビットローファー::17,200::16,500::18,000::1,500',
  '006.jpg::::クリスチャンルブタン　メッシュヒールパンプス::27,000::24,000::31,000::7,000',
  '007.jpg::::サルバトーレフェラガモ　ヴェラパンプス::11,500::10,000::13,000::3,000',
  '008.jpg::::シャネル　マトラッセヒールブーツ::46,000::41,000::50,000::9,000',
  '009.jpg::::ボッテガヴェネタ　イントレチャートフラットシューズ::18,000::15,000::21,000::6,000',
  '010.jpg::::ジミーチュウ　スタッズムートンブーツ::23,000::20,000::27,000::7,000',
  '011.jpg::::ミュウミュウ　ヒールパンプス::13,000::14,000::15,000::2,000',
  '012.jpg::::エルメス　レザースニーカー::35,000::38,000::40,000::5,000',
);


get_header(); ?>

    <div id="primary" class="cat-page content-area">
        <div class="mv_area ">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/lp_main/cat_shoes_main.jpg" alt="あなたの靴お売り下さい">
        </div>
    <p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
    <p class="main_bottom">ブランド靴”高価買取”宣言！満足価格で買取ります！</p>
    <div id="lp_head" class="shoes_ttl">
    <div>
    <p>銀座で最高水準の査定価格・サービス品質をご体験ください。</p>
    <h2>あなたの高級ブランド靴<br />どんな物でもお売り下さい！！</h2>
    </div>
    </div>

    <div class="lp_main">
            <section id="hikaku" class="watch_hikaku">
                <p class="hikaku_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/shoes_hikaku.png"></p>
            </section>

    <div style="margin-top:20px; margin-bottom: 20px;"><a href="https://kaitorisatei.info/brandrevalue/blog/doburock"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/12/caf61dc6779ec6137c0ab58dfe3a550d.jpg" alt="どぶろっく"></a></div>

            <section id="cat-point">
                <h3 class="obi_tl">高価買取のポイント</h3>
 <ul>
                    <li>
                    <p class="pt_bigtl">POINT1</p>
                        <div class="pt_wrap">
                        <p class="pt_tl">商品情報が明確だと査定がスムーズ</p>
                        <p class="pt_tx">ブランド名、モデル名が明確だと査定がスピーディに出来、買取価格にもプラスに働きます。また、新作や人気モデル、人気ブランドであれば買取価格がプラスになります。</p>
                        </div>
                    </li>
                    <li>
                    <p class="pt_bigtl">POINT2</p>
                        <div class="pt_wrap">
                        <p class="pt_tl">数点まとめての査定だと
                            <br>キャンペーンで高価買取が可能</p>
                        <p class="pt_tx">数点まとめての査定依頼ですと、買取価格をプラスさせていただきます。</p>
                        </div>
                    </li>
                    <li>
                    <p class="pt_bigtl">POINT3</p>
                        <div class="pt_wrap">
                        <p class="pt_tl">品物の状態がよいほど
                            <br>高価買取が可能</p>
                        <p class="pt_tx">お品物の状態が良ければ良いほど、買取価格もプラスになります。</p>
                        </div>
                    </li>
                </ul>
                <p>靴の査定額を高めるためには、日ごろのメンテナンスが重要となります。革靴の場合は、それぞれの素材や色に合わせた専用のクリーニング剤や、防水スプレー等を日常的に使用することで、靴に汚れが定着するのを防ぎ、素材の劣化を抑えることができます。布製の靴の場合は水やぬるま湯で洗い、汚れを落としましょう。汚れが目立つ靴は、査定前に一度クリーニングすることをお勧めします。<br><br>
また、ミュールや夏用のパンプスは、春夏にお売りいただくとシーズン品としてプラス査定することが可能ですので、ぜひシーズンに合わせてお持ちください。
                </p>
            </section>

            <section id="lp-cat-jisseki">
                <h3 class="obi_tl">買取実績</h3>

                <ul id="box-jisseki" class="list-unstyled clearfix">
                    <?php
            foreach($resultLists as $list):
            // :: で分割
            $listItem = explode('::', $list);

          ?>
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/shoes/lp/<?php echo $listItem[0]; ?>" alt=""></p>
                                <p class="itemName">
                                    <?php echo $listItem[1]; ?>
                                        <br>
                                        <?php echo $listItem[2]; ?>
                                </p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：
                                    <?php echo $listItem[3]; ?>円
                                        <br>
                                        <span class="blue">B社</span>：
                                        <?php echo $listItem[4]; ?>円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">
                                    <?php echo $listItem[5]; ?><span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>
                                    <?php echo $listItem[6]; ?>円</p>
                            </div>
                        </li>
                        <?php endforeach; ?>
                </ul>
                                <h3 class="new_list_ttl">ブランドリスト</h3>
                <ul class="list-unstyled new_list">
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/hermes'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch33.jpg" alt="エルメス"></dd>
                                <dt><span>Hermes</span>エルメス</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/celine'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch29.jpg" alt="セリーヌ"></dd>
                                <dt><span>CELINE</span>セリーヌ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/wallet/louisvuitton'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch23.jpg" alt="ルイヴィトン"></dd>
                                <dt><span>LOUIS VUITTON</span>ルイヴィトン</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/chanel'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag01.jpg" alt="シャネル"></dd>
                                <dt><span>CHANEL</span>シャネル</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/gucci'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch25.jpg" alt="グッチ"></dd>
                                <dt><span>GUCCI</span>グッチ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/prada'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag02.jpg" alt="プラダ"></dd>
                                <dt><span>PRADA</span>プラダ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/fendi'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag03.jpg" alt="プラダ"></dd>
                                <dt><span>FENDI</span>フェンディ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/dior'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag04.jpg" alt="ディオール"></dd>
                                <dt><span>Dior</span>ディオール</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                        <a href="<?php echo home_url('/cat/bag/saint_laurent'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag05.jpg" alt="サンローラン"></dd>
                                <dt><span>Saint Laurent</span>サンローラン</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/bottegaveneta'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch32.jpg" alt="ボッテガヴェネタ"></dd>
                                <dt><span>Bottegaveneta</span>ボッテガヴェネタ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/ferragamo'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch31.jpg" alt="フェラガモ"></dd>
                                <dt><span>Ferragamo</span>フェラガモ</dt>
                            </a>
                        </dl>
                    </li>
            </ul>

                <p id="catchcopy">多くのブランド買取ショップでは貴金属やジュエリーには力を入れている一方、洋服や靴といったアパレル品の査定のノウハウが乏しいケースも多いと言われています。こうした場合、せっかくのお値打ち品もびっくりするような安い買取額になってしまう可能性も。その点BRAND REVALUEは新しい店舗ではありますが、系列店で長年アパレル買取の実績を積み重ねています。<br>
ブランド靴の買取に精通したプロの鑑定士が古くなった靴でも適正に価値を査定いたしますので、他店に負けない高額査定が可能となっております。
            </p>

                <p class="jyoutai_tl">こんな状態でも買取致します!</p>
                <div id="konna">

                    <p class="example1">■ヒール釘が出ている</p>
                    <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/shoes_jyoutai01.jpg"></p>
                    <p class="example2">■傷がついている</p>
                    <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/shoes_jyoutai02.jpg"></p>
                    <p class="example3">■中・外の汚れ</p>
                    <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/shoes_jyoutai03.jpg"></p>
                    <p class="text">その他：裏の汚れ等がある状態でもお買取りいたします。</p>
                </div>
            </section>

            <section id="about_kaitori" class="clearfix">
                <?php
        // 買取について
        get_template_part('_widerange');
                get_template_part('_widerange2');
      ?>
            </section>

            <h3 class="obi_tl">ブランドリスト</h3>
            <ul class="cat_list">
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/shoes_1.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/shoes_2.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/shoes_3.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/shoes_4.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/shoes_5.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/shoes_6.jpg" alt=""></li>
        <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/shoes_7.jpg" alt=""></li>
        <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/shoes_8.jpg" alt=""></li>
        <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/shoes_9.jpg" alt=""></li>
        <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/shoes_10.jpg" alt=""></li>
            </ul>

            <section>
                <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/bn_matomegai.png">
            </section>

            <?php
        // 買取基準
        get_template_part('_criterion');

        // NGアイテム
        get_template_part('_no_criterion');

                // カテゴリーリンク
        get_template_part('_category');
            ?>
                <section id="kaitori_genre">
                    <h3 class="obi_tl">その他買取可能なジャンル</h3>
                    <p>【買取ジャンル】バッグ/ウエストポーチ/セカンドバック/トートバッグ/ビジネスバッグ/ボストンバッグ/クラッチバッグ/トランクケース/ショルダーバッグ/ポーチ/財布/カードケース/パスケース/キーケース/手帳/腕時計/ミュール/サンダル/ビジネスシューズ/パンプス/ブーツ/ペアリング/リング/ネックレス/ペンダント/ピアス/イアリング/ブローチ/ブレスレット/ライター/手袋/傘/ベルト/ペン/リストバンド/アンクレット/アクセサリー/サングラス/帽子/マフラー/ハンカチ/ネクタイ/ストール/スカーフ/バングル/カットソー/アンサンブル/ジャケット/コート/ブルゾン/ワンピース/ニット/シャツメンズ/毛皮/Tシャツ/キャミソール/タンクトップ/パーカー/ベスト/ポロシャツ/ジーンズ/スカート/スーツなど</p>
                </section>

                <?php
        // 買取方法
        get_template_part('_purchase');
      ?>

                    <section id="user_voice">
                        <h3>ご利用いただいたお客様の声</h3>

                        <p class="user_voice_text1">ちょうど家の整理をしていたところ、家のポストにチラシが入っていたので、ブランドリバリューに電話してみました。今まで買取店を利用したことがなく、不安でしたがとても丁寧な電話の対応とブランドリバリューの豊富な取り扱い品目を魅力に感じ、出張買取を依頼することにしました。 絶対に売れないだろうと思った、動かない時計や古くて痛んだバッグ、壊れてしまった貴金属のアクセサリーなども高額で買い取っていただいて、とても満足しています。古紙幣や絵画、食器なども買い取っていただけるとのことでしたので、また家を整理するときにまとめて見てもらおうと思います。
                        </p>

                        <h3>鑑定士からのコメント</h3>

                        <p class="user_voice_text2">家の整理をしているが、何を買い取ってもらえるか分からないから一回見に来て欲しいとのことでご連絡いただきました。 買取店が初めてで不安だったが、丁寧な対応に非常に安心しましたと笑顔でおっしゃって頂いたことを嬉しく思います。 物置にずっとしまっていた時計や、古いカバン、壊れてしまったアクセサリーなどもしっかりと価値を見極めて高額で提示させて頂きましたところ、お客様もこんなに高く買取してもらえるのかと驚いておりました。 これからも家の不用品を整理するときや物の価値判断ができないときは、すぐにお電話しますとおっしゃって頂きました。
                        </p>

                    </section>

        </div>
        <!-- lp_main -->
    </div>
    <!-- #primary -->

    <?php

get_footer();
