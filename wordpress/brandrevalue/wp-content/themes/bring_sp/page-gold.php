<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


get_header(); ?>
<?php

	/************/
	/* 定数定義 */
	/************/
	const DAY_BORDER_HOUR   = 10;                   // システム内の日付変更時間
	const JSON_GET_URL      = 'https://www.net-japan.co.jp/system/upload/netjapan/export/'; // JSON取得先
	const JSON_GET_PREFIX   = 'price_';             // JSON取得先ファイルの接頭語
	const JSON_GET_SAFFIX   = '_0930.json';         // JSON取得先ファイルの接尾語
	const PRICES_DIR        = '/prices/';           // サーバー内の金額ファイルの設置ディレクトリ
	const JSON_LOCAL_SAFFIX = '.json';              // サーバー内の金額ファイルの接尾語
	const MAX_DATE_GOBACK   = 14;                   // 遡る最大日数

	/************/
	/* 関数定義 */
	/************/
	// URLで指定したページ情報取得
	function getApiDataCurl($url)
	{
		$option = array(
			CURLOPT_RETURNTRANSFER => true, //文字列として返す
			CURLOPT_TIMEOUT		=> 3, // タイムアウト時間
		);

		$ch = curl_init($url);
		curl_setopt_array($ch, $option);

		$json	= curl_exec($ch);
		$info	= curl_getinfo($ch);
		$errorNo = curl_errno($ch);

		// OK以外はエラーなので空白を返す
		if ($errorNo !== CURLE_OK) {
			// 詳しくエラーハンドリングしたい場合はerrorNoで確認
			// タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
			return '';
		}

		// 200以外のステータスコードは失敗とみなし空白を返す
		if ($info['http_code'] !== 200) {
			return '';
		}

		// 文字列から変換
		//$jsonArray = json_decode($json, true);

		return $json;
	}

	// 金額取得関数
	function getPrices($fileNameBase) {

		// サーバー内の金額ファイル名生成
		$localFileName = __DIR__ . PRICES_DIR . $fileNameBase . JSON_LOCAL_SAFFIX;

		$json = '';

		if (!file_exists($localFileName)) {
		// サーバー内に金額ファイルがない場合、JSON取得して作成

			// JSON取得先のURL生成
			$apiFileName = JSON_GET_URL . JSON_GET_PREFIX . $fileNameBase . JSON_GET_SAFFIX;

			// JSONを取得できた場合、サーバー内に金額ファイル保存
			if ($json = getApiDataCurl($apiFileName)) {
				// ファイル保存（失敗時はfalseを返却)
				if (!file_put_contents($localFileName, $json)) {
					return FALSE;
				}
			}
		} else {
		// サーバー内に金額ファイルがある場合、読み込む
			if (is_readable($localFileName)) $json = file_get_contents($localFileName);
		}

		return $json;
	}

	$time = new DateTime();
	$time->setTimestamp(time())->setTimezone(new DateTimeZone('Asia/Tokyo'));
	$hour = $time->format('G'); // 現在時

	$data = array();   // データ

	for ($dateCnt = 0; $dateCnt < MAX_DATE_GOBACK; $dateCnt++) {

		if ($hour < DAY_BORDER_HOUR) {
		// 日付変更時より前なら一日前分
			$fileNameBase = date('Ymd', strtotime('-'.($dateCnt + 1).' day'));
		} else {
		// 日付変更時以降なら当日分
			$fileNameBase = date('Ymd', strtotime('-'.$dateCnt.' day'));
		}

		// jsonを取得できたらデコードして繰り返し終了
		if ($json = getPrices($fileNameBase)) {
			$data = json_decode($json, true);
			break;
		}
	}

	$market = array();
	$price  = array();

	if (!empty($data['data']['market'])) $market = $data['data']['market'];
	if (!empty($data['data']['nj_buy'])) $price  = $data['data']['nj_buy'];
?>
<script>
	// 整数判定関数
	function isInteger(x) {
		x = parseFloat(x);
		return Math.round(x) === x;
	}

	// 計算結果表示
	function autoCalc() {

		if ($('#unit_price').text() !== '-' && $('#copy_metal_weight').text() !== '-') {

			var $result = parseFloat($('#unit_price').text().replace(/,/g, '')) * parseFloat($('#copy_metal_weight').text());

			$('#calculation_result').text($result.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
		}
	}

	var $prices = new Object();
	$prices['au'] = <?= json_encode($price['au_scrap'], JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT); ?>;
	$prices['pt'] = <?= json_encode($price['pt_scrap'], JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT); ?>;
	$prices['ag'] = <?= json_encode($price['ag_scrap'], JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT); ?>;

	$(function(){

		// 品種選択時、品位のプルダウン作成
		$("input[name='radio_kind']").change(function() {

			$('#dignity option').remove();

			$kind = $(this).data('kind');

			$options = $.map($prices[$kind], function ($arr) {
				$option = $('<option>', {value: $arr['price'].replace(/,/g, ''), text: $arr['name']});
				return $option;
			});

			$options.unshift($('<option>', {value: '-', text: '選択してください'}));

			$('#dignity').append($options);

			$('#unit_price').text('-');
			$('#calculation_result').text('-');
		});

		// 品位選択時、単価と自動計算
		$("#dignity").change(function() {

			if ($(this).val() === '-') {
				$('#unit_price').text('-');
				$('#calculation_result').text('-');
			} else {
				$('#unit_price').text($(this).val().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
				autoCalc();
			}
		});

		// 重量入力時、重量と自動計算
		$("#input_metal_weight").keyup(function() {

			var $input_value = $(this).val().replace(/[０-９]/g, function(s) {
				return String.fromCharCode(s.charCodeAt(0) - 65248);
			});

			if ($input_value == '') {
			// 入力が空の場合
				$('#copy_metal_weight').text('-');
				$('#calculation_result').text('-');
				$('#input_metal_weight_err').hide();

			} else if (isInteger($input_value)) {
			// 入力が整数の場合
				$('#copy_metal_weight').text($input_value);
				autoCalc();
				$('#input_metal_weight_err').hide();
			} else {
			// 上記以外の場合
				$('#copy_metal_weight').text('-');
				$('#calculation_result').text('-');
				$('#input_metal_weight_err').show();
			}
		});
	});
</script>

<div id="primary" class="cat-page content-area">
	<div class="mv_area "> <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/lp_main/cat_gold_main.jpg" alt="時計の取扱いについて"> </div>
	<p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
	<p class="main_bottom">満足価格で買取！金・プラチナ買取ならブランドリバリュー</p>
	<div id="lp_head" class="gold_ttl">
		<div>
			<p>銀座で最高水準の査定価格・サービス品質をご体験ください。</p>
			<h2>あなたの金・プラチナ<br />
				どんな物でもお売り下さい！！</h2>
		</div>
	</div>
	<div class="lp_main">
		<p id="catchcopy">BRAND REVALUEにはブランド品・貴金属買取に精通したスタッフがそろっており、金（ゴールド）の再販ルートを確立しております。 壊れたアクセサリーに含まれた金などを含め、多少状態に難があっても素材の価値を高く査定し、 お客様にご納得いただける査定額をご提示するのがBRANDREVALUE(ブランドリバリュー)の方針です。 <br>
			さらに、BRANDREVALUE(ブランドリバリュー)はまだ新しい買取店ですので、利益以上にお客様に認知され、愛される店づくりを目指しています。 そのため、「赤字覚悟の高額査定」を実現することができているのです。 </p>
								<!--<div class="gold_bnr"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/gold/gold_bnr_02.png" alt="イエローゴールド" ></div>-->



		<div class="gold_bnr"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/gold/gold_bnr.jpg" alt="イエローゴールド" ></div>
		<section id="hikaku" class="watch_hikaku">
			<p class="hikaku_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/gold_hikaku.png"></p>
		</section>

    <div style="margin-top:20px; margin-bottom: 20px;"><a href="https://kaitorisatei.info/brandrevalue/blog/doburock"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/12/caf61dc6779ec6137c0ab58dfe3a550d.jpg" alt="どぶろっく"></a></div>

		<section class="gold_new_point">
			<h2>高価買取が出来る3つの理由</h2>
			<div class="gold_in">
				<h3><span>理由01</span>豊富な知識量で高価買取が可能</h3>
				<p class="gold_tx">BRAND REVALUEでは、刻印なしの貴金属から、小さい宝石、質が悪く他社様では重さから引かれてしまうような貴金属に宝飾されている宝石も高価買取！<br>
同時に鑑別書などのお品物を断定するような証明書をお持ち込み頂けると査定額UPにつながります。</p>
				<div class="coin_point_box01">
					<p> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/gold_img01.png" alt="金貨"></p>
					<table>
						<tr>
							<th>K18/Pt900</th>
							<td>¥370,000</td>
						</tr>
						<tr>
							<th>ダイヤモンド</th>
							<td>¥250,000</td>
						</tr>
						<tr>
							<th>カラーダイヤモンド</th>
							<td>¥300,000</td>
						</tr>
						<tr>
							<th>色石</th>
							<td>¥100,000</td>
						</tr>
						<tr>
							<th class="ds_price">デザイン料</th>
							<td class="ds_price">¥50,000</td>
						</tr>
						<tr>
							<th class="up_price">プラス加算</th>
							<td class="up_price">¥10,000</td>
						</tr>
						<tr>
							<th class="last_price">合計</th>
							<td class="last_price">¥1,080,000</td>
						</tr>
					</table>
				</div>
				<div class="coin_point_box02">
					<p class="point_ttl">鑑定ポイント</p>
					<div class="in_box">
						<p> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/gold_img02.png" alt="金貨"></p>
						<p>金とプラチナをベースにクラスの良いダイヤや希少なカラーダイヤ、
							五大宝石のエメラルド、サファイア、ルビー等が全体的に装飾された
							お品物は希少価値、デザインの価値が付くため査定額にプラスする<br />
							ことができます。</p>
					</div>
				</div>
				<h3><span>理由02</span>デザインも考慮した高価買取が可能</h3>
				<p class="gold_tx">金の仏像や宝飾品などには、貴金属価値以外の製品価値、デザイン代も上乗せし高価買取可能です。金を使用した製品は、大変希少で非常に人気がございます。<br>そんな希少なお品物をBRAND REVALUEでは、一流のバイヤーだからこそできる査定額をご提示させていただきます。</p>
				<h3><span>理由03</span>相場に関係なく高価買取が可能</h3>
				<p class="gold_tx">金＝重量×相場という固定概念を捨て、新たな価値、取引先を常に日々開拓することにより他社様には負けない高価買取が可能です。<br>金の買取ならどこよりも高いとご好評をいただいている「BRANDREVALUE」では、インゴットからアクセサリー、金歯や食器などといった幅広い貴金属製品を高価買取しております。希少な金製品なら金の重量や純度のみならず、デザインや一緒に宝飾された宝石の価値まで含めた最高額を常にご提示させていただきます。金の買取に自信がある「BRANDREVALUE」を是非ご利用下さい！ </p>
			</div>
		</section>
		<h2 class="obi_tl_gold">本日の価格情報 <span class="gold_date">
			<?= $market['price_date'] ?>
			（10:00更新）現在</span></h2>
		<ul class="gold_box_top">
			<li>
				<h4>金<span>-GOLD-</span></h4>
				<p class="gold_mp">
					<?= $price['ingod']['au']['price'] ?>
					円<span>(前日比 <span class="f_red">
					<?= $price['ingod']['au']['diff'] ?>
					</span>円)</span></p>
			</li>
			<li>
				<h4>銀<span>-SILVER-</span></h4>
				<p class="gold_mp">
					<?= $price['ingod']['ag']['price'] ?>
					円<span>(前日比 <span class="f_red">
					<?= $price['ingod']['ag']['diff'] ?>
					</span>円)</span></p>
			</li>
			<li>
				<h4>Pt<span>-PLATINUM-</span></h4>
				<p class="gold_mp">
					<?= $price['ingod']['pt']['price'] ?>
					円<span>(前日比 <span class="f_red">
					<?= $price['ingod']['pt']['diff'] ?>
					</span>円)</span></p>
			</li>
			<li>
				<h4>Pd<span>-PALLADIUM-</span></h4>
				<p class="gold_mp">
					<?= $price['ingod']['pd']['price'] ?>
					円<span>(前日比 <span class="f_red">
					<?= $price['ingod']['pd']['diff'] ?>
					</span>円)</span></p>
			</li>
		</ul>
		<div class="gold_container">
			<div class="gold_box_wrap01 gft_l">
				<h4>金<span>-GOLD-</span></h4>
				<ul class="gold_inner_list">
					<?php foreach ($price['au_scrap'] as $p): ?>
					<li<?php if($p['bold']): ?> class="pick_list"<?php endif; ?>>
						<?= str_replace(' ', '<br>', $p['name']) ?>
						<span>
						<?= $p['price'] ?>
						円/g</span></li>
					<?php endforeach; ?>
				</ul>
			</div>
			<div class="gold_box_wrap02 gft_l">
				<h4>金<span>-GOLD-</span>Pt<span>-PLATINUM-</span></h4>
				<ul class="gold_inner_list ">
					<?php foreach ($price['pk_combi'] as $p): ?>
					<li<?php if($p['bold']): ?> class="pick_list"<?php endif; ?>>
						<?= str_replace(' ', '<br>', $p['name']) ?>
						<span>
						<?= $p['price'] ?>
						円/g</span></li>
					<?php endforeach; ?>
				</ul>
			</div>
			<div class="gold_box_wrap03 gft_l">
				<h4>Pt<span>-PLATINUM-</span></h4>
				<ul class="gold_inner_list">
					<?php foreach ($price['pt_scrap'] as $p): ?>
					<li<?php if($p['bold']): ?> class="pick_list"<?php endif; ?>>
						<?= str_replace(' ', '<br>', $p['name']) ?>
						<span>
						<?= $p['price'] ?>
						円/g</span></li>
					<?php endforeach; ?>
				</ul>
				<h4 class="gl_sliv">銀<span>-PLATINUM-</span></h4>
				<ul class="gold_inner_list gl_sliv">
					<?php foreach ($price['ag_scrap'] as $p): ?>
					<li<?php if($p['bold']): ?> class="pick_list"<?php endif; ?>>
						<?= str_replace(' ', '<br>', $p['name']) ?>
						<span>
						<?= $p['price'] ?>
						円/g</span></li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
		<div class="gold_bnr"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/gold/gold_bnr.jpg" alt="イエローゴールド" ></div>

		<!--買取評価チェックコンテンツ-->

		<div class="gold_check">
			<h3>貴金属買取価格シミュレーションツール</h3>
			<form action="" name="sel_form" id="sel_form" class="mark_wrap">
				<ul class="gold_mark">
					<li>
						<p class="gold_input_ttl"><span>●</span>貴金属製品の品種をお選びください。</p>
						<label>
							<input type="radio" name="radio_kind" class="radio01-input" data-kind="au">
							<span class="radio01-parts">金</span> </label>
						<label>
							<input type="radio" name="radio_kind" class="radio01-input" data-kind="pt">
							<span class="radio01-parts">プラチナ</span> </label>
						<label>
							<input type="radio" name="radio_kind" class="radio01-input" data-kind="ag">
							<span class="radio01-parts">銀</span> </label>
					</li>
					<li>
						<p class="gold_input_ttl"><span>●</span>貴金属製品の部位をお選びください。</p>
						<select id="dignity" name="color_id">
							<option value="">選択して下さい</option>
						</select>
					</li>
					<li>
						<p class="gold_input_ttl"><span>●</span>貴金属製品の重量を入力してください。</p>
						<input type="text" id="input_metal_weight" />
						g
						<p id="input_metal_weight_err" style="color:red; display:none;">整数で入力してください。</p>
					</li>
				</ul>
				<div class="mark_result">
					<p>本日の相場×重量を自動計算</p>
					<table>
						<tr>
							<th width="30%"><span class="ico_fix01">¥</span><span id="unit_price">-</span></th>
							<td>×</td>
							<th width="30%"><span id="copy_metal_weight">-</span>&ensp;<span class="ico_fix02">g</span></th>
							<td>=</td>
							<th width="30%"><span class="ico_fix01">¥</span><span id="calculation_result">-</span></th>
						</tr>
					</table>
				</div>
			</form>
		</div>
		<h3 class="mid">金・プラチナ 一覧</h3>
		<ul class="mid_link">
			<li><a href="<?php echo home_url('/cat/gold/gold-coin'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/01.jpg" alt="金貨">
				<p class="mid_ttl">金貨</p>
				</a> </li>
			<li><a href="<?php echo home_url('/cat/gold/14k'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/02.jpg" alt="K14">
				<p class="mid_ttl">K14</p>
				</a> </li>
			<li><a href="<?php echo home_url('/cat/gold/18k'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/03.jpg" alt="K18">
				<p class="mid_ttl">K18</p>
				</a> </li>
			<li><a href="<?php echo home_url('/cat/gold/22k'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/04.jpg" alt="K22">
				<p class="mid_ttl">K22</p>
				</a> </li>
			<li><a href="<?php echo home_url('/cat/gold/24k'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/05.jpg" alt="K24">
				<p class="mid_ttl">K24</p>
				</a> </li>
			<li><a href="<?php echo home_url('/cat/gold/ingot'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/06.jpg" alt="インゴット">
				<p class="mid_ttl">インゴット</p>
				</a> </li>
			<li><a href="<?php echo home_url('/cat/gold/white-gold'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/07.jpg" alt="ホワイトゴールド">
				<p class="mid_ttl">ホワイトゴールド</p>
				</a> </li>
			<li><a href="<?php echo home_url('/cat/gold/pink-gold'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/08.jpg" alt="ピンクゴールド">
				<p class="mid_ttl">ピンクゴールド</p>
				</a> </li>
			<li><a href="<?php echo home_url('/cat/gold/yellow-gold'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/09.jpg" alt="イエローゴールド">
				<p class="mid_ttl">イエローゴールド</p>
				</a> </li>
			<li class="pdl16"><a href="<?php echo home_url('/cat/gold/platinum'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/10.jpg" alt="プラチナ">
				<p class="mid_ttl">プラチナ</p>
				</a> </li>
			<li class="pdr16"><a href="<?php echo home_url('/cat/gold/silver'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/11.jpg" alt="シルバー">
				<p class="mid_ttl">シルバー</p>
				</a> </li>
		</ul>
		<section id="lp-cat-jisseki">
			<h3 class="obi_tl">買取実績</h3>
			<ul id="box-jisseki" class="list-unstyled clearfix">
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/gold01.jpg" alt=""> </p>
						<p class="itemName">金急須</p>
						<hr>
						<p> <span class="red">A社</span>：1,261,000円 <br>
							<span class="blue">B社</span>：1,235,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">1,300,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>65,000円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/gold02.jpg" alt=""> </p>
						<p class="itemName">金プラチナコンビネックレスチェーン</p>
						<hr>
						<p> <span class="red">A社</span>：87,300円 <br>
							<span class="blue">B社</span>：85,500円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">90,000円</span> </p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>4,500円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/gold03.jpg" alt=""> </p>
						<p class="itemName">金　置物</p>
						<hr>
						<p> <span class="red">A社</span>：717,800円 <br>
							<span class="blue">B社</span>：703,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">740,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>37,000円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/gold04.jpg" alt=""> </p>
						<p class="itemName">破損・ノーブランドの貴金属アクセサリー　金　プラチナ製</p>
						<hr>
						<p> <span class="red">A社</span>：1,406,500円 <br>
							<span class="blue">B社</span>：1,377,500円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">1,450,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>72,500円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/gold05.jpg" alt=""> </p>
						<p class="itemName">プラチナ　プラチナメイプルリーフコイン</p>
						<hr>
						<p> <span class="red">A社</span>：109,610円 <br>
							<span class="blue">B社</span>：107,350円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">113,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>5,650円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/gold06.jpg" alt=""> </p>
						<p class="itemName">ウィーン金貨ハーモニー</p>
						<hr>
						<p> <span class="red">A社</span>：150,447円 <br>
							<span class="blue">B社</span>：147,345円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">155,100<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>4,653円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/gold07.jpg" alt=""> </p>
						<p class="itemName">金　ネックレス</p>
						<hr>
						<p> <span class="red">A社</span>：504,400円 <br>
							<span class="blue">B社</span>：494,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">520,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>26,000円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/gold08.jpg" alt=""> </p>
						<p class="itemName">金　リング</p>
						<hr>
						<p> <span class="red">A社</span>：87,300円 <br>
							<span class="blue">B社</span>：85,500円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">90,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>4,500円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/gold09.jpg" alt=""> </p>
						<p class="itemName">ツバルホース金貨1/2oz</p>
						<hr>
						<p> <span class="red">A社</span>：71,334円 <br>
							<span class="blue">B社</span>：69,863円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">73,540<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>3,677円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/gold10.jpg" alt=""> </p>
						<p class="itemName">アメリカンイーグル金貨1oz</p>
						<hr>
						<p> <span class="red">A社</span>：136,770円 <br>
							<span class="blue">B社</span>：133,950円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">141,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>7,050円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/gold12.jpg" alt=""> </p>
						<p class="itemName">足金リング</p>
						<hr>
						<p> <span class="red">A社</span>：50,449円 <br>
							<span class="blue">B社</span>：49,409円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">52,010<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>2,601円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/gold11.jpg" alt=""> </p>
						<p class="itemName">マン島キャット金貨 1/5oz　枠K18</p>
						<hr>
						<p> <span class="red">A社</span>：38,000円 <br>
							<span class="blue">B社</span>：37,400円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">41,100<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>3,700円</p>
					</div>
				</li>
			</ul>
			<!--    <h3 class="new_list_ttl">ブランドリスト</h3>
                <ul class="list-unstyled new_list">
          <li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/ingot'); ?>">
              <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold05.jpg" alt="インゴット"></dd>
              <dt>インゴット</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/gold-coin'); ?>">
              <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold11.jpg" alt="金貨"></dd>
              <dt>金貨</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/14k'); ?>">
              <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold01.jpg" alt="14金"></dd>
              <dt>14金</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/18k'); ?>">
              <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold02.jpg" alt="18金"></dd>
              <dt>18金</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/22k'); ?>">
              <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold03.jpg" alt="22金"></dd>
              <dt>22金</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/24k'); ?>">
              <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold04.jpg" alt="24金"></dd>
              <dt>24金</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/white-gold'); ?>">
              <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold06.jpg" alt="ホワイトゴールド"></dd>
              <dt>ホワイトゴールド</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/pink-gold'); ?>">
              <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold07.jpg" alt="ピンクゴールド"></dd>
              <dt>ピンクゴールド</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/yellow-gold'); ?>">
              <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold08.jpg" alt="イエローゴールド"></dd>
              <dt>イエローゴールド</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/platinum'); ?>">
              <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold09.jpg" alt="プラチナ"></dd>
              <dt>プラチナ</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/silver'); ?>">
              <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold10.jpg" alt="シルバー"></dd>
              <dt>シルバー</dt>
              </a>
            </dl>
          </li>




        </ul>-->

			<p id="catchcopy">BRAND REVALUEがモットーとしているのは「お客様に利益を還元する店舗づくり」。路面店ではなくあえて家賃の安い空中階に店舗を構え、人件費・広告費を抑えているのも、削減した経費の分査定額を高めるためです。ダイヤモンドの買取においても、「鑑定書がないダイヤモンド」、「ノンブランドのダイヤモンド」、「極小粒のダイヤモンド（メレ）」などどんなものでもご満足いただける価格で買い取らせていただきます。新しい店舗ではありますが、経験豊かな鑑定士が適切に査定を行いますので、どうかお気軽にご相談ください。 </p>
			<p class="jyoutai_tl">こんな状態でも買取致します!</p>
			<div id="konna">
				<p class="example1">■海外物、刻印無し</p>
				<p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/gold_jyoutai01.jpg"></p>
				<p class="example2">■金　コイン</p>
				<p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/gold_jyoutai02.jpg"></p>
				<p class="example3">■刻印無しインゴット</p>
				<p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/gold_jyoutai03.jpg"></p>
				<p class="text">その他：金具破損、眼鏡、台だけの指輪等でもお買取りいたします。</p>
			</div>
		</section>
		<section id="about_kaitori" class="clearfix">
			<?php
        // 買取について
        get_template_part('_widerange');
				get_template_part('_widerange2');
      ?>
		</section>
		<section> <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/bn_matomegai.png"> </section>
		<?php
        // 買取基準
        get_template_part('_criterion');

        // NGアイテム
        get_template_part('_no_criterion');

				// カテゴリーリンク
        get_template_part('_category');
			?>
		<section id="kaitori_genre">
			<h3 class="obi_tl">その他買取可能なジャンル</h3>
			<p>【買取ジャンル】バッグ/ウエストポーチ/セカンドバック/トートバッグ/ビジネスバッグ/ボストンバッグ/クラッチバッグ/トランクケース/ショルダーバッグ/ポーチ/財布/カードケース/パスケース/キーケース/手帳/腕時計/ミュール/サンダル/ビジネスシューズ/パンプス/ブーツ/ペアリング/リング/ネックレス/ペンダント/ピアス/イアリング/ブローチ/ブレスレット/ライター/手袋/傘/ベルト/ペン/リストバンド/アンクレット/アクセサリー/サングラス/帽子/マフラー/ハンカチ/ネクタイ/ストール/スカーフ/バングル/カットソー/アンサンブル/ジャケット/コート/ブルゾン/ワンピース/ニット/シャツメンズ/毛皮/Tシャツ/キャミソール/タンクトップ/パーカー/ベスト/ポロシャツ/ジーンズ/スカート/スーツなど</p>
		</section>
		<?php
        // 買取方法
        get_template_part('_purchase');
      ?>
		<section id="user_voice">
			<h3>ご利用いただいたお客様の声</h3>
			<p class="user_voice_text1">ちょうど家の整理をしていたところ、家のポストにチラシが入っていたので、ブランドリバリューに電話してみました。今まで買取店を利用したことがなく、不安でしたがとても丁寧な電話の対応とブランドリバリューの豊富な取り扱い品目を魅力に感じ、出張買取を依頼することにしました。 絶対に売れないだろうと思った、動かない時計や古くて痛んだバッグ、壊れてしまった貴金属のアクセサリーなども高額で買い取っていただいて、とても満足しています。古紙幣や絵画、食器なども買い取っていただけるとのことでしたので、また家を整理するときにまとめて見てもらおうと思います。 </p>
			<h3>鑑定士からのコメント</h3>
			<p class="user_voice_text2">家の整理をしているが、何を買い取ってもらえるか分からないから一回見に来て欲しいとのことでご連絡いただきました。 買取店が初めてで不安だったが、丁寧な対応に非常に安心しましたと笑顔でおっしゃって頂いたことを嬉しく思います。 物置にずっとしまっていた時計や、古いカバン、壊れてしまったアクセサリーなどもしっかりと価値を見極めて高額で提示させて頂きましたところ、お客様もこんなに高く買取してもらえるのかと驚いておりました。 これからも家の不用品を整理するときや物の価値判断ができないときは、すぐにお電話しますとおっしゃって頂きました。 </p>
		</section>
	</div>
	<!-- lp_main -->
</div>
<!-- #primary -->

<?php

get_footer();
