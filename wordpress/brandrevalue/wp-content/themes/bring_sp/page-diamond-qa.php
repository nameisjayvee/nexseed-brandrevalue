<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
  //'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  '001.jpg::ルースダイヤ　1.098ct　G　SI2　G　NONE::330,000::335,000::340,000::10,000',
  '002.jpg::ダイヤネックレス　Pt850　0.81ct　MD0.23ct::340,000::330,000::350,000 ::17,000',
  '003.jpg::ヴァンクリーフ&アーペル　クロアミニ　アチュールクロス　ダイヤネックレス::117,000::105,000::110,000 ::5,000',
  '004.jpg::ダイヤリング　Pt900　1.00ｃｔ　0.4ｃｔ::62,000::66,000::70,000::8,000',
  '005.jpg::ティファニー　Pt900　ダイヤリング::30,000::27,000::32,000::5,000',
  '006.jpg::ルースダイヤ　1.006ct　H　VS2　VG　NONE　::440,000::445,000::450,000::1,000',
  '007.jpg::ダイヤリング　Pt900　0.51ct　::45,000::47,800::50,000::5,000',
  '008.jpg::ルースダイヤ　1.793ct　J　SI2　G　NONE　::620,000::611,000::630,000::19,000',
  '009.jpg::ダイヤリング　K18　1.48ｃｔ　::58,000::60,000::63,000::5,000',
  '010.jpg::ブルガリ　B-ZERO1　ダイヤネネックレス　K18WG::220,000::215,000::225,000::10,000',
  '011.jpg::ブシュロン　ディアマン　ダイヤネックレス　750::225,000::219,000::230,000::11,000',
  '012.jpg::カルティエ　ハーフダイヤラブリング::97,000::93,000::103,000::10,000',
);


get_header(); ?>

<div id="primary" class="cat-page content-area">
	<div class="mv_area "> <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/lp_main/cat_diamond_main.jpg" alt="あなたのダイヤモンドお売り下さい！"> </div>
	<p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
	<p class="main_bottom">ダイヤモンド買取！！業界最高峰の買取価格をお約束いたします。</p>
	<div class="lp_main">
		<section class="dia_qa_cont">
			<h3>ダイヤモンド買取Q&A</h3>
			<div>
				<p class="dia_qa"><span>A1</span>４Cとは、なんですか？</p>
				<p class="dia_ans"><span>Q1</span>ダイヤモンドの４Cとは、クラリティ(Clarity)、カラー(Color)、カット(Cut)、カラット(Carat)のダイヤモンドの価値を決める4要素の頭文字をとったものです。ダイヤモンドの買取金額は、４Cによって大きく変動する為、大変重要は基準となっております。</p>
				<a  href="<?php echo home_url('/cat/diamond#l01'); ?>">詳細はこちら</a> </div>
			<div>
				<p class="dia_qa"><span>A2</span>鑑定書がない場合、ダイヤモンドの買取金額は、安くなってしまいますか？</p>
				<p class="dia_ans"><span>Q2</span>鑑定書がない場合でもダイヤモンドの買取金額は変わりません。ブランドリバリューには、ダイヤモンドの４Cと国際相場を正確に把握しているダイヤモンド専門のバイヤーがいる為、鑑定書がなくとも正確な目利きをすることができ、業界最高峰の買取価格を実現しております。</p>
			</div>
			<div>
				<p class="dia_qa"><span>A3</span>ダイヤモンドの買取金額は、どのように決定されますか？</p>
				<p class="dia_ans"><span>Q3</span>ダイヤモンドの買取金額は、４Cと国際相場をもとに決定されます。鑑定書がない場合は、お店（バイヤー）により、４Cの見立てが異なるため買取金額にバラつきが出てしまうことが良くあります。ブランドリバリューでは、徹底した教育を行い、鑑定書がなくとも正確な査定額がだせるようバイヤーの品質向上に取り組んでおります。</p>
			</div>
			<div>
				<p class="dia_qa"><span>A4</span>付属品の有無は、買取金額に影響しますか？</p>
				<p class="dia_ans"><span>Q4</span>ブランドジュエリーの場合は、箱や保証書の付属品の有無によって買取金額が変動しますが、ノーブランドのジュエリーやダイヤモンドの場合、買取金額に影響いたしません。</p>
			</div>
			<div>
				<p class="dia_qa"><span>A5</span>古いダイヤモンドは、買取可能ですか？</p>
				<p class="dia_ans"><span>Q5</span>古いダイヤモンドでも買取可能です。ダイヤモンドは、経年劣化により品質に影響することはありませんので、買取金額が著しく下がるケースや買取ができないといったケースは、一切ございません。</p>
			</div>
			<div>
				<p class="dia_qa"><span>A6</span>買取の際に必要なものはありますか？</p>
				<p class="dia_ans"><span>Q6</span>買取の際には、身分証が必要となります。犯罪防止を目的とし、古物営業法によりお客様の身分証を確認することが定められている為、必ず確認を行っております。ご協力のほどよろしくお願いいたします。</p>
			</div>
			<div>
				<p class="dia_qa"><span>A7</span>どのような査定方法がありますか？</p>
				<p class="dia_ans"><span>Q7</span>店頭査定・出張査定・宅配査定をご用意しております。お時間がないという方や遠方に住んでいて来店が難しいという方には、出張査定・宅配査定をご用意しております。高価なお品物なので宅配で郵送するのは、不安というお客様もいらっしゃると思いますが、ご安心ください。ブランドリバリューでは、年間4000件以上の郵送・宅配でのお取引実績がございます。また、郵送に際する保険サービスもご用意しておりますので、万が一の破損や紛失にもご対応可能でございます。また、郵送費や出張費など、関連する経費はすべて無料でございます。</p>
			</div>
		</section>

	</div>
				<ul class="dia_nav">
		<li><a href="<?php echo home_url('/cat/diamond/diamond-qa'); ?>">ダイヤモンド 買取Q&A</a></li>
		<li><a href="<?php echo home_url('cat/diamond/diamond-term'); ?>">ダイヤモンド 用語集</a></li>
		<li><a href="<?php echo home_url('/cat/diamond/diamond-app'); ?>">ダイヤモンド 鑑定書</a></li>
		<li><a href="<?php echo home_url('/cat/diamond/diamond-voice'); ?>">お客様の声</a></li>
		<li><a href="<?php echo home_url('/cat/diamond/diamond-staff'); ?>">査定士紹介</a></li>
	</ul>
	<!-- lp_main -->
</div>
<!-- #primary -->
<script src="//kaitorisatei.info/brandrevalue/wp-content/themes/bring/js/gaisan.js" type="text/javascript"></script>
<?php

get_footer();
