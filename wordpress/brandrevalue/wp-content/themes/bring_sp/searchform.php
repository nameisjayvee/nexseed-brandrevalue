<p class="frm_tx">ブランド名からページを探す</p>
<form method="get" class="searchform"
	action="<?php echo esc_url( home_url('/') ); ?>">
	<input type="search" placeholder="例：ルイヴィトン" name="s" class="searchfield" value="" />
	<input type="image" src="<?php echo get_s3_template_directory_uri() ?>/img/btn4.gif" value="&#xf002;" alt="検索" title="検索" class="searchsubmit">
</form>