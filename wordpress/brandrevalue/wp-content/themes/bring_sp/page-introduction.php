<?php
get_header(); ?>

<div class="mv_area ">
<img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kv-introduction.png " alt="買い取り実績">
</div>
<div class="intrItem_cnt ">
<h2><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/jsseki_tl.png " alt="買取実績 "></h2>
<!--★ブランド買取実績★-->
<div class="item_box">
	<h3>ブランド買取</h3>
  <ul class="topItem_list ">
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_01.png " alt="# "></p>
      <p class="item_name ">ロイヤルオーク オフショア</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：2,630,000円<br>
                        <span class="item_blue">B社</span>：2,750,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">2,900,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">270,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_02.png " alt="# "></p>
      <p class="item_name ">コンプリケーテッド</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：4,559,000円<br>
                        <span class="item_blue">B社</span>：4,465,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">4,700,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">235,000円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_03.png " alt="# "></p>
      <p class="item_name ">コンプリケーション</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：3,977,000円<br>
                        <span class="item_blue">B社</span>：3,895,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">4,100,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">205,000円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_brand01.png " alt="# "></p>
      <p class="item_name ">パテックフィリップ ワールドタイム</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：3,700,000円<br>
                        <span class="item_blue">B社</span>：3,650,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">3,900,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">250,000円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_04.png " alt="# "></p>
      <p class="item_name ">マトラッセ 長財布</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：97,000円<br>
                        <span class="item_blue">B社</span>：95,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">100,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">5,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_05.png " alt="# "></p>
      <p class="item_name ">ベアンスフレ</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：223,100円<br>
                        <span class="item_blue">B社</span>：218,500円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">230,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">11,500円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_06.png " alt="# "></p>
      <p class="item_name ">HERMES バーキン30</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：1,450,000円<br>
                        <span class="item_blue">B社</span>：1,520,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">1,600,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">150,000円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_07.png " alt="# "></p>
      <p class="item_name ">CELINE バッグ</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：1,805,000円<br>
                        <span class="item_blue">B社</span>：1,843,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">1,900,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">57,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_brand02.png " alt="# "></p>
      <p class="item_name ">グッチ レザー ライダースジャケット</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：70,000円<br>
                        <span class="item_blue">B社</span>：65,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">76,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">11,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_brand03.png " alt="# "></p>
      <p class="item_name ">グッチ ウール×アンゴラ ハーフコート</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：27,000円<br>
                        <span class="item_blue">B社</span>：23,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">30,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">7,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_brand04.png " alt="# "></p>
      <p class="item_name ">サンローラン チェーンハーネスブーツ</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：77,500円<br>
                        <span class="item_blue">B社</span>：76,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">80,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">4,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_brand05.png " alt="# "></p>
      <p class="item_name ">フェラガモ　スタッズ　フラットシューズ</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：15,500円<br>
                        <span class="item_blue">B社</span>：15,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">16,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">1,000円</p>
    </li>
  </ul>
	</div>

<!--★金・プラチナ買取実績★-->
<div class="item_box">
  <h3>金・プラチナ買取</h3>
  <ul class="topItem_list ">
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_gold01.png " alt="# "></p>
      <p class="item_name ">K18 120g</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：424,860円<br>
                        <span class="item_blue">B社</span>：416,100円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">438,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">21,900円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_gold02.png " alt="# "></p>
      <p class="item_name ">Pt900 80g</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：308,848円<br>
                        <span class="item_blue">B社</span>：302,480円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">318,400<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">15,920円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_gold03.png " alt="# "></p>
      <p class="item_name ">ダイヤリング Pt900 8.0g</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：247,500円<br>
                        <span class="item_blue">B社</span>：242,500円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">250,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">7,500円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_gold04.png " alt="# "></p>
      <p class="item_name ">メイプルリーフ金貨ダイヤネックレストップ</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：49,500円<br>
                        <span class="item_blue">B社</span>：48,500円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">50,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">1,500円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_gold05.png " alt="# "></p>
      <p class="item_name ">K22コイン色石付きネックレストップ</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：94,700円<br>
                        <span class="item_blue">B社</span>：94,400円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">95,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">600円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_gold06.png " alt="# "></p>
      <p class="item_name ">Ｋ18ＰＧダイヤリング</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：62,000円<br>
                        <span class="item_blue">B社</span>：61,500円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">63,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">1,500円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_gold07.png " alt="# "></p>
      <p class="item_name ">プラチナリング Pt950 3.5g</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：15,840円<br>
                        <span class="item_blue">B社</span>：15,520円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">16,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">480円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_gold08.png " alt="# "></p>
      <p class="item_name ">インゴットネックレストップ 枠K18 22.2g</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：90,160円<br>
                        <span class="item_blue">B社</span>：88,320円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">92,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">3,680円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_gold09.png " alt="# "></p>
      <p class="item_name ">K18ダイヤリング 5g</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：18,600円<br>
                        <span class="item_blue">B社</span>：18,500円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">19,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">500円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_gold10.png " alt="# "></p>
      <p class="item_name ">ダイヤリング K24 6.8g</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：34,650円<br>
                        <span class="item_blue">B社</span>：34,300円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">35,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">700円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_gold11.png " alt="# "></p>
      <p class="item_name ">K18WG 8.4g</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：29,179円<br>
                        <span class="item_blue">B社</span>：28,577円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">30,081<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">1,504円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_gold12.png " alt="# "></p>
      <p class="item_name ">K14パールブローチ</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：17,600円<br>
                        <span class="item_blue">B社</span>：17,200円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">18,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">800円</p>
    </li>
  </ul>
  </div>

<!--★宝石買取実績★-->
<div class="item_box">
  <h3>宝石買取</h3>
  <ul class="topItem_list ">
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_gem05.png " alt="# "></p>
      <p class="item_name ">ダイヤモンド ルース</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：194,000円<br>
                        <span class="item_blue">B社</span>：190,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">200,000<span class="small">円<br>プラス地金代</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">10,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_gem06.png " alt="# "></p>
      <p class="item_name ">K18YG ダイヤモンドリング</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：145,500円<br>
                        <span class="item_blue">B社</span>：142,500円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">150,000<span class="small">円<br>プラス地金代</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">7,500円</p>
    </li>

     <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_gem07.png " alt="# "></p>
      <p class="item_name "><span class="small">ティファニー ソレスト ダイヤリング</span></p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：320,100円<br>
                        <span class="item_blue">B社</span>：313,500円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">330,000<span class="small">円<br>プラス地金代</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">16,500円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_gem08.png " alt="# "></p>
      <p class="item_name ">ピンクダイヤモンド ルース</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：4,850,000円<br>
                        <span class="item_blue">B社</span>：4,750,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">5,000,000<span class="small">円<br>プラス地金代</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">250,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_gem09.png " alt="# "></p>
      <p class="item_name ">ペアシェイプ ダイヤモンドルース</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：3,395,500円<br>
                        <span class="item_blue">B社</span>：3,325,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">3,500,000<span class="small">円<br>プラス地金代</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">175,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_gem10.png " alt="# "></p>
      <p class="item_name "><span class="small">カルティエ バレリーナ ハーフエタニティリング</span></p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：388,000円<br>
                        <span class="item_blue">B社</span>：380,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">400,000<span class="small">円<br>プラス地金代</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">20,000円</p>
    </li>
  </ul>
  </div>

<!--★時計買取実績★-->
<div class="item_box">
  <h3>時計買取</h3>
  <ul class="topItem_list ">
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_watch09.png " alt="# "></p>
      <p class="item_watch ">パテックフィリップ<br>ノーチラス Ref5712</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：3,201,000円<br>
                        <span class="item_blue">B社</span>：3,135,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">3,300,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">165,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_watch10.png " alt="# "></p>
      <p class="item_watch ">BREITLING<br>ナビタイマー ref.A022B01NP</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：388,000円<br>
                        <span class="item_blue">B社</span>：380,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">400,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">20,000円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_watch11.png " alt="# "></p>
      <p class="item_watch ">パネライ<br>ルミノール クロノデイライト</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：582,000円<br>
                        <span class="item_blue">B社</span>：570,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">600,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">30,000円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_watch12.png " alt="# "></p>
      <p class="item_watch ">ボールウォッチ<br>エンジニア ハイドロカーボン</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：77,600円<br>
                        <span class="item_blue">B社</span>：76,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">80,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">4,000円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_watch01.png " alt="# "></p>
      <p class="item_watch ">ロレックス<br>サブマリーナ Ref.116610LN</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：670,000円<br>
                        <span class="item_blue">B社</span>：620,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">710,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">90,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_watch02.png " alt="# "></p>
      <p class="item_watch ">オーデマピゲ<br>ロイヤルオーク オフショア</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：2,750,000円<br>
                        <span class="item_blue">B社</span>：2,630,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">2,900,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">270,000円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_watch03.png " alt="# "></p>
      <p class="item_watch ">パテックフィリップ<br>コンプリケーテッド</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：4,100,000円<br>
                        <span class="item_blue">B社</span>：3,900,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">4,700,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">800,000円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_watch04.png " alt="# "></p>
      <p class="item_watch ">ブレゲ<br>コンプリケーション</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：3,600,000円<br>
                        <span class="item_blue">B社</span>：3,400,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">4,100,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">700,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_watch05.png " alt="# "></p>
      <p class="item_watch ">ウブロ<br>クラシックフュージョン 521.NX.1170.R</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：18,600円<br>
                        <span class="item_blue">B社</span>：18,500円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">620,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">40,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_watch13.png " alt="# "></p>
      <p class="item_watch ">OMEGA<br>スピードマスター レーシング コーアクシャル</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：194,000円<br>
                        <span class="item_blue">B社</span>：193,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">200,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">7,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_watch14.png " alt="# "></p>
      <p class="item_watch ">ディオール<br>シフルルージュ ブラックタイム クロノ</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：150,000円<br>
                        <span class="item_blue">B社</span>：120,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">165,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">45,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_watch15.png " alt="# "></p>
      <p class="item_watch ">ロレックス<br>デイトナ 116505 黒文字盤</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：2,250,000円<br>
                        <span class="item_blue">B社</span>：2,200,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">2,300,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">100,000円</p>
    </li>
  </ul>
  </div>

<!--★バッグ買取実績★-->
<div class="item_box">
  <h3>バッグ買取</h3>
  <ul class="topItem_list ">
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_bag09.png " alt="# "></p>
      <p class="item_name ">HERMES<br>エブリン3PM</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：184,300円<br>
                        <span class="item_blue">B社</span>：180,500円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">190,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">9,500円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_bag10.png " alt="# "></p>
      <p class="item_name ">PRADA<br>ハンドバッグ</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：116,400円<br>
                        <span class="item_blue">B社</span>：114,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">120,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">6,000円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_bag11.png " alt="# "></p>
      <p class="item_name ">GUCCI<br>バンブーバッグ</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：29,100円<br>
                        <span class="item_blue">B社</span>：28,500円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">30,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">1,500円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_bag12.png " alt="# "></p>
      <p class="item_name ">LOUIS VUITTON<br>モンスリGM</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：58,200円<br>
                        <span class="item_blue">B社</span>：57,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">60,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">3,000円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_bag01.png " alt="# "></p>
      <p class="item_name ">HERMES<br>バーキン30</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：1,450,000円<br>
                        <span class="item_blue">B社</span>：1,520,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">1,600,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">150,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_bag02.png " alt="# "></p>
      <p class="item_name ">CHANEL<br>マトラッセ チェーンバッグ</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：100,000円<br>
                        <span class="item_blue">B社</span>：130,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">150,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">50,000円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_bag03.png " alt="# "></p>
      <p class="item_watch ">LOUIS VUITTON<br>ダミエ ネヴァーフルMM</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：92,000円<br>
                        <span class="item_blue">B社</span>：104,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">110,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">18,000円</p>
    </li>
    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_bag04.png " alt="# "></p>
      <p class="item_watch ">CELINE<br>ラゲージマイクロショッパーバッグ</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：175,000円<br>
                        <span class="item_blue">B社</span>：160,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">185,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">25,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_bag13.png " alt="# "></p>
      <p class="item_watch ">ロエベ<br>レザー 2WAYバッグ</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：23,000円<br>
                        <span class="item_blue">B社</span>：22,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">25,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">3,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_bag14.png " alt="# "></p>
      <p class="item_watch ">グッチ<br>グッチシマ レザートートバッグ</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：38,000円<br>
                        <span class="item_blue">B社</span>：36,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">40,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">4,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_bag15.png " alt="# "></p>
      <p class="item_watch ">プラダ<br>キャンバス×サフィアーノ ダブルバッグ</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：120,000円<br>
                        <span class="item_blue">B社</span>：113,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">126,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">13,000円</p>
    </li>

    <li class="topItem ">
      <p class="item_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item_bag16.png " alt="# "></p>
      <p class="item_watch ">ボッテガヴェネタ<br>イントレチャート カバ</p>
                      <p class="item_price_ab ">
                        <span class="item_red">A社</span>：198,000円<br>
                        <span class="item_blue">B社</span>：190,000円</p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">200,000<span class="small">円</span></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/sagaku.png " alt="他社比較差額 "></p>
    <p class="item_difference">10,000円</p>
    </li>
  </ul>
  </div>

<?php
  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
