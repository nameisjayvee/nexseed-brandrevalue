<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @see https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>
    <!DOCTYPE html>
    <html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico">
        <link href="<?php echo get_template_directory_uri(); ?>/css/reset.css " rel="stylesheet ">
        <link href="<?php echo get_template_directory_uri(); ?>/css/style.css " rel="stylesheet ">
        <link href="<?php echo get_template_directory_uri(); ?>/css/flickity.css" rel="stylesheet ">
        <link href="<?php echo get_template_directory_uri(); ?>/css/customstyle.css" rel="stylesheet ">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/slick-theme.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/slick.css">
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
        <!--追加アニメーションオプション　-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome-animation/0.0.10/font-awesome-animation.css" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/earlyaccess/roundedmplus1c.css" rel="stylesheet" />
        <script src="//statics.a8.net/a8sales/a8sales.js"></script>
        <?php wp_head(); ?>
        <?php get_template_part('template-parts/gtm', 'head'); ?>
    </head>

    <body <?php body_class(); ?>>
        <?php get_template_part('template-parts/gtm', 'body'); ?>
        <div class="container ">
            <!--<h1>
                <?php wp_title(); ?>
            </h1>-->
            <!--<div class="head_tab">
                <ul>
                    <li class="tab01">時計・ブランド
                        <br>高額買取</li>
                    <a href="//kaitorisatei.info/" target="_blank">
                        <li class="tab02">メンズブランド
                            <br>古着買取</li>
                    </a>
                </ul>
            </div>-->

            <header>
                <div class="head_in">
                    <h1><a href="<?php echo home_url(''); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/top_logo.png " alt="<?php wp_title(); ?>"></a></h1>
                    <div id="sp-icon" class="sp-open"><span></span></div>
                    <div class="custom-backshadow2"></div>
                </div>
                <div class="custom-backshadow"></div>
                <div id="menu_test">
                    <div class="search_box"><?php get_search_form(); ?></div>
                    <div class="cus-ul-con">
                        <ul class="cus-ul">
                            <li class="cus-li">
                                <a href="tel:0120-970-060"><img data-src="<?php echo get_template_directory_uri(); ?>/img/menu/menu-btn-01@2x.png" class="comp-image" alt=""></a>
                            </li>
                            <li class="cus-li">
                                <a href="<?php echo home_url('line'); ?>"><img data-src="<?php echo get_template_directory_uri(); ?>/img/menu/menu-btn-02@2x.png" class="comp-image" alt=""></a>
                            </li>
                            <li class="cus-li">
                                <a href="<?php echo home_url('purchase1'); ?>"><img data-src="<?php echo get_template_directory_uri(); ?>/img/menu/menu-btn-03@2x.png" class="comp-image" alt=""></a>
                            </li>
                        </ul>
                    </div>
                    <div class="cus-ul-con">
                        <ul class="cus-ul2">
                            <li class="cus-li2">
                                <a href="<?php echo home_url('purchase/takuhai'); ?>"><img data-src="<?php echo get_template_directory_uri(); ?>/img/menu/menu-btn-04@2x.png" class="comp-image2" alt=""></a>
                            </li>
                            <li class="cus-li2">
                                <a href="<?php echo home_url('purchase/syutchou'); ?>"><img data-src="<?php echo get_template_directory_uri(); ?>/img/menu/menu-btn-05@2x.png" class="comp-image2" alt=""></a>
                            </li>
                        </ul>
                        <ul class="cus-ul2">
                            <li class="cus-li2">
                                <a href="<?php echo home_url('purchase/tentou'); ?>"><img data-src="<?php echo get_template_directory_uri(); ?>/img/menu/menu-btn-06@2x.png" class="comp-image2" alt=""></a>
                            </li>
                            <li class="cus-li2">
                                <a href="<?php echo home_url('customer'); ?>"><img data-src="<?php echo get_template_directory_uri(); ?>/img/menu/menu-btn-07@2x.png" class="comp-image2" alt=""></a>
                            </li>
                        </ul>
                    </div>
                    <button id="btn1" class="cus-btn">
                        <img data-src="<?php echo get_template_directory_uri(); ?>/img/menu/accordion01@2x.png" alt="">
                    </button>
                    <div class="btn1-content">
                        <div class="cus-row">
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/watch/rolex'); ?>">
                                        <span>ロレックス</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/bag/chanel'); ?>">
                                        <span>シャネル</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/watch/omega'); ?>">
                                        <span>オメガ</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/gem/tiffany'); ?>">
                                        <span>ティファニー</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/bag/gucci'); ?>">
                                        <span>グッチ</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/bag/celine'); ?>">
                                        <span>セリーヌ</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/gem/harrywinston'); ?>">
                                        <span>ハリーウィンストン</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/watch/audemarspiguet'); ?>">
                                        <span>オーデマピゲ</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/watch/hublot'); ?>">
                                        <span>ウブロ</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/watch/tudor'); ?>">
                                        <span>チュードル</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/wallet/louisvuitton'); ?>">
                                        <span>ルイヴィトン</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/bag/hermes'); ?>">
                                        <span>エルメス</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/gem/cartier'); ?>">
                                        <span>カルティエ</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/gem/bulgari'); ?>">
                                        <span>ブルガリ</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/bag/prada'); ?>">
                                        <span>プラダ</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/bag/bottegaveneta'); ?>">
                                        <span>ボッテガヴェネタ</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/gem/vancleefarpels'); ?>">
                                        <span>ヴァンクリーフ＆ アーペル</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/watch/patek-philippe'); ?>">
                                        <span>パテックフィリップ</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/watch/franckmuller'); ?>">
                                        <span>フランクミューラー</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div id="link-icon" class="link-con">
                                    <a href="<?php echo home_url('cat/gem/piaget'); ?>">
                                        <span>ピアジェ</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="cus-row2">
                            <div class="cus-col">
                                <div id="link-icon" class="link-con2">
                                    <a href="<?php echo home_url('brand'); ?>">
                                        <span>その他取り扱いブランドはコチラ</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button id="btn2" class="cus-btn">
                        <img data-src="<?php echo get_template_directory_uri(); ?>/img/menu/accordion02@2x.png" alt="">
                    </button>
                    <div class="btn2-content">
                        <ul class="cat_list align-center">
                            <li>
                                <a href="https://kaitorisatei.info/brandrevalue/cat/gold"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring_sp/images/cat01.png" alt="金・プラチナ"></a>
                            </li>
                            <li>
                                <a href="https://kaitorisatei.info/brandrevalue/cat/gem"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring_sp/images/cat02.png" alt="宝石"></a>
                            </li>
                            <li>
                                <a href="https://kaitorisatei.info/brandrevalue/cat/watch"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring_sp/images/cat03.png" alt="時計"></a>
                            </li>
                            <li>
                                <a href="https://kaitorisatei.info/brandrevalue/cat/bag"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring_sp/images/cat04.png" alt="バッグ"></a>
                            </li>
                            <li>
                                <a href="https://kaitorisatei.info/brandrevalue/cat/outfit"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring_sp/images/cat05.png" alt="洋服・毛皮"></a>
                            </li>
                            <li>
                                <a href="https://kaitorisatei.info/brandrevalue/kimono"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring_sp/images/cat10.png" alt="着物"></a>
                            </li>
                            <li>
                                <a href="https://kaitorisatei.info/brandrevalue/cat/wallet"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring_sp/images/cat06.png" alt="ブランド・財布"></a>
                            </li>
                            <li>
                                <a href="https://kaitorisatei.info/brandrevalue/cat/shoes"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring_sp/images/cat07.png" alt="靴"></a>
                            </li>
                            <li>
                                <a href="https://kaitorisatei.info/brandrevalue/cat/diamond"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring_sp/images/cat08.png" alt="ダイヤモンド"></a>
                            </li>
                            <li>
                                <a href="https://kaitorisatei.info/brandrevalue/cat/brandjewelery"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring_sp/images/cat_bj.png" alt="ブランドジュエリー"></a>
                            </li>
                            <li>
                                <a href="https://kaitorisatei.info/brandrevalue/cat/antique"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring_sp/images/cat09.png" alt="骨董品"></a>
                            </li>
                            <li>
                                <a href="https://kaitorisatei.info/brandrevalue/cat/mement"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring_sp/images/btn-memento_sp.png" alt="遺品"></a>
                            </li>
                            <li>
                                <a href="https://kaitorisatei.info/brandrevalue/cat/antique_rolex"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring_sp/images/catanti_rolex.png" alt="アンティークロレックス"></a>
                            </li>
                        </ul>
                    </div>
                    <button id="btn3" class="cus-btn">
                        <img data-src="<?php echo get_template_directory_uri(); ?>/img/menu/accordion03@2x.png" alt="">
                    </button>
                    <div class="btn3-content">
                        <div class="cus-row">
                            <div class="cus-col">
                                <div class="link-con">
                                    <a href="https://kaitorisatei.info/brandrevalue/service">
                                        <span>買取サービスについて</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div class="link-con">
                                    <a href="https://kaitorisatei.info/brandrevalue/shop">
                                        <span>店舗一覧</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div class="link-con">
                                    <a href="https://kaitorisatei.info/brandrevalue/brand">
                                        <span>取扱ブランド</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div class="link-con">
                                    <a href="https://kaitorisatei.info/brandrevalue/introduction">
                                        <span>実績紹介</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div class="link-con">
                                    <a href="https://kaitorisatei.info/brandrevalue/faq">
                                        <span>よくある質問</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div class="link-con">
                                    <a href="http://www.staygold-sg.com/recruit">
                                        <span>採用情報</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div class="link-con">
                                    <a href="https://kaitorisatei.info/brandrevalue/about-purchase">
                                        <span>ブランド買取について</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div class="link-con">
                                    <a href="https://kaitorisatei.info/brandrevalue/purchase">
                                        <span>買取方法</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div class="link-con">
                                    <a href="https://kaitorisatei.info/brandrevalue/cat">
                                        <span>取扱カテゴリ</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div class="link-con">
                                    <a href="https://kaitorisatei.info/brandrevalue/voice">
                                        <span>お客様の声</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div class="link-con">
                                    <a href="https://kaitorisatei.info/brandrevalue/line">
                                        <span>LINE査定</span>
                                    </a>
                                </div>
                            </div>
                            <div class="cus-col">
                                <div class="link-con">
                                    <a href="https://kaitorisatei.info/brandrevalue/customer">
                                        <span>お問い合わせ</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn-con">
                        <button id="btn-close2">
                            <img data-src="<?php echo get_template_directory_uri(); ?>/img/menu/btn-close@2x.png" alt="">
                        </button>
                    </div>
                </div>
                <script>
                    $("#sp-icon").click(function(e) {
                        (function () {
                            var _overlay = document.getElementById('menu_test');
                            var _clientY = null; // remember Y position on touch start

                            _overlay.addEventListener('touchstart', function (event) {
                                if (event.targetTouches.length === 1) {
                                    // detect single touch
                                    _clientY = event.targetTouches[0].clientY;
                                }
                            }, false);

                            _overlay.addEventListener('touchmove', function (event) {
                                if (event.targetTouches.length === 1) {
                                    // detect single touch
                                    disableRubberBand(event);
                                }
                            }, false);

                            function disableRubberBand(event) {
                                var clientY = event.targetTouches[0].clientY - _clientY;

                                if (_overlay.scrollTop === 0 && clientY > 0) {
                                    // element is at the top of its scroll
                                    event.preventDefault();
                                }

                                if (isOverlayTotallyScrolled() && clientY < 0) {
                                    //element is at the top of its scroll
                                    event.preventDefault();
                                }
                            }

                            function isOverlayTotallyScrolled() {
                                // https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollHeight#Problems_and_solutions
                                return _overlay.scrollHeight - _overlay.scrollTop <= _overlay.clientHeight;
                            }
                        }())
                        if ($(this).hasClass('sp-open')) {
                            $('#menu_test').toggleClass('menu-in')
                            $('#menu_test').css('bottom', $('.btm_nav').innerHeight())
                            $('.custom-backshadow,.custom-backshadow2').toggleClass('shadow-in')
                            $('html').toggleClass('html-in')
                            $('body').toggleClass('body-in')
                            $('.custom-backshadow,.custom-backshadow2').on('touchmove', function(e){
                                e.preventDefault()
                            })
                        } else {
                            $('#menu_test').toggleClass('menu-in')
                            $('.custom-backshadow,.custom-backshadow2').toggleClass('shadow-in')
                            $('html').toggleClass('html-in')
                            $('body').toggleClass('body-in')
                        }
                    });
                    $('#btn-close2').click(function() {
                        $('#sp-icon').toggleClass('sp-close')
                        $('#sp-icon').addClass('sp-open')
                        $('#menu_test').toggleClass('menu-in')
                        $('.custom-backshadow,.custom-backshadow2').toggleClass('shadow-in')
                        $('html').toggleClass('html-in')
                        $('body').toggleClass('body-in')
                    })
                    $("#btn1" ).click(function() {
                        $('.btn1-content').slideToggle('fast')
                    });
                    $("#btn2" ).click(function() {
                        $('.btn2-content').slideToggle('fast')
                    });
                    $("#btn3" ).click(function() {
                        $('.btn3-content').slideToggle('fast')
                    });
                </script>
                <div id="add_box">
                <div id="addmenu01">
                無料査定<span>▼</span>
                </div>
                <div id="addmenu01wrap">
                    <p><a href="tel:0120-970-060"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/img/head_tel.png " alt="電話で無料査定"></a></p>
                    <p><a href="<?php echo home_url('line'); ?>"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/img/head_line.png " alt="LINEで無料査定"></a></p>
                </div>
                <div id="addmenu02">
                店舗一覧<span>▼</span>
                </div>
                <div id="addmenu02wrap">
                <ul>
                <li><a href="<?php echo home_url('ginza'); ?>"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/images/str_img.png " alt="ブランドリバリュー銀座店舗案内 ">
                    <p>銀座店</p></a>
                </li>
                <li>
                <a href="<?php echo home_url('shibuya'); ?>"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/images/str_img02.png " alt="ブランドリバリュー銀座店舗案内 ">
                    <p>渋谷店</p>
                </a>
                </li>
                <li>
                <a href="<?php echo home_url('shinjuku'); ?>"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/images/str_img03.png " alt="ブランドリバリュー銀座店舗案内 ">
                    <p>新宿店</p>
                </a>
                </li>
                <li>
                <a href="<?php echo home_url('shinsaibashi'); ?>"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/images/str_img00.png " alt="ブランドリバリュー銀座店舗案内 ">
                    <p>心斎橋店</p>
                </a>
                </li>
                <li >
                <a href="<?php echo home_url('center'); ?>" style="margin-left:25%;"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/images/str_img05.png " alt="ブランドリバリュー買取センター ">
                    <p>買取センター</p>
                </a>
                </li>

                </ul>
                </div>



                </div>

            </header>

            <!-- #site-navigation -->
            <div id="content">
