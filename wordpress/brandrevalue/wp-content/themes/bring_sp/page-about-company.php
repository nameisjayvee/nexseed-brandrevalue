<?php

get_header(); ?>

<div class="mv_area ">
  <img data-src="images/kv.png " alt="BRAMD REVALUE ">
</div>
<div id="company">
  <table>
    <tr>
      <th>会社名</th>
      <td>株式会社STAY GOLD</td>
    </tr>
    <tr>
      <th>店舗名</th>
      <td>BRAND REVALUE（ブランドリバリュー）</td>
    </tr>
    <tr>
      <th>所在地</th>
      <td>東京都渋谷区渋谷二丁目12版11号 渋谷KKビル8階</td>
    </tr>
    <tr>
      <th>TEL</th>
      <td>0120-970-060</td>
    </tr>
    <tr>
      <th>設立</th>
      <td>平成26年4月14日</td>
    </tr>
    <tr>
      <th>役員</th>
      <td>代表取締役　柏村 淳司</td>
    </tr>
    <tr>
      <th>資本金</th>
      <td>500万円</td>
    </tr>
    <tr>
      <th>事業内容</th>
      <td>衣料品、宝石、貴金属、アクセサリー等の買取、販売及び輸出入</td>
    </tr>
    <tr>
      <th>古物商許可番号</th>
      <td>303311408927</td>
    </tr>
  </table>
</div>

<?php

  // お問い合わせ
  get_template_part('_action');

  // 買取方法

  // フッター
  get_footer();
