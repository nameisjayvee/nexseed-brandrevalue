<?php
  
get_header(); ?>

<div class="mv_area ">
            <img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit_kv.png " alt="BRAMD REVALUE ">
        </div>
        <div class="cat_cnt">
            <h2 class="cat_tl">豊富なアパレル買取実績を活かし、洋服・毛皮の高額査定を可能に</h2>
            <p class="cat_tx">「古着」の価値が再認識されている昨今、ブランド洋服や毛皮といった衣類の買取市場も盛んになっています。BRAND REVALUEはもともと系列店舗で古着買取を長年行っており、効率的な再販ルートも確立されているため、他の一般的なブランド・貴金属買取ショップと比較して高い水準の買取が可能です。古い洋服でも、品質の高いブランド品なら十分価値がありますし、多少の傷みならリフォームして再販することも可能ですので、ぜひ一度お持ちください。また、古来よりエグゼクティブの証として用いられ、ますます価値が高まっている「毛皮製品」も今が高額査定のチャンス。ブランドに関わらず、銀座ナンバーワン査定価格を目指して高値で買い取らせていただきます。</p>
        </div>

        <div id="cat-jisseki">
            <h3 class="cat-jisseki_tl">買取実績</h3>
            <p class="cat_jisseki_tx">洋服・毛皮等のアパレル品は、品目が多くチェックポイントも複雑なため、一般的なブランド・貴金属買取ショップでは適切な査定が難しいこともあります。しかし、BRAND REVALUEは系列店で培ってきた豊富な古着買取の経験があり、熟練の鑑定士がスピーディかつ正確に査定を行うことが可能です。<br>
他店では値段がほとんどつかなかったような洋服・毛皮でも、BRAND REVALUEなら納得の高額査定で買い取らせていただくケースがございます。</p>
            <ul id="box-jisseki" class="list-unstyled clearfix">
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/outfit001.png" alt=""></p>
                        <p class="itemName">
                            <br>ネルシャツ</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：5,000円
                            <br>
                            <span class="blue">B社</span>：4,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">8,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>4,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/outfit002.png" alt=""></p>
                        <p class="itemName">
                            <br>カモフラ柄ジャケット</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：21,000円
                            <br>
                            <span class="blue">B社</span>：19,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">25,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>6,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/outfit003.png" alt=""></p>
                        <p class="itemName">
                            <br>キルゴアジャケット</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：26,000円
                            <br>
                            <span class="blue">B社</span>：23,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">30,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>7,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/outfit004.png" alt=""></p>
                        <p class="itemName">
                            <br>ダッフルコート</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：41,000円
                            <br>
                            <span class="blue">B社</span>：37,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">50,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>13,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/outfit005.png" alt=""></p>
                        <p class="itemName">
                            <br>ボックスパーカー</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：23,000円
                            <br>
                            <span class="blue">B社</span>：21,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">30,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>9,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/outfit006.png" alt=""></p>
                        <p class="itemName">
                            <br>スウェットパンツ</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：16,000円
                            <br>
                            <span class="blue">B社</span>：14,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">20,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>6,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/outfit007.png" alt=""></p>
                        <p class="itemName">
                            <br>スタジャン</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：41,000円
                            <br>
                            <span class="blue">B社</span>：36,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">50,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>14,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/outfit008.png" alt=""></p>
                        <p class="itemName">
                            <br>バッファロージャケット</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：25,000円
                            <br>
                            <span class="blue">B社</span>：23,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">32,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>9,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/outfit009.png" alt=""></p>
                        <p class="itemName">
                            <br>ボックスロゴＴシャツ</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：12,000円
                            <br>
                            <span class="blue">B社</span>：11,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">15,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>4,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/outfit010.png" alt=""></p>
                        <p class="itemName">
                            <br>ボーダーＴシャツ</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：11,000円
                            <br>
                            <span class="blue">B社</span>：9,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">13,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>4,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/outfit011.png" alt=""></p>
                        <p class="itemName">
                            <br>K2ダウンジャケット</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：42,000円
                            <br>
                            <span class="blue">B社</span>：39,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">50,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>11,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/outfit012.png" alt=""></p>
                        <p class="itemName">
                            <br>ダウンベスト</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：26,000円
                            <br>
                            <span class="blue">B社</span>：22,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">30,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>8,000円</p>
                    </div>
                </li>
            </ul>
            <div class="kaitori_point">
            <h3>【洋服・毛皮の高価買取ポイント】</h3>
もし購入時に同封されていた品質保証の証明書等がある場合は、査定時にお持ちいただくことをおすすめします。<br>
スムーズかつ高額な査定をしやすくなります。その他、購入時のブランド製紙袋、箱、その他小物の換え部品等の付属品も一緒にお持ちいただくと、査定額がかなりアップすることがあります。<br>
流行のデザインの洋服などの場合、目安として発売後1年以内のお品を高額査定対象とさせていただいておりますので、お早目に査定を行われることをおすすめいたします。その他、品質タグがついた洋服や、事前にクリーニングしていただいた洋服も、査定額がアップします。
            </div>
        </div>

        <div class="point_list">
            <div class="coint_bnr">
                <img src="<?php echo get_s3_template_directory_uri() ?>/images/coin_bnr01.png" alt="他の店鋪より1円でも安ければご連絡下さい。">
                <ul class="other_price">
                    <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/coin_bnr02.png" alt="他の店鋪より1円でも安ければご連絡下さい。"></li>
                    <li>ブリングは徹底した高価買取を行っております。お客様が愛着を持って身につけてきた服は、大切に買い取りさせて頂きます。 万が一、他店よりも1円でも安い価格であればおっしゃってください。なるべくお客様のご要望に答えられるよう、査定させて頂きます。<br>
                まずは、こちらから買い取り実績について、覗いてみてください。きっと、お客様の古着に相応しい、ご期待に添える価格での買い取りを行っているはずです。</li>
                </ul>
            </div>
            <h3 class="obi_tl">高価買取のポイント</h3>
            <img src="<?php echo get_s3_template_directory_uri() ?>/images/kaitori_point.png" alt="高価買い取りのポイント" class="point_img">
            <h3 class="obi_tl">ブランドリスト</h3>
            <ul class="cat_list">
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit1.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit2.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit3.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit4.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit5.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit6.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit7.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit8.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit9.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit10.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit11.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit12.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit13.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit14.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit15.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit16.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit17.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit18.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit19.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit20.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit21.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit22.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit23.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit24.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit25.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/outfit26.png" alt=""></li>
            </ul>
        </div>

<?php
  // お問い合わせ
  get_template_part('_action');
  
  // 3つのポイント
  get_template_part('_purchase');
  
  // お問い合わせ
  get_template_part('_action2');
  
  // 店舗
  get_template_part('_shopinfo');
  
  // フッター
  get_footer();