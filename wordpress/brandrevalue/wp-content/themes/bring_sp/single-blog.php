<?php
get_header(); ?>

<div class="mv_area ">
<!--<img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kv-faq.png " alt="BRAMD REVALUE ">-->
<h2>BLOG</h2>
</div>

<section>
  <div id="blog-single">
    <?php
      //-------------------------------------
      if(have_posts()): while(have_posts()): the_post();
        // タクソノミ
        $cat = get_the_terms(get_the_ID(), 'blog-cat');
        $cat = $cat[0];
        $catName = $cat->name;
        if(!empty($catName)) {
          $catName = '【'.$catName.'】';
        }

        // アイキャッチ
        $thumbnail_id = get_post_thumbnail_id();
        $image = wp_get_attachment_image_src($thumbnail_id, 'full');
    ?>

    <div id="blogArticle">
      <p class="date"><?php the_time('Y年m月d日（D）'); ?></p>
      <h1 class="title"><?php echo $catName; the_title(); ?></h1>
      <img data-src="<?php echo $image[0]; ?>">
      <article>
        <?php the_content(); ?>
      </article>
    </div>

    <?php
      endwhile; endif;
      //-------------------------------------
    ?>

    <!----- 関連記事 ----->
    <?php
      // ページのカテゴリ情報取得
      $id = $post->ID;
      $rCat = get_the_terms($id, 'blog-cat');
      $title = $rCat[0]->name;

      if(!empty($title)) {
        $title = $rCat[0]->name.'の関連記事';
      } else {
        $title = '関連記事';
      }
    ?>

    <?php if(current_user_can('read_private_pages')) : ?>
    <hr>
    <div id="single-related">
      <h3><?php echo $title; ?></h3>
      <ul>
        <?php
          // カテゴリ記事5件表示
          $query = array(
            'post_per_page' => 5,
            'post_type'     => 'blog',
            'post__not_in'  => array($id),
            'taxonomy'      => 'blog-cat',
            'term'          => $rCat[0]->slug,
          );

          $rResultsArray = new WP_Query($query);
          $rResults = $rResultsArray->posts;

          foreach($rResults as $item) {
            $id = $item->ID;
            echo '<li><a href="'.get_the_permalink($id).'">'.$item->post_title.'</li>';
          }
        ?>
      </ul>
    </div>
    <hr>
    <?php endif; ?>

  </div>
</section>

<?php
  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
