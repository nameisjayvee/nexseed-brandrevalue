<?php

get_header(); ?>

<div class="mv_area ">
  <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/about-purchase.png" alt="買取方法">
</div>
<div class="cat_cnt">
    <h2 class="cat_tl">「売り方」のスタイルに合わせて選べる、3つの買取方法！</h2>
    <p class="cat_tx">「不用品を売りたいけど、銀座まで行くのは遠くて大変」という方は、お品物を自宅から送って査定を待つ「宅配買取サービス」を。「郵送するのも面倒」という方には「出張買取サービス」を。「高価なブランド物を売るのだから、ちゃんとお店で顔を見て売りたい」という方には「店頭買取」を。あなたにピッタリの「売り方」を、自由に選べるシステムをBRANDREVALUE(ブランドリバリュー)は用意しています。</p>
</div>

<div id="purchase">
    <div class="purchase_inn">
        <h3><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/purchase_takuhai_ttl.png" alt="買取方法"></h3>
        <dl>
            <dt><a href="<?php echo home_url('purchase/takuhai'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/purchase_takuhai_ph.png" alt="宅配買取"></a></dt>
            <dd>「品物を査定してほしいけど、銀座まで行くのは遠くて大変……」「売りたいものがたくさんあって、お店まで持っていけない！」そんな方もどうかご心配なく。
BRAND REVALUEでは、ご自宅から郵送でお品物をお送りいただき、店頭にお越しいただくことなく査定・買取を行う宅配買取サービスを行っています。もちろん配送料は無料。お品物を箱詰めするだけで簡単に梱包できる「宅配キット」をお送りいたしますので、手間をかけずにお品物をお売りいただけます。</dd>
            <dd><a href="<?php echo home_url('purchase/takuhai'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/purchase_takuhai_btn.png" alt="宅配買取"></a></dd>
        </dl>
    </div>

    <div class="purchase_inn">
        <h3><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/purchase_syutchou_ttl.png" alt="家から一歩も出ずにスピード査定を受けられる、楽ちんサービス！"></h3>
        <dl>
            <dt><a href="<?php echo home_url('purchase/syutchou'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/purchase_syutchou_ph.png" alt="出張買取"></a></dt>
            <dd>東京近郊にお住まいで、「品物を店舗まで運んだり、郵送の手続きをする時間がない」という方にお勧めしたいのが、出張買取サービスです。お申込み後「最短即日」でBRAND REVALUEの鑑定スタッフがお客様のご自宅までお伺いし、その場でお品物をスピード査定。金額にご納得いただければその場で現金をお渡しいたします。家から一歩も出ることなく、店舗と同様のスピーディな買取サービスをご利用いただける、「一番楽ちん」なスタイルです。</dd>
            <dd><a href="<?php echo home_url('purchase/syutchou'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/purchase_syutchou_btn.png" alt="宅配買取"></a></dd>
        </dl>
    </div>

    <div class="purchase_inn">
        <h3><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/purchase_tentou_ttl.png" alt="「即日で現金を手に入れたい」「顔を見て売りたい」そんな方にピッタリ！"></h3>
        <dl>
            <dt><a href="<?php echo home_url('purchase/tentou'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/purchase_tentou_ph.png" alt="店頭買取"></a></dt>
            <dd>「大事な品物だから、郵送ではなく自分の手で店舗まで運びたい」――そんな慎重な方には、店頭買取がお勧め。銀座店に常駐するプロの鑑定スタッフがその場でお品物を査定し、金額にご納得いただければその場で現金をお受け取りいただけます。店舗の様子をご覧いただくことができるだけではなく、ご不明な点や気になることがあればその場でスタッフにお問い合わせいただけるので、とにかく安心してご利用いただける買取スタイルです。</dd>
            <dd><a href="<?php echo home_url('purchase/tentou'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/purchase_tentou_btn.png" alt="宅配買取"></a></dd>
        </dl>
    </div>


</div>



<?php

  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
