<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BRING
 */

?>

    </div>
    <!-- #content -->

    <footer>
        <div class="list_wrap ">
            <h3 class="foot_logo "><a href="<?php echo home_url(''); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/foot_logo.png " alt="高額ブランド買取専門サイト BRAND REVALUE " ></a></h3>
            <div class="foot_list_l ">
                <h4><a href="<?php echo home_url(''); ?>">トップ</a></h4>
                <p><a href="<?php echo home_url('service'); ?>">サービスについて</a></p>
                <p><a href="<?php echo home_url('brand'); ?>">取扱ブランド</a></p>
                <p><a href="<?php echo home_url('cat'); ?>">取扱カテゴリ</a></p>
                <p><a href="<?php echo home_url('introduction'); ?>">実績紹介</a></p>

                <h4>買取対象品</h4>
                <p><a href="<?php echo home_url('cat/gold'); ?>">金・プラチナ・貴金属</a></p>
                <p><a href="<?php echo home_url('cat/gem'); ?>">宝石・ジュエリー</a></p>
                <p><a href="<?php echo home_url('cat/watch'); ?>">高級腕時計</a></p>
                <p><a href="<?php echo home_url('cat/bag'); ?>">ブランドバッグ・鞄</a></p>
                <p><a href="<?php echo home_url('cat/outfit'); ?>">洋服・毛皮</a></p>
                <p><a href="<?php echo home_url('cat/wallet'); ?>">ブランド財布</a></p>
                <p><a href="<?php echo home_url('cat/shoes'); ?>">ブランド靴</a></p>
                <p><a href="<?php echo home_url('cat/diamond'); ?>">ダイヤモンド</a></p>
                <p><a href="<?php echo home_url('contact'); ?>">お問い合わせ</a></p>
                <p><a href="<?php echo home_url('purchase3'); ?>">査定キットお申し込み</a></p>
                <p><a href="<?php echo home_url('purchase2'); ?>">写メール査定</a></p>
                <p><a href="<?php echo home_url('purchase1'); ?>">査定お申し込み</a></p>

            </div>

            <div class="foot_list_r ">

                <p><a href="<?php echo home_url('purchase'); ?>">買取方法</a></p>
                <p><a href="<?php echo home_url('about-purchase/takuhai'); ?>">宅配買取</a></p>
                <p><a href="<?php echo home_url('about-purchase/syutchou'); ?>">出張買取</a></p>
                <p><a href="<?php echo home_url('about-purchase/tentou'); ?>">店頭買取</a></p>
                <p><a href="<?php echo home_url('voice'); ?>">お客様の声</a></p>
                <p><a href="<?php echo home_url('line'); ?>">LINE査定案内</a></p>
                <p><a href="<?php echo home_url('privacy'); ?>">プライバシーポリシー</a></p>
                <p><a href="<?php echo home_url('company'); ?>">会社概要</a></p>

            </div>
        </div>
        <p class="copy "><small>Copyrights © 2015 ブランド古着買取のBRAND REVALUE all rights reserved.</small></p>

        <div class="btm_nav">
            <ul>
                <li><a href="tel:0120-970-060"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/foot_navbtn01.png " alt="電話で問い合わせる"></a></li>
                <li><a href="https://line.me/ti/p/%40otv4506b"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/foot_navbtn02.png " alt="LINE"></a></li>
                <li><a href="<?php echo home_url('contact'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/foot_navbtn03.png " alt="お問い合わせ"></a></li>
            </ul>
        </div>


    </footer>
    </div>
    <!-- Page -->

    <div id="pageTop" class="pageTop">
        <a href="#"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/top_btn.png " alt="トップへ戻る "></a>
    </div>

    <?php wp_footer(); ?>

        <!-- // jquery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js "></script>
        <script src="<?php echo get_template_directory_uri() ?>/js/scll.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/js/flickity.pkgd.min.js"></script>
        <script>
            $(function () {
                $(".menu ").css("display ", "none ");
                $(".button-toggle ").on("click ", function () {
                    $(".menu ").slideToggle();
                });
            });

            $('#main-gallery').flickity({
                // options
                cellAlign: 'left',
                contain: true
            });


            // ブランドリスト タブ
            $(".tab li").click(function () {
                var index = $(this).parent("ul").children("li").index(this);
                $(this).siblings("li").removeClass("active");
                $(this).addClass("active");
                $(this).parent("ul").nextAll(".panel").hide();
                $(this).parent("ul").nextAll(".panel").eq(index).show();
            });
        </script>


        <!-- 買取実績タブ　-->
        <script src="<?php echo get_template_directory_uri() ?>/js/jquery.tile.js"></script>
        <script>
            $(document).ready(function () {
                $(".box-4 .title p.itemName").tile(3);
                $(".box-4 .title p.itemdetail").tile(3);
            });
            $(window).load(function () {
                $(".box-4 .title img").tile(3);
                $(".postlist").tile(3);
                $('#lp-cat-jisseki li').tile(2);
            });

            $(function () {
                $(".menu").mouseover(function () {
                    $(".menu").removeClass("hover");
                    $(this).addClass("hover");
                    $(".content:not('.hover + .content')").fadeOut();
                    $(".hover + .content").fadeIn();
                });
            });
            $(function () {
                $(".menu2").mouseover(function () {
                    $(".menu2").removeClass("hover2");
                    $(this).addClass("hover2");
                    $(".content2:not('.hover2 + .content2')").fadeOut();
                    $(".hover2 + .content2").fadeIn();
                });
            });
            $(function () {
                $(".menu3").mouseover(function () {
                    $(".menu3").removeClass("hover3");
                    $(this).addClass("hover3");
                    $(".content3:not('.hover3 + .content3')").fadeOut();
                    $(".hover3 + .content3").fadeIn();
                });
            });
        </script>
        <!-- modal -->
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jquery.leanModal.min.js"></script>
        <script type="text/javascript">
            $(function () {
                $('a[rel*=leanModal]').leanModal({
                    top: 50, // モーダルウィンドウの縦位置を指定
                    overlay: 0.5, // 背面の透明度
                    closeButton: ".modal_close" // 閉じるボタンのCSS classを指定
                });
            });
        </script>

        <!--  jquery // -->


        </body>

        </html>
