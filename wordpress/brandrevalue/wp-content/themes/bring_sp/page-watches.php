<?php
get_header(); ?>

<div class="mv_area ">
<img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kv-introduction.png " alt="<?php echo get_the_title(); ?>">
</div>
<div class="intrItem_cnt ">
<h2><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/jsseki_tl.png " alt="<?php echo get_the_title(); ?>"></h2>
  <ul class="topItem_list ">
	<?php
	$paged = get_query_var('paged') ? get_query_var('paged') : 1;
	$args = array(
        'post_type' => 'introduction',
        'posts_per_page' => 21,
        'category_name' => 'watch',
        'paged' => $paged,
    );
    $loop = new WP_Query($args);
    if(have_posts()) :
    while($loop->have_posts()) : $loop->the_post(); ?>
    <li class="topItem ">
      <p class="item_img "><?php echo wp_get_attachment_image(get_post_meta($post->ID, 'item_image', true),'item_images'); ?></p>
      <p class="item_name "><?php echo get_post_meta($post->ID,'item_name',true); ?></p>
      <p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kingaku.png " alt="買い取り実績金額 "></p>
      <p class="item_price ">￥<?php echo get_post_meta($post->ID,'item_price',true); ?>-</p>
    </li>
    <?php endwhile; endif; ?>
  </ul>
	<?php
    if(function_exists('wp_pagenavi')){
        wp_pagenavi(array('query'=>$loop));
    }
    wp_reset_postdata(); ?>

<?php
  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
