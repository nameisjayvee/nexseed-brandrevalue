<?php
get_header(); ?>

		<div class="area_det">
			<h2>
				<?php the_title(); ?>
			</h2>
							<span class="area_ico"><img data-src="<?php the_field('エリアアイコン'); ?>" alt="<?php the_title(); ?>" style="width:30%; margin:0 35% 30px;" /></span>

			<div class="area_post">
				<?php $areaid01_01 = get_field('エリア①'); ?>
				<?php if(empty($areaid01_01)):?>
				<?php else:?>
				<h3> <?php echo the_field('エリア①'); ?></h3>
				<?php endif;?>
				</h3>
				<?php $areaid01_02 = get_field('エリア①詳細'); ?>
				<?php if(empty($areaid01_02)):?>
				<?php else:?>
				<p><?php echo the_field('エリア①詳細'); ?>
				<p>
				<?php endif;?>
				<?php $areaid02_01 = get_field('エリア②'); ?>
				<?php if(empty($areaid02_01)):?>
				<?php else:?>
				<h3> <?php echo the_field('エリア②'); ?></h3>
				<?php endif;?>
				</h3>
				<?php $areaid02_02 = get_field('エリア②詳細'); ?>
				<?php if(empty($areaid02_02)):?>
				<?php else:?>
				<p><?php echo the_field('エリア②詳細'); ?>
				<p>
				<?php endif;?>

				<?php $areaid03_01 = get_field('エリア③'); ?>
				<?php if(empty($areaid03_01)):?>
				<?php else:?>
				<h3> <?php echo the_field('エリア③'); ?></h3>
				<?php endif;?>
				</h3>
				<?php $areaid03_02 = get_field('エリア③詳細'); ?>
				<?php if(empty($areaid03_02)):?>
				<?php else:?>
				<p><?php echo the_field('エリア③詳細'); ?>
				<p>
				<?php endif;?>
				<?php $areaid04_01 = get_field('エリア④'); ?>
				<?php if(empty($areaid04_01)):?>
				<?php else:?>
				<h3> <?php echo the_field('エリア④'); ?></h3>
				<?php endif;?>
				</h3>
				<?php $areaid04_02 = get_field('エリア④詳細'); ?>
				<?php if(empty($areaid04_02)):?>
				<?php else:?>
				<p><?php echo the_field('エリア④詳細'); ?>
				<p>
				<?php endif;?>
			</div>
		</div>




		<div class="area_mv">
		<h2>買取エリア</h2>
		<h3>日本全国どこでも高額買取いたします。<br />
お気軽にお問合せください。</h3>
		<p>BRAND REVARUEは日本全国からの買取お申込みに対応しております。<br />
※往復送料・梱包キット・キャンセル料は、全て無料です。（お客様費用負担0円）<br />
※ご不明な点はお気軽に当社オペレーターまでお問い合わせくださいませ。</p>

		</div>



<div class="mv_area ">
<img data-src="<?php echo get_s3_template_directory_uri() ?>/img/takuhai_mv.png" alt="宅配買取全て無料で対応致します。">
</div>
	  <ul class="takuhai_topbnr">
	  <li><a href="tel:0120970060"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/takuhai_top_bnr01.png" alt="宅配買取全て無料で対応致します。"></a></li>
	  <li><a href="https://kaitorisatei.info/brandrevalue/purchase3"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/takuhai_top_bnr02.png" alt="無料宅配キット申し込み"></a></li>

	  </ul>

	  	  <section class="kaitori_voice">
<h3>お客様の声</h3>
<ul>
<li>
<p class="kaitori_tab takuhai_tab">宅配買取</p>
<h4><?php the_field('お客様の声①名前'); ?></h4>
<p class="voice_txt"><?php the_field('お客様の声①詳細'); ?></p>
</li>

<li>
<p class="kaitori_tab takuhai_tab">宅配買取</p>
<h4><?php the_field('お客様の声②名前'); ?></h4>
<p class="voice_txt"><?php the_field('お客様の声②詳細'); ?></p>
</li>
<li>
<p class="kaitori_tab takuhai_tab">宅配買取</p>
<h4><?php the_field('お客様の声③名前'); ?></h4>
<p class="voice_txt"><?php the_field('お客様の声③詳細'); ?></p>
</li>


</ul>
</section>



<?php
  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');
  ?>

  <?php

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
