<?php
get_header(); ?>
		<div class="area_mv">
		<h2>買取エリア</h2>
		<h3>日本全国どこでも高額買取いたします。<br />
お気軽にお問合せください。</h3>
		<p>BRAND REVARUEは日本全国からの買取お申込みに対応しております。<br />
※往復送料・梱包キット・キャンセル料は、全て無料です。（お客様費用負担0円）<br />
※ご不明な点はお気軽に当社オペレーターまでお問い合わせくださいませ。</p>

		</div>
		<section class="area_box">
		<h2>買取エリアを選ぶ</h2>
		<h3>北海道・東北エリア</h3>
		<ul class="area_list">
		<li> <a href="<?php echo home_url('/area/hokkaido'); ?>">北海道</a></li>
		<li><a href="<?php echo home_url('/area/aomori'); ?>">青森県</a></li>
		<li><a href="<?php echo home_url('/area/iwate'); ?>">岩手県</a></li>
		<li><a href="<?php echo home_url('/area/miyagi'); ?>">宮城県</a></li>
		<li><a href="<?php echo home_url('/area/akita'); ?>">秋田県</a></li>
		<li><a href="<?php echo home_url('/area/yamagata'); ?>">山形県</a></li>
		<li><a href="<?php echo home_url('/area/fukushima'); ?>">福島県</a></li>
		</ul>
		<h3>関東エリア</h3>
		<ul class="area_list">
		<li><a href="<?php echo home_url('/area/tokyo'); ?>">東京都</a></li>
		<li><a href="<?php echo home_url('/area/kanagawa'); ?>">神奈川県</a></li>
		<li><a href="<?php echo home_url('/area/chiba'); ?>">千葉県</a></li>
		<li><a href="<?php echo home_url('/area/saitama'); ?>">埼玉県</a></li>
		<li><a href="<?php echo home_url('/area/ibaraki'); ?>">茨城県</a></li>
		<li><a href="<?php echo home_url('/area/tochigi'); ?>">栃木県</a></li>
		<li><a href="<?php echo home_url('/area/gunma'); ?>">群馬県</a></li>
		</ul>
		<h3>北陸・甲信越エリア</h3>
		<ul class="area_list">
		<li><a href="<?php echo home_url('/area/niigata'); ?>">新潟県</a></li>
		<li><a href="<?php echo home_url('/area/toyama'); ?>">富山県</a></li>
		<li><a href="<?php echo home_url('/area/ishikawa'); ?>">石川県</a></li>
		<li><a href="<?php echo home_url('/area/fukui'); ?>">福井県</a></li>
		<li><a href="<?php echo home_url('/area/yamanashi'); ?>">山梨県</a></li>
		<li><a href="<?php echo home_url('/area/nagano'); ?>">長野県</a></li>
		</ul>
		<h3>東海エリア</h3>
		<ul class="area_list">
		<li><a href="<?php echo home_url('/area/gifu'); ?>">岐阜県</a></li>
		<li><a href="<?php echo home_url('/area/shizuoka'); ?>">静岡県</a></li>
		<li><a href="<?php echo home_url('/area/aichi'); ?>">愛知県</a></li>
		<li><a href="<?php echo home_url('/area/mie'); ?>">三重県</a></li>

		</ul>
		<h3>関西エリア</h3>
		<ul class="area_list">
		<li><a href="<?php echo home_url('/area/osaka'); ?>">大阪府</a></li>
		<li><a href="<?php echo home_url('/area/kyoto'); ?>">京都府</a></li>
		<li><a href="<?php echo home_url('/area/shiga'); ?>">滋賀県</a></li>
		<li><a href="<?php echo home_url('/area/hyogo'); ?>">兵庫県</a></li>
		<li><a href="<?php echo home_url('/area/nara'); ?>">奈良県</a></li>
		<li><a href="<?php echo home_url('/area/wakayama'); ?>">和歌山県</a></li>
		</ul>
		<h3>中国エリア</h3>
		<ul class="area_list">
		<li><a href="<?php echo home_url('/area/tottori'); ?>">鳥取県</a></li>
		<li><a href="<?php echo home_url('/area/shimane'); ?>">島根県</a></li>
		<li><a href="<?php echo home_url('/area/okayama'); ?>">岡山県</a></li>
		<li><a href="<?php echo home_url('/area/hiroshima'); ?>">広島県</a></li>
		<li><a href="<?php echo home_url('/area/yamaguchi'); ?>">山口県</a></li>
		</ul>
		<h3>四国エリア</h3>
		<ul class="area_list">
		<li><a href="<?php echo home_url('/area/tokushima'); ?>">徳島県</a></li>
		<li><a href="<?php echo home_url('/area/kagawa'); ?>">香川県</a></li>
		<li><a href="<?php echo home_url('/area/ehime'); ?>">愛媛県</a></li>
		<li><a href="<?php echo home_url('/area/kochi'); ?>">高知県</a></li>
		</ul>
		<h3>九州・沖縄エリア</h3>
		<ul class="area_list">
		<li><a href="<?php echo home_url('/area/fukuoka'); ?>">福岡県</a></li>
		<li><a href="<?php echo home_url('/area/saga'); ?>">佐賀県</a></li>
		<li><a href="<?php echo home_url('/area/nagasaki'); ?>">長崎県</a></li>
		<li><a href="<?php echo home_url('/area/kumamoto'); ?>">熊本県</a></li>
		<li><a href="<?php echo home_url('/area/oita'); ?>">大分県</a></li>
		<li><a href="<?php echo home_url('/area/miyazaki'); ?>">宮崎県</a></li>
		<li><a href="<?php echo home_url('/area/kagoshima'); ?>">鹿児島県</a></li>
		<li><a href="<?php echo home_url('/area/okinawa'); ?>">沖縄県</a></li>

		</ul>

		</section>



<div class="mv_area ">
<img data-src="<?php echo get_s3_template_directory_uri() ?>/img/takuhai_mv.png" alt="宅配買取全て無料で対応致します。">
</div>
	  <ul class="takuhai_topbnr">
	  <li><a href="tel:0120970060"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/takuhai_top_bnr01.png" alt="宅配買取全て無料で対応致します。"></a></li>
	  <li><a href="<?php echo home_url('purchase3') ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/takuhai_top_bnr02.png" alt="無料宅配キット申し込み"></a></li>

	  </ul>



<?php
  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');
  ?>
  <section class="kaitori_voice">
<h3>お客様の声</h3>
<ul>
<li>
<p class="kaitori_tab takuhai_tab">宅配買取</p>
<h4>東京都杉並区 | 40代 | 女性</h4>
<p class="voice_txt">クローゼットの整理をしていると、もう使わなくなったエルメスのバーキン30が出てきたので、ブランドリバリューの宅配買取をお願いしました。宅配キットを無料で準備して頂けて、その箱にアイテムを詰めて送るだけなので、杉並区から重たい荷物を店頭に持ち込むことなく、とても手軽に買取査定を利用することができました。査定額も期待以上に高額で、すぐに買取をお願いしたほどです。対応も早く、とても信頼できる買取店だと思います</p>
</li>

<li>
<p class="kaitori_tab takuhai_tab">宅配買取</p>
<h4>神奈川県鎌倉市 | 30代 | 男性</h4>
<p class="voice_txt">このたび、ブランドリバリューの宅配買取で、長い間愛用してきたROLEXのデイトナ 116520　ランダム品番 黒文字盤を納得の買取額で売却することができました。結婚資金の一部に充てたかったので、誰にも知られず、また鎌倉にある買取店よりも高額で買取してもらえて本当に感謝しています。ロレックス買取を強化しているブランドリバリューだからこその買取査定だと満足しています。今後は宅配買取だけなく、機会があれば店頭も利用してみたいと思います。		</p>
</li>
<li>
<p class="kaitori_tab takuhai_tab">宅配買取</p>
<h4>兵庫県姫路市 | 20代 | 女性</h4>
<p class="voice_txt">お店のお客様から頂いたカルティエのマイヨンパンテール　フルダイヤリングを、宅配買取でブランドリバリューに買取してもらいました。お客様も姫路市内の方なので地元の買取店は利用したくなくて、遠方の買取店をネットで探していたところ、ブランドリバリューを見つけました。高額なブランドジュエリーの買取実績も多く安心して査定をしてもらえそうだったので、利用してみることにしました。利用してみると、自宅にながら査定から買取、振込をしてもらえるので、店頭に行くよりも便利だと思いました。		</p>
</li>
</ul>
</section>
  <?php

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
