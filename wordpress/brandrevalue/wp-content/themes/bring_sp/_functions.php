<?php
/**
 * BRING functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package BRING
 */

if ( ! function_exists( 'bring_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function bring_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on BRING, use a find and replace
	 * to change 'bring' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'bring', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'bring' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'bring_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // bring_setup
add_action( 'after_setup_theme', 'bring_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function bring_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'bring_content_width', 640 );
}
add_action( 'after_setup_theme', 'bring_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function bring_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'bring' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'bring_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function bring_scripts() {
  /*----- CSS -----*/
	wp_enqueue_style( 'bring-style-info', get_stylesheet_uri() );
	//wp_enqueue_style( 'bootstrap-honoka', get_template_directory_uri().'/css/bootstrap.min.css');
	//wp_enqueue_style( 'bring-style',      get_template_directory_uri().'/css/style.css');
	
	/*----- JavaScript -----*/
	// deregister default jQuery included with Wordpress
  wp_deregister_script( 'jquery' );
  $jquery_cdn = '//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js';
  wp_enqueue_script( 'jquery', $jquery_cdn, array(), '20130115', true );
	wp_enqueue_script( 'bootstrap-honoka', get_template_directory_uri().  '/js/bootstrap.min.js', array(), 'v3.3.6', true);
	wp_enqueue_script( 'bring-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'bring-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'bring_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';



/**
 * ====================================================
 * Help Contact Form 7 Play Nice With Bootstrap
 * ==================================================== 
 * Add a Bootstrap-friendly class to the "Contact Form 7" form
 */
add_filter( 'wpcf7_form_class_attr', 'wildli_custom_form_class_attr' );
function wildli_custom_form_class_attr( $class ) {
	$class .= ' form-horizontal';
	return $class;
}



// カスタム投稿タイプ
function custom_post_type_blog() {
  $labels = array(
    'name' => _x('ブログ記事', 'blogposts'),
    'singular_name' => _x('ブログ記事', 'blogpost'),
    'add_new' => _x('新しいブログ記事を追加', 'blog'),
    'add_new_item' => __('新しいブログ記事を追加'),
    'edit_item' => __('ブログ記事を編集'),
    'new_item' => __('新しいブログ記事'),
    'view_item' => __('ブログ記事を編集'),
    'search_items' => __('ブログ記事を探す'),
    'not_found' => __('ブログ記事はありません'),
    'not_found_in_trash' => __('ゴミ箱にブログ記事はありません'), 
    'parent_item_colon' => ''
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'has_archive' => true,
    'supports' => array('title','editor', 'thumbnail')
  ); 
  register_post_type('blog',$args);
  
  // カスタムタクソノミー カテゴリ
  register_taxonomy (
    'blog-cat', 
    'blog',
    array(
      'hierarchical' => true, 
      'update_count_callback' => '_update_post_term_count',
      'label' => 'カテゴリー',
      'singular_label' => 'カテゴリー',
      'public' => true,
      'show_ui' => true
    )
  );
}

add_action('init', 'custom_post_type_blog');




/*==================================================
  LP用テンプレートショートコード
==================================================*/
function func_smart_customfield($attr) {
  extract(shortcode_atts(array('id' => ''), $attr));
  if(!preg_match("/SP_$/", $id)) {
    $attr_sp = $id.'SP';
  } else {
    $attr_sp = $id;
  }
  $text = nl2br(SCF::get($attr_sp));
  echo $text;
}
function func_smart_customfield_image($attr) {
  extract(shortcode_atts(array('id' => ''), $attr));
  if(!preg_match("/SP_$/", $id)) {
    $attr_sp = $id.'SP';
  } else {
    $attr_sp = $id;
  }
  $text = wp_get_attachment_image($attr_sp, 'full');
  return $text;
}
function func_smart_customfield_imageurl($attr) {
  extract(shortcode_atts(array('id' => ''), $attr));
  if(!preg_match("/SP_$/", $id)) {
    $attr_sp = $id.'SP';
  } else {
    $attr_sp = $id;
  }
  $text = wp_get_attachment_image_url($attr_sp, 'full');
  return $text;
}
add_shortcode('scf', 'func_smart_customfield');
add_shortcode('scf_img', 'func_smart_customfield_image');
add_shortcode('scf_imgurl', 'func_smart_customfield_imageurl');


function func_themplateurl() {
  echo get_stylesheet_directory_uri();
}
function func_homeurl() {
  echo home_url();
}
add_shortcode('themeurl', 'func_themplateurl');
add_shortcode('homeurl', 'func_homeurl');

add_action( 'wp_head' , 'jquery_pass');
  function jquery_pass() {
    echo '<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>';
  };

add_action( 'wp_head' , 'numbering_js');

  function numbering_js() {
    $js_src = get_template_directory_uri() . '/js/numbering.js';
    $output = '<script src="' . $js_src . '"></script>';
    echo $output;
  };

add_action( 'wp_footer' , 'tracking_code');

  function tracking_code() {
    $tracking_js_src = get_template_directory_uri() . '/js/tracking_code.js';
    $output = '<script src="' . $tracking_js_src . '"></script>';
    echo $output;
  };