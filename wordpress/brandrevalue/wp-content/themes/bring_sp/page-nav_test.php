<?php
  
get_header('testhead'); ?>

<div class="mv_area "> <img src="<?php echo get_s3_template_directory_uri() ?>/images/kv-shibuya.png " alt="店舗案内"> </div>
<div class="kaitori_cnt">
	<h2 class="kaitori_tl"><br />
		渋谷で時計・バッグ・ジュエリー・貴金属の買取店をお探しの皆様へ。<br />
ブランド買取なら高額査定のBRAND REVALUEへ。</h2>
	<p class="cnt_tx01">最寄り駅は、渋谷駅。渋谷駅から徒歩3分。<br />
BRANDREVALUE(ブランドリバリュー)渋谷店は、渋谷のシンボルの一つであるタワーレコードを背にの右斜め前に位置する和光ビル4階に店舗を構えております。<br>
	</p>
</div>
<div id="company">
	<table>
				<tr>
					<th>住所</th>
					<td class="shop_map">
						東京都渋谷区神南1-12-16 和光ビル4階<a class="map_btn" href="https://goo.gl/maps/2i5igG64gFp" target="_blank">Google MAP</a></td>
				</tr>
				<tr>
					<th>営業時間</th>
					<td>11：00～21：00</td>
				</tr>
				<tr>
					<th>電話番号</th>
					<td><a href="tel:0120-970-060">0120-970-060</a></td>
				</tr>
				<tr>
					<th>駐車場</th>
					<td> - </td>
				</tr>
	</table>
</div>
<div class="shop_page">
			<h3 class="mb30">渋谷店までのアクセス</h3>
			<p><span class="tx_bold">最寄り駅は、JR・地下鉄・私鉄各線が通る「渋谷駅」。</span><br />
				BRANDREVALUE(ブランドリバリュー)渋谷店は、渋谷口のハチ公口を出て、東京のシンボルの一つであるタワーレコードのほぼ正面に位置する和光ビル4階に店舗を構えております。 渋谷駅から徒歩3分と、非常にアクセスしやすい場所にあり、原宿・表参道にお越しの際にも便利にご利用頂けます。</p>
<ul class="map_root">
				<li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/new_root01.png" alt="渋谷駅スクランブル交差点" />
					<div class="root_tx_bx">
						<p class="root_ttl"><span>1</span>渋谷駅スクランブル交差点</p>
						<p>ハチ公前からTSUTAYA前までスクランブル交差点を渡ります。</p>
					</div>
				</li>
				<li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/new_root02.png" alt="A5出口" />
					<div class="root_tx_bx">
						<p class="root_ttl"><span>2</span>TSUTAYA前</p>
						<p>TSUTAYAを左に見ながら、タワーレコード方面に直進します。</p>
					</div>
				</li>
				<li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/new_root03.png" alt="地上" />
					<div class="root_tx_bx">
						<p class="root_ttl"><span>3</span>渋谷MODI前</p>
						<p>そのままタワーレコード方面に直進します。	</p>
					</div>
				</li>
				<li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/new_root04.png" alt="四谷学院ビル前" />
					<div class="root_tx_bx">
						<p class="root_ttl"><span>4</span>郵便局・タワーレコード前</p>
						<p>左手に郵便局（右手にタワーレコード）が現れたら、目の前の横断歩道を直進します。		</p>
					</div>
				</li>
				<li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/new_root05.png" alt="エレベーター前" />
					<div class="root_tx_bx">
						<p class="root_ttl"><span>5</span>和光ビル前</p>
						<p>横断歩道を渡ってから30M程度（横断歩道沿いの建物から4件程隣）直進すると、左手に和光ビルが見えます。</p>
					</div>
				</li>
				<li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/new_root06.png" alt="店内" />
					<div class="root_tx_bx">
						<p class="root_ttl"><span>6</span>ビル内</p>
						<p>エレベーターにて建物の4階へ上がり、出て左手が当店の入り口でございます。</p>
					</div>
				</li>
			</ul>
	</section>
		<section>
			<h3 class="mb30">渋谷店　店内紹介・内装へのこだわり</h3>
			<div class="shop_comm"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/kodawari_new.png" alt="店内" />
				<p>ブランドリバリュー渋谷店では、お客さまが快適に過ごして頂ける空間と最上級のサービスでおもてなし致します。 </p>
			</div>
			<div class="shop_comm"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/kodawari_new02.png" alt="店内" />
				<p>混雑状況によっては店内で少々お待たせしてしまう場合もあるかと思いますが少しでも落ち着けるよう、ソファー席をご用意しております。</p>
			</div>
			<div class="shop_comm"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/kodawari03.png" alt="店内" />
				<p>また、査定はお客さまのプライバシーをお守りするために完全個室の査定ブースを設けさせて頂いておりますので、お持ち込み頂いたお品物についてのご質問やご相談・要望などありましたら遠慮なくバイヤーへお申し付け下さい。 </p>
			</div>
		</section>
	<section>
		<h3>渋谷店　鑑定士のご紹介</h3>
		<div class="staff_bx"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/staff01.png" alt="査定士 森" />
			<p class="staff_name">森 雄大 鑑定士</p>
			<h4>仕事における得意分野</h4>
			<p>私の得意分野は、オールジャンルです。特に、買取店によって査定金額に差が出てしまう、エルメスやシャネル、ロレックス、宝石に関しては、頻繁に業者間オークションに参加し、知識向上に努めております。</p>
			<h4>自身の強み</h4>
			<p>私の強みは、高額査定にあります。長年、古物業界に携わることにより、独自の販売ルート　を構築して参りました。その為、査定金額には、自信があります。業界最高峰の査定金額でお客様にご満足いただけることをお約束いたします。 </p>
			<h4>仕事にかける思いと心がけ</h4>
			<p>お客様に「ありがとう」と笑顔で言っていただけることが、私の喜びです。お客様にご満足いただけるよう、高品質のサービスをご提供できるよう店舗運営・スタッフ教育に努めてまいります。</p>
		</div>

<div class="staff_bx"> <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2017/07/staff04.png" alt="査定士 高田" />
			<p class="staff_name">高田 啓介 鑑定士</p>
			<h4>仕事における得意分野</h4>
			<p>バッグと時計。バッグと時計は使用する方によって様々な顔を見せるため、
非常に面白味があり、得意分野としております。</p>
			<h4>自身の強み</h4>
			<p>前職が金融機関に勤めていた経験から多様な人脈があり、高値での買取を可能と
しております。</p>
			<h4>仕事にかける思いと心がけ</h4>
			<p>真摯に取り組む。その一点です。</p>
		</div>
	</section>
	<section>
				<h3>
			渋谷店の買取実績
			</h3>
<div class="topItem_cnt ">
	<div class="full_content">
		<div class="menu hover">ブランド<br>
			買取</div>
		<div class="content">
			<ul id="box-jisseki" class="list-unstyled clearfix" style="overflow:hidden;">
				<!-- barand1 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/001.jpg" alt=""></p>
						<p class="itemName">オーデマピゲ　ロイヤルオークオフショアクロノ 26470OR.OO.A002CR.01 ゴールド K18PG</p>
						<hr>
						<p> <span class="red">A社</span>：3,120,000円<br>
							<span class="blue">B社</span>：3,100,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">3,150,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>50,000円</p>
					</div>
				</li>
				
				<!-- brand2 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/002.jpg" alt=""></p>
						<p class="itemName">パテックフィリップ　コンプリケーテッド ウォッチ 5085/1A-001</p>
						<hr>
						<p> <span class="red">A社</span>：1,400,000円<br>
							<span class="blue">B社</span>：1,390,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">1,420,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>30,000円</p>
					</div>
				</li>
				
				<!-- brand3 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/003.jpg" alt=""></p>
						<p class="itemName">パテックフィリップ　コンプリケーション 5130G-001 WG</p>
						<hr>
						<p> <span class="red">A社</span>：2,970,000円<br>
							<span class="blue">B社</span>：2,950,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">3,000,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>50,000円</p>
					</div>
				</li>
				
				<!--<li class="box-4">
                <div class="title">
                  <p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/item004.png" alt=""></p>
                  <p class="itemName">マトラッセ 長財布</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：97,000円<br>
                        <span class="blue">B社</span>：95,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">100,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>5,000円</p>
                </div>
              </li>--> 
				
				<!-- brand4 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/004.jpg" alt=""></p>
						<p class="itemName">パテックフィリップ　ワールドタイム 5130R-001</p>
						<hr>
						<p> <span class="red">A社</span>：3,000,000円 <br>
							<span class="blue">B社</span>：2,980,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">3,050,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>70,000円</p>
					</div>
				</li>
				
				<!-- brand5 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/005.jpg" alt=""></p>
						<p class="itemName">シャネル　ラムスキン　マトラッセ　二つ折り長財布</p>
						<hr>
						<p> <span class="red">A社</span>：69,000円<br>
							<span class="blue">B社</span>：68,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">70,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>2,000円</p>
					</div>
				</li>
				
				<!-- brand6 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/006.jpg" alt=""></p>
						<p class="itemName">エルメス　ベアンスフレ　ブラック</p>
						<hr>
						<p> <span class="red">A社</span>：200,000円<br>
							<span class="blue">B社</span>：196,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">211,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>15,000円</p>
					</div>
				</li>
				
				<!-- brand7 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/007.jpg" alt=""></p>
						<p class="itemName">エルメス　バーキン30　トリヨンクレマンス　マラカイト　SV金具</p>
						<hr>
						<p> <span class="red">A社</span>：1,220,000円<br>
							<span class="blue">B社</span>：1,200,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">1,240,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>40,000円</p>
					</div>
				</li>
				
				<!-- brand8 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/008.jpg" alt=""></p>
						<p class="itemName">セリーヌ　ラゲージマイクロショッパー</p>
						<hr>
						<p> <span class="red">A社</span>：150,000円<br>
							<span class="blue">B社</span>：147,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">155,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>8,000円</p>
					</div>
				</li>
				
				<!-- brand9 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/009.jpg" alt=""></p>
						<p class="itemName">ルイヴィトン　裏地ダミエ柄マッキントッシュジャケット</p>
						<hr>
						<p> <span class="red">A社</span>：31,000円<br>
							<span class="blue">B社</span>：30,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">32,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>2,000円</p>
					</div>
				</li>
			</ul>
		</div>
		<div class="menu hover">金<br>
			買取</div>
		<div class="content">
			<ul id="box-jisseki" class="list-unstyled clearfix">
				<!-- gold1 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/001.jpg" alt=""></p>
						<p class="itemName">K18　ダイヤ0.11ctリング</p>
						<hr>
						<p> <span class="red">A社</span>：23,700円<br>
							<span class="blue">B社</span>：23,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price"><!-- span class="small">120g</span -->25,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>2,000円</p>
					</div>
				</li>
				
				<!-- gold2 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/002.jpg" alt=""></p>
						<p class="itemName">K18　メレダイヤリング</p>
						<hr>
						<p> <span class="red">A社</span>：38,000円<br>
							<span class="blue">B社</span>：37,500円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">39,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>1,500円</p>
					</div>
				</li>
				
				<!-- gold3 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/003.jpg" alt=""></p>
						<p class="itemName">K18/Pt900　メレダイアリング</p>
						<hr>
						<p> <span class="red">A社</span>：14,200円<br>
							<span class="blue">B社</span>：14,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">16,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>2,000円</p>
					</div>
				</li>
				
				<!-- gold4 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/004.jpg" alt=""></p>
						<p class="itemName">Pt900　メレダイヤリング</p>
						<hr>
						<p> <span class="red">A社</span>：22,600円<br>
							<span class="blue">B社</span>：21,200円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">23,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>1,800円</p>
					</div>
				</li>
				
				<!-- gold5 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/005.jpg" alt=""></p>
						<p class="itemName">K18WG　テニスブレスレット</p>
						<hr>
						<p> <span class="red">A社</span>：24,600円<br>
							<span class="blue">B社</span>：23,800円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">26,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>2,200円</p>
					</div>
				</li>
				
				<!-- gold6 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/006.jpg" alt=""></p>
						<p class="itemName">K18/K18WG　メレダイヤリング</p>
						<hr>
						<p> <span class="red">A社</span>：22,400円<br>
							<span class="blue">B社</span>：22,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">23,600<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>1,600円</p>
					</div>
				</li>
				
				<!-- gold7 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/007.jpg" alt=""></p>
						<p class="itemName">K18ブレスレット</p>
						<hr>
						<p> <span class="red">A社</span>：18,100円<br>
							<span class="blue">B社</span>：17,960円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">21,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>2,140円</p>
					</div>
				</li>
				
				<!-- gold8 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/008.jpg" alt=""></p>
						<p class="itemName">14WGブレスレット</p>
						<hr>
						<p> <span class="red">A社</span>：23,600円<br>
							<span class="blue">B社</span>：22,800円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">24,300<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>1,500円</p>
					</div>
				</li>
				
				<!-- gold9 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/009.jpg" alt=""></p>
						<p class="itemName">Pt850ブレスレット</p>
						<hr>
						<p> <span class="red">A社</span>：23,600円<br>
							<span class="blue">B社</span>：23,200円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">24,700<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>1,500円</p>
					</div>
				</li>
			</ul>
		</div>
		<div class="menu hover">宝石<br>
			買取</div>
		<div class="content">
			<ul id="box-jisseki" class="list-unstyled clearfix">
				<!--<li class="box-4">
                    <div class="title">
                      <p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem001.jpg" alt=""></p>
                      <p class="itemName">サファイア</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：145,500円<br>
                        <span class="blue">B社</span>：142,500円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">地金+<br>150,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>7,500円</p>
                    </div>
                  </li>

                  <li class="box-4">
                    <div class="title">
                      <p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem002.jpg" alt=""></p>
                      <p class="itemName">エメラルド</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：190,000円<br>
                        <span class="blue">B社</span>：194,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">地金+<br>200,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>10,000円</p>
                    </div>
                  </li>

                  <li class="box-4">
                    <div class="title">
                      <p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem003.jpg" alt=""></p>
                      <p class="itemName">ルビー</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：97,000円<br>
                        <span class="blue">B社</span>：95,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">地金+<br>100,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>5,000円</p>
                    </div>
                  </li>--> 
				
				<!-- gem1 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/001.jpg" alt=""></p>
						<p class="itemName">ダイヤルース</p>
						<p class="itemdetail">カラット：1.003ct<br>
							カラー：E<br>
							クラリティ：VS-2<br>
							カット：Good<br>
							蛍光性：FAINT<br>
							形状：ラウンドブリリアント</p>
						<hr>
						<p> <span class="red">A社</span>：215,000円<br>
							<span class="blue">B社</span>：210,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">221,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>11,000円</p>
					</div>
				</li>
				
				<!-- gem2 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/002.jpg" alt=""></p>
						<p class="itemName">Pt900 DR0.417ctリング</p>
						<p class="itemdetail">カラー：F<br>
							クラリティ：SI-1<br>
							カット：VERY GOOD <br>
							蛍光性：FAINT<br>
							形状：ラウンドブリリアント<br>
							メレダイヤモンド0.7ct<br>
							地金：18金イエローゴールド　5ｇ</p>
						<hr>
						<p> <span class="red">A社</span>：42,000円<br>
							<span class="blue">B社</span>：41,300円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">44,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>2,700円</p>
					</div>
				</li>
				
				<!-- gem3 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/003.jpg" alt=""></p>
						<p class="itemName">Pt900　DR0.25ctリング</p>
						<p class="itemdetail">カラー：H<br>
							クラリティ:VS-1<br>
							カット：Good<br>
							蛍光性：MB<br>
							形状：ラウンドブリリアント<br>
						</p>
						<hr>
						<p> <span class="red">A社</span>：67,400円<br>
							<span class="blue">B社</span>：67,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">68,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>1,000円</p>
					</div>
				</li>
				
				<!-- gem4 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/004.jpg" alt=""></p>
						<p class="itemName">K18　DR0.43ct　MD0.4ctネックレストップ</p>
						<p class="itemdetail">カラー：I<br>
							クラリティ：VS-2<br>
							カット：Good<br>
							蛍光性：WB<br>
							形状：ラウンドブリリアント</p>
						<hr>
						<p> <span class="red">A社</span>：50,000円<br>
							<span class="blue">B社</span>：49,500円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">51,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>500円</p>
					</div>
				</li>
				
				<!-- gem5 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/005.jpg" alt=""></p>
						<p class="itemName">ダイヤルース</p>
						<p class="itemdetail">カラット：0.787ct<br>
							カラー：E<br>
							クラアリティ：VVS-2<br>
							カット：Good<br>
							蛍光性：FAINT<br>
							形状：ラウンドブリリアント</p>
						<hr>
						<p> <span class="red">A社</span>：250,000円<br>
							<span class="blue">B社</span>：248,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">257,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>9,000円</p>
					</div>
				</li>
				
				<!-- gem6 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/006.jpg" alt=""></p>
						<p class="itemName">Pt950　MD0.326ct　0.203ct　0.150ctネックレス</p>
						<p class="itemdetail">カラー：F<br>
							クラリティ：SI-2<br>
							カット：Good<br>
							蛍光性：FAINT<br>
							形状：ラウンドブリリアント</p>
						<hr>
						<p> <span class="red">A社</span>：55,000円<br>
							<span class="blue">B社</span>：54,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">57,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>3,000円</p>
					</div>
				</li>
			</ul>
		</div>
		<div class="menu hover">時計<br>
			買取</div>
		<div class="content">
			<ul id="box-jisseki" class="list-unstyled clearfix">
				
				<!-- watch1 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/001.jpg" alt=""></p>
						<p class="itemName">パテックフィリップ <br>
							カラトラバ 3923</p>
						<hr>
						<p> <span class="red">A社</span>：646,000円 <br>
							<span class="blue">B社</span>：640,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">650,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>10,000円</p>
					</div>
				</li>
				
				<!-- watch2 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/002.jpg" alt=""></p>
						<p class="itemName">パテックフィリップ <br>
							アクアノート 5065-1A</p>
						<hr>
						<p> <span class="red">A社</span>：1,367,000円 <br>
							<span class="blue">B社</span>：1,350,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">1,400,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>50,000円</p>
					</div>
				</li>
				
				<!-- watch3 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/003.jpg" alt=""></p>
						<p class="itemName">オーデマピゲ <br>
							ロイヤルオーク・オフショア 26170ST</p>
						<hr>
						<p> <span class="red">A社</span>：1,200,000円 <br>
							<span class="blue">B社</span>：1,195,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">1,210,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>15,000円</p>
					</div>
				</li>
				
				<!-- watch4 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/004.jpg" alt=""></p>
						<p class="itemName">ROLEX <br>
							サブマリーナ 116610LN ランダム品番</p>
						<hr>
						<p> <span class="red">A社</span>：720,000円 <br>
							<span class="blue">B社</span>：700,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">740,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>40,000円</p>
					</div>
				</li>
				
				<!-- watch5 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/005.jpg" alt=""></p>
						<p class="itemName">ROLEX <br>
							116505 ランダム品番 コスモグラフ</p>
						<hr>
						<p> <span class="red">A社</span>：1,960,000円 <br>
							<span class="blue">B社</span>：1,950,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">2,000,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>50,000円</p>
					</div>
				</li>
				
				<!-- watch6 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/006.jpg" alt=""></p>
						<p class="itemName">ブライトリング <br>
							クロノマット44</p>
						<hr>
						<p> <span class="red">A社</span>：334,000円 <br>
							<span class="blue">B社</span>：328,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">340,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>12,000円</p>
					</div>
				</li>
				
				<!-- watch7 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/007.jpg" alt=""></p>
						<p class="itemName">パネライ <br>
							ラジオミール 1940</p>
						<hr>
						<p> <span class="red">A社</span>：594,000円 <br>
							<span class="blue">B社</span>：590,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">600,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>10,000円</p>
					</div>
				</li>
				
				<!-- watch8 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/008.jpg" alt=""></p>
						<p class="itemName">ボールウォッチ <br>
							ストークマン ストームチェイサープロ CM3090C</p>
						<hr>
						<p> <span class="red">A社</span>：128,000円 <br>
							<span class="blue">B社</span>：125,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">130,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>5,000円</p>
					</div>
				</li>
				
				<!-- watch9 -->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/009.jpg" alt=""></p>
						<p class="itemName">ブレゲ <br>
							クラシックツインバレル 5907BB12984</p>
						<hr>
						<p> <span class="red">A社</span>：615,000円 <br>
							<span class="blue">B社</span>：610,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">626,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>16,000円</p>
					</div>
				</li>
			</ul>
		</div>
		
		<!-- バック-->
		
		<div class="menu hover">バッグ<br>
			買取</div>
		<div class="content">
			<ul id="box-jisseki" class="list-unstyled clearfix">
				
				<!-- bag1-->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/001.jpg" alt=""></p>
						<p class="itemName">エルメス<br />
							エブリンⅢ　トリヨンクレマンス</p>
						<hr>
						<p> <span class="red">A社</span>：176,000円 <br>
							<span class="blue">B社</span>：172,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">180,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>8,000円</p>
					</div>
				</li>
				
				<!-- bag2-->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/002.jpg" alt=""></p>
						<p class="itemName">プラダ<br />
							シティトート2WAYバッグ</p>
						<hr>
						<p> <span class="red">A社</span>：128,000円 <br>
							<span class="blue">B社</span>：125,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">132,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>7,000円</p>
					</div>
				</li>
				
				<!-- bag3-->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/003.jpg" alt=""></p>
						<p class="itemName">バンブーデイリー2WAYバッグ</p>
						<hr>
						<p> <span class="red">A社</span>：83,000円 <br>
							<span class="blue">B社</span>：82,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">85,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>3,000円</p>
					</div>
				</li>
				
				<!-- bag4-->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/004.jpg" alt=""></p>
						<p class="itemName">LOUIS VUITTON <br>
							モノグラムモンスリGM</p>
						<hr>
						<p> <span class="red">A社</span>：64,000円 <br>
							<span class="blue">B社</span>：63,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">66,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>3,000円</p>
					</div>
				</li>
				
				<!-- bag5-->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/005.jpg" alt=""></p>
						<p class="itemName">HERMES <br>
							バーキン30</p>
						<hr>
						<p> <span class="red">A社</span>：1,150,000円 <br>
							<span class="blue">B社</span>：1,130,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">1,200,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>70,000円</p>
					</div>
				</li>
				
				<!-- bag6-->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/006.jpg" alt=""></p>
						<p class="itemName">CHANEL <br>
							マトラッセダブルフラップダブルチェーンショルダーバッグ</p>
						<hr>
						<p> <span class="red">A社</span>：252,000円 <br>
							<span class="blue">B社</span>：248,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">260,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>12,000円</p>
					</div>
				</li>
				
				<!-- bag7-->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/007.jpg" alt=""></p>
						<p class="itemName">LOUIS VUITTON <br>
							ダミエ ネヴァーフルMM</p>
						<hr>
						<p> <span class="red">A社</span>：96,000円 <br>
							<span class="blue">B社</span>：94,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">98,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>4,000円</p>
					</div>
				</li>
				
				<!-- bag8-->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/008.jpg" alt=""></p>
						<p class="itemName">CELINE <br>
							ラゲージマイクロショッパー</p>
						<hr>
						<p> <span class="red">A社</span>：150,000円 <br>
							<span class="blue">B社</span>：148,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">155,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>7,000円</p>
					</div>
				</li>
				
				<!-- bag9-->
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/009.jpg" alt=""></p>
						<p class="itemName">ロエベ <br>
							アマソナ23</p>
						<hr>
						<p> <span class="red">A社</span>：86,000円 <br>
							<span class="blue">B社</span>：82,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">90,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>8,000円</p>
					</div>
				</li>
			</ul>
		</div>
		</div>
	</div>
	
	</section>

<section class="kaitori_voice fix">
<h3>お客様の声</h3>

<ul>
<li>
<p class="kaitori_tab tentou_tab">店頭買取</p>
<h4>パネライ　時計　</h4>
<p class="voice_txt">新しい時計が気になり、今まで愛用していたパネライの腕時計を買取してらもらうことにしました。<br />
手元に保管しておくという手もあったのですが、やはり使わずに時計を手元に置いておくよりは、買取資金の足しにした方がいいのかなと思ったので・・・。<br />
通勤するときに、ブランドリバリューの近くを通るので、通勤ついでにブランドリバリューで試しで査定をしてもらうことにしたのですが、本当にこちらを利用してよかったです。<br />
私もブランドに詳しい自信がありましたが、査定士さんはそれ以上の知識があり、きちんとブランドの価値をわかってくださいました。本当に買取額に対して満足することができただけではなく、安心して買取手続きをすることができました。<br />
やはり買取査定はブランドリバリューのように、きちんとブランド知識があるところでお願いしないと満足できないですよね。</p>
</li>

<li>
<p class="kaitori_tab tentou_tab">店頭買取</p>
<h4>シャネル　バッグ　</h4>
<p class="voice_txt">シャネルのバッグの査定をしてもらいました。<br />
店頭買取をしてもらったのですが、実はブランドリバリューに出向く前に他社比較もしようと思い、他の買取店でも査定をしてもらっていたんです。
ですが、他社比較の結果、断然！ブランドリバリューは高値の査定を出してくれました！<br />
ブランドリバリューは、きちんと店頭買取をすることができる、いわば実際の店舗が存在するブランド買取店でもあるので、この安心さも兼ねそろえているのが個人的に安心でした。<br />
また、他にもブランドバッグで買取をしてほしいものが出てきたら、絶対にブランドリバリューを利用するつもりです。
もう他社比較はしません！笑</p>
</li>
<li>
<p class="kaitori_tab tentou_tab">店頭買取</p>
<h4>ボッテガヴェネタ　財布</h4>
<p class="voice_txt">好みのブランドから、新作のお財布が販売されたので、今使っているボッテガヴェネタの財布を買取してもらおうと思い、ブランドリバリューを利用しました。<br />
店舗の位置をチェックしてみると、銀座駅からもめっちゃ近かったので、店頭買取を利用しての買取です。<br />
今回は、ボッテガヴェネタの財布１つだけの買取だったので、このお財布１つだけで、買取査定を依頼するのは何だか申し訳ないなと思ったりもしたのですが、担当の方もとても丁寧で気持ちのいい対応をとってくださったので、本当によかったです。<br />
査定額も満足することができたので、機会があればまた利用したいと思っています。</p>
</li>


</ul>


</section>
		<section>
		<h3 class="mt50">渋谷店　立地へのこだわり</h3>
		<p>"私たちは2016年に銀座の激戦区でブランドリバリュー銀座店1号店を出店させていただきました。そこでは他のブランド品買取店に負けない「最高水準の高額買取」、「誠実で懇切丁寧な接客」を強みとして多くのお客様にご利用いただいております。<br /><br />

銀座で営業する中で、渋谷地域での出張買取の要望や渋谷店舗の有無のお問合せ等、お客様から渋谷店の出店要望を多くいただきました。お客様の渋谷までのアクセスの良さに可能性を感じ、この度ブランドリバリュー渋谷店2号店を渋谷のシンボルの一つであるタワーレコード前に出店させていただきました。<br /><br />

ちょっとした空き時間やお仕事帰り、銀座でのショッピングついでなどでも、ジュエリー・時計・バッグ等のブランド商品や宝石・宝飾品・貴金属品の査定・買取が可能ですので、どうぞお気軽にお立ち寄りください。<br /><br />

また、売りたいと検討中の商品の現物をお持込みいただかずとも、お電話やメールやLINE、店頭にて商品名や状態を伺って仮査定をさせていただくことも可能でございます。<br />
店頭買取だけではなく、予約制買取、出張買取、宅配買取、手ぶらで買取等お客様のニーズに応えるべく様々な買取サービスをご用意しております。 お住いから渋谷・銀座まで行くのに時間がかかってしまうというお客様は宅配買取・店頭買取を利用していらっしゃいますので併せてご検討下さい。
<br /><br />
当店では長年、リユース業界でジュエリー・時計・ バッグ等の買取に携わってきたプロフェッショナルであるバイヤーが、よりお客様にご満足いただけるよう、お客様の気持ちに親身になり他店を上回る高額査定にてご要望にお答え申し上げます。<br />
渋谷店スタッフ一同、皆様のご来店心よりお待ち申し上げております。<br />
<br /><br />
渋谷でジュエリー・時計・バッグの買取店をお探しの皆様、ブランド買取なら高額査定のBRANDREVALUE(ブランドリバリュー)へ。"							
</p>
		
		
		</section>
</div>

<?php
  
  // お問い合わせ
  get_template_part('_action');
  
  // 3つのポイント
  get_template_part('_purchase');
  
  // お問い合わせ
  get_template_part('_action2');
  
  // 店舗
  get_template_part('_shopinfo');
  
  // フッター
  get_footer();

