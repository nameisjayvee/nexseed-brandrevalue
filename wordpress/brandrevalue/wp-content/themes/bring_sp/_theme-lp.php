<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 *
 * Template Name: 特別買取強化LP用テンプレート
 */


get_header(); ?>
      

<div id="primary" class="cat-page content-area">
  <div class="mv_area ">
    <?php echo wp_get_attachment_image(SCF::get('lp-mainVIsSP'), 'full'); ?>
  </div>
  <div class="lp_main">
    <div class="mv_area ">
    <?php echo wp_get_attachment_image(SCF::get('lp-mainVIsHeadlineSP'), 'full'); ?>
  </div>

  <section id="hikaku" class="watch_hikaku">
    <p class="hikaku_img"><?php echo wp_get_attachment_image(SCF::get('lp-resultPickupImageSP'), 'full'); ?></p>
  </section>
    
  <p id="catchcopy"><?php echo nl2br(SCF::get('lp-catchcopy')); ?></p>
  
  <section id="cat-point">
    <h3 class="obi_tl">高価買取のポイント</h3>
    <ul>
      <li>
        <p class="pt_bigtl">POINT1</p>
        <div class="pt_wrap">
          <p class="pt_tl">商品情報が明確だと査定がスムーズ</p>
          <p class="pt_tx">ブランド名、モデル名が明確だと査定がスピーディに出来、買取価格にもプラスに働きます。また、新作や人気モデル、人気ブランドであれば買取価格がプラスになります。</p>
        </div>
      </li>
      <li>
        <p class="pt_bigtl">POINT2</p>
        <div class="pt_wrap">
          <p class="pt_tl">数点まとめての査定だと<br>買取がスムーズ</p>
          <p class="pt_tx">数点まとめての査定依頼ですと、買取価格をプラスさせていただきます。</p>
        </div>
      </li>
      <li>
        <p class="pt_bigtl">POINT3</p>
        <div class="pt_wrap">
          <p class="pt_tl">品物の状態がよいほど<br>査定がスムーズ</p>
          <p class="pt_tx">お品物の状態が良ければ良いほど、買取価格もプラスになります。</p>
        </div>
      </li>
    </ul>
    
    <p><?php echo nl2br(SCF::get('lp-resultPickupText')); ?></p>
  </section>

  <section id="lp-cat-jisseki">
    <h3 class="obi_tl">買取実績</h3>
    <ul id="box-jisseki" class="list-unstyled clearfix">
      <?php
        $resultLists = SCF::get('resultLists');
        foreach($resultLists as $list):
      ?>
      <li class="box-4">
        <div class="title">
          <p class="bx_img"><?php echo wp_get_attachment_image($list['resultImage'], 'full'); ?></p>
          <p class="itemName"><?php echo $list['resultName']; ?><br></p>
          <hr>
          <p><span class="red">A社</span>：<?php echo number_format($list['resultPriceA']); ?>円<br>
            <span class="blue">B社</span>：<?php echo number_format($list['resultPriceB']); ?>円</p>
        </div>
        <div class="box-jisseki-cat">
          <h3>買取価格例</h3>
          <p class="price">
            <?php echo number_format($list['resultPrice']); ?><span class="small">円</span></p>
        </div>
        <div class="sagaku">
          <p><span class="small">一言コメント</span><br>
          <?php echo nl2br($list['resultComment']); ?></p>
        </div>
      </li>
      <?php endforeach; ?>
    </ul>
  </section>

  <section id="about_kaitori" class="clearfix">
    <?php
      // 買取について
      get_template_part('_widerange2');
    ?>
  </section>
  
  <?php
    // 買取基準
    get_template_part('_criterion');

    // NGアイテム
    get_template_part('_no_criterion');

    // カテゴリーリンク
    get_template_part('_category');
  ?>
    
  <section id="kaitori_genre">
    <h3 class="obi_tl">その他買取可能なジャンル</h3>
    <p>【買取ジャンル】バッグ/ウエストポーチ/セカンドバック/トートバッグ/ビジネスバッグ/ボストンバッグ/クラッチバッグ/トランクケース/ショルダーバッグ/ポーチ/財布/カードケース/パスケース/キーケース/手帳/腕時計/ミュール/サンダル/ビジネスシューズ/パンプス/ブーツ/ペアリング/リング/ネックレス/ペンダント/ピアス/イアリング/ブローチ/ブレスレット/ライター/手袋/傘/ベルト/ペン/リストバンド/アンクレット/アクセサリー/サングラス/帽子/マフラー/ハンカチ/ネクタイ/ストール/スカーフ/バングル/カットソー/アンサンブル/ジャケット/コート/ブルゾン/ワンピース/ニット/シャツメンズ/毛皮/Tシャツ/キャミソール/タンクトップ/パーカー/ベスト/ポロシャツ/ジーンズ/スカート/スーツなど</p>
  </section>

  <?php
    // 買取方法
    get_template_part('_purchase');
  ?>

  <section id="user_voice">
    <h3>ユーザーボイス</h3>
    <p class="user_voice_text1"><?php echo nl2br(SCF::get('voice-user')); ?></p>
    
    <h3>バイヤー目線</h3>
    <p class="user_voice_text2"><?php echo nl2br(SCF::get('voice-buyer')); ?></p>
  </section>

</div>
<!-- lp_main -->
</div>
<!-- #primary -->

<?php
get_footer();