<?php
get_header(); ?>

<div class="mv_area ">
<!--<img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kv-faq.png " alt="BRAMD REVALUE ">-->
<h2>BLOG</h2>
</div>

<section id="bloglist">

  <?php
  //-------------------------------------
    if(have_posts()): while(have_posts()): the_post();
  ?>

  <div class="postlist clearfix">
    <a href="<?php the_permalink(); ?>">
      <h3><?php the_title(); ?></h3>
    </a>
    <p class="date"><?php the_time('Y年m月d日（D）'); ?></p>
    <p class="text"><?php the_excerpt(); ?></p>
    <p class="intoSingle alignright"><a href="<?php the_permalink(); ?>">続きを読む</a></p>
  </div>

  <?php
    endwhile; endif;
  //-------------------------------------
  ?>



    <div class="blog-pagination">
      <?php global $wp_rewrite;
      $paginate_base = get_pagenum_link(1);
      if(strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()){
          $paginate_format = '';
          $paginate_base = add_query_arg('paged','%#%');
      }
      else{
          $paginate_format = (substr($paginate_base,-1,1) == '/' ? '' : '/') .
          user_trailingslashit('page/%#%/','paged');;
          $paginate_base .= '%_%';
      }
      echo paginate_links(array(
          'base' => $paginate_base,
          'format' => $paginate_format,
          'total' => $wp_query->max_num_pages,
          'mid_size' => 1,
          'current' => ($paged ? $paged : 1),
          'prev_text' => '« 前',
          'next_text' => '次 »',
      )); ?>
    </div>




    </section>

<?php

  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
