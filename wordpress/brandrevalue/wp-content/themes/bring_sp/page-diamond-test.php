<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
  //'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  '001.jpg::ルースダイヤ　1.098ct　G　SI2　G　NONE::330,000::335,000::340,000::10,000',
  '002.jpg::ダイヤネックレス　Pt850　0.81ct　MD0.23ct::340,000::330,000::350,000 ::17,000',
  '003.jpg::ヴァンクリーフ&アーペル　クロアミニ　アチュールクロス　ダイヤネックレス::117,000::105,000::110,000 ::5,000',
  '004.jpg::ダイヤリング　Pt900　1.00ｃｔ　0.4ｃｔ::62,000::66,000::70,000::8,000',
  '005.jpg::ティファニー　Pt900　ダイヤリング::30,000::27,000::32,000::5,000',
  '006.jpg::ルースダイヤ　1.006ct　H　VS2　VG　NONE　::440,000::445,000::450,000::1,000',
  '007.jpg::ダイヤリング　Pt900　0.51ct　::45,000::47,800::50,000::5,000',
  '008.jpg::ルースダイヤ　1.793ct　J　SI2　G　NONE　::620,000::611,000::630,000::19,000',
  '009.jpg::ダイヤリング　K18　1.48ｃｔ　::58,000::60,000::63,000::5,000',
  '010.jpg::ブルガリ　B-ZERO1　ダイヤネネックレス　K18WG::220,000::215,000::225,000::10,000',
  '011.jpg::ブシュロン　ディアマン　ダイヤネックレス　750::225,000::219,000::230,000::11,000',
  '012.jpg::カルティエ　ハーフダイヤラブリング::97,000::93,000::103,000::10,000',
);


get_header(); ?>

<div id="primary" class="cat-page content-area">
	<div class="mv_area "> <img src="<?php echo get_s3_template_directory_uri() ?>/images/lp_main/cat_diamond_main.jpg" alt="あなたのダイヤモンドお売り下さい！"> </div>
	<p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
	<p class="main_bottom">ダイヤモンド買取！！業界最高峰の買取価格をお約束いたします。</p>
	<div id="lp_head" class="dia_ttl">
		<div>
			<p>銀座で最高水準の査定価格・サービス品質をご体験ください。</p>
			<h2>あなたのダイヤモンド<br />
				どんな物でもお売り下さい！！</h2>
		</div>
	</div>
	<div class="lp_main">
		<section id="hikaku" class="watch_hikaku">
			<p class="hikaku_img"><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat/dia_hikaku.png"></p>
		</section>
		<section id="cat-point">
			<h3 class="obi_tl">高価買取のポイント</h3>
			<ul>
				<li>
					<p class="pt_bigtl">POINT1</p>
					<div class="pt_wrap">
						<p class="pt_tl">商品情報が明確だと査定がスムーズ</p>
						<p class="pt_tx">ブランド名、モデル名が明確だと査定がスピーディに出来、買取価格にもプラスに働きます。また、新作や人気モデル、人気ブランドであれば買取価格がプラスになります。</p>
					</div>
				</li>
				<li>
					<p class="pt_bigtl">POINT2</p>
					<div class="pt_wrap">
						<p class="pt_tl">数点まとめての査定だと <br>
							キャンペーンで高価買取が可能</p>
						<p class="pt_tx">数点まとめての査定依頼ですと、買取価格をプラスさせていただきます。</p>
					</div>
				</li>
				<li>
					<p class="pt_bigtl">POINT3</p>
					<div class="pt_wrap">
						<p class="pt_tl">品物の状態がよいほど <br>
							高価買取が可能</p>
						<p class="pt_tx">お品物の状態が良ければ良いほど、買取価格もプラスになります。</p>
					</div>
				</li>
			</ul>
			<p>天然の鉱物の中で最も硬度が高いといわれるダイヤモンドですが、実は瞬間的な衝撃には弱く、欠けてしまうことも珍しくありません。 <br>
				そのため、査定のためお持ちいただく際にも傷がつかないよう、緩衝材等で保護いただくことをお勧めしております。また、ご自宅で軽く布でぬぐうなどして汚れを落としておいた方が、査定額は高くなります。ときどきリングやペンダントからダイヤモンドを外して持ってこられる方もいらっしゃいますが、取り外しの際に傷がつく可能性があるので、そのままお持ちください。もし購入時に入手した鑑別書があれば、ぜひお持ちください。査定がスムーズになり、買取価格も上がりやすくなります。もちろん鑑定書等がなくてもご心配なく。経験豊かな鑑定士が専用の機器を使い、迅速かつ正確に鑑定を行います。 </p>
		</section>
		<!--買取評価チェックコンテンツ-->
		<section id="check_cont">
			<h3>4C評価を入れて買取相場をチェック</h3>
			<form action="" name="sel_form" id="sel_form">
				<table class="kakaku_input">
					<tr>
						<th><span>●</span>カラー / 色</th>
						<td><select id="color_id" name="color_id">
								<option value="">選択して下さい</option>
								<option value="1">D</option>
								<option value="2">E</option>
								<option value="3">F</option>
								<option value="4">G</option>
								<option value="5">H</option>
								<option value="6">I</option>
								<option value="7">J</option>
							</select></td>
						</tr>
						<tr>
						<th><span>●</span>カット / 総合評価</th>
						<td><select id="cut_id" name="cut_id">
								<option value="">選択して下さい</option>
								<option value="1">Excellent(優秀)</option>
								<option value="2">Verygood(優良)</option>
								<option value="3">Good(良好)</option>
							</select></td>
					</tr>
					<tr>
						<th><span>●</span>クラリティ /内包物</th>
						<td><select id="clarity_id" name="clarity_id">
								<option value="">選択して下さい</option>
								<option value="1">VVS1</option>
								<option value="2">VVS2</option>
								<option value="3">VS1</option>
								<option value="4">VS2</option>
								<option value="5">SI1</option>
								<option value="6">SI2</option>
							</select></td>
						</tr>
												<tr>

						<th><span>●</span>カラット /重量</th>
						<td><input name="carat_value" id="carat_value" type="text" style="ime-mode:disabled;"></td>
					</tr>
				</table>
				<div id="kakaku_btn">
					<input type="button" class="hyouka_btn" id="estimation_btn" value="買取価格を調べる">
				</div>
				<div class="kakaku_box">
				<dl>
							<dt>買取価格</dt>
							<dd id="purchase_price"></dd>
				</dl>
				</div>
			</form>
		</section>
		<!--/買取評価チェックコンテンツ-->
		<section id="lp-cat-jisseki">
			<h3 class="obi_tl">買取実績</h3>
			<ul id="box-jisseki" class="list-unstyled clearfix">
				
				<!-- 
                  <li class="box-4">
                        <div class="title">
                         <p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/images/item/dia/lp/001.jpg" alt=""></p>
                            <p class="itemName">ダイヤルース1.098ct
                            </p>
                            < p class="itemdetail">カラット：2ct
                                <br> カラー：VERY LIGHT PINK
                                <br> クラリティ：VS２
                                <br> 形状：ハートシェイプ<br><br>
                            </p>
                            <hr>
                            <p> <span class="red">A社</span>：142,000円
                                <br>
                                <span class="blue">B社</span>：140,000円 </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">145,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>250,000円</p>
                        </div>
                    </li>
                    <li class="box-4">
                        <div class="title">
                         <p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/images/item/dia/lp/002.jpg" alt=""></p>
                            <p class="itemName">K18YG
                                <br> ダイヤモンドリング
                            </p>
                            <p class="itemdetail">カラット(主石)：0.54ct
                                <br> カラー：E
                                <br> クラリティ：VS２
                                <br> カット：VERY GOOD
                                <br> 形状：ラウンドブリリアント
                                <br> メレダイヤモンド0.7ct
                                <br> 地金：18金イエローゴールド　5ｇ
                            </p>
                            <hr>
                            <p> <span class="red">A社</span>：145,500円
                                <br>
                                <span class="blue">B社</span>：142,500円 </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price"><span class="small">地金＋</span>150,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>7,500円</p>
                        </div>
                    </li>
                    <li class="box-4">
                        <div class="title">
                    <p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/images/item/dia/lp/003.jpg" alt=""></p>
                            <p class="itemName">ティファニー
                                <br>ソレスト　ダイヤモンドリング</p>
                            <p class="itemdetail">
                                カラット：0.31ct
                                <br> カラー：E
                                <br> クラリティ：VVS2
                                <br> カット：EXCELLENT</p>
                            <hr>
                            <p> <span class="red">A社</span>：320,100円
                                <br>
                                <span class="blue">B社</span>：313,500円 </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price"><span class="small">地金＋</span>330,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>16,500円</p>
                        </div>
                    </li>
                     <li class="box-4">
                        <div class="title">
                         <p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/images/item/dia/lp/004.jpg" alt=""></p>
                            <p class="itemName">ペアシェイプ
                                <br> ダイヤモンドルース
                            </p>
                            <p class="itemdetail">カラット：5ct
                                <br> カラー：F
                                <br> クラアリティ：SI2
                                <br> 形状：ペアシェイプ<br><br><br><br>
                            </p>
                            <hr>
                            <p> <span class="red">A社</span>：3,395,500円
                                <br>
                                <span class="blue">B社</span>：3,325,000円 </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price"><span class="small">地金＋</span>3,500,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>175,000円</p>
                        </div>
                    </li>
                     <li class="box-4">
                        <div class="title">
                         <p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/images/item/dia/lp/005.jpg" alt=""></p>
                            <p class="itemName">ダイヤモンド　ルース</p>
                            <p class="itemdetail">カラット：1.003ct
                                <br> カラー:E
                                <br> クラリティ:SI1
                                <br> カット:EXCELLENT
                                <br> 形状:ラウンドブリリアントカット
                            </p>
                            <hr>
                            <p> <span class="red">A社</span>：194,000円
                                <br>
                                <span class="blue">B社</span>：190,000円 </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price"><span class="small">地金＋</span>480,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>10,000円</p>
                        </div>
                    </li>
                    <li class="box-4">
                        <div class="title">
                        <p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/images/item/dia/lp/006.jpg" alt=""></p>
                            <p class="itemName">カルティエ　バレリーナ　ハーフエタニティダイヤリング</p>
                            <p class="itemdetail">PT950
                                <br> カラット：0.51ct
                                <br> カラー：F
                                <br> クラリティ：VVS１
                                <br> カット：VERY　GOOD
                            </p>
                            <hr>
                            <p> <span class="red">A社</span>：388,000円
                                <br>
                                <span class="blue">B社</span>：380,000円 </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price"><span class="small">地金＋</span>400,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>20,000円</p>
                        </div>
                    </li>
                    <li class="box-4">
                        <div class="title">
                        <p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/images/item/dia/lp/007.jpg" alt=""></p>
                            <p class="itemName">ハリーウィンストン
                                <br> マドンナクロスネックレス
                            </p>
                            <p class="itemdetail">Pt950
                                <br>カラット：1.0ct
                                <br> カラー：D
                                <br>クラリティ：VVS2
                                <br> カット：EXCELLENT
                            </p>
                            <hr>
                            <p> <span class="red">A社</span>：970,000円
                                <br>
                                <span class="blue">B社</span>：950,000円 </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price"><span class="small">地金＋</span>1,000,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>50,000円</p>
                        </div>
                    </li>
                    <li class="box-4">
                        <div class="title">
                        <p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/images/item/dia/lp/008.jpg" alt=""></p>
                            <p class="itemName">ピアジェ　パッション
                                <br> ダイヤリング
                            </p>
                            <p class="itemdetail">Pt950
                                <br> カラット：0.30ct
                                <br> カラー：E
                                <br> クラリティ：VVS2
                                <br> カット：VG
                            </p>
                            <hr>
                            <p> <span class="red">A社</span>：200,000円
                                <br>
                                <span class="blue">B社</span>：210,000円 </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price"><span class="small">地金＋</span>230,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>30,000円</p>
                        </div>
                    </li> -->
				
				<?php
            foreach($resultLists as $list):
            // :: で分割
            $listItem = explode('::', $list);
          
          ?>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/images/item/dia/lp/<?php echo $listItem[0]; ?>" alt=""></p>
						<p class="itemName"> <?php echo $listItem[1]; ?> </p>
						<hr>
						<p> <span class="red">A社</span>： <?php echo $listItem[2]; ?>円 <br>
							<span class="blue">B社</span>： <?php echo $listItem[3]; ?>円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price"> <?php echo $listItem[4]; ?><span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span> <?php echo $listItem[5]; ?>円</p>
					</div>
				</li>
				<?php endforeach; ?>
			</ul>
			<h3 class="new_list_ttl">ブランドリスト</h3>
			<ul class="list-unstyled new_list">
				<li>
					<dl>
						<a href="<?php echo home_url('/cat/gold/ingot'); ?>">
						<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold05.jpg" alt="インゴット"></dd>
						<dt>インゴット</dt>
						</a>
					</dl>
				</li>
				<li>
					<dl>
						<a href="<?php echo home_url('/cat/gold/gold-coin'); ?>">
						<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold11.jpg" alt="金貨"></dd>
						<dt>金貨</dt>
						</a>
					</dl>
				</li>
				<li>
					<dl>
						<a href="<?php echo home_url('/cat/gold/14k'); ?>">
						<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold01.jpg" alt="14金"></dd>
						<dt>14金</dt>
						</a>
					</dl>
				</li>
				<li>
					<dl>
						<a href="<?php echo home_url('/cat/gold/18k'); ?>">
						<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold02.jpg" alt="18金"></dd>
						<dt>18金</dt>
						</a>
					</dl>
				</li>
				<li>
					<dl>
						<a href="<?php echo home_url('/cat/gold/22k'); ?>">
						<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold03.jpg" alt="22金"></dd>
						<dt>22金</dt>
						</a>
					</dl>
				</li>
				<li>
					<dl>
						<a href="<?php echo home_url('/cat/gold/24k'); ?>">
						<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold04.jpg" alt="24金"></dd>
						<dt>24金</dt>
						</a>
					</dl>
				</li>
				<li>
					<dl>
						<a href="<?php echo home_url('/cat/gold/white-gold'); ?>">
						<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold06.jpg" alt="ホワイトゴールド"></dd>
						<dt>ホワイトゴールド</dt>
						</a>
					</dl>
				</li>
				<li>
					<dl>
						<a href="<?php echo home_url('/cat/gold/pink-gold'); ?>">
						<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold07.jpg" alt="ピンクゴールド"></dd>
						<dt>ピンクゴールド</dt>
						</a>
					</dl>
				</li>
				<li>
					<dl>
						<a href="<?php echo home_url('/cat/gold/yellow-gold'); ?>">
						<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold08.jpg" alt="イエローゴールド"></dd>
						<dt>イエローゴールド</dt>
						</a>
					</dl>
				</li>
				<li>
					<dl>
						<a href="<?php echo home_url('/cat/gold/platinum'); ?>">
						<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold09.jpg" alt="プラチナ"></dd>
						<dt>プラチナ</dt>
						</a>
					</dl>
				</li>
				<li>
					<dl>
						<a href="<?php echo home_url('/cat/gem/cartier'); ?>">
						<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem01.jpg" alt="カルティエ"></dd>
						<dt>Cartier<span></span>カルティエ</dt>
						</a>
					</dl>
				</li>
				<li>
					<dl>
						<a href="<?php echo home_url('/cat/gem/tiffany'); ?>">
						<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem02.jpg" alt="ティファニー"></dd>
						<dt><span>Tiffany</span>ティファニー</dt>
						</a>
					</dl>
				</li>
				<li>
					<dl>
						<a href="<?php echo home_url('/cat/gem/harrywinston'); ?>">
						<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem03.jpg" alt="ハリーウィンストン"></dd>
						<dt><span>Harrywinston</span>ハリーウィンストン</dt>
						</a>
					</dl>
				</li>
				<li>
					<dl>
						<a href="<?php echo home_url('/cat/gem/piaget'); ?>">
						<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch22.jpg" alt="ピアジェ"></dd>
						<dt><span>PIAGET</span>ピアジェ</dt>
						</a>
					</dl>
				</li>
				<li>
					<dl>
						<a href="<?php echo home_url('/cat/gem/vancleefarpels'); ?>">
						<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch30.jpg" alt="ヴァンクリーフ&アーペル"></dd>
						<dt><span>Vanleefarpels</span>ヴァンクリーフ&アーペル</dt>
						</a>
					</dl>
				<li>
					<dl>
						<a href="<?php echo home_url('/cat/gem/bulgari'); ?>">
						<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch27.jpg" alt="ブルガリ"></dd>
						<dt><span>BVLGALI</span>ブルガリ</dt>
						</a>
					</dl>
				</li>
				<li>
					<dl>
						<a href="<?php echo home_url('/cat/gem/boucheron'); ?>">
						<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem06.jpg" alt="ブシュロン"></dd>
						<dt><span>Boucheron</span>ブシュロン</dt>
						</a>
					</dl>
				</li>
			</ul>
			<p id="catchcopy">BRAND REVALUEがモットーとしているのは「お客様に利益を還元する店舗づくり」。路面店ではなくあえて家賃の安い空中階に店舗を構え、人件費・広告費を抑えているのも、削減した経費の分査定額を高めるためです。ダイヤモンドの買取においても、「鑑定書がないダイヤモンド」、「ノンブランドのダイヤモンド」、「極小粒のダイヤモンド（メレ）」などどんなものでもご満足いただける価格で買い取らせていただきます。新しい店舗ではありますが、経験豊かな鑑定士が適切に査定を行いますので、どうかお気軽にご相談ください。 </p>
			<p class="jyoutai_tl">こんな状態でも買取致します!</p>
			<div id="konna">
				<p class="example1">■デザインが古い</p>
				<p class="jyoutai_img"><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat/dia_jyoutai01.jpg"></p>
				<p class="example2">■メレダイヤ</p>
				<p class="jyoutai_img"><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat/dia_jyoutai02.jpg"></p>
				<p class="example3">■石のみ</p>
				<p class="jyoutai_img"><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat/dia_jyoutai03.jpg"></p>
				<p class="text">その他：鑑定書無し等でもお買取りいたします。</p>
			</div>
		</section>
		<section id="about_kaitori" class="clearfix">
			<?php
        // 買取について
        get_template_part('_widerange');
				get_template_part('_widerange2');
      ?>
		</section>
		<section> <img src="<?php echo get_s3_template_directory_uri() ?>/images/bn_matomegai.png"> </section>
		<?php
        // 買取基準
        get_template_part('_criterion');

        // NGアイテム
        get_template_part('_no_criterion');
        
				// カテゴリーリンク
        get_template_part('_category');
			?>
		<section id="kaitori_genre">
			<h3 class="obi_tl">その他買取可能なジャンル</h3>
			<p>【買取ジャンル】バッグ/ウエストポーチ/セカンドバック/トートバッグ/ビジネスバッグ/ボストンバッグ/クラッチバッグ/トランクケース/ショルダーバッグ/ポーチ/財布/カードケース/パスケース/キーケース/手帳/腕時計/ミュール/サンダル/ビジネスシューズ/パンプス/ブーツ/ペアリング/リング/ネックレス/ペンダント/ピアス/イアリング/ブローチ/ブレスレット/ライター/手袋/傘/ベルト/ペン/リストバンド/アンクレット/アクセサリー/サングラス/帽子/マフラー/ハンカチ/ネクタイ/ストール/スカーフ/バングル/カットソー/アンサンブル/ジャケット/コート/ブルゾン/ワンピース/ニット/シャツメンズ/毛皮/Tシャツ/キャミソール/タンクトップ/パーカー/ベスト/ポロシャツ/ジーンズ/スカート/スーツなど</p>
		</section>
		<?php
        // 買取方法
        get_template_part('_purchase');
      ?>
		<section id="user_voice">
			<h3>ご利用いただいたお客様の声</h3>
			<p class="user_voice_text1">ちょうど家の整理をしていたところ、家のポストにチラシが入っていたので、ブランドリバリューに電話してみました。今まで買取店を利用したことがなく、不安でしたがとても丁寧な電話の対応とブランドリバリューの豊富な取り扱い品目を魅力に感じ、出張買取を依頼することにしました。 絶対に売れないだろうと思った、動かない時計や古くて痛んだバッグ、壊れてしまった貴金属のアクセサリーなども高額で買い取っていただいて、とても満足しています。古紙幣や絵画、食器なども買い取っていただけるとのことでしたので、また家を整理するときにまとめて見てもらおうと思います。 </p>
			<h3>鑑定士からのコメント</h3>
			<p class="user_voice_text2">家の整理をしているが、何を買い取ってもらえるか分からないから一回見に来て欲しいとのことでご連絡いただきました。 買取店が初めてで不安だったが、丁寧な対応に非常に安心しましたと笑顔でおっしゃって頂いたことを嬉しく思います。 物置にずっとしまっていた時計や、古いカバン、壊れてしまったアクセサリーなどもしっかりと価値を見極めて高額で提示させて頂きましたところ、お客様もこんなに高く買取してもらえるのかと驚いておりました。 これからも家の不用品を整理するときや物の価値判断ができないときは、すぐにお電話しますとおっしゃって頂きました。 </p>
		</section>
	</div>
	<!-- lp_main --> 
</div>
<!-- #primary -->
<script src="//kaitorisatei.info/brandrevalue/wp-content/themes/bring/js/gaisan.js" type="text/javascript"></script>

<?php

get_footer();
