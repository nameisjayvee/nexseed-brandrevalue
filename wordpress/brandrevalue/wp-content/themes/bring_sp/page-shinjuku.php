<?php

get_header(); ?>

    <div class="mv_area "> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shinjuku_mv.png " alt="店舗案内"> </div>
    <div class="shop_page">
        <h2 class="shop_bold"><br />新宿でジュエリー・時計・バッグなどのブランド買取先をお探しの皆様へ。<br>ブランド買取なら高額査定のBRAND REVALUEへ。</h2>
        <p>最寄り駅は、JR・地下鉄・私鉄各線が通る、「JR新宿駅」「東京メトロ新宿三丁目駅」。<br>BRANDREVALUE(ブランドリバリュー)新宿店は、新宿地下道A5出口を出て、東京のシンボルの一つである新宿伊勢丹の向かいに立つUFJ銀行の真裏に位置する大伸第２ビル3階び店舗を構えております。<br>最寄り駅出口から徒歩3分と、非常にアクセスしやすい場所にあり、近くには伊勢丹、ビックロ、マルイ等、お買い物、お食事、ついででお越しの際にも便利にご利用頂けます。
        </p>
    </div>
    <div class="mv_area main-gallery" id="main-gallery">
        <div class="gallery-cell"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shinjuku/shinjuku01.png" alt="銀座店">
        </div>
        <div class="gallery-cell"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shinjuku/shinjuku02.png" alt="銀座店">
        </div>
        <div class="gallery-cell"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shinjuku/shinjuku03.png" alt="銀座店">
        </div>

    </div>
    <div class="link_gr ">
         <a href="https://www.google.co.jp/maps/uv?hl=ja&pb=!1s0x60188dd283f167b9%3A0x14ca2c9168c59191!2m22!2m2!1i80!2i80!3m1!2i20!16m16!1b1!2m2!1m1!1e1!2m2!1m1!1e3!2m2!1m1!1e5!2m2!1m1!1e4!2m2!1m1!1e6!3m1!7e115!4s%2F%2Fgeo1.ggpht.com%2Fcbk%3Fpanoid%3DzugWrFwIxWhEBVFoxh4qOA%26output%3Dthumbnail%26cb_client%3Dsearch.LOCAL_UNIVERSAL.gps%26thumb%3D2%26w%3D572%26h%3D192%26yaw%3D257.73526%26pitch%3D0%26thumbfov%3D100!5z44OW44Op44Oz44OJ44Oq44OQ44Oq44Ol44O8IOaWsOWuvyAtIEdvb2dsZSDmpJzntKI&imagekey=!1e10!2sAF1QipOB_6QLb-XtuHgAeacg67vIt554c7JKaLl2YJfj" target="_blank" class="hiragino">店舗内をみる<i class="fas fa-caret-right"></i></a>

    </div>
    <div class="shop_page">
    <h2 class="shop_ttl">ブランドリバリュー新宿店 店舗案内</h2>
        <div><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shinjuku/shinjuku_map.jpg" alt="銀座店"></div>
        <table>
            <tr>
                <th>住所</th>
                <td class="shop_map">
                    〒160-0022<br>東京都新宿区新宿3丁目31-1大伸第２ビル3階</td>
            </tr>
            <tr>
                <th>営業時間</th>
                <td>11：00～21：00</td>
            </tr>
            <tr>
                <th>電話番号</th>
                <td><a href="tel:0120-970-060">0120-970-060</a></td>
            </tr>
            <tr>
                <th>駐車場</th>
                <td> - </td>
            </tr>
        </table>
    </div>
<div class="link_gr ">
        <a href="https://goo.gl/maps/dYjcme5kcj72" target="_blank" class="hiragino">地図アプリでみる<i class="fas fa-caret-right"></i></a>
</div>
    <div class="shop_page">
        <h3 class="mb30">新宿店までのアクセス</h3>
        <ul class="tab_menu02 shop_btn">
                    <li><a href="#shop01">新宿駅</a></li>
                    <li><a href="#shop02">新宿三丁目駅</a></li>
                </ul>
                <div class="tab_cont02 shop_cont" id="shop01">


        <p><span class="tx_bold">最寄り駅は、JR・地下鉄・私鉄各線が通る、「JR新宿駅」「東京メトロ新宿三丁目駅」。</span><br /> BRANDREVALUE(ブランドリバリュー)新宿店は、新宿地下道A5出口を出て、東京のシンボルの一つである新宿伊勢丹の向かいに立つUFJ銀行の真裏に位置する大伸第２ビル3階び店舗を構えております。<br>最寄り駅出口から徒歩3分と、非常にアクセスしやすい場所にあり、近くには伊勢丹、ビックロ、マルイ等、お買い物、お食事、ついででお越しの際にも便利にご利用頂けます。</p>
        <ul class="map_root">
            <li> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_shinjyuku01.jpg" alt="渋谷駅スクランブル交差点" />
                <div class="root_tx_bx">
                    <p class="root_ttl"><span>1</span>新宿駅中央東口</p>
                    <p>新宿中央東口の階段をお上がり下さい。 その他改札口をお出になった際は、中央東口までお越しください。
                    </p>
                </div>
            </li>
            <li> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_shinjyuku02.jpg" alt="A5出口" />
                <div class="root_tx_bx">
                    <p class="root_ttl"><span>2</span>アディダス・AINZ＆TULP間</p>
                    <p>中央東口出口の階段を上がると左手に「アディダス」「AINZ＆TULP(アインドアンドトルぺ)」が見えます。その間の通りへお進みください。左側の歩道をお進みください。</p>
                </div>
            </li>
            <li> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_shinjyuku03.jpg" alt="地上" />
                <div class="root_tx_bx">
                    <p class="root_ttl"><span>3</span>モスバーガー前</p>
                    <p>右手に「モスバーガー」を見ながら、そのまま直進します。</p>
                </div>
            </li>
            <li> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_shinjyuku04.jpg" alt="四谷学院ビル前" />
                <div class="root_tx_bx">
                    <p class="root_ttl"><span>4</span>大塚家具前</p>
                    <p>左手に郵便局（右手にタワーレコード）が見えましたら、目の前の横断歩道を直進します。 </p>
                </div>
            </li>
            <li> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_shinjyuku05.jpg" alt="エレベーター前" />
                <div class="root_tx_bx">
                    <p class="root_ttl"><span>5</span>ドコモショップ</p>
                    <p>駅から5分ほど進むと左手に「ドコモショップ」があります。(大伸第2ビル)</p>
                </div>
            </li>
            <li> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_shinjyuku06.jpg" alt="店内" />
                <div class="root_tx_bx">
                    <p class="root_ttl"><span>6</span>大伸第2ビル</p>
                    <p>ドコモショップの左手側に大伸第2ビルのエントランスがございます。ビル内のエレベーターで３Fまでお上がり下さい。</p>
                </div>
            </li>
        </ul>
        </div>
        <div class="tab_cont02 shop_cont" id="shop02">
                    <ul class="map_root">
                        <li> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_shinjyuku3_01.jpg" alt="新宿三丁目駅ルート" />
                            <div class="root_tx_bx">
                                <p class="root_ttl"><span>1</span>新宿三丁目駅A1出口</p>
                                <p>東京メトロ各線、新宿三丁目駅の改札を抜けましたら、地下通路よりA1出口へ。</p>
                            </div>
                        </li>
                        <li> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_shinjyuku3_02.jpg" alt="新宿三丁目駅ルート" />
                            <div class="root_tx_bx">
                                <p class="root_ttl"><span>2</span>伊勢丹新宿前</p>
                                <p>東京メトロ新宿三丁目駅のＡ１出口より地上へ。正面に伊勢丹新宿店が見えます。</p>
                            </div>
                        </li>
                        <li> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_shinjyuku3_03.jpg" alt="新宿三丁目駅ルート" />
                            <div class="root_tx_bx">
                                <p class="root_ttl"><span>3</span>新宿三丁目交差点</p>
                                <p>出口の右手に三菱ＵＦＪ、交差点の向かいにＪＴＢのビルがあります。交差点は渡らず右折します。</p>
                            </div>
                        </li>
                        <li> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_shinjyuku3_04.jpg" alt="新宿三丁目駅ルート" />
                            <div class="root_tx_bx">
                                <p class="root_ttl"><span>4</span>ドコモショップ</p>
                                <p>そのまま明治通りを直進し、ＮＴＴ Ｄｏｃｏｍｏのビルが角にある最初の交差点で右折します。</p>
                            </div>
                        </li>
                        <li> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_shinjyuku3_05.jpg" alt="新宿三丁目駅ルート" />
                            <div class="root_tx_bx">
                                <p class="root_ttl"><span>5</span>大伸第2ビル</p>
                                <p>ドコモショップ正面左手側に大伸第2ビルのエントランスがございます。</p>
                            </div>
                        </li>
                        <li> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_shinjyuku3_06.jpg" alt="新宿三丁目駅ルート" />
                            <div class="root_tx_bx">
                                <p class="root_ttl"><span>6</span>エントランス</p>
                                <p>ビル内のエレベーターで３Fまでお上がり下さい。</p>
                            </div>
                        </li>
                    </ul>
        </div>


                <h2 class="mb30">駐車場案内</h2>
                <p class="ant_ttl">NPC24H新宿３丁目パーキング　約30m</p>
                <table class="parking">
                    <tr>
                        <th>住所</th>
                        <td>東京都新宿区 新宿3-10-3</td>
                    </tr>
                    <tr>
                        <th>営業時間<br>料金</th>
                        <td>24時間</td>
                    </tr>
                </table>
                <p class="ant_ttl">ピットイン 新宿第７ 約30m</p>
                <table class="parking">
                    <tr>
                        <th>住所</th>
                        <td>東京都新宿区新宿3-32-2</td>
                    </tr>
                    <tr>
                        <th>営業時間<br>料金</th>
                        <td>全日 8:00～24:00 12分¥300<br> 全日 0:00～8:00 30分 ¥200<br> 最大料金 全日 22:00～8:00 ¥2000<br>
                        </td>
                    </tr>

                </table>
                <p class="ant_ttl">京王新宿追分ビル駐車場 約50m</p>
                <table class="parking">
                    <tr>
                        <th>住所</th>
                        <td>東京都新宿区新宿3-1-13 京王新宿追分ビル1F</td>
                    </tr>
                    <tr>
                        <th>営業時間<br>料金</th>
                        <td>24時間営業<br>8：00～24：00 100円 / 15分<br>24：00～翌8：00 100円 / 30分</td>
                    </tr>

                </table>
           <div class="takuhai_cvbox tentou">
            <p class="cv_tl">ブランドリバリュー新宿店で<br>店頭買取予約をする</p>
            <div class="custom_tel takuhai_tel">
                <a href="tel:0120-970-060">
                    <div class="tel_wrap">
                        <div class="telbox01"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/customer/telico.png" alt="お申し込みはこちら">
                            <p>お電話からでもお申込み可能！<br>ご不明な点は、お気軽にお問合せ下さい。 </p>
                        </div>
                        <div class="telbox02"> <span class="small_tx">【受付時間】11:00 ~ 21:00</span><span class="ted_tx"> 年中無休</span>
                            <p class="fts25">0120-970-060</p>
                        </div>
                    </div>
                </a>
            </div>
            <p><a href="<?php echo home_url('purchase/visit-form'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/tentou_cv.png" alt="店頭買取お申し込み"></a></p>
        </div>

        <section>
            <h3 class="mb30">新宿店　店内紹介・内装へのこだわり</h3>
            <div class="shop_comm"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/kodawari_com01.png" alt="店内" />
                <p>ブランドリバリュー新宿では、お客さまが快適に過ごして頂ける空間と最上級のサービスでおもてなし致します。 </p>
            </div>
            <div class="shop_comm"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/kodawari_com02.png" alt="店内" />
                <p>混雑状況によっては店内で少々お待たせしてしまう場合もあるかと思いますが少しでも落ち着けるよう、ソファー席をご用意しております。</p>
            </div>
            <div class="shop_comm"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/kodawari_com03.png" alt="店内" />
                <p>また、査定はお客さまのプライバシーをお守りするために完全個室の査定ブースを設けさせて頂いておりますので、お持ち込み頂いたお品物についてのご質問やご相談・要望などありましたら遠慮なくバイヤーへお申し付け下さい。 </p>
            </div>
        </section>
        <section>
            <h3>新宿店　鑑定士のご紹介</h3>
           <div class="staff_bx"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/staff05.png" alt="査定士 木村" />
				<p class="staff_name">木村 正伸 査定士</p>
				<h4>仕事における得意分野</h4>
				<p>私の得意分野は、宝石と貴金属です。
					現状に満足することなく切磋琢磨し知識の向上に努めております。</p>
				<h4>自身の強み</h4>
				<p>お客様の大事なジュエリー製品・貴金属製品を丁寧に査定いたします。
					金やプラチナなどジュエリーと呼ばれるお品物は、日々変動する貴金属相場より、お買取金額も変わってまいります。変動相場に基づきますが、地域ナンバーワンの高価買取を致します。</p>
				<h4>仕事にかける思いと心がけ</h4>
				<p>お客様目線を大切にして、常に接客において質の改善を求めております。<br />
					誠実かつ、お客様に満足いただけるサービスを提供致します。お客様の思い出もくみ取り、満足の行く価格をご提示させていただきます。 </p>
			</div>
            <!--<div class="staff_bx"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/staff03_shinjuku.png" alt="勝島 康博 鑑定士" />
                <p class="staff_name">田口 美帆 鑑定士</p>
                <h4>仕事における得意分野</h4>
                <p>ブランドバッグやジュエリーの鑑定はお任せください。
                </p>
                <h4>自身の強み</h4>
                <p>国内だけでなく海外にも販売ツールを構築しています。その為、査定金額には自信があります。まずはどんなお品物でもお持ちください。
                </p>
                <h4>仕事にかける思いと心がけ</h4>
                <p>笑顔・真心・正確性。<br>売却を考えているが、1人でお店に行くのは不安…そんな想いを抱いている方でも安心して利用頂ける場を提供します。</p>
            </div>-->
        </section>
        <section class="shopkaitori_jisseki">
        <h3>
            新宿店の買取実績
        </h3>
        <div class="topItem_cnt ">
            <div class="kaitori_box">
                <ul class="tab_menu">
                    <li><a href="#tab1">ブランド</a></li>
                    <li><a href="#tab2">金</a></li>
                    <li><a href="#tab3">宝石</a></li>
                    <li><a href="#tab4">時計</a></li>
                    <li><a href="#tab5">バック</a></li>
                </ul>
                <!--ブランド-->
                <div class="tab_cont" id="tab1">
                    <ul id="box-jisseki" class="list-unstyled clearfix">
                        <!-- barand1 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/001.jpg" alt=""></p>
                                <p class="itemName">オーデマピゲ　ロイヤルオークオフショアクロノ 26470OR.OO.A002CR.01 ゴールド K18PG</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：3,080,000円<br>
                                    <span class="blue">B社</span>：3,000,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">3,200,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>200,000円</p>
                            </div>
                        </li>

                        <!-- brand2 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/002.jpg" alt=""></p>
                                <p class="itemName">パテックフィリップ　コンプリケーテッド ウォッチ 5085/1A-001</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：1,470,000円<br>
                                    <span class="blue">B社</span>：1,380,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">1,650,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>270,000円</p>
                            </div>
                        </li>

                        <!-- brand3 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/003.jpg" alt=""></p>
                                <p class="itemName">パテックフィリップ　コンプリケーション 5130G-001 WG</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：2,630,000円<br>
                                    <span class="blue">B社</span>：2,320,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">3,300,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>800,000円</p>
                            </div>
                        </li>
                        <!-- brand4 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/004.jpg" alt=""></p>
                                <p class="itemName">パテックフィリップ　ワールドタイム 5130R-001</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：2,800,000円
                                    <br>
                                    <span class="blue">B社</span>：2,500,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">3,300,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>800,000円</p>
                            </div>
                        </li>

                        <!-- brand5 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/005.jpg" alt=""></p>
                                <p class="itemName">シャネル　ラムスキン　マトラッセ　二つ折り長財布</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：85,000円<br>
                                    <span class="blue">B社</span>：75,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">100,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>25,000円</p>
                            </div>
                        </li>
                        <!-- brand6 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/006.jpg" alt=""></p>
                                <p class="itemName">エルメス　ベアンスフレ　ブラック</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：180,000円<br>
                                    <span class="blue">B社</span>：157,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">210,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>53,000円</p>
                            </div>
                        </li>
                        <!-- brand7 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/007.jpg" alt=""></p>
                                <p class="itemName">エルメス　バーキン30　トリヨンクレマンス　マラカイト　SV金具</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：1,200,000円<br>
                                    <span class="blue">B社</span>：1,050,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">1,350,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>300,000円</p>
                            </div>
                        </li>
                        <!-- brand8 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/008.jpg" alt=""></p>
                                <p class="itemName">セリーヌ　ラゲージマイクロショッパー</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：210,000円<br>
                                    <span class="blue">B社</span>：180,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">240,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>60,000円</p>
                            </div>
                        </li>
                        <!-- brand9 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/009.jpg" alt=""></p>
                                <p class="itemName">ルイヴィトン　裏地ダミエ柄マッキントッシュジャケット</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：34,000円<br>
                                    <span class="blue">B社</span>：30,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">40,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>10,000円</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <!--ブランドここまで-->
                <!--金-->
                <div class="tab_cont" id="tab2">
                    <ul id="box-jisseki" class="list-unstyled clearfix">
                        <!-- gold1 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/001.jpg" alt=""></p>
                                <p class="itemName">K18　ダイヤ0.11ctリング</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：29,750円<br>
                                    <span class="blue">B社</span>：26,250円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">
                                    <!-- span class="small">120g</span -->35,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>8,750円</p>
                            </div>
                        </li>

                        <!-- gold2 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/002.jpg" alt=""></p>
                                <p class="itemName">K18　メレダイヤリング</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：38,250円<br>
                                    <span class="blue">B社</span>：33,750円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">45,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>11,250円</p>
                            </div>
                        </li>

                        <!-- gold3 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/003.jpg" alt=""></p>
                                <p class="itemName">K18/Pt900　メレダイヤリング</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：15,300円<br>
                                    <span class="blue">B社</span>：13,500円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">18,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>4,500円</p>
                            </div>
                        </li>

                        <!-- gold4 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/004.jpg" alt=""></p>
                                <p class="itemName">Pt900　メレダイヤリング</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：20,400円<br>
                                    <span class="blue">B社</span>：18,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">24,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>6,000円</p>
                            </div>
                        </li>

                        <!-- gold5 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/005.jpg" alt=""></p>
                                <p class="itemName">K18WG　テニスブレスレット</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：34,000円<br>
                                    <span class="blue">B社</span>：30,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">40,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>10,000円</p>
                            </div>
                        </li>

                        <!-- gold6 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/006.jpg" alt=""></p>
                                <p class="itemName">K18/K18WG　メレダイヤリング</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：25,500円<br>
                                    <span class="blue">B社</span>：22,500円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">30,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>7,500円</p>
                            </div>
                        </li>

                        <!-- gold7 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/007.jpg" alt=""></p>
                                <p class="itemName">K18ブレスレット</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：23,800円<br>
                                    <span class="blue">B社</span>：21,900円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">28,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>7,000円</p>
                            </div>
                        </li>
                        <!-- gold8 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/008.jpg" alt=""></p>
                                <p class="itemName">14WGブレスレット</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：52,700円<br>
                                    <span class="blue">B社</span>：46,500円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">62,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>1,5500円</p>
                            </div>
                        </li>
                        <!-- gold9 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/009.jpg" alt=""></p>
                                <p class="itemName">Pt850ブレスレット</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：27,200円<br>
                                    <span class="blue">B社</span>：24,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">32,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>8,000円</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <!--金ここまで-->
                <!--宝石-->
                <div class="tab_cont" id="tab3">

                    <ul id="box-jisseki" class="list-unstyled clearfix">
                        <!-- gem1 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/001.jpg" alt=""></p>
                                <p class="itemName">ダイヤルース</p>
                                <p class="itemdetail">カラット：1.003ct<br>カラー：E<br>クラリティ：VS-2<br>カット：Good<br>蛍光性：FAINT<br>形状：ラウンドブリリアント</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：380,000円<br>
                                    <span class="blue">B社</span>：355,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">450,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>115,000円</p>
                            </div>
                        </li>

                        <!-- gem2 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/002.jpg" alt=""></p>
                                <p class="itemName">Pt900 DR0.417ctリング</p>
                                <p class="itemdetail">カラー：F<br>クラリティ：SI-1<br>カット：VERY GOOD <br>蛍光性：FAINT<br>形状：ラウンドブリリアント<br>メレダイヤモンド0.7ct<br>地金：18金イエローゴールド　5ｇ</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：51,000円<br>
                                    <span class="blue">B社</span>：45,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">60,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>15,000円</p>
                            </div>
                        </li>

                        <!-- gem3 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/003.jpg" alt=""></p>
                                <p class="itemName">Pt900　DR0.25ctリング</p>
                                <p class="itemdetail">カラー：H<br>クラリティ:VS-1<br>カット：Good<br>蛍光性：MB<br>形状：ラウンドブリリアント<br></p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：58,000円<br>
                                    <span class="blue">B社</span>：51,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">68,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>17,000円</p>
                            </div>
                        </li>

                        <!-- gem4 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/004.jpg" alt=""></p>
                                <p class="itemName">K18　DR0.43ct　MD0.4ctネックレストップ</p>
                                <p class="itemdetail">カラー：I<br>クラリティ：VS-2<br>カット：Good<br>蛍光性：WB<br>形状：ラウンドブリリアント</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：60,000円<br>
                                    <span class="blue">B社</span>：52,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">70,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>18,000円</p>
                            </div>
                        </li>
                        <!-- gem5 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/005.jpg" alt=""></p>
                                <p class="itemName">ダイヤルース</p>
                                <p class="itemdetail">カラット：0.787ct<br>カラー：E<br>クラアリティ：VVS-2<br>カット：Good<br>蛍光性：FAINT<br>形状：ラウンドブリリアント</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：220,000円<br>
                                    <span class="blue">B社</span>：190,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">260,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>70,000円</p>
                            </div>
                        </li>
                        <!-- gem6 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/006.jpg" alt=""></p>
                                <p class="itemName">Pt950　MD0.326ct　0.203ct　0.150ctネックレス</p>
                                <p class="itemdetail">カラー：F<br>クラリティ：SI-2<br>カット：Good<br>蛍光性：FAINT<br>形状：ラウンドブリリアント</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：100,000円<br>
                                    <span class="blue">B社</span>：90,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">120,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>30,000円</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <!--宝石ここまで-->
                <!--時計-->
                <div class="tab_cont" id="tab4">

                    <ul id="box-jisseki" class="list-unstyled clearfix">

                        <!-- watch1 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/001.jpg" alt=""></p>
                                <p class="itemName">パテックフィリップ
                                    <br>カラトラバ 3923</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：850,000円
                                    <br>
                                    <span class="blue">B社</span>：750,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">1,000,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>250,000円</p>
                            </div>
                        </li>

                        <!-- watch2 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/002.jpg" alt=""></p>
                                <p class="itemName">パテックフィリップ
                                    <br>アクアノート 5065-1A</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：1,700,000円
                                    <br>
                                    <span class="blue">B社</span>：1,500,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">2,000,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>500,000円</p>
                            </div>
                        </li>

                        <!-- watch3 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/003.jpg" alt=""></p>
                                <p class="itemName">オーデマピゲ
                                    <br>ロイヤルオーク・オフショア 26170ST</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：1,487,500円
                                    <br>
                                    <span class="blue">B社</span>：1,312,500円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">1,750,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>437,500円</p>
                            </div>
                        </li>

                        <!-- watch4 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/004.jpg" alt=""></p>
                                <p class="itemName">ROLEX
                                    <br>サブマリーナ 116610LV ランダム品番</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：1,105,000円
                                    <br>
                                    <span class="blue">B社</span>：975,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">1,300,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>325,000円</p>
                            </div>
                        </li>
                        <!-- watch5 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/005.jpg" alt=""></p>
                                <p class="itemName">ROLEX
                                    <br>116505 ランダム品番 コスモグラフ</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：2,422,500円
                                    <br>
                                    <span class="blue">B社</span>：2,137,500円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">2,850,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>712,500円</p>
                            </div>
                        </li>
                        <!-- watch6 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/006.jpg" alt=""></p>
                                <p class="itemName">ブライトリング
                                    <br>クロノマット44</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：289,000円
                                    <br>
                                    <span class="blue">B社</span>：255,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">340,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>85,000円</p>
                            </div>
                        </li>
                        <!-- watch7 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/007.jpg" alt=""></p>
                                <p class="itemName">パネライ
                                    <br>ラジオミール 1940</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：510,000円
                                    <br>
                                    <span class="blue">B社</span>：450,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">600,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>150,000円</p>
                            </div>
                        </li>
                        <!-- watch8 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/008.jpg" alt=""></p>
                                <p class="itemName">ボールウォッチ
                                    <br>ストークマン ストームチェイサープロ CM3090C</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：110,500円
                                    <br>
                                    <span class="blue">B社</span>：97,500円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">130,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>32,500円</p>
                            </div>
                        </li>
                        <!-- watch9 -->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/009.jpg" alt=""></p>
                                <p class="itemName">ブレゲ
                                    <br>クラシックツインバレル 5907BB12984</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：595,000円
                                    <br>
                                    <span class="blue">B社</span>：525,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">700,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>175,000円</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <!--時計ここまで-->
                <!--バック-->
                <div class="tab_cont" id="tab5">

                    <ul id="box-jisseki" class="list-unstyled clearfix">

                        <!-- bag1-->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/001.jpg" alt=""></p>
                                <p class="itemName">エルメス<br />エブリンⅢ　トリヨンクレマンス</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：230,000円
                                    <br>
                                    <span class="blue">B社</span>：200,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">270,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>70,000円</p>
                            </div>
                        </li>

                        <!-- bag2-->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/002.jpg" alt=""></p>
                                <p class="itemName">プラダ<br />シティトート2WAYバッグ</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：110,000円
                                    <br>
                                    <span class="blue">B社</span>：95,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">132,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>37,000円</p>
                            </div>
                        </li>

                        <!-- bag3-->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/003.jpg" alt=""></p>
                                <p class="itemName">バンブーデイリー2WAYバッグ</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：100,000円
                                    <br>
                                    <span class="blue">B社</span>：90,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">120,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>30,000円</p>
                            </div>
                        </li>

                        <!-- bag4-->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/004.jpg" alt=""></p>
                                <p class="itemName">LOUIS VUITTON
                                    <br>モノグラムモンスリGM</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：110,000円
                                    <br>
                                    <span class="blue">B社</span>：90,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">120,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>30,000円</p>
                            </div>
                        </li>

                        <!-- bag5-->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/005.jpg" alt=""></p>
                                <p class="itemName">HERMES
                                    <br>バーキン30</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：1,360,000円
                                    <br>
                                    <span class="blue">B社</span>：1,200,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">1,600,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>400,000円</p>
                            </div>
                        </li>
                        <!-- bag6-->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/006.jpg" alt=""></p>
                                <p class="itemName">CHANEL
                                    <br>マトラッセダブルフラップダブルチェーンショルダーバッグ</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：220,000円
                                    <br>
                                    <span class="blue">B社</span>：195,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">260,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>65,000円</p>
                            </div>
                        </li>

                        <!-- bag7-->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/007.jpg" alt=""></p>
                                <p class="itemName">LOUIS VUITTON
                                    <br>ダミエ ネヴァーフルMM</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：110,000円
                                    <br>
                                    <span class="blue">B社</span>：96,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">130,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>34,000円</p>
                            </div>
                        </li>

                        <!-- bag8-->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/008.jpg" alt=""></p>
                                <p class="itemName">CELINE
                                    <br>ラゲージマイクロショッパー</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：200,000円
                                    <br>
                                    <span class="blue">B社</span>：180,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">240,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>60,000円</p>
                            </div>
                        </li>

                        <!-- bag9-->
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/009.jpg" alt=""></p>
                                <p class="itemName">ロエベ
                                    <br>アマソナ23</p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：75,000円
                                    <br>
                                    <span class="blue">B社</span>：68,000円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">90,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>22,000円</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <!--バックここま-->
            </div>
        </div>
    </section>

       <!--<section class="kaitori_voice fix">
            <h3>お客様の声</h3>

            <ul>
                <li>
                    <p class="kaitori_tab tentou_tab">店頭買取</p>
                    <h4>パネライ　時計　</h4>
                    <p class="voice_txt">新しい時計が気になり、今まで愛用していたパネライの腕時計を買取してらもらうことにしました。<br /> 手元に保管しておくという手もあったのですが、やはり使わずに時計を手元に置いておくよりは、買取資金の足しにした方がいいのかなと思ったので・・・。
                        <br /> 通勤するときに、ブランドリバリューの近くを通るので、通勤ついでにブランドリバリューで試しで査定をしてもらうことにしたのですが、本当にこちらを利用してよかったです。
                        <br /> 私もブランドに詳しい自信がありましたが、査定士さんはそれ以上の知識があり、きちんとブランドの価値をわかってくださいました。本当に買取額に対して満足することができただけではなく、安心して買取手続きをすることができました。
                        <br /> やはり買取査定はブランドリバリューのように、きちんとブランド知識があるところでお願いしないと満足できないですよね。
                    </p>
                </li>

                <li>
                    <p class="kaitori_tab tentou_tab">店頭買取</p>
                    <h4>シャネル　バッグ　</h4>
                    <p class="voice_txt">シャネルのバッグの査定をしてもらいました。<br /> 店頭買取をしてもらったのですが、実はブランドリバリューに出向く前に他社比較もしようと思い、他の買取店でも査定をしてもらっていたんです。 ですが、他社比較の結果、断然！ブランドリバリューは高値の査定を出してくれました！
                        <br /> ブランドリバリューは、きちんと店頭買取をすることができる、いわば実際の店舗が存在するブランド買取店でもあるので、この安心さも兼ねそろえているのが個人的に安心でした。
                        <br /> また、他にもブランドバッグで買取をしてほしいものが出てきたら、絶対にブランドリバリューを利用するつもりです。 もう他社比較はしません！笑
                    </p>
                </li>
                <li>
                    <p class="kaitori_tab tentou_tab">店頭買取</p>
                    <h4>ボッテガヴェネタ　財布</h4>
                    <p class="voice_txt">好みのブランドから、新作のお財布が販売されたので、今使っているボッテガヴェネタの財布を買取してもらおうと思い、ブランドリバリューを利用しました。<br /> 店舗の位置をチェックしてみると、銀座駅からもめっちゃ近かったので、店頭買取を利用しての買取です。
                        <br /> 今回は、ボッテガヴェネタの財布１つだけの買取だったので、このお財布１つだけで、買取査定を依頼するのは何だか申し訳ないなと思ったりもしたのですが、担当の方もとても丁寧で気持ちのいい対応をとってくださったので、本当によかったです。
                        <br /> 査定額も満足することができたので、機会があればまた利用したいと思っています。
                    </p>
                </li>


            </ul>


        </section>-->
        <section>
            <h3 class="mt50">新宿店　立地へのこだわり</h3>
            <p>新宿。それは東京都庁を中心とする高層ビル街と、日本一の歓楽街・歌舞伎町があり、昼夜ともに圧倒的な存在感を示す東京を代表する都市のひとつ。
                <br /><br /> ブランドリバリューは2016年銀座で立ち上がり2017年渋谷、そして2018年新宿へ進出いたします。

                <br /><br /> ブランドリバリュー新宿店は伊勢丹・マルイのある新宿3丁目の交差点から徒歩15秒程。マルイの真裏に店舗を構えております。

                <br /> 新天地でもブランドリバリューの特徴である「高価買取」・「最高級のサービス」を提供してまいります。
                <br /><br />ブランド品から時計等使わなくなったものがあれば、都内屈指の熟練バイヤーがご相談させていただきます。<br /> ご売却のことから体調の悩みまで何でも相談にのります。ブランドリバリュー新宿店スタッフ一同心よりお待ちしております。
                <br />
                <br /><br /> 新宿でジュエリー・時計・バッグの買取店をお探しの皆様、ブランド買取なら高額査定のBRANDREVALUE(ブランドリバリュー)へ。><br />
            </p>


        </section>
        <div class="takuhai_cvbox tentou">
            <p class="cv_tl">ブランドリバリュー新宿店で<br>店頭買取予約をする</p>
            <div class="custom_tel takuhai_tel">
                <a href="tel:0120-970-060">
                    <div class="tel_wrap">
                        <div class="telbox01"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/customer/telico.png" alt="お申し込みはこちら">
                            <p>お電話からでもお申込み可能！<br>ご不明な点は、お気軽にお問合せ下さい。 </p>
                        </div>
                        <div class="telbox02"> <span class="small_tx">【受付時間】11:00 ~ 21:00</span><span class="ted_tx"> 年中無休</span>
                            <p class="fts25">0120-970-060</p>
                        </div>
                    </div>
                </a>
            </div>
            <p><a href="<?php echo home_url('purchase/visit-form'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/tentou_cv.png" alt="店頭買取お申し込み"></a></p>
        </div>
    </div>

    <?php

  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');



  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
