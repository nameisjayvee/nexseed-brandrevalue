<?php
get_header(); ?>

    <div class="mv_area ">
        <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/tentou_mv.png " alt="その場で即お支払い店頭買取">
    </div>
    <div class="new_content_wrap">
        <ul class="tentou_bnr">
            <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/point01.png" alt="タクシー代お支払い"></li>
            <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/point02.png" alt="タクシー代お支払い"></li>
        </ul>
        <h2>ブランドリバリュー店舗ご案内</h2>
        <p>ブランドリバリューは、全店舗駅から徒歩圏内の好立地に店舗を構えております。 お客様が安心して快適に過ごせるように内装にもこだわり、お客様のプライバシーを配慮した個室ブースをご用意しております。
        </p>
        <p class="arw_ttl"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/arw_ttl.png" alt="タクシー代お支払い"></p>
        <div class="takuhai_cvbox tentou">
            <div class="custom_tel takuhai_tel">
                <a href="tel:0120-970-060">
                    <div class="tel_wrap">
                        <div class="telbox01"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/customer/telico.png" alt="お申し込みはこちら">
                            <p>お電話からでもお申込み可能！<br>ご不明な点は、お気軽にお問合せ下さい。 </p>
                        </div>
                        <div class="telbox02"> <span class="small_tx">【受付時間】11:00 ~ 21:00</span><span class="ted_tx"> 年中無休</span>
                            <p>0120-970-060</p>
                        </div>
                    </div>
                </a>
            </div>
            <p><a href="<?php echo home_url('purchase/visit-form'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/tentou_cv.png" alt="店頭買取お申し込み"></a></p>
        </div>
        <h2>買取の際にご用意いただきもの</h2>
        <p><span class="ft_bold">＜ 本人確認書類 ＞</span> 運転免許証　パスポート　住民基本台帳カード
            <br> 健康保険証　在留カード　個人番号カード etc…
            <span class="ft_small">※未成年の方に関しましては、保護者からの同意書もしくは同伴が必要です。</span>
        </p>

        <section class="tentou_chara">
            <div class="charabox01">
                <h3><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/tentou_ttl01.png" alt="高額商材でも即金対応！業界最高峰の買取金額！"></h3>
                <p>「即日、現金が手に入る」「スタッフの顔を見て査定を受けられる」－店頭買取にはさまざななメリットがありますが、 一番大きなメリットは、1,000万円以上を超えるような高額なお品物や一般的に査定が難しいと言われているアンティーク時計などもブランドリバリューでは、即日現金にてご対応が可能です。ブランドリバリューでは、徹底的なコストカットにより実現した「業界最高値」の査定を目指します。</p>
            </div>
            <div class="charabox02">
                <h3><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/tentou_ttl02.png" alt="全店駅から徒歩5分圏内だから便利！"></h3>
                <p>東京都内に4店舗（銀座・新宿・渋谷・神宮前）、大阪に1店舗の合計5店舗を展開しています。ブランドリバリューは、全店駅から徒歩5分圏内の好立地に店舗を構えております。また、ご来店いただいたお客様がストレスなくご利用いただけるように「サロンのような内装」というコンセプトのもと店舗作りをしております。もし店舗の場所がわからない場合は0120-970-060までお問い合わせください。道順を説明いたします。入店後は、研修カリキュラムを修了した査定士が丁寧懇切にご要望にお応えいたします。
                </p>
            </div>
            <div class="charabox03">
                <h3><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/tentou_ttl03.png" alt="最速査定！10分～15分でお取引完了！"></h3>
                <p>ブランドリバリューは、「ホスピタリティ」＝「おもてなし」の心を大切にお客様のお時間を無駄にしない迅速な査定を心がけております。最短で10分～15分で完了します。
                </p>
            </div>
        </section>
        <section class="tentou_flow_box">
            <ul class="flow_list">
                <li>
                    <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/flow01.png" alt="お申し込み">
                    <p>店舗買取にご来店の際は事前にご連絡を頂ければ幸いです。お待たせすることなく迅速にお客様の買取希望のブランド品を査定させていただきます。</p>
                </li>
                <li>
                    <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/flow02.png" alt="梱包して発送">
                    <p>店舗買取にご来店の際は事前にご連絡を頂ければ幸いです。 お待たせすることなく迅速にお客様の買取希望のブランド品を査定させていただきます。
                    </p>
                </li>
                <li>
                    <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/flow03.png" alt="査定結果のご連絡">
                    <p>熟練された経験豊富な鑑定士がご安心できるようお客様のブランドアイテムを目の前で鑑定させていただきます。</p>
                </li>
                <li>
                    <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/flow04.png" alt="同意後、即入金">
                    <p>買取成立の場合は、たとえ数百万円単位、一千万円でもその場で店舗買取させていだきます。</p>
                </li>
            </ul>
        </section>
        <p class="arw_ttl"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/arw_ttl.png" alt="タクシー代お支払い"></p>
        <div class="takuhai_cvbox tentou">
            <div class="custom_tel takuhai_tel">
                <a href="tel:0120-970-060">
                    <div class="tel_wrap">
                        <div class="telbox01"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/customer/telico.png" alt="お申し込みはこちら">
                            <p>お電話からでもお申込み可能！<br>ご不明な点は、お気軽にお問合せ下さい。 </p>
                        </div>
                        <div class="telbox02"> <span class="small_tx">【受付時間】11:00 ~ 21:00</span><span class="ted_tx"> 年中無休</span>
                            <p>0120-970-060</p>
                        </div>
                    </div>
                </a>
            </div>
            <p><a href="<?php echo home_url('purchase/visit-form'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/tentou_cv.png" alt="店頭買取お申し込み"></a></p>
        </div>
         <?php
  // お問い合わせ
  get_template_part('_action_tentou');
  ?>

        <section class="reason_select">
            <h2><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/reason_ttl01.png" alt="ブランドリバリューが選ばれる理由"></h2>
            <ul>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/reason01.png" alt="最高峰の高額買取"></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/reason02.png" alt="超スピード買取査定"></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/reason03.png" alt="即現金でお支払い"></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/reason04.png" alt="送料・査定料0円"></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/reason05.png" alt="1万点以上の取扱ブランド数"></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/reason06.png" alt="銀座駅から徒歩30秒"></li>
            </ul>
            <h2><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/reason_ttl02.png" alt="ブランドリバリューが選ばれる理由"></h2>
            <ul>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/reason_link01.png" alt="最高峰の高額買取"></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/reason_link02.png" alt="超スピード買取査定"></li>
            </ul>
        </section>
        <section class="takuhai_possible">
            <h2 class="takihai_ttl">お買取りできるもの</h2>
            <ul class="possible_list01">
                <li>
                    <p>バッグ</p>
                    <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible01.png" alt="バッグ">
                </li>
                <li>
                    <p>時計</p>
                    <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible02.png" alt="バッグ">
                </li>
                <li>
                    <p>ダイヤ</p>
                    <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible03.png" alt="バッグ">
                </li>
                <li>
                    <p>宝石</p>
                    <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible04.png" alt="バッグ">
                </li>
                <li>
                    <p>ブランドジュエリー</p>
                    <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible05.png" alt="バッグ">
                </li>
                <li>
                    <p>財布</p>
                    <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible06.png" alt="バッグ">
                </li>
                <li>
                    <p>靴</p>
                    <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible07.png" alt="バッグ">
                </li>
                <li>
                    <p>毛皮</p>
                    <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible08.png" alt="バッグ">
                </li>
                <li>
                    <p>ブランドアパレル</p>
                    <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible09.png" alt="バッグ">
                </li>
                <li>
                    <p>お酒</p>
                    <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible10.png" alt="バッグ">
                </li>
            </ul>
            <h3><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/takuhai_tx.png" alt="「傷」や「汚れ」があってもお買い取りさせて下さい！
どんな状態であっても必ずお買取りさせて頂きます。
"></h3>
            <ul class="possible_list02">
                <li>

                    <h4>ボロボロになったバッグ</h4>
                    <div>
                        <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/state_ex01.png" alt="ボロボロになったバッグ">
                        <p>・シミ / 汚れ<br>・破れ / 日焼け</p>
                    </div>
                </li>
                <li>

                    <h4>壊れてしまった時計</h4>
                    <div>
                        <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/state_ex02.png" alt="壊れてしまった時計">
                        <p>・不動<br>・パーツ破損</p>
                    </div>
                </li>
                <li>

                    <h4>破損した貴金属、宝石</h4>
                    <div>
                        <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/state_ex03.png" alt="破損した貴金属、宝石">
                        <p>・千切れてしまった<br>・石が取れてしまった<br>・イニシャル入り</p>
                    </div>
                </li>

                <li>

                    <h4>ルース(裸石)</h4>
                    <div>
                        <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/state_ex04.png" alt="破損した貴金属、宝石">
                        <p>・枠から外れてしまった<br>・鑑定書なし
                        </p>
                    </div>
                </li>


            </ul>



        </section>

    </div>
        <section class="kaitori_voice">
            <h2 class="ttl_edit01">店頭買取をご利用いただいたお客様の声</h2>
            <ul>
                <li>
                    <p class="kaitori_tab tentou_tab">店頭買取</p>
                    <h4>パネライ　時計　</h4>
                    <p class="voice_txt">新しい時計が気になり、今まで愛用していたパネライの腕時計を買取してらもらうことにしました。<br /> 手元に保管しておくという手もあったのですが、やはり使わずに時計を手元に置いておくよりは、買取資金の足しにした方がいいのかなと思ったので・・・。
                        <br /> 通勤するときに、ブランドリバリューの近くを通るので、通勤ついでにブランドリバリューで試しで査定をしてもらうことにしたのですが、本当にこちらを利用してよかったです。
                        <br /> 私もブランドに詳しい自信がありましたが、査定士さんはそれ以上の知識があり、きちんとブランドの価値をわかってくださいました。本当に買取額に対して満足することができただけではなく、安心して買取手続きをすることができました。
                        <br /> やはり買取査定はブランドリバリューのように、きちんとブランド知識があるところでお願いしないと満足できないですよね。
                    </p>
                </li>

                <li>
                    <p class="kaitori_tab tentou_tab">店頭買取</p>
                    <h4>シャネル　バッグ　</h4>
                    <p class="voice_txt">シャネルのバッグの査定をしてもらいました。<br /> 店頭買取をしてもらったのですが、実はブランドリバリューに出向く前に他社比較もしようと思い、他の買取店でも査定をしてもらっていたんです。 ですが、他社比較の結果、断然！ブランドリバリューは高値の査定を出してくれました！
                        <br /> ブランドリバリューは、きちんと店頭買取をすることができる、いわば実際の店舗が存在するブランド買取店でもあるので、この安心さも兼ねそろえているのが個人的に安心でした。
                        <br /> また、他にもブランドバッグで買取をしてほしいものが出てきたら、絶対にブランドリバリューを利用するつもりです。 もう他社比較はしません！笑
                    </p>
                </li>
                <li>
                    <p class="kaitori_tab tentou_tab">店頭買取</p>
                    <h4>ボッテガヴェネタ　財布</h4>
                    <p class="voice_txt">好みのブランドから、新作のお財布が販売されたので、今使っているボッテガヴェネタの財布を買取してもらおうと思い、ブランドリバリューを利用しました。<br /> 店舗の位置をチェックしてみると、銀座駅からもめっちゃ近かったので、店頭買取を利用しての買取です。
                        <br /> 今回は、ボッテガヴェネタの財布１つだけの買取だったので、このお財布１つだけで、買取査定を依頼するのは何だか申し訳ないなと思ったりもしたのですが、担当の方もとても丁寧で気持ちのいい対応をとってくださったので、本当によかったです。
                        <br /> 査定額も満足することができたので、機会があればまた利用したいと思っています。
                    </p>
                </li>


            </ul>


        </section>
        <?php

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
