<?php
get_header(); ?>

<div class="mv_area ">
<img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kv-contact.png " alt="BRAMD REVALUE ">
</div>

<section id="mailform">
<div class="contactform">

          <?php echo do_shortcode('[mwform_formkey key="23209"]'); ?>

</div>
</section>
<div class="textarea">
※メールでのお問い合わせ・査定は、原則的に１営業日以内にご連絡を差し上げるよう努めております。
多くのお問い合わせを頂戴している場合や店舗の混雑状況により、ご回答が遅れる可能性がございます。<br>
当店から返信がない場合には、お手数ではございますが、電話またはＬＩＮＥにてお問い合わせくださいませ。<br><br>
</div>

<script src="http://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script type="text/javascript">
(function () {

    "use strict";

    /**
     * Pardot form handler assist script.
     * Copyright (c) 2018 toBe marketing, inc.
     * Released under the MIT license
     * http://opensource.org/licenses/mit-license.php
     */

    // == 設定項目ここから ==================================================================== //

    /*
     * ＠データ取得方式
     */
    var useGetType = 1;

    /*
     * ＠フォームハンドラーエンドポイントURL
     */
    var pardotFHUrl="//go.pardot.com/l/436102/2018-01-18/dgs717";

    /*
     * ＠フォームのセレクタ名
     */
    var parentFormName = '#mw_wp_form_mw-wp-form-23209 > form';

    /*
     * ＠送信ボタンのセレクタ名
     */
    var submitButton = 'input[type=submit][name=confirm]';

    /*
     * ＠項目マッピング
     */

    var defaultFormJson=[
    { "tag_name": "sei",      "x_target": 'sei'      },
    { "tag_name": "mei",      "x_target": 'mei'      },
    { "tag_name": "kana-sei", "x_target": 'kana-sei' },
    { "tag_name": "kana-mei", "x_target": 'kana-mei' },
    { "tag_name": "email",    "x_target": 'email'    },
    { "tag_name": "tel",      "x_target": 'tel'      },
    { "tag_name": "subject",  "x_target": 'subject'  },
    { "tag_name": "message",  "x_target": 'message'  }
    ];

    // == 設定項目ここまで ==================================================================== //

    // 区切り文字設定
    var separateString = ',';

    var iframeFormData = '';
    for (var i in defaultFormJson) {
        iframeFormData = iframeFormData + '<input id="pd' + defaultFormJson[i]['tag_name'] + '" type="text" name="' + defaultFormJson[i]['tag_name'] + '"/>';
    }

    var iframeHeadSrc =
        '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';

    var iframeSrcDoc =
        '<form id="shadowForm" action="' + pardotFHUrl + '" method="post" accept-charset=\'UTF-8\'>' +
        iframeFormData +
        '<input id="shadowSubmit" type="submit" value="send" onclick="document.charset=\'UTF-8\';"/>' +
        '</form>';

    var shadowSubmited = false;
    var isError = false;
    var pdHiddenName = 'pdHiddenFrame';

    /**
     * エスケープ処理
     * @param val
     */
    function escapeSelectorString(val) {
        return val.replace(/[ !"#$%&'()*+,.\/:;<=>?@\[\\\]^`{|}~]/g, "\\$&");
    }

    $(function () {

        $(submitButton).click(function () {
            // Submitボタンを無効にする。
            $(submitButton).prop("disabled", true);
        });

        $('#' + pdHiddenName).remove();

        $('<iframe>', {
            id: pdHiddenName
        })
            .css({
                display: 'none'
            })
            .on('load', function () {

                if (shadowSubmited) {
                    if (!isError) {

                        shadowSubmited = false;

                        // 送信ボタンを押せる状態にする。
                        $(submitButton).prop("disabled", false);

                        // 親フォームを送信
                        $(parentFormName).submit();
                    }
                } else {
                    $('#' + pdHiddenName).contents().find('head').prop('innerHTML', iframeHeadSrc);
                    $('#' + pdHiddenName).contents().find('body').prop('innerHTML', iframeSrcDoc);

                    $(submitButton).click(function () {
                        shadowSubmited = true;
                        try {
                            for (var j in defaultFormJson) {
                                var tmpData = '';

                                // NANE値取得形式
                                if (useGetType === 1) {
                                    $(parentFormName + ' [name="' + escapeSelectorString(defaultFormJson[j]['x_target']) + '"]').each(function () {

                                        //checkbox,radioの場合、未選択項目は送信除外
                                        if (["checkbox", "radio"].indexOf($(this).prop("type")) >= 0) {
                                            if ($(this).prop("checked") == false) {
                                                return true;
                                            }

                                        }

                                        if (tmpData !== '') {
                                            tmpData += separateString;
                                        }

                                        // 取得タイプ 1 or Null(default) :val()方式, 2:text()形式
                                        if (defaultFormJson[j]['x_type'] === 2) {
                                            tmpData += $(this).text().trim();
                                        } else {
                                            tmpData += $(this).val().trim();
                                        }
                                    });
                                }

                                // セレクタ取得形式
                                if (useGetType === 2) {
                                    $(defaultFormJson[j]['x_target']).each(function () {
                                        if (tmpData !== '') {
                                            tmpData += separateString;
                                        }

                                        // 取得タイプ 1 or Null(default) :val()方式, 2:text()形式
                                        if (defaultFormJson[j]['x_type'] === 2) {
                                            tmpRegexData = $(this).text().trim();
                                        } else {
                                            tmpRegexData = $(this).val().trim();
                                        }
                                    });
                                }

                                $('#' + pdHiddenName).contents().find('#pd' + escapeSelectorString(defaultFormJson[j]['tag_name'])).val(tmpData);

                            }

                            $('#' + pdHiddenName).contents().find('#shadowForm').submit();

                        } catch (e) {
                            isError = true;

                            $(submitButton).prop("disabled", false);

                            $(parentFormName).submit();
                            shadowSubmited = false;
                        }
                        return false;
                    });
                }
            })
            .appendTo('body');
    });
})();
</script>

<?php
  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
