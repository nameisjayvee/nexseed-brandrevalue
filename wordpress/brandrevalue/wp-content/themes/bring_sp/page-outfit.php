<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  '001.jpg::::ルイヴィトン　リバーシブルブルゾン::57,000::55,000::61,000::6,000',
  '002.jpg::::モンクレール　マヤ　迷彩柄ダウンジャケット::120,000::117,000::126,000::9,000',
  '003.jpg::::シャネル　ツイードジャケット::118,000::115,000::122,000::7,000',
  '004.jpg::::ドルチェ＆ガッバーナ　スタッズ付きダメージデニムパンツ::18,000::15,000::21,000::6,000',
  '005.jpg::::グッチ　ムートンジャケット::46,000::40,000::50,000::10,000',
  '006.jpg::::エルメス　レザージャケット::94,000::90,000::100,000::10,000',
  '007.jpg::::リアルマッコイ　ムートンジャケット::80,000::75,000::86,000::11,000',
  '008.jpg::::クロムハーツ　デニムジャケット::54,000::50,000::58,000::8,000',
  '009.jpg::::シャネル　セットアップ::90,000::87,000::93,000::6,000',
  '010.jpg::::エルメス　エルミエンヌ　ダウンコート::90,000::86,000::92,000::6,000',
  '011.jpg::::エルメス　メンズラムスキンレザージャケット::74,000::72,000::80,000::8,000',
  '012.jpg::::シャネル　ツイードジャケット::58,000::55,000::61,000::6,000',
);

get_header(); ?>

    <div id="primary" class="cat-page content-area">
        <div class="mv_area ">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/lp_main/cat_outfit_main.jpg" alt="あなたの洋服お売りください">
        </div>
    <p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
    <p class="main_bottom">ブランドリバリューは洋服・毛皮の買取実績多数！</p>
    <div id="lp_head" class="outfit_ttl">
    <div>
    <p>銀座で最高水準の査定価格・サービス品質をご体験ください。</p>
    <h2>あなたの毛皮・洋服<br />どんな物でもお売り下さい！！</h2>
    </div>
    </div>
 <div class="lp_main">


            <section id="hikaku" class="watch_hikaku">
                <p class="hikaku_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/outfit_hikaku.png"></p>
            </section>

            <!--<section id=" lp_history ">
                <h3 class="obi_tl">有名ブランドの歴史</h3>
                <ul class="his_btn">
                    <li><a rel="leanModal" href="#div_point01"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/btn_history01.jpg" alt="ロレックス"></a></li>
                    <li><a rel="leanModal" href="#div_point02"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/btn_history02.jpg" alt="パテックフィリップ"></a></li>
                    <li><a rel="leanModal" href="#div_point03"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/btn_history03.jpg" alt="ウブロ"></a></li>
                    <li><a rel="leanModal" href="#div_point04"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/btn_history04.jpg" alt="オーデマピゲ"></a></li>
                    <li><a rel="leanModal" href="#div_point05"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/btn_history05.jpg" alt="ブライトリング"></a></li>
                    <li><a rel="leanModal" href="#div_point06"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/btn_history06.jpg" alt="ブレゲ"></a></li>
                </ul>

                <div id="div_point01" class="div_point">
                    <h5>ROLEX（ロレックス）とは</h5>
                    <p>ROLEX（ロレックス）は1905年にウィルスドルフ＆デイビスという名前の時計商社として設立され
                        <br> 幾度と名前の変更を経て、現在のROLEX（ロレックス）となりました。
                    </p>
                    <p>ROLEX（ロレックス）と聞けば王冠マークをすぐに思い浮かべられるほど誰もが知る有名高級時計です。
                        <br> それほどの知名度を築くに至ったのもROLEX（ロレックス）が独自に開発をし、現在では多くの
                        <br> 自動巻き時計に採用されている数々の特許技術でしょう。
                    </p>
                    <p>まずはパーペチュアル機構です。それ以前にも自動巻きの機構は存在しましたが、ROLEX（ロレックス）の
                        <br> 開発したパーペチュアル機構（全回転機構）により自動巻き時計は完成となり、ROLEX（ロレックス）は
                        <br> ブランド自動巻き時計の第一号となったのです。
                    </p>
                    <p>その後もオイスターケースという防水仕様のケースの開発、デイトジャストと呼ばれる日付が0時ちょうどで
                        <br> 切り替わる機構など、数々の革新的な技術の開発を行ってきました。
                    </p>
                    <p>また、機構の開発だけに留まらず時計そのものとしてのクオリティーも細部に至るまでこだわり作られている
                        <br> こともROLEX（ロレックス）が現在の地位を獲得した要因でしょう。
                    </p>
                    <p>10年に1度はオーバーホールが必要ではありますが、きちんとしたメンテナンスを行っていれば正に一生ものと
                        <br> 呼ぶにふさわしい時計です。
                    </p>
                    <p>有名なモデルとして、デイトジャスト、サブマリーナ、デイトナ、GMTマスター、エクスプローラーⅠ、 デイトジャスト、ヨットマスター、ターノグラフ、エアキング、シードュエラ、ミルガウスなど
                    </p>
                </div>

                <div id="div_point02" class="div_point">
                    <h5>PATEK PHILIPPE（パテックフィリップ）とは</h5>
                    <p>PATEK PHILIPPE（パテックフィリップ）は誰もが知るブランド時計では御座いません。
                        <br> しかし、世界三大高級時計として世界各国で愛されている最高級の時計ブランドです。
                    </p>
                    <p>PATEK PHILIPPE（パテックフィリップ）はカルティエやティファニー、ギュブラン、宝石店向けに製品を
                        <br> 納品していたこともあり、世界一高級な時計を販売するマニュファクチュールとして知られています。
                    </p>
                    <p>PATEK PHILIPPE（パテックフィリップ）はどんなに古い商品でも自社の物であれば修理可能と永久修理保証と
                        <br> 宣伝している為、PATEK PHILIPPE（パテックフィリップ）の時計は一生ものというイメージを確立しております。</p>
                    <p>有名なモデルとして、カラトラバ、コンプリケーテッド、グランドコンプリケオーション、アクアノート、ゴンドーロ、 トゥエンティー・フォー、ノーチラス、ゴールデンイリプスなど
                    </p>
                </div>

                <div id="div_point03" class="div_point">
                    <h5>HUBLOT（ウブロ）とは</h5>
                    <p>HUBLOT（ウブロ）は1980年に誕生し、独特なベゼル、ケースデザインやベルトにラバーを採用するなど
                        <br> 独自の世界観で多くの時計ファンに衝撃を与えた高級時計ブランドです。
                    </p>
                    <p>ユニークなデザインの為、クラシックなものと違い派手さ、華やかさから多くのスポーツ選手や芸能人が
                        <br> 愛用しております。
                    </p>
                    <p>有名なモデルにクラシックフュージョン、ビッグバン、ビッグバンキング、キングパワーなど</p>
                </div>

                <div id="div_point04" class="div_point">
                    <h5>AUDEMARS　PIGUET（オーデマピゲ）とは</h5>
                    <p>AUDEMARS　PIGUET（オーデマピゲ）は1875年ジュール＝ルイ・オーデマと
                        <br> 友人のエドワール＝オーギュスト・ピゲにより設立された高級時計メーカーです。
                    </p>
                    <p>お互いの技術力の高さから複雑時計の開発を始め、1889年に第10回パリ万博で
                        <br> スプリットセコンドグラフや永久カレンダーなどを装備する、グランド・コンプリカシオン
                        <br> 懐中時計を発表。1892年に世界で初めてのミニッツリピーターを搭載した腕時計の
                        <br> 独創的な製品を発表し「複雑度系のオーデマピゲ」として名声を獲得しております。
                    </p>
                    <p>現在でも、創業者一族が経営を引き継いでおり高い情熱と技術が継承されております。</p>
                    <p>厚さ1.62mmの世界最薄手巻きムーブメントや、1972年にはラグジュアリースポーツウォッチと
                        <br> いつ新カテゴリーを築いたロイヤルオークを発表し、誕生から40年をヘタ現在も高い人気を誇ります。
                    </p>
                    <p>有名なモデルにロイヤルオーク オフショア、ロイヤルオーク、ジュール・オーデマ、エドワール・ピゲ、ミレネリーなど</p>
                </div>

                <div id="div_point05" class="div_point">
                    <h5>BREITLING（ブライトリング）とは</h5>
                    <p>BREITLING（ブライトリング）の歴史はクロノグラフの進化の歴史といっても過言ではありません。</p>
                    <p>創業者のレオン・ブライトリングは計測機器の開発に特化しており、各分野のプロ用クロノグラフを
                        <br> 製造。息子ガストンが会社を継ぎ、1915年に独立した専用プッシュボタンを備えた世界初の腕時計型
                        <br> クロノグラフの開発に成功。1934年にリセットボタンを開発し、現在のクロノグラフの基礎を築いた。
                    </p>
                    <p>そして、1936年にイギリス空軍とコックピットクロックの供給契約を皮切りに航空業界で勢力を拡大。
                        <br> 1952年に航空用回転計算尺を搭載した「ナビタイマー」によって、航空時計ナンバーワンブランドの
                        <br> 地位を確立した。
                    </p>
                    <p>有名なモデルにクロノマット、スーパーオーシャン、トランスオーシャン、ナビタイマー、ウィンドライダー、スカイレーサーなど</p>
                </div>

                <div id="div_point06" class="div_point">
                    <h5>BREGUET（ブレゲ）とは</h5>
                    <p>BREGUET（ブレゲ）は1775年、パリにオープンした工房から始まった。</p>
                    <p>1780年にはブラビアン＝ルイ・ブレゲが自動巻き機構を開発。衝撃吸収装置、ブレゲヒゲゼンマイ、
                        <br> トゥールビヨン機構、スプリットセコンドクロノグラフなど、次々と画期的な機構を開発、ブレゲ針や
                        <br> ブレゲ数字、文字盤のギョ―シェ彫りもブレゲの発案によるものだ。
                    </p>
                    <p>天才と言われる彼の技術はマリー・アントワネット妃の注文により作られた超複雑時計No.160
                        <br> 「マリー・アントワネット」に集約されている。
                    </p>
                    <p>1823年にブレゲは死去するが、その技術は弟子たちに受け継がれ、経営がブラウン家、宝石商ショーメと
                        <br> 移るが、その間も数多くの複雑度系を世に送り出した。
                    </p>
                    <p>現在はスウォッチ・グループの傘下となり、安定した経営基盤を獲得し、自社ムーブメントの開発や
                        <br> シリコン素材の導入など、新境地を開き続けている。
                    </p>
                    <p>有名なモデルにタイプトゥエンティー、マリーン、クイーン・オブ・ネイプルズ、クラシック、トラディション、ヘリテージ、コンプリケーションなど</p>
                </div>

            </section>-->
    <div style="margin-top:20px; margin-bottom: 20px;"><a href="https://kaitorisatei.info/brandrevalue/blog/doburock"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/12/caf61dc6779ec6137c0ab58dfe3a550d.jpg" alt="どぶろっく"></a></div>
            <section id="cat-point">
                <h3 class="obi_tl">高価買取のポイント</h3>
                 <ul>
                    <li>
                    <p class="pt_bigtl">POINT1</p>
                        <div class="pt_wrap">
                        <p class="pt_tl">商品情報が明確だと査定がスムーズ</p>
                        <p class="pt_tx">ブランド名、モデル名が明確だと査定がスピーディに出来、買取価格にもプラスに働きます。また、新作や人気モデル、人気ブランドであれば買取価格がプラスになります。</p>
                        </div>
                    </li>
                    <li>
                    <p class="pt_bigtl">POINT2</p>
                        <div class="pt_wrap">
                        <p class="pt_tl">数点まとめての査定だと
                            <br>キャンペーンで高価買取が可能</p>
                        <p class="pt_tx">数点まとめての査定依頼ですと、買取価格をプラスさせていただきます。</p>
                        </div>
                    </li>
                    <li>
                    <p class="pt_bigtl">POINT3</p>
                        <div class="pt_wrap">
                        <p class="pt_tl">品物の状態がよいほど
                            <br>高価買取が可能</p>
                        <p class="pt_tx">お品物の状態が良ければ良いほど、買取価格もプラスになります。</p>
                        </div>
                    </li>
                </ul>
                <p>もし購入時に同封されていた品質保証の証明書等がある場合は、査定時にお持ちいただくことをおすすめします。
スムーズかつ高額な査定をしやすくなります。その他、購入時のブランド製紙袋、箱、その他小物の換え部品等の付属品も一緒にお持ちいただくと、査定額がかなりアップすることがあります。<br><br>
流行のデザインの洋服などの場合、目安として発売後1年以内のお品を高額査定対象とさせていただいておりますので、お早目に査定を行われることをおすすめいたします。その他、品質タグがついた洋服や、事前にクリーニングしていただいた洋服も、査定額がアップします。
                </p>
            </section>
             <h3 class="mid">洋服・毛皮 一覧</h3>
        <ul class="mid_link">
            <li>
            <a href="<?php echo home_url('/cat/bag/chanel/chanel-apparel'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/fit/01.jpg" alt="シャネル">
            <p class="mid_ttl">シャネル</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/gucci/gucci-apparel'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/fit/02.jpg" alt="グッチ">
            <p class="mid_ttl">グッチ</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/hermes/hermes-apparel'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/fit/03.jpg" alt="エルメス">
            <p class="mid_ttl">エルメス</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/wallet/louisvuitton/louisvuitton-apparel'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/fit/04.jpg" alt="ルイヴィトン">
            <p class="mid_ttl">ルイヴィトン</p>
            </a>
            </li>
        </ul>

            <section id="lp-cat-jisseki">
                <h3 class="obi_tl">買取実績</h3>

                <ul id="box-jisseki" class="list-unstyled clearfix">
                    <?php
            foreach($resultLists as $list):
            // :: で分割
            $listItem = explode('::', $list);

          ?>
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/outfit/lp/<?php echo $listItem[0]; ?>" alt=""></p>
                                <p class="itemName">
                                    <?php echo $listItem[1]; ?>
                                        <br>
                                        <?php echo $listItem[2]; ?>
                                </p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：
                                    <?php echo $listItem[3]; ?>円
                                        <br>
                                        <span class="blue">B社</span>：
                                        <?php echo $listItem[4]; ?>円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">
                                    <?php echo $listItem[5]; ?><span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>
                                    <?php echo $listItem[6]; ?>円</p>
                            </div>
                        </li>
                        <?php endforeach; ?>
                </ul>
                           <!--     <h3 class="new_list_ttl">ブランドリスト</h3>
                <ul class="list-unstyled new_list">
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/hermes'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch33.jpg" alt="エルメス"></dd>
                                <dt><span>Hermes</span>エルメス</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/celine'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch29.jpg" alt="セリーヌ"></dd>
                                <dt><span>CELINE</span>セリーヌ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/wallet/louisvuitton'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch23.jpg" alt="ルイヴィトン"></dd>
                                <dt><span>LOUIS VUITTON</span>ルイヴィトン</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/chanel'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag01.jpg" alt="シャネル"></dd>
                                <dt><span>CHANEL</span>シャネル</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/gucci'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch25.jpg" alt="グッチ"></dd>
                                <dt><span>GUCCI</span>グッチ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/prada'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag02.jpg" alt="プラダ"></dd>
                                <dt><span>PRADA</span>プラダ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/fendi'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag03.jpg" alt="プラダ"></dd>
                                <dt><span>FENDI</span>フェンディ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/dior'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag04.jpg" alt="ディオール"></dd>
                                <dt><span>Dior</span>ディオール</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                        <a href="<?php echo home_url('/cat/bag/saint_laurent'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag05.jpg" alt="サンローラン"></dd>
                                <dt><span>Saint Laurent</span>サンローラン</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/bottegaveneta'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch32.jpg" alt="ボッテガヴェネタ"></dd>
                                <dt><span>Bottegaveneta</span>ボッテガヴェネタ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/ferragamo'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch31.jpg" alt="フェラガモ"></dd>
                                <dt><span>Ferragamo</span>フェラガモ</dt>
                            </a>
                        </dl>
                    </li>
            </ul>    -->            <p id="catchcopy">洋服・毛皮等のアパレル品は、品目が多くチェックポイントも複雑なため、一般的なブランド・貴金属買取ショップでは適切な査定が難しいこともあります。しかし、BRAND REVALUEは系列店で培ってきた豊富な古着買取の経験があり、熟練の鑑定士がスピーディかつ正確に査定を行うことが可能です。<br><br>
他店では値段がほとんどつかなかったような洋服・毛皮でも、BRAND REVALUEなら納得の高額査定で買い取らせていただくケースがございます。
            </p>
                <p class="jyoutai_tl">こんな状態でも買取致します!</p>
                <div id="konna">

                    <p class="example1">■デザインが古い</p>
                    <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/out_jyoutai01.jpg"></p>
                    <p class="example2">■破れている</p>
                    <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/out_jyoutai02.jpg"></p>
                    <p class="example3">■毛が出ている</p>
                    <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/out_jyoutai03.jpg"></p>
                    <p class="text">その他、状態についてのご質問はお気軽にご連絡下さい。</p>
                </div>
            </section>

            <section id="about_kaitori" class="clearfix">
                <?php
        // 買取について
        get_template_part('_widerange');
                get_template_part('_widerange2');
      ?>
            </section>

            <!--<h3 class="obi_tl">ブランドリスト</h3>
            <ul class="cat_list">
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit1.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit2.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit3.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit4.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit5.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit6.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit7.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit8.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit9.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit10.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit11.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit12.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit13.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit14.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit15.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit16.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit17.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit18.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit19.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit20.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit21.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit22.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit23.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit24.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit25.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit26.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit27.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit28.png" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/outfit29.png" alt=""></li>

            </ul>-->

            <section>
                <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/bn_matomegai.png">
            </section>

            <?php
        // 買取基準
        get_template_part('_criterion');

        // NGアイテム
        get_template_part('_no_criterion');

                // カテゴリーリンク
        get_template_part('_category');
            ?>
                <section id="kaitori_genre">
                    <h3 class="obi_tl">その他買取可能なジャンル</h3>
                    <p>【買取ジャンル】バッグ/ウエストポーチ/セカンドバック/トートバッグ/ビジネスバッグ/ボストンバッグ/クラッチバッグ/トランクケース/ショルダーバッグ/ポーチ/財布/カードケース/パスケース/キーケース/手帳/腕時計/ミュール/サンダル/ビジネスシューズ/パンプス/ブーツ/ペアリング/リング/ネックレス/ペンダント/ピアス/イアリング/ブローチ/ブレスレット/ライター/手袋/傘/ベルト/ペン/リストバンド/アンクレット/アクセサリー/サングラス/帽子/マフラー/ハンカチ/ネクタイ/ストール/スカーフ/バングル/カットソー/アンサンブル/ジャケット/コート/ブルゾン/ワンピース/ニット/シャツメンズ/毛皮/Tシャツ/キャミソール/タンクトップ/パーカー/ベスト/ポロシャツ/ジーンズ/スカート/スーツなど</p>
                </section>

                <?php
        // 買取方法
        get_template_part('_purchase');
      ?>

                    <section id="user_voice">
                        <h3>ご利用いただいたお客様の声</h3>

                        <p class="user_voice_text1">ちょうど家の整理をしていたところ、家のポストにチラシが入っていたので、ブランドリバリューに電話してみました。今まで買取店を利用したことがなく、不安でしたがとても丁寧な電話の対応とブランドリバリューの豊富な取り扱い品目を魅力に感じ、出張買取を依頼することにしました。 絶対に売れないだろうと思った、動かない時計や古くて痛んだバッグ、壊れてしまった貴金属のアクセサリーなども高額で買い取っていただいて、とても満足しています。古紙幣や絵画、食器なども買い取っていただけるとのことでしたので、また家を整理するときにまとめて見てもらおうと思います。
                        </p>

                        <h3>鑑定士からのコメント</h3>

                        <p class="user_voice_text2">家の整理をしているが、何を買い取ってもらえるか分からないから一回見に来て欲しいとのことでご連絡いただきました。 買取店が初めてで不安だったが、丁寧な対応に非常に安心しましたと笑顔でおっしゃって頂いたことを嬉しく思います。 物置にずっとしまっていた時計や、古いカバン、壊れてしまったアクセサリーなどもしっかりと価値を見極めて高額で提示させて頂きましたところ、お客様もこんなに高く買取してもらえるのかと驚いておりました。 これからも家の不用品を整理するときや物の価値判断ができないときは、すぐにお電話しますとおっしゃって頂きました。
                        </p>

                    </section>

        </div>
        <!-- lp_main -->
    </div>
    <!-- #primary -->

    <?php

get_footer();
