<?php
get_header(); ?>


<div class="mv_area ">
  <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/service.png" alt="サービスについて">
</div>
    <div class="cat_cnt">
        <h2 class="cat_tl">業界で最高水準の査定価格・サービス品質をご体験ください</h2>
        <p class="cat_tx">BRAND REVALUE(ブランドリバリュー)の店舗があるのは、銀座をはじめとする中古ブランド品の買取店がひしめく一等地。私たちはこの激戦区で、他店に負けない「最高水準の査定価格」と「お客様のニーズに応える多彩なサービスメニュー」、そして「懇切丁寧・誠実な接客」を強みとして買取サービスを提供しております。「不要になったブランド品・貴金属類を、信頼できる店で高額で売りたい」「即日で品物を現金化したい」そんなニーズをお持ちのお客様にBRAND REVALUE(ブランドリバリュー)が選ばれているのには、次の4つの理由があります。</p>
    </div>

    <div class="service_point cat_cnt">
        <h3><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/servise_bar01.png" alt="特長1 赤字覚悟！ 業界一の高額水準の査定を実現"></h3>
        <dl>
            <dt><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/servise_ph01.png" alt=""></dt>
            <dd>BRAND REVALUE(ブランドリバリュー)は銀座をはじめとし、利便性抜群の立地ながら、路面店よりも遥かに家賃の安い空中階にあります。
スペースも最低限に抑え、常駐スタッフも最小限の人数で運営。こうした徹底的な経費削減を行った分、地域トップクラスの高額査定を実現いたしました。さらに現在は新店オープンを記念し、赤字覚悟でお客様に利益を還元しております。</dd>
        </dl>
    </div>

    <div class="service_point cat_cnt">
        <h3><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/servise_bar02.png" alt="特長2 日本全国、ご自宅にいながら買取サービスをご利用いただけます"></h3>
        <dl>
            <dt><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/servise_ph02.png" alt=""></dt>
            <dd>「自宅から遠いので店舗まで行くのが大変……」そんな方もご安心ください。<br>
BRAND REVALUE(ブランドリバリュー)では「店頭買取」の他に「出張買取サービス」、日本全国でご利用いただける「宅配買取サービス」を行っており、 ご自宅から出ることなく店舗同様の査定・買取をご利用いただくことが可能となっております。</dd>
        </dl>
    </div>

    <div class="service_point cat_cnt">
        <h3><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/servise_bar03.png" alt="特長3 一号店はブランド品買取りの聖地「銀座」"></h3>
        <dl>
            <dt><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/servise_ph03.png" alt=""></dt>
            <dd>BRAND REVALUEの店舗があるのは銀座四丁目交差点。<br>
            ラグジュアリーな街・銀座に相応しくシンプルながら美しい店舗で、マナー・査定技術ともに一流のスタッフがお客様をお待ちしております。
            ブランド品買取店が集中する日本屈指の「聖地」ですから、相見積もりをとり他店と比較されることも大歓迎。<br>
            ぜひ、その最後にご来店ください。他より高く査定いたします。</dd>
        </dl>
    </div>

    <div class="service_point cat_cnt">
        <h3><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/servise_bar04.png" alt="特長4 丁寧・親切な対応をお約束いたします"></h3>
        <dl>
            <dt><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/servise_ph04.png" alt=""></dt>
            <dd>何よりもお客様に喜んでいただくことを大切にしているBRAND REVALUEでは、どこよりも高額な査定を追求するだけではなく、お客様の細かなご要望にも丁寧にお応えし、
            サービスの品質向上を図っております。ご不明な点などございましたら、どうかお気軽にご相談ください。<br>
            また、ご郵送いただいたお品物の取り扱い・返送等も丁寧に行いますので、安心してご利用いただけます。</dd>
        </dl>
    </div>



<?php
  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
