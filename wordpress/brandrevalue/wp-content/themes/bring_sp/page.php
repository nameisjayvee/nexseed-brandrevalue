<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 *
 */

get_header(); ?>

<style type="text/css">
  .pc-only {
    display: none;
  }
  .pt-wrap {
    padding: 20px;
  }
  p, li {
    line-height: 1.75;
  }
  #lp-cat-jisseki li {
    font-size: 10px;
  }
  .hitokoto {
    text-align: left;
    background-color: #EFEFF0;
    padding: 5px;
  }
  .hitokoto span {
    display: block;
  }
  #user_voice h4 {
    background: #ccc;
    color: #333 !important;
    font-weight: bold;
    padding: 10px 0 10px 10px;
    margin-top: 10px;
    margin-bottom: 10px;
  }
  #lp_head h2{

  font-family: 
    "Roboto Slab", Garamond, "Times New Roman", "游明朝", "Yu Mincho", "游明朝体", "YuMincho",
    "ヒラギノ明朝 Pro W3", "Hiragino Mincho Pro", "HiraMinProN-W3", "HGS明朝E", "ＭＳ Ｐ明朝", "MS PMincho", serif;
		font-size:2.5vh;
	line-height:1;
	margin:0;
	padding:0 !important;
	margin-top:5px;
	font-weight:bold;
}

#lp_head{
	background-repeat:no-repeat;
	background-size:100% !important;
	background-position:bottom;
}
#lp_head div{
	padding-left:32%;
	padding-bottom:5%;
	line-height:1;
}
#lp_head p{
	color:#7e7420;
	font-size:12px;
	margin:0;
	padding:0;
	line-height:1.2;
	display:block;
	margin:0 !important;
	padding:0 !important;

}
</style>


<div id="primary" class="cat-page content-area">
  <div class="mv_area ">
    <?php echo wp_get_attachment_image(SCF::get('lp-mainVisSP'), 'full'); ?>
  </div>
  
  <div class="lp_main">
    <p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
   <?php
   $lp_mainVisBottom_txt = SCF::get( 'lp_mainVisBottom_txt' ); ?>
	<p class="main_bottom"><?php echo nl2br( $lp_mainVisBottom_txt ); ?></p>

    <section id="hikaku" class="watch_hikaku">
      <p class="hikaku_img"><?php echo wp_get_attachment_image(SCF::get('lp-resultPickupImageSP'), 'full'); ?></p>
    </section>
    
    <?php the_content(); ?>

  </div>
<!-- lp_main -->
   <?php // 店舗案内
  get_template_part('_shopinfo');
    ?>
  
</div>
<!-- #primary -->

<?php
get_footer();