<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
  //'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  '001.jpg::ルースダイヤ　1.098ct　G　SI2　G　NONE::330,000::335,000::340,000::10,000',
  '002.jpg::ダイヤネックレス　Pt850　0.81ct　MD0.23ct::340,000::330,000::350,000 ::17,000',
  '003.jpg::ヴァンクリーフ&アーペル　クロアミニ　アチュールクロス　ダイヤネックレス::117,000::105,000::110,000 ::5,000',
  '004.jpg::ダイヤリング　Pt900　1.00ｃｔ　0.4ｃｔ::62,000::66,000::70,000::8,000',
  '005.jpg::ティファニー　Pt900　ダイヤリング::30,000::27,000::32,000::5,000',
  '006.jpg::ルースダイヤ　1.006ct　H　VS2　VG　NONE　::440,000::445,000::450,000::1,000',
  '007.jpg::ダイヤリング　Pt900　0.51ct　::45,000::47,800::50,000::5,000',
  '008.jpg::ルースダイヤ　1.793ct　J　SI2　G　NONE　::620,000::611,000::630,000::19,000',
  '009.jpg::ダイヤリング　K18　1.48ｃｔ　::58,000::60,000::63,000::5,000',
  '010.jpg::ブルガリ　B-ZERO1　ダイヤネネックレス　K18WG::220,000::215,000::225,000::10,000',
  '011.jpg::ブシュロン　ディアマン　ダイヤネックレス　750::225,000::219,000::230,000::11,000',
  '012.jpg::カルティエ　ハーフダイヤラブリング::97,000::93,000::103,000::10,000',
);


get_header(); ?>

<div id="primary" class="cat-page content-area">
	<div class="mv_area "> <img data-src="<?php echo get_template_directory_uri() ?>/images/lp_main/cat_diamond_main.jpg" alt="あなたのダイヤモンドお売り下さい！"> </div>
	<p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
	<p class="main_bottom">ダイヤモンド買取！！業界最高峰の買取価格をお約束いたします。</p>
	<div class="lp_main">
		<section class="dia_staff01">
			<h3>ダイヤモンド担当査定士のご紹介</h3>
			<p class="dia_staff_tx">ブランドリバリュー銀座店の査定士をご紹介させて頂きます。<br />
				各バイヤーのご挨拶や査定に関する強みをご説明させていただきます。<br />
				店頭へご来店や郵送査定、出張査定での接客も私たちが承ります。 </p>
		</section>
		<section class="dia_staff02"> <img data-src="<?php echo get_template_directory_uri() ?>/img/lp/dia/staff01.png" alt="スタッフ">
			<p class="dia_staff_name">森　雄大  - <span class="dia_staff_post">査定士・総合リユース事業部　部長 </span> </p>
			<div class="dia_type_wrap">
				<p class="dia_type">好きな宝石：ダイヤモンド</p>
				<p class="type_det">ダイヤモンドの査定は、単に目利きをして買取することではありません。
					お客さまがお持ちになるダイヤモンドやジュエリーの大事な思い出やご購入した背景をお聞きした上で、新たな価値を吹き込むことが査定士の本来の仕事であると考えております。その上でお客さまにご満足いただける買取価格をご提示することこそがブランドリバリューにしかできないサービスです！
					加えて、宝石の査定は、非常に奥が深く専門的な知識が必要となります。
					国内外の市場動向を把握し、宝石を鑑定・鑑別する腕を日々磨いております。ブランドリバリューにしかできない「サービス」を体験して頂ければと思います。
					皆さまのご来店・お問合せお待ちしております。 </p>
			</div>
		</section>
		<section class="dia_staff02"> <img data-src="<?php echo get_template_directory_uri() ?>/img/lp/dia/staff02.png" alt="スタッフ">
			<p class="dia_staff_name">佐藤　昴太  - <span class="dia_staff_post">査定士・店長・サイト運営等 </span> </p>
			<div class="dia_type_wrap">
				<p class="dia_type">好きな宝石：スター効果（星彩効果）を有する宝石各種　スタールビーやスターサファイヤ・スターアレキサンドライトといった希少性が高い珍しい宝石が好きです！</p>
				<p class="type_det">私は「査定士」としてお客様の目に映る事を意識しています。 宝石やジュエリー等の宝飾関係は独特の専門的な用語や知識が多く一般のお客様にはわかりずらい事が多々あります。

				お客様の立場で考えれば、
				「難しい言葉を並べられ訳も分からずダイヤモンドに査定価格を付けられた！しかも、その値段が適切かどうかもわからない！」
				ということは不安であり心から納得された買取にはならないと私は思います。
				提示した値段が高ければそれだけでよいのか？
				私はそうは思いません！
				お客様の不安を払拭するため査定の際に専門的な用語や知識を、どこよりもお客様に伝わりやすく、わかりやすいようにご説明をすることを心掛けております。

				また、宝石関係のオークションに出入りすることが多く、市場の需要にたいして誰よりも詳しい自信がございます！
				そこで培われた相場の感覚が、正確な値段提示を可能にしており、市場の需要を熟知することで、お持ちいただいたお品物の価値をどこよりも高く見出します。 </p>
			</div>
		</section>
		<section class="dia_staff02"> <img data-src="<?php echo get_template_directory_uri() ?>/img/lp/dia/staff04.png" alt="スタッフ">
			<p class="dia_staff_name">高田　啓介  - <span class="dia_staff_post">査定士・店長・サイト運営等 </span> </p>
			<div class="dia_type_wrap">
				<p class="dia_type">好きな宝石：ファンシーカラーダイヤモンド・半貴石全般</p>
				<p class="type_det">私は、査定士であり営業も担当している高田啓介と申します。 特定の物が好きではなく貴石、半貴石問わずすべてが好きです！ 宝飾系の専門学校卒業後、一貫して査定に携わってきました。 査定のみではなくお客様と同じ好きなものを語り合いたい！と日々思っております。 「傷があるから取り扱えない。」、「需要が無いから取り扱えない。」、 買取店で見積もりをとろうとしたらそのようなことを言われたご経験はありませんでしたでしょうか？ 私は違います！ 価値が無いのならこちらで作ればいいのです！それが査定士の仕事と考えております。 バッグや時計など様々なジャンルがございますが、とりわけダイヤモンドなどのジュエリーアクセサリーや宝石類は、お客様の思い入れが強いお品物だと思います。 その思いもくみ取らせていただきたい一心で営業に尽力をしております。 その営業の甲斐もあり、取引先の新規開拓に力を入れ、取引先様を増やす事により他社様では価値が出にくいようなお品物も、当店であれば高い価値を見いだすことが可能となっております。 その思いが結果を生んだのか私の担当したお客様のリピート率は、80％を超えることができました。 今後、さらに精進し多くのお客様に対しよりよいサービスを提供するべく邁進してまいります。</p>
			</div>
		</section>
				<section class="dia_staff02"> <img data-src="<?php echo get_template_directory_uri() ?>/img/lp/dia/staff06.png" alt="スタッフ">
			<p class="dia_staff_name">伊藤 裕樹  - <span class="dia_staff_post">査定・営業・チーフバイヤー・サイト運営 </span> </p>
			<div class="dia_type_wrap">
				<p class="dia_type">好きな宝石：エメラルド</p>
				<p class="type_det">私は「査定士」としてお客様に納得した上でお買取りさせて頂く事を心掛けております。
					ご存知かと思いますが宝石とは多くの種類が存在し、又多くの奇跡が起こり生まれた石なのです。
					そんな奇跡の宝石の中でもエメラルドは綺麗なグリーンで、産地により違う顔を見せるエメラルドが私は昔から好きです。
					宝石の中でも非常にデリケートで欠けやすく柔らかい宝石なので扱い方も慎重に扱わなければなりません。
					お客様の大切な宝石だから、奇跡の宝石だから、思い出の宝石だからこそ日々勉強し国内外の相場を熟知し最高の知識と最高の査定を心掛けております。
					日々培った相場感、知識を活かし最高の査定をさせて頂いております！
					</p>
			</div>
		</section>

				<section class="dia_staff02"> <img data-src="<?php echo get_template_directory_uri() ?>/img/lp/dia/staff05.png" alt="スタッフ">
			<p class="dia_staff_name">木村 正伸  - <span class="dia_staff_post">査定・営業・チーフバイヤー・サイト運営 </span> </p>
			<div class="dia_type_wrap">
				<p class="dia_type">好きな宝石：トパーズ、インディアンジュエリー</p>
				<p class="type_det">査定士の木村正伸と申します。宝石や石に興味を持つようになったのは、私自身の誕生石がきっかけでした。もともと好きだったターコイズが私の誕生石であることを知ってから、誕生石、石言葉などを調べ始めました。石を取り扱うことが仕事になるとは、当時は思ってもいませんでした。ちなみに私の好きな宝石はトパーズ(石言葉…誠実)です。今でも趣味でインディアンジュエリーを集めていますが、眺めているだけで幸せです。
				私は、石本来の魅力とは表情であると思っています。大きさ、輝き、そしてお客様の想い。それらを重ねたすべてが、石の価値になっていると考えております。
				そういった価値すべてを汲み取り、お買取りをさせて頂くのが査定士としてのやり甲斐であります。
				できるだけご満足頂ける価格への反映と、金額理由をご理解頂ける明確な説明で、誠実なサービスに努めて参ります。
				皆様の石を拝見できる機会を、心よりお待ちしております。 </p>
			</div>
		</section>

	</div>
				<ul class="dia_nav">
		<li><a href="<?php echo home_url('/cat/diamond/diamond-qa'); ?>">ダイヤモンド 買取Q&A</a></li>
		<li><a href="<?php echo home_url('cat/diamond/diamond-term'); ?>">ダイヤモンド 用語集</a></li>
		<li><a href="<?php echo home_url('/cat/diamond/diamond-app'); ?>">ダイヤモンド 鑑定書</a></li>
		<li><a href="<?php echo home_url('/cat/diamond/diamond-voice'); ?>">お客様の声</a></li>
		<li><a href="<?php echo home_url('/cat/diamond/diamond-staff'); ?>">査定士紹介</a></li>
	</ul>
	<!-- lp_main -->
</div>
<!-- #primary -->
<script src="//test-kaitorisatei.info/brandrevalue/wp-content/themes/bring/js/gaisan.js" type="text/javascript"></script>
<?php

get_footer();
