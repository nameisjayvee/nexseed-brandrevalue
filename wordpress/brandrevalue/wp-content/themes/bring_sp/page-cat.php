<?php

get_header(); ?>

<div class="mv_area ">
  <img data-src="<?php echo get_s3_template_directory_uri(); ?>/images/kv-cat.png " alt="取扱いカテゴリー">
</div>

<div class="cat_cnt">
  <h2 class="cat_tl">貴金属、ダイヤモンド、時計、毛皮など、幅広いお品物を買い取ります</h2>

  <ul class="cat_list">
    <li>
        <a href="<?php echo home_url('cat/gold'); ?>"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/images/cat01.png" alt="金・プラチナ"></a>
    </li>
    <li>
        <a href="<?php echo home_url('cat/gem'); ?>"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/images/cat02.png" alt="宝石"></a>
    </li>
    <li>
        <a href="<?php echo home_url('cat/watch'); ?>"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/images/cat03.png" alt="時計"></a>
    </li>
    <li>
        <a href="<?php echo home_url('cat/bag'); ?>"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/images/cat04.png" alt="バッグ"></a>
    </li>
    <li>
        <a href="<?php echo home_url('cat/outfit'); ?>"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/images/cat05.png" alt="洋服・毛皮"></a>
    </li>
    <li>
        <a href="<?php echo home_url('kimono'); ?>"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/images/cat10.png" alt="着物"></a>
    </li>
    <li>
        <a href="<?php echo home_url('cat/wallet'); ?>"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/images/cat06.png" alt="ブランド・財布"></a>
    </li>
    <li>
        <a href="<?php echo home_url('cat/shoes'); ?>"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/images/cat07.png" alt="靴"></a>
    </li>
    <li>
        <a href="<?php echo home_url('cat/diamond'); ?>"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/images/cat08.png" alt="ダイヤモンド"></a>
    </li>
    <li>
        <a href="<?php echo home_url('cat/brandjewelery'); ?>"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/images/cat_bj.png" alt="ブランドジュエリー"></a>
    </li>
    <li>
        <a href="<?php echo home_url('cat/antique'); ?>"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/images/cat09.png" alt="骨董品"></a>
    </li>
    <li>
        <a href="<?php echo home_url('cat/mement'); ?>"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/images/btn-memento_sp.png" alt="遺品"></a>
    </li>
</ul>
<div class="cat_list single">
    <a href="<?php echo home_url('cat/antique_rolex'); ?>"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/images/catanti_rolex.png" alt="アンティークロレックス"></a>
</div>


  <p class="cat_tx">「店舗に足を運んだり、郵送の手続きをする時間がない」という方にお勧めしたいのが、「出張買取サービス」です。</p>
  <p class="cat_tx2">お申込み後「最短即日」でBRAND REVALUEの鑑定スタッフがお客様のご自宅までお伺いし、その場でお品物をスピード査定。<br>
  金、宝石、時計、バッグ、財布、毛皮、洋服など、価値あるお品物を、どこよりも高く買い取りさせていただきます。<br>
  ロレックス、エルメス、ルイ・ヴィトン、プラダ、グッチといった代表的なブランドはもちろん、その他の多くのブランドについても積極的に取り扱っております。<br>
  1000万円以上の超高級品の買取も可能。自宅に不要なブランド品があるけれど、高価な品物だから信頼できるお店でなるべく高く売りたい、そんな方にもBRAND REVALUEの買取りサービスは必ずご納得いただけると思います。<br>
  出張買取は家から一歩も出ることなく、店舗と同様のスピーディな買取サービスをご利用いただける、「一番楽ちん」なスタイルです。</p>
</div>


<?php
  // 買取基準
  get_template_part('_criterion');

  // NGなアイテム
  get_template_part('_no_criterion');

  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
