<?php
  
get_header(); ?>

<div class="mv_area ">
            <img src="<?php echo get_s3_template_directory_uri() ?>/images/diamon_kv.png " alt="BRAMD REVALUE ">
        </div>
        <div class="cat_cnt">
            <h2 class="cat_tl">ますます価値が急上昇する「宝石の王」ダイヤモンドを高額買取</h2>
            <p class="cat_tx">ダイヤモンドといえば、古来多くの人間が憧れ、権力や富の象徴として変わらぬ価値を誇ってきた「宝石の王」。近年は中国やインドなどアジア諸国に富裕層が増えたことからダイヤモンドの需要が伸び、これまで以上に高い価格相場で取引されるようになりました。また、ダイヤモンドをはじめとする貴金属は、アメリカドル建ての価格を円に換算して買取価格が決められることが多いため、日本では円高ドル安の為替の影響を受けてますます値上がりしているのです。つまり、ダイヤモンドを売りたいと考えている方にとって、今こそ絶好のチャンス。ダイヤモンド単体はもちろん、古い指輪やペンダント、ピアス等に付属したダイヤモンドも、石の品質・状態次第では驚くほどの高い価格で査定される可能性があります。</p>
        </div>

        <div id="cat-jisseki">
            <h3 class="cat-jisseki_tl">買取実績</h3>
            <p class="cat_jisseki_tx">BRAND REVALUEがモットーとしているのは「お客様に利益を還元する店舗づくり」。路面店ではなくあえて家賃の安い空中階に店舗を構え、人件費・広告費を抑えているのも、削減した経費の分査定額を高めるためです。ダイヤモンドの買取においても、「鑑定書がないダイヤモンド」、「ノンブランドのダイヤモンド」、「極小粒のダイヤモンド（メレ）」などどんなものでもご満足いただける価格で買い取らせていただきます。新しい店舗ではありますが、経験豊かな鑑定士が適切に査定を行いますので、どうかお気軽にご相談ください。</p>
            <ul id="box-jisseki" class="list-unstyled clearfix">
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/dia001.jpg" alt=""></p>
                        <p class="itemName">ルース
                            <br>1ct / カラーF / クラリティVS1 / カットVeryGood</p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">300,000<span class="small">円</span></p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/dia001.jpg" alt=""></p>
                        <p class="itemName">ルース
                            <br>0.5ct / カラーF / クラリティVS1 / カットVeryGood</p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">84,000<span class="small">円</span></p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/dia001.jpg" alt=""></p>
                        <p class="itemName">ルース
                            <br>0.3ct / カラーF / クラリティVS1 / カットVeryGood</p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">29,000<span class="small">円</span></p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/dia001.jpg" alt=""></p>
                        <p class="itemName">ルース
                            <br>0.2ct / カラーF / クラリティVS1 / カットVeryGood</p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">10,000<span class="small">円</span></p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/dia001.jpg" alt=""></p>
                        <p class="itemName">ルース
                            <br>2ct / カラーF / クラリティVS1 / カットVeryGood</p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">1,580,000<span class="small">円</span></p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/dia001.jpg" alt=""></p>
                        <p class="itemName">メレー
                            <br>0.03ct / カラーF / クラリティVS1 / カットVeryGood</p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price"><span class="small">一粒</span>594<span class="small">円</span></p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/dia001.jpg" alt=""></p>
                        <p class="itemName">メレー
                            <br>0.03ct / カラーF / クラリティVS1 / カットVeryGood</p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price"><span class="small">一粒</span>370<span class="small">円</span></p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/dia001.jpg" alt=""></p>
                        <p class="itemName">メレー
                            <br>0.03ct / カラーF / クラリティVS1 / カットVeryGood</p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price"><span class="small">一粒</span>150<span class="small">円</span></p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/dia001.jpg" alt=""></p>
                        <p class="itemName">
                            <br>1.05ct D-VVS1-VG</p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">900,000<span class="small">円</span></p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/dia002.jpg" alt=""></p>
                        <p class="itemName">
                            <br>2.01ct D-VVS1-EX</p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">3,368,760<span class="small">円</span></p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/dia003.jpg" alt=""></p>
                        <p class="itemName">
                            <br>0.575ct E-VVS2-VG</p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">150,000<span class="small">円</span></p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/dia004.jpg" alt=""></p>
                        <p class="itemName">
                            <br>0.72ct D-VVS1-VG</p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">320,000<span class="small">円</span></p>
                    </div>
                </li>
            </ul>
            <div class="kaitori_point">
            <h3>【ダイヤモンドの高価買取ポイント】</h3>
天然の鉱物の中で最も硬度が高いといわれるダイヤモンドですが、実は瞬間的な衝撃には弱く、欠けてしまうことも珍しくありません。<br>
そのため、査定のためお持ちいただく際にも傷がつかないよう、緩衝材等で保護いただくことをお勧めしております。また、ご自宅で軽く布でぬぐうなどして汚れを落としておいた方が、査定額は高くなります。ときどきリングやペンダントからダイヤモンドを外して持ってこられる方もいらっしゃいますが、取り外しの際に傷がつく可能性があるので、そのままお持ちください。もし購入時に入手した鑑別書があれば、ぜひお持ちください。査定がスムーズになり、買取価格も上がりやすくなります。もちろん鑑定書等がなくてもご心配なく。経験豊かな鑑定士が専用の機器を使い、迅速かつ正確に鑑定を行います。
            </div>
        </div>

        <div class="point_list">
            <div class="coint_bnr">
                <img src="<?php echo get_s3_template_directory_uri() ?>/images/coin_bnr01.png" alt="他の店鋪より1円でも安ければご連絡下さい。">
                <ul class="other_price">
                    <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/coin_bnr02.png" alt="他の店鋪より1円でも安ければご連絡下さい。"></li>
                    <li>ブリングは徹底した高価買取を行っております。お客様が愛着を持って身につけてきた服は、大切に買い取りさせて頂きます。 万が一、他店よりも1円でも安い価格であればおっしゃってください。なるべくお客様のご要望に答えられるよう、査定させて頂きます。<br>
                まずは、こちらから買い取り実績について、覗いてみてください。きっと、お客様の古着に相応しい、ご期待に添える価格での買い取りを行っているはずです。</li>
                </ul>
            </div>
            <h3 class="obi_tl">高価買取のポイント</h3>
            <img src="<?php echo get_s3_template_directory_uri() ?>/images/kaitori_point.png" alt="高価買い取りのポイント" class="point_img">
            <h3 class="obi_tl">ブランドリスト</h3>
            <ul class="cat_list">
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list01.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list02.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list03.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list04.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list05.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list06.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list07.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list08.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list09.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list10.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list11.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list12.png" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list13.png" alt=""></li>
            </ul>
        </div>

<?php
  // お問い合わせ
  get_template_part('_action');
  
  // 3つのポイント
  get_template_part('_purchase');
  
  // お問い合わせ
  get_template_part('_action2');
  
  // 店舗
  get_template_part('_shopinfo');
  
  // フッター
  get_footer();