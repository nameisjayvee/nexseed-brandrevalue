<?php
get_header(); ?>

<div class="line">
<div class="l_cnt01">
    <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/line_mv.png" alt="LINE査定はじめました！">
</div>

    <div class="catch">
        <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/line_ttl.png" alt="ご来店も郵送も不要！スマートフォンひとつで、スピード査定が可能に">
            <p>「クローゼットの中に使わなくなったブランド品があるけど、どれぐらいの金額になるのか見当がつかないし、お店に行く時間もない」そんな方にお勧めしたいのがLINE査定です。
            携帯電話の内蔵カメラでお品物を写し、その画像をLINEやメールでお送りいただくだけでOK。<br>
            24時間以内に簡易査定を行い、目安となる買取金額をお伝えいたします。</p>
    </div>
<div class="step_box">
    <h3><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/step_ttl.png" alt="登録はかんたん！たったの3ステップ"></h3>
    <dl class="step">
        <dt><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/step1_bar.png" alt="Step1"></dt>
        <dd>LINEを開き「その他」メニューから「友だち追加」を選択して下さい。</dd>
    </dl>
    <dl class="step">
        <dt><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/step2_bar.png" alt="Step2"></dt>
        <dd>
            <!--<p class="left"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/line_qr.png" alt="QR"></p>
            <p class="right">-->@brandrevalueで検索して下さい。<br>
            <a href="https://line.me/ti/p/%40otv4506b"><img width="100%" border="0" alt="友だち追加数" src="http://biz.line.naver.jp/line_business/img/btn/addfriends_ja.png"></a><!--</p>-->
        </dd>
    </dl>
    <dl class="step">
        <dt><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/step3_bar.png" alt="Step3"></dt>
        <dd>「BRAND REVALUE」を 友だちリストに追加して下さい。</dd>
    </dl>
</div>


<section class="line_tech cat_cnt">
  <dl>
      <dt><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/line_ttl02.png" alt="受付は24時間実施中！LINE査定の流れ"></dt>
      <dd>
          <p>LINEの査定はとても簡単です。まずはスマートフォン等で査定を依頼したいお品物の写真を撮影。その画像を、BRAND REVALUEのLINEアカウントにお送りください。受付は24時間行っております。<br>
          最低限送っていただく必要があるものは、お品物の画像と、お品物に関する簡単な概要の文章だけ。
          お名前やお電話番号、ご住所といった情報はなくてもOKです。24時間以内に画像をもとに簡易査定を行い、お客様のLINEアカウントに目安となる買取金額をお送りいたします。<br>
          ※LINEアカウントをお持ちでない方は、パソコンのメールで画像をお送りいただいてもけっこうです。</p>
          <h4><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/line_okurikata_ttl.png" alt="高額査定になる写真の送り方"></h4>
              <dl class="okurikata">
                  <dt><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/line_okurikata01.png" alt="高額査定になる写真の送り方"></dt>
                  <dd>LINE査定ではお客様がご用意した写真をもとに査定を行います。よりスピーディーに、より高く査定を行えるように「明るい場所で」「ピントを合わせて」撮影を行って下さい。</dd>
                  <dt><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/line_okurikata02.png" alt="高額査定になる写真の送り方"></dt>
                  <dd>1枚だけでなく、複数枚写真を送っていただけると詳細な査定が行えます。細かい部分まできっちり査定を行うので、高額査定に繋がります！
※その他、ご質問等ございましたら、LINEまたはメールにてお気軽にお伝えください。</dd>
              </dl>
      </dd>
  </dl>
</section>

<div class="line_att">
    <h4>LINE査定に関する注意事項</h4>
    <p>正式な査定は、お品物の現物を見なければできません。<br>
    LINE画像による査定ではお品物を隅々まで見ることが難しいため、あくまで目安となる金額をお伝えするということをご了承ください。
    お品物の品番や製造年など、詳細情報が不明な場合は画像をもとにBRAND REVALUEにて可能な限り情報をお調べいたしますが、これらの情報も現物を見なければ正確に査定するのは困難です。<br>
    詳細の情報をお送りいただけたほうが、より精度の高い簡易査定を行えます。</p>
</div>

</div>


<section class="line_tech cat_cnt">
  <dl>
      <dt><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/line_tech_bar.png" alt="こうすれば査定額がより正確に！LINE査定の高額査定テクニック"></dt>
      <dd>
          <p>LINE査定はあくまで「目安となる買取金額をお伝えする簡易査定」ですが、以下の点にご注意いただければ、より正確な金額をお伝えしやすくなります。
          ぜひ、ご参考にしてみてください。</p>
          <ul>
              <li>写真は複数枚撮影し、外観全体が見えるようにしてください。<br>
              ダメージがある箇所、型番等の表記はアップで写すとより効果的です。</li>
              <li>購入先、購入時期、型番等、おわかりになる範囲で情報をご明記ください。</li>
              <li>お品物のサイズ（寸法）もあわせてご明記ください。同じ形状でサイズが異なる商品もございます。</li>
          </ul>
          <p>その他、ご質問等ございましたら、LINEまたはメールにてお気軽にお伝えください。</p>
      </dd>
  </dl>
</section>


<?php
  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
