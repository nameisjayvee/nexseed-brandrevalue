<?php
get_header(); ?>

<div class="mv_area ">
<img src="<?php echo get_s3_template_directory_uri() ?>/images/kv-purchase1.png " alt="BRAMD REVALUE ">
</div>
<section id="mailform">
<div class="">

<!--<div role="form" class="wpcf7" id="wpcf7-f66-o1" lang="ja" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/purchase1#wpcf7-f66-o1" method="post" class="wpcf7-form form-horizontal" enctype="multipart/form-data" novalidate>
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="66" />
<input type="hidden" name="_wpcf7_version" value="4.3.1" />
<input type="hidden" name="_wpcf7_locale" value="ja" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f66-o1" />
<input type="hidden" name="_wpnonce" value="3dcf703af9" />
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div>
<div class="form-group form-name">
<label class="control-label col-sm-3"><span class="req">必須</span>お名前</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap sei"><input type="text" name="sei" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control span2" aria-required="true" aria-invalid="false" placeholder="お名前" /></span></div>
</div>
<div class="form-group form-name">
<label class="control-label col-sm-3">フリガナ</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap kana-sei"><input type="text" name="kana-sei" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control input-mini" aria-required="true" aria-invalid="false" placeholder="フリガナ" /></span></div>
</div>
<div class="form-group form-email">
<label class="control-label col-sm-3">電話番号</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap email"><input type="tel" name="tel" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group form-email">
<label class="control-label col-sm-3"><span class="req">必須</span>メールアドレス</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group form-email">
<label class="control-label col-sm-3"><span class="req">必須</span>確認のためもう一度</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap email-check"><input type="email" name="email-check" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group form-file">
<label class="control-label col-sm-3"><span class="req">必須</span>添付ファイル</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap file1"><input type="file" name="file1" size="40" class="wpcf7-form-control wpcf7-file wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span><span class="wpcf7-form-control-wrap file2"><input type="file" name="file2" size="40" class="wpcf7-form-control wpcf7-file" aria-invalid="false" /></span><span class="wpcf7-form-control-wrap file3"><input type="file" name="file3" size="40" class="wpcf7-form-control wpcf7-file" aria-invalid="false" /></span></div>
</div>
<div class="form-group form-check">
<label class="control-label col-sm-3">メルマガ配信</label>
<input type="checkbox" name="riyu" value="1">
</div>
<div class="form-group form-text">
<label class="control-label col-sm-3">備考欄</label>
<div class="col-sm-9">
<p><span class="wpcf7-form-control-wrap bikou"><textarea name="bikou" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea form-control" aria-invalid="false"></textarea></span></p>
</div>
</div>
<div class="text-center">
<input type="image" value="リセット" class="" src="<?php echo get_s3_template_directory_uri() ?>/images/reset_btn.png" />
<input type="image" value="送信" class="wpcf7-form-control" src="<?php echo get_s3_template_directory_uri() ?>/images/sub_btn.png" />
</div>
</form>
</div>
-->

<?php echo do_shortcode('[mwform_formkey key="23208"]'); ?>

</div>
</section>
<div class="textarea">
※メールでのお問い合わせ・査定は、原則的に１営業日以内にご連絡を差し上げるよう努めております。
多くのお問い合わせを頂戴している場合や店舗の混雑状況により、ご回答が遅れる可能性がございます。<br>
当店から返信がない場合には、お手数ではございますが、電話またはＬＩＮＥにてお問い合わせくださいませ。<br><br>
</div>

<script src="http://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script type="text/javascript">
(function () {

    "use strict";

    /**
     * Pardot form handler assist script.
     * Copyright (c) 2018 toBe marketing, inc.
     * Released under the MIT license
     * http://opensource.org/licenses/mit-license.php
     */

    // == 設定項目ここから ==================================================================== //

    /*
     * ＠データ取得方式
     */
    var useGetType = 1;

    /*
     * ＠フォームハンドラーエンドポイントURL
     */
    var pardotFHUrl = 'https://go.pardot.com/l/436102/2017-12-13/d5lyjf';

    /*
     * ＠フォームのセレクタ名
     */
    var parentFormName = '#mw_wp_form_mw-wp-form-23208 > form';

    /*
     * ＠送信ボタンのセレクタ名
     */
    var submitButton = 'input[type=submit][name=confirm]';

    /*
     * ＠項目マッピング
     */

    var defaultFormJson = [
      { "tag_name": "sei",      "x_target": 'sei' },
      { "tag_name": "mei",      "x_target": 'mei' },
      { "tag_name": "kana-sei", "x_target": 'kana-sei' },
      { "tag_name": "kana-mei", "x_target": 'kana-mei' },
      { "tag_name": "email",    "x_target": 'email' },
      { "tag_name": "tel",      "x_target": 'tel' },
      { "tag_name": "bikou",    "x_target": 'bikou' }
    ];

    // == 設定項目ここまで ==================================================================== //

    // 区切り文字設定
    var separateString = ',';

    var iframeFormData = '';
    for (var i in defaultFormJson) {
        iframeFormData = iframeFormData + '<input id="pd' + defaultFormJson[i]['tag_name'] + '" type="text" name="' + defaultFormJson[i]['tag_name'] + '"/>';
    }

    var iframeHeadSrc =
        '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';

    var iframeSrcDoc =
        '<form id="shadowForm" action="' + pardotFHUrl + '" method="post" accept-charset=\'UTF-8\'>' +
        iframeFormData +
        '<input id="shadowSubmit" type="submit" value="send" onclick="document.charset=\'UTF-8\';"/>' +
        '</form>';

    var shadowSubmited = false;
    var isError = false;
    var pdHiddenName = 'pdHiddenFrame';

    /**
     * エスケープ処理
     * @param val
     */
    function escapeSelectorString(val) {
        return val.replace(/[ !"#$%&'()*+,.\/:;<=>?@\[\\\]^`{|}~]/g, "\\$&");
    }

    $(function () {

        $(submitButton).click(function () {
            // Submitボタンを無効にする。
            $(submitButton).prop("disabled", true);
        });

        $('#' + pdHiddenName).remove();

        $('<iframe>', {
            id: pdHiddenName
        })
            .css({
                display: 'none'
            })
            .on('load', function () {

                if (shadowSubmited) {
                    if (!isError) {

                        shadowSubmited = false;

                        // 送信ボタンを押せる状態にする。
                        $(submitButton).prop("disabled", false);

                        // 親フォームを送信
                        $(parentFormName).submit();
                    }
                } else {
                    $('#' + pdHiddenName).contents().find('head').prop('innerHTML', iframeHeadSrc);
                    $('#' + pdHiddenName).contents().find('body').prop('innerHTML', iframeSrcDoc);

                    $(submitButton).click(function () {
                        shadowSubmited = true;
                        try {
                            for (var j in defaultFormJson) {
                                var tmpData = '';

                                // NANE値取得形式
                                if (useGetType === 1) {
                                    $(parentFormName + ' [name="' + escapeSelectorString(defaultFormJson[j]['x_target']) + '"]').each(function () {

                                        //checkbox,radioの場合、未選択項目は送信除外
                                        if (["checkbox", "radio"].indexOf($(this).prop("type")) >= 0) {
                                            if ($(this).prop("checked") == false) {
                                                return true;
                                            }

                                        }

                                        if (tmpData !== '') {
                                            tmpData += separateString;
                                        }

                                        // 取得タイプ 1 or Null(default) :val()方式, 2:text()形式
                                        if (defaultFormJson[j]['x_type'] === 2) {
                                            tmpData += $(this).text().trim();
                                        } else {
                                            tmpData += $(this).val().trim();
                                        }
                                    });
                                }

                                // セレクタ取得形式
                                if (useGetType === 2) {
                                    $(defaultFormJson[j]['x_target']).each(function () {
                                        if (tmpData !== '') {
                                            tmpData += separateString;
                                        }

                                        // 取得タイプ 1 or Null(default) :val()方式, 2:text()形式
                                        if (defaultFormJson[j]['x_type'] === 2) {
                                            tmpRegexData = $(this).text().trim();
                                        } else {
                                            tmpRegexData = $(this).val().trim();
                                        }
                                    });
                                }

                                $('#' + pdHiddenName).contents().find('#pd' + escapeSelectorString(defaultFormJson[j]['tag_name'])).val(tmpData);

                            }

                            $('#' + pdHiddenName).contents().find('#shadowForm').submit();

                        } catch (e) {
                            isError = true;

                            $(submitButton).prop("disabled", false);

                            $(parentFormName).submit();
                            shadowSubmited = false;
                        }
                        return false;
                    });
                }
            })
            .appendTo('body');
    });
})();
</script>

<?php
  // お問い合わせ
  get_template_part('_action');
  
  // 3つのポイント
  get_template_part('_purchase');
  
  // お問い合わせ
  get_template_part('_action2');
  
  // 店舗
  get_template_part('_shopinfo');
  
  // フッター
  get_footer();