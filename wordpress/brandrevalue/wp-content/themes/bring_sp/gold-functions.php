<?php

	/************/
	/* 定数定義 */
	/************/
	const DAY_BORDER_HOUR   = 10;                   // システム内の日付変更時間
	const JSON_GET_URL      = 'https://www.net-japan.co.jp/system/upload/netjapan/export/'; // JSON取得先
	const JSON_GET_PREFIX   = 'price_';             // JSON取得先ファイルの接頭語
	const JSON_GET_SAFFIX   = '_0930.json';         // JSON取得先ファイルの接尾語
	const PRICES_DIR        = '/prices/';           // サーバー内の金額ファイルの設置ディレクトリ
	const JSON_LOCAL_SAFFIX = '.json';              // サーバー内の金額ファイルの接尾語
	const MAX_DATE_GOBACK   = 14;                   // 遡る最大日数

	/************/
	/* 関数定義 */
	/************/
	// URLで指定したページ情報取得
	function getApiDataCurl($url)
	{
		$option = array(
			CURLOPT_RETURNTRANSFER => true, //文字列として返す
			CURLOPT_TIMEOUT		=> 3, // タイムアウト時間
		);

		$ch = curl_init($url);
		curl_setopt_array($ch, $option);

		$json	= curl_exec($ch);
		$info	= curl_getinfo($ch);
		$errorNo = curl_errno($ch);

		// OK以外はエラーなので空白を返す
		if ($errorNo !== CURLE_OK) {
			// 詳しくエラーハンドリングしたい場合はerrorNoで確認
			// タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
			return '';
		}

		// 200以外のステータスコードは失敗とみなし空白を返す
		if ($info['http_code'] !== 200) {
			return '';
		}

		// 文字列から変換
		//$jsonArray = json_decode($json, true);

		return $json;
	}

	// 金額取得関数
	function getPrices($fileNameBase) {

		// サーバー内の金額ファイル名生成
		$localFileName = __DIR__ . PRICES_DIR . $fileNameBase . JSON_LOCAL_SAFFIX;

		$json = '';

		if (!file_exists($localFileName)) {
		// サーバー内に金額ファイルがない場合、JSON取得して作成

			// JSON取得先のURL生成
			$apiFileName = JSON_GET_URL . JSON_GET_PREFIX . $fileNameBase . JSON_GET_SAFFIX;

			// JSONを取得できた場合、サーバー内に金額ファイル保存
			if ($json = getApiDataCurl($apiFileName)) {
				// ファイル保存（失敗時はfalseを返却)
				if (!file_put_contents($localFileName, $json)) {
					return FALSE;
				}
			}
		} else {
		// サーバー内に金額ファイルがある場合、読み込む
			if (is_readable($localFileName)) $json = file_get_contents($localFileName);
		}

		return $json;
	}

	$time = new DateTime();
	$time->setTimestamp(time())->setTimezone(new DateTimeZone('Asia/Tokyo'));
	$hour = $time->format('G'); // 現在時

	$data = array();   // データ

	for ($dateCnt = 0; $dateCnt < MAX_DATE_GOBACK; $dateCnt++) {

		if ($hour < DAY_BORDER_HOUR) {
		// 日付変更時より前なら一日前分
			$fileNameBase = date('Ymd', strtotime('-'.($dateCnt + 1).' day'));
		} else {
		// 日付変更時以降なら当日分
			$fileNameBase = date('Ymd', strtotime('-'.$dateCnt.' day'));
		}

		// jsonを取得できたらデコードして繰り返し終了
		if ($json = getPrices($fileNameBase)) {
			$data = json_decode($json, true);
			break;
		}
	}

	$market = array();
	$price  = array();

	if (!empty($data['data']['market'])) $market = $data['data']['market'];
	if (!empty($data['data']['nj_buy'])) $price  = $data['data']['nj_buy'];
?>
<script>
	// 整数判定関数
	function isInteger(x) {
		x = parseFloat(x);
		return Math.round(x) === x;
	}

	// 計算結果表示
	function autoCalc() {

		if ($('#unit_price').text() !== '-' && $('#copy_metal_weight').text() !== '-') {

			var $result = parseFloat($('#unit_price').text().replace(/,/g, '')) * parseFloat($('#copy_metal_weight').text());

			$('#calculation_result').text($result.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
		}
	}

	var $prices = new Object();
	$prices['au'] = <?= json_encode($price['au_scrap'], JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT); ?>;
	$prices['pt'] = <?= json_encode($price['pt_scrap'], JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT); ?>;
	$prices['ag'] = <?= json_encode($price['ag_scrap'], JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT); ?>;

	$(function(){

		// 品種選択時、品位のプルダウン作成
		$("input[name='radio_kind']").change(function() {

			$('#dignity option').remove();

			$kind = $(this).data('kind');

			$options = $.map($prices[$kind], function ($arr) {
				$option = $('<option>', {value: $arr['price'].replace(/,/g, ''), text: $arr['name']});
				return $option;
			});

			$options.unshift($('<option>', {value: '-', text: '選択してください'}));

			$('#dignity').append($options);

			$('#unit_price').text('-');
			$('#calculation_result').text('-');
		});

		// 品位選択時、単価と自動計算
		$("#dignity").change(function() {

			if ($(this).val() === '-') {
				$('#unit_price').text('-');
				$('#calculation_result').text('-');
			} else {
				$('#unit_price').text($(this).val().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
				autoCalc();
			}
		});

		// 重量入力時、重量と自動計算
		$("#input_metal_weight").keyup(function() {

			var $input_value = $(this).val().replace(/[０-９]/g, function(s) {
				return String.fromCharCode(s.charCodeAt(0) - 65248);
			});

			if ($input_value == '') {
			// 入力が空の場合
				$('#copy_metal_weight').text('-');
				$('#calculation_result').text('-');
				$('#input_metal_weight_err').hide();

			} else if (isInteger($input_value)) {
			// 入力が整数の場合
				$('#copy_metal_weight').text($input_value);
				autoCalc();
				$('#input_metal_weight_err').hide();
			} else {
			// 上記以外の場合
				$('#copy_metal_weight').text('-');
				$('#calculation_result').text('-');
				$('#input_metal_weight_err').show();
			}
		});
	});
</script>

					<div class="gold_bnr"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/gold/gold_bnr.jpg" alt="イエローゴールド" ></div>
		<h2 class="obi_tl_gold">本日の価格情報 <span class="gold_date"><?= $market['price_date'] ?>（10:00更新）現在</span></h2>
		<ul class="gold_box_top">
			<li>
				<h4>金<span>-GOLD-</span></h4>
				<p class="gold_mp"><?= $price['ingod']['au']['price'] ?>円<span>(前日比 <span class="f_red"><?= $price['ingod']['au']['diff'] ?></span>円)</span></p>
			</li>
			<li>
				<h4>銀<span>-SILVER-</span></h4>
				<p class="gold_mp"><?= $price['ingod']['ag']['price'] ?>円<span>(前日比 <span class="f_red"><?= $price['ingod']['ag']['diff'] ?></span>円)</span></p>
			</li>
			<li>
				<h4>Pt<span>-PLATINUM-</span></h4>
				<p class="gold_mp"><?= $price['ingod']['pt']['price'] ?>円<span>(前日比 <span class="f_red"><?= $price['ingod']['pt']['diff'] ?></span>円)</span></p>
			</li>
			<li>
				<h4>Pd<span>-PALLADIUM-</span></h4>
				<p class="gold_mp"><?= $price['ingod']['pd']['price'] ?>円<span>(前日比 <span class="f_red"><?= $price['ingod']['pd']['diff'] ?></span>円)</span></p>
			</li>
		</ul>
		<div class="gold_container">
			<div class="gold_box_wrap01 gft_l">
				<h4>金<span>-GOLD-</span></h4>
				<ul class="gold_inner_list">
<?php foreach ($price['au_scrap'] as $p): ?>
					<li<?php if($p['bold']): ?> class="pick_list"<?php endif; ?>><?= str_replace(' ', '<br>', $p['name']) ?><span><?= $p['price'] ?>円/g</span></li>
<?php endforeach; ?>
				</ul>
			</div>
			<div class="gold_box_wrap02 gft_l">
				<h4>金<span>-GOLD-</span>Pt<span>-PLATINUM-</span></h4>
				<ul class="gold_inner_list ">
<?php foreach ($price['pk_combi'] as $p): ?>
					<li<?php if($p['bold']): ?> class="pick_list"<?php endif; ?>><?= str_replace(' ', '<br>', $p['name']) ?><span><?= $p['price'] ?>円/g</span></li>
<?php endforeach; ?>
				</ul>
			</div>
			<div class="gold_box_wrap03 gft_l">
				<h4>Pt<span>-PLATINUM-</span></h4>
				<ul class="gold_inner_list">
<?php foreach ($price['pt_scrap'] as $p): ?>
					<li<?php if($p['bold']): ?> class="pick_list"<?php endif; ?>><?= str_replace(' ', '<br>', $p['name']) ?><span><?= $p['price'] ?>円/g</span></li>
<?php endforeach; ?>
				</ul>
				<h4 class="gl_sliv">銀<span>-PLATINUM-</span></h4>
				<ul class="gold_inner_list gl_sliv">
<?php foreach ($price['ag_scrap'] as $p): ?>
					<li<?php if($p['bold']): ?> class="pick_list"<?php endif; ?>><?= str_replace(' ', '<br>', $p['name']) ?><span><?= $p['price'] ?>円/g</span></li>
<?php endforeach; ?>
				</ul>
			</div>
		</div>
		<!--買取評価チェックコンテンツ-->

		<div class="gold_check">
			<h3>本日の相場を自動計算</h3>
			<form action="" name="sel_form" id="sel_form" class="mark_wrap">
				<ul class="gold_mark">
					<li>
						<p class="gold_input_ttl"><span>●</span>貴金属製品の品種をお選びください。</p>
						<label>
							<input type="radio" name="radio_kind" class="radio01-input" data-kind="au">
							<span class="radio01-parts">金</span> </label>
						<label>
							<input type="radio" name="radio_kind" class="radio01-input" data-kind="pt">
							<span class="radio01-parts">プラチナ</span> </label>
						<label>
							<input type="radio" name="radio_kind" class="radio01-input" data-kind="ag">
							<span class="radio01-parts">銀</span> </label>
					</li>
					<li>
						<p class="gold_input_ttl"><span>●</span>貴金属製品の部位をお選びください。</p>
						<select id="dignity" name="color_id">
							<option value="">選択して下さい</option>
						</select>
					</li>
					<li>
						<p class="gold_input_ttl"><span>●</span>貴金属製品の重量を入力してください。</p>
						<input type="text" id="input_metal_weight" style="width: 100px;" />g
						&nbsp;<span id="input_metal_weight_err" style="color:red; display:none;">整数で入力してください。</span>
					</li>
				</ul>
				<div class="mark_result">
				<p>本日の相場×重量を自動計算</p>
				<table>
				<tr>
				<th width="30%"><span class="ico_fix01">¥</span><span id="unit_price">-</span></th>
				<td>×</td>
				<th width="30%"><span id="copy_metal_weight">-</span>&ensp;<span class="ico_fix02">g</span></th>
				<td>=</td>
				<th width="30%"><span class="ico_fix01">¥</span><span id="calculation_result">-</span></th>
				</tr>
				</table>


				</div>

			</form>
		</div>
