<?php
    get_header();
    $categoryLinkArray = array(
        '時計' => array('url' => home_url('cat/watch'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_watch.jpg', 'alt' => '時計買取実績', 'text' => 'その他のブランド時計参考価格公開中'),
        'バッグ' => array('url' => home_url('cat/bag'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_bag.jpg', 'alt' => '', 'text' => 'その他のブランドバッグ参考価格公開中'),
        'BJ' => array('url' => home_url('cat/brandjewelery'), 'image' => get_s3_template_directory_uri() . '/img/ranking_cate_jewelry_sp.jpg', 'alt' => '', 'text' => 'その他のブランドジュエリー参考価格公開中'),
        '財布' => array('url' => home_url('cat/wallet'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_bag.jpg', 'alt' => '', 'text' => 'その他のブランド財布参考価格公開中'),
        '金' => array('url' => home_url('cat/gold'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_bag.jpg', 'alt' => '', 'text' => 'その他の金・プラチナ参考価格公開中'),
        '洋服・毛皮' => array('url' => home_url('cat/outfit'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_bag.jpg', 'alt' => '', 'text' => 'その他の洋服・毛皮参考価格公開中'),
        '宝石' => array('url' => home_url('cat/gem'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_bag.jpg', 'alt' => '', 'text' => 'その他のジュエリー参考価格公開中'),
        '骨董品' => array('url' => home_url('introduction/antique'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_bag.jpg', 'alt' => '', 'text' => 'その他の骨董品参考価格公開中'),
        '靴' => array('url' => home_url('cat/shoes'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_bag.jpg', 'alt' => '', 'text' => 'その他のブランド靴参考価格公開中'),
        'アンティークロレックス' => array('url' => home_url('watch/antique_rolex'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_bag.jpg', 'alt' => '', 'text' => 'その他のアンティークロレックス参考価格公開中'),
        'ダイヤモンド' => array('url' => home_url('cat/diamond'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_bag.jpg', 'alt' => '', 'text' => 'その他のダイヤモンド参考価格公開中')
    );

    $brandNameArray = array(
        'エルメス' => 'HERMES<span>エルメス</span>',
        'ロレックス' => 'ROLEX<span>ロレックス</span>',
        'オーデマピゲ' => 'AUDEMARS PIGUET<span>オーデマピゲ</span>',
        'ハリーウィンストン' => 'HARRY WINSTON<span>ハリーウィンストン</span>',
        '小判' => '小判',
        'カルティエ' => 'Cartier<span>カルティエ</span>',
        'ルイヴィトン' => 'LOUIS VUITTON<span>ルイヴィトン</span>',
        'リシャールミル' => 'RICHARD MILLE<span>リシャールミル</span>');

    $under5categorytext = 'その他アイテムの参考価格公開中';

    // 毎月変える部分
    $yymm = '2019';
    $rankTopImageName = "ranking_sp_mv{$yymm}.jpg";

    $spreadSheetData = '1	時計	ロレックス	サブマリーナ デイト  116610LV	¥1,360,000	¥1,460,000	¥1,620,000
2	BJ	ハリーウィンストン	リリークラスター ドロップピアス	¥1,300,000	¥1,350,000	¥1,400,000
3	時計	ロレックス	コスモグラフ デイトナ  116500LN	¥2,300,000	¥2,360,000	¥2,460,000
4	バッグ	エルメス	バーキン30 オーストリッチ パーシュマン	¥2,300,000	¥2,500,000	¥2,720,000
5	BJ	カルティエ	ジュストアンクル 	¥498,000	¥520,000	¥550,000
6	時計	パテックフィリップ	パテック・フィリップ ワールドタイム  5130	¥3,160,000	¥3,300,000	¥3,500,000
7	BJ	ハリーウィンストン	マイクロパヴェダイヤリング	¥2,300,000	¥2,400,000	¥2,450,000
8	バッグ	ルイヴィトン	ダミエ ネヴァーフル MM N41358	¥115,000	¥128,000	¥140,000
9	時計	オメガ	デ・ヴィル プレステージ コーアクシャル	¥330,000	¥360,000	¥400,000
10	バッグ	シャネル	ラムスキン トレンディ CC	¥250,000	¥280,000	¥300,000
';

    $pickupBrand = 'ヴァンクリーフ＆アーペル';
    $pickupLink = home_url('cat/gem/vancleefarpels');

    $pickupData = 'アルハンブラ パピヨン ターコイズ ネックレス	\140,000
マジックアルハンブラ シェルネックレス	\300,000
スイートアルハンブラ ピアス マザーオブパール	\180,000
ヴィンテージアルハンブラ ブレスレット オニキス5Ｐ	\300,000
    ';
    // 毎月変える部分ここまで

    $ignoreRegExp = '/[\\\\¥,]/';
    $spreadSheetDataArray = explode("\n", $spreadSheetData);
    $spreadSheetDataArray = array_map(function($data) use ($ignoreRegExp) {
        $data = explode("\t", $data);
        $data = array(
            'rank' => $data[0],
            'category' => $data[1],
            'brand' => $data[2],
            'item_name' => $data[3],
            'a_price' => (int) preg_replace($ignoreRegExp, '', $data[4]),
            'b_price' => (int) preg_replace($ignoreRegExp, '', $data[5]),
            'j_price' => (int) preg_replace($ignoreRegExp, '', $data[6]),
        );
        return $data;
    }, $spreadSheetDataArray);
    $pickupDataArray = explode("\n", $pickupData);
    $pickupDataArray = array_map(function($data) use ($ignoreRegExp) {
        $data = explode("\t", $data);
        $data = array(
            'item_name' => $data[0],
            'price' => (int) preg_replace($ignoreRegExp, '', $data[1])
        );
        return $data;
    }, $pickupDataArray);
?>
<!--page-ranking-->

<h2><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/ranking/<?php echo $rankTopImageName ?>" alt="ブランドリバリュー買取注目ランキング"></h2>
<div class="ranking_page">
    <ul class="rankingbox_top">
        <?php for ($i = 0; $i < 4; $i++) : ?>
            <?php
                $rank = $i + 1;
                $didit2Rank = sprintf("%02d", $rank);
                $categoryData = $categoryLinkArray[$spreadSheetDataArray[$i]['category']];
                if (!empty($brandNameArray[$spreadSheetDataArray[$i]['brand']])) {
                    $brandName = $brandNameArray[$spreadSheetDataArray[$i]['brand']];
                } else {
                    $brandName = $spreadSheetDataArray[$i]['brand'];
                }
            ?>
            <li>
                <div class="ranking_box_wrap">
                    <div class="ranking_img_box">
                        <p class="ranking_ico">
                            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/ranking/badge<?php echo $didit2Rank ?>.png">
                        </p>
                        <p class="ranking_item">
                            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/ranking/ranking_item<?php echo $didit2Rank ?>.png?<?php echo $yymm ?>" alt="<?php echo $spreadSheetDataArray[$i]['item_name'] ?>">
                        </p>
                    </div>
                    <div class="ranking_info_box">
                        <p class="itemname_top"><?php echo $brandName  ?></p>
                        <p class="itemname_btm"><?php echo $spreadSheetDataArray[$i]['item_name'] ?></p>
                        <div class="price_top">
                            <p><span>A社</span><?php echo number_format($spreadSheetDataArray[$i]['a_price']) ?>円</p>
                            <p><span>B社</span><?php echo number_format($spreadSheetDataArray[$i]['b_price']) ?>円</p>
                        </div>
                        <div class="price_btm">
                            <p>BRAND REVALUE 買取価格</p>
                            <p>~<?php echo number_format($spreadSheetDataArray[$i]['j_price']) ?>円</p>
                        </div>
                    </div>
                </div>
                <?php if ($rank < 4) : ?>
                    <a href="<?php echo $categoryData['url'] ?>"><img data-src="<?php echo $categoryData['image'] ?>" alt="<?php echo $categoryData['alt'] ?>"></a>
                <?php elseif ($rank === 4) : ?>
                    <p class="link"><a href="<?php echo $categoryData['url'] ?>"><?php echo $categoryData['text'] ?></a></p>
                <?php endif; ?>
            </li>
        <?php endfor; ?>
    </ul>

    <ul class="rankingbox_btm">
        <?php for ($i = 4; $i < 10; $i++) : ?>
            <?php
                $rank = $i + 1;
                $didit2Rank = sprintf("%02d", $rank);
                $categoryData = $categoryLinkArray[$spreadSheetDataArray[$i]['category']];
                if (!empty($brandNameArray[$spreadSheetDataArray[$i]['brand']])) {
                    $brandName = $brandNameArray[$spreadSheetDataArray[$i]['brand']];
                } else {
                    $brandName = $spreadSheetDataArray[$i]['brand'];
                }
            ?>
            <li>
                <p class="ranking_ico">
                    <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/ranking/badge<?php echo $didit2Rank ?>.png">
                </p>
                <p class="ranking_item">
                    <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/ranking/ranking_item<?php echo $didit2Rank ?>.png?<?php echo $yymm ?>" alt="<?php echo $spreadSheetDataArray[$i]['item_name'] ?>">
                </p>
                <div class="ranking_info_box">
                    <p class="itemname_top"><?php echo $brandName ?></p>
                    <p class="itemname_btm"><?php echo $spreadSheetDataArray[$i]['item_name'] ?></p>
                    <div class="price_top">
                        <p><span>A社：</span><?php echo number_format($spreadSheetDataArray[$i]['a_price']) ?>円</p>
                        <p><span>B社：</span><?php echo number_format($spreadSheetDataArray[$i]['b_price']) ?>円</p>
                    </div>
                    <div class="price_btm">
                        <p>BRAND REVALUE 買取価格</p>
                        <p>~<?php echo number_format($spreadSheetDataArray[$i]['j_price']) ?>円</p>
                    </div>
                </div>
                <p class="link"><a href="<?php echo $categoryData['url'] ?>"><?php echo $under5categorytext ?></a></p>
            </li>
        <?php endfor; ?>
    </ul>
</div>

<div class="ranking-pickup">
    <div class="ranking-pickup-title">
        <h2><img data-src="<?php echo get_s3_template_directory_uri() . '/img/pickup/pickup_head_sp.jpg' ?>" alt="今月の買取強化PICK UPブランド <?php echo $pickupBrand ?>" /></h2>
    </div>
    <ul class="archive_purchase">
        <?php for ($i = 0; $i < 4; $i++) : ?>
            <?php $digit2 = sprintf("%02d", $i+1); ?>
            <li>
                <p class="kaitori-image"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/pickup/pickup_<?php echo $digit2 ?>.jpg" alt=""></p>
                <p class="kaitori-title"><?php echo $pickupDataArray[$i]['item_name'] ?></p>
                <p class="kaitori-price-header">買取価格</p>
                <P class="kaitori-price"> ¥<?php echo number_format($pickupDataArray[$i]['price']) ?></P>
            </li>
        <?php endfor; ?>
    </ul>
    <p>※価格は買取時の商品の状態や付属品の有無に基づいており、最高価格の目安ではありません。（古品、付属品の不足した商品も含まれています。）</p>
    <div class="ranking-pickup-link">
        <a href="<?php echo $pickupLink ?>">その他、<?php echo $pickupBrand ?>の買取実績はコチラ</a>
    </div>
</div>

<div class="kijyun_cnt ">
    <h2><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/toriatsukaicat_tl.png " alt="取扱カテゴリー "></h2>
    <ul class="cat_list">
        <li>
            <a href="<?php echo home_url('cat/gold'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat01.png" alt="金・プラチナ"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/gem'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat02.png" alt="宝石"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/watch'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat03.png" alt="時計"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/bag'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat04.png" alt="バッグ"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/outfit'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat05.png" alt="洋服・毛皮"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/wallet'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat06.png" alt="ブランド・財布"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/shoes'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat07.png" alt="靴"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/diamond'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat08.png" alt="ダイヤモンド"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/antique'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat09.png" alt="骨董品"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/mement'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/btn-memento_sp.png" alt="遺品"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/brandjewelery'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat_bj.png" alt="ブランドジュエリー"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/antique_rolex'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/catanti_rolex.png" alt="アンティークロレックス"></a>
        </li>
    </ul>
</div>
<div class="point_cnt ">
    <h2><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/top_point.png " alt="ブランドリバリュー当店の3つのポイント "></h2>
</div>

<?php
    // お問い合わせ
    get_s3_template_part('_action');



    // 店舗
    get_s3_template_part('_shopinfo');

    // フッター
    get_footer();
