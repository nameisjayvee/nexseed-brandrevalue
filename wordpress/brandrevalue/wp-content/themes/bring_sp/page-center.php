<?php

get_header(); ?>

    <div class="mv_area "> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center_mv.png " alt="店舗案内"> </div>
    <div class="shop_page">
        <h2 class="shop_bold"><br> ブランド買取店をお探しのお客様へ。
            <br>ブランド買取なら高価買取のBRAND REVALUE(ブランドリバリュー)へ。</h2>
        <p>最寄り駅は、「原宿駅」、「渋谷駅」、千代田線・副都心線「明治神宮前駅」。BRAND REVALUE買取センターは、完全予約制のプライベート買取センターとなります。「買取時に他の人の目が気になる」「落ち着いて査定を受けたい」というお客様の要望からプライベート買取ブースを設置しております。 BRAND REVALUE買取センターでは、お客様が快適に過ごして頂ける空間と最上級のサービスでおもてなし致します。</p>
    </div>
    <div class="mv_area main-gallery" id="main-gallery">
        <div class="gallery-cell"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center/center01.png" alt="買取センター">
        </div>
        <div class="gallery-cell"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center/center02.png" alt="買取センター">
        </div>
        <div class="gallery-cell"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center/center03.png" alt="買取センター">
        </div>
        <div class="gallery-cell"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center/center04.png" alt="買取センター">
        </div>
    </div>
    <!--<div class="link_gr ">
        <a href="https://www.google.co.jp/maps/uv?hl=ja&pb=!1s0x60188be68aa53555%3A0x7eb5bd0982deca18!2m22!2m2!1i80!2i80!3m1!2i20!16m16!1b1!2m2!1m1!1e1!2m2!1m1!1e3!2m2!1m1!1e5!2m2!1m1!1e4!2m2!1m1!1e6!3m1!7e115!4shttps%3A%2F%2Flh5.googleusercontent.com%2Fp%2FAF1QipNbicAD6rZpXueDIS-O1YR57JJcS6d599LpezKM%3Dw264-h176-k-no!5z44OW44Op44Oz44OJ44Oq44OQ44Oq44Ol44O8IOmKgOW6pyAtIEdvb2dsZSDmpJzntKI&imagekey=!1e10!2sAF1QipNbWblj6EBwQxMqtXGfryq_WaBUIVHU6df6TnfL&sa=X&ved=2ahUKEwjN18KS6sPdAhXFvbwKHTCVBEEQoiowDXoECAgQCQ" target="_blank" class="hiragino">店舗内をみる<i class="fas fa-caret-right"></i></a>

    </div>-->
    <div class="shop_page">
        <h2>買取センター(完全予約制)　店舗案内</h2>
        <div><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center/center_map.jpg" alt="銀座店"></div>
        <table>
            <tr>
                <th>住所</th>
                <td class="shop_map">
                    東京都渋谷区神宮前6-23-3 第9SYビル5階</td>
            </tr>
            <tr>
                <th>営業時間</th>
                <td>11：00～21：00</td>
            </tr>
            <tr>
                <th>電話番号</th>
                <td><a href="tel:0120-970-060">0120-970-060</a></td>
            </tr>
            <tr>
                <th>駐車場</th>
                <td> - </td>
            </tr>
        </table>
    </div>
<div class="link_gr ">
        <a href="https://goo.gl/maps/CfkLnPVXGZ22" target="_blank" class="hiragino">地図アプリでみる<i class="fas fa-caret-right"></i></a>
</div>
    <div class="shop_page">
        <h3 class="mb30">買取センターまでのアクセス</h3>
        <ul class="map_root">
            <li> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_center01.png" alt="原宿駅 表参道口" />
                <div class="root_tx_bx">
                    <p class="root_ttl"><span>1</span>原宿駅 表参道口</p>
                    <p>原宿駅から、ラフォーレ方面へ向かいます。交差点を右折し、明治通りへ出ます。</p>
                </div>
            </li>
            <li> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_center02.png" alt="A5出口" />
                <div class="root_tx_bx">
                    <p class="root_ttl"><span>2</span>神宮前交差点を右折</p>
                    <p>右手に明治神宮前駅の7番出口があります。地下鉄をご利用の方は、この出口をご利用ください。</p>
                </div>
            </li>
            <li> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_center03.png" alt="明治通りを渋谷方向に直進" />
                <div class="root_tx_bx">
                    <p class="root_ttl"><span>3</span>明治通りを渋谷方向に直進</p>
                    <p>5分ほど明治通りを直進していただき、Right-on奥の歩道橋まで進みます。</p>
                </div>
            </li>
            <li> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_center04.png" alt="第9SYビル(エントランス)" />
                <div class="root_tx_bx">
                    <p class="root_ttl"><span>4</span>第9SYビル(エントランス)</p>
                    <p>１FにEDIFICE様がある第9SYビルの5階がブランドリバリュー買取センターです。</p>
                </div>
            </li>
        </ul>
        <h2 class="mb30">駐車場案内</h2>
        <p class="ant_ttl">NPC24H原宿第２パーキング</p>
        <table class="parking">
            <tr>
                <th>住所</th>
                <td>東京都渋谷区神宮前6丁目25−14</td>
            </tr>
            <tr>
                <th>営業時間<br>料金</th>
                <td>24時間営業</td>
            </tr>
        </table>
        <p class="ant_ttl">ポケットパーク 神宮前駐車場</p>
        <table class="parking">
            <tr>
                <th>住所</th>
                <td>東京都渋谷区神宮前6丁目12−17</td>
            </tr>
            <tr>
                <th>営業時間<br>料金</th>
                <td>24時間営業</td>
            </tr>

        </table>
        <p class="ant_ttl">京セラ原宿ビルコイン 駐車場</p>
        <table class="parking">
            <tr>
                <th>住所</th>
                <td>東京都渋谷区神宮前6丁目27−8</td>
            </tr>
            <tr>
                <th>営業時間<br>料金</th>
                <td>24時間営業</td>
            </tr>

        </table>
        <div class="takuhai_cvbox tentou">
            <p class="cv_tl">ブランドリバリュー買取センターで<br>プライベート買取を予約する</p>
            <div class="custom_tel takuhai_tel">
                <a href="tel:0120-970-060">
                    <div class="tel_wrap">
                        <div class="telbox01"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/customer/telico.png" alt="お申し込みはこちら">
                            <p>お電話からでもお申込み可能！<br>ご不明な点は、お気軽にお問合せ下さい。 </p>
                        </div>
                        <div class="telbox02"> <span class="small_tx">【受付時間】11:00 ~ 21:00</span><span class="ted_tx"> 年中無休</span>
                            <p class="fts25">0120-970-060</p>
                        </div>
                    </div>
                </a>
            </div>
            <p><a href="<?php echo home_url('purchase/visit-form'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/tentou_cv.png" alt="店頭買取お申し込み"></a></p>
        </div>



        <section>
            <h3 class="mb30">買取センター　店内紹介・内装へのこだわり</h3>
            <div class="shop_comm"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center_kodawari01.png" alt="店内" />
                <p>ブランドリバリュー買取センターでは、お客さまをお待たせすることなくサービスを受けて頂きたく完全予約制とさせて頂いております。<br>プライベートな空間と最上級のサービスでおもてなし致します。</p>
            </div>
            <div class="shop_comm"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center_kodawari02.png" alt="店内" />
                <p>各お客さまへ専属バイヤーが対応させて頂きます。経験豊富で信頼の置ける当店屈指の専属バイヤーは超高額品やレア商品など専門知識の必要なお品物も丁寧に査定させて頂きます。</p>
            </div>
            <div class="shop_comm"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center_kodawari03.png" alt="店内" />
                <p>お客さまが人目を気にせず安心して査定して頂けるように完全個室の査定ブースを設けさせて頂きました。持ち込み頂いたお品物についてのご質問やご相談・要望などありましたら遠慮なく専属バイヤーへお申し付け下さい。</p>
            </div>
        </section>
        <section>
            <h3>買取センター　鑑定士のご紹介</h3>
            <div class="staff_bx"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/staff01.png" alt="査定士 森" />
                <p class="staff_name">森 雄大 鑑定士</p>
                <h4>仕事における得意分野</h4>
                <p>私の得意分野は、オールジャンルです。特に、買取店によって査定金額に差が出てしまう、エルメスやシャネル、ロレックス、宝石に関しては、頻繁に業者間オークションに参加し、知識向上に努めております。</p>
                <h4>自身の強み</h4>
                <p>私の強みは、高額査定にあります。長年、古物業界に携わることにより、独自の販売ルート　を構築して参りました。その為、査定金額には、自信があります。業界最高峰の査定金額でお客様にご満足いただけることをお約束いたします。 </p>
                <h4>仕事にかける思いと心がけ</h4>
                <p>お客様に「ありがとう」と笑顔で言っていただけることが、私の喜びです。お客様にご満足いただけるよう、高品質のサービスをご提供できるよう店舗運営・スタッフ教育に努めてまいります。</p>
            </div>

            <div class="staff_bx"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/staff02.png" alt="佐藤 昂太 鑑定士" />
                <p class="staff_name">佐藤 昂太 鑑定士</p>
                <h4>仕事における得意分野</h4>
                <p>私の得意分野は、オールジャンルですが、特に相場の変動が激しいジュエリー関連と時計関連です。知識向上のため買取品だけでなく、経済や相場も含め日々勉強しております。</p>
                <h4>自身の強み</h4>
                <p>どんな良いブランドでも日々相場は変動しお買取り金額にも影響致します。日々変動するお買取り相場からお客様に損させることなく最高水準でのお買取り金額のご案内ができますよう弊社独自の販売ルートの確保をさせていただいております。</p>
                <h4>仕事にかける思いと心がけ</h4>
                <p>ご売却される理由は様々ございますが、お客様の大切なお品物を決して損させる事なくお買取りさせていただいております。どこよりも高くお買取りができるというのが私のできる最大のサービスであると考えております。 </p>
            </div>
        </section>
        <section class="shopkaitori_jisseki">
            <h3>
                買取センターの買取実績
            </h3>
            <div class="topItem_cnt ">
                <div class="kaitori_box">
                    <ul class="tab_menu">
                        <li><a href="#tab1">ブランド</a></li>
                        <li><a href="#tab2">金</a></li>
                        <li><a href="#tab3">宝石</a></li>
                        <li><a href="#tab4">時計</a></li>
                        <li><a href="#tab5">バック</a></li>
                    </ul>
                    <!--ブランド-->
                    <div class="tab_cont" id="tab1">
                        <ul id="box-jisseki" class="list-unstyled clearfix">
                            <!-- barand1 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/001.jpg" alt=""></p>
                                    <p class="itemName">オーデマピゲ　ロイヤルオークオフショアクロノ 26470OR.OO.A002CR.01 ゴールド K18PG</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：3,080,000円<br>
                                        <span class="blue">B社</span>：3,000,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">3,200,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>200,000円</p>
                                </div>
                            </li>

                            <!-- brand2 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/002.jpg" alt=""></p>
                                    <p class="itemName">パテックフィリップ　コンプリケーテッド ウォッチ 5085/1A-001</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：1,470,000円<br>
                                        <span class="blue">B社</span>：1,380,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">1,650,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>270,000円</p>
                                </div>
                            </li>

                            <!-- brand3 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/003.jpg" alt=""></p>
                                    <p class="itemName">パテックフィリップ　コンプリケーション 5130G-001 WG</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：2,630,000円<br>
                                        <span class="blue">B社</span>：2,320,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">3,300,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>800,000円</p>
                                </div>
                            </li>
                            <!-- brand4 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/004.jpg" alt=""></p>
                                    <p class="itemName">パテックフィリップ　ワールドタイム 5130R-001</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：2,800,000円
                                        <br>
                                        <span class="blue">B社</span>：2,500,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">3,300,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>800,000円</p>
                                </div>
                            </li>

                            <!-- brand5 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/005.jpg" alt=""></p>
                                    <p class="itemName">シャネル　ラムスキン　マトラッセ　二つ折り長財布</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：85,000円<br>
                                        <span class="blue">B社</span>：75,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">100,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>25,000円</p>
                                </div>
                            </li>
                            <!-- brand6 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/006.jpg" alt=""></p>
                                    <p class="itemName">エルメス　ベアンスフレ　ブラック</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：180,000円<br>
                                        <span class="blue">B社</span>：157,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">210,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>53,000円</p>
                                </div>
                            </li>
                            <!-- brand7 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/007.jpg" alt=""></p>
                                    <p class="itemName">エルメス　バーキン30　トリヨンクレマンス　マラカイト　SV金具</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：1,200,000円<br>
                                        <span class="blue">B社</span>：1,050,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">1,350,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>300,000円</p>
                                </div>
                            </li>
                            <!-- brand8 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/008.jpg" alt=""></p>
                                    <p class="itemName">セリーヌ　ラゲージマイクロショッパー</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：210,000円<br>
                                        <span class="blue">B社</span>：180,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">240,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>60,000円</p>
                                </div>
                            </li>
                            <!-- brand9 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/009.jpg" alt=""></p>
                                    <p class="itemName">ルイヴィトン　裏地ダミエ柄マッキントッシュジャケット</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：34,000円<br>
                                        <span class="blue">B社</span>：30,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">40,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>10,000円</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!--ブランドここまで-->
                    <!--金-->
                    <div class="tab_cont" id="tab2">
                        <ul id="box-jisseki" class="list-unstyled clearfix">
                            <!-- gold1 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/001.jpg" alt=""></p>
                                    <p class="itemName">K18　ダイヤ0.11ctリング</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：29,750円<br>
                                        <span class="blue">B社</span>：26,250円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">
                                        <!-- span class="small">120g</span -->35,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>8,750円</p>
                                </div>
                            </li>

                            <!-- gold2 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/002.jpg" alt=""></p>
                                    <p class="itemName">K18　メレダイヤリング</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：38,250円<br>
                                        <span class="blue">B社</span>：33,750円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">45,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>11,250円</p>
                                </div>
                            </li>

                            <!-- gold3 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/003.jpg" alt=""></p>
                                    <p class="itemName">K18/Pt900　メレダイヤリング</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：15,300円<br>
                                        <span class="blue">B社</span>：13,500円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">18,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>4,500円</p>
                                </div>
                            </li>

                            <!-- gold4 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/004.jpg" alt=""></p>
                                    <p class="itemName">Pt900　メレダイヤリング</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：20,400円<br>
                                        <span class="blue">B社</span>：18,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">24,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>6,000円</p>
                                </div>
                            </li>

                            <!-- gold5 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/005.jpg" alt=""></p>
                                    <p class="itemName">K18WG　テニスブレスレット</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：34,000円<br>
                                        <span class="blue">B社</span>：30,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">40,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>10,000円</p>
                                </div>
                            </li>

                            <!-- gold6 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/006.jpg" alt=""></p>
                                    <p class="itemName">K18/K18WG　メレダイヤリング</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：25,500円<br>
                                        <span class="blue">B社</span>：22,500円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">30,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>7,500円</p>
                                </div>
                            </li>

                            <!-- gold7 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/007.jpg" alt=""></p>
                                    <p class="itemName">K18ブレスレット</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：23,800円<br>
                                        <span class="blue">B社</span>：21,900円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">28,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>7,000円</p>
                                </div>
                            </li>
                            <!-- gold8 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/008.jpg" alt=""></p>
                                    <p class="itemName">14WGブレスレット</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：52,700円<br>
                                        <span class="blue">B社</span>：46,500円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">62,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>1,5500円</p>
                                </div>
                            </li>
                            <!-- gold9 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/009.jpg" alt=""></p>
                                    <p class="itemName">Pt850ブレスレット</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：27,200円<br>
                                        <span class="blue">B社</span>：24,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">32,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>8,000円</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!--金ここまで-->
                    <!--宝石-->
                    <div class="tab_cont" id="tab3">

                        <ul id="box-jisseki" class="list-unstyled clearfix">
                            <!-- gem1 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/001.jpg" alt=""></p>
                                    <p class="itemName">ダイヤルース</p>
                                    <p class="itemdetail">カラット：1.003ct<br>カラー：E<br>クラリティ：VS-2<br>カット：Good<br>蛍光性：FAINT<br>形状：ラウンドブリリアント</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：380,000円<br>
                                        <span class="blue">B社</span>：355,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">450,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>115,000円</p>
                                </div>
                            </li>

                            <!-- gem2 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/002.jpg" alt=""></p>
                                    <p class="itemName">Pt900 DR0.417ctリング</p>
                                    <p class="itemdetail">カラー：F<br>クラリティ：SI-1<br>カット：VERY GOOD <br>蛍光性：FAINT<br>形状：ラウンドブリリアント<br>メレダイヤモンド0.7ct<br>地金：18金イエローゴールド　5ｇ</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：51,000円<br>
                                        <span class="blue">B社</span>：45,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">60,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>15,000円</p>
                                </div>
                            </li>

                            <!-- gem3 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/003.jpg" alt=""></p>
                                    <p class="itemName">Pt900　DR0.25ctリング</p>
                                    <p class="itemdetail">カラー：H<br>クラリティ:VS-1<br>カット：Good<br>蛍光性：MB<br>形状：ラウンドブリリアント<br></p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：58,000円<br>
                                        <span class="blue">B社</span>：51,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">68,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>17,000円</p>
                                </div>
                            </li>

                            <!-- gem4 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/004.jpg" alt=""></p>
                                    <p class="itemName">K18　DR0.43ct　MD0.4ctネックレストップ</p>
                                    <p class="itemdetail">カラー：I<br>クラリティ：VS-2<br>カット：Good<br>蛍光性：WB<br>形状：ラウンドブリリアント</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：60,000円<br>
                                        <span class="blue">B社</span>：52,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">70,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>18,000円</p>
                                </div>
                            </li>
                            <!-- gem5 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/005.jpg" alt=""></p>
                                    <p class="itemName">ダイヤルース</p>
                                    <p class="itemdetail">カラット：0.787ct<br>カラー：E<br>クラアリティ：VVS-2<br>カット：Good<br>蛍光性：FAINT<br>形状：ラウンドブリリアント</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：220,000円<br>
                                        <span class="blue">B社</span>：190,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">260,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>70,000円</p>
                                </div>
                            </li>
                            <!-- gem6 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/006.jpg" alt=""></p>
                                    <p class="itemName">Pt950　MD0.326ct　0.203ct　0.150ctネックレス</p>
                                    <p class="itemdetail">カラー：F<br>クラリティ：SI-2<br>カット：Good<br>蛍光性：FAINT<br>形状：ラウンドブリリアント</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：100,000円<br>
                                        <span class="blue">B社</span>：90,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">120,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>30,000円</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!--宝石ここまで-->
                    <!--時計-->
                    <div class="tab_cont" id="tab4">

                        <ul id="box-jisseki" class="list-unstyled clearfix">

                            <!-- watch1 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/001.jpg" alt=""></p>
                                    <p class="itemName">パテックフィリップ
                                        <br>カラトラバ 3923</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：850,000円
                                        <br>
                                        <span class="blue">B社</span>：750,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">1,000,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>250,000円</p>
                                </div>
                            </li>

                            <!-- watch2 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/002.jpg" alt=""></p>
                                    <p class="itemName">パテックフィリップ
                                        <br>アクアノート 5065-1A</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：1,700,000円
                                        <br>
                                        <span class="blue">B社</span>：1,500,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">2,000,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>500,000円</p>
                                </div>
                            </li>

                            <!-- watch3 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/003.jpg" alt=""></p>
                                    <p class="itemName">オーデマピゲ
                                        <br>ロイヤルオーク・オフショア 26170ST</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：1,487,500円
                                        <br>
                                        <span class="blue">B社</span>：1,312,500円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">1,750,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>437,500円</p>
                                </div>
                            </li>

                            <!-- watch4 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/004.jpg" alt=""></p>
                                    <p class="itemName">ROLEX
                                        <br>サブマリーナ 116610LV ランダム品番</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：1,105,000円
                                        <br>
                                        <span class="blue">B社</span>：975,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">1,300,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>325,000円</p>
                                </div>
                            </li>
                            <!-- watch5 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/005.jpg" alt=""></p>
                                    <p class="itemName">ROLEX
                                        <br>116505 ランダム品番 コスモグラフ</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：2,422,500円
                                        <br>
                                        <span class="blue">B社</span>：2,137,500円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">2,850,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>712,500円</p>
                                </div>
                            </li>
                            <!-- watch6 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/006.jpg" alt=""></p>
                                    <p class="itemName">ブライトリング
                                        <br>クロノマット44</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：289,000円
                                        <br>
                                        <span class="blue">B社</span>：255,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">340,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>85,000円</p>
                                </div>
                            </li>
                            <!-- watch7 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/007.jpg" alt=""></p>
                                    <p class="itemName">パネライ
                                        <br>ラジオミール 1940</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：510,000円
                                        <br>
                                        <span class="blue">B社</span>：450,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">600,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>150,000円</p>
                                </div>
                            </li>
                            <!-- watch8 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/008.jpg" alt=""></p>
                                    <p class="itemName">ボールウォッチ
                                        <br>ストークマン ストームチェイサープロ CM3090C</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：110,500円
                                        <br>
                                        <span class="blue">B社</span>：97,500円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">130,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>32,500円</p>
                                </div>
                            </li>
                            <!-- watch9 -->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/009.jpg" alt=""></p>
                                    <p class="itemName">ブレゲ
                                        <br>クラシックツインバレル 5907BB12984</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：595,000円
                                        <br>
                                        <span class="blue">B社</span>：525,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">700,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>175,000円</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!--時計ここまで-->
                    <!--バック-->
                    <div class="tab_cont" id="tab5">

                        <ul id="box-jisseki" class="list-unstyled clearfix">

                            <!-- bag1-->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/001.jpg" alt=""></p>
                                    <p class="itemName">エルメス<br />エブリンⅢ　トリヨンクレマンス</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：230,000円
                                        <br>
                                        <span class="blue">B社</span>：200,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">270,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>70,000円</p>
                                </div>
                            </li>

                            <!-- bag2-->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/002.jpg" alt=""></p>
                                    <p class="itemName">プラダ<br />シティトート2WAYバッグ</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：110,000円
                                        <br>
                                        <span class="blue">B社</span>：95,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">132,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>37,000円</p>
                                </div>
                            </li>

                            <!-- bag3-->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/003.jpg" alt=""></p>
                                    <p class="itemName">バンブーデイリー2WAYバッグ</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：100,000円
                                        <br>
                                        <span class="blue">B社</span>：90,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">120,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>30,000円</p>
                                </div>
                            </li>

                            <!-- bag4-->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/004.jpg" alt=""></p>
                                    <p class="itemName">LOUIS VUITTON
                                        <br>モノグラムモンスリGM</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：110,000円
                                        <br>
                                        <span class="blue">B社</span>：90,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">120,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>30,000円</p>
                                </div>
                            </li>

                            <!-- bag5-->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/005.jpg" alt=""></p>
                                    <p class="itemName">HERMES
                                        <br>バーキン30</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：1,360,000円
                                        <br>
                                        <span class="blue">B社</span>：1,200,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">1,600,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>400,000円</p>
                                </div>
                            </li>
                            <!-- bag6-->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/006.jpg" alt=""></p>
                                    <p class="itemName">CHANEL
                                        <br>マトラッセダブルフラップダブルチェーンショルダーバッグ</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：220,000円
                                        <br>
                                        <span class="blue">B社</span>：195,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">260,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>65,000円</p>
                                </div>
                            </li>

                            <!-- bag7-->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/007.jpg" alt=""></p>
                                    <p class="itemName">LOUIS VUITTON
                                        <br>ダミエ ネヴァーフルMM</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：110,000円
                                        <br>
                                        <span class="blue">B社</span>：96,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">130,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>34,000円</p>
                                </div>
                            </li>

                            <!-- bag8-->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/008.jpg" alt=""></p>
                                    <p class="itemName">CELINE
                                        <br>ラゲージマイクロショッパー</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：200,000円
                                        <br>
                                        <span class="blue">B社</span>：180,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">240,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>60,000円</p>
                                </div>
                            </li>

                            <!-- bag9-->
                            <li class="box-4">
                                <div class="title">
                                    <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/009.jpg" alt=""></p>
                                    <p class="itemName">ロエベ
                                        <br>アマソナ23</p>
                                    <hr>
                                    <p>
                                        <span class="red">A社</span>：75,000円
                                        <br>
                                        <span class="blue">B社</span>：68,000円
                                    </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">90,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>22,000円</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!--バックここま-->
                </div>
            </div>
        </section>

        <section class="kaitori_voice">
            <h3>お客様の声</h3>

            <ul>
                    <li>

                        <h4>大満足です。</h4>
                        <p class="voice_txt">昔に購入したリングやネックレスなどの査定に、ブランドリバリューさんの買取センターを利用しました。なぜ買取センターにしたかというと、大勢いらっしゃるお買い物のお客さんと顔を合わせたくなかったから。落ち着いた雰囲気のブースでじっくりと査定してほしかったからです。
「査定士さんが怖い人だったらどうしよう?」そんな風にも思っていたのですが、対応してくれた方の人柄もよく安心。十分な広さのブースは密室感もなかったので、リラックスして査定してもらうこともできました。
そして出された査定結果は、こちらの期待を良い意味で大きく裏切るもの!!　金額に納得できなければ持ち帰る気満々だったのですが、デザインも考慮してくれたことが良い結果につながったようです。

                        </p>
                    </li>
                    <li>

                        <h4>安心して利用することができました。</h4>
                        <p class="voice_txt">ロレックスやエルメス、シャネルなど……。ブランド好きだった親戚が亡くなったので、全てを現金にする必要があり、ブランドリバリューさんにお願いしました。理由は時計をはじめとして高価なものを持ち込むだけに、人目に触れたくなかったから。
その点、ブランドリバリューさんの買取センターは完全予約制だと聞いていましたし、査定専用のブースも完備。プライベートが守られますから、安心して利用することができました。
そして、査定士さんと買取り価格について、じっくりとつめられるのもよかった点。他のお客さんを待たせているのが気になって、落ち着かないまま査定が進められることもなく、納得できる買取価格を出してもらうことができました。
叔母の遺品、次の方も大切に使っていただけることを願っています。

                        </p>
                    </li>

                </ul>


        </section>
        <section>
            <h3 class="mt50">渋谷店　立地へのこだわり</h3>
            <p>"私たちは2016年に銀座の激戦区でブランドリバリュー銀座店1号店を出店させていただきました。そこでは他のブランド品買取店に負けない「最高水準の高額買取」、「誠実で懇切丁寧な接客」を強みとして多くのお客様にご利用いただいております。<br /><br /> 銀座で営業する中で、渋谷地域での出張買取の要望や渋谷店舗の有無のお問合せ等、お客様から渋谷店の出店要望を多くいただきました。お客様の渋谷までのアクセスの良さに可能性を感じ、この度ブランドリバリュー渋谷店2号店を渋谷のシンボルの一つであるタワーレコード前に出店させていただきました。

                <br /><br /> ちょっとした空き時間やお仕事帰り、銀座でのショッピングついでなどでも、ジュエリー・時計・バッグ等のブランド商品や宝石・宝飾品・貴金属品の査定・買取が可能ですので、どうぞお気軽にお立ち寄りください。

                <br /><br /> また、売りたいと検討中の商品の現物をお持込みいただかずとも、お電話やメールやLINE、店頭にて商品名や状態を伺って仮査定をさせていただくことも可能でございます。

                <br /> 店頭買取だけではなく、予約制買取、出張買取、宅配買取、手ぶらで買取等お客様のニーズに応えるべく様々な買取サービスをご用意しております。 お住いから渋谷・銀座まで行くのに時間がかかってしまうというお客様は宅配買取・店頭買取を利用していらっしゃいますので併せてご検討下さい。
                <br /><br /> 当店では長年、リユース業界でジュエリー・時計・ バッグ等の買取に携わってきたプロフェッショナルであるバイヤーが、よりお客様にご満足いただけるよう、お客様の気持ちに親身になり他店を上回る高額査定にてご要望にお答え申し上げます。<br /> 渋谷店スタッフ一同、皆様のご来店心よりお待ち申し上げております。
                <br />
                <br /><br /> 渋谷でジュエリー・時計・バッグの買取店をお探しの皆様、ブランド買取なら高額査定のBRANDREVALUE(ブランドリバリュー)へ。"
            </p>


        </section>
        <div class="takuhai_cvbox tentou">
            <p class="cv_tl">ブランドリバリュー買取センターで<br>プライベート買取を予約する</p>
            <div class="custom_tel takuhai_tel">
                <a href="tel:0120-970-060">
                    <div class="tel_wrap">
                        <div class="telbox01"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/customer/telico.png" alt="お申し込みはこちら">
                            <p>お電話からでもお申込み可能！<br>ご不明な点は、お気軽にお問合せ下さい。 </p>
                        </div>
                        <div class="telbox02"> <span class="small_tx">【受付時間】11:00 ~ 21:00</span><span class="ted_tx"> 年中無休</span>
                            <p class="fts25">0120-970-060</p>
                        </div>
                    </div>
                </a>
            </div>
            <p><a href="<?php echo home_url('purchase/visit-form'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/tentou_cv.png" alt="店頭買取お申し込み"></a></p>
        </div>
    </div>

    <?php

  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');



  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
