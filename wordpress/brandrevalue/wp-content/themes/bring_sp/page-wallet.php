<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  '001.jpg::CHANEL（シャネル）::キャビアスキン　マトラッセチェーンウォレット::100,000::96,000::105,000::9,000',
  '002.jpg::LOUIS VUITTON（ルイヴィトン）::モノグラム　ポルトフォイユクレマンス::45,000::38,000::48,000::10,000',
  '003.jpg::HERMES（エルメス）::ドゴンＧＭ　T刻印::125,000::115,000::150,000::35,000',
  '004.jpg::PRADA（プラダ）::サフィアーノ　長財布::32,000::28,000::36,000::8,000',
  '005.jpg::BOTTEGA VENETA（ボッテガヴェネタ）::ヴェネタ　イントレチャートラウンドファスナー長財布::42,000::39,500::44,000::4,500',
  '006.jpg::BOTTEGA VENETA（ボッテガヴェネタ）::ヴェネタ　イントレチャート長財布::38,000::35,000::40,000::5,000',
  '007.jpg::LOUIS VUITTON（ルイヴィトン）::モノグラム　ポルトフォイユインターナショナル::25,000::10,000::30,000::20,000',
  '008.jpg::HERMES（エルメス）::ベアンスフレ　T刻印::140,000::125,000::170,000::45,000',
  '009.jpg::LOUIS VUITTON（ルイヴィトン）::ダミエ　ジッピーウォレット::63,000::58,000::70,000::12,000',
  '010.jpg::GUCCI（グッチ）::ダブルG　長財布::32,000::30,000::35,000::5,000',
  '011.jpg::CHANEL（シャネル）::キャビアスキン　マトラッセラウンドファスナー長財布::90,000::87,000::95,000::8,000',
  '012.jpg::LOUIS VUITTON（ルイヴィトン）::モノグラム　ポルトフォイユサラ::58,000::54,000::60,000::6,000',
);


get_header(); ?>

    <div id="primary" class="cat-page content-area">
        <div class="mv_area ">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/lp_main/cat_wallet_main.jpg" alt="あなたの財布お売りください。">
        </div>
    <p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
   	<p class="main_bottom">満足度No1宣言！ブランド財布買取のブランドリバリューへ！</p>
	<div id="lp_head" class="wallet_ttl">
	<div>
	<p>銀座で最高水準の査定価格・サービス品質をご体験ください。</p>
	<h2>あなたの高級ブランド財布<br />どんな物でもお売り下さい！！</h2>
	</div>
	</div>
    <div class="lp_main">


            <section id="hikaku" class="watch_hikaku">
                <p class="hikaku_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/wallet_hikaku.png"></p>
            </section>

          <!--  <section id=" lp_history ">
                <h3 class="obi_tl">有名ブランドの歴史</h3>
                <ul class="his_btn">
                    <li><a rel="leanModal" href="#div_point01"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/btn_history01.jpg" alt="ロレックス"></a></li>
                    <li><a rel="leanModal" href="#div_point02"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/btn_history02.jpg" alt="パテックフィリップ"></a></li>
                    <li><a rel="leanModal" href="#div_point03"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/btn_history03.jpg" alt="ウブロ"></a></li>
                    <li><a rel="leanModal" href="#div_point04"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/btn_history04.jpg" alt="オーデマピゲ"></a></li>
                    <li><a rel="leanModal" href="#div_point05"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/btn_history05.jpg" alt="ブライトリング"></a></li>
                    <li><a rel="leanModal" href="#div_point06"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/btn_history06.jpg" alt="ブレゲ"></a></li>
                </ul>

                <div id="div_point01" class="div_point">
                    <h5>ROLEX（ロレックス）とは</h5>
                    <p>ROLEX（ロレックス）は1905年にウィルスドルフ＆デイビスという名前の時計商社として設立され
                        <br> 幾度と名前の変更を経て、現在のROLEX（ロレックス）となりました。
                    </p>
                    <p>ROLEX（ロレックス）と聞けば王冠マークをすぐに思い浮かべられるほど誰もが知る有名高級時計です。
                        <br> それほどの知名度を築くに至ったのもROLEX（ロレックス）が独自に開発をし、現在では多くの
                        <br> 自動巻き時計に採用されている数々の特許技術でしょう。
                    </p>
                    <p>まずはパーペチュアル機構です。それ以前にも自動巻きの機構は存在しましたが、ROLEX（ロレックス）の
                        <br> 開発したパーペチュアル機構（全回転機構）により自動巻き時計は完成となり、ROLEX（ロレックス）は
                        <br> ブランド自動巻き時計の第一号となったのです。
                    </p>
                    <p>その後もオイスターケースという防水仕様のケースの開発、デイトジャストと呼ばれる日付が0時ちょうどで
                        <br> 切り替わる機構など、数々の革新的な技術の開発を行ってきました。
                    </p>
                    <p>また、機構の開発だけに留まらず時計そのものとしてのクオリティーも細部に至るまでこだわり作られている
                        <br> こともROLEX（ロレックス）が現在の地位を獲得した要因でしょう。
                    </p>
                    <p>10年に1度はオーバーホールが必要ではありますが、きちんとしたメンテナンスを行っていれば正に一生ものと
                        <br> 呼ぶにふさわしい時計です。
                    </p>
                    <p>有名なモデルとして、デイトジャスト、サブマリーナ、デイトナ、GMTマスター、エクスプローラーⅠ、 デイトジャスト、ヨットマスター、ターノグラフ、エアキング、シードュエラ、ミルガウスなど
                    </p>
                </div>

                <div id="div_point02" class="div_point">
                    <h5>PATEK PHILIPPE（パテックフィリップ）とは</h5>
                    <p>PATEK PHILIPPE（パテックフィリップ）は誰もが知るブランド時計では御座いません。
                        <br> しかし、世界三大高級時計として世界各国で愛されている最高級の時計ブランドです。
                    </p>
                    <p>PATEK PHILIPPE（パテックフィリップ）はカルティエやティファニー、ギュブラン、宝石店向けに製品を
                        <br> 納品していたこともあり、世界一高級な時計を販売するマニュファクチュールとして知られています。
                    </p>
                    <p>PATEK PHILIPPE（パテックフィリップ）はどんなに古い商品でも自社の物であれば修理可能と永久修理保証と
                        <br> 宣伝している為、PATEK PHILIPPE（パテックフィリップ）の時計は一生ものというイメージを確立しております。</p>
                    <p>有名なモデルとして、カラトラバ、コンプリケーテッド、グランドコンプリケオーション、アクアノート、ゴンドーロ、 トゥエンティー・フォー、ノーチラス、ゴールデンイリプスなど
                    </p>
                </div>

                <div id="div_point03" class="div_point">
                    <h5>HUBLOT（ウブロ）とは</h5>
                    <p>HUBLOT（ウブロ）は1980年に誕生し、独特なベゼル、ケースデザインやベルトにラバーを採用するなど
                        <br> 独自の世界観で多くの時計ファンに衝撃を与えた高級時計ブランドです。
                    </p>
                    <p>ユニークなデザインの為、クラシックなものと違い派手さ、華やかさから多くのスポーツ選手や芸能人が
                        <br> 愛用しております。
                    </p>
                    <p>有名なモデルにクラシックフュージョン、ビッグバン、ビッグバンキング、キングパワーなど</p>
                </div>

                <div id="div_point04" class="div_point">
                    <h5>AUDEMARS　PIGUET（オーデマピゲ）とは</h5>
                    <p>AUDEMARS　PIGUET（オーデマピゲ）は1875年ジュール＝ルイ・オーデマと
                        <br> 友人のエドワール＝オーギュスト・ピゲにより設立された高級時計メーカーです。
                    </p>
                    <p>お互いの技術力の高さから複雑時計の開発を始め、1889年に第10回パリ万博で
                        <br> スプリットセコンドグラフや永久カレンダーなどを装備する、グランド・コンプリカシオン
                        <br> 懐中時計を発表。1892年に世界で初めてのミニッツリピーターを搭載した腕時計の
                        <br> 独創的な製品を発表し「複雑度系のオーデマピゲ」として名声を獲得しております。
                    </p>
                    <p>現在でも、創業者一族が経営を引き継いでおり高い情熱と技術が継承されております。</p>
                    <p>厚さ1.62mmの世界最薄手巻きムーブメントや、1972年にはラグジュアリースポーツウォッチと
                        <br> いつ新カテゴリーを築いたロイヤルオークを発表し、誕生から40年をヘタ現在も高い人気を誇ります。
                    </p>
                    <p>有名なモデルにロイヤルオーク オフショア、ロイヤルオーク、ジュール・オーデマ、エドワール・ピゲ、ミレネリーなど</p>
                </div>

                <div id="div_point05" class="div_point">
                    <h5>BREITLING（ブライトリング）とは</h5>
                    <p>BREITLING（ブライトリング）の歴史はクロノグラフの進化の歴史といっても過言ではありません。</p>
                    <p>創業者のレオン・ブライトリングは計測機器の開発に特化しており、各分野のプロ用クロノグラフを
                        <br> 製造。息子ガストンが会社を継ぎ、1915年に独立した専用プッシュボタンを備えた世界初の腕時計型
                        <br> クロノグラフの開発に成功。1934年にリセットボタンを開発し、現在のクロノグラフの基礎を築いた。
                    </p>
                    <p>そして、1936年にイギリス空軍とコックピットクロックの供給契約を皮切りに航空業界で勢力を拡大。
                        <br> 1952年に航空用回転計算尺を搭載した「ナビタイマー」によって、航空時計ナンバーワンブランドの
                        <br> 地位を確立した。
                    </p>
                    <p>有名なモデルにクロノマット、スーパーオーシャン、トランスオーシャン、ナビタイマー、ウィンドライダー、スカイレーサーなど</p>
                </div>

                <div id="div_point06" class="div_point">
                    <h5>BREGUET（ブレゲ）とは</h5>
                    <p>BREGUET（ブレゲ）は1775年、パリにオープンした工房から始まった。</p>
                    <p>1780年にはブラビアン＝ルイ・ブレゲが自動巻き機構を開発。衝撃吸収装置、ブレゲヒゲゼンマイ、
                        <br> トゥールビヨン機構、スプリットセコンドクロノグラフなど、次々と画期的な機構を開発、ブレゲ針や
                        <br> ブレゲ数字、文字盤のギョ―シェ彫りもブレゲの発案によるものだ。
                    </p>
                    <p>天才と言われる彼の技術はマリー・アントワネット妃の注文により作られた超複雑時計No.160
                        <br> 「マリー・アントワネット」に集約されている。
                    </p>
                    <p>1823年にブレゲは死去するが、その技術は弟子たちに受け継がれ、経営がブラウン家、宝石商ショーメと
                        <br> 移るが、その間も数多くの複雑度系を世に送り出した。
                    </p>
                    <p>現在はスウォッチ・グループの傘下となり、安定した経営基盤を獲得し、自社ムーブメントの開発や
                        <br> シリコン素材の導入など、新境地を開き続けている。
                    </p>
                    <p>有名なモデルにタイプトゥエンティー、マリーン、クイーン・オブ・ネイプルズ、クラシック、トラディション、ヘリテージ、コンプリケーションなど</p>
                </div>

            </section>-->
<div style="margin-top:20px; margin-bottom: 20px;"><a href="https://kaitorisatei.info/brandrevalue/blog/doburock"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/12/caf61dc6779ec6137c0ab58dfe3a550d.jpg" alt="どぶろっく"></a></div>
           <section id="cat-point">
                <h3 class="obi_tl">高価買取のポイント</h3>
 <ul>
                    <li>
                    <p class="pt_bigtl">POINT1</p>
                        <div class="pt_wrap">
                        <p class="pt_tl">商品情報が明確だと査定がスムーズ</p>
                        <p class="pt_tx">ブランド名、モデル名が明確だと査定がスピーディに出来、買取価格にもプラスに働きます。また、新作や人気モデル、人気ブランドであれば買取価格がプラスになります。</p>
                        </div>
                    </li>
                    <li>
                    <p class="pt_bigtl">POINT2</p>
                        <div class="pt_wrap">
                        <p class="pt_tl">数点まとめての査定だと
                            <br>キャンペーンで高価買取が可能</p>
                        <p class="pt_tx">数点まとめての査定依頼ですと、買取価格をプラスさせていただきます。</p>
                        </div>
                    </li>
                    <li>
                    <p class="pt_bigtl">POINT3</p>
                        <div class="pt_wrap">
                        <p class="pt_tl">品物の状態がよいほど
                            <br>高価買取が可能</p>
                        <p class="pt_tx">お品物の状態が良ければ良いほど、買取価格もプラスになります。</p>
                        </div>
                    </li>
                </ul>                <p>財布の査定額は、ちょっとした工夫で高くなることがあるのをご存知でしょうか。
それは「ご自宅で簡単にメンテナンスをしてから査定に出す」というもの。モノ自体がよくても、埃がたまっているなど汚れが目立つ財布は査定額が下がってしまう場合があるのです。ご自宅でできる範囲で表面の汚れをふき取り、カード入れや小銭入れ等の小さなポケットにたまった埃をとるだけでも評価額が上がるので、ぜひお試しください。<br>
その他、購入時の箱や袋、保証書等の付属品が一式そろっていると、査定金額が上がります。
                </p>
            </section>

            <section id="lp-cat-jisseki">
                <h3 class="obi_tl">買取実績</h3>

                <ul id="box-jisseki" class="list-unstyled clearfix">
                    <?php
            foreach($resultLists as $list):
            // :: で分割
            $listItem = explode('::', $list);

          ?>
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/wallet/lp/<?php echo $listItem[0]; ?>" alt=""></p>
                                <p class="itemName">
                                    <?php echo $listItem[1]; ?>
                                        <br>
                                        <?php echo $listItem[2]; ?>
                                </p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：
                                    <?php echo $listItem[3]; ?>円
                                        <br>
                                        <span class="blue">B社</span>：
                                        <?php echo $listItem[4]; ?>円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">
                                    <?php echo $listItem[5]; ?><span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>
                                    <?php echo $listItem[6]; ?>円</p>
                            </div>
                        </li>
                        <?php endforeach; ?>
                </ul>
				                <h3 class="new_list_ttl">ブランドリスト</h3>
                <ul class="list-unstyled new_list">
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/hermes'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch33.jpg" alt="エルメス"></dd>
                                <dt><span>Hermes</span>エルメス</dt>
                            </a>
                        </dl>
                    </li>
					<li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/celine'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch29.jpg" alt="セリーヌ"></dd>
                                <dt><span>CELINE</span>セリーヌ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/wallet/louisvuitton'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch23.jpg" alt="ルイヴィトン"></dd>
                                <dt><span>LOUIS VUITTON</span>ルイヴィトン</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/chanel'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag01.jpg" alt="シャネル"></dd>
                                <dt><span>CHANEL</span>シャネル</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/gucci'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch25.jpg" alt="グッチ"></dd>
                                <dt><span>GUCCI</span>グッチ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/prada'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag02.jpg" alt="プラダ"></dd>
                                <dt><span>PRADA</span>プラダ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/fendi'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag03.jpg" alt="プラダ"></dd>
                                <dt><span>FENDI</span>フェンディ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/dior'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag04.jpg" alt="ディオール"></dd>
                                <dt><span>Dior</span>ディオール</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
						<a href="<?php echo home_url('/cat/bag/saint_laurent'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag05.jpg" alt="サンローラン"></dd>
                                <dt><span>Saint Laurent</span>サンローラン</dt>
                            </a>
                        </dl>
                    </li>
					<li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/bottegaveneta'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch32.jpg" alt="ボッテガヴェネタ"></dd>
                                <dt><span>Bottegaveneta</span>ボッテガヴェネタ</dt>
                            </a>
                        </dl>
                    </li>
					<li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/ferragamo'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch31.jpg" alt="フェラガモ"></dd>
                                <dt><span>Ferragamo</span>フェラガモ</dt>
                            </a>
                        </dl>
                    </li>
			</ul>
                <p id="catchcopy">BRAND REVALUEはまだ新しい買取ショップですが、財布買取に精通したスタッフがそろっており再販ルートも確立しております。<br>
お客様の大切な財布の価値を見逃すことなく高く査定させていただいていることから、オープン後財布の買取実績も急速に伸びております。また、BRAND REVALUEは銀座の中心地という利便性の高い地域に店舗を構えていますが、路面店と比べて格段に家賃の安い空中階に店舗があるため、他店と比べて圧倒的に経費を安く抑えることができています。
その他、広告費、人件費等も可能な限り削減しており、その分査定額を高めに設定してお客様に利益を還元しております。
            </p>
                <p class="jyoutai_tl">こんな状態でも買取致します!</p>
                <div id="konna">

                    <p class="example1">■汚れ</p>
                    <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/wallet_jyoutai01.jpg"></p>
                    <p class="example2">■型くずれ</p>
                    <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/wallet_jyoutai02.jpg"></p>
                    <p class="example3">■角スレ</p>
                    <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/wallet_jyoutai03.jpg"></p>
                    <p class="text">その他：箱、ギャランティー、付属品無し、電池切れや故障による不動品でもお買取りいたします。</p>
                </div>
            </section>

            <section id="about_kaitori" class="clearfix">
                <?php
        // 買取について
        get_template_part('_widerange');
				get_template_part('_widerange2');
      ?>
            </section>

           <!-- <h3 class="obi_tl">ブランドリスト</h3>
            <ul class="cat_list">
                  <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_1.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_2.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_3.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_4.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_5.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_6.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_7.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_8.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_9.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_10.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_11.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_12.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_13.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_14.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_15.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_16.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_17.jpg" alt=""></li>
                <li><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_18.jpg" alt=""></li>


            </ul>-->

            <section>
                <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/bn_matomegai.png">
            </section>

            <?php
        // 買取基準
        get_template_part('_criterion');

        // NGアイテム
        get_template_part('_no_criterion');

				// カテゴリーリンク
        get_template_part('_category');
			?>
                <section id="kaitori_genre">
                    <h3 class="obi_tl">その他買取可能なジャンル</h3>
                    <p>【買取ジャンル】バッグ/ウエストポーチ/セカンドバック/トートバッグ/ビジネスバッグ/ボストンバッグ/クラッチバッグ/トランクケース/ショルダーバッグ/ポーチ/財布/カードケース/パスケース/キーケース/手帳/腕時計/ミュール/サンダル/ビジネスシューズ/パンプス/ブーツ/ペアリング/リング/ネックレス/ペンダント/ピアス/イアリング/ブローチ/ブレスレット/ライター/手袋/傘/ベルト/ペン/リストバンド/アンクレット/アクセサリー/サングラス/帽子/マフラー/ハンカチ/ネクタイ/ストール/スカーフ/バングル/カットソー/アンサンブル/ジャケット/コート/ブルゾン/ワンピース/ニット/シャツメンズ/毛皮/Tシャツ/キャミソール/タンクトップ/パーカー/ベスト/ポロシャツ/ジーンズ/スカート/スーツなど</p>
                </section>

                <?php
        // 買取方法
        get_template_part('_purchase');
      ?>

                    <section id="user_voice">
                        <h3>ご利用いただいたお客様の声</h3>

                        <p class="user_voice_text1">ちょうど家の整理をしていたところ、家のポストにチラシが入っていたので、ブランドリバリューに電話してみました。今まで買取店を利用したことがなく、不安でしたがとても丁寧な電話の対応とブランドリバリューの豊富な取り扱い品目を魅力に感じ、出張買取を依頼することにしました。 絶対に売れないだろうと思った、動かない時計や古くて痛んだバッグ、壊れてしまった貴金属のアクセサリーなども高額で買い取っていただいて、とても満足しています。古紙幣や絵画、食器なども買い取っていただけるとのことでしたので、また家を整理するときにまとめて見てもらおうと思います。
                        </p>

                        <h3>鑑定士からのコメント</h3>

                        <p class="user_voice_text2">家の整理をしているが、何を買い取ってもらえるか分からないから一回見に来て欲しいとのことでご連絡いただきました。 買取店が初めてで不安だったが、丁寧な対応に非常に安心しましたと笑顔でおっしゃって頂いたことを嬉しく思います。 物置にずっとしまっていた時計や、古いカバン、壊れてしまったアクセサリーなどもしっかりと価値を見極めて高額で提示させて頂きましたところ、お客様もこんなに高く買取してもらえるのかと驚いておりました。 これからも家の不用品を整理するときや物の価値判断ができないときは、すぐにお電話しますとおっしゃって頂きました。
                        </p>

                    </section>

        </div>
        <!-- lp_main -->
    </div>
    <!-- #primary -->

    <?php

get_footer();
