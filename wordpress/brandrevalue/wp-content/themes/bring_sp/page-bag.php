<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  '001.jpg::エブリンⅢ::トリヨンクレマンス　R刻印::164,000::160,000::170,000::10,000',
  '002.jpg::プラダ::2WAYハンドバッグ::70,000::67,000::74,000::7,000',
  '003.jpg::グッチ::2WAYハンドバッグ::84,000::82,000::90,000::8,000',
  '004.jpg::ルイヴィトン::ダミエ　モンスリGM::110,000::100,000::115,000::15,000',
  '005.jpg::エルメス::ミニケリー　○I刻印::1,200,000::1,180,000::1,236,000::56,000',
  '006.jpg::デカマトラッセ::Ｗフラップ　Ｗチェーン　Ｇ金具::364,000::358,000 ::378,000::20,000',
  '007.jpg::ルイヴィトン::ダミエ　スピーディバンドリエール::118,000::114,000::123,000::9,000',
  '008.jpg::セリーヌ::トラペーズ・スモール::174,000::170,000::180,000::10,000',
  '009.jpg::エルメス::ピコタンロック　エクラPM　X刻印::242,000::230,000::250,000::20,000',
  '010.jpg::クリスチャンディオール::レディディオール::220,000::216,000::240,000::24,000',
  '011.jpg::フェンディ::プチトゥジュール　モンスター　２WAYバッグ::156,000::150,000::160,000::10,000',
  '012.jpg::プラダ::カナパ　ビジュー付き::68,000::65,000::72,000::7,000',
);

get_header(); ?>

    <div id="primary" class="cat-page content-area">
        <div class="mv_area ">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/lp_main/cat_bag_main.jpg" alt="あなたのバッグお売り下さい">
        </div>
    <p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
    <p class="main_bottom">どこよりも高価買取いたします！ブランドバッグ・鞄買取特別強化中！</p>
    <div id="lp_head" class="bag_ttl">
    <div>
    <p>銀座で最高水準の査定価格・サービス品質をご体験ください。</p>
    <h2>あなたの高級ブランドバッグ<br />どんな物でもお売り下さい！！</h2>
    </div>
    </div>
        <div class="lp_main">
            <section id="hikaku" class="watch_hikaku">
                <p class="hikaku_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/bag_hikaku.png"></p>
            </section>

    <div style="margin-top:20px; margin-bottom: 20px;"><a href="https://kaitorisatei.info/brandrevalue/blog/doburock"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/12/caf61dc6779ec6137c0ab58dfe3a550d.jpg" alt="どぶろっく"></a></div>

            <section id="cat-point">
                <h3 class="obi_tl">高価買取のポイント</h3>
 <ul>
                    <li>
                    <p class="pt_bigtl">POINT1</p>
                        <div class="pt_wrap">
                        <p class="pt_tl">商品情報が明確だと査定がスムーズ</p>
                        <p class="pt_tx">ブランド名、モデル名が明確だと査定がスピーディに出来、買取価格にもプラスに働きます。また、新作や人気モデル、人気ブランドであれば買取価格がプラスになります。</p>
                        </div>
                    </li>
                    <li>
                    <p class="pt_bigtl">POINT2</p>
                        <div class="pt_wrap">
                        <p class="pt_tl">数点まとめての査定だと
                            <br>キャンペーンで高価買取が可能</p>
                        <p class="pt_tx">数点まとめての査定依頼ですと、買取価格をプラスさせていただきます。</p>
                        </div>
                    </li>
                    <li>
                    <p class="pt_bigtl">POINT3</p>
                        <div class="pt_wrap">
                        <p class="pt_tl">品物の状態がよいほど
                            <br>高価買取が可能</p>
                        <p class="pt_tx">お品物の状態が良ければ良いほど、買取価格もプラスになります。</p>
                        </div>
                    </li>
                </ul>                <p>ブランドバッグ・鞄を高く売るためには、日頃のメンテナンスが重要です。よく手入れをすることや気をつけて使うことでバッグは長持ちしますし、売却する際の査定の評価も良いものとなります。また、バッグのブランド名、モデル名、購入した時期などをお伝えいただくことでスピーディーな査定を行うことができます。<br>
バッグの保証書や箱、付属品の小物やベルトなどがありましたら必ず一緒にご持参ください。付属品が一式そろっていることで、査定金額に反映することができます。
                </p>
            </section>
            <h3 class="mid">ブランドバッグ 一覧</h3>
        <ul class="mid_link">
            <li>
            <a href="<?php echo home_url('/cat/bag/hermes/hermes-bag'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/01.jpg" alt="エルメス買取">
            <p class="mid_ttl">エルメス</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/celine'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/02.jpg" alt="セリーヌ買取">
            <p class="mid_ttl">セリーヌ</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/wallet/louisvuitton/louisvuitton-bag'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/03.jpg" alt="ルイヴィトン買取">
            <p class="mid_ttl">ルイヴィトン</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/chanel/chanel-bag'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/04.jpg" alt="シャネル買取">
            <p class="mid_ttl">シャネル</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/gucci/gucci-bag'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/05.jpg" alt="グッチ買取">
            <p class="mid_ttl">グッチ</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('cat/gem/cartier/cartier-bag'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/06.jpg" alt="カルティエ買取">
            <p class="mid_ttl">カルティエ</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/gem/tiffany/tiffany-bag'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/07.jpg" alt="ティファニー買取">
            <p class="mid_ttl">ティファニー</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/prada'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/09.jpg" alt="プラダ買取">
            <p class="mid_ttl">プラダ</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/fendi'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/10.jpg" alt="フェンディ買取">
            <p class="mid_ttl">フェンディ</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/dior/dior-bag'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/08.jpg" alt="ディオール買取">
            <p class="mid_ttl">ディオール</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/saint_laurent'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/12.jpg" alt="サンローラン買取">
            <p class="mid_ttl">サンローラン</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/bottegaveneta'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/13.jpg" alt="ボッテガ・ヴェネタ買取">
            <p class="mid_ttl">ボッテガ・ヴェネタ</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/ferragamo'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/14.jpg" alt="フェラガモ買取">
            <p class="mid_ttl">フェラガモ</p>
            </a>
            </li>
            <!--<li>
            <a href="<?php echo home_url('/cat/bag/hermes/hermes-bag'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/14.jpg" alt="エルメス買取">
            <p class="mid_ttl">エルメス</p>
            </a>
            </li>-->
            <li>
            <a href="<?php echo home_url('/cat/bag/loewe'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/15.jpg" alt="ロエベ買取">
            <p class="mid_ttl">ロエベ</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/berluti'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/16.jpg" alt="ベルルッティ">
            <p class="mid_ttl">ベルルッティ</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/goyal'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/17.jpg" alt="ゴヤール">
            <p class="mid_ttl">ゴヤール</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/rimowa'); ?>">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/18.jpg" alt="リモワ">
            <p class="mid_ttl">リモワ</p>
            </a>
            </li>
            <li>
<a href="<?php echo home_url('/cat/bag/globe-trotter'); ?>">
<img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/19.jpg" alt="グローブトロッター">
<p class="mid_ttl">グローブトロッター</p>
            </a>
            </li>
             <li>
<a href="<?php echo home_url('/cat/bag/valextra'); ?>">
<img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/20.jpg" alt="ヴァレクストラ">
<p class="mid_ttl">ヴァレクストラ</p>
            </a>
            </li>


            </ul>

            <section id="lp-cat-jisseki">
                <h3 class="obi_tl">買取実績</h3>

                <ul id="box-jisseki" class="list-unstyled clearfix">
                    <?php
            foreach($resultLists as $list):
            // :: で分割
            $listItem = explode('::', $list);

          ?>
                        <li class="box-4">
                            <div class="title">
                                <p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/bag/lp/<?php echo $listItem[0]; ?>" alt=""></p>
                                <p class="itemName">
                                    <?php echo $listItem[1]; ?>
                                        <br>
                                        <?php echo $listItem[2]; ?>
                                </p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：
                                    <?php echo $listItem[3]; ?>円
                                        <br>
                                        <span class="blue">B社</span>：
                                        <?php echo $listItem[4]; ?>円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">
                                    <?php echo $listItem[5]; ?><span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>
                                    <?php echo $listItem[6]; ?>円</p>
                            </div>
                        </li>
                        <?php endforeach; ?>
                </ul>
                <p id="catchcopy">BRAND REVALUEでは様々な種類のブランドバッグの買取を行っております。人気の高いルイ・ヴィトンやエルメス、コーチ、シャネル、グッチ、プラダなどのブランドバッグ・鞄が査定対象です。ひと昔前のデザインのバッグでもメンテナンス状態がよければ高額での買取査定が可能ですので、ぜひご自宅に使わなくなったバッグがありましたらBRAND REVALUEまでお持ちください。<br><br>
傷があったり、ベルトが切れてしまっているバッグについても、お気軽にご相談ください。
他店では値段がほとんどつかなかったようなブランドバッグでも、BRAND REVALUEなら納得の高額査定で買い取りができるケースもございます。
            </p>
                <p class="jyoutai_tl">こんな状態でも買取致します!</p>
                <div id="konna">

                    <p class="example1">■粘つき・ひび割れ</p>
                    <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/bag_jyoutai01.jpg"></p>
                    <p class="example2">■化粧のシミ</p>
                    <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/bag_jyoutai02.jpg"></p>
                    <p class="example3">■日焼け</p>
                    <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/bag_jyoutai03.jpg"></p>
                    <p class="text">その他：箱、ギャランティー、付属品無し、電池切れや故障による不動品でもお買取りいたします。</p>
                </div>
            </section>

            <section id="about_kaitori" class="clearfix">
                <?php
        // 買取について
        get_template_part('_widerange');
                get_template_part('_widerange2');
      ?>
            </section>
<!--<h3 class="new_list_ttl">ブランドリスト</h3>
                <ul class="list-unstyled new_list">
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/hermes'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch33.jpg" alt="エルメス"></dd>
                                <dt><span>Hermes</span>エルメス</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/celine'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch29.jpg" alt="セリーヌ"></dd>
                                <dt><span>CELINE</span>セリーヌ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/wallet/louisvuitton'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch23.jpg" alt="ルイヴィトン"></dd>
                                <dt><span>LOUIS VUITTON</span>ルイヴィトン</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/chanel'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag01.jpg" alt="シャネル"></dd>
                                <dt><span>CHANEL</span>シャネル</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/gucci'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch25.jpg" alt="グッチ"></dd>
                                <dt><span>GUCCI</span>グッチ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/prada'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag02.jpg" alt="プラダ"></dd>
                                <dt><span>PRADA</span>プラダ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/fendi'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag03.jpg" alt="プラダ"></dd>
                                <dt><span>FENDI</span>フェンディ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/dior'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag04.jpg" alt="ディオール"></dd>
                                <dt><span>Dior</span>ディオール</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                        <a href="<?php echo home_url('/cat/bag/saint_laurent'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag05.jpg" alt="サンローラン"></dd>
                                <dt><span>Saint Laurent</span>サンローラン</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/bottegaveneta'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch32.jpg" alt="ボッテガヴェネタ"></dd>
                                <dt><span>Bottegaveneta</span>ボッテガヴェネタ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/ferragamo'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch31.jpg" alt="フェラガモ"></dd>
                                <dt><span>Ferragamo</span>フェラガモ</dt>
                            </a>
                        </dl>
                    </li>
            </ul>-->

            <section>
                <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/bn_matomegai.png">
            </section>

            <?php
        // 買取基準
        get_template_part('_criterion');

        // NGアイテム
        get_template_part('_no_criterion');

                // カテゴリーリンク
        get_template_part('_category');
            ?>
                <section id="kaitori_genre">
                    <h3 class="obi_tl">その他買取可能なジャンル</h3>
                    <p>【買取ジャンル】バッグ/ウエストポーチ/セカンドバック/トートバッグ/ビジネスバッグ/ボストンバッグ/クラッチバッグ/トランクケース/ショルダーバッグ/ポーチ/財布/カードケース/パスケース/キーケース/手帳/腕時計/ミュール/サンダル/ビジネスシューズ/パンプス/ブーツ/ペアリング/リング/ネックレス/ペンダント/ピアス/イアリング/ブローチ/ブレスレット/ライター/手袋/傘/ベルト/ペン/リストバンド/アンクレット/アクセサリー/サングラス/帽子/マフラー/ハンカチ/ネクタイ/ストール/スカーフ/バングル/カットソー/アンサンブル/ジャケット/コート/ブルゾン/ワンピース/ニット/シャツメンズ/毛皮/Tシャツ/キャミソール/タンクトップ/パーカー/ベスト/ポロシャツ/ジーンズ/スカート/スーツなど</p>
                </section>

                <?php
        // 買取方法
        get_template_part('_purchase');
      ?>

                    <section id="user_voice">
                        <h3>ご利用いただいたお客様の声</h3>

                        <p class="user_voice_text1">ちょうど家の整理をしていたところ、家のポストにチラシが入っていたので、ブランドリバリューに電話してみました。今まで買取店を利用したことがなく、不安でしたがとても丁寧な電話の対応とブランドリバリューの豊富な取り扱い品目を魅力に感じ、出張買取を依頼することにしました。 絶対に売れないだろうと思った、動かない時計や古くて痛んだバッグ、壊れてしまった貴金属のアクセサリーなども高額で買い取っていただいて、とても満足しています。古紙幣や絵画、食器なども買い取っていただけるとのことでしたので、また家を整理するときにまとめて見てもらおうと思います。
                        </p>

                        <h3>鑑定士からのコメント</h3>

                        <p class="user_voice_text2">家の整理をしているが、何を買い取ってもらえるか分からないから一回見に来て欲しいとのことでご連絡いただきました。 買取店が初めてで不安だったが、丁寧な対応に非常に安心しましたと笑顔でおっしゃって頂いたことを嬉しく思います。 物置にずっとしまっていた時計や、古いカバン、壊れてしまったアクセサリーなどもしっかりと価値を見極めて高額で提示させて頂きましたところ、お客様もこんなに高く買取してもらえるのかと驚いておりました。 これからも家の不用品を整理するときや物の価値判断ができないときは、すぐにお電話しますとおっしゃって頂きました。
                        </p>

                    </section>

        </div>
        <!-- lp_main -->
    </div>
    <!-- #primary -->

    <?php

get_footer();
