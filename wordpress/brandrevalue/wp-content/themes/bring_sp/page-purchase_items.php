<?php
get_header(); ?>

<style>
    .wp-pagenavi{
        display: none;
    }
</style>


<div class="pd_side10">
<h2 class="ttl_blue">ブランドリバリュー ブランド買取実績</h2>
            <ul class="archive_purchase_list">
                <li><a href="<?php echo home_url('purchase_items/purchase_watch'); ?>">時計</a></li>
                <li><a href="<?php echo home_url('purchase_items/purchase_antique'); ?>">アンティーク時計</a></li>
                <li><a href="<?php echo home_url('purchase_items/purchase_bag'); ?>">バッグ</a></li>
                <li><a href="<?php echo home_url('purchase_items/purchase_wallet'); ?>">財布</a></li>
                <li><a href="<?php echo home_url('purchase_items/purchase_outfit'); ?>">洋服毛皮</a></li>
                <li><a href="<?php echo home_url('purchase_items/purchase_shoes'); ?>">靴</a></li>
                <li><a href="<?php echo home_url('purchase_items/purchase_jewelry'); ?>">ブランドジュエリー</a></li>
                <li><a href="<?php echo home_url('purchase_items/purchase_gold'); ?>">金・ブラチナ</a></li>
                <li><a href="<?php echo home_url('purchase_items/purchase_dia'); ?>">ダイヤ</a></li>
                <li><a href="<?php echo home_url('purchase_items/purchase_mement'); ?>">骨董品</a></li>
            </ul>
</div>

<ul class="archive_purchase">
<?php query_posts(
  array(
  'post_type' => 'purchase_item',
  'taxonomy' => 'purchase_item',
  'posts_per_page' => 20,
  'paged' => $paged,
  ));
  if (have_posts()): while ( have_posts() ) : the_post();
  ?>
<li><a href="<?php the_permalink(); ?>">
    <p class="kaitori-date"><?php the_field('kaitori-date'); ?></p>
    <p class="kaitori-image"><img data-src="<?php the_field('kaitori-img'); ?>" alt="<?php the_title(); ?>"></p>
    <p class="kaitori-title"><?php the_title(); ?></p>
    <p class="kaitori-price-header">買取価格</p>
    <P class="kaitori-price"> ¥<?php the_field('kaitori-price'); ?></P>
    <p class="kaitori-method"><?php the_field('kaitori-method'); ?></p>
    <p class="kaitori-detail">詳細はこちら</p>
</a></li>
    <?php endwhile; endif; wp_pagenavi(); wp_reset_query(); ?>
</ul>

<?php
  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');
  ?>

  <?php

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
