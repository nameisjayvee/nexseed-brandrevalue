<?php
  
get_header(); ?>

<div class="mv_area ">
  <img src="<?php echo get_s3_template_directory_uri() ?>/images/gem_kv.png " alt="BRAMD REVALUE ">
</div>
<div class="cat_cnt">
  <h2 class="cat_tl">流行に左右されない「宝石」は思わぬ高額買取も！</h2>
  <p class="cat_tx">宝石の多くはリング、ピアス、ネックレスなどのアクセサリーにセットされているものです。「指輪の台座が破損している」「ピアスのデザインが流行遅れになっている」といったケースもあるかもしれませんが、ご安心ください。買取業界では、買取後のアクセサリーは宝石と枠に分けて再販を行うため、枠の価値が下がっても宝石そのものは状態さえよければ価値が下がることはないのです。そもそも宝石は、古くから高い換金性を持つ資産として大切に流通されてきたもの。BRAND REVALUEでは、流行に左右されることなく宝石そのものの価値を最大限評価し、高く高額で鑑定買取をしていますので、「何年もつけていない、眠っている宝石」があればぜひお持ちください。</p>
</div>

<div id="cat-jisseki">
  <h3 class="cat-jisseki_tl">買取実績</h3>
        <p class="cat_jisseki_tx">BRAND REVALUEには宝石鑑定において長年の経験を積んできたスタッフが在籍しており、主要な宝石すべてに対し適切な査定を行っております。<br>
        ダイヤモンド、ルビー、サファイア、エメラルド、真珠といったメジャーな宝石はもちろん、オパール、キャッツアイ、タンザナイト、アクアマリン、トパーズ、トルマリン、珊瑚、翡翠など、比較的市場での流通が少ない宝石についても対応しておりますので、「他店で査定ができなかった宝石」なども遠慮なくお持ちください。</p>

      <ul id="box-jisseki" class="list-unstyled clearfix">

                  <li class="box-4">
                    <div class="title">
                      <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/gem001.jpg" alt=""></p>
                      <p class="itemName">サファイア</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：145,500円<br>
                        <span class="blue">B社</span>：142,500円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">地金+<br>150,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>7,500円</p>
                    </div>
                  </li>

                  <li class="box-4">
                    <div class="title">
                      <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/gem002.jpg" alt=""></p>
                      <p class="itemName">エメラルド</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：190,000円<br>
                        <span class="blue">B社</span>：194,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">地金+<br>200,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>10,000円</p>
                    </div>
                  </li>

                  <li class="box-4">
                    <div class="title">
                      <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/gem003.jpg" alt=""></p>
                      <p class="itemName">ルビー</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：97,000円<br>
                        <span class="blue">B社</span>：95,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">地金+<br>100,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>5,000円</p>
                    </div>
                  </li>

                  <li class="box-4">
                    <div class="title">
                      <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/gem004.jpg" alt=""></p>
                      <p class="itemName">アレキサンドライト</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：67,900円<br>
                        <span class="blue">B社</span>：66,500円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">地金+<br>70,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>4,500円</p>
                    </div>
                  </li>
                  <li class="box-4">
                    <div class="title">
                      <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/gem005.jpg" alt=""></p>
                      <p class="itemName">ダイヤモンド　ルース</p>
                      <p class="itemdetail">カラット：1.003ct<br>カラー:E<br>クラリティ:SI1<br>カット:EXCELLENT<br>形状:ラウンドブリリアントカット</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：194,000円<br>
                        <span class="blue">B社</span>：190,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">地金+<br>480,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>10,000円</p>
                    </div>
                  </li>

                  <li class="box-4">
                    <div class="title">
                      <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/gem006.jpg" alt=""></p>
                      <p class="itemName">K18YG<br>ダイヤモンドリング</p>
                      <p class="itemdetail">カラット(主石)：0.54ct<br>カラー：E<br>クラリティ：VS２<br>カット：VERY GOOD <br>形状：ラウンドブリリアント<br>メレダイヤモンド0.7ct<br>地金：18金イエローゴールド　5ｇ</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：145,500円<br>
                        <span class="blue">B社</span>：142,500円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">地金+<br>150,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>7,500円</p>
                    </div>
                  </li>
                  <li class="box-4">
                    <div class="title">
                      <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/gem007.jpg" alt=""></p>
                      <p class="itemName">ティファニー<br>ソレスト　ダイヤモンドリング</p>
                      <p class="itemdetail">PT950　カラット0.31ct<br>カラー：E<br>クラリティVVS２<br>カット：EXCELLENT</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：320,100円<br>
                        <span class="blue">B社</span>：313,500円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">地金+<br>330,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>16,500円</p>
                    </div>
                  </li>

                  <li class="box-4">
                    <div class="title">
                      <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/gem008.jpg" alt=""></p>
                      <p class="itemName">ハートシェイプ<br>ピンクダイヤモンド　ルース</p>
                      <p class="itemdetail">カラット：2ct<br>カラー：VERY LIGHT PINK<br>クラリティ：VS２<br>形状：ハートシェイプ</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：4,850,000円<br>
                        <span class="blue">B社</span>：4,750,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">地金+<br>5,000,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>250,000円</p>
                    </div>
                  </li>

                  <li class="box-4">
                    <div class="title">
                      <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/gem009.jpg" alt=""></p>
                      <p class="itemName">ペアシェイプ<br>ダイヤモンドルース</p>
                      <p class="itemdetail">カラット：5ct<br>カラー：F<br>クラアリティ：SI2<br>形状：ペアシェイプ</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：3,395,500円<br>
                        <span class="blue">B社</span>：3,325,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">地金+<br>3,500,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>175,000円</p>
                    </div>
                  </li>
                  <li class="box-4">
                    <div class="title">
                      <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/gem010.jpg" alt=""></p>
                      <p class="itemName">カルティエ　バレリーナ　ハーフエタニティダイヤリング</p>
                      <p class="itemdetail">PT950<br>カラット：0.51ct<br>カラー：F<br>クラリティ：VVS１<br>カット：VERY　GOOD</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：388,000円<br>
                        <span class="blue">B社</span>：380,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">地金+<br>400,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>20,000円</p>
                    </div>
                  </li>

                  <li class="box-4">
                    <div class="title">
                      <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/gem011.jpg" alt=""></p>
                      <p class="itemName">ハリーウィンストン<br>マドンナクロスネックレス</p>
                      <p class="itemdetail">カラット：<br>カラー：クラリティ：<br>カット：</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：970,000円<br>
                        <span class="blue">B社</span>：950,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">地金+<br>1,000,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>7,500円</p>
                    </div>
                  </li>
 
                  <li class="box-4">
                    <div class="title">
                      <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/gem012.jpg" alt=""></p>
                      <p class="itemName">ピアジェ　パッション<br>ダイヤリング</p>
                      <p class="itemdetail">Pt950<br>カラット：0.30ct<br>カラー：E<br>クラリティ：VVS2<br>カット：VG</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：　　　　円<br>
                        <span class="blue">B社</span>：　　　　円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price"><br>　　　　<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>　　　　円</p>
                    </div>
                  </li>
 
 
      </ul>

  <div class="kaitori_point">
            <h3>【宝石の高価買取ポイント】</h3>
宝石の査定時には、購入時に付属していた鑑定書や鑑別書もぜひあわせてお持ちください。<br>
これらの書類には、「宝石が天然ものか人工的に作られたものか」といった情報が記載されており、査定の処理がスムーズになります。また、宝石入りアクセサリーの場合購入時に入っていた箱や替えのチェーン等の付属品があると査定金額が上がる可能性があります。<br>
もちろん、鑑定書・鑑別書、箱等がなくても宝石そのものの価値は変わりません。「銀座一の高額査定」を目指して高く評価いたしますので、ご安心ください。 </div>

</div>

<div class="point_list">
            <div class="coint_bnr">
                <img src="<?php echo get_s3_template_directory_uri() ?>/images/coin_bnr01.png" alt="他の店鋪より1円でも安ければご連絡下さい。">
                <ul class="other_price">
                    <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/coin_bnr02.png" alt="他の店鋪より1円でも安ければご連絡下さい。"></li>
                    <li>ブリングは徹底した高価買取を行っております。お客様が愛着を持って身につけてきた服は、大切に買い取りさせて頂きます。 万が一、他店よりも1円でも安い価格であればおっしゃってください。なるべくお客様のご要望に答えられるよう、査定させて頂きます。<br>
                まずは、こちらから買い取り実績について、覗いてみてください。きっと、お客様の古着に相応しい、ご期待に添える価格での買い取りを行っているはずです。</li>
                </ul>
            </div>
  <h3 class="obi_tl">高価買取のポイント</h3>
  <img src="<?php echo get_s3_template_directory_uri() ?>/images/kaitori_point.png" alt="高価買い取りのポイント" class="point_img">
  <h3 class="obi_tl">ブランドリスト</h3>
  <ul class="cat_list">
      <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list01.png" alt=""></li>
      <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list02.png" alt=""></li>
      <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list03.png" alt=""></li>
      <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list04.png" alt=""></li>
      <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list05.png" alt=""></li>
      <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list06.png" alt=""></li>
      <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list07.png" alt=""></li>
      <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list08.png" alt=""></li>
      <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list09.png" alt=""></li>
      <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list10.png" alt=""></li>
      <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list11.png" alt=""></li>
      <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list12.png" alt=""></li>
      <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/cat_list13.png" alt=""></li>
  </ul>
</div>

<?php
  // お問い合わせ
  get_template_part('_action');
  
  // 3つのポイント
  get_template_part('_purchase');
  
  // お問い合わせ
  get_template_part('_action2');
  
  // 店舗
  get_template_part('_shopinfo');
  
  // フッター
  get_footer();