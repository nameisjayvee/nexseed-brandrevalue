<?php
get_header(); ?>
<span id="a8sales"></span>
<script>
a8sales({
"pid": "s00000017941001",
"order_number": "<?php echo do_shortcode("[cfdb-value form='買取査定（キット）' show='contact_id' limit='1']"); ?>",
"currency": "JPY",
"items": [
{
"code": "a8",
"price": 4000,
"quantity": 1
},
],
"total_price": 4000,
});
</script>
<div class="mv_area ">
<img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kv-contact.png " alt="BRAMD REVALUE ">
</div>
	  	  <div class="contact_end">
	  <h3>宅配買取申し込みの送信完了</h3>
	  <p>お問合せ誠にありがとうございました。<br><br>

送信いただいた内容は、担当者が確認した後、折り返しご連絡をさしあげます。<br>
お問合せ内容の確認メールを、自動配信でお送りしております。<br>
メールが届かない場合は、送信が完了していないことがございます。<br>
お手数ではございますが、下記メールアドレス宛までご連絡ください。<br><br>

E-Mail:<a href="mailto:info@shopbring.jp">info@brandrevalue.jp</a></p>
	  </div>

<?php echo do_shortcode('[mwform_formkey key="23205"]'); ?>
<?php
  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
