<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
  //'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  '001.jpg::ルースダイヤ　1.098ct　G　SI2　G　NONE::330,000::335,000::340,000::10,000',
  '002.jpg::ダイヤネックレス　Pt850　0.81ct　MD0.23ct::340,000::330,000::350,000 ::17,000',
  '003.jpg::ヴァンクリーフ&アーペル　クロアミニ　アチュールクロス　ダイヤネックレス::117,000::105,000::110,000 ::5,000',
  '004.jpg::ダイヤリング　Pt900　1.00ｃｔ　0.4ｃｔ::62,000::66,000::70,000::8,000',
  '005.jpg::ティファニー　Pt900　ダイヤリング::30,000::27,000::32,000::5,000',
  '006.jpg::ルースダイヤ　1.006ct　H　VS2　VG　NONE　::440,000::445,000::450,000::1,000',
  '007.jpg::ダイヤリング　Pt900　0.51ct　::45,000::47,800::50,000::5,000',
  '008.jpg::ルースダイヤ　1.793ct　J　SI2　G　NONE　::620,000::611,000::630,000::19,000',
  '009.jpg::ダイヤリング　K18　1.48ｃｔ　::58,000::60,000::63,000::5,000',
  '010.jpg::ブルガリ　B-ZERO1　ダイヤネネックレス　K18WG::220,000::215,000::225,000::10,000',
  '011.jpg::ブシュロン　ディアマン　ダイヤネックレス　750::225,000::219,000::230,000::11,000',
  '012.jpg::カルティエ　ハーフダイヤラブリング::97,000::93,000::103,000::10,000',
);


get_header(); ?>

<div id="primary" class="cat-page content-area">
	<div class="mv_area "> <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/lp_main/cat_diamond_main.jpg" alt="あなたのダイヤモンドお売り下さい！"> </div>
	<p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
	<p class="main_bottom">ダイヤモンド買取！！業界最高峰の買取価格をお約束いたします。</p>
	<div class="lp_main">
		<section class="dia_app_cont">
			<div>
			<h3>ダイヤモンドの鑑定書・グレーディングレポート・ソーティング・鑑別書について</h3>
			<p class="pdl">買取店のサイトでもたびたび見かける「鑑定書」、「グレーティングレポート」、「ソーティング」、「鑑別書」。それぞれどのようなものなのか、その役割や信用度の違いをご説明します。</p>
		</section>
		<section class="dia_app_cont">
			<h3>鑑定書（グレーディングレポート）の見方</h3>
			<p class="pdl">ダイヤモンドのグレーディングレポートとは鑑定書のことを指します。
				ダイヤモンドの価値を判別するのに必要な様々な指標とその詳細が記載されており、鑑定士に内容をお伝え頂きますと査定がより正確なお値段をスムーズに提示できますので是非ご活用下さいませ。 </p>
			<p class="dia_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/dia/app01.jpg" alt="スタッフ"></p>
			<div class="found_box">
				<h4><span>1</span>カット・形状</h4>
				<p>ダイヤモンドのカットや形状が記載されます。 </p>
			</div>
			<div class="found_box">
				<h4><span>2</span>寸法</h4>
				<p>自動計測装置を使用しガードル最大と最小と深さ（高さ）が記載されます。</p>
			</div>
			<div class="found_box">
				<h4><span>3</span>カラット（重量）</h4>
				<p>重量を表すカラットのほかにカット形式やサイズも記入記載しています。1カラット=0.2グラム。<br />
					ｇは小数点以下３位まで表示することが多いです。 </p>
			</div>
			<div class="found_box">
				<h4><span>4</span>カラー・グレード</h4>
				<p>GIAマスターストーン（基準石）と比較をして無色のＤから順にＺまでランク付けをします。 </p>
			</div>
			<div class="found_box">
				<h4><span>5</span>クラリティ・グレード</h4>
				<p>ダイヤモンドを10倍拡大で、インクルージョン（内包物）に関する特徴の程度（有無、場所、大きさなど）を考慮し評価されます。</p>
			</div>
			<div class="found_box">
				<h4><span>6</span>カットの特級</h4>
				<p>ダイヤモンドの形と仕上げを基に自動測定装置と目視で5段階で評価をします。</p>
			</div>
			<div class="found_box">
				<h4><span>7</span>蛍光性</h4>
				<p>ダイヤモンドの中にはブラックライト（紫外線）でブルーに発光をするものがあります。<br />
					蛍光マスターストーンと比較をして評価をし、蛍光性が低いダイヤモンドは価値が高くなります。 </p>
			</div>
			<div class="found_box">
				<h4><span>8</span>レポート番号</h4>
			</div>
			<div class="found_box">
				<h4><span>9</span>プロポーション</h4>
				<p>テーブルの角から反対側までの4カ所を測定し、自動測定装置を使用して算出しています。</p>
			</div>
			<div class="found_box">
				<h4><span>10</span>その他</h4>
				<p>レポート記載以外のその他の項目や備考を記載します。</p>
			</div>
			<div class="found_box">
				<h4><span>11</span>プロット</h4>
				<p>ダイヤモンドの内包物を記載します。緑色はダイヤモンドの表面、赤色はダイヤ内側の内側、
					黒色はカッティングの際に余計につけられた傷として表記されます。 </p>
			</div>
		</section>
		<section class="dia_app_cont">
			<h3>ソーティングについて</h3>
			<p class="dia_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/dia/app02.jpg" alt="スタッフ"></p>
			<p class="pdl">ソーティングとは、ダイヤモンドの鑑定機関が発行する簡易鑑定になります。
				鑑定書のような冊子にはなっておらず、鑑定書のように詳細な記述はありません。
				鑑定書と違い、カットグレードなどダイヤモンドの４Cに関する詳細な記述はありませんが、実質的には、鑑定書と同様にダイヤモンドの品質に関して信用性があるもので、実務でclass="pdl"は、ダイヤモンドの取引業者の間で主に使用されています。 </p>
		</section>
		<section class="dia_app_cont">
			<h3>鑑別書について</h3>
			<p class="dia_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/dia/app03.jpg" alt="スタッフ"></p>
			<p class="pdl">鑑別書は、ダイヤモンドのみならず、宝石全般の鑑別結果を示す為に用意されるものです。通常、宝石の種類（天然石、合成石、模造石）や人口処理の有無が記されています。
				また、鑑別書は、あくまでもの鑑別結果を表記するためのもので、その宝石の品質についての記載はされていません。
				ダイヤモンドの場合は、その品質に対する評価が記載された鑑定書（グレーティングレポート）、あるいは羅石のダイヤモンドの場合はソーティングが添えられます。鑑別書のみの場合、そのダイヤモンドの品質についての保証は無いということができます。鑑別所のみの場合は、通常宝石自体の品質はあまり高くなく、販売者側の都合でそれを隠していることが多いので、ダイヤモンドの買取査定額には注意が必要です。 </p>
		</section>

	</div>
				<ul class="dia_nav">
		<li><a href="<?php echo home_url('/cat/diamond/diamond-qa'); ?>">ダイヤモンド 買取Q&A</a></li>
		<li><a href="<?php echo home_url('cat/diamond/diamond-term'); ?>">ダイヤモンド 用語集</a></li>
		<li><a href="<?php echo home_url('/cat/diamond/diamond-app'); ?>">ダイヤモンド 鑑定書</a></li>
		<li><a href="<?php echo home_url('/cat/diamond/diamond-voice'); ?>">お客様の声</a></li>
		<li><a href="<?php echo home_url('/cat/diamond/diamond-staff'); ?>">査定士紹介</a></li>
	</ul>
	<!-- lp_main -->
</div>
<!-- #primary -->
<script src="//kaitorisatei.info/brandrevalue/wp-content/themes/bring/js/gaisan.js" type="text/javascript"></script>
<?php

get_footer();
