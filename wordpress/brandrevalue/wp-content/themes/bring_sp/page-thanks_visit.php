<?php
get_header(); ?>


<div class="mv_area ">
<img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kv-contact.png " alt="BRAMD REVALUE ">
</div>
	  	  <div class="contact_end">
		<h3>来店お申込みの完了</h3>
	  <p>お申込み頂き誠にありがとうございました。<br><br>

送信いただいた内容は、担当者が確認した後、折り返しご連絡をさしあげます。<br>
お問合せ内容の確認メールを、自動配信でお送りしております。<br>
メールが届かない場合は、送信が完了していないことがございます。<br>
お手数ではございますが、下記メールアドレス宛までご連絡ください。<br><br>

E-Mail:<a href="mailto:info@shopbring.jp">info@brandrevalue.jp</a></p>
	  </div>

<?php
  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
