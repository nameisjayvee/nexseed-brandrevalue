<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


get_header(); ?>

    <div id="primary" class="cat-page content-area">
        <div class="mv_area "> <img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/mv.png" alt="着物買取"> </div>

        <div id="kimono_content">
            <div class="kimono_innner">
              <div class="cv_block">
                  <div class="tel_cv">
                      <p>今すぐ電話で無料相談！</p>
                      <p><a href="tel:0120970060"><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/cv01.png" alt="着物">0120-970-060</a></p>
                      <p>受付時間:11時～21時　※年中無休</p>
                  </div>
                  <ul>
                      <li><a href="<?php echo home_url('purchase/syutchou'); ?>"><span>その場で査定、即現金支払い</span>出張買取</a></li>
                      <li><a href="<?php echo home_url('purchase/takuhai'); ?>"><span>最短翌日ご連絡&ご入金</span>宅配買取</a></li>
                  </ul>
              </div>
              <section class="first_catch">
                  <h2>どんな着物も喜んで査定します！<br />どこよりも高額で買い取ります！</h2>
                  <p>箪笥に眠ったままの思い入れのある着物や帯など、処分に困っている和装品全般のお悩みは私たちにお任せください。<br />卓越した着物の査定技術を持ったスタッフが悩みを解消し、ご満足いただけるよう丁寧に対応致します。<br />適正な査定と独自の流通ルートの確保により、どこよりも高額な買取を実現しています。<br />着物に関するご質問、お気軽にお問合せください！
                  </p>
              </section>

                <div style="margin-top:20px; margin-bottom: 20px;"><a href="https://kaitorisatei.info/brandrevalue/blog/doburock"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/uploads/2018/12/caf61dc6779ec6137c0ab58dfe3a550d.jpg" alt="どぶろっく"></a></div>

            </div>
            <section id="lp-cat-jisseki">

								<div class="kimono_innner">
                <div class="jisseki_top">
                <h2><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/jisseki_ttl.png" alt="他社との買取額を比較すると"></h2>
                    <p>当店なら<span>3</span>点まとめてこの価格 !</p>
                    <p><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/hikaku.png" alt="着物"></p>
                    <div class="price_txt">
                    <h3>型絵染着物<br />まとめ</h3>
                    <div class="price_warp">
                      <p><span>A社</span>：182,000円</p>
                      <p><span>B社</span>：298,000円</p>
                    </div>
                    </div>
                        <div class="our_price">
                            <p><span>ブランドリバリュー</span>買取価格</p>
                          <p>334,000円</p>
                        </div>

                </div>
								<h3 class="jisseki_ttl">買取実績</h3>
                <ul id="box-jisseki" class="list-unstyled clearfix">
                  <li class="box-4">
                      <div class="title"> <img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/item/kimono01.jpg" alt="人間国宝　久保田一竹　訪問着">
                        <p class="itemName"><span>辻が花訪問着<br />モダンな帯　まとめ</span></p>
                        <hr /><p class="price_data"><span class="red">A社</span>：53,000円<br /><span class="blue">B社</span>：98,000円</p></div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">148,000<span class="small">円</span></p>
                    </div>
                  </li>
                  <li class="box-4">
                      <div class="title"> <img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/item/kimono02.jpg" alt="型絵染着物　まとめ">
                        <p class="itemName"><span>付け下げ　袋帯</span></p>
                        <hr /><p class="price_data"><span class="red">A社</span>：8,000円<br /><span class="blue">B社</span>：13,000円</p></div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">26,000<span class="small">円</span></p>
                    </div>
                  </li>
                  <li class="box-4">
                      <div class="title"> <img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/item/kimono03.jpg" alt="型絵染着物　まとめ">
                          <p class="itemName"><span>型絵染着物　まとめ</span></p>
                          <hr /><p class="price_data"><span class="red">A社</span>：182,000円<br /><span class="blue">B社</span>：298,000円</p></div>
                      <div class="box-jisseki-cat">
                          <h3>買取価格例</h3>
                          <p class="price">368,000<span class="small">円</span></p>
                      </div>
                  </li>

                  <li class="box-4">
                      <div class="title"> <img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/item/kimono04.jpg" alt="着物まとめ">
                          <p class="itemName"><span>着物　まとめ</span></p>
                          <hr /><p class="price_data"><span class="red">A社</span>：2,000円<br /><span class="blue">B社</span>：11,000円</p></div>
                      <div class="box-jisseki-cat">
                          <h3>買取価格例</h3>
                          <p class="price">28,000<span class="small">円</span></p>
                      </div>
                  </li>
                  <li class="box-4">
                      <div class="title"> <img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/item/kimono05.jpg" alt="黒留袖　西陣織帯　まとめ">
                          <p class="itemName"><span>黒留袖<br />西陣織帯　まとめ</span></p>
                          <hr /><p class="price_data"><span class="red">A社</span>：8,000円<br /><span class="blue">B社</span>：21,000円</p></div>
                      <div class="box-jisseki-cat">
                          <h3>買取価格例</h3>
                          <p class="price">33,000<span class="small">円</span></p>
                      </div>
                  </li>
                  <li class="box-4">
                      <div class="title"> <img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/item/kimono06.jpg" alt="紬　紬帯　西陣帯　まとめ">
                        <p class="itemName"><span>男性もの　着物一式</span></p>
                        <hr /><p class="price_data"><span class="red">A社</span>：3,500円<br /><span class="blue">B社</span>：8,000円</p></div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">25,000<span class="small">円</span></p>
                    </div>
                  </li>
                  <li class="box-4">
                      <div class="title"> <img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/item/kimono07.jpg" alt="付け下げ　袋帯">
                          <p class="itemName"><span>人間国宝<br />久保田一竹訪問着</span></p>
                          <hr /><p class="price_data"><span class="red">A社</span>：160,000円<br /><span class="blue">B社</span>：181,000円</p></div>
                      <div class="box-jisseki-cat">
                          <h3>買取価格例</h3>
                          <p class="price">285,000<span class="small">円</span></p>
                      </div>
                  </li>
                  <li class="box-4">
                      <div class="title"> <img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/item/kimono08.jpg" alt="紬　紬帯　西陣帯　まとめ">
                          <p class="itemName"><span>紬　紬帯<br />西陣帯　まとめ</span></p>
                          <hr /><p class="price_data"><span class="red">A社</span>：8,000円<br /><span class="blue">B社</span>：21,000円</p></div>
                      <div class="box-jisseki-cat">
                          <h3>買取価格例</h3>
                          <p class="price">48,000<span class="small">円</span></p>
                      </div>
                  </li>
                </ul>
							</div>
            </section>
            <section class="kimoo_koumokou">
              <h2 class="content_ttl">高価買取項目</h2>
              <div class="kimono_innner">
                <h3>人間国宝・有名染織作家</h3>
                <p>北村武資、森口邦彦、平良敏子、福田喜重、小宮康孝など国の重要無形文化財保持者いわゆる人間国宝の、作品や羽田登喜男、木村雨山、志村ふくみ、山下めゆ、田島比呂子、田畑喜八、玉那覇有公、福田喜重、森口華弘、山田栄一、久保田一竹、由水十久、松井青々、稲垣稔次郎、千葉あやの、芹沢銈介、森口邦彦、二塚長生、鎌倉芳太郎、北出与三郎、柿本市郎などの有名作家の作品。</p>
                <h3>有名産地の染め着物・紬</h3>
                  <p>加賀友禅・京友禅・東京友禅 有名産地作品の本場結城紬や、本場大島紬、本場黄八丈、牛首紬、久米島紬など。 結城紬は「結」マーク、「紬」マークで本物かどうか確認できます。</p>
                  <h3>有名織元・染匠・呉服店</h3>
                  <p>千總、勝山織物、桝屋高尾、白木染匠、都喜ヱ門、広瀬織物、龍村美術織物、川島織物、ゑり善、銀座むら田、銀座伊勢由、 銀座こうげい、志ま亀、大羊居、大彦、竺仙、紬屋吉平、満つ本、きしやなど有名織元、染匠、呉服店の品 。</p>
                  <h3>アンティーク着物</h3>
                  <p>大正ロマン着物や昭和モダン着物と呼ばれる昭和初期以前の着物をアンティーク着物と呼びます。 リサイクル着物と一緒にされがちですが、アンティーク着物は価値がある古い着物であるともいえます。 まだまだ大量生産の技術がない時代のものなので、刺繍や染めなど大変手間がかかっており、 経年変化で変わりゆく色合いもアンティーク着物の魅力。 アンティークの着物を売る場合も価値がきちんとわかる専門の業者でないと、古いという理由でリサイクル着物としての価値をつけてくれないケースもあるので注意が必要です。</p>

              </div>
            </section>

        <?php
// 買取方法
get_template_part('_purchase');
?>

<section class="kimoo_item">
  <h2 class="content_ttl">買取アイテム</h2>
  <div class="kimono_innner">
    <ul>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/01.png" alt="着物買取"><span>留袖</span></li>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/02.png" alt="着物買取"><span>振袖</span></li>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/03.png" alt="着物買取"><span>訪問着</span></li>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/04.png" alt="着物買取"><span>付け下げ</span></li>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/05.png" alt="着物買取"><span>小紋</span></li>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/06.png" alt="着物買取"><span>色無地</span></li>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/07.png" alt="着物買取"><span>紬</span></li>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/08.png" alt="着物買取"><span>友禅</span></li>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/09.png" alt="着物買取"><span>上布</span></li>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/10.png" alt="着物買取"><span>沖縄着物</span></li>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/11.png" alt="着物買取"><span>アンティーク</span></li>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/12.png" alt="着物買取"><span>男性着物</span></li>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/13.png" alt="着物買取"><span>作家着物</span></li>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/14.png" alt="着物買取"><span>袋帯</span></li>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/15.png" alt="着物買取"><span>名古屋帯</span></li>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/16.png" alt="着物買取"><span>丸帯</span></li>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/17.png" alt="着物買取"><span>半幅帯</span></li>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/18.png" alt="着物買取"><span>反物</span></li>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/19.png" alt="着物買取"><span>和装小物</span></li>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/kaitori_item/20.png" alt="着物買取"><span>和装上着</span></li>
    </ul>
  </div>
  <div class="kimono_point">
    <div class="kimono_innner">
      <h3>高価買取のポイント</h3>
      <h4>1 : 証紙</h4>
      <p>着物の産地や品質を証明するための証紙も大事なポイントです 。</p>
      <h4>2 : パールトーン加工</h4>
      <p>撥水加工の有無でも査定が変わります。</p>
      <h4>3 : 仕付け糸</h4>
      <p>未使用、洗い張り後の証となります。</p>
      <h4>4 : サイズ</h4>
      <p>裄、丈等、長いものほど高価査定が可能です。</p>
      <h4>5 : 作家</h4>
      <p>作家が作ったものかどうかで大きく査定額も変わります。</p>
      <h4>6 : おまとめ査定</h4>
      <p>数点まとめていただくことで査定額がUPします。</p>
    </div>
  </div>
</section>
<section class="kimoo_onayami">
  <h2 class="content_ttl">こんな状態でも買取します</h2>
  <div class="kimono_innner">
    <h3>こんなことで悩んでいませんか？</h3>
    <ul>
      <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/state01.png" alt="着物買取"><span>祖母が来ていた古いもの…</span></li>
        <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/state02.png" alt="着物買取"><span>シミ、汚れが…</span></li>
          <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/state03.png" alt="着物買取"><span>紋付の着物</span></li>
            <li><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/state04.png" alt="着物買取"><span>帯、反物だけ…</span></li>
    </ul>
</div>
<div class="kouka_kaitori">
  <div class="kimono_innner">
  <h3><span>古い物、傷物</span>も<br />高く買取査定致します！</h3>
  <p>
    傷のついたお品物、古くなったお品物も、コスト削減を徹底し高額買取りを可能としているBRAND REVALUEでは水準以上の価格で査定しています。
「どうせ高くは売れないから……」とあきらめる前に、ぜひ一度ご相談ください。
ワケあり品も高額に 査定できる可能性は十分にあります。もちろん査定自体は無料です。
宅配買取、出張買取も行っていますので、 こちらもお気軽にご利用ください。
  </p>
</div>
</div>
</section>
<section class="kimoo_satei">
  <h2>着物買取の<span>査定ポイント</span>をご紹介。<br />どんな着物も喜んで査定します！</h2>
  <div class="kimono_innner">
    <h3>まず押さえておきたいポイント、それは「着物の流行」です。</h3>
    <p>振袖や留袖は顕著で、世代ごとに流行の柄が変わります。流行りの柄だともちろん査定金額が高くなります。
着物もファッションですからその時々で好みも変わります。伝統的な色合いや奥ゆかしさ、年齢を問わずお召しになれるお着物は人気が高く
多くの方に親しまれ査定金額は高くなります。</p>
<h3>次に「作家物や老舗ブランドの品か」というのも大切なポイントです。</h3>
<p>有名作家や人間国宝、伝統工芸品などの作品は、美術的観点から見てもより高額査定になる可能性が高くなります。
芭蕉布、宮古上布、琉球紅型といった各地の伝統工芸品、「三大友禅」で知られる、京友禅、加賀友禅、東京友禅の着物など
文化的背景も弊社一流の査定士が拝見すれば、立派なブランドとして高く評価させて頂きます。</p>
<h3>忘れていけない、大事な「生地の素材」と「サイズ」について。</h3>
<p>着物の生地で高く売れやすいのは、正絹（しょうけん）で織られた着物です。正絹とは絹100%で誂えられ、価値が高いからです。
ポリエステルなどの化学繊維、ウールなどで作られた着物は査定額が低くなりやすく、買い取りの対象外になる場合もあります。
また仕立て直しのしやすい大きいサイズの着物の方が、サイズが小さい着物よりも高い金額がつきやすく、評価されやすい傾向があります。</p>
<h3>「シミや汚れ」も見落とせない重要なポイントです。</h3>
<p>査定をするときは、着物についたシミ、汚れ、ほつれ、ヤケ具合、抱きのシワなどをチェックします。
お繋ぎした方が気持ちよく袖を通す為にもお着物のお手入れの状態が非常に大切になってきます。
このような着物の汚れは、ご自身で対処しようとすると逆に今よりも汚れてしまう恐れがありますので、そのままにしておくことをお勧めします。</p>
<h3>お買取りの相場について。</h3>
<p>呉服屋さんでは、流通過程の段階で大幅に利益を上げないと、柄遅れや汚れが発生して成り立たない商売です。買取業者の査定は問屋の元値のまたさらにシビアな
ご評価になるので、小売り価格からは大幅に安いものになります。物や状態にもよりますが買ったときの１００分の１の金額のものや、買値のおよそ５分の１以下になるものもありそれ以上の値を付けるのは、作家物や余程のブランド物の場合だけです。</p>
<h3>おまとめ査定も大事なポイント！</h3>
<p>着物買取の際には複数の商品をまとめて買取に出すことで買取価格アップを期待することができます。
一点ではお買取りが難しいお品物でも、例えば成人式用の着物をセットで販売すればそれだけ多くの人が魅力的な商品だと考えますから、市場価値アップに繋がるのです。　そのため帯やコートなどがあれば、それを一緒に着物買取に出すことで、よりお得な買取をしてもらえます。</p>
  </div>
</section>


<section class="kimoo_voice">
  <h2 class="content_ttl">お客様の声	</h2>
  <div class="kimono_innner">
    <div class="voicebox">
      <p>タンスに肥やしていた着物がまさかのお値段に （70代女性）</p>
      <p>
        以前、華道や日本舞踊を嗜んでおりましたが着物を着る機会も少なくなり、作家物の着物だからと娘に着てもらおうか相談したところ好みや流行からか着てくれることはありませんでした為、タンスに眠らせたままになっておりました。その後、娘にこちらの会社様を紹介してもらい、少々不安ながらも予約をしたところオペレーターの方が親身になって聞いて下さり、ご依頼させて頂きました。ささやかながらも娘家族と孫と共に旅行に行くことができ大変満足しております。新しい思い出を有難うございました。
      </p>
    </div>
    <div class="voicebox">
      <p>亡くなった母の形見の着物も大切に見ていただけました。（70代女性）</p>
      <p>
      昨年母が亡くなり、以前から大切にしていた着物の行き先が無く、ただ傷んでしまい処分するのも忍びない気持ちでしたが、こちらのホームページを拝見し喜んで頂ける方がいらっしゃるなら母も報われる。そんな気持ちで申し込みを致しました。来て下さった方はとても腰が低く、こちらが恐縮してしまう程謙虚に査定して下さり、一点一点大切に取り扱ってくださいました。家族一同、心からありがとうございました。次回はいつになるかわかりませんが、私の着物もお願い致します。
      </p>
    </div>
    <div class="voicebox">
      <p>本当のプロフェッショナルでした。(60代女性)</p>
      <p>
      実家が呉服屋を営んでおり、私は幼き頃から着物に携わって参りました。この度、建て替えのため実家の空き家を立て壊すことになりその際に実家にあった茶器や骨董を含め着物と共に売却することになりました。着物にはウルさいほうだと自負している為下手な業者には依頼をしたくありませんでしたが、御社の鑑定士の方は知識や作法、言葉選び一つをとっても、何一つ不快にならず、心地よい時間を演出して頂きました。口うるさいおばばで申し訳御座いませんでした。お友達にも是非紹介させて頂きます。
      </p>
    </div>
    <div class="voicebox">
      <p>古い着物もきちんと見て頂き妻も私も喜んでおります。(60代男性)</p>
      <p>
      留袖や訪問着、紬や和装の小物を何点か買い取ってもらいました。特に着る機会が無ければ身内に欲しがる人もおらず思い切って売ることにしました。箪笥が3竿あり枚数が多く運ぶのが困難なため出張買取にてご依頼させて頂きました。査定時間は3時間ほどかかりましたが、値のつかない着物でさえ大切に見て頂けたため、とても温かい気持ちになりました。すべて買取してもらい心も晴れやかになっております。夫婦共々感謝申し上げます。
      </p>
    </div>

  </div>
</section>
<div class="kimono_innner">
    <div class="cv_block">
        <div class="tel_cv">
            <p>今すぐ電話で無料相談！</p>
            <p><a href="tel:0120970060"><img data-src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/cv01.png" alt="着物">0120-970-060</a></p>
            <p>受付時間:11時～21時　※年中無休</p>
        </div>
        <ul>
            <li><a href="<?php echo home_url('purchase/syutchou'); ?>"><span>その場で査定、即現金支払い</span>出張買取</a></li>
            <li><a href="<?php echo home_url('purchase/takuhai'); ?>"><span>最短翌日ご連絡&ご入金</span>宅配買取</a></li>
        </ul>
    </div>

</div>


</div>
<!-- #kimono_content -->
    </div>
    <!-- #primary -->

    <?php
    get_template_part('_shopinfo');
get_footer();
