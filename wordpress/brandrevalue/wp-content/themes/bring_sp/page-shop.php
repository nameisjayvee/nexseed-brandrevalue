<?php

get_header(); ?>

    <div class="mv_area "> <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kv-ginza.png " alt="店舗案内"> </div>
    <div class="kaitori_cnt">
        <h2 class="kaitori_tl"><br /> 銀座でジュエリー・時計・バッグの買取店をお探しの皆様へ。
            <br> ブランド買取なら高額査定のBRAND REVALUEへ。</h2>
        <p class="cnt_tx01">最寄り駅は、丸ノ内線・銀座線・日比谷線「銀座駅」のA5出口。<br> BRANDREVALUE(ブランドリバリュー)銀座店は、東京のシンボル三越銀座店の正面に位置する四谷学院ビル5階に店舗を構えております。
            <br>
        </p>
    </div>
    <div id="company">
        <table>
            <tr>
                <th>住所</th>
                <td>〒104-0061 東京都中央区銀座5-8-3 四谷学院ビル5階<br />
                    <a class="map_btn" href="https://goo.gl/maps/P7Hh7E9ayG22" target="_blank">Google MAP</a></td>
            </tr>
            <tr>
                <th>営業時間</th>
                <td>11：00～21：00</td>
            </tr>
            <tr>
                <th>電話番号</th>
                <td>0120-970-060</td>
            </tr>
            <tr>
                <th>駐車場</th>
                <td>駐車場</td>
            </tr>
        </table>
        <div class="shop_more"><a href="<?php echo home_url('ginza'); ?>">詳しくはこちら</a></div>
    </div>

    <div class="mv_area "> <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kv-shibuya.png " alt="店舗案内"> </div>
    <div class="kaitori_cnt">
        <h2 class="kaitori_tl"><br /> 渋谷で時計・バッグ・ジュエリー・貴金属の買取店をお探しの皆様へ。
            <br /> ブランド買取なら高額査定のBRAND REVALUEへ。</h2>
        <p class="cnt_tx01">最寄り駅は、渋谷駅。渋谷駅から徒歩3分。<br /> BRANDREVALUE(ブランドリバリュー)渋谷店は、渋谷のシンボルの一つであるタワーレコードを背にの右斜め前に位置する和光ビル4階に店舗を構えております。
            <br>
        </p>
    </div>
    <div id="company">
        <table>
            <tr>
                <th>住所</th>
                <td class="shop_map">
                    東京都渋谷区神南1-12-16 和光ビル4階<a class="map_btn" href="https://goo.gl/maps/2i5igG64gFp" target="_blank">Google MAP</a></td>
            </tr>
            <tr>
                <th>営業時間</th>
                <td>11：00～21：00</td>
            </tr>
            <tr>
                <th>電話番号</th>
                <td><a href="tel:0120-970-060">0120-970-060</a></td>
            </tr>
            <tr>
                <th>駐車場</th>
                <td> - </td>
            </tr>
        </table>
        <div class="shop_more"><a href="<?php echo home_url('shibuya'); ?>">詳しくはこちら</a></div>
    </div>
    <div class="mv_area "> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shinjuku_mv.png " alt="店舗案内"> </div>
    <div class="kaitori_cnt">
        <h2 class="kaitori_tl"><br /> 新宿でジュエリー・時計・バッグなどのブランド品買取先をお探しの皆様へ。<br>ブランド買取なら高額査定のBRAND REVALUEへ。</h2>
        <p class="cnt_tx01">最寄り駅は、JR・地下鉄・私鉄各線が通る、「JR新宿駅」「東京メトロ新宿三丁目駅」。<br>BRANDREVALUE(ブランドリバリュー)新宿店は、新宿地下道A5出口を出て、東京のシンボルの一つである新宿伊勢丹の向かいに立つUFJ銀行の真裏に位置する大伸第２ビル3階び店舗を構えております。<br>最寄り駅出口から徒歩3分と、非常にアクセスしやすい場所にあり、近くには伊勢丹、ビックロ、マルイ等、お買い物、お食事、ついででお越しの際にも便利にご利用頂けます。
        </p>
    </div>
    <div id="company">
        <table>
            <tr>
                <th>住所</th>
                <td class="shop_map">
                    〒160-0022<br>東京都新宿区新宿3丁目31-1大伸第２ビル3階<a class="map_btn" href="https://goo.gl/maps/dYjcme5kcj72" target="_blank">Google MAP</a></td>
            </tr>
            <tr>
                <th>営業時間</th>
                <td>11：00～21：00</td>
            </tr>
            <tr>
                <th>電話番号</th>
                <td><a href="tel:0120-970-060">0120-970-060</a></td>
            </tr>
            <tr>
                <th>駐車場</th>
                <td> - </td>
            </tr>
        </table>
        <div class="shop_more"><a href="<?php echo home_url('shinjuku'); ?>">詳しくはこちら</a></div>
    </div>

    <div class="mv_area "> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shinsaibashi_mv.png " alt="店舗案内"> </div>
    <div class="kaitori_cnt">
        <h2 class="kaitori_tl"><br /> 大阪・心斎橋でジュエリー・時計・バッグなどのブランド品買取先をお探しの皆様へ。<br>ブランド買取なら高額査定のBRAND REVALUEへ。</h2>
        <p class="cnt_tx01">銀座・渋谷・新宿で高価買取実績のブランドリバリュー（BRAND REVALUE）がついに大阪心斎橋でオープン！最寄り駅は、地下鉄御堂筋線「心斎橋駅」。<br>ルイヴィトンの目の前という好立地で心斎橋駅3番出口徒歩3秒のビルに店舗を構えております。駅スグなので商店街を通ることもなく商品の持ち運びはラクラク。お気軽にご利用いただけます。
        </p>
    </div>
<div id="company">
        <table>
            <tr>
                <th>住所</th>
                <td class="shop_map">
                    〒542-0081<br>大阪府大阪市中央区南船場4-4-8 クリエイティブ心斎橋8階<a class="map_btn" href="https://goo.gl/maps/3JQXWvJ3idF2" target="_blank">Google MAP</a></td>
            </tr>
            <tr>
                <th>営業時間</th>
                <td>11：00～21：00</td>
            </tr>
            <tr>
                <th>電話番号</th>
                <td><a href="tel:0120-970-060">0120-970-060</a></td>
            </tr>
            <tr>
                <th>駐車場</th>
                <td> - </td>
            </tr>
        </table>
        <div class="shop_more"><a href="<?php echo home_url('shinsaibashi'); ?>">詳しくはこちら</a></div>
    </div>







    <?php

  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');



  // フッター
  get_footer();
