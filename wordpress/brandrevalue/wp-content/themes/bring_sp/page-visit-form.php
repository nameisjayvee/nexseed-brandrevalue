<?php
get_header(); ?>

<div class="mv_area">
<img data-src="<?php echo get_s3_template_directory_uri() ?>/img/visit_form/mv.png " alt="BRAMD REVALUE ">
</div>
<div class="kaitori_cnt visit mt30">
<div class="cnt01">
<h3 class="cnt_tl"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/visit_form/ttl01.png" alt="待ち時間短縮、査定基準は全国一律！"></h3>
<p class="cnt_tx">ご予約のお客様を優先的にご案内！混みあってしまった場合でも、お待ち頂かずにご案内可能です。
また、査定基準の統一により「場所によって金額に差があるのでは…」「〇〇の店舗にしか行けない…」等といったお客様の不安を解消いたします。</p>
</div>

<div class="cnt02">
<h3 class="cnt_tl"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/visit_form/ttl02.png" alt="予約で金額アップ！"></h3>
<p class="cnt_tx">ご来店予約でさらに査定金額アップが可能となります。ご成約の場合には、さらにタクシー代の負担もさせていただきます。
※一部対象外エリアがございます。ご希望のお客様は、事前にカスタマーサポートセンターまでお問合せください。
</p>
</div>

<div class="cnt03">
<h3 class="cnt_tl"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/visit_form/ttl03.png" alt="手ぶらで買取"></h3>
<p class="cnt_tx">大きなお荷物、たくさんの商品を売りたいけど持って行くのが大変…」
「しっかり対面で説明を受けたい」そんなお客様にお勧めさせて頂くサービスです。
お買取り希望のお品物を先に店頭へ宅配して頂いた後、お客様は手ぶらでご来店頂きます。
来店されましたら、当店査定士がお買取りをさせて頂きます。
宅配用キットは弊社でご用意させて頂きますので、ご安心下さいませ。</p>
</div>
</div>

<section>
			<h2 class="obi_tl mgnone">来店お申込みフォーム</h2>
			<div class="mailform_new02"> <?php echo do_shortcode('[mwform_formkey key="23535"]'); ?> </div>
		</section>

<script src="http://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script type="text/javascript">
(function () {

    "use strict";

    /**
     * Pardot form handler assist script.
     * Copyright (c) 2018 toBe marketing, inc.
     * Released under the MIT license
     * http://opensource.org/licenses/mit-license.php
     */

    // == 設定項目ここから ==================================================================== //

    /*
     * ＠データ取得方式
     */
    var useGetType = 1;

    /*
     * ＠フォームハンドラーエンドポイントURL
     */
    var pardotFHUrl="//go.staygold.shop/l/436102/2018-05-02/f3jb4y";

    /*
     * ＠フォームのセレクタ名
     */
    var parentFormName = '#mw_wp_form_mw-wp-form-23535 > form';

    /*
     * ＠送信ボタンのセレクタ名
     */
    var submitButton = 'input[type=submit][name=confirm]';

    /*
     * ＠項目マッピング
     */

    var defaultFormJson=[
    { "tag_name": "sei",               "x_target": 'sei'               },
    { "tag_name": "mei",               "x_target": 'mei'               },
    { "tag_name": "kana-sei",          "x_target": 'kana-sei'          },
    { "tag_name": "kana-mei",          "x_target": 'kana-mei'          },
    { "tag_name": "email",             "x_target": 'email'             },
    { "tag_name": "tel",               "x_target": 'tel'               },
    { "tag_name": "request_store",     "x_target": 'request_store'     },
    { "tag_name": "request_date1",     "x_target": 'request_date1'     },
    { "tag_name": "request_time1",     "x_target": 'request_time1'     },
    { "tag_name": "request_date2",     "x_target": 'request_date2'     },
    { "tag_name": "request_time2",     "x_target": 'request_time2'     },
    { "tag_name": "request_date3",     "x_target": 'request_date3'     },
    { "tag_name": "request_time3",     "x_target": 'request_time3'     },
    { "tag_name": "inquiry",           "x_target": 'inquiry'           },
    { "tag_name": "inquiry_item",      "x_target": 'inquiry_item'      },
    { "tag_name": "mailing",           "x_target": 'mailing'           }
    ];

    // == 設定項目ここまで ==================================================================== //

    // 区切り文字設定
    var separateString = ',';

    var iframeFormData = '';
    for (var i in defaultFormJson) {
        iframeFormData = iframeFormData + '<input id="pd' + defaultFormJson[i]['tag_name'] + '" type="text" name="' + defaultFormJson[i]['tag_name'] + '"/>';
    }

    var iframeHeadSrc =
        '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';

    var iframeSrcDoc =
        '<form id="shadowForm" action="' + pardotFHUrl + '" method="post" accept-charset=\'UTF-8\'>' +
        iframeFormData +
        '<input id="shadowSubmit" type="submit" value="send" onclick="document.charset=\'UTF-8\';"/>' +
        '</form>';

    var shadowSubmited = false;
    var isError = false;
    var pdHiddenName = 'pdHiddenFrame';

    /**
     * エスケープ処理
     * @param val
     */
    function escapeSelectorString(val) {
        return val.replace(/[ !"#$%&'()*+,.\/:;<=>?@\[\\\]^`{|}~]/g, "\\$&");
    }

    $(function () {

        $(submitButton).click(function () {
            // Submitボタンを無効にする。
            $(submitButton).prop("disabled", true);
        });

        $('#' + pdHiddenName).remove();

        $('<iframe>', {
            id: pdHiddenName
        })
            .css({
                display: 'none'
            })
            .on('load', function () {

                if (shadowSubmited) {
                    if (!isError) {

                        shadowSubmited = false;

                        // 送信ボタンを押せる状態にする。
                        $(submitButton).prop("disabled", false);

                        // 親フォームを送信
                        $(parentFormName).submit();
                    }
                } else {
                    $('#' + pdHiddenName).contents().find('head').prop('innerHTML', iframeHeadSrc);
                    $('#' + pdHiddenName).contents().find('body').prop('innerHTML', iframeSrcDoc);

                    $(submitButton).click(function () {
                        shadowSubmited = true;
                        try {
                            for (var j in defaultFormJson) {
                                var tmpData = '';

                                // NANE値取得形式
                                if (useGetType === 1) {
                                    $(parentFormName + ' [name="' + escapeSelectorString(defaultFormJson[j]['x_target']) + '"]').each(function () {

                                        //checkbox,radioの場合、未選択項目は送信除外
                                        if (["checkbox", "radio"].indexOf($(this).prop("type")) >= 0) {
                                            if ($(this).prop("checked") == false) {
                                                return true;
                                            }

                                        }

                                        if (tmpData !== '') {
                                            tmpData += separateString;
                                        }

                                        // 取得タイプ 1 or Null(default) :val()方式, 2:text()形式
                                        if (defaultFormJson[j]['x_type'] === 2) {
                                            tmpData += $(this).text().trim();
                                        } else {
                                            tmpData += $(this).val().trim();
                                        }
                                    });
                                }

                                // セレクタ取得形式
                                if (useGetType === 2) {
                                    $(defaultFormJson[j]['x_target']).each(function () {
                                        if (tmpData !== '') {
                                            tmpData += separateString;
                                        }

                                        // 取得タイプ 1 or Null(default) :val()方式, 2:text()形式
                                        if (defaultFormJson[j]['x_type'] === 2) {
                                            tmpRegexData = $(this).text().trim();
                                        } else {
                                            tmpRegexData = $(this).val().trim();
                                        }
                                    });
                                }

                                $('#' + pdHiddenName).contents().find('#pd' + escapeSelectorString(defaultFormJson[j]['tag_name'])).val(tmpData);

                            }

                            $('#' + pdHiddenName).contents().find('#shadowForm').submit();

                        } catch (e) {
                            isError = true;

                            $(submitButton).prop("disabled", false);

                            $(parentFormName).submit();
                            shadowSubmited = false;
                        }
                        return false;
                    });
                }
            })
            .appendTo('body');
    });
})();
</script>

<?php
  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');
  ?>

  <?php

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
