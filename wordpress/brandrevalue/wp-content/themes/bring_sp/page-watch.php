<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  '001.jpg::パテックフィリップ::カラトラバ PG 手巻き::1,480,000::1,420,000::1,510,000::90,000',
  '002.jpg::ブライトリング::アナビタイマー AB0120::370,000::364,000::380,000::16,000',
  '003.jpg::パネライ::PAM00088 GMT::370,000::364,000::380,000::16,000',
  '004.jpg::ボールウォッチ::ストークマン ストームチェイサー::128,000::124,000::131,000::7,000',
  '005.jpg::ROLEX::サブマリーナ 116610LN::570,000::560,000::600,000::40,000',
  '006.jpg::オーデマピゲ::ミレネリー::1,000,000::987,000::1,024,000::37,000',
  '007.jpg::パテックフィリップ::ゴンドーロ 手巻き::1,100,000::1,000,000::1,140,000::140,000',
  '008.jpg::ブレゲ::トランスアトランティック::1,012,000::1,000,000::1,020,000 ::20,000',
  '009.jpg::ブライトリング::ベントレーGT A13362::362,000::358,000::376,000::18,000',
  '010.jpg::コルム::アドミラルズカップ チャレンジ44 スプリット::310,000::306,000::314,000::8,000',
  '011.jpg::オメガ::スピードマスター クロノグラフ::115,000::110,000::121,000::11,000',
  '012.jpg::パネライ::ラジオミール 750WG ダイヤインデックス::1,760,000::1,750,000::1,800,000::50,000',
);


get_header(); ?>

<div id="primary" class="cat-page content-area">
    <div class="mv_area "> <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/lp_main/cat_watch_main.jpg" alt="時計の取扱いについて"> </div>
    <p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
    <p class="main_bottom">他社を圧倒！ブランドリバリューはブランド時計の買取実績多数！</p>
    <div id="lp_head" class="watch_ttl">
        <div>
            <p>銀座で最高水準の査定価格・サービス品質をご体験ください。</p>
            <h2>あなたの高級ブランド腕時計<br />
                どんな物でもお売り下さい！！</h2>
        </div>
    </div>
    <div class="lp_main">
        <section id="hikaku" class="watch_hikaku">
            <p class="hikaku_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/watch_hikaku.png"></p>
        </section>

        <!-- <section id=" lp_history ">
                <h3 class="obi_tl">有名ブランドの歴史</h3>
                <ul class="his_btn">
                    <li><a rel="leanModal" href="#div_point01"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/btn_history01.jpg" alt="ロレックス"></a></li>
                    <li><a rel="leanModal" href="#div_point02"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/btn_history02.jpg" alt="パテックフィリップ"></a></li>
                    <li><a rel="leanModal" href="#div_point03"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/btn_history03.jpg" alt="ウブロ"></a></li>
                    <li><a rel="leanModal" href="#div_point04"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/btn_history04.jpg" alt="オーデマピゲ"></a></li>
                    <li><a rel="leanModal" href="#div_point05"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/btn_history05.jpg" alt="ブライトリング"></a></li>
                    <li><a rel="leanModal" href="#div_point06"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/btn_history06.jpg" alt="ブレゲ"></a></li>
                </ul>

                <div id="div_point01" class="div_point">
                <a class="modal_close" href="#"></a>
                    <h5>ROLEX（ロレックス）とは</h5>
                    <p>ROLEX（ロレックス）は1905年にウィルスドルフ＆デイビスという名前の時計商社として設立され
                        <br> 幾度と名前の変更を経て、現在のROLEX（ロレックス）となりました。
                    </p>
                    <p>ROLEX（ロレックス）と聞けば王冠マークをすぐに思い浮かべられるほど誰もが知る有名高級時計です。
                        <br> それほどの知名度を築くに至ったのもROLEX（ロレックス）が独自に開発をし、現在では多くの
                        <br> 自動巻き時計に採用されている数々の特許技術でしょう。
                    </p>
                    <p>まずはパーペチュアル機構です。それ以前にも自動巻きの機構は存在しましたが、ROLEX（ロレックス）の
                        <br> 開発したパーペチュアル機構（全回転機構）により自動巻き時計は完成となり、ROLEX（ロレックス）は
                        <br> ブランド自動巻き時計の第一号となったのです。
                    </p>
                    <p>その後もオイスターケースという防水仕様のケースの開発、デイトジャストと呼ばれる日付が0時ちょうどで
                        <br> 切り替わる機構など、数々の革新的な技術の開発を行ってきました。
                    </p>
                    <p>また、機構の開発だけに留まらず時計そのものとしてのクオリティーも細部に至るまでこだわり作られている
                        <br> こともROLEX（ロレックス）が現在の地位を獲得した要因でしょう。
                    </p>
                    <p>10年に1度はオーバーホールが必要ではありますが、きちんとしたメンテナンスを行っていれば正に一生ものと
                        <br> 呼ぶにふさわしい時計です。
                    </p>
                    <p>有名なモデルとして、デイトジャスト、サブマリーナ、デイトナ、GMTマスター、エクスプローラーⅠ、 デイトジャスト、ヨットマスター、ターノグラフ、エアキング、シードュエラ、ミルガウスなど
                    </p>
                </div>

                <div id="div_point02" class="div_point">
                <a class="modal_close" href="#"></a>
                    <h5>PATEK PHILIPPE（パテックフィリップ）とは</h5>
                    <p>PATEK PHILIPPE（パテックフィリップ）は誰もが知るブランド時計では御座いません。
                        <br> しかし、世界三大高級時計として世界各国で愛されている最高級の時計ブランドです。
                    </p>
                    <p>PATEK PHILIPPE（パテックフィリップ）はカルティエやティファニー、ギュブラン、宝石店向けに製品を
                        <br> 納品していたこともあり、世界一高級な時計を販売するマニュファクチュールとして知られています。
                    </p>
                    <p>PATEK PHILIPPE（パテックフィリップ）はどんなに古い商品でも自社の物であれば修理可能と永久修理保証と
                        <br> 宣伝している為、PATEK PHILIPPE（パテックフィリップ）の時計は一生ものというイメージを確立しております。</p>
                    <p>有名なモデルとして、カラトラバ、コンプリケーテッド、グランドコンプリケオーション、アクアノート、ゴンドーロ、 トゥエンティー・フォー、ノーチラス、ゴールデンイリプスなど
                    </p>
                </div>

                <div id="div_point03" class="div_point">
                <a class="modal_close" href="#"></a>
                    <h5>HUBLOT（ウブロ）とは</h5>
                    <p>HUBLOT（ウブロ）は1980年に誕生し、独特なベゼル、ケースデザインやベルトにラバーを採用するなど
                        <br> 独自の世界観で多くの時計ファンに衝撃を与えた高級時計ブランドです。
                    </p>
                    <p>ユニークなデザインの為、クラシックなものと違い派手さ、華やかさから多くのスポーツ選手や芸能人が
                        <br> 愛用しております。
                    </p>
                    <p>有名なモデルにクラシックフュージョン、ビッグバン、ビッグバンキング、キングパワーなど</p>
                </div>

                <div id="div_point04" class="div_point">
                <a class="modal_close" href="#"></a>
                    <h5>AUDEMARS　PIGUET（オーデマピゲ）とは</h5>
                    <p>AUDEMARS　PIGUET（オーデマピゲ）は1875年ジュール＝ルイ・オーデマと
                        <br> 友人のエドワール＝オーギュスト・ピゲにより設立された高級時計メーカーです。
                    </p>
                    <p>お互いの技術力の高さから複雑時計の開発を始め、1889年に第10回パリ万博で
                        <br> スプリットセコンドグラフや永久カレンダーなどを装備する、グランド・コンプリカシオン
                        <br> 懐中時計を発表。1892年に世界で初めてのミニッツリピーターを搭載した腕時計の
                        <br> 独創的な製品を発表し「複雑度系のオーデマピゲ」として名声を獲得しております。
                    </p>
                    <p>現在でも、創業者一族が経営を引き継いでおり高い情熱と技術が継承されております。</p>
                    <p>厚さ1.62mmの世界最薄手巻きムーブメントや、1972年にはラグジュアリースポーツウォッチと
                        <br> いつ新カテゴリーを築いたロイヤルオークを発表し、誕生から40年をヘタ現在も高い人気を誇ります。
                    </p>
                    <p>有名なモデルにロイヤルオーク オフショア、ロイヤルオーク、ジュール・オーデマ、エドワール・ピゲ、ミレネリーなど</p>
                </div>

                <div id="div_point05" class="div_point">
                <a class="modal_close" href="#"></a>
                    <h5>BREITLING（ブライトリング）とは</h5>
                    <p>BREITLING（ブライトリング）の歴史はクロノグラフの進化の歴史といっても過言ではありません。</p>
                    <p>創業者のレオン・ブライトリングは計測機器の開発に特化しており、各分野のプロ用クロノグラフを
                        <br> 製造。息子ガストンが会社を継ぎ、1915年に独立した専用プッシュボタンを備えた世界初の腕時計型
                        <br> クロノグラフの開発に成功。1934年にリセットボタンを開発し、現在のクロノグラフの基礎を築いた。
                    </p>
                    <p>そして、1936年にイギリス空軍とコックピットクロックの供給契約を皮切りに航空業界で勢力を拡大。
                        <br> 1952年に航空用回転計算尺を搭載した「ナビタイマー」によって、航空時計ナンバーワンブランドの
                        <br> 地位を確立した。
                    </p>
                    <p>有名なモデルにクロノマット、スーパーオーシャン、トランスオーシャン、ナビタイマー、ウィンドライダー、スカイレーサーなど</p>
                </div>

                <div id="div_point06" class="div_point">
                <a class="modal_close" href="#"></a>
                    <h5>BREGUET（ブレゲ）とは</h5>
                    <p>BREGUET（ブレゲ）は1775年、パリにオープンした工房から始まった。</p>
                    <p>1780年にはブラビアン＝ルイ・ブレゲが自動巻き機構を開発。衝撃吸収装置、ブレゲヒゲゼンマイ、
                        <br> トゥールビヨン機構、スプリットセコンドクロノグラフなど、次々と画期的な機構を開発、ブレゲ針や
                        <br> ブレゲ数字、文字盤のギョ―シェ彫りもブレゲの発案によるものだ。
                    </p>
                    <p>天才と言われる彼の技術はマリー・アントワネット妃の注文により作られた超複雑時計No.160
                        <br> 「マリー・アントワネット」に集約されている。
                    </p>
                    <p>1823年にブレゲは死去するが、その技術は弟子たちに受け継がれ、経営がブラウン家、宝石商ショーメと
                        <br> 移るが、その間も数多くの複雑度系を世に送り出した。
                    </p>
                    <p>現在はスウォッチ・グループの傘下となり、安定した経営基盤を獲得し、自社ムーブメントの開発や
                        <br> シリコン素材の導入など、新境地を開き続けている。
                    </p>
                    <p>有名なモデルにタイプトゥエンティー、マリーン、クイーン・オブ・ネイプルズ、クラシック、トラディション、ヘリテージ、コンプリケーションなど</p>
                </div>

            </section>-->
    <div style="margin-top:20px; margin-bottom: 20px;"><a href="https://kaitorisatei.info/brandrevalue/blog/doburock"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/12/caf61dc6779ec6137c0ab58dfe3a550d.jpg" alt="どぶろっく"></a></div>
        <section id="cat-point">
            <h2 class="obi_tl">ブランド時計の高価買取ポイント</h2>
            <ul>
                <li>
                    <p class="pt_bigtl">POINT1</p>
                    <div class="pt_wrap">
                        <p class="pt_tl">付属品が揃っていれば高価買取が可能</p>
                        <p class="pt_tx">保証書(ギャランティ)、ワランティ等の付属品は購入されたお品物を証明する付属品である為、保証書等の付属品がそろっていると査定額UPに繋がります。</p>
                    </div>
                </li>
                <li>
                    <p class="pt_bigtl">POINT2</p>
                    <div class="pt_wrap">
                        <p class="pt_tl">新作や人気モデルだと査定金額UP</p>
                        <p class="pt_tx">新作や人気モデル、人気ブランドであれば通常相場よりさらに金額を上乗せしてお買取りが可能です。また流通量が少ない希少なモデルも高価買取させて頂きます。</p>
                    </div>
                </li>
                <li>
                    <p class="pt_bigtl">POINT3</p>
                    <div class="pt_wrap">
                        <p class="pt_tl">品物の状態がよいほど高価買取が可能</p>
                        <p class="pt_tx">汚れてしまっているのであれば少しでもふき取ってお持ち込み下さい。<br>
                        時計を購入された状態に近ければ近いほど査定金額がUPします。</p>
                    </div>
                </li>
            </ul>
            <p>腕時計の査定の際にぜひお持ちいただきたいのは、購入時に同封されていた保証書や明細書。 <br>
                そのモデルの種類、購入価格等がわかればよりスムーズに査定を行うことができます。購入時の箱や付属品のベルト等、付属品がそろっていれば、より査定金額が高くなるでしょう。 <br>
                もちろん、保証書や箱などの付属品がなくても時計そのものの価値は変わりませんのでご心配なく。ベルトがボロボロになっている場合なども、時計本体の状態によっては高い査定価格のご提示も可能です。 <br>
                手軽に利用できるLINE査定などもありますので、ぜひお気軽にご相談ください。 </p>
        </section>
        <h3 class="mid">ブランド時計 一覧</h3>
        <ul class="mid_link">
            <li> <a href="<?php echo home_url('/cat/watch/rolex'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/01.jpg" alt="ロレックス買取">
                <p class="mid_ttl">ロレックス</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/franckmuller'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/02.jpg" alt="フランクミュラー買取">
                <p class="mid_ttl">フランクミュラー</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/omega'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/03.jpg" alt="オメガ買取">
                <p class="mid_ttl">オメガ</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/patek-philippe'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/04.jpg" alt="パテックフィリップ買取">
                <p class="mid_ttl">パテックフィリップ</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/breitling'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/05.jpg" alt="ブライトリング買取">
                <p class="mid_ttl">ブライトリング</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/iwc'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/06.jpg" alt="アイダブルシー買取">
                <p class="mid_ttl">アイダブルシー</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/hublot'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/07.jpg" alt="ウブロ買取">
                <p class="mid_ttl">ウブロ</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/breguet'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/08.jpg" alt="ブレゲ買取">
                <p class="mid_ttl">ブレゲ</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/audemarspiguet'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/09.jpg" alt="オーデマピゲ買取">
                <p class="mid_ttl">オーデマピゲ</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/alange-soehne'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/11.jpg" alt="ランゲ＆ゾーネ">
                <p class="mid_ttl">ランゲ&ゾーネ</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/jaeger-lecoultre'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/12.jpg" alt="ジャガールクルト買取">
                <p class="mid_ttl">ジャガールクルト</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/gagamilano'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/13.jpg" alt="ガガミラノ買取">
                <p class="mid_ttl">ガガミラノ</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/rogerdubuis'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/14.jpg" alt="ロジェ・デュブイ買取">
                <p class="mid_ttl">ロジェ・デュブイ</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/richardmille'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/15.jpg" alt="リシャール・ミル買取">
                <p class="mid_ttl">リシャール・ミル</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/vacheron'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/16.jpg" alt="ヴァシュロンコンスタンタン買取">
                <p class="mid_ttl">ヴァシュロンコンスタンタン</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/jacob'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/17.jpg" alt="ジェイコブ買取">
                <p class="mid_ttl">ジェイコブ</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/tagheuer'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/18.jpg" alt="タグホイヤー買取">
                <p class="mid_ttl">タグホイヤー</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/rolex'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/19.jpg" alt="ハミルトン買取">
                <p class="mid_ttl">ハミルトン</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/panerai'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/20.jpg" alt="パネライ買取">
                <p class="mid_ttl">パネライ</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/blancpain'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/21.jpg" alt="ブランパン買取">
                <p class="mid_ttl">ブランパン</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/watch/seiko'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/22.jpg" alt="セイコー買取">
                <p class="mid_ttl">セイコー</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/gem/cartier/cartier-watch'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/23.jpg" alt="カルティエ買取">
                <p class="mid_ttl">カルティエ</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/gem/tiffany/tiffany-watch'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/24.jpg" alt="ティファニー買取">
                <p class="mid_ttl">ティファニー</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/gem/harrywinston/harrywinston-watch'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/25.jpg" alt="ハリーウィンストン買取">
                <p class="mid_ttl">ハリーウィンストン</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/gem/piaget/piaget-watch'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/26.jpg" alt="ピアジェ買取">
                <p class="mid_ttl">ピアジェ</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/gem/vancleefarpels/vancleefarpels-watch'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/27.jpg" alt="ヴァンクリーフ＆アーペル買取">
                <p class="mid_ttl">ヴァンクリーフ&アーペル</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/gem/bulgari/bulgari-watch'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/28.jpg" alt="ブルガリ買取">
                <p class="mid_ttl">ブルガリ</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/bag/hermes/hermes-watch'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/29.jpg" alt="エルメス買取">
                <p class="mid_ttl">エルメス</p>
                </a> </li>
            <li> <a href="<?php echo home_url('/cat/bag/chanel/chanel-watch'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/30.jpg" alt="シャネル買取">
                <p class="mid_ttl">シャネル</p>
                </a> </li>
            <li> <a href="<?php echo home_url('cat/bag/dior/dior-watch'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/31.jpg" alt="ディオール買取">
                <p class="mid_ttl">ディオール</p>
                </a> </li>
            <li> <a href="<?php echo home_url('cat/wallet/louisvuitton/louisvuitton-watch'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/32.jpg" alt="ルイヴィトン買取">
                <p class="mid_ttl">ルイヴィトン</p>
                </a> </li>
             <li> <a href="<?php echo home_url('cat/watch/tudor'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/33.jpg" alt="チュードル買取">
                <p class="mid_ttl">チュードル</p>
                </a> </li>
             <li> <a href="<?php echo home_url('cat/watch/ulysse-nardin'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/34.jpg" alt="ユリスナルダン買取">
                <p class="mid_ttl">ユリスナルダン</p>
                </a> </li>
             <li> <a href="<?php echo home_url('cat/watch/edox'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/35.jpg" alt="エドックス買取">
                <p class="mid_ttl">エドックス</p>
                </a> </li>
             <li> <a href="<?php echo home_url('cat/watch/alain-silberstein'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/36.jpg" alt="アランシルベスタイン買取">
                <p class="mid_ttl">アランシルベスタイン</p>
                </a> </li>
<li> <a href="<?php echo home_url('cat/watch/zenith'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/37.jpg" alt="ゼニス買取">
				 <p class="mid_ttl">ゼニス</p>
                </a> </li>
<li> <a href="<?php echo home_url('cat/watch/geraldgenta'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/38.jpg" alt="ジェラルドジェンタ買取">
                 <p class="mid_ttl">ジェラルドジェンタ</p>
                </a> </li>
<li> <a href="<?php echo home_url('cat/watch/graham'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/39.jpg" alt="グラハム買取">
                 <p class="mid_ttl">グラハム</p>
                </a> </li>
<li> <a href="<?php echo home_url('cat/watch/jaquet-droz'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/40.jpg" alt="ジャケドロー買取">
                 <p class="mid_ttl">ジャケドロー</p>
                </a> </li>
<li> <a href="<?php echo home_url('cat/watch/bovet'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/41.jpg" alt="ボヴェ買取">
                 <p class="mid_ttl">ボヴェ</p>
                </a> </li>
<li> <a href="<?php echo home_url('cat/watch/girard-perregaux'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/42.jpg" alt="ジラールペルゴ買取">
<p class="mid_ttl">ジラールペルゴ</p>
                </a> </li>
<li> <a href="<?php echo home_url('cat/watch/frederique'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/43.jpg" alt="フレデリック・コンスタント買取">
<p class="mid_ttl">フレデリック・コンスタント</p>
                </a> </li>
<li> <a href="<?php echo home_url('cat/watch/baume-mercier'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/44.jpg" alt="ボーム＆メルシエ買取">
<p class="mid_ttl">ボーム＆メルシエ</p>
                </a> </li>
<li> <a href="<?php echo home_url('cat/watch/bellandross'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/45.jpg" alt="ベル＆ロス買取">
<p class="mid_ttl">ベル＆ロス</p>
                </a> </li>
<li> <a href="<?php echo home_url('cat/watch/chronoswiss'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/46.jpg" alt="クロノスイス買取">
<p class="mid_ttl">クロノスイス</p>
                </a> </li>
<li> <a href="<?php echo home_url('cat/watch/mauricelacroix'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/47.jpg" alt="モーリスラクロア買取">
<p class="mid_ttl">モーリスラクロア</p>
                </a> </li>
<li> <a href="<?php echo home_url('cat/watch/rado'); ?>"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/watch/48.jpg" alt="ラドー買取">
<p class="mid_ttl">ラドー</p>
                </a> </li>

        </ul>
        <section id="lp-cat-jisseki">
            <h2 class="obi_tl">ブランド時計の買取実績</h2>
            <ul id="box-jisseki" class="list-unstyled clearfix">
                <?php
            foreach($resultLists as $list):
            // :: で分割
            $listItem = explode('::', $list);

          ?>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/watch/lp/<?php echo $listItem[0]; ?>" alt=""></p>
                        <p class="itemName"> <?php echo $listItem[1]; ?> <br>
                            <?php echo $listItem[2]; ?> </p>
                        <hr>
                        <p> <span class="red">A社</span>： <?php echo $listItem[3]; ?>円 <br>
                            <span class="blue">B社</span>： <?php echo $listItem[4]; ?>円 </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price"> <?php echo $listItem[5]; ?><span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span> <?php echo $listItem[6]; ?>円</p>
                    </div>
                </li>
                <?php endforeach; ?>
            </ul>
            <!--<h3 class="new_list_ttl">ブランドリスト</h3>
            <ul class="list-unstyled new_list">
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/rolex'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch01.jpg" alt="ロレックス"></dd>
                        <dt><span>ROLEX</span>ロレックス</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/breitling'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch05.jpg" alt="ブライトリング"></dd>
                        <dt><span>Breitling</span>ブライトリング</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/franckmuller'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch02.jpg" alt="フランクミューラー"></dd>
                        <dt><span>Franckmuller</span>フランクミューラー</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/iwc'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch06.jpg" alt="アイダブルシー"></dd>
                        <dt><span>IWC</span>アイダブルシー</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/hublot'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch07.jpg" alt="ウブロ"></dd>
                        <dt><span>HUBLOT</span>ウブロ</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/omega'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch03.jpg" alt="オメガ"></dd>
                        <dt><span>OMEGA</span>オメガ</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/breguet'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch08.jpg" alt="ブレゲ"></dd>
                        <dt><span>Breguet</span>ブレゲ</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/gem/bulgari'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch27.jpg" alt="ブルガリ"></dd>
                        <dt><span>BVLGALI</span>ブルガリ</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/gem/cartier'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch28.jpg" alt="カルティエ"></dd>
                        <dt><span>Cartier</span>カルティエ</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/audemarspiguet'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>//img/link_cat/watch/watch09.jpg" alt="オーデマピゲ"></dd>
                        <dt><span>Audemarspiguet</span>オーデマピゲ</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/alange-soehne'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch10.jpg" alt="ランゲ&ゾーネ"></dd>
                        <dt><span>A.lange-soehne</span>ランゲ&ゾーネ</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/patek-philippe'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch04.jpg" alt="パテックフィリップ"></dd>
                        <dt><span>Patek Philippe</span>パテックフィリップ</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/jaeger-lecoultre'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch11.jpg" alt="ジャガールクルト"></dd>
                        <dt><span>Jaeger Lecoultre</span>ジャガールクルト</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/gagamilano'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch12.jpg" alt="ガガミラノ"></dd>
                        <dt><span>GaGa MILANO</span>ガガミラノ</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/rogerdubuis'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch13.jpg" alt="ロジェ・デュブイ"></dd>
                        <dt><span>RogerDubuis</span>ロジェ・デュブイ</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/richardmille'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch14.jpg" alt="リシャール・ミル"></dd>
                        <dt><span>RichardMille</span>リシャール・ミル</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/vacheron'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch15.jpg" alt="ヴァシュロンコンスタンタン"></dd>
                        <dt style="font-size:9px;"><span>Vacheron Constatin</span>ヴァシュロンコンスタンタン</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/jacob'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch16.jpg" alt="ジェイコブ"></dd>
                        <dt><span>JACOB CO</span>ジェイコブ</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/tagheuer'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch17.jpg" alt="タグホイヤー"></dd>
                        <dt><span>Tagheuer</span>タグホイヤー</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/hamilton'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch18.jpg" alt="ハミルトン"></dd>
                        <dt><span>Hamilton</span>ハミルトン</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/panerai'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch19.jpg" alt="パネライ"></dd>
                        <dt><span>Panerai</span>パネライ</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/blancpain'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch20.jpg" alt="ブランパン"></dd>
                        <dt><span>Blancpain</span>ブランパン</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/watch/seiko'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch21.jpg" alt="SEIKO"></dd>
                        <dt><span>SEIKO</span>セイコー</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/gem/piaget'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch22.jpg" alt="ピアジェ"></dd>
                        <dt><span>PIAGET</span>ピアジェ</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/wallet/louisvuitton'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch23.jpg" alt="ルイヴィトン"></dd>
                        <dt><span>LOUIS VUITTON</span>ルイヴィトン</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/gem/tiffany'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch24.jpg" alt="ピアジェ"></dd>
                        <dt><span>TIFFANY</span>ティファニー</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/bag/gucci'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch25.jpg" alt="グッチ"></dd>
                        <dt><span>GUCCI</span>グッチ</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/gem/harrywinston'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch26.jpg" alt="グッチ"></dd>
                        <dt><span>HARRY WINSTON</span>ハリーウィンストン</dt>
                        </a>
                    </dl>
                </li>

                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/bag/celine'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch29.jpg" alt="セリーヌ"></dd>
                        <dt><span>CELINE</span>セリーヌ</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/gem/vancleefarpels'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch30.jpg" alt="ヴァンクリーフ&アーペル"></dd>
                        <dt><span>Vanleefarpels</span>ヴァンクリーフ&アーペル</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/bag/ferragamo'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch31.jpg" alt="フェラガモ"></dd>
                        <dt><span>Ferragamo</span>フェラガモ</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/bag/bottegaveneta'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch32.jpg" alt="ボッテガヴェネタ"></dd>
                        <dt><span>Bottegaveneta</span>ボッテガヴェネタ</dt>
                        </a>
                    </dl>
                </li>
                <li>
                    <dl>
                        <a href="<?php echo home_url('/cat/bag/hermes'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch33.jpg" alt="エルメス"></dd>
                        <dt><span>Hermes</span>エルメス</dt>
                        </a>
                    </dl>
                </li>
            </ul>-->
            <p id="catchcopy">BRANDREVALUE(ブランドリバリュー)では独自の中古時計再販ルートを構築しているため、幅広いブランドの腕時計を高く査定し、買い取ることが可能となっております。 <br>
                また、ブランド腕時計の買取において豊富な経験を積んだスタ ッフが在籍しており、故障した時計、電池切れで動かない時計、傷ついた時計等も価値を見落とすことなく最大限高く査定。 <br>
                <br>
                「他店で査定を断られたような時計が、BRANDREVALUE(ブランドリバリュー)では <br>
                驚くほどの高額買取の対象となった」 <br>
                ――そのようなことも全く珍しくはありません。 </p>
            <p class="jyoutai_tl">こんな状態でも買取致します!</p>
            <div id="konna">
                <p class="example1">■風防割れ</p>
                <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/watch_jyoutai01.jpg"></p>
                <p class="example2">■ベルトのよれ、劣化</p>
                <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/watch_jyoutai02.jpg"></p>
                <p class="example3">■文字盤焼け</p>
                <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/watch_jyoutai03.jpg"></p>
                <p class="text">その他：箱、ギャランティー、付属品無し、電池切れや故障による不動品でもお買取りいたします。</p>
            </div>
        </section>
        <section id="about_kaitori" class="clearfix">
            <?php
        // 買取について
        get_template_part('_widerange');
                get_template_part('_widerange2');
      ?>
        </section>
        <?php
        // 買取基準
        get_template_part('_criterion');

        // NGアイテム
        get_template_part('_no_criterion');

                // カテゴリーリンク
        get_template_part('_category');
            ?>
        <section id="kaitori_genre">
            <h3 class="obi_tl">その他買取可能なジャンル</h3>
            <p>【買取ジャンル】バッグ/ウエストポーチ/セカンドバック/トートバッグ/ビジネスバッグ/ボストンバッグ/クラッチバッグ/トランクケース/ショルダーバッグ/ポーチ/財布/カードケース/パスケース/キーケース/手帳/腕時計/ミュール/サンダル/ビジネスシューズ/パンプス/ブーツ/ペアリング/リング/ネックレス/ペンダント/ピアス/イアリング/ブローチ/ブレスレット/ライター/手袋/傘/ベルト/ペン/リストバンド/アンクレット/アクセサリー/サングラス/帽子/マフラー/ハンカチ/ネクタイ/ストール/スカーフ/バングル/カットソー/アンサンブル/ジャケット/コート/ブルゾン/ワンピース/ニット/シャツメンズ/毛皮/Tシャツ/キャミソール/タンクトップ/パーカー/ベスト/ポロシャツ/ジーンズ/スカート/スーツなど</p>
        </section>
        <?php
        // 買取方法
        get_template_part('_purchase');
      ?>
        <section id="user_comment">
            <h2>ブランド時計買取をご利用いただいたお客様の声</h2>
            <p class="user_voice_text1">ちょうど家の整理をしていたところ、家のポストにチラシが入っていたので、ブランドリバリューに電話してみました。今まで買取店を利用したことがなく、不安でしたがとても丁寧な電話の対応とブランドリバリューの豊富な取り扱い品目を魅力に感じ、出張買取を依頼することにしました。 絶対に売れないだろうと思った、動かない時計や古くて痛んだバッグ、壊れてしまった貴金属のアクセサリーなども高額で買い取っていただいて、とても満足しています。古紙幣や絵画、食器なども買い取っていただけるとのことでしたので、また家を整理するときにまとめて見てもらおうと思います。 </p>
        </section>
        <section id="user_voice">
            <h3>鑑定士からのコメント</h3>
            <p class="user_voice_text2">家の整理をしているが、何を買い取ってもらえるか分からないから一回見に来て欲しいとのことでご連絡いただきました。 買取店が初めてで不安だったが、丁寧な対応に非常に安心しましたと笑顔でおっしゃって頂いたことを嬉しく思います。 物置にずっとしまっていた時計や、古いカバン、壊れてしまったアクセサリーなどもしっかりと価値を見極めて高額で提示させて頂きましたところ、お客様もこんなに高く買取してもらえるのかと驚いておりました。 これからも家の不用品を整理するときや物の価値判断ができないときは、すぐにお電話しますとおっしゃって頂きました。 </p>
        </section>
    </div>
    <!-- lp_main -->
</div>
<!-- #primary -->

<?php

get_footer();
