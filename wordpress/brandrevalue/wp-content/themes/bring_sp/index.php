<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BRING
 */

// リダイレクト設定
$url = $_SERVER["REQUEST_URI"];
$url = str_replace(home_url(), "", $url);
//var_dump($url);

$ua = $_SERVER['HTTP_USER_AGENT'];
//var_dump($ua);

/*if(is_page() || is_front_page()) {
  // 固定ページのみリダイレクト
if(
  (strpos($ua, 'Android') !== false) &&
  (strpos($ua, 'Mobile') !== false) ||
  (strpos($ua, 'iPhone') !== false) ||
  (strpos($ua, 'Windows Phone') !== false)) {
    // スマートフォンからアクセスされた場合
    header("Location: /sp".$url);
    exit();

} elseif (
  (strpos($ua, 'Android') !== false) ||
  (strpos($ua, 'iPad') !== false)) {
    // タブレットからアクセスされた場合
    header("Location: /sp".$url);
    exit();

} elseif (
  (strpos($ua, 'DoCoMo') !== false) ||
  (strpos($ua, 'KDDI') !== false) ||
  (strpos($ua, 'SoftBank') !== false) ||
  (strpos($ua, 'Vodafone') !== false) ||
  (strpos($ua, 'J-PHONE') !== false)) {
    // 携帯からアクセスされた場合
    header("Location: /sp".$url);
    exit();

} else {
    // その他（PC）からアクセスされた場合はなにもしない
}
}*/

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
<?php get_template_part('template-parts/gtm', 'head') ?>
</head>

<body <?php body_class(); ?>>
<?php get_template_part('template-parts/gtm', 'body') ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'bring' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="container ">
        <header>
            <h1 class="top_logo "><a href="#"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/top_logo.png " alt="高級ブランド買取専門サイト BRANDREVALUE "></a></h1>
            <div class="ico_bx ">
                <div class="tel_ico ">
                    <a href="tel:0362645412"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/tel_ico.png " alt="電話連絡はこちら "></a>
                </div>
                <div class="button-toggle "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/menu_ico.png " alt="メニュー "></div>
            </div>
        </header>
        <nav class="menu">
            <ul>
                <li><a href="service/index.html">サービスについて</a></li>
                <li><a href="about-purchase/index.html">買取方法</a></li>
                <li><a href="brand/index.html">取り扱いブランド</a></li>
                <li><a href="cat/index.html">取り扱いカテゴリ</a></li>
                <li><a href="introduction/index.html">実績紹介</a></li>
                <li><a href="voice/index.html">お客様の声</a></li>
                <li><a href="faq/index.html">よくある質問</a></li>
                <li><a href="line/index.html">LINE査定</a></li>
                <li><a href="company/index.html">会社概要</a></li>
            </ul>
        </nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<?php if(is_front_page() && is_home()): ?>
    <section id="mainVisual" class="front-page-mv">
      <div id="carousel_top01" class="carousel slide container" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carousel_top01" data-slide-to="0" class="active"></li>
          <li data-target="#carousel_top01" data-slide-to="1"></li>
        </ol>

        <div class="carousel-inner">
          <div class="item active">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/slider/slide01.png" alt="">
          </div>
          <div class="item">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/slider/slide02.png" alt="">
          </div>
        </div>
       <!-- <a class="left carousel-control" href="#carousel_01" role="button" date-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel_01" role="button" date-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>-->
      </div>
    </section>
  <?php else: ?>
    <!-- パンくず -->
    <div class="breadcrumbs">
      <?php if(function_exists('bcn_display')) {
        bcn_display();
      }?>
    </div>
  <?php endif; ?>
