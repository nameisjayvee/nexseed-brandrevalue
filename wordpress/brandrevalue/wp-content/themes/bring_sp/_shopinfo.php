<div class="str_info ">
    <h2>
        <p class="info_tl "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/str_tl.png " alt="ブランドリバリューの店舗案内 "></p>
    </h2>

    <p class="str_img "><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/str_img_ginza.jpg " alt="ブランドリバリュー銀座店舗案内 "></p>
    <p class="str_name ">銀座店</p>
    <dl class="info ">
        <dt>〒104-0061  東京都中央区銀座5-8-3 四谷学院ビル5階</dt>
        <dt>TEL：<a href="tel:0120-970-060">0120-970-060</a></dt>
    </dl>
    <p class="str_img"><a href="<?php echo home_url('ginza'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/more_btn.png" alt="詳しくはこちら"></a></p>


    <p class="str_img" style="margin-top:20px;"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/str_img_shibuya.jpg " alt="ブランドリバリュー渋谷店舗案内 "></p>
    <p class="str_name ">渋谷店</p>
    <dl class="info ">
        <dt>〒150-0041  東京都渋谷区神南1-12-16 和光ビル4階</dt>
        <dt>TEL：<a href="tel:0120-970-060">0120-970-060</a></dt>
    </dl>
    <p class="str_img"><a href="<?php echo home_url('shibuya'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/more_btn.png" alt="詳しくはこちら"></a></p>


    <p class="str_img" style="margin-top:20px;"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/str_img_shinjuku.jpg " alt="ブランドリバリュー新宿店舗案内 "></p>
    <p class="str_name ">新宿店</p>
    <dl class="info ">
        <dt>〒160-0022  東京都新宿区新宿3丁目31-1大伸第２ビル3階</dt>
        <dt>TEL：<a href="tel:0120-970-060">0120-970-060</a></dt>
    </dl>
    <p class="str_img"><a href="<?php echo home_url('shinjuku'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/more_btn.png" alt="詳しくはこちら"></a></p>

    <p class="str_img" style="margin-top:20px;"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/str_img_shinsaibashi.jpg " alt="ブランドリバリュー心斎橋店舗案内 "></p>
    <p class="str_name ">心斎橋店</p>
    <dl class="info ">
        <dt>〒542-0081 大阪府大阪市中央区南船場4-4-8 クリエイティブ心斎橋8階</dt>
        <dt>TEL：<a href="tel:0120-970-060">0120-970-060</a></dt>
    </dl>
    <p class="str_img"><a href="<?php echo home_url('shinsaibashi'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/more_btn.png" alt="詳しくはこちら"></a></p>


</div>
