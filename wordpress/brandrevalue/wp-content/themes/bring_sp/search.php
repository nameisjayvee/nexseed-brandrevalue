<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package BRING
 */

get_header(); ?>
<style>
#lp_mainVisual{
	display:none;
}

	
</style>
<section id="primary" class="content-area" style="width:95%; margin:20px 2.5%">
	<main id="main" class="site-main" role="main">
	
		<h3 class="obi_tl">『<?php echo get_search_query(); ?>』での検索結果一覧</h3>
		<?php if(have_posts()): ?>
		<?php while(have_posts()): the_post(); ?>
		<h4 class="frm_ttl"><a href="<?php the_permalink(); ?>">
			<?php the_title(); ?>
			</a></h4>
		
		<?php endwhile; ?>
		<?php else : ?>
		<p>検索条件にヒットした記事がありませんでした。</p>
		<?php endif; ?>
	</main>
	<!-- #main --> 
</section>
<!-- #primary -->

<?php
  // お問い合わせ
  get_template_part('_action');
  
  // 3つのポイント
  get_template_part('_purchase');
  
  // お問い合わせ
  get_template_part('_action2');
  
  // 店舗
  get_template_part('_shopinfo');
  
  // フッター
  get_footer();