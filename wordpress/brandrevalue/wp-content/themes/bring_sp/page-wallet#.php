<?php
  
get_header(); ?>

<div class="mv_area ">
            <img src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_kv.png" alt="BRAMD REVALUE ">
        </div>
        <div class="cat_cnt">
            <h2 class="cat_tl">長く使えるブランド財布なら、古くても高額査定・買取が可能</h2>
            <p class="cat_tx">その年の流行によって大きくデザインが変わる洋服と違い、財布はトレンドが変わっても大きくデザインが変化しないもの。また、時代が変わっても「必ず誰もが一つは所有する普遍的な道具」という側面があることから、中古市場でも安定した相場で取引されています。発売後一定の期間が過ぎていても、保存状態がよければかなりの高額査定が期待できます。もし「自宅に長らく使っていないブランド財布がある」ということであれば、ぜひBRAND REVALUEへお持ちください。使い古して汚れがついたものも、程度によっては査定額にあまり影響がない場合があります。とはいえ、金属製の時計などと違い、時間が経過するほど素材が少しずつ劣化することは避けられません。お早めに査定するのが、高く売る一番のコツといえるでしょう。</p>
        </div>

        <div id="cat-jisseki">
            <h3 class="cat-jisseki_tl">買取実績</h3>
            <p class="cat_jisseki_tx">BRAND REVALUEはまだ新しい買取ショップですが、財布買取に精通したスタッフがそろっており再販ルートも確立しております。<br>
            お客様の大切な財布の価値を見逃すことなく高く査定させていただいていることから、オープン後財布の買取実績も急速に伸びております。また、BRAND REVALUEは銀座の中心地という利便性の高い地域に店舗を構えていますが、路面店と比べて格段に家賃の安い空中階に店舗があるため、他店と比べて圧倒的に経費を安く抑えることができています。<br>
            その他、広告費、人件費等も可能な限り削減しており、その分査定額を高めに設定してお客様に利益を還元しております。</p>
            <ul id="box-jisseki" class="list-unstyled clearfix">
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/wallet001.jpg" alt=""></p>
                        <p class="itemName">CHANEL
                            <br>マトラッセ 長財布</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：60,000円
                            <br>
                            <span class="blue">B社</span>：80,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">100,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>40,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/wallet002.jpg" alt=""></p>
                        <p class="itemName">LOUIS VUITTON
                            <br>モノグラムボルトフォイユサラ</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：30,000円
                            <br>
                            <span class="blue">B社</span>：35,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">42,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>12,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/wallet003.jpg" alt=""></p>
                        <p class="itemName">HERMES
                            <br>ドゴン</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：79,000円
                            <br>
                            <span class="blue">B社</span>：81,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">93,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>14,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/wallet004.jpg" alt=""></p>
                        <p class="itemName">PRADA
                            <br>ラゲージマイクロショッパーバッグ</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：19,000円
                            <br>
                            <span class="blue">B社</span>：22,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">27,500<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>8,500円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/wallet005.jpg" alt=""></p>
                        <p class="itemName">BOTTEGA VENETA
                            <br>長財布</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：21,000円
                            <br>
                            <span class="blue">B社</span>：26,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">31,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>10,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/wallet006.jpg" alt=""></p>
                        <p class="itemName">BOTTEGA VENETA
                            <br>長財布ラウンドファスナー</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：68,000円
                            <br>
                            <span class="blue">B社</span>：72,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">81,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>13,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/wallet007.jpg" alt=""></p>
                        <p class="itemName">LOUIS VUITTON
                            <br>ダミエ ポルトフォイユインターナショナル</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：42,000円
                            <br>
                            <span class="blue">B社</span>：47,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">53,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>13,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/wallet008.jpg" alt=""></p>
                        <p class="itemName">HERMES
                            <br>ベアンスフレ</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：180,000円
                            <br>
                            <span class="blue">B社</span>：200,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">230,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>50,000円</p>
                    </div>
                </li>
            </ul>
            <div class="kaitori_point">
            <h3>【財布の高価買取ポイント】</h3>
財布の査定額は、ちょっとした工夫で高くなることがあるのをご存知でしょうか。<br>
それは「ご自宅で簡単にメンテナンスをしてから査定に出す」というもの。モノ自体がよくても、埃がたまっているなど汚れが目立つ財布は査定額が下がってしまう場合があるのです。ご自宅でできる範囲で表面の汚れをふき取り、カード入れや小銭入れ等の小さなポケットにたまった埃をとるだけでも評価額が上がるので、ぜひお試しください。<br>
その他、購入時の箱や袋、保証書等の付属品が一式そろっていると、査定金額が上がります。
            </div>
        </div>

        <div class="point_list">
            <div class="coint_bnr">
                <img src="<?php echo get_s3_template_directory_uri() ?>/images/coin_bnr01.png" alt="他の店鋪より1円でも安ければご連絡下さい。">
                <ul class="other_price">
                    <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/coin_bnr02.png" alt="他の店鋪より1円でも安ければご連絡下さい。"></li>
                    <li>ブリングは徹底した高価買取を行っております。お客様が愛着を持って身につけてきた服は、大切に買い取りさせて頂きます。 万が一、他店よりも1円でも安い価格であればおっしゃってください。なるべくお客様のご要望に答えられるよう、査定させて頂きます。<br>
                まずは、こちらから買い取り実績について、覗いてみてください。きっと、お客様の古着に相応しい、ご期待に添える価格での買い取りを行っているはずです。</li>
                </ul>
            </div>
            <h3 class="obi_tl">高価買取のポイント</h3>
            <img src="<?php echo get_s3_template_directory_uri() ?>/images/kaitori_point.png" alt="高価買い取りのポイント" class="point_img">
            <h3 class="obi_tl">ブランドリスト</h3>
            <ul class="cat_list">
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_1.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_2.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_3.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_4.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_5.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_6.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_7.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_8.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_9.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_10.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_11.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_12.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_13.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_14.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_15.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_16.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_17.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/wallet_18.jpg" alt=""></li>


            </ul>
        </div>

<?php
  // お問い合わせ
  get_template_part('_action');
  
  // 3つのポイント
  get_template_part('_purchase');
  
  // お問い合わせ
  get_template_part('_action2');
  
  // 店舗
  get_template_part('_shopinfo');
  
  // フッター
  get_footer();