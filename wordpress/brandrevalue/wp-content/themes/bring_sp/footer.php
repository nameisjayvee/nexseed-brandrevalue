<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @see https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>
    </div>
    <!-- #content -->
    <footer>
        <div class="list_wrap ">
            <h3 class="foot_logo "><a href="<?php echo home_url(''); ?>"><img src="<?php echo get_s3_template_directory_uri(); ?>/images/foot_logo.png " alt="高額ブランド買取専門サイト BRAND REVALUE " ></a></h3>
            <div class="foot_list_l ">
                <h4><a href="<?php echo home_url(''); ?>">トップ</a></h4>
                <p><a href="<?php echo home_url('service'); ?>">サービスについて</a></p>
                <p><a href="<?php echo home_url('brand'); ?>">取扱ブランド</a></p>
                <p><a href="<?php echo home_url('cat'); ?>">取扱カテゴリ</a></p>
                <p><a href="<?php echo home_url('introduction'); ?>">実績紹介</a></p>
                <p><a href="<?php echo home_url('privacy'); ?>">ご利用規約一覧</a></p>
                <p><a href="<?php echo home_url('company'); ?>">会社概要</a></p>
                <h4 class="mt_top20">買取対象品</h4>
                <p><a href="<?php echo home_url('cat/gold'); ?>">金・プラチナ・貴金属</a></p>
                <p><a href="<?php echo home_url('cat/gem'); ?>">宝石・ジュエリー</a></p>
                <p><a href="<?php echo home_url('cat/watch'); ?>">高級腕時計</a></p>
                <p><a href="<?php echo home_url('cat/bag'); ?>">ブランドバッグ・鞄</a></p>
                <p><a href="<?php echo home_url('cat/outfit'); ?>">洋服・毛皮</a></p>
                <p><a href="<?php echo home_url('cat/wallet'); ?>">ブランド財布</a></p>
                <p><a href="<?php echo home_url('cat/shoes'); ?>">ブランド靴</a></p>
                <p><a href="<?php echo home_url('cat/diamond'); ?>">ダイヤモンド</a></p>
                <p><a href="<?php echo home_url('cat/kimono'); ?>">着物</a></p>
            </div>
            <div class="foot_list_r ">
                <div class="footer_tel">
                    <p>買取査定お申込み</p>
                    <p><i class="fa fa-phone-square"></i><a href="tel:0120-970-060">0120-970-060</a></p>
                </div>
                <p><a href="<?php echo home_url('purchase/visit-form'); ?>">来店予約お申込み</a></p>
                <p><a href="<?php echo home_url('purchase3'); ?>">宅配買取お申込み</a></p>
                <p><a href="<?php echo home_url('purchase/syutchou'); ?>">出張買取お申込み</a></p>
                <p><a href="<?php echo home_url('purchase1'); ?>">メール査定お申込み</a></p>
                <p><a href="<?php echo home_url('line'); ?>">LINE査定</a></p>
                <p><a href="<?php echo home_url('purchase1'); ?>">WEB査定お申し込み</a></p>
                <p><a href="<?php echo home_url('contact'); ?>">お問い合わせ</a></p>

                <p><a href="<?php echo home_url('customer'); ?>"><img src="<?php echo get_s3_template_directory_uri(); ?>/images/sub_btn.png" alt="今すぐ査定" ></a></p>
                <h4 class="mt_top20">姉妹店</h4>
                <p><a href="https://kaitorisatei.info/sp/">ブランド古着買取のBRING</a></p>
            </div>
        </div>
        <p class="copy "><small>Copyrights © 2015 ブランド古着買取のBRAND REVALUE all rights reserved.</small></p>
        <?php if (!is_page(array('purchase3', 'visit-form', 'syutchou', 'purchase1', 'contact'))) : ?>
        <div class="btm_nav">
            <ul>
                <li><a href="tel:0120-970-060"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/img/ft_ico/ftico01.png" alt="電話で問い合わせる"></a></li>

                <?php if ( is_iphone() ) : ?>
                    <li><a href="#" id="open"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/img/ft_ico/ftico02.png" alt="LINE"></a></li>
                <?php else: ?>
                    <li><a href="https://line.me/ti/p/%40otv4506b"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/img/ft_ico/ftico02.png" alt="LINE"></a></li>
                <?php endif; ?>
                <li><a href="<?php echo home_url('purchase1'); ?>"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/img/ft_ico/ftico03.png" alt="お問い合わせ"></a></li>
                <li id="ftbtn01"><img data-src="<?php echo get_s3_template_directory_uri(); ?>/img/ft_ico/ftico04.png" alt="お問い合わせ">
                </li>
                <div id="ftbtn01wrap">
                    <ul class="custom_cv_box02 ftcv">
                        <li>
                            <a href="<?php echo home_url('purchase/tentou'); ?>">
                                <p>駅前だから便利!</p>
                                <p>店頭買取</p>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo home_url('purchase/takuhai'); ?>">
                                <p>忙しい方にオススメ!</p>
                                <p>宅配買取</p>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo home_url('purchase/syutchou'); ?>">
                                <p>点数が多い方にぴったり!</p>
                                <p>出張買取</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </ul>
        </div>
        <?php endif; ?>
    </footer>
    </div>
    <!-- Page -->
    <div id="pageTop" class="pageTop">
        <a href="#"><img src="<?php echo get_s3_template_directory_uri(); ?>/images/top_btn.png " alt="トップへ戻る "></a>
    </div>

    <div id="modal">
    <p>BRAND REVALUE(ブランドリバリュー)</p><br>
    <p>LINEアプリ不具合のお知らせ</p><br>
    <p>一部ブラウザでLINEアプリが起動できない不具合が発生しております。</p>
    <p>LINEアプリが起動できない場合には大変お手数ですがLINEアプリから友達検索にて「@brandrevalue」の追加をお願い致します。</p>
    <p>対応方法▼</p>
    <p>@brandrevalue　を<button class="copy-target" data-clipboard-text="@brandrevalue"><i class="far fa-copy"></i>コピー</button></p>
    <p>↓</p>
    <p>LINEアプリを起動</p>
    <p>↓</p>
    <p>ホーム</p>
    <p>↓</p>
    <p>右上の<img class="line_add_icon" src="<?php echo get_template_directory_uri(); ?>/img/line_add_icon.png ">アイコンをクリック</p>
    <p>↓</p>
    <p>検索窓に「@brandrevalue」を貼り付け</p>
    <p>↓</p>
    <p>友だち追加完了後、LINE査定をご利用頂けます。</p>
    <ul class="modal-button">
        <a id="close" class="btn-flat-border"><li>戻る</li></a>
        <a href="https://line.me/ti/p/%40otv4506b" class="btn-flat-border"><li>LINE査定へ進む</li></a>
    </ul>
    <br>
    <p>株式会社STAY GOLD</p>
</div>
<div id="overlay"></div>

    <?php wp_footer(); ?>
    <script type="text/javascript">
        jQuery(function($) {
            $("input[name='request_date']").datepicker({
                "yearSuffix": "\u5e74",
                "dateFormat": "yy-mm-dd",
                "minDate": "0y+3d",
                "dayNames": ["\u65e5\u66dc\u65e5", "\u6708\u66dc\u65e5", "\u706b\u66dc\u65e5", "\u6c34\u66dc\u65e5", "\u6728\u66dc\u65e5", "\u91d1\u66dc\u65e5", "\u571f\u66dc\u65e5"],
                "dayNamesMin": ["\u65e5", "\u6708", "\u706b", "\u6c34", "\u6728", "\u91d1", "\u571f"],
                "dayNamesShort": ["\u65e5\u66dc", "\u6708\u66dc", "\u706b\u66dc", "\u6c34\u66dc", "\u6728\u66dc", "\u91d1\u66dc", "\u571f\u66dc"],
                "monthNames": ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
                "monthNamesShort": ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
                "showMonthAfterYear": "true",
                "changeYear": "true",
                "changeMonth": "true"
            });
            $("input[name='request_date1']").attr('readonly', true);
        });

    </script>

    <script type="text/javascript">
        jQuery(function($) {
            $("input[name='request_date1']").datepicker({
                "yearSuffix": "\u5e74",
                        "dateFormat": "yy-mm-dd",
                "minDate": "0y",
                "dayNames": ["\u65e5\u66dc\u65e5", "\u6708\u66dc\u65e5", "\u706b\u66dc\u65e5", "\u6c34\u66dc\u65e5", "\u6728\u66dc\u65e5", "\u91d1\u66dc\u65e5", "\u571f\u66dc\u65e5"],
                "dayNamesMin": ["\u65e5", "\u6708", "\u706b", "\u6c34", "\u6728", "\u91d1", "\u571f"],
                "dayNamesShort": ["\u65e5\u66dc", "\u6708\u66dc", "\u706b\u66dc", "\u6c34\u66dc", "\u6728\u66dc", "\u91d1\u66dc", "\u571f\u66dc"],
                "monthNames": ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
                "monthNamesShort": ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
                "showMonthAfterYear": "true",
                "changeYear": "true",
                "changeMonth": "true"
            });
            $("input[name='request_date1']").attr('readonly', true);
        });

    </script>
    <script type="text/javascript">
        jQuery(function($) {
            $("input[name='request_date2']").datepicker({
                "yearSuffix": "\u5e74",
                        "dateFormat": "yy-mm-dd",
                "minDate": "0y",
                "dayNames": ["\u65e5\u66dc\u65e5", "\u6708\u66dc\u65e5", "\u706b\u66dc\u65e5", "\u6c34\u66dc\u65e5", "\u6728\u66dc\u65e5", "\u91d1\u66dc\u65e5", "\u571f\u66dc\u65e5"],
                "dayNamesMin": ["\u65e5", "\u6708", "\u706b", "\u6c34", "\u6728", "\u91d1", "\u571f"],
                "dayNamesShort": ["\u65e5\u66dc", "\u6708\u66dc", "\u706b\u66dc", "\u6c34\u66dc", "\u6728\u66dc", "\u91d1\u66dc", "\u571f\u66dc"],
                "monthNames": ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
                "monthNamesShort": ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
                "showMonthAfterYear": "true",
                "changeYear": "true",
                "changeMonth": "true"
            });
            $("input[name='request_date2']").attr('readonly', true);
        });

    </script>
    <script type="text/javascript">
        jQuery(function($) {
            $("input[name='request_date3']").datepicker({
                "yearSuffix": "\u5e74",
                        "dateFormat": "yy-mm-dd",
                "minDate": "0y",
                "dayNames": ["\u65e5\u66dc\u65e5", "\u6708\u66dc\u65e5", "\u706b\u66dc\u65e5", "\u6c34\u66dc\u65e5", "\u6728\u66dc\u65e5", "\u91d1\u66dc\u65e5", "\u571f\u66dc\u65e5"],
                "dayNamesMin": ["\u65e5", "\u6708", "\u706b", "\u6c34", "\u6728", "\u91d1", "\u571f"],
                "dayNamesShort": ["\u65e5\u66dc", "\u6708\u66dc", "\u706b\u66dc", "\u6c34\u66dc", "\u6728\u66dc", "\u91d1\u66dc", "\u571f\u66dc"],
                "monthNames": ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
                "monthNamesShort": ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
                "showMonthAfterYear": "true",
                "changeYear": "true",
                "changeMonth": "true"
            });
            $("input[name='request_date3']").attr('readonly', true);
        });

    </script>
    <!-- // jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/scll.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/flickity.pkgd.min.js"></script>
    <script>
        $(function() {
            $(".menu ").css("display ", "none ");
            $(".button-toggle ").on("click ", function() {
                $(".menu ").slideToggle();
            });
        });
        $('#main-gallery').flickity({
            // options
            autoPlay: 5000,
            cellAlign: 'left',
            contain: true
        });
        // ブランドリスト タブ
        $(".tab li").click(function() {
            var index = $(this).parent("ul").children("li").index(this);
            $(this).siblings("li").removeClass("active");
            $(this).addClass("active");
            $(this).parent("ul").nextAll(".panel").hide();
            $(this).parent("ul").nextAll(".panel").eq(index).show();
        });
    </script>
    <script type="text/javascript">
        jQuery(function($) {
            $('#ftbtn01').on('click', function() {
                $("#ftbtn01wrap").slideToggle();
                $("#ftbtn02wrap").hide();
            });
            $('#addmenu01').on('click', function() {
                $("#addmenu01wrap").slideToggle();
                $("#addmenu02wrap").hide();
            });
            $('#addmenu02').on('click', function() {
                $("#addmenu02wrap").slideToggle();
                $("#addmenu01wrap").hide();
            });
            $('#sp-icon').on('click', function() {
                $("#addmenu01wrap").hide();
                $("#addmenu02wrap").hide();
            });
            $(".more_wrap").on("click", function() {
                $(this).toggleClass("on-click");
                $(".hide-text").slideToggle(1000);
            });
             $(".more_wrap02").on("click", function() {
                $(this).toggleClass("on-click02");
                $(".hide-text02").slideDown(1000);
            });
             $(".more_wrap02").on("click", function() {
                $(".more_wrap03").toggleClass("on-click03");

            });
            $(".more_wrap03").on("click", function() {
                $(".more_wrap03").toggleClass("on-click03");
                $(".hide-text02").slideUp(1000);
                 $(".more_wrap02").toggleClass("on-click02");
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".tab_cont").hide();
            $("ul.tab_menu li:first").addClass("active").show();
            $(".tab_cont:first").show();
            $("ul.tab_menu li").click(function() {
                $("ul.tab_menu li").removeClass("active");
                $(this).addClass("active");
                $(".tab_cont").hide();
                var activeTab = $(this).find("a").attr("href");
                $(activeTab).fadeIn();
                return false;
            });
        });

        $(document).ready(function() {
            $(".tab_cont02").hide();
            $("ul.tab_menu02 li:first").addClass("active").show();
            $(".tab_cont02:first").show();
            $("ul.tab_menu02 li").click(function() {
                $("ul.tab_menu02 li").removeClass("active");
                $(this).addClass("active");
                $(".tab_cont02").hide();
                var activeTab = $(this).find("a").attr("href");
                $(activeTab).fadeIn();
                return false;
            });
        });
    </script>

    <!-- 買取実績タブ　-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.tile.js"></script>
    <script>
        $(document).ready(function() {
            $(".box-4 .title p.itemName").tile(3);
            $(".box-4 .title p.itemdetail").tile(3);
        });
        $(window).load(function() {
            $(".box-4 .title img").tile(3);
            $(".postlist").tile(3);
            $('#lp-cat-jisseki li').tile(2);
        });

        $(function() {
            $(".menu").mouseover(function() {
                $(".menu").removeClass("hover");
                $(this).addClass("hover");
                $(".content:not('.hover + .content')").fadeOut();
                $(".hover + .content").fadeIn();
            });
        });
        $(function() {
            $(".menu2").mouseover(function() {
                $(".menu2").removeClass("hover2");
                $(this).addClass("hover2");
                $(".content2:not('.hover2 + .content2')").fadeOut();
                $(".hover2 + .content2").fadeIn();
            });
        });
        $(function() {
            $(".menu3").mouseover(function() {
                $(".menu3").removeClass("hover3");
                $(this).addClass("hover3");
                $(".content3:not('.hover3 + .content3')").fadeOut();
                $(".hover3 + .content3").fadeIn();
            });
        });

    </script>
    <!-- modal -->

<script src="https://cdn.jsdelivr.net/npm/jquery@3/dist/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/slick.js"></script>
        <script>$('.archive_purchase.slider').slick({
   autoplay:true,
    autoplaySpeed:3000,
    dots:true,
    slidesToShow:2,
    slidesToScroll:1
});</script>


<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.leanModal.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>
<script type="text/javascript">
    $(function() {
        $('a[rel*=leanModal]').leanModal({
            top: 50, // モーダルウィンドウの縦位置を指定
            overlay: 0.5, // 背面の透明度
            closeButton: ".modal_close" // 閉じるボタンのCSS classを指定
        });
    });
    $(document).ready(function() {
        $("#open").on("click", function(e) {
            e.preventDefault();
            $("#overlay, #modal").addClass("active");

            $("#close, #overlay").on("click", function() {
                $("#overlay, #modal").removeClass("active");
                return false;
            });
        });
    });
    var clipboard = new ClipboardJS('.copy-target');
    // クリップ成功
    clipboard.on('success', (e) => {
        alert('コピーしました');
    });
</script>
    <!--  jquery // -->
    <script type="text/javascript">
        piAId = '437102';
        piCId = '55756';
        piHostname = 'pi.pardot.com';
        (function() {
            function async_load() {
                var s = document.createElement('script');
                s.type = 'text/javascript';
                s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
                var c = document.getElementsByTagName('script')[0];
                c.parentNode.insertBefore(s, c);
            }
            if (window.attachEvent) {
                window.attachEvent('onload', async_load);
            } else {
                window.addEventListener('load', async_load, false);
            }
        })();

    </script>
    </body>
    </html>
