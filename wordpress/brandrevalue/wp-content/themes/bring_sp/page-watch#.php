<?php
  
get_header(); ?>

<div class="mv_area ">
  <img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_kv.png " alt="時計の取扱いについて">
</div>
<div class="cat_cnt">
    <h2 class="cat_tl">動かなくなったブランド時計も高額で買取いたします</h2>
    <p class="cat_tx">時刻を知るという実用性と同時に、ファッション性や社会的ステイタスまでも演出するうえで重要なアイテム――それがブランド時計というもの。丁寧にメンテナンスをすれば一生使えるような名品もあり、古い時計の中には新品以上に価値のあるものも少なくありません。「昔買ったけど最近使っていない」「電池切れでずっと止まったまま」「昔プレゼントされたけど故障して動かなくなった」そんなブランド時計があれば、ぜひBRAND REVALUEへお持ちください。動かなくなった時計も、ちょっとした修理や電池交換で直るようなケースでは想像以上に高額の査定額となりますし、傷、汚れのある時計もレア度やブランド性を加味して最大限高く評価いたします。</p>
</div>

<div id="cat-jisseki">
    <h3 class="cat-jisseki_tl">買取実績</h3>
        <p class="cat_jisseki_tx">BRAND REVALUEでは独自の中古時計再販ルートを構築しているため、幅広いブランドの腕時計を高く査定し、買い取ることが可能となっております。また、ブランド腕時計の買取において豊富な経験を積んだスタッフが在籍しており、故障した時計、電池切れで動かない時計、傷ついた時計等も価値を見落とすことなく最大限高く査定。<br>
「他店で査定を断られたような時計が、BRAND REVALUEでは驚くほどの高額買取の対象となった」――そのようなことも全く珍しくはありません。</p>
        <ul id="box-jisseki" class="list-unstyled clearfix">
            <li class="box-4">
                <div class="title">
                    <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/watch009.png" alt=""></p>
                    <p class="itemName">PATEK PHILIPPE
                        <br>ノーチラス Ref5712</p>
                    <hr>
                    <p>
                        <span class="red">A社</span>：3,201,000円
                        <br>
                        <span class="blue">B社</span>：3,135,000円
                    </p>
                </div>
                <div class="box-jisseki-cat">
                    <h3>買取価格例</h3>
                    <p class="price">3,300,000<span class="small">円</span></p>
                </div>
                <div class="sagaku">
                    <p><span class="small">買取差額“最大”</span>165,000円</p>
                </div>
            </li>
            <li class="box-4">
                <div class="title">
                    <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/watch010.png" alt=""></p>
                    <p class="itemName">BREITLING
                        <br>ナビタイマー ref. A022B01NP</p>
                    <hr>
                    <p>
                        <span class="red">A社</span>：388,000円
                        <br>
                        <span class="blue">B社</span>：380,000円
                    </p>
                </div>
                <div class="box-jisseki-cat">
                    <h3>買取価格例</h3>
                    <p class="price">400,000<span class="small">円</span></p>
                </div>
                <div class="sagaku">
                    <p><span class="small">買取差額“最大”</span>20,000円</p>
                </div>
            </li>
            <li class="box-4">
                <div class="title">
                    <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/watch011.png" alt=""></p>
                    <p class="itemName">PANERAI
                        <br>ルミノール クロノデイライト</p>
                    <hr>
                    <p>
                        <span class="red">A社</span>：582,000円
                        <br>
                        <span class="blue">B社</span>：570,000円
                    </p>
                </div>
                <div class="box-jisseki-cat">
                    <h3>買取価格例</h3>
                    <p class="price">600,000<span class="small">円</span></p>
                </div>
                <div class="sagaku">
                    <p><span class="small">買取差額“最大”</span>30,000円</p>
                </div>
            </li>
            <li class="box-4">
                <div class="title">
                    <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/watch012.png" alt=""></p>
                    <p class="itemName">ボールウォッチ
                        <br>エンジニア ハイドロカーボン</p>
                    <hr>
                    <p>
                        <span class="red">A社</span>：77,600円
                        <br>
                        <span class="blue">B社</span>：76,000円
                    </p>
                </div>
                <div class="box-jisseki-cat">
                    <h3>買取価格例</h3>
                    <p class="price">80,000<span class="small">円</span></p>
                </div>
                <div class="sagaku">
                    <p><span class="small">買取差額“最大”</span>4,000円</p>
                </div>
            </li>
            <li class="box-4">
                <div class="title">
                    <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/watch001.jpg" alt=""></p>
                    <p class="itemName">ROLEX
                        <br>サブマリーナ</p>
                    <hr>
                    <p>
                        <span class="red">A社</span>：480,000円
                        <br>
                        <span class="blue">B社</span>：510,000円
                    </p>
                </div>
                <div class="box-jisseki-cat">
                    <h3>買取価格例</h3>
                    <p class="price">580,000<span class="small">円</span></p>
                </div>
                <div class="sagaku">
                    <p><span class="small">買取差額“最大”</span>100,000円</p>
                </div>
            </li>
            <li class="box-4">
                <div class="title">
                    <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/watch002.jpg" alt=""></p>
                    <p class="itemName">AUDEMARS PIGUET
                        <br>ロイヤルオーク オフショア</p>
                    <hr>
                    <p>
                        <span class="red">A社</span>：2,630,000円
                        <br>
                        <span class="blue">B社</span>：2,750,000円
                    </p>
                </div>
                <div class="box-jisseki-cat">
                    <h3>買取価格例</h3>
                    <p class="price">2,900,000<span class="small">円</span></p>
                </div>
                <div class="sagaku">
                    <p><span class="small">買取差額“最大”</span>270,000円</p>
                </div>
            </li>
            <li class="box-4">
                <div class="title">
                    <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/watch003.jpg" alt=""></p>
                    <p class="itemName">PATEK PHILIPPE
                        <br>コンプリケーテッド</p>
                    <hr>
                    <p>
                        <span class="red">A社</span>：3,900,000円
                        <br>
                        <span class="blue">B社</span>：4,100,000円
                    </p>
                </div>
                <div class="box-jisseki-cat">
                    <h3>買取価格例</h3>
                    <p class="price">4,700,000<span class="small">円</span></p>
                </div>
                <div class="sagaku">
                    <p><span class="small">買取差額“最大”</span>800,000円</p>
                </div>
            </li>
            <li class="box-4">
                <div class="title">
                    <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/watch004.jpg" alt=""></p>
                    <p class="itemName">BREGUET
                        <br>コンプリケーション</p>
                    <hr>
                    <p>
                        <span class="red">A社</span>：3,400,000円
                        <br>
                        <span class="blue">B社</span>：3,600,000円
                    </p>
                </div>
                <div class="box-jisseki-cat">
                    <h3>買取価格例</h3>
                    <p class="price">4,100,000<span class="small">円</span></p>
                </div>
                <div class="sagaku">
                    <p><span class="small">買取差額“最大”</span>700,000円</p>
                </div>
            </li>
            <li class="box-4">
                <div class="title">
                    <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/watch005.jpg" alt=""></p>
                    <p class="itemName">BREITLING
                        <br>ナビタイマー</p>
                    <hr>
                    <p>
                        <span class="red">A社</span>：400,000円
                        <br>
                        <span class="blue">B社</span>：410,000円
                    </p>
                </div>
                <div class="box-jisseki-cat">
                    <h3>買取価格例</h3>
                    <p class="price">470,000<span class="small">円</span></p>
                </div>
                <div class="sagaku">
                    <p><span class="small">買取差額“最大”</span>70,000円</p>
                </div>
            </li>
        </ul>
  <div class="kaitori_point">
            <h3>【時計の高価買取ポイント】</h3>
腕時計の査定の際にぜひお持ちいただきたいのは、購入時に同封されていた保証書や明細書。<br>
そのモデルの種類、購入価格等がわかればよりスムーズに査定を行うことができます。購入時の箱や付属品のベルト等、付属品がそろっていれば、より査定金額が高くなるでしょう。もちろん、保証書や箱などの付属品がなくても時計そのものの価値は変わりませんのでご心配なく。ベルトがボロボロになっている場合なども、時計本体の状態によっては高い査定価格のご提示も可能です。<br>
手軽に利用できるLINE査定などもありますので、ぜひお気軽にご相談ください。</div>

</div>


<div class="point_list">
            <div class="coint_bnr">
                <img src="<?php echo get_s3_template_directory_uri() ?>/images/coin_bnr01.png" alt="他の店鋪より1円でも安ければご連絡下さい。">
                <ul class="other_price">
                    <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/coin_bnr02.png" alt="他の店鋪より1円でも安ければご連絡下さい。"></li>
                    <li>ブリングは徹底した高価買取を行っております。お客様が愛着を持って身につけてきた服は、大切に買い取りさせて頂きます。 万が一、他店よりも1円でも安い価格であればおっしゃってください。なるべくお客様のご要望に答えられるよう、査定させて頂きます。<br>
                まずは、こちらから買い取り実績について、覗いてみてください。きっと、お客様の古着に相応しい、ご期待に添える価格での買い取りを行っているはずです。</li>
                </ul>
            </div>
    <h3 class="obi_tl">高価買取のポイント</h3>
    <img src="<?php echo get_s3_template_directory_uri() ?>/images/kaitori_point.png" alt="高価買い取りのポイント" class="point_img">
    <h3 class="obi_tl">ブランドリスト</h3>
    <ul class="cat_list">
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_1.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_2.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_3.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_4.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_5.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_6.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_7.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_8.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_9.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_10.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_11.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_12.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_13.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_14.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_15.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_16.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_17.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_18.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_19.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_20.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_21.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_22.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_23.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_24.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_25.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_26.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_27.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_28.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_29.jpg" alt=""></li>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/watch_30.jpg" alt=""></li>

    </ul>
</div>

<?php
  // お問い合わせ
  get_template_part('_action');
  
  // 3つのポイント
  get_template_part('_purchase');
  
  // お問い合わせ
  get_template_part('_action2');
  
  // 店舗
  get_template_part('_shopinfo');
  
  // フッター
  get_footer();