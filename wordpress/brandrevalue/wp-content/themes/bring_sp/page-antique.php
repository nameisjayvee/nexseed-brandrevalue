<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */
// 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  'anti01.jpg::中国切手::赤猿::170,000::167,000::180,000::13,000',
  'anti02.jpg::中国切手::子供のころから科学を愛そう::112,000::98,000::120,000::22,000',
  'anti03.jpg::竜の図::鍔::203,000::189,000::210,000::21,000',
  'anti04.jpg::中国 大型 咸豊通宝::一百::179,000::190,000::200,000::21,000',
  'anti05.jpg::明治36年1円銀貨::NGC鑑定済み::110,000::112,000::115,000::5,000',
  'anti06.jpg::釜師浄雪造 丸釜 釜輪付共箱::煎茶道具::47,500::48,000::50,000::2,000',
  'anti07.jpg::中国古玩::堆朱人物山水風景細密彫碁笥::56,000::5,2000::58,000::6,000',
  'anti08.jpg::桃山時代　絵志野茶碗::検織部魯山人瀬戸黒陶片向付::109,000::112,000::123,000::14,000',
  'anti09.jpg::瀬戸黒茶碗::荒川　豊蔵::55,000::60,000::65,000::10,000',
  'anti10.jpg::練上嘯裂文大壷::松井康成::110,000::120.000::130,000::20,000',
  'anti11.jpg::六十二間筋兜緑糸威二枚胴具足::　　::350,000::370,000::400,000::50,000',
  'anti12.jpg::黒茶碗::十四代  覚入::190,000::195,000::200,000::10,000',
);

    get_header(); ?>

    <div id="primary" class="cat-page content-area">
        <div class="mv_area ">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/lp_main/cat_antique_main.jpg" alt="時計の取扱いについて">
        </div>
<p><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/heading_antique_sp.png" alt="あなたの骨董品お売り下さい"></p>
        <div class="lp_main">

            <section id="hikaku" class="watch_hikaku">
                <p class="hikaku_img"><img data-src="http://kaitorisatei.info/brandrevalue/wp-content/uploads/2017/03/antique.png"></p>
            </section>


            <section id="cat-point">
                <h3 class="obi_tl">高価買取のポイント</h3>
                <ul>
                    <li>
                        <p class="pt_bigtl">POINT1</p>
                        <div class="pt_wrap">
                            <p class="pt_tl">商品情報が明確だと査定がスムーズ</p>
                            <p class="pt_tx">ブランド名、モデル名が明確だと査定がスピーディに出来、買取価格にもプラスに働きます。また、新作や人気モデル、人気ブランドであれば買取価格がプラスになります。</p>
                        </div>
                    </li>
                    <li>
                        <p class="pt_bigtl">POINT2</p>
                        <div class="pt_wrap">
                            <p class="pt_tl">数点まとめての査定だと
                                <br>買取がスムーズ</p>
                            <p class="pt_tx">数点まとめての査定依頼ですと、買取価格をプラスさせていただきます。</p>
                        </div>
                    </li>
                    <li>
                        <p class="pt_bigtl">POINT3</p>
                        <div class="pt_wrap">
                            <p class="pt_tl">品物の状態がよいほど
                                <br>査定がスムーズ</p>
                            <p class="pt_tx">お品物の状態が良ければ良いほど、買取価格もプラスになります。</p>
                        </div>
                    </li>
                </ul>
                <p>BRAND REVALUEはまだ新しい買取ショップですが、骨董品買取に精通したスタッフがそろっており再販ルートも確立しております。</br>
お客様の大切な骨董品の価値を見逃すことなく高く査定させていただいていることから、オープン後骨董品の買取実績も急速に伸びております。また、BRANDREVALUE(ブランドリバリュー)は銀座の中心地という利便性の高い地域に店舗を構えていますが、路面店と比べて格段に家賃の安い空中階に店舗があるため、他店と比べて圧倒的に経費を安く抑えることができています。</br>
</br>
その他、広告費、人件費等も可能な限り削減しており、その分査定額を高めに設定してお客様に利益を還元しております。</p>
            </section>

            <section id="lp-cat-jisseki">
                <h3 class="obi_tl">買取実績</h3>

                <ul id="box-jisseki" class="list-unstyled clearfix">
                    <?php
            foreach($resultLists as $list):
            // :: で分割
            $listItem = explode('::', $list);

          ?>
                        <li class="box-4">
                            <div class="title">
                                <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/<?php echo $listItem[0]; ?>" alt="">
                                <p class="itemName">
                                    <?php echo $listItem[1]; ?>
                                        <br>
                                        <?php echo $listItem[2]; ?>
                                </p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：
                                    <?php echo $listItem[3]; ?>円
                                        <br>
                                        <span class="blue">B社</span>：
                                        <?php echo $listItem[4]; ?>円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">
                                    <?php echo $listItem[5]; ?><span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>
                                    <?php echo $listItem[6]; ?>円</p>
                            </div>
                        </li>
                        <?php endforeach; ?>
                </ul>
                <p id="catchcopy">骨董品の査定額は、ちょっとした工夫で高くなることがあるのをご存知でしょうか。それは「ご自宅で簡単にメンテナンスをしてから査定に出す」というもの。モノ自体がよくても、埃がたまっているなど汚れが目立つ場合は査定額が下がってしまう場合があるのです。ご自宅でできる範囲で表面の汚れをふき取るだけでも評価額が上がるので、ぜひお試しください。その他、購入時の箱や袋等の付属品が一式そろっていると、査定金額が上がります。
                </p>
                <p class="jyoutai_tl">こんな状態でも買取致します!</p>
                <div id="konna">

                    <p class="example1">■ほこりまみれ</p>
                    <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/anti_jyoutai01.jpg"></p>
                    <p class="example2">■価値が分からない</p>
                    <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/anti_jyoutai02.jpg"></p>
                    <p class="example3">■大量</p>
                    <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/anti_jyoutai03.jpg"></p>
                    <p class="text">その他：作者が分からないものでもお買取りいたします。</p>
                </div>
            </section>

            <section id="about_kaitori" class="clearfix">
                <?php
        // 買取について
        get_template_part('_widerange');
				get_template_part('_widerange2');
      ?>
            </section>



            <section>
                <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/bn_matomegai.png">
            </section>

            <?php
        // 買取基準
        get_template_part('_criterion');

        // NGアイテム
        get_template_part('_no_criterion');

				// カテゴリーリンク
        get_template_part('_category');
			?>
                <section id="kaitori_genre">
                    <h3 class="obi_tl">その他買取可能なジャンル</h3>
                    <p>【買取ジャンル】バッグ/ウエストポーチ/セカンドバック/トートバッグ/ビジネスバッグ/ボストンバッグ/クラッチバッグ/トランクケース/ショルダーバッグ/ポーチ/財布/カードケース/パスケース/キーケース/手帳/腕時計/ミュール/サンダル/ビジネスシューズ/パンプス/ブーツ/ペアリング/リング/ネックレス/ペンダント/ピアス/イアリング/ブローチ/ブレスレット/ライター/手袋/傘/ベルト/ペン/リストバンド/アンクレット/アクセサリー/サングラス/帽子/マフラー/ハンカチ/ネクタイ/ストール/スカーフ/バングル/カットソー/アンサンブル/ジャケット/コート/ブルゾン/ワンピース/ニット/シャツメンズ/毛皮/Tシャツ/キャミソール/タンクトップ/パーカー/ベスト/ポロシャツ/ジーンズ/スカート/スーツなど</p>
                </section>

                <?php
        // 買取方法
        get_template_part('_purchase');
      ?>

                    <section id="user_voice">
                        <h3>ご利用いただいたお客様の声</h3>

                        <p class="user_voice_text1">ちょうど家の整理をしていたところ、家のポストにチラシが入っていたので、ブランドリバリューに電話してみました。今まで買取店を利用したことがなく、不安でしたがとても丁寧な電話の対応とブランドリバリューの豊富な取り扱い品目を魅力に感じ、出張買取を依頼することにしました。 絶対に売れないだろうと思った、動かない時計や古くて痛んだバッグ、壊れてしまった貴金属のアクセサリーなども高額で買い取っていただいて、とても満足しています。古紙幣や絵画、食器なども買い取っていただけるとのことでしたので、また家を整理するときにまとめて見てもらおうと思います。
                        </p>

                        <h3>鑑定士からのコメント</h3>

                        <p class="user_voice_text2">家の整理をしているが、何を買い取ってもらえるか分からないから一回見に来て欲しいとのことでご連絡いただきました。 買取店が初めてで不安だったが、丁寧な対応に非常に安心しましたと笑顔でおっしゃって頂いたことを嬉しく思います。 物置にずっとしまっていた時計や、古いカバン、壊れてしまったアクセサリーなどもしっかりと価値を見極めて高額で提示させて頂きましたところ、お客様もこんなに高く買取してもらえるのかと驚いておりました。 これからも家の不用品を整理するときや物の価値判断ができないときは、すぐにお電話しますとおっしゃって頂きました。
                        </p>

                    </section>

        </div>
        <!-- lp_main -->
    </div>
    <!-- #primary -->

    <?php

get_footer();
