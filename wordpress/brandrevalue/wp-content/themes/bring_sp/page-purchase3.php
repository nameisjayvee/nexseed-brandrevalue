<?php
get_header(); ?>

<div class="mv_area ">
<img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kv-purchase3.png " alt="BRAMD REVALUE ">
</div>
<section id="mailform">
<div class="">

<!--
<div role="form" class="wpcf7" id="wpcf7-f66-o1" lang="ja" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/purchase1#wpcf7-f66-o1" method="post" class="wpcf7-form form-horizontal" enctype="multipart/form-data" novalidate>
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="66" />
<input type="hidden" name="_wpcf7_version" value="4.3.1" />
<input type="hidden" name="_wpcf7_locale" value="ja" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f66-o1" />
<input type="hidden" name="_wpnonce" value="3dcf703af9" />
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div>
<div class="form-group form-name">
<label class="control-label col-sm-3"><span class="req">必須</span>お名前</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap sei"><input type="text" name="sei" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control span2" aria-required="true" aria-invalid="false" placeholder="お名前" /></span></div>
</div>
<div class="form-group form-name">
<label class="control-label col-sm-3">フリガナ</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap kana-sei"><input type="text" name="kana-sei" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control input-mini" aria-required="true" aria-invalid="false" placeholder="フリガナ" /></span></div>
</div>
<div class="form-group form-email">
<label class="control-label col-sm-3"><span class="req">必須</span>電話番号</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap email"><input type="tel" name="tel" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group form-email">
<label class="control-label col-sm-3"><span class="req">必須</span>ご希望送付点数</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap email">約<input type="number" name="number" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" aria-required="true" aria-invalid="false" min="0"/>点</span>
<p>※おおよその点数で構いません。</p>
</div>
</div>
<div class="form-group form-name">
<label class="control-label col-sm-3"><span class="req">必須</span>ご住所</label>

<label class="control-label col-sm-3 place">郵便番号</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap sei"><input type="text" name="postal" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control span2" aria-required="true" aria-invalid="false"  /></span>
<label class="control-label col-sm-3 place">都道府県</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap sei"><input type="text" name="prefectures" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control span2" aria-required="true" aria-invalid="false"/></span>
<label class="control-label col-sm-3 place">市区町村</label>
<div class="col-sm-9 city"><span class="wpcf7-form-control-wrap sei"><input type="text" name="city" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control span2" aria-required="true" aria-invalid="false"/></span></div>
</div>
<div class="form-group form-email">
<label class="control-label col-sm-3"><span class="req">必須</span>メールアドレス</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group form-email">
<label class="control-label col-sm-3"><span class="req">必須</span>確認のためもう一度</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap email-check"><input type="email" name="email-check" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" aria-required="true" aria-invalid="false" /></span></div>
</div>

<div class="form-group item_name">
<label class="control-label col-sm-3"><span class="req">必須</span>商品名</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap item_name"><input type="text" name="item_name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control span2" aria-required="true" aria-invalid="false" /></span>
<span class="wpcf7-form-control-wrap item_name"><input type="text" name="item_name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control span2" aria-required="true" aria-invalid="false" /></span>
<span class="wpcf7-form-control-wrap item_name"><input type="text" name="item_name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control span2" aria-required="true" aria-invalid="false" /></span><span class="wpcf7-form-control-wrap item_name"><input type="text" name="item_name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control span2" aria-required="true" aria-invalid="false" /></span><span class="wpcf7-form-control-wrap item_name"><input type="text" name="item_name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control span2" aria-required="true" aria-invalid="false" /></span></div>
<p>※最低でも一つのブランド名または宝石の名前を入力してください。
<br> ※不明の場合は不明とご記入お願い致します。
</p>
</div>
<div class="form-group form-check">
<label class="control-label col-sm-3">メルマガ配信</label>
<input type="checkbox" name="riyu" value="1">
</div>
<div class="form-group form-text">
<label class="control-label col-sm-3">備考欄</label>
<div class="col-sm-9">
<p><span class="wpcf7-form-control-wrap bikou"><textarea name="bikou" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea form-control" aria-invalid="false"></textarea></span></p>
</div>
</div>
<div class="text-center">
<input type="image" value="リセット" class="" src="<?php echo get_s3_template_directory_uri() ?>/images/reset_btn.png" />
<input type="image" value="送信" class="wpcf7-form-control" src="<?php echo get_s3_template_directory_uri() ?>/images/sub_btn.png" />
</div>
</form>
</div>
-->
          <?php echo do_shortcode('[mwform_formkey key="23205"]'); ?>

</div>
</section>
      <section class="kaitori_hoken" style="padding:0 10px;">
	  <h3>宅配保険サービス</h3>
	  <p class="bol">高額なお品物でも安心してご利用頂けるよう、宅配保険サービスをご用意しております。</p>
	  <dl class="hoken_box">
	  <dt class="hoken_at">必須条件1</dt>
	  <dd><p>事前査定<br />メール・LINE・電話にて事前に査定を受けて下さい。</p></dd>
	  </dl>
	  <dl class="hoken_box">
	  <dt class="hoken_at">必須条件2</dt>
	  <dd><p>当店からお送りする宅配梱包キット<br />（梱包材・着払い伝票）をご使用ください。</p></dd>
	  </dl>
	  <p class="small_txt">※事前査定の金額が補償金額の上限となります。事前査定がない場合は、配送会社の規定に準じます。<br />※ご不明点がございましたら直接お電話にてお問合せくださいませ。</p>
	  <div class="mousikomi_box">
	  <p>※お申し込みは下記のボタンより</p>
	  <a class="red_btn" href="https://kaitorisatei.info/brandrevalue/purchase3">宅配買取り申し込みはコチラ</a>

	  </div>


	  </section>


<script src="http://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script type="text/javascript">
(function () {

    "use strict";

    /**
     * Pardot form handler assist script.
     * Copyright (c) 2018 toBe marketing, inc.
     * Released under the MIT license
     * http://opensource.org/licenses/mit-license.php
     */

    // == 設定項目ここから ==================================================================== //

    /*
     * ＠データ取得方式
     */
    var useGetType = 1;

    /*
     * ＠フォームハンドラーエンドポイントURL
     */
    var pardotFHUrl="//go.pardot.com/l/436102/2018-01-18/dgs1sp";

    /*
     * ＠フォームのセレクタ名
     */
    var parentFormName = '#mw_wp_form_mw-wp-form-23205 > form';

    /*
     * ＠送信ボタンのセレクタ名
     */
    var submitButton = 'input[type=submit][name=confirm]';

    /*
     * ＠項目マッピング
     */

    var defaultFormJson=[
    { "tag_name": "sei",               "x_target": 'sei'               },
    { "tag_name": "mei",               "x_target": 'mei'               },
    { "tag_name": "kana-sei",          "x_target": 'kana-sei'          },
    { "tag_name": "kana-mei",          "x_target": 'kana-mei'          },
    { "tag_name": "email",             "x_target": 'email'             },
    { "tag_name": "tel",               "x_target": 'tel'               },
    { "tag_name": "address1",          "x_target": 'address1'          },
    { "tag_name": "address2",          "x_target": 'address2'          },
    { "tag_name": "address3",          "x_target": 'address3'          },
    { "tag_name": "address4",          "x_target": 'address4'          },
    { "tag_name": "address5",          "x_target": 'address5'          },
    { "tag_name": "request_insurance", "x_target": 'request_insurance' },
    { "tag_name": "request_kit",       "x_target": 'request_kit'       },
    { "tag_name": "bikou",             "x_target": 'bikou'             },
	{ "tag_name": "inquiry",           "x_target": 'inquiry'           },
	{ "tag_name": "inquiry_item",      "x_target": 'inquiry_item'      },
	{ "tag_name": "acceptance",        "x_target": 'acceptance[data]'  }
    ];

    // == 設定項目ここまで ==================================================================== //

    // 区切り文字設定
    var separateString = ',';

    var iframeFormData = '';
    for (var i in defaultFormJson) {
        iframeFormData = iframeFormData + '<input id="pd' + defaultFormJson[i]['tag_name'] + '" type="text" name="' + defaultFormJson[i]['tag_name'] + '"/>';
    }

    var iframeHeadSrc =
        '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';

    var iframeSrcDoc =
        '<form id="shadowForm" action="' + pardotFHUrl + '" method="post" accept-charset=\'UTF-8\'>' +
        iframeFormData +
        '<input id="shadowSubmit" type="submit" value="send" onclick="document.charset=\'UTF-8\';"/>' +
        '</form>';

    var shadowSubmited = false;
    var isError = false;
    var pdHiddenName = 'pdHiddenFrame';

    /**
     * エスケープ処理
     * @param val
     */
    function escapeSelectorString(val) {
        return val.replace(/[ !"#$%&'()*+,.\/:;<=>?@\[\\\]^`{|}~]/g, "\\$&");
    }

    $(function () {

        $(submitButton).click(function () {
            // Submitボタンを無効にする。
            $(submitButton).prop("disabled", true);
        });

        $('#' + pdHiddenName).remove();

        $('<iframe>', {
            id: pdHiddenName
        })
            .css({
                display: 'none'
            })
            .on('load', function () {

                if (shadowSubmited) {
                    if (!isError) {

                        shadowSubmited = false;

                        // 送信ボタンを押せる状態にする。
                        $(submitButton).prop("disabled", false);

                        // 親フォームを送信
                        $(parentFormName).submit();
                    }
                } else {
                    $('#' + pdHiddenName).contents().find('head').prop('innerHTML', iframeHeadSrc);
                    $('#' + pdHiddenName).contents().find('body').prop('innerHTML', iframeSrcDoc);

                    $(submitButton).click(function () {
                        shadowSubmited = true;
                        try {
                            for (var j in defaultFormJson) {
                                var tmpData = '';

                                // NANE値取得形式
                                if (useGetType === 1) {
                                    $(parentFormName + ' [name="' + escapeSelectorString(defaultFormJson[j]['x_target']) + '"]').each(function () {

                                        //checkbox,radioの場合、未選択項目は送信除外
                                        if (["checkbox", "radio"].indexOf($(this).prop("type")) >= 0) {
                                            if ($(this).prop("checked") == false) {
                                                return true;
                                            }

                                        }

                                        if (tmpData !== '') {
                                            tmpData += separateString;
                                        }

                                        // 取得タイプ 1 or Null(default) :val()方式, 2:text()形式
                                        if (defaultFormJson[j]['x_type'] === 2) {
                                            tmpData += $(this).text().trim();
                                        } else {
                                            tmpData += $(this).val().trim();
                                        }
                                    });
                                }

                                // セレクタ取得形式
                                if (useGetType === 2) {
                                    $(defaultFormJson[j]['x_target']).each(function () {
                                        if (tmpData !== '') {
                                            tmpData += separateString;
                                        }

                                        // 取得タイプ 1 or Null(default) :val()方式, 2:text()形式
                                        if (defaultFormJson[j]['x_type'] === 2) {
                                            tmpRegexData = $(this).text().trim();
                                        } else {
                                            tmpRegexData = $(this).val().trim();
                                        }
                                    });
                                }

                                $('#' + pdHiddenName).contents().find('#pd' + escapeSelectorString(defaultFormJson[j]['tag_name'])).val(tmpData);

                            }

                            $('#' + pdHiddenName).contents().find('#shadowForm').submit();

                        } catch (e) {
                            isError = true;

                            $(submitButton).prop("disabled", false);

                            $(parentFormName).submit();
                            shadowSubmited = false;
                        }
                        return false;
                    });
                }
            })
            .appendTo('body');
    });
})();
</script>

<?php
  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
