<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

// ランキング更新時はここだけ弄る
$rankTopImageName = 'toppage_ranking_sp2019.jpg';
get_header(); ?>

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/slide-video.css">
<script src="<?php echo get_template_directory_uri() ?>/js/slide-video.js"></script>

<div class="mv_area main-gallery" id="main-gallery" style="margin-top:-20px;">

    <div class="gallery-cell"><a href="<?php echo home_url('customer'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/slider/slide_sp_satei.jpg" alt="スピード無料査定"></a></div>
    <div class="gallery-cell"><a href="<?php echo home_url('cat/watch'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/slider/slide_sp_watch.jpg" alt="“業界最高峰”の時計買取価格"></a></div>
    <div class="gallery-cell"><a href="<?php echo home_url('cat/bag'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/slider/slide_sp_bag.jpg" alt="“業界最高峰”のバッグ買取価格"></a></div>
    <div class="gallery-cell"><a href="<?php echo home_url('cat/brandjewelery'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/slider/slide_sp_jewelry.jpg" alt="業界最高峰”のブランドジュエリー買取価格"></a></div>
    <div class="gallery-cell"><a href="<?php echo home_url('purchase'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/slider/slide_sp_kaitori.jpg" alt="3つの買取方法"></a></div>
    <div class="gallery-cell"><a href="<?php echo home_url('shinsaibashi'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/slider/slide_sp_shinsaibashi.jpg" alt="心斎橋店　NEWOPEN"></a></div>
    <div class="gallery-cell"><a href="<?php echo home_url('shop'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/slider/slide_sp_shop.jpg" alt="店舗情報"></a></div>
    <div class="gallery-cell"><a href="#slide-video-popup" id="slide-video"><img src="<?php echo get_s3_template_directory_uri() ?>/video/image_thumb_sp.jpg" alt="動画サムネイル"></a></div>
</div>

<div id="slide-video-popup"><video controls preload="none" webkit-playsinline playsinline width="320px"><source src="<?php echo get_s3_template_directory_uri() ?>/video/image.mp4"></video></div>
<div id="slide-video-popup-filter"></div>

<div id="mv_bottom">
    <h2>注目の買取ブランド</h2>
    <div id="mv_bottom_3colomun">
        <div><a href="<?php echo home_url('cat/bag/hermes'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/mv/brand_mv_hermes_sp.jpg" alt="HERMES(エルメス)"></a></div>
        <div><a href="<?php echo home_url('cat/bag/chanel'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/mv/brand_mv_chanel_sp.jpg" alt="シャネル(CHANEL)"></a></div>
        <div><a href="<?php echo home_url('cat/wallet/louisvuitton'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/mv/brand_mv_louisvuitton_sp.jpg" alt="ルイ・ヴィトン(LOUIS VUITON)"></a></div>
    </div>
</div>

<ul class="more_bx ">
    <li>
        <a href="<?php echo home_url('about-purchase/takuhai'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/images/more01.png " alt="宅配買取 "></a>
    </li>
    <li>
        <a href="<?php echo home_url('about-purchase/syutchou'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/images/more02.png " alt="出張買取 "></a>
    </li>
    <li>
        <a href="<?php echo home_url('about-purchase/tentou'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/images/more03.png " alt="店頭買取 "></a>
    </li>
    <li>
        <a href="<?php echo home_url('about-purchase/tebura'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/images/more04.png " alt="手ぶら買取 "></a>
    </li>
</ul>

<section class="kaitori_cat" style="margin-top:20px;">
    <ul>
        <li>
            <a href="<?php echo home_url('blog/doburock'); ?>"><img src="https://kaitorisatei.info/brandrevalue/wp-content/uploads/2018/12/caf61dc6779ec6137c0ab58dfe3a550d.jpg" alt="どぶろっく"></a>
        </li>
    </ul>
</section>

<div class="infomation_box">
    <h3 class="content_ttl">お知らせ</h3>
    <?php
    $posts = get_posts(array(
        'posts_per_page' => 3, // 表示件数
        'category' => '181' // カテゴリIDもしくはスラッグ名
    ));
    ?>
    <ul class="information_list">
        <?php if($posts): foreach($posts as $post): setup_postdata($post); ?>
            <li><a href="<?php the_permalink() ?>"><span class="date">
                <?php the_time('Y/m/d') ?>
            </span>
            <?php the_title(); ?>
            </a></li>
        <?php endforeach; endif; ?>
    </ul>
</div>

<div class="top_search_box">
    <form method="get" class="searchform" action="<?php echo esc_url( home_url('/') ); ?>">
        <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/sp_search_bg.jpg">
        <div class="top_search_box_inner">
            <input type="search" placeholder="ブランド名でさがす" name="s" class="searchfield_top" value="" />
            <input type="image" src="<?php bloginfo('template_directory'); ?>/img/search_btn.png" alt="検索" class="searchsubmit_top">
        </div>
    </form>
    <a class="search_link" href="<?php echo home_url('/brand'); ?>">取扱ブランド一覧からさがす<i class="fas fa-arrow-circle-right"></i></a>
</div>


<div class="kijyun_cnt ">
    <h2><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/toriatsukaicat_tl.png " alt="取扱カテゴリー "></h2>
    <ul class="cat_list">
        <li>
            <a href="<?php echo home_url('cat/gold'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat01.png" alt="金・プラチナ"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/gem'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat02.png" alt="宝石"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/watch'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat03.png" alt="時計"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/bag'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat04.png" alt="バッグ"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/outfit'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat05.png" alt="洋服・毛皮"></a>
        </li>
        <li>
            <a href="<?php echo home_url('kimono'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat10.png" alt="着物"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/wallet'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat06.png" alt="ブランド・財布"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/shoes'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat07.png" alt="靴"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/diamond'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat08.png" alt="ダイヤモンド"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/brandjewelery'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat_bj.png" alt="ブランドジュエリー"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/antique'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat09.png" alt="骨董品"></a>
        </li>
        <li>
            <a href="<?php echo home_url('cat/mement'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/btn-memento_sp.png" alt="遺品"></a>
        </li>
    </ul>
    <div class="cat_list single">
        <a href="<?php echo home_url('cat/antique_rolex'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/catanti_rolex.png" alt="アンティークロレックス"></a>
    </div>
</div>
<div class="infomation_box">
    <h3 class="content_ttl">最新の買取実績はこちら</h3>
    <ul class="archive_purchase slider">
        <?php query_posts(
            array(
                'post_type' => 'purchase_item',
                'taxonomy' => 'purchase_item',
                'posts_per_page' => 8,
                'paged' => $paged,
            ));
        if (have_posts()): while ( have_posts() ) : the_post();
        ?>
            <li><a href="<?php the_permalink(); ?>">


                <p class="kaitori-date"><?php the_field('kaitori-date'); ?></p>
                <p class="kaitori-image"><img data-src="<?php the_field('kaitori-img'); ?>" alt="<?php the_title(); ?>"></p>
                <p class="kaitori-title"><?php the_title(); ?></p>
                <p class="kaitori-price-header">買取価格</p>
                <P class="kaitori-price"> ¥<?php the_field('kaitori-price'); ?></P>
                <p class="kaitori-method"><?php the_field('kaitori-method'); ?></p>
            </a></li>
        <?php endwhile; endif; wp_reset_query(); ?>
    </ul>
    <a href="<?php echo home_url('purchase_items'); ?>" class="purchase_more">買取実績一覧はコチラ<i class="fas fa-caret-right"></i></a>
    <a href="<?php echo home_url('ranking'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/ranking/<?php echo $rankTopImageName ?>" alt="買い取りランキング"></a>
</div>





<div class="point_cnt ">
    <h2><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/top_point.png " alt="ブランドリバリュー当店の3つのポイント "></h2>
</div>
<?php
// 3つの買取方法
get_template_part('cv_parts/page_cv01');
?>


<ul class="custom_cv_box01">
    <li class="mb20">
        <a href="<?php echo home_url('customer'); ?>">
            <p>買取お申込み総合案内</p>
        </a>
    </li>
</ul>

<div class="topItem_cnt ">
    <h2><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/jsseki_tl.png " alt="ブランドリバリューの買取実績 "></h2>
    <div class="kaitori_box">
        <ul class="tab_menu">
            <li><a href="#tab1">ブランド</a></li>
            <li><a href="#tab2">金</a></li>
            <li><a href="#tab3">宝石</a></li>
            <li><a href="#tab4">時計</a></li>
            <li><a href="#tab5">バック</a></li>
        </ul>
        <!--ブランド-->
        <div class="tab_cont" id="tab1">
            <ul id="box-jisseki" class="list-unstyled clearfix">
                <!-- barand1 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/001.jpg" alt=""></p>
                        <p class="itemName">オーデマピゲ　ロイヤルオークオフショアクロノ 26470OR.OO.A002CR.01 ゴールド K18PG</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：3,080,000円<br>
                            <span class="blue">B社</span>：3,000,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">3,200,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>200,000円</p>
                    </div>
                </li>

                <!-- brand2 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/002.jpg" alt=""></p>
                        <p class="itemName">パテックフィリップ　コンプリケーテッド ウォッチ 5085/1A-001</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：1,470,000円<br>
                            <span class="blue">B社</span>：1,380,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">1,650,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>270,000円</p>
                    </div>
                </li>

                <!-- brand3 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/003.jpg" alt=""></p>
                        <p class="itemName">パテックフィリップ　コンプリケーション 5130G-001 WG</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：2,630,000円<br>
                            <span class="blue">B社</span>：2,320,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">3,300,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>800,000円</p>
                    </div>
                </li>
                <!-- brand4 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/004.jpg" alt=""></p>
                        <p class="itemName">パテックフィリップ　ワールドタイム 5130R-001</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：2,800,000円
                            <br>
                            <span class="blue">B社</span>：2,500,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">3,300,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>800,000円</p>
                    </div>
                </li>

                <!-- brand5 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/005.jpg" alt=""></p>
                        <p class="itemName">シャネル　ラムスキン　マトラッセ　二つ折り長財布</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：85,000円<br>
                            <span class="blue">B社</span>：75,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">100,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>25,000円</p>
                    </div>
                </li>
                <!-- brand6 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/006.jpg" alt=""></p>
                        <p class="itemName">エルメス　ベアンスフレ　ブラック</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：180,000円<br>
                            <span class="blue">B社</span>：157,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">210,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>53,000円</p>
                    </div>
                </li>
                <!-- brand7 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/007.jpg" alt=""></p>
                        <p class="itemName">エルメス　バーキン30　トリヨンクレマンス　マラカイト　SV金具</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：1,200,000円<br>
                            <span class="blue">B社</span>：1,050,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">1,350,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>300,000円</p>
                    </div>
                </li>
                <!-- brand8 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/008.jpg" alt=""></p>
                        <p class="itemName">セリーヌ　ラゲージマイクロショッパー</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：210,000円<br>
                            <span class="blue">B社</span>：180,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">240,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>60,000円</p>
                    </div>
                </li>
                <!-- brand9 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand/top/009.jpg" alt=""></p>
                        <p class="itemName">ルイヴィトン　裏地ダミエ柄マッキントッシュジャケット</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：34,000円<br>
                            <span class="blue">B社</span>：30,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">40,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>10,000円</p>
                    </div>
                </li>
            </ul>
        </div>
        <!--ブランドここまで-->
        <!--金-->
        <div class="tab_cont" id="tab2">
            <ul id="box-jisseki" class="list-unstyled clearfix">
                <!-- gold1 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/001.jpg" alt=""></p>
                        <p class="itemName">K18　ダイヤ0.11ctリング</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：29,750円<br>
                            <span class="blue">B社</span>：26,250円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">
                            <!-- span class="small">120g</span -->35,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>8,750円</p>
                    </div>
                </li>

                <!-- gold2 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/002.jpg" alt=""></p>
                        <p class="itemName">K18　メレダイヤリング</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：38,250円<br>
                            <span class="blue">B社</span>：33,750円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">45,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>11,250円</p>
                    </div>
                </li>

                <!-- gold3 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/003.jpg" alt=""></p>
                        <p class="itemName">K18/Pt900　メレダイヤリング</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：15,300円<br>
                            <span class="blue">B社</span>：13,500円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">18,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>4,500円</p>
                    </div>
                </li>

                <!-- gold4 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/004.jpg" alt=""></p>
                        <p class="itemName">Pt900　メレダイヤリング</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：20,400円<br>
                            <span class="blue">B社</span>：18,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">24,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>6,000円</p>
                    </div>
                </li>

                <!-- gold5 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/005.jpg" alt=""></p>
                        <p class="itemName">K18WG　テニスブレスレット</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：34,000円<br>
                            <span class="blue">B社</span>：30,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">40,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>10,000円</p>
                    </div>
                </li>

                <!-- gold6 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/006.jpg" alt=""></p>
                        <p class="itemName">K18/K18WG　メレダイヤリング</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：25,500円<br>
                            <span class="blue">B社</span>：22,500円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">30,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>7,500円</p>
                    </div>
                </li>

                <!-- gold7 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/007.jpg" alt=""></p>
                        <p class="itemName">K18ブレスレット</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：23,800円<br>
                            <span class="blue">B社</span>：21,900円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">28,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>7,000円</p>
                    </div>
                </li>
                <!-- gold8 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/008.jpg" alt=""></p>
                        <p class="itemName">14WGブレスレット</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：52,700円<br>
                            <span class="blue">B社</span>：46,500円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">62,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>1,5500円</p>
                    </div>
                </li>
                <!-- gold9 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold/top/009.jpg" alt=""></p>
                        <p class="itemName">Pt850ブレスレット</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：27,200円<br>
                            <span class="blue">B社</span>：24,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">32,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>8,000円</p>
                    </div>
                </li>
            </ul>
        </div>
        <!--金ここまで-->
        <!--宝石-->
        <div class="tab_cont" id="tab3">

            <ul id="box-jisseki" class="list-unstyled clearfix">
                <!-- gem1 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/001.jpg" alt=""></p>
                        <p class="itemName">ダイヤルース</p>
                        <p class="itemdetail">カラット：1.003ct<br>カラー：E<br>クラリティ：VS-2<br>カット：Good<br>蛍光性：FAINT<br>形状：ラウンドブリリアント</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：380,000円<br>
                            <span class="blue">B社</span>：355,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">450,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>115,000円</p>
                    </div>
                </li>

                <!-- gem2 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/002.jpg" alt=""></p>
                        <p class="itemName">Pt900 DR0.417ctリング</p>
                        <p class="itemdetail">カラー：F<br>クラリティ：SI-1<br>カット：VERY GOOD <br>蛍光性：FAINT<br>形状：ラウンドブリリアント<br>メレダイヤモンド0.7ct<br>地金：18金イエローゴールド　5ｇ</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：51,000円<br>
                            <span class="blue">B社</span>：45,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">60,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>15,000円</p>
                    </div>
                </li>

                <!-- gem3 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/003.jpg" alt=""></p>
                        <p class="itemName">Pt900　DR0.25ctリング</p>
                        <p class="itemdetail">カラー：H<br>クラリティ:VS-1<br>カット：Good<br>蛍光性：MB<br>形状：ラウンドブリリアント<br></p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：58,000円<br>
                            <span class="blue">B社</span>：51,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">68,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>17,000円</p>
                    </div>
                </li>

                <!-- gem4 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/004.jpg" alt=""></p>
                        <p class="itemName">K18　DR0.43ct　MD0.4ctネックレストップ</p>
                        <p class="itemdetail">カラー：I<br>クラリティ：VS-2<br>カット：Good<br>蛍光性：WB<br>形状：ラウンドブリリアント</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：60,000円<br>
                            <span class="blue">B社</span>：52,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">70,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>18,000円</p>
                    </div>
                </li>
                <!-- gem5 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/005.jpg" alt=""></p>
                        <p class="itemName">ダイヤルース</p>
                        <p class="itemdetail">カラット：0.787ct<br>カラー：E<br>クラアリティ：VVS-2<br>カット：Good<br>蛍光性：FAINT<br>形状：ラウンドブリリアント</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：220,000円<br>
                            <span class="blue">B社</span>：190,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">260,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>70,000円</p>
                    </div>
                </li>
                <!-- gem6 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem/top/006.jpg" alt=""></p>
                        <p class="itemName">Pt950　MD0.326ct　0.203ct　0.150ctネックレス</p>
                        <p class="itemdetail">カラー：F<br>クラリティ：SI-2<br>カット：Good<br>蛍光性：FAINT<br>形状：ラウンドブリリアント</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：100,000円<br>
                            <span class="blue">B社</span>：90,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">120,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>30,000円</p>
                    </div>
                </li>
            </ul>
        </div>
        <!--宝石ここまで-->
        <!--時計-->
        <div class="tab_cont" id="tab4">

            <ul id="box-jisseki" class="list-unstyled clearfix">

                <!-- watch1 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/001.jpg" alt=""></p>
                        <p class="itemName">パテックフィリップ
                            <br>カラトラバ 3923</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：850,000円
                                <br>
                                <span class="blue">B社</span>：750,000円
                            </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">1,000,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>250,000円</p>
                    </div>
                </li>

                <!-- watch2 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/002.jpg" alt=""></p>
                        <p class="itemName">パテックフィリップ
                            <br>アクアノート 5065-1A</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：1,700,000円
                                <br>
                                <span class="blue">B社</span>：1,500,000円
                            </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">2,000,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>500,000円</p>
                    </div>
                </li>

                <!-- watch3 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/003.jpg" alt=""></p>
                        <p class="itemName">オーデマピゲ
                            <br>ロイヤルオーク・オフショア 26170ST</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：1,487,500円
                                <br>
                                <span class="blue">B社</span>：1,312,500円
                            </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">1,750,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>437,500円</p>
                    </div>
                </li>

                <!-- watch4 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/004.jpg" alt=""></p>
                        <p class="itemName">ROLEX
                            <br>サブマリーナ 116610LV ランダム品番</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：1,105,000円
                                <br>
                                <span class="blue">B社</span>：975,000円
                            </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">1,300,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>325,000円</p>
                    </div>
                </li>
                <!-- watch5 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/005.jpg" alt=""></p>
                        <p class="itemName">ROLEX
                            <br>116505 ランダム品番 コスモグラフ</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：2,422,500円
                                <br>
                                <span class="blue">B社</span>：2,137,500円
                            </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">2,850,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>712,500円</p>
                    </div>
                </li>
                <!-- watch6 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/006.jpg" alt=""></p>
                        <p class="itemName">ブライトリング
                            <br>クロノマット44</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：289,000円
                                <br>
                                <span class="blue">B社</span>：255,000円
                            </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">340,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>85,000円</p>
                    </div>
                </li>
                <!-- watch7 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/007.jpg" alt=""></p>
                        <p class="itemName">パネライ
                            <br>ラジオミール 1940</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：510,000円
                                <br>
                                <span class="blue">B社</span>：450,000円
                            </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">600,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>150,000円</p>
                    </div>
                </li>
                <!-- watch8 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/008.jpg" alt=""></p>
                        <p class="itemName">ボールウォッチ
                            <br>ストークマン ストームチェイサープロ CM3090C</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：110,500円
                                <br>
                                <span class="blue">B社</span>：97,500円
                            </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">130,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>32,500円</p>
                    </div>
                </li>
                <!-- watch9 -->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch/top/009.jpg" alt=""></p>
                        <p class="itemName">ブレゲ
                            <br>クラシックツインバレル 5907BB12984</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：595,000円
                                <br>
                                <span class="blue">B社</span>：525,000円
                            </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">700,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>175,000円</p>
                    </div>
                </li>
            </ul>
        </div>
        <!--時計ここまで-->
        <!--バック-->
        <div class="tab_cont" id="tab5">

            <ul id="box-jisseki" class="list-unstyled clearfix">

                <!-- bag1-->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/001.jpg" alt=""></p>
                        <p class="itemName">エルメス<br />エブリンⅢ　トリヨンクレマンス</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：230,000円
                            <br>
                            <span class="blue">B社</span>：200,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">270,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>70,000円</p>
                    </div>
                </li>

                <!-- bag2-->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/002.jpg" alt=""></p>
                        <p class="itemName">プラダ<br />シティトート2WAYバッグ</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：110,000円
                            <br>
                            <span class="blue">B社</span>：95,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">132,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>37,000円</p>
                    </div>
                </li>

                <!-- bag3-->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/003.jpg" alt=""></p>
                        <p class="itemName">バンブーデイリー2WAYバッグ</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：100,000円
                            <br>
                            <span class="blue">B社</span>：90,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">120,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>30,000円</p>
                    </div>
                </li>

                <!-- bag4-->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/004.jpg" alt=""></p>
                        <p class="itemName">LOUIS VUITTON
                            <br>モノグラムモンスリGM</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：110,000円
                                <br>
                                <span class="blue">B社</span>：90,000円
                            </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">120,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>30,000円</p>
                    </div>
                </li>

                <!-- bag5-->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/005.jpg" alt=""></p>
                        <p class="itemName">HERMES
                            <br>バーキン30</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：1,360,000円
                                <br>
                                <span class="blue">B社</span>：1,200,000円
                            </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">1,600,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>400,000円</p>
                    </div>
                </li>
                <!-- bag6-->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/006.jpg" alt=""></p>
                        <p class="itemName">CHANEL
                            <br>マトラッセダブルフラップダブルチェーンショルダーバッグ</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：220,000円
                                <br>
                                <span class="blue">B社</span>：195,000円
                            </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">260,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>65,000円</p>
                    </div>
                </li>

                <!-- bag7-->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/007.jpg" alt=""></p>
                        <p class="itemName">LOUIS VUITTON
                            <br>ダミエ ネヴァーフルMM</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：110,000円
                                <br>
                                <span class="blue">B社</span>：96,000円
                            </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">130,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>34,000円</p>
                    </div>
                </li>

                <!-- bag8-->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/008.jpg" alt=""></p>
                        <p class="itemName">CELINE
                            <br>ラゲージマイクロショッパー</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：200,000円
                                <br>
                                <span class="blue">B社</span>：180,000円
                            </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">240,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>60,000円</p>
                    </div>
                </li>

                <!-- bag9-->
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img data-src="https://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag/top/009.jpg" alt=""></p>
                        <p class="itemName">ロエベ
                            <br>アマソナ23</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：75,000円
                                <br>
                                <span class="blue">B社</span>：68,000円
                            </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">90,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>22,000円</p>
                    </div>
                </li>
            </ul>
        </div>
        <!--バックここま-->
    </div>
    <div class="top_brand">
        <h3>取り扱いブランド多数</h3>
        <ul class="list-unstyled new_list">
            <li>
                <dl>
                    <a href="<?php echo home_url('/cat/watch/rolex'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch01.jpg" alt="ロレックス"></dd>
                        <dt><span>ROLEX</span>ロレックス</dt>
                    </a>
                </dl>
            </li>
            <li>
                <dl>
                    <a href="<?php echo home_url('/cat/watch/breitling'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch05.jpg" alt="ブライトリング"></dd>
                        <dt><span>Breitling</span>ブライトリング</dt>
                    </a>
                </dl>
            </li>
            <li>
                <dl>
                    <a href="<?php echo home_url('/cat/watch/franckmuller'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch02.jpg" alt="フランクミューラー"></dd>
                        <dt><span>Franckmuller</span>フランクミューラー</dt>
                    </a>
                </dl>
            </li>
            <li>
                <dl>
                    <a href="<?php echo home_url('/cat/watch/iwc'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch06.jpg" alt="アイダブルシー"></dd>
                        <dt><span>IWC</span>アイダブルシー</dt>
                    </a>
                </dl>
            </li>
            <li>
                <dl>
                    <a href="<?php echo home_url('/cat/watch/omega'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch03.jpg" alt="オメガ"></dd>

                        <dt><span>OMEGA</span>オメガ</dt>
                    </a>
                </dl>
            </li>
            <li>
                <dl>
                    <a href="<?php echo home_url('/cat/gem/bulgari'); ?>">

                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch27.jpg" alt="ブルガリ"></dd>
                        <dt><span>BVLGALI</span>ブルガリ</dt>
                    </a>
                </dl>

            </li>
            <li>
                <dl>
                    <a href="<?php echo home_url('/cat/gem/cartier'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch28.jpg" alt="カルティエ"></dd>
                        <dt><span>Cartier</span>カルティエ</dt>
                    </a>
                </dl>
            </li>
            <li>
                <dl>
                    <a href="<?php echo home_url('/cat/watch/patek-philippe'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch04.jpg" alt="パテックフィリップ"></dd>
                        <dt><span>Patek Philippe</span>パテックフィリップ</dt>
                    </a>
                </dl>
            </li>

            <li>
                <dl>
                    <a href="<?php echo home_url('/cat/gem/piaget'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch22.jpg" alt="ピアジェ"></dd>
                        <dt><span>PIAGET</span>ピアジェ</dt>
                    </a>
                </dl>
            </li>
            <li>
                <dl>
                    <a href="<?php echo home_url('/cat/wallet/louisvuitton'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch23.jpg" alt="ルイヴィトン"></dd>
                        <dt><span>LOUIS VUITTON</span>ルイヴィトン</dt>
                    </a>
                </dl>
            </li>
            <li>
                <dl>
                    <a href="<?php echo home_url('/cat/gem/tiffany'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch24.jpg" alt="ピアジェ"></dd>
                        <dt><span>TIFFANY</span>ティファニー</dt>
                    </a>
                </dl>
            </li>
            <li>
                <dl>
                    <a href="<?php echo home_url('/cat/gem/harrywinston'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch26.jpg" alt="グッチ"></dd>
                        <dt><span>HARRY WINSTON</span>ハリーウィンストン</dt>
                    </a>
                </dl>
            </li>

            <li>
                <dl>
                    <a href="<?php echo home_url('/cat/gem/vancleefarpels'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch30.jpg" alt="ヴァンクリーフ&アーペル"></dd>
                        <dt><span>Vanleefarpels</span>ヴァンクリーフ&アーペル</dt>
                    </a>
                </dl>
            </li>
            <li>
                <dl>
                    <a href="<?php echo home_url('/cat/bag/hermes'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch33.jpg" alt="エルメス"></dd>
                        <dt><span>Hermes</span>エルメス</dt>
                    </a>
                </dl>
            </li>
            <li>
                <dl>
                    <a href="<?php echo home_url('/cat/bag/prada'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag02.jpg" alt="プラダ"></dd>
                        <dt><span>PRADA</span>プラダ</dt>
                    </a>
                </dl>
            </li>
            <li>
                <dl>
                    <a href="<?php echo home_url('/cat/bag/fendi'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag03.jpg" alt="プラダ"></dd>
                        <dt><span>FENDI</span>フェンディ</dt>
                    </a>
                </dl>
            </li>
            <li>
                <dl>
                    <a href="<?php echo home_url('/cat/bag/dior'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag04.jpg" alt="ディオール"></dd>
                        <dt><span>Dior</span>ディオール</dt>
                    </a>
                </dl>
            </li>
            <li>
                <dl>
                    <a href="<?php echo home_url('/cat/bag/chanel'); ?>">
                        <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag01.jpg" alt="シャネル"></dd>
                        <dt><span>CHANEL</span>シャネル</dt>
                    </a>
                </dl>
            </li>


        </ul>
        <p><a href="<?php echo home_url('brand'); ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/bn_brand02.png" alt=""></a></p>
    </div>

    <?php
    // 買取基準
    get_template_part('_criterion');

    // お問い合わせ
    get_template_part('_action');

    // 店舗案内
    get_template_part('_shopinfo');

    // ブログ記事
    echo '<section id="blog-newlist">';
    // 新着記事4件取得
    $query = array(
        'posts_per_page' => '4',
        'post_type'     => 'blog',
        'post_status' => 'publish',
        'orderby'       => 'post_modified',
        'order'         => 'DESC',
    );
    $result = new WP_Query($query);

    //if(current_user_can('read_private_pages')) :
    echo '<h2>新着コラム</h2>';
    echo '<p class="bloglink"><a href="/brandrevalue/blog"> > 一覧を見る</a></p>';
    echo '<ul class="clearfix">';
    // ログインユーザーのみ閲覧可
    while ($result->have_posts()) { $result->the_post();
        echo '<li class="postlist">';
        echo '<a href="'.get_the_permalink().'">';
        the_post_thumbnail();
        echo '<p class="date">'.get_the_time("Y年m月d日（D）").'</p>';
        echo '<h3>'.get_the_title().'</h3>';
        echo '</a>';
        echo '</li>';
    }
    echo '</ul>';
    //endif;
    wp_reset_postdata();
    echo '</section>';

    // フッター
    get_footer();
