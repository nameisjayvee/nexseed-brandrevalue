<?php
  
get_header(); ?>

<div class="mv_area ">
            <img src="<?php echo get_s3_template_directory_uri() ?>/images/shoes_kv.png " alt="BRAMD REVALUE ">
        </div>
        <div class="cat_cnt">
            <h2 class="cat_tl">スニーカー、ブーツ、サンダルなどあらゆるブランド靴を高額買取！</h2>
            <p class="cat_tx">BRAND REVALUEではあらゆる種類のブランド靴の買取を行っております。スニーカー、ブーツ、ブーティ、ショートブーツ、ロングブーツ、サンダル、ミュール、パンプス、ローファー、スリッポン、レインブーツなど紳士靴、婦人靴を問わず、多岐にわたる靴が査定対象です。新品やそれに近い靴はもちろん、履き古した靴でもメンテナンス状態がよければ高額での買取査定が可能ですので、ぜひご自宅に不要な靴がありましたらお気軽にご相談ください。そもそも、品質のよい靴は丁寧に使えば一生モノとも言われます。「長く履いて愛着のある靴だからこそ、価値のわかる店でちゃんと見てもらい、可能な限り高額で買い取ってほしい」そんな方にこそ、BRAND REVALUEのブランド靴買取サービスはお勧めです。</p>
        </div>

        <div id="cat-jisseki">
            <h3 class="cat-jisseki_tl">買取実績</h3>
            <p class="cat_jisseki_tx">多くのブランド買取ショップでは貴金属やジュエリーには力を入れている一方、洋服や靴といったアパレル品の査定のノウハウが乏しいケースも多いと言われています。こうした場合、せっかくのお値打ち品もびっくりするような安い買取額になってしまう可能性も。その点BRAND REVALUEは新しい店舗ではありますが、系列店で長年アパレル買取の実績を積み重ねています。<br>
            ブランド靴の買取に精通したプロの鑑定士が古くなった靴でも適正に価値を査定いたしますので、他店に負けない高額査定が可能となっております。</p>
            <ul id="box-jisseki" class="list-unstyled clearfix">
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/shoes001.png" alt=""></p>
                        <p class="itemName">
                            <br>レザーサイドジップスニーカー</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：22,000円
                            <br>
                            <span class="blue">B社</span>：20,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">25,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>5000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/shoes002.png" alt=""></p>
                        <p class="itemName">
                            <br>ChristianLouboutin(ルブタン)20周年限定スタッズハイカットスニーカー</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：100,000円
                            <br>
                            <span class="blue">B社</span>：90,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">120,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>30,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/shoes003.png" alt=""></p>
                        <p class="itemName">
                            <br>RICKOWENS ハイカットスニーカー</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：42,000円
                            <br>
                            <span class="blue">B社</span>：40,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">60,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>20,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/shoes004.jpg" alt=""></p>
                        <p class="itemName">
                            <br>2014 SS カリグラフィー</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：57,000円
                            <br>
                            <span class="blue">B社</span>：56,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">77,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>21,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/shoes005.jpg" alt=""></p>
                        <p class="itemName">
                            <br>デザインショートブーツ</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：85,000円
                            <br>
                            <span class="blue">B社</span>：75,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">105,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>30,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/shoes006.jpg" alt=""></p>
                        <p class="itemName">
                            <br>オルガ・トロワ・コレクションアレッサンドロ</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：30,000円
                            <br>
                            <span class="blue">B社</span>：23,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">58,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>25,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/shoes007.jpg" alt=""></p>
                        <p class="itemName">
                            <br>リシュリューアレッサンドロ</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：49,000円
                            <br>
                            <span class="blue">B社</span>：47,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">77,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>30,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/shoes008.jpg" alt=""></p>
                        <p class="itemName">
                            <br>アレッサンドロ デムジュール</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：75,000円
                            <br>
                            <span class="blue">B社</span>：72,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">98,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>27,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/shoes009.jpg" alt=""></p>
                        <p class="itemName">
                            <br>パイレーツブーツ</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：34,000円
                            <br>
                            <span class="blue">B社</span>：31,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">40,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>9,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/shoes010.jpg" alt=""></p>
                        <p class="itemName">
                            <br>汚れ加工レザーハイカットスニーカー</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：13,000円
                            <br>
                            <span class="blue">B社</span>：11,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">14,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>3,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/shoes011.jpg" alt=""></p>
                        <p class="itemName">
                            <br>レースアップサイドジップブーツ</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：34,000円
                            <br>
                            <span class="blue">B社</span>：32,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">40,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>8,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/shoes012.jpg" alt=""></p>
                        <p class="itemName">
                            <br>サイドジップハイカットスニーカー</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：17,000円
                            <br>
                            <span class="blue">B社</span>：16,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">22,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>6,000円</p>
                    </div>
                </li>
            </ul>
            <div class="kaitori_point">
            <h3>【靴の高価買取ポイント】</h3>
靴の査定額を高めるためには、日ごろのメンテナンスが重要となります。革靴の場合は、それぞれの素材や色に合わせた専用のクリーニング剤や、防水スプレー等を日常的に使用することで、靴に汚れが定着するのを防ぎ、素材の劣化を抑えることができます。布製の靴の場合は水やぬるま湯で洗い、汚れを落としましょう。汚れが目立つ靴は、査定前に一度クリーニングすることをお勧めします。<br>
また、ミュールや夏用のパンプスは、春夏にお売りいただくとシーズン品としてプラス査定することが可能ですので、ぜひシーズンに合わせてお持ちください。
            </div>

        </div>

        <div class="point_list">
            <div class="coint_bnr">
                <img src="<?php echo get_s3_template_directory_uri() ?>/images/coin_bnr01.png" alt="他の店鋪より1円でも安ければご連絡下さい。">
                <ul class="other_price">
                    <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/coin_bnr02.png" alt="他の店鋪より1円でも安ければご連絡下さい。"></li>
                    <li>ブリングは徹底した高価買取を行っております。お客様が愛着を持って身につけてきた服は、大切に買い取りさせて頂きます。 万が一、他店よりも1円でも安い価格であればおっしゃってください。なるべくお客様のご要望に答えられるよう、査定させて頂きます。<br>
                まずは、こちらから買い取り実績について、覗いてみてください。きっと、お客様の古着に相応しい、ご期待に添える価格での買い取りを行っているはずです。</li>
                </ul>
            </div>
            <h3 class="obi_tl">高価買取のポイント</h3>
            <img src="<?php echo get_s3_template_directory_uri() ?>/images/kaitori_point.png" alt="高価買い取りのポイント" class="point_img">
            <h3 class="obi_tl">ブランドリスト</h3>
            <ul class="cat_list">
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/shoes_1.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/shoes_2.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/shoes_3.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/shoes_4.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/shoes_5.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/shoes_6.jpg" alt=""></li>

            </ul>
        </div>

<?php
  // お問い合わせ
  get_template_part('_action');
  
  // 3つのポイント
  get_template_part('_purchase');
  
  // お問い合わせ
  get_template_part('_action2');
  
  // 店舗
  get_template_part('_shopinfo');
  
  // フッター
  get_footer();