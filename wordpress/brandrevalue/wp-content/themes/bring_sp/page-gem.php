<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>

    <div id="primary" class="cat-page content-area">
        <div class="mv_area ">
            <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/lp_main/cat_gem_main.jpg" alt="あなたの宝石お売り下さい!">
        </div>
    <p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
    <p class="main_bottom">満足価格で買取！宝石・ジュエリー買取ならブランドリバリュー</p>
  <div id="lp_head" class="gem_ttl">
  <div>
  <p>銀座で最高水準の査定価格・サービス品質をご体験ください。</p>
  <h2>あなたの宝石<br />どんな物でもお売り下さい！！</h2>
  </div>
  </div>
    <div class="lp_main">


            <section id="hikaku" class="watch_hikaku">
                <p class="hikaku_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/04/f93d66e7422470d0db5c4daecf54c909.jpg"></p>
            </section>

    <div style="margin-top:20px; margin-bottom: 20px;"><a href="https://kaitorisatei.info/brandrevalue/blog/doburock"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/12/caf61dc6779ec6137c0ab58dfe3a550d.jpg" alt="どぶろっく"></a></div>

            <section id="cat-point">
                <h3 class="obi_tl">他店様より高く宝石を買い取れる3つのポイント！</h3>
                <ul>
                    <li>
                        <p class="pt_bigtl">POINT1</p>
                        <div class="pt_wrap">
                            <p class="pt_tl">国際基準に基づいた厳密な査定</p>
                            <p class="pt_tx">「BRAND REVALUE｣では、資格を所有したプロが在籍しておりますので、大きさに関係なく一つ一つの石を宝石国際鑑定機関の評価基準で見誤る事なくスムーズに査定すことが可能です。</p>
                        </div>
                    </li>
                    <li>
                        <p class="pt_bigtl">POINT2</p>
                        <div class="pt_wrap">
                            <p class="pt_tl">国内外の価格変動情報を共有</p>
                            <p class="pt_tx">常に専門のバイヤーが国内外の流通情報を日々把握、共有しており、最新の相場価格を把握できる情報網、環境を整えている「BRAND REVALUE｣は限界ギリギリの買取が可能です。</p>
                        </div>
                    </li>
                    <li>
                        <p class="pt_bigtl">POINT3</p>
                        <div class="pt_wrap">
                            <p class="pt_tl">最高額で販売できるルートの確保</p>
                            <p class="pt_tx">「BRAND REVALUE｣では、最高額でお買取りさせて頂く為に、日本国内外問わず常に最高額で販売できるよう市場開拓、顧客様へのアプローチ等、販売ルートの確保を行っている為、お客様の大切なお品物を他店様より高く買い取らせていただくことができます。</p>
                        </div>
                    </li>
                </ul>
                                    <section class="mb50"> <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/gem/jw_grf.png"> </section>

                <p>宝石の査定時には、購入時に付属していた鑑定書や鑑別書もぜひあわせてお持ちください。 これらの書類には、「宝石が天然ものか人工的に作られたものか」といった情報が記載されており、査定の処理がスムーズになります。また、宝石入りアクセサリーの場合購入時に入っていた箱や替えのチェーン等の付属品があると査定金額が上がる可能性があります。
                    <br> もちろん、鑑定書・鑑別書、箱等がなくても宝石そのものの価値は変わりません。「銀座一の高額査定」を目指して高く評価いたしますので、ご安心ください。
                </p>
            </section>
      <h3 class="mid">宝石 一覧</h3>
    <ul class="mid_link">
      <li>
      <a href="<?php echo home_url('/cat/diamond'); ?>">
      <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gem/01.jpg" alt="ダイヤ買取">
      <p class="mid_ttl">ダイヤ</p>
      </a>
      </li>
      <li>
      <a href="<?php echo home_url('/cat/gem/emerald'); ?>">
      <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gem/02.jpg" alt="エメラルド買取">
      <p class="mid_ttl">エメラルド</p>
      </a>
      </li>
      <li>
      <a href="<?php echo home_url('/cat/gem/opal'); ?>">
      <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gem/03.jpg" alt="オパール買取">
      <p class="mid_ttl">オパール</p>
      </a>
      </li>
      <li>
      <a href="<?php echo home_url('/cat/gem/sapphire'); ?>">
      <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gem/04.jpg" alt="サファイア買取">
      <p class="mid_ttl">サファイア</p>
      </a>
      </li>
      <li>
      <a href="<?php echo home_url('/cat/gem/tourmaline'); ?>">
      <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gem/05.jpg" alt="トルマリン買取">
      <p class="mid_ttl">トルマリン</p>
      </a>
      </li>
      <li>
      <a href="<?php echo home_url('/cat/gem/paraibatourmaline'); ?>">
      <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gem/06.jpg" alt="パライバトルマリン買取">
      <p class="mid_ttl">パライバトルマリン</p>
      </a>
      </li>
      <li>
      <a href="<?php echo home_url('/cat/gem/ruby'); ?>">
      <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gem/07.jpg" alt="ルビー買取">
      <p class="mid_ttl">ルビー</p>
      </a>
      </li>
      <li>
      <a href="<?php echo home_url('/cat/gem/hisui'); ?>">
      <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gem/08.jpg" alt="ヒスイ買取">
      <p class="mid_ttl">ヒスイ</p>
      </a>
      </li>
      <li>
      <a href="<?php echo home_url('/cat/brandjewelery'); ?>">
      <img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gem/09.jpg" alt="ブランドジュエリー買取">
      <p class="mid_ttl">ブランドジュエリー</p>
      </a>
      </li>
      </ul>

            <section id="lp-cat-jisseki">
                <h3 class="obi_tl">買取実績</h3>
                <ul id="box-jisseki" class="list-unstyled clearfix">

<!-- 1 -->
                    <li class="box-4">
                        <div class="title">
                            <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2017/10/sapphire.jpg" alt="">
                            </p>
                            <p class="itemName">Pt900 非加熱サファイア
ダイヤリング</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：1,780,000円
                                <br>
                                <span class="blue">B社</span>：1,750,000円
                            </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">1,850,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>100,000円</p>
                        </div>
                    </li>
<!-- 2 -->
                    <li class="box-4">
                        <div class="title">
                            <p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/gem/lp/004.jpg" alt="">
                            </p>
                            <p class="itemName">Pt900　エメラルド4.3ct　MD0384ctリング</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：380,000円
                                <br>
                                <span class="blue">B社</span>：379,000円
                            </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">400,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>21,000円</p>
                        </div>
                    </li>

<!-- 3 -->
                    <li class="box-4">
                        <div class="title">
                            <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2017/10/ruby.jpg" alt="">
                            </p>
                            <p class="itemName">ルビー ダイヤリング Pt900</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：489,000円
                                <br>
                                <span class="blue">B社</span>：470,000円
                            </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">500,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>30,000円</p>
                        </div>
                    </li>
<!-- 4 -->
                    <li class="box-4">
                        <div class="title">
                            <p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/gem/lp/002.jpg" alt="">
                            </p>
                            <p class="itemName">Pt900　ブラックオパール3.18ct　MD131ctリング</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：367,000円
                                <br>
                                <span class="blue">B社</span>：354,000円
                            </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">380,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>26,000円</p>
                        </div>
                    </li>

<!-- 5 -->
                    <li class="box-4">
                        <div class="title">
                            <p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/gem/lp/003.jpg" alt="">
                            </p>
                            <p class="itemName">黒蝶真珠　9.1mm～12.1mm　留め具K18ネックレス</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：300,000円
                                <br>
                                <span class="blue">B社</span>：280,000円
                            </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">350,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>70,000円</p>
                        </div>
                    </li>



<!-- 6 -->
                    <li class="box-4">
                        <div class="title">
                            <p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/gem/lp/006.jpg" alt="">
                            </p>
                            <p class="itemName">天然ジェダイト（翡翠）15.8ct　MD2.1ctリング</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：250,000円
                                <br>
                                <span class="blue">B社</span>：246,000円
                            </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">258,000 <span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>12,000円</p>
                        </div>
                    </li>

<!-- 7 -->
                    <li class="box-4">
                        <div class="title">
                            <p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/gem/lp/007.jpg" alt="">
                            </p>
                            <p class="itemName">Pt900/K18　アクアマリン12.35ct　MD0.6ctリング</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：96,000円
                                <br>
                                <span class="blue">B社</span>：92,000円
                            </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">100,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>8,000円</p>
                        </div>
                    </li>

<!-- 8 -->
                    <li class="box-4">
                        <div class="title">
                            <p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/gem/lp/008.jpg" alt="">
                            </p>
                            <p class="itemName">K18　アメジスト　MDリング</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：47,800円
                                <br>
                                <span class="blue">B社</span>：44,000円
                            </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">50,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>6,000円</p>
                        </div>
                    </li>

<!-- 9 -->
                    <li class="box-4">
                        <div class="title">
                            <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/04/9.jpg" alt="パライバトルマリンダイヤネックレス">
                            </p>
                            <p class="itemName">パライバトルマリンダイヤネックレス</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：420,000円
                                <br>
                                <span class="blue">B社</span>：280,000円
                            </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">500,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>220,000円</p>
                        </div>
                    </li>

<!-- 10 -->
                    <li class="box-4">
                        <div class="title">
                            <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/04/10.jpg" alt="アレキサンドライトダイヤリング">
                            </p>
                            <p class="itemName">アレキサンドライトダイヤリング</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：180,000円
                                <br>
                                <span class="blue">B社</span>：100,000円
                            </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">250,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>150,000円</p>
                        </div>
                    </li>

<!-- 11 -->
                    <li class="box-4">
                        <div class="title">
                            <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/04/11.jpg" alt="赤珊瑚ダイヤリング">
                            </p>
                            <p class="itemName">赤珊瑚ダイヤリング</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：130,000円
                                <br>
                                <span class="blue">B社</span>：480,000円
                            </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">170,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>90,000円</p>
                        </div>
                    </li>

<!-- 12 -->
                    <li class="box-4">
                        <div class="title">
                            <p class="bx_img"><img data-src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/04/8.jpg" alt="パールダイヤリング">
                            </p>
                            <p class="itemName">パールダイヤリング</p>
                            <hr>
                            <p>
                                <span class="red">A社</span>：40,000円
                                <br>
                                <span class="blue">B社</span>：25,000円
                            </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">60,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>35,000円</p>
                        </div>
                    </li>
                </ul>
<section id="list-brand" class="clearfix">
                    <!--    <h3 class="new_list_ttl">ブランドリスト</h3>
                <ul class="list-unstyled new_list">
          <li>
            <dl>
            <a href="<?php echo home_url('/cat/gem/cartier'); ?>">
              <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem01.jpg" alt="カルティエ"></dd>
              <dt>Cartier<span></span>カルティエ</dt>
              </a>
            </dl>
          </li>
          <li>
            <dl>
            <a href="<?php echo home_url('/cat/gem/tiffany'); ?>">
              <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem02.jpg" alt="ティファニー"></dd>
              <dt><span>Tiffany</span>ティファニー</dt>
              </a>
            </dl>
          </li>
          <li>
            <dl>
            <a href="<?php echo home_url('/cat/gem/harrywinston'); ?>">
              <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem03.jpg" alt="ハリーウィンストン"></dd>
              <dt><span>Harrywinston</span>ハリーウィンストン</dt>
              </a>
            </dl>
          </li>

                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/gem/piaget'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch22.jpg" alt="ピアジェ"></dd>
                                <dt><span>PIAGET</span>ピアジェ</dt>
                            </a>
                        </dl>
                    </li>
          <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/gem/vancleefarpels'); ?>">
                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch30.jpg" alt="ヴァンクリーフ&アーペル"></dd>
                                <dt><span>Vanleefarpels</span>ヴァンクリーフ&アーペル</dt>
                            </a>
                        </dl>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/gem/bulgari'); ?>">

                                <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch27.jpg" alt="ブルガリ"></dd>
                                <dt><span>BVLGALI</span>ブルガリ</dt>
                            </a>
                        </dl>

                    </li>
          <li>
                        <dl>
                        <a href="<?php echo home_url('/cat/gem/boucheron'); ?>">

              <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem06.jpg" alt="ブシュロン"></dd>
                            <dt><span>Boucheron</span>ブシュロン</dt>
                            </a>
                        </dl>
                    </li>
<li>
                        <dl>
                        <a href="<?php echo home_url('/cat/gem/emerald'); ?>">

                            <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem07.jpg" alt="エメラルド"></dd>
                            <dt><span>Emerald</span>エメラルド</dt>
                            </a>
                        </dl>
                    </li>
<li>
                        <dl>
                        <a href="<?php echo home_url('/cat/gem/opal'); ?>">

                            <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem08.jpg" alt="オパール"></dd>
                            <dt><span>Opal</span>オパール</dt>
                            </a>
                        </dl>
                    </li>
<li>
                        <dl>
                        <a href="<?php echo home_url('/cat/gem/sapphire'); ?>">

                            <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem09.jpg" alt="サファイア"></dd>
                            <dt><span>Sapphire</span>サファイア</dt>
                            </a>
                        </dl>
                    </li>

<li>
                        <dl>
                        <a href="<?php echo home_url('/cat/gem/tourmaline'); ?>">

                            <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem10.jpg" alt="トルマリン"></dd>
                            <dt><span>Tourmaline</span>トルマリン</dt>
                            </a>
                        </dl>
                    </li>
<li>
                        <dl>
                        <a href="<?php echo home_url('/cat/gem/paraibatourmaline'); ?>">

                            <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem11.jpg" alt="パライバトルマリン"></dd>
                            <dt><span style="font-size:9px;">Paraibatourmaline</span>パライバトルマリン</dt>
                            </a>
                        </dl>
                    </li>
<li>
                        <dl>
                        <a href="<?php echo home_url('/cat/gem/hisui'); ?>">

                            <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem12.jpg" alt="ヒスイ"></dd>
                            <dt><span>Hisui</span>ヒスイ</dt>
                            </a>
                        </dl>
                    </li>

<li>
                        <dl>
                        <a href="<?php echo home_url('/cat/gem/ruby'); ?>">

                            <dd><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem13.jpg" alt="ルビー"></dd>
                            <dt><span>Ruby</span>ルビー</dt>
                            </a>
                        </dl>
                    </li>


        </ul> -->


                <p id="catchcopy">BRAND REVALUEには宝石鑑定において長年の経験を積んできたスタッフが在籍しており、主要な宝石すべてに対し適切な査定を行っております。
                    <br> ダイヤモンド、ルビー、サファイア、エメラルド、真珠といったメジャーな宝石はもちろん、オパール、キャッツアイ、タンザナイト、アクアマリン、トパーズ、トルマリン、珊瑚、翡翠など、比較的市場での流通が少ない宝石についても対応しておりますので、「他店で査定ができなかった宝石」なども遠慮なくお持ちください。
                </p>
                <p class="jyoutai_tl">こんな状態でも買取致します!</p>
                <div id="konna">

                    <p class="example1">■イニシャル入り</p>
                    <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/gem_jyoutai01.jpg"></p>
                    <p class="example2">■金具破損</p>
                    <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/gem_jyoutai02.jpg"></p>
                    <p class="example3">■石のみ</p>
                    <p class="jyoutai_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cat/gem_jyoutai03.jpg"></p>
                    <p class="text">その他：片方だけのピアス等でもお買取りいたします。</p>
                </div>
            </section>

            <section id="about_kaitori" class="clearfix">
                <?php
        // 買取について
        get_template_part('_widerange');
        get_template_part('_widerange2');
      ?>
            </section>



            <section>
                <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/bn_matomegai.png">
            </section>

            <?php
        // 買取基準
        get_template_part('_criterion');

        // NGアイテム
        get_template_part('_no_criterion');

        // カテゴリーリンク
        get_template_part('_category');
      ?>
                <section id="kaitori_genre">
                    <h3 class="obi_tl">その他買取可能なジャンル</h3>
                    <p>【買取ジャンル】バッグ/ウエストポーチ/セカンドバック/トートバッグ/ビジネスバッグ/ボストンバッグ/クラッチバッグ/トランクケース/ショルダーバッグ/ポーチ/財布/カードケース/パスケース/キーケース/手帳/腕時計/ミュール/サンダル/ビジネスシューズ/パンプス/ブーツ/ペアリング/リング/ネックレス/ペンダント/ピアス/イアリング/ブローチ/ブレスレット/ライター/手袋/傘/ベルト/ペン/リストバンド/アンクレット/アクセサリー/サングラス/帽子/マフラー/ハンカチ/ネクタイ/ストール/スカーフ/バングル/カットソー/アンサンブル/ジャケット/コート/ブルゾン/ワンピース/ニット/シャツメンズ/毛皮/Tシャツ/キャミソール/タンクトップ/パーカー/ベスト/ポロシャツ/ジーンズ/スカート/スーツなど</p>
                </section>

                <?php
        // 買取方法
        get_template_part('_purchase');
      ?>

                    <section id="user_voice">
                        <h3>ご利用いただいたお客様の声</h3>

                        <p class="user_voice_text1">ちょうど家の整理をしていたところ、家のポストにチラシが入っていたので、ブランドリバリューに電話してみました。今まで買取店を利用したことがなく、不安でしたがとても丁寧な電話の対応とブランドリバリューの豊富な取り扱い品目を魅力に感じ、出張買取を依頼することにしました。 絶対に売れないだろうと思った、動かない時計や古くて痛んだバッグ、壊れてしまった貴金属のアクセサリーなども高額で買い取っていただいて、とても満足しています。古紙幣や絵画、食器なども買い取っていただけるとのことでしたので、また家を整理するときにまとめて見てもらおうと思います。
                        </p>

                        <h3>鑑定士からのコメント</h3>

                        <p class="user_voice_text2">家の整理をしているが、何を買い取ってもらえるか分からないから一回見に来て欲しいとのことでご連絡いただきました。 買取店が初めてで不安だったが、丁寧な対応に非常に安心しましたと笑顔でおっしゃって頂いたことを嬉しく思います。 物置にずっとしまっていた時計や、古いカバン、壊れてしまったアクセサリーなどもしっかりと価値を見極めて高額で提示させて頂きましたところ、お客様もこんなに高く買取してもらえるのかと驚いておりました。 これからも家の不用品を整理するときや物の価値判断ができないときは、すぐにお電話しますとおっしゃって頂きました。
                        </p>

                    </section>

        </div>
        <!-- lp_main -->
    </div>
    <!-- #primary -->

    <?php

get_footer();
