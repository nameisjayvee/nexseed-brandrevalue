<?php
  
get_header(); ?>

<div class="mv_area ">
            <img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_kv.png " alt="BRAMD REVALUE ">
        </div>
        <div class="cat_cnt">
            <h2 class="cat_tl">あらゆるブランドバッグ・鞄を高額買取いたします！</h2>
            <p class="cat_tx">BRAND REVALUEでは貴金属や時計等と共にブランドバッグ・鞄の買取にも力を入れています。ブランド物の中でもバッグは特に人気が高く、そのためコピー品や偽物も多く出回っている状況です。人気が高いため精巧な偽物も存在し、ブランドバッグの鑑定には特別な技能が要求されることもあります。BRAND REVALUEは系列店舗で古着買取を長年行っており、ブランドバッグの買取を熟知したプロの鑑定士が実績を積み重ねています。また、効率的な再販ルートも確立しており、一般的なブランド・貴金属買取ショップと比べて高額な買取査定が可能です。ファッションのトレンドの移り変わりは早く、流行りが過ぎてしまったデザインのバックの価値は査定額が低くなってしまうこともあります。クローゼットにしまってあるだけのブランドバッグがありましたら、ぜひBRAND REVALUEへお持ちください。</p>
        </div>

        <div id="cat-jisseki">
            <h3 class="cat-jisseki_tl">買取実績</h3>
            <p class="cat_jisseki_tx">BRAND REVALUEでは様々な種類のブランドバッグの買取を行っております。人気の高いルイ・ヴィトンやエルメス、コーチ、シャネル、グッチ、プラダなどのブランドバッグ・鞄が査定対象です。ひと昔前のデザインのバッグでもメンテナンス状態がよければ高額での買取査定が可能ですので、ぜひご自宅に使わなくなったバッグがありましたらBRAND REVALUEまでお持ちください。<br>
傷があったり、ベルトが切れてしまっているバッグについても、お気軽にご相談ください。<br>
他店では値段がほとんどつかなかったようなブランドバッグでも、BRAND REVALUEなら納得の高額査定で買い取りができるケースもございます。</p>
            <ul id="box-jisseki" class="list-unstyled clearfix">
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/bag009.png" alt=""></p>
                        <p class="itemName">HERMES
                            <br>エブリン3PM</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：184,300円
                            <br>
                            <span class="blue">B社</span>：180,500円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">190,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>9,500円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/bag010.png" alt=""></p>
                        <p class="itemName">PRADA
                            <br>ハンドバッグ</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：116,400円
                            <br>
                            <span class="blue">B社</span>：114,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">120,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>6,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/bag011.png" alt=""></p>
                        <p class="itemName">GUCCI
                            <br>バンブーバッグ</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：29,100円
                            <br>
                            <span class="blue">B社</span>：28,500円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">30,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>1,500円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/bag012.png" alt=""></p>
                        <p class="itemName">LOUIS VUITTON
                            <br>モンスリGM</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：58,200円
                            <br>
                            <span class="blue">B社</span>：57,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">60,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>3,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/bag001.jpg" alt=""></p>
                        <p class="itemName">HERMES
                            <br>バーキン30</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：1,300,000円
                            <br>
                            <span class="blue">B社</span>：1,600,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">1,900,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>60,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/bag002.jpg" alt=""></p>
                        <p class="itemName">CHANEL
                            <br>マトラッセ チェーンバッグ</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：100,000円
                            <br>
                            <span class="blue">B社</span>：130,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">150,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>50,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/bag003.jpg" alt=""></p>
                        <p class="itemName">LOUIS VUITTON
                            <br>ダミエ ネヴァーフル</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：130,000円
                            <br>
                            <span class="blue">B社</span>：152,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">180,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>50,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/bag004.jpg" alt=""></p>
                        <p class="itemName">CELINE
                            <br>ラゲージマイクロショッパーバッグ</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：1,300,000円
                            <br>
                            <span class="blue">B社</span>：1,600,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">1,900,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>60,000円</p>
                    </div>
                </li>
                <li class="box-4">
                    <div class="title">
                        <p class="bx_img"><img src="http://brand.kaitorisatei.info/wp-content/themes/bring/img/item/bag005.jpg" alt=""></p>
                        <p class="itemName">HERMES
                            <br>ピコタンロック</p>
                        <hr>
                        <p>
                            <span class="red">A社</span>：120,000円
                            <br>
                            <span class="blue">B社</span>：150,000円
                        </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price">170,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span>50,000円</p>
                    </div>
                </li>
            </ul>


            <div class="kaitori_point">
            <h3>【バッグの高価買取ポイント】</h3>
ブランドバッグ・鞄を高く売るためには、日頃のメンテナンスが重要です。よく手入れをすることや気をつけて使うことでバッグは長持ちしますし、売却する際の査定の評価も良いものとなります。また、バッグのブランド名、モデル名、購入した時期などをお伝えいただくことでスピーディーな査定を行うことができます。<br>バッグの保証書や箱、付属品の小物やベルトなどがありましたら必ず一緒にご持参ください。付属品が一式そろっていることで、査定金額に反映することができます。
            </div>

        </div>

        <div class="point_list">
            <div class="coint_bnr">
                <img src="<?php echo get_s3_template_directory_uri() ?>/images/coin_bnr01.png" alt="他の店鋪より1円でも安ければご連絡下さい。">
                <ul class="other_price">
                    <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/coin_bnr02.png" alt="他の店鋪より1円でも安ければご連絡下さい。"></li>
                    <li>ブリングは徹底した高価買取を行っております。お客様が愛着を持って身につけてきた服は、大切に買い取りさせて頂きます。 万が一、他店よりも1円でも安い価格であればおっしゃってください。なるべくお客様のご要望に答えられるよう、査定させて頂きます。<br>
                まずは、こちらから買い取り実績について、覗いてみてください。きっと、お客様の古着に相応しい、ご期待に添える価格での買い取りを行っているはずです。</li>
                </ul>
            </div>
            <h3 class="obi_tl">高価買取のポイント</h3>
            <img src="<?php echo get_s3_template_directory_uri() ?>/images/kaitori_point.png" alt="高価買い取りのポイント" class="point_img">
            <h3 class="obi_tl">ブランドリスト</h3>
            <ul class="cat_list">
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_1.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_2.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_3.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_4.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_5.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_6.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_7.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_8.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_9.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_10.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_11.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_12.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_13.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_14.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_15.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_16.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_17.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_18.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_19.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_20.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_21.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_22.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_23.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_24.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_25.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_26.jpg" alt=""></li>
                <li><img src="<?php echo get_s3_template_directory_uri() ?>/images/bag_27.jpg" alt=""></li>

            </ul>
        </div>

<?php
  // お問い合わせ
  get_template_part('_action');
  
  // 3つのポイント
  get_template_part('_purchase');
  
  // お問い合わせ
  get_template_part('_action2');
  
  // 店舗
  get_template_part('_shopinfo');
  
  // フッター
  get_footer();