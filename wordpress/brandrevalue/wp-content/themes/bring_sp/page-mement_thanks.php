<?php

    /*
       Template Name: thanksページ
     */

?>

<!DOCTYPE html>
<html lang="ja">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=640px,user-scalable=yes,maximum-scale=1.5" />
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/mement.css">
        <link rel="icon" href="<?php echo get_template_directory_uri() ?>/img/favicon.ico">

        <title>送信完了 | 遺品買取のブランドリバリュー</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <style>
            h1 {
                font-size: 32px;
                margin: 2em 0;
            }
            p {
                font-size: 20px;
                text-align:center;
            }
            footer {
                position: fixed;
                bottom: 0;
                width: 100%;
            }
            header .tel,
            header .tel-sp {
                right: 0;
            }
        </style>
        <?php get_template_part('template-parts/gtm', 'head') ?>
    </head>

    <body class="body">
        <?php get_template_part('template-parts/gtm', 'body') ?>
        <article>
            <header id="global">
                <div class="container">
                    <a class="logo" href="<?php echo home_url('/') ?>"><img data-src="<?php echo get_s3_template_directory_uri() ?>/imgs/header_logo.png" alt=""></a>
                </div>
            </header>

            <div class="container">
                <div class="containerInner" style="text-align: center; padding-top: 80px;">
                    <h1>フォーム送信完了</h1>
                    <p>送信は正常に完了しました。</p>
                    <p>お申し込みありがとうございました。</p>
                    <p><a href="<?php echo home_url('cat/mement') ?>">戻る</a></p>
                </div>
            </div>

            <footer id="global">
                <p class="copyright">Copyrights © <?php echo date('Y') ?> 高額ブランド買取専門サイト BRAND REVALUE all rights reserved</p>
            </footer>

        </article>

        <script src="<?php echo get_template_directory_uri() ?>/vendor/jquery/js/jquery-3.1.1.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/vendor/bootstrap/js/bootstrap.min.js"></script>

        <!-- Google Tag Manager -->

        <!-- End Google Tag Manager -->

        <!-- YTM -->

        <!-- YTM END -->
    </body>

</html>
