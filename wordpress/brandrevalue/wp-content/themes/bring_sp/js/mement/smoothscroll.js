$(function() {
  var offsetY = -80;
  var duration = 500;
  $('a[href^="#"]').click(function() {
    var target = $(this.hash);
    if (!target.length) return ;
    var targetY = target.offset().top+offsetY;
    $('html,body').animate({scrollTop: targetY}, duration, 'swing');
    window.history.pushState(null, null, this.hash);
    return false;
  });
});
