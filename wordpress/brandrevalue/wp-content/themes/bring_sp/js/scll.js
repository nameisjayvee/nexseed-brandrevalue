$(function () {
    var topBtn = $('#pageTop');
    topBtn.hide();
    //スクロールが100に達したらボタン表示
    $(window).scroll(function () {
        if ($(this).scrollTop() > 200) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
    //スクロールしてトップ
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
});

(function ($) {
    function sp_resize() {
        $('#menu').css({
            'width': $(window).width() + 'px',
        });
    }
    function sp_open_close() {
        var open_close = $(this).attr('class');
        if (open_close === 'sp-open') {
            $(this).attr('class', 'sp-close');
            $('#menu')
                .css({
                    'width': $(window).width() + 'px',
                    'top': $('div#sp-icon').height() + 'px',
                })
                .slideToggle();
            $('#add_box').css('display', 'none');
        } else {
            $(this).attr('class', 'sp-open');
            $('#menu').slideToggle();
            $('#add_box').css('display', 'block');
        }
    }
    $(window).on('resize', sp_resize);
    $('div#sp-icon').on('click', sp_open_close);
})(jQuery);
