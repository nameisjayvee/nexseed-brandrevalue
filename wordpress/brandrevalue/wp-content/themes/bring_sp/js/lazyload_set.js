// lazyloadの実行
let images = document.querySelectorAll("img");
lazyload(images);

// googleのクローラー対策
document.addEventListener('DOMContentLoaded', function(){
  const imagesDom = document.querySelectorAll('img');
  [].forEach.call(imagesDom, function(dom){
    dom.insertAdjacentHTML('afterend',`<noscript><img src＝"${dom.dataset.src}" alt="${dom.alt}"></noscript>`);
  });
});
