<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
  //'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
    '001.jpg::ダイヤネックレス PT900::2,200,000::2,000,000::2,500,000::500,000',
  '002.jpg::イエローダイヤネックレス PT900::1,920,000::1,900,000::2,000,000 ::100,000',
  '003.jpg::ダイヤテニスネックレス K18::125,000::110,000::150,000 ::40,000',
  '004.jpg::ダイヤテニスネックレス PT900::150,000::130,000::180,000::50,000',
  '005.jpg::ダイヤリング PT900::730,000::700,000::800,000::100,000',
  '006.jpg::イエローダイヤリング K18::900,000::840,000::1,000,000::160,000',
  '007.jpg::ダイヤブレスレット PT900::645,000::620,000::700,000::80,000',
  '008.jpg::ダイヤブレスレット PT900::500,000::478,000::580,000::102,000',
  '009.jpg::イエローダイヤピアス PT850::150,000::120,000::190,000::70,000',
  '010.jpg::ピンクダイヤピアス PT900::450,000::400,000::470,000::70,000',
  '011.jpg::ピンクダイヤ ルース 0.5ct::3,800,000::3,500,000::4,000,000::500,000',
  '012.jpg::ダイヤルース 2.02ct::3,280,000::3,100,000::3,500,000::400,000',
);


get_header(); ?>

<div id="primary" class="cat-page content-area">
	<div class="mv_area "> <img data-src="<?php echo get_s3_template_directory_uri() ?>/images/lp_main/cat_diamond_main.jpg" alt="あなたのダイヤモンドお売り下さい！"> </div>
	<p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
	<p class="main_bottom">ダイヤモンド買取！！業界最高峰の買取価格をお約束いたします。</p>
	<div id="lp_head" class="dia_ttl">
		<div>
			<p>銀座で最高水準の査定価格・サービス品質をご体験ください。</p>
			<h2>あなたのダイヤモンド<br />
				どんな物でもお売り下さい！！</h2>
		</div>
	</div>
	<div class="lp_main">
		<section id="hikaku" class="watch_hikaku">
			<p class="hikaku_img"><img data-src="//kaitorisatei.info/brandrevalue/wp-content/uploads/2017/10/hikaku_sp_diamond.jpg"></p>
		</section>
		<section class="new_dia_cont">
			<h3>より高く売るために！ダイヤモンド買取の基礎知識</h3>
			<p class="new_dia_tx">ダイヤモンドの価格を決定する主な３つの要件をご説明いたします。</p>
			<div class="found_box">
				<h4><span>1</span>4C</h4>
				<p>ダイヤモンドの４Cとは、クラリティ(Clarity)、カラー(Color)、カット(Cut)、カラット(Carat)のダイヤモンドの価値を決める4要素の頭文字をとったものです。ダイヤモンドの買取金額は、４Cによって大きく変動する為、大変重要は基準となっております。傷の無いダイヤモンド、透明度が高いダイヤモンド、綺麗なカットがほどこされているダイヤモンド、質量の重いダイヤモンドは希少なため価値が高くなります。 </p>
				<a href="#l01">ダイヤモンドの４Cと査定額について</a> </div>
			<div class="found_box">
				<h4><span>2</span>付属品の有無</h4>
				<p>ダイヤモンドのリングやネックレスなどがカルティエやハリーウィンストンなどのブランドジュエリーの場合に限りますが、購入時の箱やブランドが発行している保証書があるとより高額になります。もちろん、それらが無くても買取は可能ですが、査定にいらっしゃる前にご自宅をお探しいただくことをお勧めいたします。 </p>
			</div>
			<div class="found_box">
				<h4><span>3</span>査定士の質</h4>
				<p>ダイヤモンドを始め宝石の買取には非常に深い専門的な知識が必要になるため、宝石査定士により、買取金額にバラつきが出てしまうことが良くあります。ブランドリバリューでは、社内外の研修や勉強会などの徹底した教育体制を整えている為、査定士の質には自信がございます。 </p>
				<a href="">その他、ダイヤモンド買取のQ＆Aのコンテンツはこちら</a> </div>
		</section>
		<!--買取評価チェックコンテンツ-->
		<section id="check_cont">
			<h3>ダイヤモンド買取価格シミュレーション</h3>
			<form action="" name="sel_form" id="sel_form">
				<table class="kakaku_input">
					<tr>
						<th><span>●</span>カラー / 色</th>
						<td><select id="color_id" name="color_id">
								<option value="">選択して下さい</option>
								<option value="1">D</option>
								<option value="2">E</option>
								<option value="3">F</option>
								<option value="4">G</option>
								<option value="5">H</option>
								<option value="6">I</option>
								<option value="7">J</option>
							</select></td>
						</tr>
						<tr>
						<th><span>●</span>カット / 総合評価</th>
						<td><select id="cut_id" name="cut_id">
								<option value="">選択して下さい</option>
								<option value="1">Excellent(優秀)</option>
								<option value="2">Verygood(優良)</option>
								<option value="3">Good(良好)</option>
							</select></td>
					</tr>
					<tr>
						<th><span>●</span>クラリティ /内包物</th>
						<td><select id="clarity_id" name="clarity_id">
								<option value="">選択して下さい</option>
								<option value="1">VVS1</option>
								<option value="2">VVS2</option>
								<option value="3">VS1</option>
								<option value="4">VS2</option>
								<option value="5">SI1</option>
								<option value="6">SI2</option>
							</select></td>
						</tr>
												<tr>

						<th><span>●</span>カラット /重量</th>
						<td><input name="carat_value" id="carat_value" type="text" style="ime-mode:disabled;"></td>
					</tr>
				</table>
				<div id="kakaku_btn">
					<input type="button" class="hyouka_btn" id="estimation_btn" value="買取相場を調べる">
				</div>
				<div class="kakaku_box">
				<dl>
							<dt>買取相場</dt>
							<dd id="purchase_price"></dd>
				</dl>
				</div>
			</form>
		</section>
		<!--/買取評価チェックコンテンツ-->
		<section id="lp-cat-jisseki">
		<h3 class="obi_tl">買取実績</h3>
		<ul id="box-jisseki" class="list-unstyled clearfix">

			<!--
                  <li class="box-4">
                        <div class="title">
                         <p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/dia/lp/001.jpg" alt=""></p>
                            <p class="itemName">ダイヤルース1.098ct
                            </p>
                            < p class="itemdetail">カラット：2ct
                                <br> カラー：VERY LIGHT PINK
                                <br> クラリティ：VS２
                                <br> 形状：ハートシェイプ<br><br>
                            </p>
                            <hr>
                            <p> <span class="red">A社</span>：142,000円
                                <br>
                                <span class="blue">B社</span>：140,000円 </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">145,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>250,000円</p>
                        </div>
                    </li>
                    <li class="box-4">
                        <div class="title">
                         <p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/dia/lp/002.jpg" alt=""></p>
                            <p class="itemName">K18YG
                                <br> ダイヤモンドリング
                            </p>
                            <p class="itemdetail">カラット(主石)：0.54ct
                                <br> カラー：E
                                <br> クラリティ：VS２
                                <br> カット：VERY GOOD
                                <br> 形状：ラウンドブリリアント
                                <br> メレダイヤモンド0.7ct
                                <br> 地金：18金イエローゴールド　5ｇ
                            </p>
                            <hr>
                            <p> <span class="red">A社</span>：145,500円
                                <br>
                                <span class="blue">B社</span>：142,500円 </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price"><span class="small">地金＋</span>150,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>7,500円</p>
                        </div>
                    </li>
                    <li class="box-4">
                        <div class="title">
                    <p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/dia/lp/003.jpg" alt=""></p>
                            <p class="itemName">ティファニー
                                <br>ソレスト　ダイヤモンドリング</p>
                            <p class="itemdetail">
                                カラット：0.31ct
                                <br> カラー：E
                                <br> クラリティ：VVS2
                                <br> カット：EXCELLENT</p>
                            <hr>
                            <p> <span class="red">A社</span>：320,100円
                                <br>
                                <span class="blue">B社</span>：313,500円 </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price"><span class="small">地金＋</span>330,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>16,500円</p>
                        </div>
                    </li>
                     <li class="box-4">
                        <div class="title">
                         <p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/dia/lp/004.jpg" alt=""></p>
                            <p class="itemName">ペアシェイプ
                                <br> ダイヤモンドルース
                            </p>
                            <p class="itemdetail">カラット：5ct
                                <br> カラー：F
                                <br> クラアリティ：SI2
                                <br> 形状：ペアシェイプ<br><br><br><br>
                            </p>
                            <hr>
                            <p> <span class="red">A社</span>：3,395,500円
                                <br>
                                <span class="blue">B社</span>：3,325,000円 </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price"><span class="small">地金＋</span>3,500,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>175,000円</p>
                        </div>
                    </li>
                     <li class="box-4">
                        <div class="title">
                         <p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/dia/lp/005.jpg" alt=""></p>
                            <p class="itemName">ダイヤモンド　ルース</p>
                            <p class="itemdetail">カラット：1.003ct
                                <br> カラー:E
                                <br> クラリティ:SI1
                                <br> カット:EXCELLENT
                                <br> 形状:ラウンドブリリアントカット
                            </p>
                            <hr>
                            <p> <span class="red">A社</span>：194,000円
                                <br>
                                <span class="blue">B社</span>：190,000円 </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price"><span class="small">地金＋</span>480,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>10,000円</p>
                        </div>
                    </li>
                    <li class="box-4">
                        <div class="title">
                        <p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/dia/lp/006.jpg" alt=""></p>
                            <p class="itemName">カルティエ　バレリーナ　ハーフエタニティダイヤリング</p>
                            <p class="itemdetail">PT950
                                <br> カラット：0.51ct
                                <br> カラー：F
                                <br> クラリティ：VVS１
                                <br> カット：VERY　GOOD
                            </p>
                            <hr>
                            <p> <span class="red">A社</span>：388,000円
                                <br>
                                <span class="blue">B社</span>：380,000円 </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price"><span class="small">地金＋</span>400,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>20,000円</p>
                        </div>
                    </li>
                    <li class="box-4">
                        <div class="title">
                        <p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/dia/lp/007.jpg" alt=""></p>
                            <p class="itemName">ハリーウィンストン
                                <br> マドンナクロスネックレス
                            </p>
                            <p class="itemdetail">Pt950
                                <br>カラット：1.0ct
                                <br> カラー：D
                                <br>クラリティ：VVS2
                                <br> カット：EXCELLENT
                            </p>
                            <hr>
                            <p> <span class="red">A社</span>：970,000円
                                <br>
                                <span class="blue">B社</span>：950,000円 </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price"><span class="small">地金＋</span>1,000,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>50,000円</p>
                        </div>
                    </li>
                    <li class="box-4">
                        <div class="title">
                        <p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/dia/lp/008.jpg" alt=""></p>
                            <p class="itemName">ピアジェ　パッション
                                <br> ダイヤリング
                            </p>
                            <p class="itemdetail">Pt950
                                <br> カラット：0.30ct
                                <br> カラー：E
                                <br> クラリティ：VVS2
                                <br> カット：VG
                            </p>
                            <hr>
                            <p> <span class="red">A社</span>：200,000円
                                <br>
                                <span class="blue">B社</span>：210,000円 </p>
                        </div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price"><span class="small">地金＋</span>230,000<span class="small">円</span></p>
                        </div>
                        <div class="sagaku">
                            <p><span class="small">買取差額“最大”</span>30,000円</p>
                        </div>
                    </li> -->

			<?php
            foreach($resultLists as $list):
            // :: で分割
            $listItem = explode('::', $list);

          ?>
			<li class="box-4">
				<div class="title">
					<p class="bx_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/item/dia/lp/<?php echo $listItem[0]; ?>" alt=""></p>
					<p class="itemName"> <?php echo $listItem[1]; ?> </p>
					<hr>
					<p> <span class="red">A社</span>： <?php echo $listItem[2]; ?>円 <br>
						<span class="blue">B社</span>： <?php echo $listItem[3]; ?>円 </p>
				</div>
				<div class="box-jisseki-cat">
					<h3>買取価格例</h3>
					<p class="price"> <?php echo $listItem[4]; ?><span class="small">円</span></p>
				</div>
				<div class="sagaku">
					<p><span class="small">買取差額“最大”</span> <?php echo $listItem[5]; ?>円</p>
				</div>
			</li>
			<?php endforeach; ?>
		</ul>
		<section class="dia_about" id="l01">
			<h3>ダイヤモンド４Cと査定について</h3>
			<h4>4Cとは</h4>
			<h5 class="bold">ダイヤモンドの品質を評価する基準として、G.I.Aの開発した”4C”と呼ばれるCut（カット）Carat（カラット）Color（カラー）Clarity（クラリティ）の４つの要素で評価します。</h5>
			<div class="dia_det_box">
				<p>・最初アメリカの宝石教育機関ＧＩＡが独自に開発したものですが、このほかにもスカンジナビア方式、Ｃ　ＩＢＪＯ 方式、ＨＲＤ方式などいくつか方式があります。<br />
					国際的に統一しようという動きもありますが、まだ合意点に達してはおりません。現在日本ではＧＩＡ方　式が最も　広く通用しており、また国際的にもこの方式が広く受け入れられております。<br />
					<br />
					・当店ではＧＩＡ方式を採用しております。<br />
					<br />
					・４Ｃの評価で価格がすべて決まるわけではなく、その他さまざまな要素（産出国や品質を保証している機　関など）を考慮し、価格評価をいたします。<br />
					　４Ｃというのは、価格評価するための一つの目安としてお考えください。</p>
			</div>
			<h5 class="bold">Carat（カラット）：重量</h5>
			<div class="dia_det_box">
				<p>ダイヤモンドやその他のだいたいの宝石の重量はメトリック・カラットを単位として測定し、記号（ＣＴ）が用意されています。
					１メトリック・カラットとは、0.200ｇで、1ｇは5ｃｔです。通常、電子デジタル天秤を用いて千分の１カラットまで測定し、少数３位を８捨９入するか、第３位まで表示します。<br />
					原石のほとんどは、１ｃｔ未満の小粒な石です。大粒の石はまれにしか取れず大変希少です。この種の原石からブリリアントカットなどカッティングするわけですから基本的に、大きいほど価値が高くなる傾向があります。<br />
					4Cのその他の規準が同じグレードでしたら、重量が大きいほど評価が高くなります。</p>
				<p><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/dia/dia_cont01.jpg" alt="カラット一覧"></p>
			</div>
			<h5 class="bold">Color（カラー）</h5>
			<div class="dia_det_box">
				<p>宝石品質のダイヤモンドの大半は、わずかな色が認められており、無色にどれだけ近いか、離れているかが評価の基準となります。<br />
					無色に近ければ近いほど希少なため評価は高くなります。<br />
					色のグレードは、ＧＩＡのマスターストーン（基準石）と比較し、黄色の濃さに応じてランク分けされます。<br />
					ダイヤモンドのカラーグレードは最高ランク「Ｄ」：無色透明から最低ランク「Z」：黄色までにランク分けされています。※ファンシーカラーダイヤモンド・・・「Z」カラー以下の有色のダイヤモンドでも、例外として無色透明よりも価値がつく場合があり、別の分類評価をします。
					下記の図は一例となります。 </p>
				<p><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/dia/dia_cont02.jpg" alt="カラー"></p>
			</div>
			<h5 class="bold">Clarity（クラリティ）</h5>
			<div class="dia_det_box">
				<p>内部の特徴（インクルージョン）と外部の特徴（ブレミッシュ）とに分類して行われています。外部特徴はカット傷などの有無で判断いたします。<br />
					内部特徴としては、色やインクルージョンの大きさや位置などを１０倍拡大という条件で観察してグレードを11段階で決定いたします。<br />
					内包物が少なければ少ないほど、希少価値が高くなります。<br />
					<br />
					評価が高い順に、フローレス（FL）、インターナリーフローレス（IF）、ベリーベリースライトリ―インクル―デッド（VVS1およびVVS2）、ベリースライトリ―インクル―デッド（VS1およびVS2）、
					スライトリ―インクル―デッド（SI1およびSI２） 、インクル―デッド（I1、I2、I3）となります。

					下記の図は一例となります。 </p>
				<p><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/dia/dia_cont03.jpg" alt="クラリティ" ></p>
			</div>
			<h5 class="bold">Cut（カット）</h5>
			<div class="dia_det_box">
				<p>カットは人間が美しさに関与できる唯一の要素です。<br />
					輝きが十分に発揮される適切なカットは、外見に影響するわけですから石の価値を決めるうえでの重要な要素となります。ダイヤモンドのプロポーションとフィニッシュ（対称性、研磨状態）を元に総合的に評価します。<br />
					EXCELLENT（エクセレント）・VERY GOOD（ベリーグッド）・GOOD（グッド）・FAIR（フェア）・POOR（プア）の五段階評価です。 <br />
					カット・グレードは個々のプロモーションではなく、総合的な総合的な外見や品質で適切に判断されるプロモーションの組み合わせに基づくので <br />
					同じカット・グレードのダイヤモンドでも選択肢の数が増えることになります。したがって、自身の目で見て好ましいと思える石を選ぶことが重要です。<br />
					<br />
					・リカットとポリッシュについて<br />
					リカットとはもう一度カットをし直すことです。ポリッシュとは、研磨のことを指します。<br />
					エッジがたっていて、表面に傷がないものは手直しをする必要がないので価値が高くなります。<br />
					逆にエッジがだれていたり表面に傷がありくすんでいるものは手直しの必要があるので価値が下がってしまいます。 </p>
				<p style="text-align:center;"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/dia/dia_cont04.jpg" alt="クラリティ" width="70%"></p>
			</div>
			<h5 class="bold">蛍光性</h5>
			<div class="dia_det_box">
				<p>天然ダイヤモンドの特性の一つに「蛍光性」と呼ばれるものがあり、紫外線（ブラックライト）を照射すると紫外線を吸収して発光するダイヤモンドが存在します。<br />
					天然ダイヤモンドの約60%に見られる自然の特徴ではありますが蛍光性が強くなるにつれて自然光下では白く濁ってしまいます。<br />
					鑑定書にはNONE（無し）FAINT（弱い）MEDIUM（中）STRONG（強い）VERY STRONG（かなり強い）と蛍光性の欄に記載されています。Noneが一番蛍光性が弱く、次にFaint、Medium Blue、Strong Blueそして最も蛍光性の強いものがVery Strong Blueとなり、蛍光性が少なく弱い方が価値は高くなる傾向があります。<br />
					稀にブルーやグリーン、イエロー・オレンジなどのカラーが入った蛍光性も見受けられます。（例：FAINT BLUE）<br />
					<br />
					・ダイヤモンドの蛍光反応と価格評価の関係性について<br />
					強い青色の蛍光は石の黄色味を打ち消す働きがあります。このことは、カラー・グレーディングにあたって考慮されておりますので、この欄は、ダイヤモンドの性質を記載しているだけだと理解ください。<br />
					<br />
					・蛍光性と価格評価に関しては諸説あり、お店により判断が異なるケースが多いです。<br />
					・小売店によってはベリーストロングを意図的に省いているお店もあります。 </p>
				<p><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/dia/dia_cont05.jpg" alt="クラリティ"></p>
			</div>
		</section>
				<section class="dia_btm_cont">
			<h3>BRAND REVALUEの強み</h3>
			<div class="btm_cont_wrap">
				<h4><span class="red_bold">高額買取</span>できる<br /><span class="bl_bold">理由</span>があります</h4>
				<div class="btm_inner">
					<h5>Reason1</h5>
					<div class="btm_left">
						<p class="blue_tx">国内外を問わず豊富な販売チャネルを確保</p>
						<p class="btm_det">国内外問わず多くの市場開拓を行い、豊富な販売ルートを確保しております。<br />
							<br />
							多くの販売網を構築することで、高価買取は実現できると考えております。弊社は、宝石専門で買取を行っているわけではございません。<br />
							しかし、販売ルートの確保・宝石知識の向上を念頭に社内全体でダイヤモンドをはじめ、宝石買取を強化して参りました。そのため、宝石買取専門店にも負けない高価買取に自信があります。 </p>
					</div>
					<p class="btm_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/dia/dia_btm01.png" alt="クラリティ"></p>
				</div>
				<div class="btm_inner">
					<h5>Reason2</h5>
					<div class="btm_left">
						<p class="blue_tx">国内外のバイヤーと連携</p>
						<p class="btm_det">ダイヤモンドの４Cにより国内外の需要は変わってきます。<br />
							また、バイヤー単位で抱えている顧客様は、様々であり、ニーズは多種多様です。<br />
							国内外のバイヤーと連携を取り、日々、弊社の顧客さまのみならず、対外的にニーズを把握することにより、業界最高峰の買取価格を実現いたしました。適材適所を心掛け、ニーズのキャッチに注力しております。 </p>
					</div>
					<p class="btm_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/dia/dia_btm02.png" alt="クラリティ"></p>
				</div>
				<div class="btm_inner">
					<h5>Reason3</h5>
					<div class="btm_left">
						<p class="blue_tx">スタッフ教育の徹底</p>
						<p class="btm_det">ブランドリバリューでは、お客さまにご満足いただけるよう日々スタッフ教育に力を注いでおります。<br />
							特にお持ちいただいた宝石やジュエリーアクセサリーに鑑定書がないときには、バイヤーにより査定金額が違ってしまうケースが多々あります。<br />
							<br />
							弊社では、そうしたことがないようスタッフ全員がしっかりとダイヤモンドの価値を正確に把握できるよう、週1回の宝石勉強会に加えて国内外の市場見学やジュエリーショー見学など教育体制を整えております。スタッフ全員がお客さま満足度NO１の買取店を目指しております！</p>
					</div>
					<p class="btm_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/img/lp/dia/dia_btm03.png" alt="クラリティ"></p>
				</div>
			</div>
		</section>

	</div>
				<ul class="dia_nav">
		<li><a href="<?php echo home_url('/cat/diamond/diamond-qa'); ?>">ダイヤモンド 買取Q&A</a></li>
		<li><a href="<?php echo home_url('cat/diamond/diamond-term'); ?>">ダイヤモンド 用語集</a></li>
		<li><a href="<?php echo home_url('/cat/diamond/diamond-app'); ?>">ダイヤモンド 鑑定書</a></li>
		<li><a href="<?php echo home_url('/cat/diamond/diamond-voice'); ?>">お客様の声</a></li>
		<li><a href="<?php echo home_url('/cat/diamond/diamond-staff'); ?>">査定士紹介</a></li>
	</ul>
	<!-- lp_main -->
</div>
<!-- #primary -->
<script data-src="//kaitorisatei.info/brandrevalue/wp-content/themes/bring/js/gaisan.js" type="text/javascript"></script>
<?php

get_footer();
