<?php
get_header(); ?>

<div class="mv_area ">
<img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kv-purchase2.png " alt="BRAMD REVALUE ">
</div>
<section id="mailform">
<div class="">

<!--
<div role="form" class="wpcf7" id="wpcf7-f66-o1" lang="ja" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/purchase1#wpcf7-f66-o1" method="post" class="wpcf7-form form-horizontal" enctype="multipart/form-data" novalidate>
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="66" />
<input type="hidden" name="_wpcf7_version" value="4.3.1" />
<input type="hidden" name="_wpcf7_locale" value="ja" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f66-o1" />
<input type="hidden" name="_wpnonce" value="3dcf703af9" />
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div>
<div class="form-group form-name">
<label class="control-label col-sm-3"><span class="req">必須</span>お名前</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap sei"><input type="text" name="sei" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control span2" aria-required="true" aria-invalid="false" placeholder="お名前" /></span></div>
</div>
<div class="form-group form-name">
<label class="control-label col-sm-3">フリガナ</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap kana-sei"><input type="text" name="kana-sei" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control input-mini" aria-required="true" aria-invalid="false" placeholder="フリガナ" /></span></div>
</div>
<div class="form-group form-email">
<label class="control-label col-sm-3">電話番号</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap email"><input type="tel" name="tel" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group form-email">
<label class="control-label col-sm-3"><span class="req">必須</span>メールアドレス</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group form-email">
<label class="control-label col-sm-3"><span class="req">必須</span>確認のためもう一度</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap email-check"><input type="email" name="email-check" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" aria-required="true" aria-invalid="false" /></span></div>
</div>
<div class="form-group form-file">
<label class="control-label col-sm-3"><span class="req">必須</span>添付ファイル</label>
<div class="col-sm-9"><span class="wpcf7-form-control-wrap file1"><input type="file" name="file1" size="40" class="wpcf7-form-control wpcf7-file wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span><span class="wpcf7-form-control-wrap file2"><input type="file" name="file2" size="40" class="wpcf7-form-control wpcf7-file" aria-invalid="false" /></span><span class="wpcf7-form-control-wrap file3"><input type="file" name="file3" size="40" class="wpcf7-form-control wpcf7-file" aria-invalid="false" /></span></div>
</div>
<div class="form-group form-check">
<label class="control-label col-sm-3">メルマガ配信</label>
<input type="checkbox" name="riyu" value="1">
</div>
<div class="form-group form-text">
<label class="control-label col-sm-3">備考欄</label>
<div class="col-sm-9">
<p><span class="wpcf7-form-control-wrap bikou"><textarea name="bikou" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea form-control" aria-invalid="false"></textarea></span></p>
</div>
</div>
<div class="text-center">
<input type="image" value="リセット" class="" src="<?php echo get_s3_template_directory_uri() ?>/images/reset_btn.png" />
<input type="image" value="送信" class="wpcf7-form-control" src="<?php echo get_s3_template_directory_uri() ?>/images/sub_btn.png" />
</div>
</form>
</div>
-->

<?php echo do_shortcode('[contact-form-7 id="67" title="買取査定（写メ）"]'); ?>

</div>
</section>


<?php
  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
