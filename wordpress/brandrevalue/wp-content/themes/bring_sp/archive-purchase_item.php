<?php
get_header(); ?>

<style>
    .wp-pagenavi{
        display: none;
    }
</style>

<ul class="archive_purchase">
<?php query_posts(
  array(
  'post_type' => 'purchase_item',
  'taxonomy' => 'purchase_item',
  'posts_per_page' => 100,
  'paged' => $paged,
  ));
  if (have_posts()): while ( have_posts() ) : the_post();
  ?>
<li><a href="<?php the_permalink(); ?>">


    <p><?php the_field('kaitori-method'); ?></p>
    <p><?php the_field('kaitori-date'); ?></p>
    <p><img data-src="<?php the_field('kaitori-img'); ?>" alt="<?php the_title(); ?>"></p>
    <p><?php the_title(); ?></p>
    <p>買取価格</p>
    <P> ¥<?php the_field('kaitori-price'); ?></P>
</a></li>
    <?php endwhile; endif; wp_pagenavi(); wp_reset_query(); ?>
</ul>




<?php
  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');
  ?>

  <?php

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
