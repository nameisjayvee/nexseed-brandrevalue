<?php
get_header(); ?>

<div class="mv_area ">
<img data-src="<?php echo get_s3_template_directory_uri() ?>/images/kv-syutchou.png " alt="BRAMD REVALUE ">
</div>
<div class="kaitori_cnt">
<h2 class="kaitori_tl">家から一歩も出ずにスピード査定を受けられる、楽ちんサービス！</h2>
<p class="cnt_tx01">東京近郊にお住まいで、「品物を店舗まで運んだり、郵送の手続きをする時間がない」という方にお勧めしたいのが、出張買取サービスです。お申込み後「最短即日」でBRAND REVALUEの鑑定スタッフがお客様のご自宅までお伺いし、その場でお品物をスピード査定。金額にご納得いただければその場で現金をお渡しいたします。出張買取は家から一歩も出ることなく、店舗と同様のスピーディな買取サービスをご利用いただける、「一番楽ちん」なスタイルです。</p>
<div class="cnt01">
<h3 class="cnt_tl"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cnt_tl_shutchou01.png" alt="交通費も送料も、面倒な梱包の準備も必要ありません"></h3>
<p class="cnt_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cnt_shutchou01_img.png" alt="#"></p>
<p class="cnt_tx">「店頭まで行くのは面倒だし、無駄な交通費をかけたくない」――そんな効率重視の方にピッタリな宅配買取。BRAND REVALUEの宅配買取は、お品物を郵送でお送りいただくだけで、査定・買取を行う便利なサービスです。しかも、送料は当社が全額負担いたしますし、郵送に必要な箱等の宅配セットもご希望の方には無料でご提供！　無駄な時間もお金も全く使わずに品物を納得の価格で換金することができる、人気の買取方法です。</p>
</div>

<div class="cnt02">
<h3 class="cnt_tl"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cnt_tl_shutchou02.png" alt="とっても簡単、宅配の流れ"></h3>
<p class="cnt_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cnt_shutchou02_img.png" alt="#"></p>
<p class="cnt_tx">出張買取サービスをご希望の方は、まずは当WEBサイトの申し込みフォーム（http://brand.kaitorisatei.info/）またはお電話（0120-970-060）よりお申込みください。出張買取の対象エリアは原則東京23区内となっておりますので、それ以外の場合は別途お問い合わせください。ご希望のご訪問日を調整後、最短即日で鑑定スタッフがご自宅へ伺います。査定は1品約3分とスピーディに行い、金額にご納得いただければその場で現金をお支払いいたします。</p>
</div>

<div class="cnt03">
<h3 class="cnt_tl"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cnt_tl_shutchou03.png" alt="ご用意頂きたいもの、ご用意頂きたいこと"></h3>
<p class="cnt_img"><img data-src="<?php echo get_s3_template_directory_uri() ?>/images/cnt_shutchou03_img.png" alt="#"></p>
<p class="cnt_tx">出張買取サービスをご利用いただく際には、お品物の査定を依頼するご本人の身分証明書をご用意いただく必要がございます。身分証明書としては、運転免許証、パスポート、保険証、住民基本台帳カード（住基カード）のいずれかをご用意下さい。いずれもコピーでもOKです。お振込の希望の場合、お振込み先情報につきましては、BRAND REVALUEにて用紙を用意いたしますので、そちらにご記入ください。</p>
</div>
<p class="kaitori_tl">下記をご用意下さい。</p>
<table>
<tr>
<th>お品物</th>
<td>買い取りしてほしい商品をご用意ください。</td>
</tr>
<tr>
<th>身分証明</th>
<td>運転免許証、保険証、パスポートなど身分を 証明できるもの
</td>
</tr>
</table>
<p class="kaitori_at">※買い取り希望ブランドアイテムの保証書、鑑定書、箱や付属品などもあわせて持ってきてください。
<br> ※未成年の方に関しましては、保護者からの同意書、もしくは同伴が必要です。
</p>
</div>
      <section id="mailform">
	  	  <h2 class="obi_tl" style="margin:30px 0;">出張査定お申込みフォーム</h2>

        <div class="mailform_new">
          <?php echo do_shortcode('[mwform_formkey key="23204"]'); ?>
        </div>
      </section>


<?php
  // お問い合わせ
  get_template_part('_action');

  // 3つのポイント
  get_template_part('_purchase');

  // お問い合わせ
  get_template_part('_action2');
  ?>
  <section class="kaitori_voice">
<h3>お客様の声</h3>
<ul>
<li>
<p class="kaitori_tab syu_tab">出張買取</p>
<h4>パテックフィリップ　時計　</h4>
<p class="voice_txt">ホームページを拝見し買取実績の多さと買取価格の高さから、ブランドリバリューを利用させてもらうことにしました。<br />
直接、ブランドリバリューに持ち込もうと思ったのですが、家が遠方なので、出張買取を選ばせていただきました。<br />
初めて利用させてもらいましたが、高額買取をしてもらえてとても満足することができましたし、何より査定士の方がこちらの質問にも丁寧にお答えくださり、本当に安心してお任せすることができました。<br />
大切にしていたパティックフィリップの時計だったので、いいお店で高額買取をしてもらえて本当に大満足です。<br />
また、機会がありましたら、ブランドリバリューを利用したいと思っているので、その時はよろしくお願いいたします。</p>
</li>

<li>
<p class="kaitori_tab syu_tab">出張買取</p>
<h4>ルイヴィトン　バッグ　</h4>
<p class="voice_txt">若いころ、ルイヴィトンが大好きだったので定番のモノグラムをはじめ、ダミエ・エベヌやダミエ・アズールなど、いくつも購入していました。<br />
ですが今は別のブランドで気に入っているものがあるので、クローゼット整理のためこれらのルイヴィトンのバッグをまとめて査定に出すことにしたんです。<br />
それなりの数があり、商品を店舗に持っていき買取をしてもらうのはとても面倒だったので、出張買取で買取をしてもらうことにしました。
他社比較を自分なりにした結果、ブランドリバリューさんが一番高額の査定が期待できそうだと思ったので今回はこちらにお世話になることにしました。<br />
出張買取で、実際に査定をしてもっうとやはり予想通り、高値での買取をしてもらえたので本当に嬉しかったです。
クローゼットもすっきりしましたし、この買取で得たお金もまた新しいブランド物に充てることができるので、本当に満足です。</p>
</li>
<li>
<p class="kaitori_tab syu_tab">出張買取</p>
<h4>カルティエ　ネックレス</h4>
<p class="voice_txt">以前、ハリーウィンストンのネックレスを購入したのですが、子どもを出産し、身に着ける機会がなくなってしまいました。<br />
日常生活で身に着けるのもありかなと思ったのですが、やはり子供が引っ張ってしまうので・・・。<br />
なので、思い切ってハリーウィンストンのネックレスを手放そうと決めたので買取査定を依頼することにしました。<br />
ですが子供が小さいので、直接出向いて買取査定をするのは難しかったので、出張買取という方法で、買取をしてもらうことにしたんです。<br />
最初は、自分から出向かないなんて申し訳ないな・・・なんて思っていたのですが、ブランドリバリューさんは本当に親切な対応で出張買取をしてくださったのでとても良かったです。
やはり気持ちの良い対応をしていただけると、買取を依頼する側もとてもうれしくなりますよね。
査定額も満足することができましたし、本当に良い出張買取をしてもらえたなと思いました。</p>
</li>


</ul>


</section>

<script src="http://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script type="text/javascript">
(function () {

    "use strict";

    /**
     * Pardot form handler assist script.
     * Copyright (c) 2018 toBe marketing, inc.
     * Released under the MIT license
     * http://opensource.org/licenses/mit-license.php
     */

    // == 設定項目ここから ==================================================================== //

    /*
     * ＠データ取得方式
     */
    var useGetType = 1;

    /*
     * ＠フォームハンドラーエンドポイントURL
     */
    var pardotFHUrl="//go.pardot.com/l/436102/2018-01-18/dgrtp7";

    /*
     * ＠フォームのセレクタ名
     */
    var parentFormName = '#mw_wp_form_mw-wp-form-23204 > form';

    /*
     * ＠送信ボタンのセレクタ名
     */
    var submitButton = 'input[type=submit][name=confirm]';

    /*
     * ＠項目マッピング
     */

    var defaultFormJson=[
    { "tag_name": "sei",               "x_target": 'sei'               },
    { "tag_name": "mei",               "x_target": 'mei'               },
    { "tag_name": "kana-sei",          "x_target": 'kana-sei'          },
    { "tag_name": "kana-mei",          "x_target": 'kana-mei'          },
    { "tag_name": "email",             "x_target": 'email'             },
    { "tag_name": "tel",               "x_target": 'tel'               },
    { "tag_name": "address1",          "x_target": 'address1'          },
    { "tag_name": "address2",          "x_target": 'address2'          },
    { "tag_name": "address3",          "x_target": 'address3'          },
    { "tag_name": "address4",          "x_target": 'address4'          },
    { "tag_name": "address5",          "x_target": 'address5'          },
    { "tag_name": "contact_means",     "x_target": 'contact_means'     },
    { "tag_name": "request_date1",     "x_target": 'request_date1'     },
    { "tag_name": "request_date2",     "x_target": 'request_date2'     },
    { "tag_name": "request_time1",     "x_target": 'request_time1'     },
    { "tag_name": "request_time2",     "x_target": 'request_time2'     },
    { "tag_name": "request_bland",     "x_target": 'request_bland'     },
    { "tag_name": "number_of_request", "x_target": 'number_of_request' },
    { "tag_name": "bikou",             "x_target": 'bikou'             }
    ];

    // == 設定項目ここまで ==================================================================== //

    // 区切り文字設定
    var separateString = ',';

    var iframeFormData = '';
    for (var i in defaultFormJson) {
        iframeFormData = iframeFormData + '<input id="pd' + defaultFormJson[i]['tag_name'] + '" type="text" name="' + defaultFormJson[i]['tag_name'] + '"/>';
    }

    var iframeHeadSrc =
        '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';

    var iframeSrcDoc =
        '<form id="shadowForm" action="' + pardotFHUrl + '" method="post" accept-charset=\'UTF-8\'>' +
        iframeFormData +
        '<input id="shadowSubmit" type="submit" value="send" onclick="document.charset=\'UTF-8\';"/>' +
        '</form>';

    var shadowSubmited = false;
    var isError = false;
    var pdHiddenName = 'pdHiddenFrame';

    /**
     * エスケープ処理
     * @param val
     */
    function escapeSelectorString(val) {
        return val.replace(/[ !"#$%&'()*+,.\/:;<=>?@\[\\\]^`{|}~]/g, "\\$&");
    }

    $(function () {

        $(submitButton).click(function () {
            // Submitボタンを無効にする。
            $(submitButton).prop("disabled", true);
        });

        $('#' + pdHiddenName).remove();

        $('<iframe>', {
            id: pdHiddenName
        })
            .css({
                display: 'none'
            })
            .on('load', function () {

                if (shadowSubmited) {
                    if (!isError) {

                        shadowSubmited = false;

                        // 送信ボタンを押せる状態にする。
                        $(submitButton).prop("disabled", false);

                        // 親フォームを送信
                        $(parentFormName).submit();
                    }
                } else {
                    $('#' + pdHiddenName).contents().find('head').prop('innerHTML', iframeHeadSrc);
                    $('#' + pdHiddenName).contents().find('body').prop('innerHTML', iframeSrcDoc);

                    $(submitButton).click(function () {
                        shadowSubmited = true;
                        try {
                            for (var j in defaultFormJson) {
                                var tmpData = '';

                                // NANE値取得形式
                                if (useGetType === 1) {
                                    $(parentFormName + ' [name="' + escapeSelectorString(defaultFormJson[j]['x_target']) + '"]').each(function () {

                                        //checkbox,radioの場合、未選択項目は送信除外
                                        if (["checkbox", "radio"].indexOf($(this).prop("type")) >= 0) {
                                            if ($(this).prop("checked") == false) {
                                                return true;
                                            }

                                        }

                                        if (tmpData !== '') {
                                            tmpData += separateString;
                                        }

                                        // 取得タイプ 1 or Null(default) :val()方式, 2:text()形式
                                        if (defaultFormJson[j]['x_type'] === 2) {
                                            tmpData += $(this).text().trim();
                                        } else {
                                            tmpData += $(this).val().trim();
                                        }
                                    });
                                }

                                // セレクタ取得形式
                                if (useGetType === 2) {
                                    $(defaultFormJson[j]['x_target']).each(function () {
                                        if (tmpData !== '') {
                                            tmpData += separateString;
                                        }

                                        // 取得タイプ 1 or Null(default) :val()方式, 2:text()形式
                                        if (defaultFormJson[j]['x_type'] === 2) {
                                            tmpRegexData = $(this).text().trim();
                                        } else {
                                            tmpRegexData = $(this).val().trim();
                                        }
                                    });
                                }

                                $('#' + pdHiddenName).contents().find('#pd' + escapeSelectorString(defaultFormJson[j]['tag_name'])).val(tmpData);

                            }

                            $('#' + pdHiddenName).contents().find('#shadowForm').submit();

                        } catch (e) {
                            isError = true;

                            $(submitButton).prop("disabled", false);

                            $(parentFormName).submit();
                            shadowSubmited = false;
                        }
                        return false;
                    });
                }
            })
            .appendTo('body');
    });
})();
</script>

  <?php

  // 店舗
  get_template_part('_shopinfo');

  // フッター
  get_footer();
