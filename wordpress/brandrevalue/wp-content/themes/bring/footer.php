<?php
    /**
     * The template for displaying the footer.
     *
     * Contains the closing of the #content div and all content after.
     *
     * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
     *
     * @package BRING
     */

?>
    </div>
    <!-- #content -->

    <footer id="colophon" class="site-footer clearfix" role="contentinfo">
        <div class="container">
            <div class="site-info">
                <a href="<?php echo home_url(''); ?>">
                    <img src="<?php echo get_s3_template_directory_uri() ?>/img/logo-footer.png" alt="高額ブランド買取専門サイト BRAND REVALUE"></a>
                    <p class="action_btn"><a href="<?php echo home_url('customer'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/action-app.png" alt="今すぐ査定申込"></a></p>
            </div>

            <div class="footerSiteMap clearfix">
                <dl>
                    <dt><a href="<?php echo home_url(''); ?>">トップ</a></dt>
                    <dt><a href="<?php echo home_url('service'); ?>">サービスについて</a></dt>
                    <dt><a href="<?php echo home_url('brand'); ?>">取扱ブランド</a></dt>
                    <dt><a href="<?php echo home_url('cat'); ?>">取扱カテゴリ</a></dt>
                    <dt><a href="<?php echo home_url('introduction'); ?>">実績紹介</a></dt>
                </dl>
                <dl>
                    <dt><a href="<?php echo home_url('purchase'); ?>">買取方法</a></dt>
                    <dd>
                        <ul class="list-unstyled">
                            <li><a href="<?php echo home_url('about-purchase/takuhai'); ?>">宅配買取</a></li>
                            <li><a href="<?php echo home_url('about-purchase/syutchou'); ?>">出張買取</a></li>
                            <li><a href="<?php echo home_url('about-purchase/tentou'); ?>">店頭買取</a></li>
                        </ul>
                    </dd>
                </dl>
                <dl>
                    <dt>買取対象品</dt>
                    <dd>
                        <ul class="list-unstyled">
                            <li><a href="<?php echo home_url('cat/gold'); ?>">金・プラチナ</a></li>
                            <li><a href="<?php echo home_url('cat/gem'); ?>">宝石・ジュエリー</a></li>
                            <li><a href="<?php echo home_url('cat/watch'); ?>">ブランド時計</a></li>
                            <li><a href="<?php echo home_url('cat/bag'); ?>">ブランドバッグ・鞄</a></li>
                            <li><a href="<?php echo home_url('cat/outfit'); ?>">洋服・毛皮</a></li>
                            <li><a href="<?php echo home_url('cat/wallet'); ?>">ブランド財布</a></li>
                            <li><a href="<?php echo home_url('cat/shoes'); ?>">ブランド靴</a></li>
                            <li><a href="<?php echo home_url('cat/diamond'); ?>">ダイヤモンド</a></li>
                        </ul>
                    </dd>
                </dl>
                <dl>
                    <dt><a href="<?php echo home_url('voice'); ?>">お客様の声</a></dt>
                    <dt><a href="<?php echo home_url('line'); ?>">LINE査定案内</a></dt>
                    <dt><a href="<?php echo home_url('privacy'); ?>">ご利用規約一覧</a></dt>
                    <dt><a href="<?php echo home_url('company'); ?>">会社概要</a></dt>
                    <dt><a href="<?php echo home_url('recruit'); ?>">採用情報</a></dt>
                </dl>
                <dl>
                    <dt><a href="<?php echo home_url('contact'); ?>">お問い合わせ</a></dt>
                    <dd>
                        <ul class="list-unstyled">
                            <li><a href="<?php echo home_url('purchase/visit-form'); ?>">来店予約お申込み</a></li>
                            <li><a href="<?php echo home_url('purchase3'); ?>">宅配買取お申込み</a></li>
                            <li><a href="<?php echo home_url('purchase/syutchou'); ?>">出張買取お申込み</a></li>
                            <li><a href="<?php echo home_url('purchase1'); ?>">メール査定お申込み</a></li>
                            <li><a href="<?php echo home_url('line'); ?>">LINE査定</a></li>
                        </ul>
                        <div class="footer_tel">
                            <p>電話番号</p>
                            <p><i class="fa fa-phone-square"></i><a href="tel:0120-970-060">0120-970-060</a></p>
                        </div>
                    </dd>
                </dl>
            </div>

        </div>
        <p class="copyright">
            <br>
            Copyrights &copy; 2015 高額ブランド買取専門サイト BRAND REVALUE all rights reserved.
        </p>

    </footer>
    <!-- #colophon -->
    <!--<div class="right_banner">
         <a href="<?php echo home_url('contact'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/right_btn.png" alt="24時間査定受付中"></a>
         </div>-->


    <!-- footer固定 -->
    <div class="foot_banner">
        <ul>
            <li class="tel adgain_tel">
                <p>土日・祝日も承ります！</p>
                <dl>
                    <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/foot_tel.png"><span id="phone_number_holder">0120-970-060</span></dt>
                    <dd>【受付時間】11:00 ～ 21:00 ※年中無休</dd>
                </dl>
            </li>
            <li class="web">
                <a href="<?php echo home_url('purchase1'); ?>">

                    <dl>
                        <dt>24時間査定受付中</dt>
                        <dd>今すぐWeb査定申込み</dd>
                    </dl>
                </a>
            </li>
            <li class="line">
                <a href="https://kaitorisatei.info/brandrevalue/line">
                    <dl>
                        <dt>LINEで簡単！</dt>
                        <dd>LINE査定 申し込みはコチラ</dd>
                    </dl>
                </a>
            </li>

            <li class="ft_pch" id="ftbtn01">
                <dl>
                    <dt>スタイル別で選べる!</dt>
                    <dd>買取申し込みはコチラ</dd>
                </dl>
            </li>
            <div id="ftbtn01wrap">
                <ul class="custom_cv_box02 ftcvbox">
                    <li>
                        <a href="<?php echo home_url('purchase/tentou'); ?>">
                            <p>駅前だから便利!</p>
                            <p>店頭買取</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo home_url('purchase/takuhai'); ?>">
                            <p>忙しい方にオススメ!</p>
                            <p>宅配買取</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo home_url('purchase/syutchou'); ?>">
                            <p>点数が多い方にぴったり!</p>
                            <p>出張買取</p>
                        </a>
                    </li>
                </ul>

            </div>
            <li>
                <?php get_search_form(); ?>
            </li>
        </ul>
    </div>
    </div>
    <!-- #page -->

    <?php wp_footer(); ?>
    <script type="text/javascript">
        jQuery(function($) {
            $("input[name='request_date']").datepicker({
                "yearSuffix": "\u5e74",
                "dateFormat": "yy-mm-dd",
                "minDate": "0y+3d",
                "dayNames": ["\u65e5\u66dc\u65e5", "\u6708\u66dc\u65e5", "\u706b\u66dc\u65e5", "\u6c34\u66dc\u65e5", "\u6728\u66dc\u65e5", "\u91d1\u66dc\u65e5", "\u571f\u66dc\u65e5"],
                "dayNamesMin": ["\u65e5", "\u6708", "\u706b", "\u6c34", "\u6728", "\u91d1", "\u571f"],
                "dayNamesShort": ["\u65e5\u66dc", "\u6708\u66dc", "\u706b\u66dc", "\u6c34\u66dc", "\u6728\u66dc", "\u91d1\u66dc", "\u571f\u66dc"],
                "monthNames": ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
                "monthNamesShort": ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
                "showMonthAfterYear": "true",
                "changeYear": "true",
                "changeMonth": "true"
            });
            $("input[name='request_date1']").attr('readonly', true);
        });

    </script>

    <script type="text/javascript">
        jQuery(function($) {
            $("input[name='request_date1']").datepicker({
                "yearSuffix": "\u5e74",
                "dateFormat": "yy-mm-dd",
                "minDate": "0y",
                "dayNames": ["\u65e5\u66dc\u65e5", "\u6708\u66dc\u65e5", "\u706b\u66dc\u65e5", "\u6c34\u66dc\u65e5", "\u6728\u66dc\u65e5", "\u91d1\u66dc\u65e5", "\u571f\u66dc\u65e5"],
                "dayNamesMin": ["\u65e5", "\u6708", "\u706b", "\u6c34", "\u6728", "\u91d1", "\u571f"],
                "dayNamesShort": ["\u65e5\u66dc", "\u6708\u66dc", "\u706b\u66dc", "\u6c34\u66dc", "\u6728\u66dc", "\u91d1\u66dc", "\u571f\u66dc"],
                "monthNames": ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
                "monthNamesShort": ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
                "showMonthAfterYear": "true",
                "changeYear": "true",
                "changeMonth": "true"
            });
            $("input[name='request_date1']").attr('readonly', true);
        });

    </script>
    <script type="text/javascript">
        jQuery(function($) {
            $("input[name='request_date2']").datepicker({
                "yearSuffix": "\u5e74",
                "dateFormat": "yy-mm-dd",
                "minDate": "0y",
                "dayNames": ["\u65e5\u66dc\u65e5", "\u6708\u66dc\u65e5", "\u706b\u66dc\u65e5", "\u6c34\u66dc\u65e5", "\u6728\u66dc\u65e5", "\u91d1\u66dc\u65e5", "\u571f\u66dc\u65e5"],
                "dayNamesMin": ["\u65e5", "\u6708", "\u706b", "\u6c34", "\u6728", "\u91d1", "\u571f"],
                "dayNamesShort": ["\u65e5\u66dc", "\u6708\u66dc", "\u706b\u66dc", "\u6c34\u66dc", "\u6728\u66dc", "\u91d1\u66dc", "\u571f\u66dc"],
                "monthNames": ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
                "monthNamesShort": ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
                "showMonthAfterYear": "true",
                "changeYear": "true",
                "changeMonth": "true"
            });
            $("input[name='request_date2']").attr('readonly', true);
        });

    </script>
    <script type="text/javascript">
        jQuery(function($) {
            $("input[name='request_date3']").datepicker({
                "yearSuffix": "\u5e74",
                "dateFormat": "yy-mm-dd",
                "minDate": "0y",
                "dayNames": ["\u65e5\u66dc\u65e5", "\u6708\u66dc\u65e5", "\u706b\u66dc\u65e5", "\u6c34\u66dc\u65e5", "\u6728\u66dc\u65e5", "\u91d1\u66dc\u65e5", "\u571f\u66dc\u65e5"],
                "dayNamesMin": ["\u65e5", "\u6708", "\u706b", "\u6c34", "\u6728", "\u91d1", "\u571f"],
                "dayNamesShort": ["\u65e5\u66dc", "\u6708\u66dc", "\u706b\u66dc", "\u6c34\u66dc", "\u6728\u66dc", "\u91d1\u66dc", "\u571f\u66dc"],
                "monthNames": ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
                "monthNamesShort": ["1\u6708", "2\u6708", "3\u6708", "4\u6708", "5\u6708", "6\u6708", "7\u6708", "8\u6708", "9\u6708", "10\u6708", "11\u6708", "12\u6708"],
                "showMonthAfterYear": "true",
                "changeYear": "true",
                "changeMonth": "true"
            });
            $("input[name='request_date3']").attr('readonly', true);   
        });

    </script>

    <script src="<?php echo get_template_directory_uri() ?>/js/jquery.tile.js"></script>
    <script>
        $(document).ready(function() {
            $(".box-4 .title p.itemName").tile();
            $(".box-4 .title p.itemdetail").tile(4);
        });
        $(window).load(function() {
            $(".box-4 .title img").tile();
        });

        $(function() {
            $(".menu").mouseover(function() {
                $(".menu").removeClass("hover");
                $(this).addClass("hover");
                $(".content:not('.hover + .content')").fadeOut();
                $(".hover + .content").fadeIn();
            });
        });
        $(function() {
            $(".menu2").mouseover(function() {
                $(".menu2").removeClass("hover2");
                $(this).addClass("hover2");
                $(".content2:not('.hover2 + .content2')").fadeOut();
                $(".hover2 + .content2").fadeIn();
            });
        });
        $(function() {
            $(".menu3").mouseover(function() {
                $(".menu3").removeClass("hover3");
                $(this).addClass("hover3");
                $(".content3:not('.hover3 + .content3')").fadeOut();
                $(".hover3 + .content3").fadeIn();
            });
        });

    </script>

    <script type="text/javascript">
        jQuery(function($) {
            $('#ftbtn01').on('click', function() {
                $("#ftbtn01wrap").slideToggle();

            });

        });

    </script>

    <script type="text/javascript">
        $(function() {
            $('img').hover(function() {
                $(this).attr('src', $(this).attr('src').replace('_off', '_on'));
            }, function() {
                if (!$(this).hasClass('currentPage')) {
                    $(this).attr('src', $(this).attr('src').replace('_on', '_off'));
                }
            });
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".tab_cont").hide();
            $("ul.tab_menu li:first").addClass("active").show();
            $(".tab_cont:first").show();
            $("ul.tab_menu li").click(function() {
                $("ul.tab_menu li").removeClass("active");
                $(this).addClass("active");
                $(".tab_cont").hide();
                var activeTab = $(this).find("a").attr("href");
                $(activeTab).fadeIn();
                return false;
            });
        });

        $(document).ready(function() {
            $(".tab_cont02").hide();
            $("ul.tab_menu02 li:first").addClass("active").show();
            $(".tab_cont02:first").show();
            $("ul.tab_menu02 li").click(function() {
                $("ul.tab_menu02 li").removeClass("active");
                $(this).addClass("active");
                $(".tab_cont02").hide();
                var activeTab = $(this).find("a").attr("href");
                $(activeTab).fadeIn();
                return false;
            });
        });

        $(function(){
            $('.thumb li').click(function(){
                var class_name = $(this).attr("class"); //クリックしたサムネイルのclass名を取得
                var num = class_name.slice(5); //class名の末尾の数字を取得
                $('.shop_main li').hide(); //メインの画像を全て隠す
                $('.item' + num).fadeIn(); //クリックしたサムネイルに対応するメイン画像を表示
            });
        });

    </script>



    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/slick.js"></script>
    <script>$('.archive_purchase.slider').slick({
            autoplay:true,
            autoplaySpeed:3000,
            dots:true,
            slidesToShow:4,
            slidesToScroll:1
        });</script>


    <!-- slider -->
    <script type="text/javascript">
        var wsSetTimer;
        $(function() {
            var $setElm = $('.wideslider'),
                baseWidth = 1040,
                baseHeight = 450,

                slideSpeed = 500,
                delayTime = 5000,
                easing = 'linear',

                autoPlay = '1', // notAutoPlay = '0'

                btnOpacity = 0.6,
                pnOpacity = 0.6;

            $setElm.each(function() {
                var targetObj = $(this);
                var widesliderWidth = baseWidth;
                var widesliderHeight = baseHeight;

                targetObj.children('ul').wrapAll('<div class="wideslider_base"><div class="wideslider_wrap"></div><div class="slider_prev"></div><div class="slider_next"></div></div>');

                var findBase = targetObj.find('.wideslider_base');
                var findWrap = targetObj.find('.wideslider_wrap');
                var findPrev = targetObj.find('.slider_prev');
                var findNext = targetObj.find('.slider_next');

                var baseListWidth = baseWidth;
                var baseListCount = findWrap.children('ul').children('li').length;

                var baseWrapWidth = (baseListWidth) * (baseListCount);

                var pagination = $('<div class="pagination"></div>');
                targetObj.append(pagination);
                var baseList = findWrap.children('ul').children('li');
                baseList.each(function(i) {
                    $(this).css({
                        width: (baseWidth),
                        height: (baseHeight)
                    });
                    pagination.append('<a href="javascript:void(0);" class="pn' + (i + 1) + '"></a>');
                });

                var pnPoint = pagination.children('a');
                var pnFirst = pagination.children('a:first');
                var pnLast = pagination.children('a:last');
                var pnCount = pagination.children('a').length;
                pnPoint.css({
                    opacity: (pnOpacity)
                }).hover(function() {
                    $(this).stop().animate({
                        opacity: '1'
                    }, 500);
                }, function() {
                    $(this).stop().animate({
                        opacity: (pnOpacity)
                    }, 500);
                });
                pnFirst.addClass('active');
                pnPoint.click(function() {
                    if (autoPlay == '1') {
                        clearInterval(wsSetTimer);
                    }
                    var setNum = pnPoint.index(this);
                    var moveLeft = ((baseListWidth) * (setNum)) + baseWrapWidth;
                    findWrap.stop().animate({
                        left: -(moveLeft)
                    }, slideSpeed, easing);
                    pnPoint.removeClass('active');
                    $(this).addClass('active');
                    if (autoPlay == '1') {
                        wsTimer();
                    }
                });

                var makeClone = findWrap.children('ul');
                makeClone.clone().prependTo(findWrap);
                makeClone.clone().appendTo(findWrap);

                var allListWidth = findWrap.children('ul').children('li').width();
                var allListCount = findWrap.children('ul').children('li').length;

                var allLWrapWidth = (allListWidth) * (allListCount);
                var windowWidth = $(window).width();
                var posAdjust = ((windowWidth) - (baseWidth)) / 2;

                findBase.css({
                    left: (posAdjust),
                    width: (baseWidth),
                    height: (baseHeight)
                });
                findPrev.css({
                    left: -(baseWrapWidth),
                    width: (baseWrapWidth),
                    height: (baseHeight),
                    opacity: (btnOpacity)
                });
                findNext.css({
                    right: -(baseWrapWidth),
                    width: (baseWrapWidth),
                    height: (baseHeight),
                    opacity: (btnOpacity)
                });
                $(window).bind('resize', function() {
                    var windowWidth = $(window).width();
                    var posAdjust = ((windowWidth) - (baseWidth)) / 2;
                    findBase.css({
                        left: (posAdjust)
                    });
                    findPrev.css({
                        left: -(posAdjust),
                        width: (posAdjust)
                    });
                    findNext.css({
                        right: -(posAdjust),
                        width: (posAdjust)
                    });
                }).resize();

                findWrap.css({
                    left: -(baseWrapWidth),
                    width: (allLWrapWidth),
                    height: (baseHeight)
                });
                findWrap.children('ul').css({
                    width: (baseWrapWidth),
                    height: (baseHeight)
                });

                var posResetNext = -(baseWrapWidth) * 2;
                var posResetPrev = -(baseWrapWidth) + (baseWidth);

                if (autoPlay == '1') {
                    wsTimer();
                }

                function wsTimer() {
                    wsSetTimer = setInterval(function() {
                        findNext.click();
                    }, delayTime);
                }
                findNext.click(function() {
                    findWrap.not(':animated').each(function() {
                        if (autoPlay == '1') {
                            clearInterval(wsSetTimer);
                        }
                        var posLeft = parseInt($(findWrap).css('left'));
                        var moveLeft = ((posLeft) - (baseWidth));
                        findWrap.stop().animate({
                            left: (moveLeft)
                        }, slideSpeed, easing, function() {
                            var adjustLeft = parseInt($(findWrap).css('left'));
                            if (adjustLeft == posResetNext) {
                                findWrap.css({
                                    left: -(baseWrapWidth)
                                });
                            }
                        });
                        var pnPointActive = pagination.children('a.active');
                        pnPointActive.each(function() {
                            var pnIndex = pnPoint.index(this);
                            var listCount = pnIndex + 1;
                            if (pnCount == listCount) {
                                pnPointActive.removeClass('active');
                                pnFirst.addClass('active');
                            } else {
                                pnPointActive.removeClass('active').next().addClass('active');
                            }
                        });
                        if (autoPlay == '1') {
                            wsTimer();
                        }
                    });
                }).hover(function() {
                    $(this).stop().animate({
                        opacity: ((btnOpacity) + 0.1)
                    }, 100);
                }, function() {
                    $(this).stop().animate({
                        opacity: (btnOpacity)
                    }, 100);
                });

                findPrev.click(function() {
                    findWrap.not(':animated').each(function() {
                        if (autoPlay == '1') {
                            clearInterval(wsSetTimer);
                        }
                        var posLeft = parseInt($(findWrap).css('left'));
                        var moveLeft = ((posLeft) + (baseWidth));
                        findWrap.stop().animate({
                            left: (moveLeft)
                        }, slideSpeed, easing, function() {
                            var adjustLeft = parseInt($(findWrap).css('left'));
                            var adjustLeftPrev = (posResetNext) + (baseWidth);
                            if (adjustLeft == posResetPrev) {
                                findWrap.css({
                                    left: (adjustLeftPrev)
                                });
                            }
                        });
                        var pnPointActive = pagination.children('a.active');
                        pnPointActive.each(function() {
                            var pnIndex = pnPoint.index(this);
                            var listCount = pnIndex + 1;
                            if (1 == listCount) {
                                pnPointActive.removeClass('active');
                                pnLast.addClass('active');
                            } else {
                                pnPointActive.removeClass('active').prev().addClass('active');
                            }
                        });
                        if (autoPlay == '1') {
                            wsTimer();
                        }
                    });
                }).hover(function() {
                    $(this).stop().animate({
                        opacity: ((btnOpacity) + 0.1)
                    }, 100);
                }, function() {
                    $(this).stop().animate({
                        opacity: (btnOpacity)
                    }, 100);
                });
            });
        });

    </script>
    <script type="text/javascript">
        piAId = '437102';
        piCId = '55756';
        piHostname = 'pi.pardot.com';

        (function() {
            function async_load() {
                var s = document.createElement('script');
                s.type = 'text/javascript';
                s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
                var c = document.getElementsByTagName('script')[0];
                c.parentNode.insertBefore(s, c);
            }
            if (window.attachEvent) {
                window.attachEvent('onload', async_load);
            } else {
                window.addEventListener('load', async_load, false);
            }
        })();

    </script>
    </body>

    </html>
