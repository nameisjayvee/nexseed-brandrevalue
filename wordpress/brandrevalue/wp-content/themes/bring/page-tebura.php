<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */
   // 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  'bag009.png::HERMES::エブリン3PM::184,300::180,500::190,000::9,500',
  'bag010.png::PRADA::ハンドバッグ::116,400::114,000::120,000::6,000',
  'bag011.png::GUCCI::バンブーバッグ::29,100::28,500::30,000::30,000',
  'bag012.png::LOUIS VUITTON::モンスリGM::58,200::57,000::60,000::3,000',
  'bag001.jpg::HERMES::バーキン30::1,450,000::1,520,000::1,600,000::150,000',
  'bag002.jpg::CHANEL::マトラッセ チェーンバッグ::100,000::130,000::150,000::50,000',
  'bag003.jpg::LOUIS VUITTON::ダミエ ネヴァーフルMM::92,000::104,000::110,000::18,000',
  'bag004.jpg::CELINE::ラゲージマイクロショッパーバッグ::160,000::175,000::1850,000::25,000',
);

// 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  'watch/top/001.jpg::パテックフィリップ::カラトラバ 3923::646,000::640,000::650,000::10,000',
  'watch/top/002.jpg::パテックフィリップ::アクアノート 5065-1A::1,367,000::1,350,000::1,400,000::50,000',
  'watch/top/003.jpg::オーデマピゲ::ロイヤルオーク・オフショア 26170ST::1,200,000::1,195,000::1,210,000::15,000',
  'watch/top/004.jpg::ROLEX::サブマリーナ 116610LN ランダム品番::720,000::700,000::740,000::40,000',
  'watch/top/005.jpg::ROLEX::デイトナ 116505 ランダム品番 コスモグラフ::1,960,000::1,950,000::2,000,000::50,000',
  'watch/top/006.jpg::ブライトリング::クロノマット44::334,000::328,000::340,000::12,000',
  'watch/top/007.jpg::パネライ::ラジオミール 1940::594,000::590,000::600,000::10,000',
  'watch/top/008.jpg::ボールウォッチ::ストークマン ストームチェイサープロ CM3090C::128,000::125,000::130,000::5,000',
  'watch/top/009.jpg::ブレゲ::クラシックツインバレル 5907BB12984::615,000::610,000::626,000::16,000',
  'watch/top/010.jpg::ウブロ::ビッグバン ブラックマジック::720,000::695,000::740,000::45,000',
  'watch/top/011.jpg::オメガ::シーマスター プラネットオーシャン 2208-50::192,000::190,000::200,000::10,000',
  'watch/top/012.jpg::ディオール::シフルルージュ クロノグラフ CD084612 M001::190,000::180,000::200,000::20,000',
);


get_header(); ?>

	<div id="primary" class="about-purchase content-area">
		<main id="main" class="site-main" role="main">
      <section id="mainVisual" style="background:url(<?php echo get_s3_template_directory_uri() ?>/img/mv/about-purchase-tebura.png)">
        <h2 class="text-hide">店頭郵送買取</h2>
      </section>
      
      <section id="catchcopy">
        <h3>先に郵送！手ぶらで来店、待ち時間無しで査定支払い！</h3>
        <p>BRANDREVALUE(ブランドリバリュー)では、お客様からお品物を店舗までお持ち込み頂かなくても買取が行えます。<br>
まずはお問い合わせをいただければ、こちらで発送の手順とお手続き等ご説明を致します。ご説明後にお客様からBRANDREVALUE(ブランドリバリュー)に商品を送っていただければ経験豊富な鑑定スタッフが確実な査定を致します。<br>
査定が終了次第、お客様にお電話をさせて頂きます。<br>
ご来店の際は身分を証明出来る物だけお持ちいただければ手続きに問題ありません。<br>
またお問い合わせまたはご来店の際には、査定の詳細を細かくお伝え致します。
当然かと思いますが、査定結果にご納得のいかない場合はキャンセルまたは返品等致します。
こちらからの買取金額のお支払に関しましては、ご来店の際に現金でお手続きをさせて頂きますので、お手元に現金に換えたい品がございましたらBRAND REVALUEの店頭郵送買取サービスを是非ご利用下さい。</p>
      </section>
      
      
      <section id="about-purchase-feature1" class="clearfix">
        <h3><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/tebura-feature1.png" alt="重たい荷物も送料も、必要ありません！待ち時間もゼロ！"></h3>
        <div>
          <img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/tebura01.png" class="pull-left">
          <p>「店頭でしっかり鑑定して欲しいけど、重い荷物は持って移動したくない」「時間を有効に使いたい」<br>
そんな効率重視の方にピッタリな店頭郵送買取。BRANDREVALUE(ブランドリバリュー)の店頭郵送買取は、お客様の貴重な時間を無駄にすることなく簡単に店頭買取ができるサービスです。<br>
もちろん、当日の査定時間は、ゼロです。お品物を郵送でお送り頂き、プロのバイヤーがしっかりと査定し、お客様のご要望にお答え致します。しかも、送料は当社が全額負担いたしますし、郵送に必要な箱等の宅配セットもご希望の方には無料でご提供！<br>
宅配買取と同様に、無駄な時間もお金も全く使わずに品物を納得の価格で換金することができる、人気の買取方法です。当日必要なものは、身分証明書のみです。</p>
        </div>
      </section>
      
      <section id="about-purchase-feature2">
        <h3><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/tebura-feature2.png" alt="とっても簡単！店頭郵送買取の流れ"></h3>
        <div>
          <p>まずは当WEBサイトの申し込みフォーム（<a href="http://brand.kaitorisatei.info/">http://brand.kaitorisatei.info/</a>）またはお電話（0120-970-060）よりお申込みください（この際、ご氏名等の必要な情報をお聞きいたします）。<br>
その後、ご希望に応じて宅配キットをお宅まで郵送いたします。お品物の梱包がお済みになりましたら、着払いにてお品物をご郵送ください。お品物がBRAND REVALUEに届きましたらすぐにご連絡いたしますので、来店日時をご予約ください。<br>
ご来店頂きましたら、鑑定スタッフが査定の結果をご説明致します。査定金額にご納得いただけた場合、その場で即現金化、ご納得いただけなかった場合は、お品物を返送致します。もちろん、その場でお持ち帰り頂くことも可能でございます。</p>
        </div>
      </section>
      
      <section id="about-purchase-feature3">
        <h3><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/tentou-feature3.png"></h3>

          <div class="tentou_purchase">
          <dl>
              <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/flow-tebura01.png" alt="1 まずは当店にお問い合わせ"></dt>
              <dd>まずは当店にお問い合わせ下さい。お問い合わせ後に商品を発送します。またお問い合わせの際に、商品の詳細とお客様の情報をお伝え下さい。こちらから発送用の段ボールをお送りすることも可能です。</dd>
          </dl>
          <dl>
              <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/flow-tebura02.png" alt="2 ご来店の日時確認"></dt>
              <dd>商品の到着後。<br>鑑定スタッフよりお客様にご連絡を致しますので、ご来店の日時等教えて頂けると幸いです。
また急なご来店日時の変更も受け付けておりますので気軽にお申し付けください。</dd>
          </dl>
          <dl>
              <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/flow-tebura03.png" alt="3 鑑定スタッフと相対で商品の査定"></dt>
              <dd>お客様ご来店の際。鑑定スタッフと相対で商品の査定を開始致します。身分証明を忘れないようお願い致します。他に必要な物はございません。
専門の鑑定スタッフがお客様に細かく査定の結果をご説明致します。<br>また査定金額にご不満等あればすぐに返送またはその場で返品をさせて頂きます。</dd>
          </dl>
          <dl>
              <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/flow-tebura04.png" alt="4 査定内容確認、支払い"></dt>
              <dd>ご来店にて。鑑定スタッフが査定内容を説明。<br>査定後の買取金額にご納得いただければ買取をさせて頂きます。買取金額に上限は設けておりませんので、その場で即時に現金にてお支払をさせて頂きます。
ただあまりにも高額なお手続きの場合はお支払い方法をご相談させて頂く場合がありますので、まずはお問い合わせ下さい。</dd>
          </dl>
          </div>

        <div>
          <img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/tentou02.png" class="pull-left">
          <p>店頭買取サービスをご利用いただく際には、お品物の査定を依頼するご本人の身分証明書をご用意いただく必要がございます。身分証明書としては、運転免許証、パスポート、保険証、住民基本台帳カード（住基カード）のいずれかをお送りください。いずれもコピーでOKです。お振込み先情報につきましては、BRAND REVALUEにて用紙を用意いたしますので、そちらにご記入いただきお品物とご同梱ください。なお、腕時計等の壊れやすいお品物につきましては、郵送中の故障・破損を未然に防ぐため、緩衝材でしっかりお包みください。また、お品物の点数に誤りがないよう、用紙にご記入いただきますよう、お願いいたします。</p>
        
          <table class="table table-bordered">
            <caption>下記をご用意下さい。</caption>
            <tr><th>お品物</th><td>買取して欲しい商品をご用意ください。</td></tr>
            <tr><th>身分証明書</th><td>運転免許証、保険証、パスポートなど身分を証明できるもの</td></tr>
          </table>
          <p>
            ※買い取り希望ブランドアイテムの保証書、鑑定書、箱や付属品などもあわせて持ってきてください。<br>
            ※未成年の方に関しましては、保護者からの同意書、もしくは同伴が必要です。
          </p>
        </div>
      </section>
	  		<div><a href="<?php echo home_url('purchase/visit-form'); ?>" class="visit_link_btn"><i class="fa fa-angle-double-right faa-horizontal animated"></i>買取来店予約はこちら</a></div>

      
      
      <?php
        // アクションポイント
        get_template_part('_action');
		?>
		<section id="top-jisseki">
			<h2 class="ttl_edit02">ブランドリバリュー店頭郵送買取の買取実績</h2>
			<div class="full_content"> 
				
				<!-- ブランド買取 -->
				<div class="menu hover"><span class="glyphicon glyphicon-tag"></span> ブランド買取</div>
				<div class="content"> 
					<!-- box -->
					<ul id="box-jisseki" class="list-unstyled clearfix">
						<!-- brand1 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/001.jpg" alt="">
								<p class="itemName">オーデマピゲ　ロイヤルオークオフショアクロノ 26470OR.OO.A002CR.01 ゴールド K18PG</p>
								<hr>
								<p> <span class="red">A社</span>：3,120,000円<br>
									<span class="blue">B社</span>：3,100,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">3,150,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>50,000円</p>
							</div>
						</li>
						
						<!-- brand2 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/002.jpg" alt="">
								<p class="itemName">パテックフィリップ　コンプリケーテッド ウォッチ 5085/1A-001</p>
								<hr>
								<p> <span class="red">A社</span>：1,400,000円<br>
									<span class="blue">B社</span>：1,390,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">1,420,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>30,000円</p>
							</div>
						</li>
						
						<!-- brand3 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/003.jpg" alt="">
								<p class="itemName">パテックフィリップ　コンプリケーション 5130G-001 WG</p>
								<hr>
								<p> <span class="red">A社</span>：2,970,000円<br>
									<span class="blue">B社</span>：2,950,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">3,000,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>50,000円</p>
							</div>
						</li>
						
						<!-- brand4 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/004.jpg" alt="">
								<p class="itemName">パテックフィリップ　ワールドタイム 5130R-001</p>
								<hr>
								<p> <span class="red">A社</span>：3,000,000円<br>
									<span class="blue">B社</span>：2,980,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">3,050,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>70,000円</p>
							</div>
						</li>
						
						<!-- brand5 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/005.jpg" alt="">
								<p class="itemName">シャネル　ラムスキン　マトラッセ　二つ折り長財布</p>
								<hr>
								<p> <span class="red">A社</span>：69,000円<br>
									<span class="blue">B社</span>：68,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">70,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,000円</p>
							</div>
						</li>
						
						<!-- brand6 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/006.jpg" alt="">
								<p class="itemName">エルメス　ベアンスフレ　ブラック</p>
								<hr>
								<p> <span class="red">A社</span>：200,000円<br>
									<span class="blue">B社</span>：196,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">211,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>15,000円</p>
							</div>
						</li>
						
						<!-- brand7 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/007.jpg" alt="">
								<p class="itemName">エルメス　バーキン30　トリヨンクレマンス　マラカイト　SV金具</p>
								<hr>
								<p> <span class="red">A社</span>：1,220,000円<br>
									<span class="blue">B社</span>：1,200,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">1,240,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>40,000円</p>
							</div>
						</li>
						
						<!-- brand8 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/008.jpg" alt="">
								<p class="itemName">セリーヌ　ラゲージマイクロショッパー</p>
								<hr>
								<p> <span class="red">A社</span>：150,000円<br>
									<span class="blue">B社</span>：147,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">155,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>8,000円</p>
							</div>
						</li>
						
						<!--brand9 -->
						
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/009.jpg" alt="">
								<p class="itemName">ルイヴィトン　裏地ダミエ柄マッキントッシュジャケット</p>
								<hr>
								<p> <span class="red">A社</span>：31,000円<br>
									<span class="blue">B社</span>：30,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">32,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,000円</p>
							</div>
						</li>
						
						<!-- brand10 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/010.jpg" alt="">
								<p class="itemName">シャネル　ココマーク　ラパンファーマフラー</p>
								<hr>
								<p> <span class="red">A社</span>：68,000円<br>
									<span class="blue">B社</span>：65,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">71,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>6,000円</p>
							</div>
						</li>
						
						<!-- brand11 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/011.jpg" alt="">
								<p class="itemName">ルイヴィトン　ダミエ柄サイドジップレザーブーツ</p>
								<hr>
								<p> <span class="red">A社</span>：49,000円<br>
									<span class="blue">B社</span>：48,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">51,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>3,000円</p>
							</div>
						</li>
						
						<!-- brand12 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/012.jpg" alt="">
								<p class="itemName">サルバトーレフェラガモ　ヴェラリボンパンプス</p>
								<hr>
								<p> <span class="red">A社</span>：9,000円<br>
									<span class="blue">B社</span>：8,500円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">10,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,500円</p>
							</div>
						</li>
					</ul>
				</div>
				
				<!-- 金買取 -->
				<div class="menu"><span class="glyphicon glyphicon-bookmark"></span> 金買取</div>
				<div class="content"> 
					<!-- box -->
					<ul id="box-jisseki" class="list-unstyled clearfix">
						<!-- 1 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/001.jpg" alt="">
								<p class="itemName">K18　ダイヤ0.11ctリング</p>
								<hr>
								<p> <span class="red">A社</span>：23,700円<br>
									<span class="blue">B社</span>：23,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price"><!-- span class="small" >120g</span -->25,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,000円</p>
							</div>
						</li>
						<!-- 2 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/002.jpg" alt="">
								<p class="itemName">K18　メレダイヤリング</p>
								<hr>
								<p> <span class="red">A社</span>：38,000円<br>
									<span class="blue">B社</span>：37,500円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price"><!-- span class="small" >120g</span -->39,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,500円</p>
							</div>
						</li>
						<!-- 3 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/003.jpg" alt="">
								<p class="itemName">K18/Pt900　メレダイアリング</p>
								<hr>
								<p> <span class="red">A社</span>：14,200円<br>
									<span class="blue">B社</span>：14,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">16,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,000円</p>
							</div>
						</li>
						<!-- 4 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/004.jpg" alt="">
								<p class="itemName">Pt900　メレダイヤリング</p>
								<hr>
								<p> <span class="red">A社</span>：22,600円<br>
									<span class="blue">B社</span>：21,200円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">23,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,800円</p>
							</div>
						</li>
						<!-- 5 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/005.jpg" alt="">
								<p class="itemName">K18WG　テニスブレスレット</p>
								<hr>
								<p> <span class="red">A社</span>：24,600円<br>
									<span class="blue">B社</span>：23,800円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">26,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,200円</p>
							</div>
						</li>
						<!-- 6 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/006.jpg" alt="">
								<p class="itemName">K18/K18WG　メレダイヤリング</p>
								<hr>
								<p> <span class="red">A社</span>：22,400円<br>
									<span class="blue">B社</span>：22,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">23,600<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,600円</p>
							</div>
						</li>
						<!-- 7 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/007.jpg" alt="">
								<p class="itemName">K18ブレスレット</p>
								<hr>
								<p> <span class="red">A社</span>：18,100円<br>
									<span class="blue">B社</span>：17,960円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">20,100<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,140円</p>
							</div>
						</li>
						<!-- 8 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/008.jpg" alt="">
								<p class="itemName">K14WGブレスレット</p>
								<hr>
								<p> <span class="red">A社</span>：23,600円<br>
									<span class="blue">B社</span>：22,800円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">24,300<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,500円</p>
							</div>
						</li>
						<!-- 9 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/009.jpg" alt="">
								<p class="itemName">Pt850ブレスレット</p>
								<hr>
								<p> <span class="red">A社</span>：23,600円<br>
									<span class="blue">B社</span>：23,200円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">24,700<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,500円</p>
							</div>
						</li>
						<!-- 10 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/010.jpg" alt="">
								<p class="itemName">Pt900/Pt850メレダイヤネックレス</p>
								<hr>
								<p> <span class="red">A社</span>：29,600円<br>
									<span class="blue">B社</span>：28,400円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">30,500<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,100円</p>
							</div>
						</li>
						<!-- 11 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/011.jpg" alt="">
								<p class="itemName">K18/Pt900/K24ネックレストップ</p>
								<hr>
								<p> <span class="red">A社</span>：43,600円<br>
									<span class="blue">B社</span>：43,100円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">45,900<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,800円</p>
							</div>
						</li>
						<!-- 12 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/012.jpg" alt="">
								<p class="itemName">K24インゴットメレダイヤネックレストップ</p>
								<hr>
								<p> <span class="red">A社</span>：53,700円<br>
									<span class="blue">B社</span>：52,900円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">56,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>3,100円</p>
							</div>
						</li>
					</ul>
				</div>
				
				<!-- 宝石買取 -->
				<div class="menu"><span class="glyphicon glyphicon-magnet"></span> 宝石買取</div>
				<div class="content"> 
					<!-- box -->
					<ul id="box-jisseki" class="list-unstyled clearfix">
						<!--<li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem001.jpg" alt="">
            <p class="itemName">エメラルド</p>
            <hr>
            <p> <span class="red">A社</span>：190,000円<br>
              <span class="blue">B社</span>：194,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>200,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>7,500円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem002.jpg" alt="">
            <p class="itemName">サファイア</p>
            <hr>
            <p> <span class="red">A社</span>：145,500円<br>
              <span class="blue">B社</span>：142,500円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>150,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>10,000円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem003.jpg" alt="">
            <p class="itemName">ルビー</p>
            <hr>
            <p> <span class="red">A社</span>：97,000円<br>
              <span class="blue">B社</span>：95,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>100,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>5,000円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem004.jpg" alt="">
            <p class="itemName">アレキサンドライト</p>
            <hr>
            <p> <span class="red">A社</span>：67,900円<br>
              <span class="blue">B社</span>：66,500円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>70,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>4,500円</p>
          </div>
        </li>--> 
						
						<!-- 1 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/001.jpg" alt="">
								<p class="itemName">ダイヤルース</p>
								<p class="itemdetail">カラット：1.003ct<br>
									カラー：G<br>
									クラリティ：VS-2<br>
									カット：Good<br>
									蛍光性：FAINT<br>
									形状：ラウンドブリリアント</p>
								<hr>
								<p> <span class="red">A社</span>：215,000円<br>
									<span class="blue">B社</span>：210,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price"><!-- span class="small">地金＋</span -->221,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>11,000円</p>
							</div>
						</li>
						
						<!-- 2 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/002.jpg" alt="">
								<p class="itemName">Pt900 DR0.417ctリング</p>
								<p class="itemdetail"> カラー：F<br>
									クラリティ：SI-1<br>
									カット：VERY GOOD <br>
									蛍光性：FAINT<br>
									形状：ラウンドブリリアント</p>
								<hr>
								<p> <span class="red">A社</span>：42,000円<br>
									<span class="blue">B社</span>：41,300円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">44,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,700円</p>
							</div>
						</li>
						
						<!-- 3 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/003.jpg" alt="">
								<p class="itemName">Pt900　DR0.25ctリング</p>
								<p class="itemdetail"> カラー：H<br>
									クラリティ:VS-1<br>
									カット：Good</br>
									蛍光性：MB<br>
									形状：ラウンドブリリアント<br>
								
								<hr>
								<p> <span class="red">A社</span>：67,400円<br>
									<span class="blue">B社</span>：67,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">68,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,000円</p>
							</div>
						</li>
						
						<!-- 4 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/004.jpg" alt="">
								<p class="itemName">K18　DR0.43ct　MD0.4ctネックレストップ</p>
								<p class="itemdetail"> カラー：I<br>
									クラリティ：VS-2<br>
									カット：Good<br>
									蛍光性：WB<br>
									形状：ラウンドブリリアント</p>
								<hr>
								<p> <span class="red">A社</span>：50,000円<br>
									<span class="blue">B社</span>：49,500円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">51,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,500円</p>
							</div>
						</li>
						
						<!-- 5 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/005.jpg" alt="">
								<p class="itemName">ダイヤルース</p>
								<p class="itemdetail">カラット：0.787ct<br>
									カラー：E<br>
									クラアリティ：VVS-2<br>
									カット：Good<br>
									蛍光性：FAINT<br>
									形状：ラウンドブリリアント</p>
								<hr>
								<p> <span class="red">A社</span>：250,000円<br>
									<span class="blue">B社</span>：248,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">257,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>9,000円</p>
							</div>
						</li>
						
						<!-- 6 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/006.jpg" alt="">
								<p class="itemName">Pt950　MD0.326ct　0.203ct　0.150ctネックレス</p>
								<p class="itemdetail"> カラー：F<br>
									クラリティ：SI-2<br>
									カット：Good<br>
									蛍光性：FAINT<br>
									形状：ラウンドブリリアント</p>
								<hr>
								<p> <span class="red">A社</span>：55,000円<br>
									<span class="blue">B社</span>：54,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">57,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>3,000円</p>
							</div>
						</li>
						
						<!-- 7 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/007.jpg" alt="">
								<p class="itemName">ダイヤルース</p>
								<p class="itemdetail">カラット：1.199ct<br>
									カラー：K<br>
									クラリティ：SI-2<br>
									カット：評価無<br>
									蛍光性：NONE<br>
									形状：パビリオン<br>
								</p>
								<hr>
								<p> <span class="red">A社</span>：58,000円<br>
									<span class="blue">B社</span>：56,800円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">60,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>3,200円</p>
							</div>
						</li>
						
						<!-- 8 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/008.jpg" alt="">
								<p class="itemName">ダイヤルース</p>
								<p class="itemdetail">カラット：8.1957ct<br>
									カラー：LIGTH YELLOW<br>
									クラリティ：VS-1<br>
									カット：VERY GOOD<br>
									蛍光性：FAINT<br>
									形状：ラウンドブリリアント</p>
								<hr>
								<p> <span class="red">A社</span>：6,540,000円<br>
									<span class="blue">B社</span>：6,500,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">6,680,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>180,000円</p>
							</div>
						</li>
					</ul>
				</div>
				<div class="menu"><span class="glyphicon glyphicon-time"></span> 時計買取</div>
				<div class="content">
					<ul id="box-jisseki" class="list-unstyled clearfix">
						<?php
                    foreach($resultLists as $list):
                    // :: で分割
                    $listItem = explode('::', $list);
                  
                  ?>
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/<?php echo $listItem[0]; ?>" alt="">
								<p class="itemName"><?php echo $listItem[1]; ?><br>
									<?php echo $listItem[2]; ?></p>
								<hr>
								<p> <span class="red">A社</span>：<?php echo $listItem[3]; ?>円<br>
									<span class="blue">B社</span>：<?php echo $listItem[4]; ?>円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price"><?php echo $listItem[5]; ?><span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span><?php echo $listItem[6]; ?>円</p>
							</div>
						</li>
						<?php endforeach; ?>
					</ul>
				</div>
				
				<!-- バッグ買取 -->
				<div class="menu"><span class="glyphicon glyphicon-briefcase"></span> バッグ買取</div>
				<div class="content">
					<ul id="box-jisseki" class="list-unstyled clearfix">
						<!-- 1 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/001.jpg" alt="">
								<p class="itemName">エルメス　エブリンⅢ　トリヨンクレマンス</p>
								<hr>
								<p> <span class="red">A社</span>：176,000円<br>
									<span class="blue">B社</span>：172,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">180,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>8,000円</p>
							</div>
						</li>
						<!-- 2 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/002.jpg" alt="">
								<p class="itemName">プラダ　シティトート2WAYバッグ</p>
								<hr>
								<p> <span class="red">A社</span>：128,000円<br>
									<span class="blue">B社</span>：125,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">132,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>7,000円</p>
							</div>
						</li>
						<!-- 3 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/003.jpg" alt="">
								<p class="itemName">バンブーデイリー2WAYバッグ</p>
								<hr>
								<p> <span class="red">A社</span>：83,000円<br>
									<span class="blue">B社</span>：82,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">85,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>3,000円</p>
							</div>
						</li>
						<!-- 4 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/004.jpg" alt="">
								<p class="itemName">ルイヴィトン　モノグラムモンスリGM</p>
								<hr>
								<p> <span class="red">A社</span>：64,000円<br>
									<span class="blue">B社</span>：63,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">66,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>3,000円</p>
							</div>
						</li>
						<!-- 5 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/005.jpg" alt="">
								<p class="itemName">エルメス　バーキン30</p>
								<hr>
								<p> <span class="red">A社</span>：1,150,000円<br>
									<span class="blue">B社</span>：1,130,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">1,200,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>70,000円</p>
							</div>
						</li>
						<!-- 6 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/006.jpg" alt="">
								<p class="itemName">シャネル　マトラッセダブルフラップダブルチェーンショルダーバッグ</p>
								<hr>
								<p> <span class="red">A社</span>：252,000円<br>
									<span class="blue">B社</span>：248,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">260,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>12,000円</p>
							</div>
						</li>
						<!-- 7 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/007.jpg" alt="">
								<p class="itemName">ルイヴィトン　ダミエネヴァーフルMM</p>
								<hr>
								<p> <span class="red">A社</span>：96,000円<br>
									<span class="blue">B社</span>：94,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">98,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>4,000円</p>
							</div>
						</li>
						<!-- 8 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/008.jpg" alt="">
								<p class="itemName">セリーヌ　ラゲージマイクロショッパー</p>
								<hr>
								<p> <span class="red">A社</span>：150,000円<br>
									<span class="blue">B社</span>：148,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">155,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>7,000円</p>
							</div>
						</li>
						<!-- 9 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/009.jpg" alt="">
								<p class="itemName">ロエベ　アマソナ23</p>
								<hr>
								<p> <span class="red">A社</span>：86,000円<br>
									<span class="blue">B社</span>：82,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">90,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>8,000円</p>
							</div>
						</li>
						<!-- 10 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/010.jpg" alt="">
								<p class="itemName">グッチ　グッチシマ　ビジネスバッグ</p>
								<hr>
								<p> <span class="red">A社</span>：50,000円<br>
									<span class="blue">B社</span>：48,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">53,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>5,000円</p>
							</div>
						</li>
						<!-- 11 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/011.jpg" alt="">
								<p class="itemName">プラダ　サフィアーノ2WAYショルダーバッグ</p>
								<hr>
								<p> <span class="red">A社</span>：115,000円<br>
									<span class="blue">B社</span>：110,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">125,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>15,000円</p>
							</div>
						</li>
						<!-- 12 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/012.jpg" alt="">
								<p class="itemName">ボッテガヴェネタ　カバMM　ポーチ付き</p>
								<hr>
								<p> <span class="red">A社</span>：150,000円<br>
									<span class="blue">B社</span>：146,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">155,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>9,000円</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</section>
<section> <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/bn_matomegai.png"> </section>
<section class="kaitori_voice">
<h2 class="ttl_edit01">店頭郵送買取をご利用いただいたお客様の声</h2>
<ul>
<li>
<p class="kaitori_tab tebra_tab">店頭郵送買取</p>
<h4>フランクミュラー　時計</h4>
<p class="voice_txt">店頭郵送買取という買取方法が気になったので、こちらを利用してフランクミュラーの時計の買取を依頼しました。
最初に時計を郵送し、後日ブランドリバリューの店舗に出向くだけなので、本当にこの買取方法はスムーズでいいですね。郵送費もすべて負担いていただいて言うことなしです。<br />
仕事の帰りに合わせて予約をいれたので、この時間に立ち寄ったのですが、高額買取をしていただけて満足です。
待ち時間もないので、立ち寄った後に用事があっても安心して買取をしてもらうことができますし、きちんとすぐその場で現金での支払いをしてもらえるので、助かります。<br />
これからも、何かブランド物を買取査定をしてもらいたいと思ったときは、店頭郵送買取を利用しようと思っています。
特に高額なブランドアイテムや、かさばるブランドアイテムの時はとても重宝しそうな買取方法だと思います。</p>
</li>

<li>
<p class="kaitori_tab tebra_tab">店頭郵送買取</p>
<h4>セリーヌ　バッグ</h4>
<p class="voice_txt">セリーヌのバッグを買取査定に出したいと思っていました。職場が銀座ですが、家は銀座に遠いので、職場に買取品を持っていくわけにも行かず、と悩んでいたところホームページで店頭郵送買取を知りました。他社の買取実績と比較をしてみても、ブランドリバリューさんは高値の査定が期待できそうだったので、試しに店頭郵送買取で、セリーヌのバッグの買取を依頼することにしたんです。<br />
その後たまたま会社飲み会が銀座であったので、このタイミングで買取をして立ち寄るように予定をたてることもできました。<br />
自分のスケジュールに合わせて、セリーヌのバッグを効率よく高値で査定してもらえてよかったです。</p>
</li>
<li>
<p class="kaitori_tab tebra_tab">店頭郵送買取</p>
<h4>ルイヴィトン　財布</h4>
<p class="voice_txt">新しい財布を購入したため、以前使っていたルイヴィトンの長財布を買取してもらうことにしました。<br />
いろいろ買取方法がありましたが、先に財布を送って、あとから店頭に出向いて査定額を受け取るだけなので、店頭郵送買取を選びました。<br />
それなら宅配買取の方が楽じゃない？って言われそうですけど、直接査定してもらうときは、担当の人と話して査定をしてもらいたいと思っているタイプの人間なので。。。<br />
なので、そんなわがままな自分にとって、めちゃくちゃ合ってたんです、店頭郵送買取という買取方法は。
査定額もきちんと満足することができましたし、実際に店舗で気になる部分についても質問してしっかりとした回答をしていただけたので、対応も大満足です。<br />
もういいかなと思っているルイヴィトンのバッグも手元にあるので、この買取査定も店頭郵送買取を利用してやってもらおうかなと考えているところですよ。</p>
</li>


</ul>


</section>



<?php
        
        // 買取方法
        get_template_part('_purchase');
        
        // 店舗案内
        get_template_part('_shopinfo');
      ?>		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
