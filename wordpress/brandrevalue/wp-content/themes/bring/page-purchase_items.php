<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <h2 class="ttl_blue">ブランドリバリュー ブランド買取実績</h2>
        <ul class="archive_purchase_list">
            <li><a href="<?php echo home_url('purchase_items/purchase_watch'); ?>">時計</a></li>
            <li><a href="<?php echo home_url('purchase_items/purchase_antique'); ?>">アンティーク時計</a></li>
            <li><a href="<?php echo home_url('purchase_items/purchase_bag'); ?>">バッグ</a></li>
            <li><a href="<?php echo home_url('purchase_items/purchase_wallet'); ?>">財布</a></li>
            <li><a href="<?php echo home_url('purchase_items/purchase_outfit'); ?>">洋服毛皮</a></li>
            <li><a href="<?php echo home_url('purchase_items/purchase_shoes'); ?>">靴</a></li>
            <li><a href="<?php echo home_url('purchase_items/purchase_jewelry'); ?>">ブランドジュエリー</a></li>
            <li><a href="<?php echo home_url('purchase_items/purchase_gold'); ?>">金・プラチナ</a></li>
            <li><a href="<?php echo home_url('purchase_items/purchase_dia'); ?>">ダイヤ・宝石</a></li>
            <li><a href="<?php echo home_url('purchase_items/purchase_mement'); ?>">骨董品</a></li>
        </ul>
<ul class="archive_purchase">
<?php query_posts(
  array(
  'post_type' => 'purchase_item',
  'taxonomy' => 'purchase_item',
  'posts_per_page' => 20,
  'paged' => $paged,
  ));
  if (have_posts()): while ( have_posts() ) : the_post();
  ?>
<li><a href="<?php the_permalink(); ?>">
    <p class="kaitori-date"><?php the_field('kaitori-date'); ?></p>
    <p class="kaitori-image"><img src="<?php the_field('kaitori-img'); ?>" alt="<?php the_title(); ?>"></p>
    <p class="kaitori-title"><?php the_title(); ?></p>
    <p class="kaitori-price-header">買取価格</p>
    <P class="kaitori-price"> ¥<?php the_field('kaitori-price'); ?></P>
    <p class="kaitori-method"><?php the_field('kaitori-method'); ?></p>
    <p class="kaitori-detail">詳細はこちら</p>
</a></li>
    <?php endwhile; endif; wp_pagenavi(); wp_reset_query(); ?>
</ul>
<?php

       // アクションポイント
        get_template_part('_action');
        // 店舗案内
        get_template_part('_shopinfo');
      ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
