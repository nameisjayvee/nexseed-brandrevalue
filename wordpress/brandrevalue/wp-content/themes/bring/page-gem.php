<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */
get_header(); ?>

<p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
<p class="main_bottom">満足価格で買取！宝石・ジュエリー買取ならブランドリバリュー</p>
<div id="primary" class="cat-page content-area">
<main id="main" class="site-main" role="main">
<div id="lp_head" class="gem_ttl">
<div>
<p>銀座で最高水準の査定価格・サービス品質をご体験ください。</p>
<h2>あなたの宝石<br />どんな物でもお売り下さい！！</h2>
</div>
</div>
            <p id="catchcopy">BRANDREVALUE(ブランドリバリュー)には宝石鑑定において長年の経験を積んできたスタッフが在籍しており、主要な宝石すべてに対し適切な査定を行っております。ダイヤモンド、ルビー、サファイア、エメラルド、真珠といったメジャーな宝石はもちろん、オパール、キャッツアイ、タンザナイト、アクアマリン、トパーズ、トルマリン、珊瑚、翡翠など、比較的市場での流通が少ない宝石についても対応しておりますので、「他店で査定ができなかった宝石」なども遠慮なくお持ちください。</p>

            <section id="hikaku" class="gem_hikaku">
                <h3 class="text-hide">他社の買取額と比較すると</h3>
                <p class="hikakuName"><span class="name_small">サファイアダイヤリング　PT900 </span></p>
                <p class="hikakuText">※状態が良い場合や当店で品薄の場合などは
                    <br> 　特に高価買取致します。
                </p>
                <p class="hikakuPrice1"><span class="red">A社</span>：520,000円</p>
                <p class="hikakuPrice2"><span class="blue">B社</span>：400,000円</p>
                <p class="hikakuPrice3">650,000<span class="small">円</span></p>
            </section>
            <section class="kaitori_cat">
                <ul>
                    <li>
                        <a href="https://kaitorisatei.info/brandrevalue/blog/doburock"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/12/caf61dc6779ec6137c0ab58dfe3a550d.jpg" alt="どぶろっく"></a>
                    </li>
                </ul>
            </section>            
    <h3 class="mid">宝石 一覧</h3>
    <ul class="mid_link">
      <li>
      <a href="<?php echo home_url('/cat/diamond'); ?>">
      <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gem/01.jpg" alt="ダイヤ買取">
      <p class="mid_ttl">ダイヤ</p>
      </a>
      </li>
      <li>
      <a href="<?php echo home_url('/cat/gem/emerald'); ?>">
      <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gem/02.jpg" alt="エメラルド買取">
      <p class="mid_ttl">エメラルド</p>
      </a>
      </li>
      <li>
      <a href="<?php echo home_url('/cat/gem/opal'); ?>">
      <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gem/03.jpg" alt="オパール買取">
      <p class="mid_ttl">オパール</p>
      </a>
      </li>
      <li>
      <a href="<?php echo home_url('/cat/gem/sapphire'); ?>">
      <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gem/04.jpg" alt="サファイア買取">
      <p class="mid_ttl">サファイア</p>
      </a>
      </li>
      <li>
      <a href="<?php echo home_url('/cat/gem/tourmaline'); ?>">
      <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gem/05.jpg" alt="トルマリン買取">
      <p class="mid_ttl">トルマリン</p>
      </a>
      </li>
      <li>
      <a href="<?php echo home_url('/cat/gem/paraibatourmaline'); ?>">
      <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gem/06.jpg" alt="パライバトルマリン買取">
      <p class="mid_ttl">パライバトルマリン</p>
      </a>
      </li>
      <li>
      <a href="<?php echo home_url('/cat/gem/ruby'); ?>">
      <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gem/07.jpg" alt="ルビー買取">
      <p class="mid_ttl">ルビー</p>
      </a>
      </li>
      <li>
      <a href="<?php echo home_url('/cat/gem/hisui'); ?>">
      <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gem/08.jpg" alt="ヒスイ買取">
      <p class="mid_ttl">ヒスイ</p>
      </a>
      </li>
      <li>
      <a href="<?php echo home_url('/cat/brandjewelery'); ?>">
      <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gem/09.jpg" alt="ブランドジュエリー買取">
      <p class="mid_ttl">ブランドジュエリー</p>
      </a>
      </li>
      </ul>         
<section id="cat-point" class="gold-point">
<h2 class="obi_tl">宝石・ジュエリー高価買取のポイント</h2>
<ul class="list-unstyled">
<li>
<div class="pt-wrap">
<p class="pt_tl">国際基準に基づいた厳密な査定</p>
<p class="pt_tx">「BRAND REVALUE｣では、資格を所有したプロが在籍しておりますので、大きさに関係なく一つ一つの石を宝石国際鑑定機関の評価基準で見誤る事なくスムーズに査定すことが可能です。</p>
</div>
</li>
<li>
<div class="pt-wrap">
<p class="pt_tl">国内外の価格変動情報を共有</p>
<p class="pt_tx">常に専門のバイヤーが国内外の流通情報を日々把握、共有しており、最新の相場価格を把握できる情報網、環境を整えている「BRAND REVALUE｣は限界ギリギリの買取が可能です。
</p>
</div>
</li>
<li>
<div class="pt-wrap">
<p class="pt_tl">最高額で販売できるルートの確保</p>
<p class="pt_tx" style="font-size:14px;">最高額で販売できるルートの確保「BRAND REVALUE｣では、最高額でお買取りさせて頂く為に、日本国内外問わず常に最高額で販売できるよう市場開拓、顧客様へのアプローチ等、販売ルートの確保を行っている為、お客様の大切なお品物を他店様より高く買い取らせていただくことができます。</p>
</div>
</li>
</ul>
<p>宝石の査定時には、購入時に付属していた鑑定書や鑑別書もぜひあわせてお持ちください。これらの書類には、「宝石が天然ものか人工的に作られたものか」といった情報が記載されており、査定の処理がスムーズになります。また、宝石入りアクセサリーの場合購入時に入っていた箱や替えのチェーン等の付属品があると査定金額が上がる可能性があります。もちろん、鑑定書・鑑別書、箱等がなくても宝石そのものの価値は変わりません。「銀座一の高額査定」を目指して高く評価いたしますので、ご安心ください。</p>
</section>
      
      
      
      
      
      
      
      
          <section> <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/gem/jw_grf.png"> </section>


            <section id="lp-cat-jisseki">
                <h2 class="ttl_edit02">宝石・ジュエリーの買取実績</h2>
                <ul id="box-jisseki" class="list-unstyled clearfix">
                    <li class="box-4">
                <div class="title">
                  <p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2017/10/sapphire.jpg" alt="">
</p>
                  <p class="itemName">Pt900 非加熱サファイア
ダイヤリング</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：1,780,000円<br>
                        <span class="blue">B社</span>：1,750,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price"><span class="small"></span> 1,850,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>100,000円</p>
                </div>
              </li>
              <li class="box-4">
                <div class="title">
                  <p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/lp/004.jpg" alt="">
</p>
                  <p class="itemName">Pt900　エメラルド4.3ct　MD0384ctリング</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：380,000円<br>
                        <span class="blue">B社</span>：379,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">400,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>21,000円</p>
                </div>
              </li>
                    <li class="box-4">
                <div class="title">
                  <p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2017/10/ruby.jpg" alt="">
</p>
                  <p class="itemName">ルビー ダイヤリング Pt900</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：489,000円<br>
                        <span class="blue">B社</span>：470,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price"><span class="small"></span> 500,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>30,000円</p>
                </div>
              </li>
              <li class="box-4">
                <div class="title">
                  <p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/lp/002.jpg" alt="">
</p>
                  <p class="itemName">Pt900  ブラックオパール 3.18ct　MD131ctリング</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：367,000円<br>
                        <span class="blue">B社</span>：354,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price"> 380,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>26,000円</p>
                </div>
              </li>
              <li class="box-4">
                <div class="title">
                  <p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/lp/003.jpg" alt="">
</p>
                  <p class="itemName">黒蝶真珠　9.1mm～12.1mm　留め具K18ネックレス</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：300,000円<br>
                        <span class="blue">B社</span>：280,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">350,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>70,000円</p>
                </div>
                            </li>
              <li class="box-4">
                <div class="title">
                  <p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/lp/006.jpg" alt="">
</p>
                  <p class="itemName">天然ジェダイト（翡翠）15.8ct　MD2.1ctリング</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：250,000円<br>
                        <span class="blue">B社</span>：246,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price"> 258,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>12,000円</p>
                </div>
              </li>
              <li class="box-4">
                <div class="title">
                  <p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/lp/007.jpg" alt="">
</p>
                  <p class="itemName">Pt900/K18　アクアマリン12.35ct　MD0.6ctリング</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：96,000円<br>
                        <span class="blue">B社</span>：92,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">100,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>8,000円</p>
                </div>
              </li>
              <li class="box-4">
                <div class="title">
                  <p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/lp/008.jpg" alt="">
</p>
                  <p class="itemName">アメジストK18リング</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：47,800円<br>
                        <span class="blue">B社</span>：44,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">50,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>6,000円</p>
                </div>
              </li>
                   <li class="box-4">
                <div class="title">
                  <p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/04/9.jpg" alt="パライバトルマリンダイヤネックレス">
</p>
                  <p class="itemName">パライバトルマリンダイヤネックレス</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：420,000円<br>
                        <span class="blue">B社</span>：280,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price"><span class="small"></span> 500,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>220,000円</p>
                </div>
              </li>
              <li class="box-4">
                <div class="title">
                  <p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/04/10.jpg" alt="アレキサンドライトダイヤリング">
</p>
                  <p class="itemName">アレキサンドライトダイヤリング</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：180,000円<br>
                        <span class="blue">B社</span>：100,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price"> 250,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>150,000円</p>
                </div>
              </li>
              <li class="box-4">
                <div class="title">
                  <p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/04/11.jpg" alt="赤珊瑚ダイヤリング">
</p>
                  <p class="itemName">赤珊瑚ダイヤリング</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：130,000円<br>
                        <span class="blue">B社</span>：80,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">170,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>90,000円</p>
                </div>
              </li>
              <li class="box-4">
                <div class="title">
                  <p class="bx_img"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/04/8.jpg" alt="パールダイヤリング">
</p>
                  <p class="itemName">パールダイヤリング</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：40,000円<br>
                        <span class="blue">B社</span>：25,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">60,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>35,000円</p>
                </div>
              </li>
                </ul>
</section>
<section id="list-brand" class="clearfix">
<!--<h2>宝石・ジュエリーのブランドリスト</h2>
        <ul class="list-unstyled">
          <li>
            <dl>
            <a href="<?php echo home_url('/cat/gem/cartier'); ?>">
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem01.jpg" alt="カルティエ"></dd>
              <dt>Cartier<span></span>カルティエ</dt>
              </a>
            </dl>
          </li>
          <li>
            <dl>
            <a href="<?php echo home_url('/cat/gem/tiffany'); ?>">
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem02.jpg" alt="ティファニー"></dd>
              <dt><span>Tiffany</span>ティファニー</dt>
              </a>
            </dl>
          </li>
          <li>
            <dl>
            <a href="<?php echo home_url('/cat/gem/harrywinston'); ?>">
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem03.jpg" alt="ハリーウィンストン"></dd>
              <dt><span>Harrywinston</span>ハリーウィンストン</dt>
              </a>
            </dl>
          </li>
          
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/gem/piaget'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch22.jpg" alt="ピアジェ"></dd>
                                <dt><span>PIAGET</span>ピアジェ</dt>
                            </a>
                        </dl>
                    </li>
          <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/gem/vancleefarpels'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch30.jpg" alt="ヴァンクリーフ&アーペル"></dd>
                                <dt><span>Vanleefarpels</span>ヴァンクリーフ&アーペル</dt>
                            </a>
                        </dl>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/gem/bulgari'); ?>">

                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch27.jpg" alt="ブルガリ"></dd>
                                <dt><span>BVLGALI</span>ブルガリ</dt>
                            </a>
                        </dl>

                    </li>
          <li>
                        <dl>
                        <a href="<?php echo home_url('/cat/gem/boucheron'); ?>">

              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem06.jpg" alt="ブシュロン"></dd>
                            <dt><span>Boucheron</span>ブシュロン</dt>
                            </a>
                        </dl>
                    </li>
<li>
                        <dl>
                        <a href="<?php echo home_url('/cat/gem/emerald'); ?>">

                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem07.jpg" alt="エメラルド"></dd>
                            <dt><span>Emerald</span>エメラルド</dt>
                            </a>
                        </dl>
                    </li>
<li>
                        <dl>
                        <a href="<?php echo home_url('/cat/gem/opal'); ?>">

                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem08.jpg" alt="オパール"></dd>
                            <dt><span>Opal</span>オパール</dt>
                            </a>
                        </dl>
                    </li>
<li>
                        <dl>
                        <a href="<?php echo home_url('/cat/gem/sapphire'); ?>">

                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem09.jpg" alt="サファイア"></dd>
                            <dt><span>Sapphire</span>サファイア</dt>
                            </a>
                        </dl>
                    </li>

<li>
                        <dl>
                        <a href="<?php echo home_url('/cat/gem/tourmaline'); ?>">

                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem10.jpg" alt="トルマリン"></dd>
                            <dt><span>Tourmaline</span>トルマリン</dt>
                            </a>
                        </dl>
                    </li>
<li>
                        <dl>
                        <a href="<?php echo home_url('/cat/gem/paraibatourmaline'); ?>">

                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem11.jpg" alt="パライバトルマリン"></dd>
                            <dt><span style="font-size:9px;">Paraibatourmaline</span>パライバトルマリン</dt>
                            </a>
                        </dl>
                    </li>
<li>
                        <dl>
                        <a href="<?php echo home_url('/cat/gem/hisui'); ?>">

                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem12.jpg" alt="ヒスイ"></dd>
                            <dt><span>Hisui</span>ヒスイ</dt>
                            </a>
                        </dl>
                    </li>

<li>
                        <dl>
                        <a href="<?php echo home_url('/cat/gem/ruby'); ?>">

                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem13.jpg" alt="ルビー"></dd>
                            <dt><span>Ruby</span>ルビー</dt>
                            </a>
                        </dl>
                    </li>
                    
         
        </ul>-->
      </section>
<section id="lp-cat-jisseki" class="clearfix">

                <div id="konna" class="konna_gem">
                    <p class="example1">■イニシャル入り</p>
                    <p class="example2">■金具破損</p>
                    <p class="example3">■石のみ</p>
                    <p class="text">その他：片方だけのピアス等でもお買取りいたします。</p>
                </div>
            </section>

            <section id="about_kaitori" class="clearfix">
                <?php
        // 買取について
        get_template_part('_widerange');
        get_template_part('_widerange2');
      ?>
            </section>

            

            <section>
                <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/bn_matomegai.png">
            </section>

            <?php
        // 買取基準
        get_template_part('_criterion');

        // NGアイテム
        get_template_part('_no_criterion');
        
        // カテゴリーリンク
        get_template_part('_category');
      ?>
                <section id="kaitori_genre">
                    <h3 class="text-hide">その他買取可能なジャンル</h3>
                    <p>【買取ジャンル】バッグ/ウエストポーチ/セカンドバック/トートバッグ/ビジネスバッグ/ボストンバッグ/クラッチバッグ/トランクケース/ショルダーバッグ/ポーチ/財布/カードケース/パスケース/キーケース/手帳/腕時計/ミュール/サンダル/ビジネスシューズ/パンプス/ブーツ/ペアリング/リング/ネックレス/ペンダント/ピアス/イアリング/ブローチ/ブレスレット/ライター/手袋/傘/ベルト/ペン/リストバンド/アンクレット/アクセサリー/サングラス/帽子/マフラー/ハンカチ/ネクタイ/ストール/スカーフ/バングル/カットソー/アンサンブル/ジャケット/コート/ブルゾン/ワンピース/ニット/シャツメンズ/毛皮/Tシャツ/キャミソール/タンクトップ/パーカー/ベスト/ポロシャツ/ジーンズ/スカート/スーツなど</p>
                </section>

                <?php
        // 買取方法
        get_template_part('_purchase');
      ?>

                    <section id="user_voice">
                        <h2>宝石・ジュエリー買取をご利用いただいたお客様の声</h2>

                        <p class="user_voice_text1">ちょうど家の整理をしていたところ、家のポストにチラシが入っていたので、ブランドリバリューに電話してみました。今まで買取店を利用したことがなく、不安でしたがとても丁寧な電話の対応とブランドリバリューの豊富な取り扱い品目を魅力に感じ、出張買取を依頼することにしました。 絶対に売れないだろうと思った、動かない時計や古くて痛んだバッグ、壊れてしまった貴金属のアクセサリーなども高額で買い取っていただいて、とても満足しています。古紙幣や絵画、食器なども買い取っていただけるとのことでしたので、また家を整理するときにまとめて見てもらおうと思います。
                        </p>

                        <h4>鑑定士からのコメント</h4>
                        <div class="clearfix">
                            <p class="user_voice_text2">家の整理をしているが、何を買い取ってもらえるか分からないから一回見に来て欲しいとのことでご連絡いただきました。 買取店が初めてで不安だったが、丁寧な対応に非常に安心しましたと笑顔でおっしゃって頂いたことを嬉しく思います。 物置にずっとしまっていた時計や、古いカバン、壊れてしまったアクセサリーなどもしっかりと価値を見極めて高額で提示させて頂きましたところ、お客様もこんなに高く買取してもらえるのかと驚いておりました。 これからも家の不用品を整理するときや物の価値判断ができないときは、すぐにお電話しますとおっしゃって頂きました。
                            </p>

                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/photo-user-voice.jpg" class="right">
                        </div>
                    </section>

<section>
  <div class="cont_widerange">
              <h2>宝石の買取にブランド、ノーブランドは関係ある?！</h2>
              <ul>
                  <li>ジュエリーブティックだけでなく百貨店や宝石展示会など、宝石を購入するシーンは意外にも多いものです。宝石のグレードによっては購入価格が高額なジュエリーや宝石が多いため、一度手にするとなかなか売却しようとは思わないかもしれません。しかし、「最近着けなくなった」、「好みが変わった」、「持っているジュエリーや宝石を整理したくて…」とその処分方法に困っている人もいます。<br>
もし手元に売却を検討している宝石やジュエリーがあれば、まずブランドジュエリーとノーブランドジュエリーに分けてみましょう。なぜならブランドやモデルがはっきりしているジュエリーや宝石類は、ノーブランドの宝石やジュエリーと比較してそのブランド価値によって買取価格がアップする可能性があるため、買取価格に関係している場合が多いためです。<br>
例えば同じゴールドの台にダイヤが散りばめられたジュエリーでも、その査定価格は大きく変わる場合があります。その最もな例が、ブランドジュエリーか、ノーブランドジュエリーか違いによる場合です。<br>
ブランドジュエリーを購入時に発行されるブランドメーカーのギャランティーカードやブランドネームの入ったジュエリーケース、保存袋などがきちんと揃っている場合は、同じランクの宝石やジュエリーでもブランドの方がグッと価値が上がります。その主な理由として、商品情報が明確なブランドジュエリーの場合、品質はブランドが保証しているためコレクションやモデルネームからその価値を素早く見極めることができる上、中古市場での人気の高さもすぐに分かるため、査定価格に大きくプラスになることが多いようです。<br>
さらに、人気ブランドの新作コレクションの場合は、中古市場ではまだ流通量が少ないため稀少価値が高く買取価格がアップすることもあります。気になる買取でも人気のジュエリーブランドとして、ハリーウィンストン、ヴァンクリーフ＆アーペルをはじめ、カルティエ、ブリガリ、ティファニーなどが常に高い人気を誇っています。<br><br>
</li>
              </ul>
          </div>
  <div class="cont_widerange">
              <h2>買取に人気の宝石の種類とは？</h2>
              <ul>
                  <li>ジュエリーには宝石がついてこそ、その輝きが一層増します。百貨店のジュエリーコーナーでもジュエリーブティックでも、小さなダイヤモンドが一粒セットされているだけでそのジュエリーは一気に魅力的なジュエリーに見えてしまうのですから、宝石の輝きはまさにマジックとも言えるでしょう。<br>
このようにジュエリーの主役ともいえる宝石には様々な種類があります。それぞれ宝石の魅力は異なりますが、その宝石も同じ種類の意思でも稀少性や大きさ、生産地などによって価格が大きく変わってきます。特に、宝石の中でもサファイアやルビー、エメラルドなどの一般に「色石」と呼ばれる石は、流通経路も複雑であることから価格変動が大きい宝石としても知られています。<br>
数ある宝石の中でも買取で人気が高い宝石は、やはりダイヤモンドでしょう。「宝石の王」とも呼ばれるダイヤモンドはジュエリーに最もよく使用される宝石です。重量を表すカラット、無色透明度を表すカラー、不純物を含まない輝き表すクラリティー、そしてブリリアンカットなどが代表的なダイヤモンドのカット方法によってランクが明確にされていることもダイヤモンドの大きな特徴です。購入時には鑑識証明書などが発行される場合が多いため、ダイヤのランクをその価値をしっかり見極めることができることから買取価格が高い場合が多いのです。ただし素人がダイヤモンドの鑑定や判別をするのは大変難しく、買取店の中に必ず宝石専門の鑑定士がいつも担当してくれるような買取店で査定をしてもらうようにしましょう。<br>
その他にも、サファイアやルビー、エメラルド、パール、アクアマリン、アメジスト、言った代表的な宝石も買取では人気の宝石となっています。買取店の中には、ヒスイやオパール、トルマリン、サンゴなどの買取も積極的に取扱っている店舗もあります。どの宝石も天然の本物の石の場合には購入時に鑑定証書が付いてくるため、査定の時には必ず売却したい宝石と一緒に出すようにしましょう。<br><br>
</li>
              </ul>
          </div>
  <div class="cont_widerange">
              <h2>買取可能な状態の宝石は？</h2>
              <ul>
                  <li>古いデザインの宝石やジュエリー、気が付けばジュエリーボックスやタンスの引き出しの中に結構眠っていることもあります。買取店で査定してもらって、その価値を現金にしてみるのも良いかもしれないと思っている人もいるはずです。しかし、宝石を買い取りしもらうためには、傷やダメージ、破損などがない完璧な状態でないと買取ってもらえないのではと不安に思っている人は少なくありません。<br>
いくら大切に保管したり使用してきた宝石でも、何かの拍子で宝石だけが台から落ちてしまうこともあります。そんな主石が取れてしまったジュエリーこそ身に着けることもできないので、買取ってほしいと思うのが正直なところですよ。そんな時は査定や買取に出す前に宝石やジュエリーは買取可能な状態かどうかをチェックできるように、買取可能な状態について少しでも知っておくと良いかもしれません。<br>
まず買取可能な状態の宝石の最低条件は、貴金属以外のメッキなどの材質や人為的にカラーを付けた偽物と鑑定される宝石などではないこと、そして宝石の石に大きな傷や破損がないことです。つまり金やプラチナ、シルバーといった貴金属であること、そして本物の宝石であること、石の状態が良いことです。そのため古くなってしまったデザインや石が取れてしまったジュエリー、イニシャルや刻印のある指輪やペンダントトップ、留め金などの金具が壊れてたり、チェーンが切れてしまっていても、全く問題のない買取可能な状態と言うことになります。なかには、取れてしまった主石の宝石やダイヤモンドの石だけでも買取してもらうことは可能なのです。<br>
宝石の買取可能となる幅は広いため、家に眠っている宝石を探せば必ず買取可能な状態のものはあるはずです。また、査定を依頼する場合は、できれば購入時に発行された鑑定証などがあれば一緒に査定に出して、その宝石の価値をしっかり査定してもらいましょう。有名ブランドの宝石の場合も、ギャランティーカードやボックス、保存袋などと一緒に査定に出すと査定価格がアップすることもあるので、ぜひ揃えておきましょう。
</li>
              </ul>
          </div>
</section>

        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->

    <?php
get_sidebar();
get_footer();