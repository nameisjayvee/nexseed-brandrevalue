<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>



	<div id="primary" class="cat-page content-area">
		<main id="main" class="site-main" role="main">
        <!--<section id="mainVisual" style="background:url(<?php echo get_s3_template_directory_uri() ?>/img/mv/purchase_mv.png)">
          <h2 class="text-hide">買取方法</h2>
        </section>-->
        <h2>BLOG</h2>
      
      <section>
        <?php
          //-------------------------------------
          if(have_posts()): while(have_posts()): the_post();
            $cat = get_the_terms(get_the_ID(), 'blog-cat');
            $cat = $cat[0];
            $catName = $cat->name;
            if(!empty($catName)) {
              $catName = '【'.$catName.'】';
            }
        ?>
      
        <div class="postlist clearfix">
          <a href="<?php the_permalink(); ?>">
            <h3><?php echo $catName; ?><?php the_title(); ?></h3>
          </a>
          <p class="date"><?php the_time('Y年m月d日（D）'); ?></p>
          <p class="text"><?php the_excerpt(); ?></p>
          <p class="intoSingle alignright"><a href="<?php the_permalink(); ?>">続きを読む</a></p>
        </div>
      
        <?php
          endwhile; endif;
          //-------------------------------------
        ?>
        
        <div class="blog-pagination">
          <?php global $wp_rewrite;
          $paginate_base = get_pagenum_link(1);
          if(strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()){
              $paginate_format = '';
              $paginate_base = add_query_arg('paged','%#%');
          }
          else{
              $paginate_format = (substr($paginate_base,-1,1) == '/' ? '' : '/') .
              user_trailingslashit('page/%#%/','paged');;
              $paginate_base .= '%_%';
          }
          echo paginate_links(array(
              'base' => $paginate_base,
              'format' => $paginate_format,
              'total' => $wp_query->max_num_pages,
              'mid_size' => 4,
              'current' => ($paged ? $paged : 1),
              'prev_text' => '« 前へ',
              'next_text' => '次へ »',
          )); ?>
      </div>
        
        
      </section>
      
      
      
      <?php
        // 買取方法
        get_template_part('_purchase');
        
        // 買取基準
        //get_template_part('_criterion');
        
        // アクションポイント
        get_template_part('_action');
        
        // 店舗案内
        get_template_part('_shopinfo');
      ?>
      
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
