<?php
/**
 * BRING functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package BRING
 */

if ( ! function_exists( 'bring_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function bring_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on BRING, use a find and replace
	 * to change 'bring' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'bring', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'bring' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'bring_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // bring_setup
add_action( 'after_setup_theme', 'bring_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function bring_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'bring_content_width', 640 );
}
add_action( 'after_setup_theme', 'bring_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function bring_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'bring' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'bring_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function bring_scripts() {
  /*----- CSS -----*/
	wp_enqueue_style( 'bring-style-info', get_stylesheet_uri() );
	wp_enqueue_style( 'bootstrap-honoka', get_template_directory_uri().'/css/bootstrap.min.css');
	wp_enqueue_style( 'bring-style',      get_template_directory_uri().'/css/style.css');

	/*----- JavaScript -----*/
	// deregister default jQuery included with Wordpress
  wp_deregister_script( 'jquery' );
  $jquery_cdn = '//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js';
  wp_enqueue_script( 'jquery', $jquery_cdn, array(), '20130115', true );
	wp_enqueue_script( 'bootstrap-honoka', get_template_directory_uri().  '/js/bootstrap.min.js', array(), 'v3.3.6', true);
	wp_enqueue_script( 'bring-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'bring-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'bring_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/*==================================================
  自動整形防止
==================================================*/
function override_mce_options( $init_array ) {
    global $allowedposttags;

    $init_array['valid_elements']          = '*[*]';
    $init_array['extended_valid_elements'] = '*[*]';
    $init_array['valid_children']          = '+a[' . implode( '|', array_keys( $allowedposttags ) ) . ']';
    $init_array['indent']                  = true;
    $init_array['wpautop']                 = false;
    $init_array['force_p_newlines']        = false;

    return $init_array;
}

add_filter( 'tiny_mce_before_init', 'override_mce_options' );


/**
 * ====================================================
 * Help Contact Form 7 Play Nice With Bootstrap
 * ====================================================
 * Add a Bootstrap-friendly class to the "Contact Form 7" form
 */
add_filter( 'wpcf7_form_class_attr', 'wildli_custom_form_class_attr' );
function wildli_custom_form_class_attr( $class ) {
	$class .= ' form-horizontal';
	return $class;
}


// アイキャッチ有効化
add_theme_support('post-thumbnails');


// カスタム投稿タイプ
function custom_post_type_blog() {
  $labels = array(
    'name' => _x('ブログ記事', 'blogposts'),
    'singular_name' => _x('ブログ記事', 'blogpost'),
    'add_new' => _x('新しいブログ記事を追加', 'blog'),
    'add_new_item' => __('新しいブログ記事を追加'),
    'edit_item' => __('ブログ記事を編集'),
    'new_item' => __('新しいブログ記事'),
    'view_item' => __('ブログ記事を編集'),
    'search_items' => __('ブログ記事を探す'),
    'not_found' => __('ブログ記事はありません'),
    'not_found_in_trash' => __('ゴミ箱にブログ記事はありません'),
    'parent_item_colon' => ''
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'has_archive' => true,
    'supports' => array('title','editor', 'thumbnail')
  );
  register_post_type('blog',$args);

  // カスタムタクソノミー カテゴリ
  register_taxonomy (
    'blog-cat',
    'blog',
    array(
      'hierarchical' => true,
      'update_count_callback' => '_update_post_term_count',
      'label' => 'カテゴリー',
      'singular_label' => 'カテゴリー',
      'public' => true,
      'show_ui' => true
    )
  );
}

add_action('init', 'custom_post_type_blog');


function area() {
  $labels = array(
    'name' => _x('買取エリア', 'area'),
    'singular_name' => _x('買取エリア', 'area'),
    'add_new' => _x('新しい買取エリアを追加', 'area'),
    'add_new_item' => __('新しい買取エリアを追加'),
    'edit_item' => __('買取エリアを編集'),
    'new_item' => __('新しい買取エリア'),
    'view_item' => __('買取エリアを編集'),
    'search_items' => __('買取エリアを探す'),
    'not_found' => __('買取エリアはありません'),
    'not_found_in_trash' => __('ゴミ箱に買取エリアはありません'),
    'parent_item_colon' => ''
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 7,
    'has_archive' => true,
    'supports' => array('title','editor', 'thumbnail')
  );
  register_post_type('area',$args);

  // カスタムタクソノミー カテゴリ
  register_taxonomy (
    'area-cat',
    'area',
    array(
      'hierarchical' => true,
      'update_count_callback' => '_update_post_term_count',
      'label' => 'カテゴリー',
      'singular_label' => 'カテゴリー',
      'public' => true,
      'show_ui' => true
    )
  );
}
//カスタム投稿：買取実績
function purchase_items_cpt() {
  $args = array(
    'label' => '買取実績',
    'labels' => array(
    'singular_name' => '買取実績詳細',
    'add_new_item' => '買取実績を追加',
    'add_new' => '新規追加',
    'new_item' => '新規追加',
    'view_item' => '買取実績を表示',
    'not_found' => '買取実績は見つかりませんでした',
    'not_found_in_trash' => 'ゴミ箱に買取実績はありません。',
    'search_items' => '買取実績を検索',
    ),
    'public' => true,
    'has_archive' => true,
    'show_ui' => true,
    'query_var' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 5,
    'supports' => array('title','editor')
  );
  register_post_type('purchase_item', $args);

  $args = array(
    'label' => 'カテゴリ',
    'labels' => array(
    'name' => 'カテゴリ',
    'singular_name' => 'カテゴリ',
    'search_items' => 'カテゴリを検索',
    'popular_items' => 'よく使われているカテゴリ',
    'all_items' => 'すべてのカテゴリ',
    'parent_item' => '親カテゴリ',
    'edit_item' => 'カテゴリの編集',
    'update_item' => '更新',
    'add_new_item' => '新規カテゴリを追加',
    'new_item_name' => '新しいカテゴリ',
    ),
    'public' => true,
    'show_ui' => true,
    'hierarchical' => true,
  );
  register_taxonomy('purchase_item-cat', 'purchase_item', $args);


    $args = array(
    'hierarchical' => false, //タグタイプの指定（階層をもたない）
    'update_count_callback' => '_update_post_term_count',
    'label' => 'タグ',
    'public' => true,
    'show_ui' => true
  );
  register_taxonomy('purchase_item_tag', 'purchase_item', $args);




}


function custom_post_type() {
purchase_items_cpt();
area();
}

add_action('init', 'custom_post_type' );


function my_remove_post_support() {
remove_post_type_support('purchase_item','editor'); // 本文
}
add_action('init','my_remove_post_support');


/*==================================================
  LP用テンプレートショートカット
==================================================*/
function func_smart_customfield($attr) {
  extract(shortcode_atts(array('id' => ''), $attr));
  $text = nl2br(SCF::get($id));
  echo $text;
}
add_shortcode('scf', 'func_smart_customfield');


function func_smart_customfield_image($attr) {
  extract(shortcode_atts(array('id' => ''), $attr));
  $text = wp_get_attachment_image(SCF::get($id), 'full');
  return $text;
}
add_shortcode('scf_img', 'func_smart_customfield_image');


function func_smart_customfield_imageurl($attr) {
  extract(shortcode_atts(array('id' => ''), $attr));
  $text = wp_get_attachment_image_url(SCF::get($id), 'full');
  return $text;
}
add_shortcode('scf_imgurl', 'func_smart_customfield_imageurl');


function func_themplateurl() {
  echo get_stylesheet_directory_uri();
}
add_shortcode('themeurl', 'func_themplateurl');


function func_homeurl() {
  echo home_url();
}
add_shortcode('homeurl', 'func_homeurl');

/*==================================================
  LPブランド別サイドバー用
==================================================*/
function is_parent_slug() {
  global $post;
  if ($post->post_parent) {
    $post_data = get_post($post->post_parent);
    return $post_data->post_name;
  }
}

add_action( 'wp_head' , 'jquery_pass');
  function jquery_pass() {
    echo '<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>';
  };

add_action( 'wp_head' , 'numbering_js');

  function numbering_js() {
    $js_src = get_template_directory_uri() . '/js/numbering.js';
    $output = '<script src="' . $js_src . '"></script>';
    echo $output;
  };

add_action( 'wp_footer' , 'tracking_code');

  function tracking_code() {
    $tracking_js_src = get_template_directory_uri() . '/js/tracking_code.js';
    $output = '<script src="' . $tracking_js_src . '"></script>';
    echo $output;
  };

//ショートコードを使ったphpファイルの呼び出し方法
function my_php_Include($params = array()) {
extract(shortcode_atts(array('file' => 'default'), $params));
ob_start();
include(STYLESHEETPATH . "/$file.php");
return ob_get_clean();
}
add_shortcode('myphp', 'my_php_Include');

 function SearchFilter($query) {
 if ( !is_admin() && $query->is_main_query() && $query->is_search() ) {
  $query->set( 'post_type', 'page');
 }
}

add_action( 'pre_get_posts','SearchFilter' );

wp_enqueue_script( 'yubinbango', 'https://yubinbango.github.io/yubinbango/yubinbango.js', array(), null, true );


add_action( 'wp_footer', 'mycustom_wp_footer' );

function mycustom_wp_footer() {
?>
<script type="text/javascript">
document.addEventListener( 'wpcf7mailsent', function( event ) {
    if ( '22346' == event.detail.contactFormId ) {
        ga( 'send', 'event', 'Contact Form', 'submit' );
		location.replace('https://kaitorisatei.info/brandrevalue/thanks_purchase_test02');
    }
}, false );
</script>
<?php
}

// MW WP Formのクラスをyubinbangoのクラスに変更する
function mwform_form_class() {
?>
<script>
jQuery(function($) {
  $('.mw_wp_form form').attr( 'class', 'h-adr' );
});
</script>
<?php
}
add_action( 'wp_head', 'mwform_form_class', 10000 );

// MW WP Form の最後に採番されたお問い合わせ番号を取得する
function get_mwform_last_tracking_number($attributes) {
	$attributes = shortcode_atts(array('key' => ''), $attributes);

	$tracking_number = '';
	if (!empty( $attributes['key'])) {
		$tracking_number = get_post_meta($attributes['key'], MWF_Config::TRACKINGNUMBER, true);
		$tracking_number = $tracking_number ? ($tracking_number - 1) : "1";
	}

	return $tracking_number;
}
add_shortcode('mwform_last_tracking_number' , 'get_mwform_last_tracking_number');

function admin_favicon() {
 echo '<link rel="shortcut icon" type="image/x-icon" href="' . get_template_directory_uri() . '/img/favicon.ico" />';
}

add_action('admin_head', 'admin_favicon');

global $wp_rewrite;
$wp_rewrite->flush_rules();

function get_s3_template_directory_uri() {
  $template_dir = get_template_directory_uri();
  $s3_dir = str_replace('://', '://img.', $template_dir);
  return $s3_dir;
}
