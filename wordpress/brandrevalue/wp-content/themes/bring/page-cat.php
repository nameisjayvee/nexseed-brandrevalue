<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @see https://codex.wordpress.org/Template_Hierarchy
 */
get_header(); ?>

	<div id="primary" class="cat-page content-area">
		<main id="main" class="site-main" role="main">
      <section id="mainVisual" style="background:url(<?php echo get_s3_template_directory_uri(); ?>/img/mv/cat.png)">
        <h2 class="text-hide">取扱いカテゴリー</h2>
      </section>

      <section id="catchcopy">
        <h3>貴金属、ダイヤモンド、時計、毛皮など、幅広いお品物を買い取ります</h3>
        <p>「店舗に足を運んだり、郵送の手続きをする時間がない」という方にお勧めしたいのが、「出張買取サービス」です。<br>お申込み後「最短即日」でBRAND REVALUEの鑑定スタッフがお客様のご自宅までお伺いし、その場でお品物をスピード査定。
        金、宝石、時計、バッグ、財布、毛皮、洋服など、価値あるお品物を、どこよりも高く買い取りさせていただきます。
        ロレックス、エルメス、ルイ・ヴィトン、プラダ、グッチといった代表的なブランドはもちろん、その他の多くのブランドについても積極的に取り扱っております。
        1000万円以上の超高級品の買取も可能。自宅に不要なブランド品があるけれど、高価な品物だから信頼できるお店でなるべく高く売りたい、そんな方にもBRAND REVALUEの買取りサービスは必ずご納得いただけると思います。
        出張買取は家から一歩も出ることなく、店舗と同様のスピーディな買取サービスをご利用いただける、「一番楽ちん」なスタイルです。</p>
      </section>

      <section class="kaitori_cat">
    <ul class="top_kaitori_bn">
        <li class="gold">
            <a href="<?php echo home_url('cat/gold'); ?>">
                <h3>金・プラチナ<span>GOLD・PLATINUM</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="jewelry">
            <a href="<?php echo home_url('cat/gem'); ?>">
                <h3>宝石<span>GEM</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="watch">
            <a href="<?php echo home_url('cat/watch'); ?>">
                <h3>時計<span>BRAND WATCH</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="bag">
            <a href="<?php echo home_url('cat/bag'); ?>">
                <h3>バッグ<span>BAG</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="outfit">
            <a href="<?php echo home_url('cat/outfit'); ?>">
                <h3>洋服・毛皮<span>OUTFIT</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="wallet">
            <a href="<?php echo home_url('cat/wallet'); ?>">
                <h3>ブランド財布<span>BRAND WALLET</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="shoes">
            <a href="<?php echo home_url('cat/shoes'); ?>">
                <h3>靴<span>SHOES</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="diamond">
            <a href="<?php echo home_url('cat/diamond'); ?>">
                <h3>ダイヤモンド<span>DIAMOND</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="kimono">
            <a href="<?php echo home_url('kimono'); ?>">
                <h3>着物<span>KIMONO</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="antique">
            <a href="<?php echo home_url('cat/antique'); ?>">
                <h3>骨董品<span>ANTIQUE</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="add bj_bnr">
            <a href="<?php echo home_url('cat/brandjewelery'); ?>">
                <h3><span class="lh">ブランド<br>ジュエリー</span><span>Brand jewelery</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="add an_rolex">
            <a href="<?php echo home_url('cat/antique_rolex'); ?>">
                <h3><span class="lh">アンティーク<br>ロレックス</span><span>Antique ROLEX</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li><a href="<?php echo home_url('cat/mement'); ?>"><img src="<?php echo get_s3_template_directory_uri(); ?>/img/top/top-category-memento.png" alt="遺品 買取"></a></li>
    </ul>
</section>
      <section>
          <img src="<?php echo get_s3_template_directory_uri(); ?>/img/cat/bn_matomegai.png">
      </section>

      <?php
        // 買取基準
        get_template_part('_criterion');

        // NGアイテム
        get_template_part('_no_criterion');

        // アクションポイント
        get_template_part('_action');

        // 買取方法
        get_template_part('_purchase');

        // 店舗案内
        get_template_part('_shopinfo');
      ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
