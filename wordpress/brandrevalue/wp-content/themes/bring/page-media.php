<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>

	<div id="primary" class="cat-page content-area">
		<main id="main" class="site-main" role="main">
      <section id="no-mainVisual" style="background:url()">
        <h2 class="">メディア情報</h2>
      </section>
      
      <section id="catchcopy">
        <!--<h3></h3>-->
        <p>現在メディア情報はございません</p>
      </section>
      
      <?php
        // 買取基準
        get_template_part('_criterion');
        
        // アクションポイント
        get_template_part('_action');
        
        // 買取方法
        get_template_part('_purchase');
        
        // 店舗案内
        get_template_part('_shopinfo');
      ?>
      
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
