<section>
  <div class="cont_widerange">
      <h4><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/about_kaitori.jpg" alt="ブランドバッグ、時計、アクセサリーなど、幅広いお品物を買い取ります。"></h4>
      <ul>
          <li class="left_tx">前職は、営業としてメーカーに勤務しておりました。鑑定士を志すきっかけは、幼い頃からブランド品やジュエリーなどを扱う仕事に憧れを持っていたからです。鑑定士となった今、一番に意識していることは、お客様の笑顔です。お客様の笑顔こそが私たちの仕事のやりがいであり、大切な思い出となります。「また来たいな」と思っていただけるよう、今後も鑑定士としての腕を磨き、最高の接客でお客様をおもてなししたいと思います。BRAND REVALUEは、今までの買取店の概念を変える「高級サロン」のような内装、空間になっております。買取店が初めてというお客様も、ぜひ一度足を運んでいただけたらと思います。これからもお客様にご満足いただけるよう、スタッフ一同努力して参りますので、よろしくお願い致します。ご来店、お問合せ心よりお待ちしております。</li>
          <li class="right_ph"><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/img_about_kaitori.jpg"></li>
      </ul>
  </div>
</section>