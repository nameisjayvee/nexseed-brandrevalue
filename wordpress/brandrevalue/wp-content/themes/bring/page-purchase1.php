<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>

	<div id="primary" class="form-purchase content-area">
		<main id="main" class="site-main" role="main">
      <section id="mainVisual" style="background:url(<?php echo get_s3_template_directory_uri() ?>/img/mv/purchase.png)">
        <h2 class="text-hide">査定希望お申し込み</h2>
      </section>
      
		  <div class="purchase1-bnr-box"><p class="purchase1-bnr-Txt"><span id="phone_number_holder_5">0120-970-060</span></p></div>
		  <p><a href="http://kaitorisatei.info/brandrevalue/line"><img src="<?php echo get_s3_template_directory_uri() ?>/img/mv/purchase1_bnr02.jpg" alt="LINE査定"></a></p>
      
      <section>
        <div class="mailform_new02">
          <?php echo do_shortcode('[mwform_formkey key="23208"]'); ?>
        </div>
      </section>
<div class="textarea">
※メールでのお問い合わせ・査定は、原則的に１営業日以内にご連絡を差し上げるよう努めております。
多くのお問い合わせを頂戴している場合や店舗の混雑状況により、ご回答が遅れる可能性がございます。<br>
当店から返信がない場合には、お手数ではございますが、電話またはＬＩＮＥにてお問い合わせくださいませ。<br><br>
</div>
      <?php
        // アクションポイント
        get_template_part('_action');
        
        // 買取方法
        get_template_part('_purchase');
        
        // 店舗案内
        get_template_part('_shopinfo');
      ?>
      
		</main><!-- #main -->
	</div><!-- #primary -->

<script src="http://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script type="text/javascript">
(function () {

    "use strict";

    /**
     * Pardot form handler assist script.
     * Copyright (c) 2018 toBe marketing, inc.
     * Released under the MIT license
     * http://opensource.org/licenses/mit-license.php
     */

    // == 設定項目ここから ==================================================================== //

    /*
     * ＠データ取得方式
     */
    var useGetType = 1;

    /*
     * ＠フォームハンドラーエンドポイントURL
     */
    var pardotFHUrl = 'https://go.pardot.com/l/436102/2017-12-13/d5lyjf';

    /*
     * ＠フォームのセレクタ名
     */
    var parentFormName = '#mw_wp_form_mw-wp-form-23208 > form';

    /*
     * ＠送信ボタンのセレクタ名
     */
    var submitButton = 'input[type=submit][name=confirm]';

    /*
     * ＠項目マッピング
     */

    var defaultFormJson = [
      { "tag_name": "sei",      "x_target": 'sei' },
      { "tag_name": "mei",      "x_target": 'mei' },
      { "tag_name": "kana-sei", "x_target": 'kana-sei' },
      { "tag_name": "kana-mei", "x_target": 'kana-mei' },
      { "tag_name": "email",    "x_target": 'email' },
      { "tag_name": "tel",      "x_target": 'tel' },
      { "tag_name": "bikou",    "x_target": 'bikou' }
    ];

    // == 設定項目ここまで ==================================================================== //

    // 区切り文字設定
    var separateString = ',';

    var iframeFormData = '';
    for (var i in defaultFormJson) {
        iframeFormData = iframeFormData + '<input id="pd' + defaultFormJson[i]['tag_name'] + '" type="text" name="' + defaultFormJson[i]['tag_name'] + '"/>';
    }

    var iframeHeadSrc =
        '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';

    var iframeSrcDoc =
        '<form id="shadowForm" action="' + pardotFHUrl + '" method="post" accept-charset=\'UTF-8\'>' +
        iframeFormData +
        '<input id="shadowSubmit" type="submit" value="send" onclick="document.charset=\'UTF-8\';"/>' +
        '</form>';

    var shadowSubmited = false;
    var isError = false;
    var pdHiddenName = 'pdHiddenFrame';

    /**
     * エスケープ処理
     * @param val
     */
    function escapeSelectorString(val) {
        return val.replace(/[ !"#$%&'()*+,.\/:;<=>?@\[\\\]^`{|}~]/g, "\\$&");
    }

    $(function () {

        $(submitButton).click(function () {
            // Submitボタンを無効にする。
            $(submitButton).prop("disabled", true);
        });

        $('#' + pdHiddenName).remove();

        $('<iframe>', {
            id: pdHiddenName
        })
            .css({
                display: 'none'
            })
            .on('load', function () {

                if (shadowSubmited) {
                    if (!isError) {

                        shadowSubmited = false;

                        // 送信ボタンを押せる状態にする。
                        $(submitButton).prop("disabled", false);

                        // 親フォームを送信
                        $(parentFormName).submit();
                    }
                } else {
                    $('#' + pdHiddenName).contents().find('head').prop('innerHTML', iframeHeadSrc);
                    $('#' + pdHiddenName).contents().find('body').prop('innerHTML', iframeSrcDoc);

                    $(submitButton).click(function () {
                        shadowSubmited = true;
                        try {
                            for (var j in defaultFormJson) {
                                var tmpData = '';

                                // NANE値取得形式
                                if (useGetType === 1) {
                                    $(parentFormName + ' [name="' + escapeSelectorString(defaultFormJson[j]['x_target']) + '"]').each(function () {

                                        //checkbox,radioの場合、未選択項目は送信除外
                                        if (["checkbox", "radio"].indexOf($(this).prop("type")) >= 0) {
                                            if ($(this).prop("checked") == false) {
                                                return true;
                                            }

                                        }

                                        if (tmpData !== '') {
                                            tmpData += separateString;
                                        }

                                        // 取得タイプ 1 or Null(default) :val()方式, 2:text()形式
                                        if (defaultFormJson[j]['x_type'] === 2) {
                                            tmpData += $(this).text().trim();
                                        } else {
                                            tmpData += $(this).val().trim();
                                        }
                                    });
                                }

                                // セレクタ取得形式
                                if (useGetType === 2) {
                                    $(defaultFormJson[j]['x_target']).each(function () {
                                        if (tmpData !== '') {
                                            tmpData += separateString;
                                        }

                                        // 取得タイプ 1 or Null(default) :val()方式, 2:text()形式
                                        if (defaultFormJson[j]['x_type'] === 2) {
                                            tmpRegexData = $(this).text().trim();
                                        } else {
                                            tmpRegexData = $(this).val().trim();
                                        }
                                    });
                                }

                                $('#' + pdHiddenName).contents().find('#pd' + escapeSelectorString(defaultFormJson[j]['tag_name'])).val(tmpData);

                            }

                            $('#' + pdHiddenName).contents().find('#shadowForm').submit();

                        } catch (e) {
                            isError = true;

                            $(submitButton).prop("disabled", false);

                            $(parentFormName).submit();
                            shadowSubmited = false;
                        }
                        return false;
                    });
                }
            })
            .appendTo('body');
    });
})();
</script>

<?php
get_sidebar();
get_footer();
