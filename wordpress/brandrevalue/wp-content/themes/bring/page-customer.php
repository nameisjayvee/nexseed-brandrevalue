<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


get_header(); ?>

    <div id="primary" class="about-purchase content-area">
        <main id="main" class="site-main" role="main">
            <section> <img src="<?php echo get_s3_template_directory_uri() ?>/img/customer/mv.png"> </section>
            <section class="customer_cv_box">
                <p class="custom_tx">ブランドリバリューのカスタマーサポートセンターは、<br /> お客様のご不明点やご相談など丁寧かつ分かりやすくご案内致します。
                </p>
                <div class="custom_cv_box01_wrap">
                    <p class="tx_center"><img src="<?php echo get_s3_template_directory_uri() ?>/img/customer/cv01_ttl.png" alt="お申し込みはこちら"></p>
                    <ul class="custom_cv_box01">
                        <li>
                            <a href="tel:0120-970-060">
                                <p>電話査定</p>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo home_url('line'); ?>">
                                <p>LINE査定</p>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo home_url('purchase1'); ?>">
                                <p>メール査定</p>
                            </a>
                        </li>
                    </ul>
                    <p class="tx_center tx_green">最新情報をもとにおおよその査定金額をご案内いたします。<br /> ささいな疑問やご相談にもお答えいたします。 </p>
                    <ul class="cv_box01_list">
                        <li>店舗の混雑状況は？</li>
                        <li>買取の申し込み方法は？</li>
                        <li>保証書や状態について</li>
                        <li>店舗はどこにあるの？</li>
                        <li>どんな買取方法があるの？</li>
                    </ul>
                </div>
                <div class="custom_cv_box02_wrap">
                    <p class="tx_center"><img src="<?php echo get_s3_template_directory_uri() ?>/img/cv01/cv02_ttl.png" alt="お申し込みはこちら"></p>
                    <ul class="custom_cv_box02">
                        <li>
                            <a href="<?php echo home_url('purchase/tentou'); ?>">
                                <p>駅前だから便利!</p>
                                <p>店頭買取</p>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo home_url('purchase/takuhai'); ?>">
                                <p>忙しい方にオススメ!</p>
                                <p>宅配買取</p>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo home_url('purchase/syutchou'); ?>">
                                <p>点数が多い方にぴったり!</p>
                                <p>出張買取</p>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="custom_cv_box03_wrap">
                    <p class="tx_center"><img src="<?php echo get_s3_template_directory_uri() ?>/img/cv01/cv03_ttl.png" alt="お申し込みはこちら"></p>
                    <div class="cdt_wrap">
                        <div id="CDT"></div>
                    </div>
                    <script language="JavaScript" type="text/javascript">
                        function CountdownTimer(elm, tl, mes) {
                            this.initialize.apply(this, arguments);
                        }
                        CountdownTimer.prototype = {
                            initialize: function(elm, tl, mes) {
                                this.elem = document.getElementById(elm);
                                this.tl = tl;
                                this.mes = mes;
                            },
                            countDown: function() {
                                var timer = '';
                                var today = new Date();
                                var day = Math.floor((this.tl - today) / (24 * 60 * 60 * 1000));
                                var hour = Math.floor(((this.tl - today) % (24 * 60 * 60 * 1000)) / (60 * 60 * 1000));
                                var min = Math.floor(((this.tl - today) % (24 * 60 * 60 * 1000)) / (60 * 1000)) % 60;
                                var sec = Math.floor(((this.tl - today) % (24 * 60 * 60 * 1000)) / 1000) % 60 % 60;
                                var milli = Math.floor(((this.tl - today) % (24 * 60 * 60 * 1000)) / 10) % 100;
                                var me = this;

                                if ((this.tl - today) > 0) {
                                    if (day) timer += 'あと<span class="day">' + day + '日</span>';
                                    if (hour) timer += '<span class="hour">' + hour + '時間</span>';
                                    timer += '<span class="min">' + this.addZero(min) + '分</span><span class="sec">' + this.addZero(sec) + '秒</span><span class="milli">' + this.addZero(milli) + '</span>';
                                    this.elem.innerHTML = timer;
                                    tid = setTimeout(function() {
                                        me.countDown();
                                    }, 10);
                                } else {
                                    this.elem.innerHTML = this.mes;
                                    return;
                                }
                            },
                            addZero: function(num) {
                                return ('0' + num).slice(-2);
                            }
                        }

                        function CDT() {
                            var tl = new Date('2019/4/20 00:00:00');
                            var timer = new CountdownTimer('CDT', tl, '終了しました');
                            timer.countDown();
                        }
                        window.onload = function() {
                            CDT();
                        }

                    </script>
                </div>
            </section>
            <div class="custom_tel">
                <a href="tel:0120-970-060">
                    <div class="tel_wrap">
                        <div class="telbox01"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/customer/telico.png" alt="お申し込みはこちら">
                            <p>お電話からでもお申込み可能！<br>ご不明な点は、お気軽にお問合せ下さい。 </p>
                        </div>
                        <div class="telbox02"> <span class="small_tx">【受付時間】11:00 ~ 21:00</span><span class="ted_tx"> 年中無休</span>
                            <p>0120-970-060</p>
                        </div>
                    </div>
                </a>
            </div>
            <a href="<?php echo home_url('line'); ?>"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/customer/line_bnr.png" alt="お申し込みはこちら"></a> </section>
            <section class="customer_qa_box">
                <h2 class="new_ttl">ブランドリバリュー Q&amp;A</h2>
                <div class="qa_wrap">
                    <ul>
                        <li>
                            <p>Q.どんな物が売れるのか知りたい</p>
                            <p><span>A</span>ブランドバッグ時計ジュエリー貴金属アパレル食器骨董品等、中古取引市場で取引されているものは全てご売却可能です。</p>
                        </li>
                        <li>
                            <p>Q.付属品（保証書、箱、ストラップ等）が無い物でも買取は可能？</p>
                            <p><span>A</span>付属品が無くても全く問題ありません。お品物のみでも可能です。</p>
                        </li>
                        <li>
                            <p>Q.本物かどうか分からない商品を見てもらうことは可能？</p>
                            <p><span>A</span>長年の販売実績に基づく買取基準に応じて、弊社鑑定士が鑑定させていただきますので、ご安心してお持ち込みください。</p>
                        </li>
                        <li>
                            <p>Q.汚れている物、壊れている物でも買取は可能？</p>
                            <p><span>A</span>汚れていても壊れていてもお買取り可能でございます。弊社では一部修復し販売しておりますので、高価買取させていただいております。</p>
                        </li>
                        <li>
                            <p>Q.買取金額が高い商品はどんなもの？</p>
                            <p><span>A</span>基本的には、人気（需要）が高いものは買取金額が高い傾向にあります。<br /> 例えば、時計の廃盤になったもの（ロレックス　デイトナ等）が今でも人気があったり、昔からの定番商品で盤石の人気がある商品等（シャネル　マトラッセ　チェーンバッグ等）は買取金額が高い傾向にあります。
                            </p>
                        </li>
                        <li>
                            <p>Q.高く売れる時期はあるの？</p>
                            <p><span>A</span>中古取引相場は需要と共有のバランスは当然ながら為替相場や金相場等、経済的政治的な要因によって変動いたします。<br /> 従って一概には高く売れる時期は申し上げにくいですが、中古商品が多く取引される年末（11月、12月）あたりは比較的高い傾向にあります。ただし、上記記載の通り需要と共有のバランスが前提ですので、色や素材の突発的な人気の高まり等がございますので、是非弊社鑑定士にご相談いただければと存じます。
                            </p>
                        </li>
                        <li>
                            <p>Q.どうやって物を売ればいいの？</p>
                            <p><span>A</span>弊社では店舗（銀座または渋谷）、郵送、出張でのお買取りが可能です。<br /> 店舗は銀座及び渋谷も駅から5分圏内との好立地のため重い商品も持ち運び可能です。その場で現金をお支払いいたします。郵送は無料で宅配キットを配送し、返送後査定を行い電話にてご相談いたします。金額が折り合わなければ即日返送、折り合えば即日振込いたします。出張は弊社鑑定士がお宅にお伺いし、その場で査定その場でお支払いいたします。
                            </p>
                        </li>
                        <li>
                            <p>Q.高額な商品なので郵送は嫌だけど、商品持っていくのは重いし、、</p>
                            <p><span>A</span>出張でのお買取りも可能ですが、即日査定をご希望の場合はタクシー代を負担※しておりますので、タクシーをご利用ください。領収書をお持ち込みいただければ査定額に上乗せしてお支払いいたします。<br /> ※タクシー代は弊社店舗までのタクシー代を一部負担させていただいております。商品によっては負担とならない場合（低単価商品等）がございますのでご了承ください。
                            </p>
                        </li>
                        <li>
                            <p>Q.買取時に必要なものはある？</p>
                            <p><span>A</span>公的な身分証をご提示ください。（運転免許証、保険証、パスポート、住基カード等）また、未成年の場合は親権者の同意書が必要となります。</p>
                        </li>
                        <li>
                            <p>Q.他社よりも高価買取できる理由は?</p>
                            <p><span>A</span>弊社は海外含め豊富な販売先との提携を行っております。お客様の商品が一番高く売れる相場を熟知しているため高価買取が可能となっております。</p>
                        </li>
                    </ul>
                </div>
            </section>
            <p class="cam_tx_center">上記内容以外のお問合せ、その他気になる事が御座いましたら下記からお問い合わせ下さいませ。</p>
            <ul class="cv_box01">
                <li>
                    <a href="tel:0120970060">
                        <p class="cv01_ttl">お電話でのお問い合わせ</p>
                        <p>0120-970-060</p>
                        <p>【受付時間】11:00 ~ 21:00 ※年中無休</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo home_url('line'); ?>">
                        <p class="cv01_ttl">LINEでカンタン査定!</p>
                        <p>最短30秒でのお申し込み!</p>
                        <p>LINE査定についてはこちら<i class="fas fa-caret-right fa-lg faa-horizontal "></i></p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo home_url('purchase1'); ?>">
                        <p class="cv01_ttl">メールで簡単査定</p>
                        <p>パソコン・スマホから24時間受付 !</p>
                        <p><img src="<?php echo get_s3_template_directory_uri() ?>/img/cv01/btn.png" alt="お申し込みはこちら"></p>
                    </a>
                </li>
            </ul>
            <section>
                <h2 class="new_ttl">ブランドリバリューの高価買取実績！他社には負けません！</h2>
                <ul class="custom_item">
                    <li>
                        <a href="<?php echo home_url('/cat/watch'); ?>">
                            <p><img src="<?php echo get_s3_template_directory_uri() ?>/img/campaign/item01.png" alt="ロレックス デイトナ 16520"></p>
                            <p>ロレックス デイトナ<br /> 16520
                            </p>
                            <p>買取価格<span>¥2,000,000</span></p>
                            <p>詳しくはこちら<i class="fas fa-caret-right fa-lg faa-horizontal "></i></p>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo home_url('cat/bag'); ?>">
                            <p><img src="<?php echo get_s3_template_directory_uri() ?>/img/campaign/item02.png" alt="バーキン30 トゴ 黒"></p>
                            <p>バーキン30 トゴ 黒</p>
                            <p>買取価格<span>¥1,700,000</span></p>
                            <p>詳しくはこちら<i class="fas fa-caret-right fa-lg faa-horizontal "></i></p>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo home_url('cat/brandjewelery'); ?>">
                            <p><img src="<?php echo get_s3_template_directory_uri() ?>/img/campaign/item03.png" alt="ハリーウィンストンリリークラスター ピアス"></p>
                            <p>ハリーウィンストン<br /> リリークラスター ピアス</p>
                            <p>買取価格<span>¥900,000</span></p>
                            <p>詳しくはこちら<i class="fas fa-caret-right fa-lg faa-horizontal "></i></p>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo home_url('cat/gem'); ?>">
                            <p><img src="<?php echo get_s3_template_directory_uri() ?>/img/campaign/item04.png" alt="サファイヤダイヤリング"></p>
                            <p>サファイヤダイヤリング</p>
                            <p>買取価格<span>¥500,000</span></p>
                            <p>詳しくはこちら<i class="fas fa-caret-right fa-lg faa-horizontal "></i></p>
                        </a>
                    </li>
                </ul>
            </section>
        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->

    <?php
get_sidebar();
get_footer();
