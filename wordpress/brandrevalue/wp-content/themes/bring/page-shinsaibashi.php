<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  'watch/top/001.jpg::パテックフィリップ::カラトラバ 3923::646,000::640,000::650,000::10,000',
  'watch/top/002.jpg::パテックフィリップ::アクアノート 5065-1A::1,367,000::1,350,000::1,400,000::50,000',
  'watch/top/003.jpg::オーデマピゲ::ロイヤルオーク・オフショア 26170ST::1,200,000::1,195,000::1,210,000::15,000',
  'watch/top/004.jpg::ROLEX::サブマリーナ 116610LN ランダム品番::720,000::700,000::740,000::40,000',
  'watch/top/005.jpg::ROLEX::デイトナ 116505 ランダム品番 コスモグラフ::1,960,000::1,950,000::2,000,000::50,000',
  'watch/top/006.jpg::ブライトリング::クロノマット44::334,000::328,000::340,000::12,000',
  'watch/top/007.jpg::パネライ::ラジオミール 1940::594,000::590,000::600,000::10,000',
  'watch/top/008.jpg::ボールウォッチ::ストークマン ストームチェイサープロ CM3090C::128,000::125,000::130,000::5,000',
  'watch/top/009.jpg::ブレゲ::クラシックツインバレル 5907BB12984::615,000::610,000::626,000::16,000',
  'watch/top/010.jpg::ウブロ::ビッグバン ブラックマジック::720,000::695,000::740,000::45,000',
  'watch/top/011.jpg::オメガ::シーマスター プラネットオーシャン 2208-50::192,000::190,000::200,000::10,000',
  'watch/top/012.jpg::ディオール::シフルルージュ クロノグラフ CD084612 M001::190,000::180,000::200,000::20,000',
);

get_header(); ?>

    <div id="primary" class="cat-page content-area">
        <main id="main" class="site-main" role="main">
            <section id="mainVisual" style="background:url(<?php echo get_s3_template_directory_uri() ?>/img/shop/shinsaibashi_mv.png)">
                <h2 class="text-hide">心斎橋店</h2>
            </section>
            <section id="catchcopy">
            
                <h3>銀座・渋谷・新宿で高価買取実績のブランドリバリュー（BRAND REVALUE）がついに大阪心斎橋でオープン！ルイヴィトンの目の前という好立地で御堂筋線「心斎橋駅」3番出口から徒歩3秒！</h3>
                <p>最寄り駅は、地下鉄御堂筋線「心斎橋駅」。心斎橋駅3番出口徒歩3秒のビルに店舗を構えております。駅上の好立地から商店街を通ることもなく商品の持ち運びはラクラク。お気軽にご利用いただけます。
                </p>
            </section>
            <section class="shop_gal">
                 <h2 class="shop_ttl">ブランドリバリュー心斎橋店 店舗案内</h2>
                <!--<div class="link_gr ">
                    <a href="https://www.google.co.jp/maps/uv?hl=ja&pb=!1s0x60188be68aa53555%3A0x7eb5bd0982deca18!2m22!2m2!1i80!2i80!3m1!2i20!16m16!1b1!2m2!1m1!1e1!2m2!1m1!1e3!2m2!1m1!1e5!2m2!1m1!1e4!2m2!1m1!1e6!3m1!7e115!4shttps%3A%2F%2Flh5.googleusercontent.com%2Fp%2FAF1QipNbicAD6rZpXueDIS-O1YR57JJcS6d599LpezKM%3Dw264-h176-k-no!5z44OW44Op44Oz44OJ44Oq44OQ44Oq44Ol44O8IOmKgOW6pyAtIEdvb2dsZSDmpJzntKI&imagekey=!1e10!2sAF1QipNbWblj6EBwQxMqtXGfryq_WaBUIVHU6df6TnfL&sa=X&ved=2ahUKEwjN18KS6sPdAhXFvbwKHTCVBEEQoiowDXoECAgQCQ" target="_blank" class="hiragino">店舗内をみる<i class="fas fa-caret-right"></i></a>

                </div>-->
                
                <ul class="shop_main">
                    <li class="item1"><img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shinsaibashi/shinsaibashi01.png" alt="銀座店"></li>
                    <li class="item2"><img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shinsaibashi/shinsaibashi02.png" alt="銀座店"></li>
                    <li class="item3"><img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shinsaibashi/shinsaibashi03.png" alt="銀座店"></li>
                    <li class="item4"><img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shinsaibashi/shinsaibashi04.png" alt="銀座店"></li>
                </ul>
                <ul class="thumb">
                    <li class="thumb1"><img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shinsaibashi/shinsaibashi01.png" alt="銀座店"></li>
                    <li class="thumb2"><img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shinsaibashi/shinsaibashi02.png" alt="銀座店"></li>
                    <li class="thumb3"><img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shinsaibashi/shinsaibashi03.png" alt="銀座店"></li>
                    <li class="thumb4"><img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shinsaibashi/shinsaibashi04.png" alt="銀座店"></li>
                </ul>
            </section>
            <section class="shop_page ovf">
                <div class="map_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shinsaibashi/shinsaibashi_map.jpg" alt="銀座店"></div>
                <table class="info_table">
                    <tr>
                        <th>住所</th>
                        <td class="shop_map">
                            〒542-0081<br>大阪府大阪市中央区南船場4-4-8 クリエイティブ心斎橋8階</td>
                    </tr>
                    <tr>
                        <th>営業時間</th>
                        <td>11：00～21：00</td>
                    </tr>
                    <tr>
                        <th>電話番号</th>
                        <td><a href="tel:0120-970-060">0120-970-060</a></td>
                    </tr>
                    <tr>
                        <th>駐車場</th>
                        <td> - </td>
                    </tr>
                </table>
            <div class="link_map ">
                    <a href="https://goo.gl/maps/3JQXWvJ3idF2" target="_blank" class="hiragino">MAPを見る<i class="fas fa-caret-right"></i></a>
                </div>
            </section>
			
            <section>
                <h3 class="mb30">心斎橋店までのアクセス</h3>
                <p><span class="tx_bold">最寄り駅は地下鉄御堂筋線・四つ橋線・長堀鶴見緑地線が通る「心斎橋駅」。</span><br /> BRANDREVALUE(ブランドリバリュー)心斎橋店は地下鉄御堂筋線北改札目の前の3番出口を出てすぐ右側の建物に店舗を構えております。最寄り駅出口から目の前と、非常にアクセスしやすい場所にあり、近くにはルイヴィトン等のブランドショップから、大丸、心斎橋筋商店街等、お買い物、お食事、ついででお越しの際にも便利にご利用頂けます。</p>
                <ul class="map_root ">
                    <li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_shinsaibashi01.jpg" alt="地下鉄御堂筋線心斎橋駅北改札" />
                        <div class="root_tx_bx">
                            <p class="root_ttl"><span>1</span>地下鉄御堂筋線心斎橋駅北改札</p>
                            <p>改札を出て3番出口方面へお進みください。
                            </p>
                        </div>
                    </li>
                    <li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_shinsaibashi02.jpg" alt="A5出口" />
                        <div class="root_tx_bx">
                            <p class="root_ttl"><span>2</span>3番出口</p>
                            <p>3番出口から地上までお上がり下さい。</p>
                        </div>
                    </li>
                    <li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_shinsaibashi03.jpg" alt="地上" />
                        <div class="root_tx_bx">
                            <p class="root_ttl"><span>3</span>3番出口地上</p>
                            <p>3番出口を出てすぐ右側の建物にクリエイティブ心斎橋の建物があります。</p>
                        </div>
                    </li>
                    <li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_shinsaibashi04.jpg" alt="四谷学院ビル前" />
                        <div class="root_tx_bx">
                            <p class="root_ttl"><span>4</span>クリエイティブ心斎橋ビル前</p>
                            <p>3番出口を出て右側目の前にクリエイティブ心斎橋ビルがございます。その建物の8階が当店となります。 </p>
                        </div>
                    </li>

                </ul>
            </section>
			<div class="takuhai_cvbox tentou">
                <p class="cv_tl">ブランドリバリュー心斎橋店で店頭買取予約をする</p>
                <div class="custom_tel takuhai_tel">
                    <a href="tel:0120-970-060">
                        <div class="tel_wrap">
                            <div class="telbox01"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/customer/telico.png" alt="お申し込みはこちら">
                                <p>お電話からでもお申込み可能！<br>ご不明な点は、お気軽にお問合せ下さい。 </p>
                            </div>
                            <div class="telbox02"> <span class="small_tx">【受付時間】11:00 ~ 21:00</span><span class="ted_tx"> 年中無休</span>
                                <p>0120-970-060</p>
                            </div>
                        </div>
                    </a>
                </div>
                <p><a href="<?php echo home_url('purchase/visit-form'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/tentou_cv01.png" alt="店頭買取お申し込み"></a></p>
            </div>

            <section class="mb50">
                <h3 class="mb30">心斎橋店　店内紹介・内装へのこだわり</h3>
                <div class="shop_comm"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/kodawari_shinsaibashi01.png" alt="店内" />
                    <p>ブランドリバリュー心斎橋店では、お客さまが快適に過ごして頂ける空間と最上級のサービスでおもてなし致します。 </p>
                </div>
                <div class="shop_comm"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/kodawari_shinsaibashi02.png" alt="店内" />
                    <p>混雑状況によっては店内で少々お待たせしてしまう場合もあるかと思いますが少しでも落ち着けるよう、ソファー席をご用意しております。</p>
                </div>
                <div class="shop_comm"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/kodawari_shinsaibashi03.png" alt="店内" />
                    <p>また、査定はお客さまのプライバシーをお守りするために完全個室の査定ブースを設けさせて頂いておりますので、お持ち込み頂いたお品物についてのご質問やご相談・要望などありましたら遠慮なくバイヤーへお申し付け下さい。 </p>
                </div>
            </section>
            <section>
            
                <h3>心斎橋店　鑑定士のご紹介</h3>
                <!--<div class="staff_bx"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/staff01_shinjuku.png" alt="天野 豪仁 鑑定士" />
                    <p class="staff_name">高田 啓介 鑑定士</p>
                    <h4>仕事における得意分野</h4>
                    <p>バッグと時計。バッグと時計は使用する方によって様々な顔を見せるため、 非常に面白味があり、得意分野としております。</p>
                    <h4>自身の強み</h4>
                    <p>前職が金融機関に勤めていた経験から多様な人脈があり、高値での買取を可能と しております。</p>
                    <h4>仕事にかける思いと心がけ</h4>
                    <p>真摯に取り組む。その一点です。</p>
                </div>-->
<div class="staff_bx"> <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2017/07/staff04.png" alt="高田 啓介 鑑定士" />
    <p class="staff_name">高田 啓介 鑑定士</p>
    <h4>仕事における得意分野</h4>
    <p>バッグと時計。バッグと時計は使用する方によって様々な顔を見せるため、 非常に面白味があり、得意分野としております。</p>
    <h4>自身の強み</h4>
    <p>前職が金融機関に勤めていた経験から多様な人脈があり、高値での買取を可能と しております。</p>
    <h4>仕事にかける思いと心がけ</h4>
    <p>真摯に取り組む。その一点です。</p>
</div>

            </section>
            <section id="top-jisseki">
                <h3>
                    心斎橋店の買取実績
                </h3>
                <div class="full_content">

                    <!-- ブランド買取 -->
                    <div class="menu hover"><span class="glyphicon glyphicon-tag"></span> ブランド買取</div>
                    <div class="content">
                        <!-- box -->
                        <ul id="box-jisseki" class="list-unstyled clearfix">
                            <!-- brand1 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/001.jpg" alt="">
                                    <p class="itemName">オーデマピゲ　ロイヤルオークオフショアクロノ 26470OR.OO.A002CR.01 ゴールド K18PG</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：3,120,000円<br>
                                        <span class="blue">B社</span>：3,100,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">3,150,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>50,000円</p>
                                </div>
                            </li>

                            <!-- brand2 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/002.jpg" alt="">
                                    <p class="itemName">パテックフィリップ　コンプリケーテッド ウォッチ 5085/1A-001</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：1,400,000円<br>
                                        <span class="blue">B社</span>：1,390,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">1,420,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>30,000円</p>
                                </div>
                            </li>

                            <!-- brand3 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/003.jpg" alt="">
                                    <p class="itemName">パテックフィリップ　コンプリケーション 5130G-001 WG</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：2,970,000円<br>
                                        <span class="blue">B社</span>：2,950,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">3,000,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>50,000円</p>
                                </div>
                            </li>

                            <!-- brand4 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/004.jpg" alt="">
                                    <p class="itemName">パテックフィリップ　ワールドタイム 5130R-001</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：3,000,000円<br>
                                        <span class="blue">B社</span>：2,980,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">3,050,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>70,000円</p>
                                </div>
                            </li>

                            <!-- brand5 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/005.jpg" alt="">
                                    <p class="itemName">シャネル　ラムスキン　マトラッセ　二つ折り長財布</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：69,000円<br>
                                        <span class="blue">B社</span>：68,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">70,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>2,000円</p>
                                </div>
                            </li>

                            <!-- brand6 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/006.jpg" alt="">
                                    <p class="itemName">エルメス　ベアンスフレ　ブラック</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：200,000円<br>
                                        <span class="blue">B社</span>：196,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">211,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>15,000円</p>
                                </div>
                            </li>

                            <!-- brand7 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/007.jpg" alt="">
                                    <p class="itemName">エルメス　バーキン30　トリヨンクレマンス　マラカイト　SV金具</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：1,220,000円<br>
                                        <span class="blue">B社</span>：1,200,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">1,240,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>40,000円</p>
                                </div>
                            </li>

                            <!-- brand8 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/008.jpg" alt="">
                                    <p class="itemName">セリーヌ　ラゲージマイクロショッパー</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：150,000円<br>
                                        <span class="blue">B社</span>：147,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">155,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>8,000円</p>
                                </div>
                            </li>

                            <!--brand9 -->

                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/009.jpg" alt="">
                                    <p class="itemName">ルイヴィトン　裏地ダミエ柄マッキントッシュジャケット</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：31,000円<br>
                                        <span class="blue">B社</span>：30,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">32,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>2,000円</p>
                                </div>
                            </li>

                            <!-- brand10 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/010.jpg" alt="">
                                    <p class="itemName">シャネル　ココマーク　ラパンファーマフラー</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：68,000円<br>
                                        <span class="blue">B社</span>：65,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">71,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>6,000円</p>
                                </div>
                            </li>

                            <!-- brand11 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/011.jpg" alt="">
                                    <p class="itemName">ルイヴィトン　ダミエ柄サイドジップレザーブーツ</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：49,000円<br>
                                        <span class="blue">B社</span>：48,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">51,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>3,000円</p>
                                </div>
                            </li>

                            <!-- brand12 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/012.jpg" alt="">
                                    <p class="itemName">サルバトーレフェラガモ　ヴェラリボンパンプス</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：9,000円<br>
                                        <span class="blue">B社</span>：8,500円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">10,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>1,500円</p>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <!-- 金買取 -->
                    <div class="menu"><span class="glyphicon glyphicon-bookmark"></span> 金買取</div>
                    <div class="content">
                        <!-- box -->
                        <ul id="box-jisseki" class="list-unstyled clearfix">
                            <!-- 1 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/001.jpg" alt="">
                                    <p class="itemName">K18　ダイヤ0.11ctリング</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：23,700円<br>
                                        <span class="blue">B社</span>：23,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">
                                        <!-- span class="small" >120g</span -->25,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>2,000円</p>
                                </div>
                            </li>
                            <!-- 2 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/002.jpg" alt="">
                                    <p class="itemName">K18　メレダイヤリング</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：38,000円<br>
                                        <span class="blue">B社</span>：37,500円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">
                                        <!-- span class="small" >120g</span -->39,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>1,500円</p>
                                </div>
                            </li>
                            <!-- 3 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/003.jpg" alt="">
                                    <p class="itemName">K18/Pt900　メレダイアリング</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：14,200円<br>
                                        <span class="blue">B社</span>：14,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">16,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>2,000円</p>
                                </div>
                            </li>
                            <!-- 4 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/004.jpg" alt="">
                                    <p class="itemName">Pt900　メレダイヤリング</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：22,600円<br>
                                        <span class="blue">B社</span>：21,200円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">23,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>1,800円</p>
                                </div>
                            </li>
                            <!-- 5 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/005.jpg" alt="">
                                    <p class="itemName">K18WG　テニスブレスレット</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：24,600円<br>
                                        <span class="blue">B社</span>：23,800円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">26,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>2,200円</p>
                                </div>
                            </li>
                            <!-- 6 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/006.jpg" alt="">
                                    <p class="itemName">K18/K18WG　メレダイヤリング</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：22,400円<br>
                                        <span class="blue">B社</span>：22,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">23,600<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>1,600円</p>
                                </div>
                            </li>
                            <!-- 7 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/007.jpg" alt="">
                                    <p class="itemName">K18ブレスレット</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：18,100円<br>
                                        <span class="blue">B社</span>：17,960円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">20,100<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>2,140円</p>
                                </div>
                            </li>
                            <!-- 8 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/008.jpg" alt="">
                                    <p class="itemName">K14WGブレスレット</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：23,600円<br>
                                        <span class="blue">B社</span>：22,800円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">24,300<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>1,500円</p>
                                </div>
                            </li>
                            <!-- 9 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/009.jpg" alt="">
                                    <p class="itemName">Pt850ブレスレット</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：23,600円<br>
                                        <span class="blue">B社</span>：23,200円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">24,700<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>1,500円</p>
                                </div>
                            </li>
                            <!-- 10 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/010.jpg" alt="">
                                    <p class="itemName">Pt900/Pt850メレダイヤネックレス</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：29,600円<br>
                                        <span class="blue">B社</span>：28,400円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">30,500<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>2,100円</p>
                                </div>
                            </li>
                            <!-- 11 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/011.jpg" alt="">
                                    <p class="itemName">K18/Pt900/K24ネックレストップ</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：43,600円<br>
                                        <span class="blue">B社</span>：43,100円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">45,900<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>2,800円</p>
                                </div>
                            </li>
                            <!-- 12 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/012.jpg" alt="">
                                    <p class="itemName">K24インゴットメレダイヤネックレストップ</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：53,700円<br>
                                        <span class="blue">B社</span>：52,900円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">56,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>3,100円</p>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <!-- 宝石買取 -->
                    <div class="menu"><span class="glyphicon glyphicon-magnet"></span> 宝石買取</div>
                    <div class="content">
                        <!-- box -->
                        <ul id="box-jisseki" class="list-unstyled clearfix">
                            <!--<li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem001.jpg" alt="">
            <p class="itemName">エメラルド</p>
            <hr>
            <p> <span class="red">A社</span>：190,000円<br>
              <span class="blue">B社</span>：194,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>200,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>7,500円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem002.jpg" alt="">
            <p class="itemName">サファイア</p>
            <hr>
            <p> <span class="red">A社</span>：145,500円<br>
              <span class="blue">B社</span>：142,500円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>150,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>10,000円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem003.jpg" alt="">
            <p class="itemName">ルビー</p>
            <hr>
            <p> <span class="red">A社</span>：97,000円<br>
              <span class="blue">B社</span>：95,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>100,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>5,000円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem004.jpg" alt="">
            <p class="itemName">アレキサンドライト</p>
            <hr>
            <p> <span class="red">A社</span>：67,900円<br>
              <span class="blue">B社</span>：66,500円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>70,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>4,500円</p>
          </div>
        </li>-->

                            <!-- 1 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/001.jpg" alt="">
                                    <p class="itemName">ダイヤルース</p>
                                    <p class="itemdetail">カラット：1.003ct<br> カラー：G
                                        <br> クラリティ：VS-2
                                        <br> カット：Good
                                        <br> 蛍光性：FAINT
                                        <br> 形状：ラウンドブリリアント
                                    </p>
                                    <hr>
                                    <p> <span class="red">A社</span>：215,000円<br>
                                        <span class="blue">B社</span>：210,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">
                                        <!-- span class="small">地金＋</span -->221,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>11,000円</p>
                                </div>
                            </li>

                            <!-- 2 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/002.jpg" alt="">
                                    <p class="itemName">Pt900 DR0.417ctリング</p>
                                    <p class="itemdetail"> カラー：F<br> クラリティ：SI-1
                                        <br> カット：VERY GOOD <br> 蛍光性：FAINT
                                        <br> 形状：ラウンドブリリアント
                                    </p>
                                    <hr>
                                    <p> <span class="red">A社</span>：42,000円<br>
                                        <span class="blue">B社</span>：41,300円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">44,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>2,700円</p>
                                </div>
                            </li>

                            <!-- 3 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/003.jpg" alt="">
                                    <p class="itemName">Pt900　DR0.25ctリング</p>
                                    <p class="itemdetail"> カラー：H<br> クラリティ:VS-1
                                        <br> カット：Good
                                        </br>
                                        蛍光性：MB<br> 形状：ラウンドブリリアント
                                        <br>

                                        <hr>
                                        <p> <span class="red">A社</span>：67,400円<br>
                                            <span class="blue">B社</span>：67,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">68,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>1,000円</p>
                                </div>
                            </li>

                            <!-- 4 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/004.jpg" alt="">
                                    <p class="itemName">K18　DR0.43ct　MD0.4ctネックレストップ</p>
                                    <p class="itemdetail"> カラー：I<br> クラリティ：VS-2
                                        <br> カット：Good
                                        <br> 蛍光性：WB
                                        <br> 形状：ラウンドブリリアント
                                    </p>
                                    <hr>
                                    <p> <span class="red">A社</span>：50,000円<br>
                                        <span class="blue">B社</span>：49,500円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">51,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>1,500円</p>
                                </div>
                            </li>

                            <!-- 5 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/005.jpg" alt="">
                                    <p class="itemName">ダイヤルース</p>
                                    <p class="itemdetail">カラット：0.787ct<br> カラー：E
                                        <br> クラアリティ：VVS-2
                                        <br> カット：Good
                                        <br> 蛍光性：FAINT
                                        <br> 形状：ラウンドブリリアント
                                    </p>
                                    <hr>
                                    <p> <span class="red">A社</span>：250,000円<br>
                                        <span class="blue">B社</span>：248,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">257,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>9,000円</p>
                                </div>
                            </li>

                            <!-- 6 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/006.jpg" alt="">
                                    <p class="itemName">Pt950　MD0.326ct　0.203ct　0.150ctネックレス</p>
                                    <p class="itemdetail"> カラー：F<br> クラリティ：SI-2
                                        <br> カット：Good
                                        <br> 蛍光性：FAINT
                                        <br> 形状：ラウンドブリリアント
                                    </p>
                                    <hr>
                                    <p> <span class="red">A社</span>：55,000円<br>
                                        <span class="blue">B社</span>：54,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">57,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>3,000円</p>
                                </div>
                            </li>

                            <!-- 7 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/007.jpg" alt="">
                                    <p class="itemName">ダイヤルース</p>
                                    <p class="itemdetail">カラット：1.199ct<br> カラー：K
                                        <br> クラリティ：SI-2
                                        <br> カット：評価無
                                        <br> 蛍光性：NONE
                                        <br> 形状：パビリオン
                                        <br>
                                    </p>
                                    <hr>
                                    <p> <span class="red">A社</span>：58,000円<br>
                                        <span class="blue">B社</span>：56,800円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">60,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>3,200円</p>
                                </div>
                            </li>

                            <!-- 8 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/008.jpg" alt="">
                                    <p class="itemName">ダイヤルース</p>
                                    <p class="itemdetail">カラット：8.1957ct<br> カラー：LIGTH YELLOW<br> クラリティ：VS-1
                                        <br> カット：VERY GOOD<br> 蛍光性：FAINT
                                        <br> 形状：ラウンドブリリアント
                                    </p>
                                    <hr>
                                    <p> <span class="red">A社</span>：6,540,000円<br>
                                        <span class="blue">B社</span>：6,500,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">6,680,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>180,000円</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="menu"><span class="glyphicon glyphicon-time"></span> 時計買取</div>
                    <div class="content">
                        <ul id="box-jisseki" class="list-unstyled clearfix">
                            <?php
                    foreach($resultLists as $list):
                    // :: で分割
                    $listItem = explode('::', $list);
                  
                  ?>
                                <li class="box-4">
                                    <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/<?php echo $listItem[0]; ?>" alt="">
                                        <p class="itemName">
                                            <?php echo $listItem[1]; ?><br>
                                            <?php echo $listItem[2]; ?>
                                        </p>
                                        <hr>
                                        <p> <span class="red">A社</span>：
                                            <?php echo $listItem[3]; ?>円<br>
                                            <span class="blue">B社</span>：
                                            <?php echo $listItem[4]; ?>円 </p>
                                    </div>
                                    <div class="box-jisseki-cat">
                                        <h3>買取価格例</h3>
                                        <p class="price">
                                            <?php echo $listItem[5]; ?><span class="small">円</span></p>
                                    </div>
                                    <div class="sagaku">
                                        <p><span class="small">買取差額“最大”</span>
                                            <?php echo $listItem[6]; ?>円</p>
                                    </div>
                                </li>
                                <?php endforeach; ?>
                        </ul>
                    </div>

                    <!-- バッグ買取 -->
                    <div class="menu"><span class="glyphicon glyphicon-briefcase"></span> バッグ買取</div>
                    <div class="content">
                        <ul id="box-jisseki" class="list-unstyled clearfix">
                            <!-- 1 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/001.jpg" alt="">
                                    <p class="itemName">エルメス　エブリンⅢ　トリヨンクレマンス</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：176,000円<br>
                                        <span class="blue">B社</span>：172,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">180,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>8,000円</p>
                                </div>
                            </li>
                            <!-- 2 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/002.jpg" alt="">
                                    <p class="itemName">プラダ　シティトート2WAYバッグ</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：128,000円<br>
                                        <span class="blue">B社</span>：125,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">132,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>7,000円</p>
                                </div>
                            </li>
                            <!-- 3 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/003.jpg" alt="">
                                    <p class="itemName">バンブーデイリー2WAYバッグ</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：83,000円<br>
                                        <span class="blue">B社</span>：82,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">85,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>3,000円</p>
                                </div>
                            </li>
                            <!-- 4 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/004.jpg" alt="">
                                    <p class="itemName">ルイヴィトン　モノグラムモンスリGM</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：64,000円<br>
                                        <span class="blue">B社</span>：63,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">66,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>3,000円</p>
                                </div>
                            </li>
                            <!-- 5 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/005.jpg" alt="">
                                    <p class="itemName">エルメス　バーキン30</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：1,150,000円<br>
                                        <span class="blue">B社</span>：1,130,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">1,200,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>70,000円</p>
                                </div>
                            </li>
                            <!-- 6 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/006.jpg" alt="">
                                    <p class="itemName">シャネル　マトラッセダブルフラップダブルチェーンショルダーバッグ</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：252,000円<br>
                                        <span class="blue">B社</span>：248,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">260,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>12,000円</p>
                                </div>
                            </li>
                            <!-- 7 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/007.jpg" alt="">
                                    <p class="itemName">ルイヴィトン　ダミエネヴァーフルMM</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：96,000円<br>
                                        <span class="blue">B社</span>：94,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">98,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>4,000円</p>
                                </div>
                            </li>
                            <!-- 8 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/008.jpg" alt="">
                                    <p class="itemName">セリーヌ　ラゲージマイクロショッパー</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：150,000円<br>
                                        <span class="blue">B社</span>：148,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">155,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>7,000円</p>
                                </div>
                            </li>
                            <!-- 9 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/009.jpg" alt="">
                                    <p class="itemName">ロエベ　アマソナ23</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：86,000円<br>
                                        <span class="blue">B社</span>：82,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">90,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>8,000円</p>
                                </div>
                            </li>
                            <!-- 10 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/010.jpg" alt="">
                                    <p class="itemName">グッチ　グッチシマ　ビジネスバッグ</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：50,000円<br>
                                        <span class="blue">B社</span>：48,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">53,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>5,000円</p>
                                </div>
                            </li>
                            <!-- 11 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/011.jpg" alt="">
                                    <p class="itemName">プラダ　サフィアーノ2WAYショルダーバッグ</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：115,000円<br>
                                        <span class="blue">B社</span>：110,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">125,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>15,000円</p>
                                </div>
                            </li>
                            <!-- 12 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/012.jpg" alt="">
                                    <p class="itemName">ボッテガヴェネタ　カバMM　ポーチ付き</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：150,000円<br>
                                        <span class="blue">B社</span>：146,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">155,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>9,000円</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
            <!-- <section class="kaitori_voice clearfix shop_page mb50">
                <h3 class="mb30">心斎橋店のお客様の声</h3>
                <ul>
                    <li>
                        <p class="kaitori_tab tentou_tab">店頭買取</p>
                        <h4>パネライ　時計　</h4>
                        <p class="voice_txt">新しい時計が気になり、今まで愛用していたパネライの腕時計を買取してらもらうことにしました。<br /> 手元に保管しておくという手もあったのですが、やはり使わずに時計を手元に置いておくよりは、買取資金の足しにした方がいいのかなと思ったので・・・。
                            <br /> 通勤するときに、ブランドリバリューの近くを通るので、通勤ついでにブランドリバリューで試しで査定をしてもらうことにしたのですが、本当にこちらを利用してよかったです。
                            <br /> 私もブランドに詳しい自信がありましたが、査定士さんはそれ以上の知識があり、きちんとブランドの価値をわかってくださいました。本当に買取額に対して満足することができただけではなく、安心して買取手続きをすることができました。
                            <br /> やはり買取査定はブランドリバリューのように、きちんとブランド知識があるところでお願いしないと満足できないですよね。
                        </p>
                    </li>
                    <li>
                        <p class="kaitori_tab tentou_tab">店頭買取</p>
                        <h4>シャネル　バッグ　</h4>
                        <p class="voice_txt">シャネルのバッグの査定をしてもらいました。<br /> 店頭買取をしてもらったのですが、実はブランドリバリューに出向く前に他社比較もしようと思い、他の買取店でも査定をしてもらっていたんです。 ですが、他社比較の結果、断然！ブランドリバリューは高値の査定を出してくれました！
                            <br /> ブランドリバリューは、きちんと店頭買取をすることができる、いわば実際の店舗が存在するブランド買取店でもあるので、この安心さも兼ねそろえているのが個人的に安心でした。
                            <br /> また、他にもブランドバッグで買取をしてほしいものが出てきたら、絶対にブランドリバリューを利用するつもりです。 もう他社比較はしません！笑
                        </p>
                    </li>
                    <li>
                        <p class="kaitori_tab tentou_tab">店頭買取</p>
                        <h4>ボッテガヴェネタ　財布</h4>
                        <p class="voice_txt">好みのブランドから、新作のお財布が販売されたので、今使っているボッテガヴェネタの財布を買取してもらおうと思い、ブランドリバリューを利用しました。<br /> 店舗の位置をチェックしてみると、銀座駅からもめっちゃ近かったので、店頭買取を利用しての買取です。
                            <br /> 今回は、ボッテガヴェネタの財布１つだけの買取だったので、このお財布１つだけで、買取査定を依頼するのは何だか申し訳ないなと思ったりもしたのですが、担当の方もとても丁寧で気持ちのいい対応をとってくださったので、本当によかったです。
                            <br /> 査定額も満足することができたので、機会があればまた利用したいと思っています。
                        </p>
                    </li>
                </ul>
            </section>-->
            <section class="clearfix" style="margin-top:20px;">
            
                <h3 class="mb30">心斎橋　立地へのこだわり</h3>
                <p>私たちは2016年に銀座の激戦区でブランドリバリュー銀座店1号店を出店させていただきました。そこでは他のブランド品買取店に負けない「最高水準の高額買取」、「誠実で懇切丁寧な接客」を強みとして多くのお客様にご利用いただいております。2017年2号店の渋谷も渋谷のシンボルの一つであるタワーレコード前に出店、2018年には新宿伊勢丹近くに3号店をオープンいたしました。お客様のニーズに応えるという一心でやってきた結果、店舗数の拡大に繋がっていると信じております。<br><br> この度、弊社の「高価買取」「誠実丁寧な接客」の役割を全国で担うべく西日本最大の繁華街である難波・心斎橋へオープンさせていただくこととなりました。
                    <br> 心斎橋駅上の利便性の高い場所で営業しておりますので、ちょっとした空き時間やお仕事帰り、ショッピングついでなどでも、ジュエリー・時計・バッグ等のブランド商品や宝石・宝飾品・貴金属品の査定・買取が可能ですので、どうぞお気軽にお立ち寄りください。
                    <br><br> また、売りたいと検討中の商品の現物をお持込みいただかずとも、お電話やメールやLINE、店頭にて商品名や状態を伺って仮査定をさせていただくことも可能でございます。
                    <br> 店頭買取だけではなく、予約制買取、出張買取、宅配買取、手ぶらで買取等お客様のニーズに応えるべく様々な買取サービスをご用意しております。 お住いから渋谷・銀座まで行くのに時間がかかってしまうというお客様は宅配買取・店頭買取を利用していらっしゃいますので併せてご検討下さい。 <br><br> 当店では長年、リユース業界でジュエリー・時計・ バッグ等の買取に携わってきたプロフェッショナルであるバイヤーが、よりお客様にご満足いただけるよう、お客様の気持ちに親身になり他店を上回る高額査定にてご要望にお答え申し上げます。<br> 心斎橋店スタッフ一同、皆様のご来店心よりお待ち申し上げております。
                    <br><br> 大阪・心斎橋でジュエリー・時計・バッグの買取店をお探しの皆様、ブランド買取なら高額査定のBRANDREVALUE(ブランドリバリュー)へ。
                </p>

            </section>
<div class="takuhai_cvbox tentou">
                <p class="cv_tl">ブランドリバリュー心斎橋店で店頭買取予約をする</p>
                <div class="custom_tel takuhai_tel">
                    <a href="tel:0120-970-060">
                        <div class="tel_wrap">
                            <div class="telbox01"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/customer/telico.png" alt="お申し込みはこちら">
                                <p>お電話からでもお申込み可能！<br>ご不明な点は、お気軽にお問合せ下さい。 </p>
                            </div>
                            <div class="telbox02"> <span class="small_tx">【受付時間】11:00 ~ 21:00</span><span class="ted_tx"> 年中無休</span>
                                <p>0120-970-060</p>
                            </div>
                        </div>
                    </a>
                </div>
                <p><a href="<?php echo home_url('purchase/visit-form'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/tentou_cv01.png" alt="店頭買取お申し込み"></a></p>
            </div>

            <?php
        // アクションポイント
        get_template_part('_action');
        
        // 買取方法
        get_template_part('_purchase');
      ?>
        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->

    <?php
get_sidebar();
get_footer();
