<?php
    /**
     * The template for displaying all pages.
     *
     * This is the template that displays all pages by default.
     * Please note that this is the WordPress construct of pages
     * and that other 'pages' on your WordPress site may use a
     * different template.
     *
     * @link https://codex.wordpress.org/Template_Hierarchy
     *
     * @package BRING
     */
?>

<!doctype html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <title></title>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/provis/reset.css" type="text/css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/provis/style.css" type="text/css">
        <link rel="shortcut icon" href="favicon.ico">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

        <style>

            .rec-lp {
                padding-top: 100px;
                width: 100%;
                text-align: center;
            }

            .rec-lp img {
                width: 100%;
                height: auto;
                display: block;
                max-width: 1800px;
                margin: 0 auto;
            }



        </style>
        <?php get_template_part('template-parts/gtm', 'head') ?>
    </head>

    <body>
        <?php get_template_part('template-parts/gtm', 'body') ?>
        <header id="top-head">
            <div class="inner">
                <div id="mobile-head">
                    <h1 class="logo raleway">STAY GOLD</h1>
                    <!--<div id="nav-toggle">
                         <div> <span></span> <span></span> <span></span> </div>
                         </div>-->
                </div>
            </div>
        </header>

        <section class="rec-lp">
            <img src="<?= get_s3_template_directory_uri(); ?>/images/recruit_lp/01.jpg" alt="">
            <img src="<?= get_s3_template_directory_uri(); ?>/images/recruit_lp/02.jpg" alt="">
            <img src="<?= get_s3_template_directory_uri(); ?>/images/recruit_lp/03.jpg" alt="">
            <img src="<?= get_s3_template_directory_uri(); ?>/images/recruit_lp/04.jpg" alt="">
            <img src="<?= get_s3_template_directory_uri(); ?>/images/recruit_lp/05.jpg" alt="">
            <img src="<?= get_s3_template_directory_uri(); ?>/images/recruit_lp/06.jpg" alt="">
        </section>
        <section class="rec_cont04">
            <div class="cont_inner">
                <h2>募集要項</h2>
                <table>
                    <tbody>
                        <tr>
                            <th>雇用形態</th>
                            <td>正社員</td>
                        </tr>
                        <tr>
                            <th>募集職種</th>
                            <td>営業事務職</td>
                        </tr>
                        <tr>
                            <th>応募条件</th>
                            <td>・未経験大歓迎！<br>
                                ・第二新卒も大歓迎！<br>
                                ・学歴不問<br>
                                ・35歳までの方<br><br>
                                ～あったら嬉しい経験～<br>
                                ・事務経験<br>
                                ・バックオフィス経験<br>
                                ・ブランド品、貴金属の販売経験
                            </td>
                        </tr>
                        <tr>
                            <th>勤務地</th>
                            <td>■BRANDREVALUE 銀座店<br>
                                東京都中央銀座5-8-3 四谷学院ビル5階<br>
                                各線「銀座駅」A5出口より徒歩10秒！<br>
                                <br>
                                ■BRANDREVALUE 渋谷店<br>
                                東京都渋谷区神南1-12-16 和光ビル4F<br>
                                各線「渋谷駅」より徒歩3分<br>
                                <br>
                                【銀座・渋谷・新宿・原宿の姉妹店でも働けるかも】</td>
                        </tr>
                        <tr>
                            <th>給与</th>
                            <td>
                                【営業事務職】<br>
                                月給20万円以上＋賞与年2回<br>
                                ※スキル・経験等を考慮の上、最大限に優遇いたします<br>
                                ※試用期間3ヵ月あり(給与・待遇等は同じです)<br>
                                ・[モデル年収]1年目：月収24万円(事務職経験者)<br><br>
                                ※経験や能力に応じてこれ以上の待遇も相談してください。
                            </td>
                        </tr>
                        <tr>
                            <th>諸手当</th>
                            <td>・通勤交通費支給<br>
                                ・役職手当 </td>
                        </tr>
                        <tr>
                            <th>休日休暇</th>
                            <td>・完全週休2日制(シフト制) <br>
                                ・祝日<br>
                                ・夏季休暇<br>
                                ・年末年始休暇<br>
                                ・慶弔休暇 <br>
                                ・有給休暇 <br>
                                ・育児、介護休業</td>
                        </tr>
                        <tr>
                            <th>勤務時間</th>
                            <td>・9：00～20：00のうち実働8時間あなたの希望する時間に働いてOK。<br>
                                9:00～18:00まで、10:00～19:00などぜひ希望をお聞かせください♪<br>
                                ・残業なし！
                            </td>
                        </tr>
                        <tr>
                            <th>昇給</th>
                            <td>・昇給随時</td>
                        </tr>
                        <tr>
                            <th>福利厚生</th>
                            <td>・社会保険完備<br>
                                ・社内勉強会・外部研修参加無料 <br>
                                ・資格取得支援あり <br>
                                ・社員割引<br>
                                ・社員交流費あり<br>
                                ・社員紹介制度(最大5万円支給) <br>
                                ・独立支援制度あり <br>
                                ・毎月店舗達成ボーナス支給</td>
                        </tr>
                        <tr>
                            <th>研修制度</th>
                            <td>ビジネスマナーから接客・営業方法、ブランド・商品知識など学べる様々な研修を実施！</td>
                        </tr>
                        <tr>
                            <th>選考フロー</th>
                            <td>・条件に合った人であれば、面談します！！<br>
                                　　↓<br>
                                ・1次面談<br>
                                　　↓<br>
                                ・最終面談<br>
                                　　↓<br>
                                ・採用</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="btn_wrap">
                <div class="link_btn"><a href="<?php home_url('/') ?>">TOPへ戻る</a></div>
                <div class="link_btn02"><a href="https://woman.type.jp/job-offer/367633/?routeway=80" target="_blank">女の転職求人情報</a></div>
            </div>
        </section>
        <footer>
            <p ><small class="raleway">&copy;<?php echo date('Y') ?> STAY GOLD Inc.</small> </p>
            </div>
        </footer>
    </body>
</html>
