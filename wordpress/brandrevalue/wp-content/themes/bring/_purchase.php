<section id="new_purchase" class="new_purchase">
  <h2><img src="<?php echo get_s3_template_directory_uri() ?>/img/title-selectpurchase.png" alt="ブランドリバリュー4つの買取方法"></h2>
  <ul class="list-unstyled clearfix">
    <li class="new_push01"><a href="<?php echo home_url('about-purchase/takuhai') ?>"><p class="pur_tx">1.宅配買取<span>送って<br>連絡を待つだけ</span></p></a></li>
    <li class="new_push02"><a href="<?php echo home_url('about-purchase/syutchou') ?>"><p class="pur_tx">2.出張買取<span>連絡して<br>自宅で待つだけ</span></p></a></li>
    <li class="new_push03"><a href="<?php echo home_url('about-purchase/tentou') ?>"><p class="pur_tx">3.店頭買取<span>連絡して<br>自宅で待つだけ</span></p></a></li>
    <li class="new_push04"><a href="<?php echo home_url('customer') ?>"><p class="pur_tx">4.スピード査定<span>無料ですぐに<br>査定額がわかる</span></p></a></li>
  </ul>
</section>
