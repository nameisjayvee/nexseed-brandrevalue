<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>



	<div id="primary" class="cat-page content-area">
		<main id="main" class="site-main" role="main">
        <!--<section id="mainVisual" style="background:url(<?php echo get_s3_template_directory_uri() ?>/img/mv/purchase_mv.png)">
          <h2 class="text-hide">買取方法</h2>
        </section>-->
        
      <section>
      <div id="blog-single">
      
        <?php
          //-------------------------------------
          if(have_posts()): while(have_posts()): the_post();
            // タクソノミ
            $cat = get_the_terms(get_the_ID(), 'blog-cat');
            $cat = $cat[0];
            $catName = $cat->name;
            if(!empty($catName)) {
              $catName = '【'.$catName.'】';
            }
          
            // アイキャッチ
            $thumbnail_id = get_post_thumbnail_id();
            $image = wp_get_attachment_image_src($thumbnail_id, 'full');
        ?>
        
        <div id="blogArticle" class="postlist">
          <p class="date"><?php the_time('Y年m月d日（D）'); ?></p>
          <h1 class="title"><?php echo $catName; ?><?php the_title(); ?></h1>
          <img src="<?php echo $image[0]; ?>">
          <article>
            <?php the_content(); ?>
          </article>
        </div>
        
        <?php
          endwhile; endif;
          //-------------------------------------
        ?>
        
        <!----- 関連記事 ----->
        <?php
          // ページのカテゴリ情報取得
          $id = $post->ID;
          $rCat = get_the_terms($id, 'blog-cat');
          $title = $rCat[0]->name;
          
          if(!empty($title)) {
            $title = $rCat[0]->name.'の関連記事';
          } else {
            $title = '関連記事';
          }
        ?>
        
        <?php //if(current_user_can('read_private_pages')) : ?>
        <hr>
        <div id="single-related">
          <h3><?php echo $title; ?></h3>
          <ul>
          <?php
            // カテゴリ記事5件表示
            $query = array(
              'post_per_page' => 5,
              'post_type'     => 'blog',
              'post__not_in'  => array($id),
              'taxonomy'      => 'blog-cat',
              'term'          => $rCat[0]->slug,
            );
            
            $rResultsArray = new WP_Query($query);
            $rResults = $rResultsArray->posts;
            
            foreach($rResults as $item) {
              $id = $item->ID;
              echo '<li><a href="'.get_the_permalink($id).'">'.$item->post_title.'</a></li>';
            }
          ?>
          </ul>
        </div>
        <hr>
        <?php //endif; ?>
        
      </div>
      </section>
      
      
      
     <section id="new_purchase" class="new_purchase">
  <h2><img src="<?php echo get_s3_template_directory_uri() ?>/img/title-selectpurchase.png" alt="都合に合わせて自由にチョイス！選べる3つの買取方法"></h2>
  <ul class="list-unstyled clearfix">
    <li class="new_push01"><a href="http://kaitorisatei.info/brandrevalue/about-purchase/takuhai" ><p class="pur_tx">1.宅配買取<span>送って<br>連絡を待つだけ</span></p></a></li>
    <li class="new_push02"><a href="http://kaitorisatei.info/brandrevalue/about-purchase/syutchou"><p class="pur_tx">2.出張買取<span>連絡して<br>自宅で待つだけ</span></p></a></li>
    <li class="new_push03"><a href="http://kaitorisatei.info/brandrevalue/about-purchase/tentou"><p class="pur_tx">3.店頭買取<span>その場で<br>即お支払い</span></p></a></li>
    <li class="new_push04"><a href="http://kaitorisatei.info/brandrevalue/about-purchase/tebura"><p class="pur_tx">4.店頭郵送買取<span>先に郵送<br>来店頂きお支払い</span></p></a></li>
  </ul>
</section>
        
        <?php
        
        // アクションポイント
        get_template_part('_action');
		
		
        
        // 店舗案内
        get_template_part('_shopinfo');
      ?>
      
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
