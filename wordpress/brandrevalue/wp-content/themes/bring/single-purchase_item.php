<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


get_header(); ?>
    <div id="primary" class="cat-page content-area">
        <main id="main" class="site-main" role="main">
            <h2 class="ttl_blue">ブランドリバリュー ブランド買取実績</h2>
            <ul class="archive_purchase_list">
                <li><a href="<?php echo home_url('purchase_items/purchase_watch'); ?>">時計</a></li>
                <li><a href="<?php echo home_url('purchase_items/purchase_antique'); ?>">アンティーク時計</a></li>
                <li><a href="<?php echo home_url('purchase_items/purchase_bag'); ?>">バッグ</a></li>
                <li><a href="<?php echo home_url('purchase_items/purchase_wallet'); ?>">財布</a></li>
                <li><a href="<?php echo home_url('purchase_items/purchase_outfit'); ?>">洋服毛皮</a></li>
                <li><a href="<?php echo home_url('purchase_items/purchase_shoes'); ?>">靴</a></li>
                <li><a href="<?php echo home_url('purchase_items/purchase_jewelry'); ?>">ブランドジュエリー</a></li>
                <li><a href="<?php echo home_url('purchase_items/purchase_gold'); ?>">金・プラチナ</a></li>
                <li><a href="<?php echo home_url('purchase_items/purchase_dia'); ?>">ダイヤ・宝石</a></li>
                <li><a href="<?php echo home_url('purchase_items/purchase_mement'); ?>">骨董品</a></li>
            </ul>
            <div class="page_kaitoriitem">
                <?php if (have_posts()) : while (have_posts()) : the_post();?>
                <p class="red_tab">最新買取情報</p>
                <h2 class="mincho">
                    <?php the_title(); ?>
                </h2>
                <div class="kaitoriitem_wrap">
                    <p class="item_img"><img src="<?php the_field('kaitori-img'); ?>" alt="<?php the_title(); ?>"></p>
                    <div class="kaitoriitem_info">
                        <p>商品情報</p>
                        <table>
                            <tbody>
                                <tr>
                                    <th>買取アイテム</th>
                                    <td>
                                        <?php the_field('kaitori-item'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>ブランド名</th>
                                    <td>
                                        <?php the_field('brand-name'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>アイテム名</th>
                                    <td>
                                        <?php the_field('item-name'); ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p>買取情報</p>
                        <table>
                            <tbody>
                                <tr>
                                    <th>お買取日</th>
                                    <td>
                                        <?php the_field('kaitori-date'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>買取店舗</th>
                                    <td>
                                        <?php the_field('kaitori-shop'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>買取方法</th>
                                    <td>
                                        <?php the_field('kaitori-method'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>買取価格</th>
                                    <td>
                                        ¥<?php the_field('kaitori-price'); ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </div>
                    <div class="kaitori-coment">
                        <p><?php the_field('kaitori-coment'); ?></p>
                    </div>
                
                <?php endwhile; endif;?>
            </div>
            <?php
        // アクションポイント
        get_template_part('_action');
        // 店舗案内
        get_template_part('_shopinfo');
      ?>
        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->

    <?php
get_sidebar();
get_footer();
