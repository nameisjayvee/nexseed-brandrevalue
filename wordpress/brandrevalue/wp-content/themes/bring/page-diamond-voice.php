<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>
<script>
$(function(){
  $('a[href^=#]').click(function(){
    var speed = 500;
    var href= $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top;
    $("html, body").animate({scrollTop:position}, speed, "swing");
    return false;
  });
});
</script>

<p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
<p class="main_bottom">ダイヤモンド買取！！業界最高峰の買取価格をお約束いたします。</p>
<div id="primary" class="cat-page content-area">
	<main id="main" class="site-main" role="main">
		<section id="user_voice">
			<h3>ご利用いただいたお客様の声 ①</h3>
			<p class="user_voice_text1">"以前プレゼントされたダイヤモンドの指輪があったのですが、身に着ける機会もなくなりました。
				そこで思い切って、査定をしてもらおうと思い買取に出すことにしたんです。<br />
				正直言って、そこまで大きいダイヤモンドではなかったので、あまり査定額には期待をしていなかったんです。
				ですが実際に査定が終わり、査定額を提示されたときは驚きました。<br />
				<br />
				自分が予想していた査定額よりも、かなり良い査定額が提示されたので・・・。
				これならもっと早く、身に着けていなかったのですから、このダイヤモンドの指輪を買取に出しておけばよかったなと思いましたね。<br />
				また、何か買取に出すものが出てきたときは、査定をお願いしたいと思います。ありがとうございました。
				
				"</p>
			<h4>鑑定士からのコメント</h4>
			<div class="clearfix">
				<p class="user_voice_text2">"この度は、お客様が大切になさっていたダイヤモンドの指輪の買取をご依頼してくださり、誠にありがとうございました。<br />
					お客様のダイヤモンドの指輪は、とても状態が良いものであり、なおかつ鑑定書もきちんと用意されておりましたので、今回はこのような査定額を提示させていただきました。<br />
					また査定額に関しましても、お客様自身とてもご満足していただけたようで、嬉しく思います。
					今回のようなダイヤモンドの指輪のように、また何か買取に出すアイテムが出てきた際には、ぜひまた買取をお申込みくださいませ。
					お客様のダイヤモンドをしっかりと鑑定士が全力で、鑑定させていただきます。
					鑑定士一同、お客様の買取依頼を心よりお待ちしております。
					"</p>
				<p><img class="right pc-only" src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/lp/staff_img.png" alt="" />
			</div>
		</section>
		<section id="user_voice">
			<h3>ご利用いただいたお客様の声 ②</h3>
			<p class="user_voice_text1">"急にどうしてもお金が必要となり、ダイヤモンドのネックレスを買取してもらうことにしました。
				このような状況でしたから、できるだけ高額な査定額を出してもらえるところで、<br />
				なおかつ時間がなかったので、できるだけ迅速に対応してもらうことができるところで査定をしてもらいたい・・・。<br />
				そう思い買取先を探していた時に見つけたのが、ブランドリバリューさんだったんです。<br />
				口コミもよかったので、依頼をしてみましたが口コミ通り満足することができる査定額を出してもらえました！<br />
				また、それだけではなくとにかく迅速な対応をしてもらうことができ、すぐに現金で受け取ることができたので本当に助かりました。<br />
				本当に、今回はダイヤモンドのネックレスの買取をこちらに任せて大正解でした！
				"</p>
			<h4>鑑定士からのコメント</h4>
			<div class="clearfix">
				<p class="user_voice_text2">この度は、ダイヤモンドのネックレスの査定のご依頼、ありがとうございました。<br />
					お客様のご希望に添えることができ、鑑定士共々嬉しく思います。<br />
					お客様が今回、ご依頼してくださりましたダイヤモンドのネックレスは、ダイヤモンドのサイズがとても大きいだけではなく、アクセサリーそのもののブランドとしても大変人気のあるお品物であったことから、今回はこのような査定額を提示させていただきました。<br />
					また、迅速な対応とおっしゃっていただけ、大変うれしく思います。<br />
					当社におります鑑定士は、豊富な鑑定知識を持っているため、しっかりと迅速にお客様からご依頼をしていただけましたダイヤモンドの鑑定を行うことが可能です。<br />
					また、ぜひダイヤモンドの買取がございましたら、当社にお任せください。<br />
					この度は、ありがとうございました。</p>
				<p><img class="right pc-only" src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/cat/photo-user-voice.jpg" alt="" />
			</div>
		</section>
		<section id="user_voice">
			<h3>ご利用いただいたお客様の声 ③</h3>
			<p class="user_voice_text1">一時期、宝飾類の中でも特にダイヤを集中的に購入していた時期がありました。
				このとき、ネックレスや指輪、ブランド品のジュエリーに至るまで、ダイヤの宝飾類ばかりを集めておりましたが、今となっては、「あの時は何だったの？」というくらい、興味がなくなりましたので、査定を依頼をすることにいたしました。<br />
				ダイヤの装飾品を持ち歩くのも、物騒に感じ、出張買取でまとめて査定していただきましたが、ご担当の査定士さんには一つ一つ丁寧にじっくりと査定していただき、驚きました。<br />
				また、鑑定書を紛失してしまったものも中にはあったのですが、それでも納得の査定額で買取をしてもらえたので、本当に感謝しております。<br />
				すべて買取していただいたとは思いますが、また、ダイヤの宝飾類が見つけた際は、絶対ブランドリバリューさんに買取していただきます。</p>
			<h4>鑑定士からのコメント</h4>
			<div class="clearfix">
				<p class="user_voice_text2">今回は、ダイヤの宝飾類をいくつか査定にだしていただき、ありがとうございます。
					１つ１つ丁寧に査定をさせていただきましたが、どれもお客様が買取に出していただきましたダイヤの宝飾類は、確かにお客様が仰る通り、鑑定書がないものもございましたが、目立つ傷もなく状態もとても良いもので、なおかつ宝飾類自体の劣化も気にならなかったため、今回はこのような査定額を提示させていただきました。<br />
					この査定額に対し、お客様がとても満足していただけたようで嬉しく思います。<br />
					数多くあります買取店の中から、当社を選んでくださりありがとうございました。<br />
					また次回も、鑑定士共々心よりお客様のご依頼をお待ちしておりますので、よろしくお願いいたします。</p>
				<p><img class="right pc-only" src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/lp/staff_img.png" alt="" />
			</div>
		</section>
	</main>
	<ul class="dia_nav">
    <li><a href="<?php echo home_url('/cat/diamond/diamond-qa'); ?>">ダイヤモンド<br />買取Q&A</a></li>
    <li><a href="<?php echo home_url('cat/diamond/diamond-term'); ?>">ダイヤモンド<br />用語集</a></li>
    <li><a href="<?php echo home_url('/cat/diamond/diamond-app'); ?>">ダイヤモンド<br />鑑定書</a></li>
    <li><a href="<?php echo home_url('/cat/diamond/diamond-voice'); ?>">お客様の声</a></li>
    <li><a href="<?php echo home_url('/cat/diamond/diamond-staff'); ?>">査定士紹介</a></li>
	</ul>
	
	<!-- #main --> 
</div>
<!-- #primary --> 
<script src="//kaitorisatei.info/brandrevalue/wp-content/themes/bring/js/gaisan.js" type="text/javascript"></script>
<?php
get_sidebar();
get_footer();















