<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BRING
 */
?>

    <div id="secondary" class="widget-area" role="complementary">
        <section id="side-purchase">
            <ul class="list-unstyled">
                <li><a href="<?php echo home_url('about-purchase/takuhai'); ?>">
        <img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-kaitori-takuhai.png" alt="宅配買取"></a></li>
                <li><a href="<?php echo home_url('about-purchase/syutchou'); ?>">
        <img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-kaitori-syutchou.png" alt="出張買取"></a></li>
                <li><a href="<?php echo home_url('about-purchase/tentou'); ?>">
        <img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-kaitori-tentou.png" alt="店頭買取"></a></li>
                <li><a href="<?php echo home_url('about-purchase/tebura'); ?>">
        <img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-kaitori-tebura.png" alt="手ぶら買取"></a></li>
                <li><a href="<?php echo home_url('contact'); ?>">
        <img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-satei2.png" alt="今すぐ査定申込"></a></li>
            </ul>
        </section>

        <!--<section id="side-pickup">
    <h3><img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/side-title-pickup.png" alt="PICK UP BRAND"></h3>
    <ul class="list-unstyled">
      <li><a href="<?php echo home_url('brand'); ?>">
        <dl><dd><img src="http://placehold.jp/50x50.png" class="logo" alt=""></dd>
        <dt><h4>コムデギャルソン<span class="small">COMME des GARCONS</span></h4></dt></dl>
      </a></li>
      <li><a href="<?php echo home_url('brand'); ?>">
        <dl><dd><img src="http://placehold.jp/50x50.png" class="logo" alt=""></dd>
        <dt><h4>コムデギャルソン<span class="small">COMME des GARCONS</span></h4></dt></dl>
      </a></li>
      <li><a href="<?php echo home_url('brand'); ?>">
        <dl><dd><img src="http://placehold.jp/50x50.png" class="logo" alt=""></dd>
        <dt><h4>コムデギャルソン<span class="small">COMME des GARCONS</span></h4></dt></dl>
      </a></li>
    </ul>
  </section>-->

        <section id="side-categorylist">
            <h3><img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/side-title-category.png"></h3>
            <nav class="side_on" id="side_on">
                <ul class="list-unstyled">
                    <li><a href="<?php echo home_url('cat/gold'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-category-gold.png"　alt="金・プラチナ"></a>
                        <div>
                            <ul>
                                <li><a href="<?php echo home_url('/cat/gold'); ?>">・金貨</a></li>
                                <li><a href="<?php echo home_url('/cat/gold/14k'); ?>">・14金</a></li>
                                <li><a href="<?php echo home_url('/cat/gold/18k'); ?>">・18金</a></li>
                                <li><a href="<?php echo home_url('/cat/gold/22k'); ?>">・22金</a></li>
                                <li><a href="<?php echo home_url('/cat/gold/24k'); ?>">・24金</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/ingot'); ?>">・インゴット</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/white-gold'); ?>">・ホワイトゴールド</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/pink-gold'); ?>">・ピンクゴールド</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/yellow-gold'); ?>">・イエローゴールド</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/platinum'); ?>">・プラチナ</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/silver'); ?>">・シルバー</a></li>
                            </ul>
                        </div>


                    </li>
                    <li><a href="<?php echo home_url('cat/gem'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-category-gem.png"　alt="宝石"></a>
                        <div>
                            <ul>
                                <li><a href="<?php echo home_url('/cat/gem/emerald'); ?>">・エメラルド</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/opal'); ?>">・オパール</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/sapphire'); ?>">・サファイア</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/hisui'); ?>">・ヒスイ</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/cartier'); ?>">・カルティエ</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/tourmaline'); ?>">・トルマリン</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/paraibatourmaline'); ?>">・パライバトルマリン</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/piaget'); ?>">・ピアジェ</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/boucheron'); ?>">・ブシュロン</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/ruby'); ?>">・ルビー</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/vancleefarpels'); ?>">・ヴァンクリーフ&amp;アーペル</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/harrywinston'); ?>">・ハリーウィンストン</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/bulgari'); ?>">・ブルガリ</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/tiffany'); ?>">・ティファニー</a></li>
                            </ul>
                        </div>

                    </li>
                    <li><a href="<?php echo home_url('cat/watch'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-category-watch.png"　alt="金・時計"></a>
                        <div>
                            <ul>
                                <li><a href="<?php echo home_url('/cat/watch/rolex'); ?>">・ロレックス</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/iwc'); ?>">・IWC</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/hublot'); ?>">・HUBLOT</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/omega'); ?>">・オメガ</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/panerai'); ?>">・パネライ</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/breitling'); ?>">・ブライトリング</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/franckmuller'); ?>">・フランクミュラー</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/breguet'); ?>">・ブレゲ</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/audemarspiguet'); ?>">・オーデマピゲ</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/patek-philippe'); ?>">・パテックフィリップ</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/jaeger-lecoultre'); ?>">・ジャガールクルト</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/jacob'); ?>">・ジェイコブ</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/vacheron'); ?>">・ヴァシュロンコンスタンタン</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/seiko'); ?>">・セイコー</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/gagamilano'); ?>">・ガガミラノ</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/alange-soehne'); ?>">・ランゲ&amp;ゾーネ</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/richardmille'); ?>">・リシャール・ミル</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/rogerdubuis'); ?>">・ロジェ・デュブイ</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/tagheuer'); ?>">・タグホイヤー</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/hamilton'); ?>">・ハルミトン</a></li>
                                <li><a href="<?php echo home_url('/cat/watch/blancpain'); ?>">・ブランパン</a></li>




                            </ul>
                        </div>

                    </li>
                    <li>
                        <a href="<?php echo home_url('cat/bag'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-category-bag.png"　alt="バッグ"></a>
                        <div>
                            <ul>
                                <li><a href="<?php echo home_url('/cat/bag/hermes'); ?>">・エルメス</a></li>
                                <li><a href="<?php echo home_url('/cat/wallet/louisvuitton'); ?>">・ルイヴィトン</a></li>
                                <li><a href="<?php echo home_url('/cat/bag/gucci'); ?>">・グッチ</a></li>
                                <li><a href="<?php echo home_url('/cat/bag/prada'); ?>">・プラダ</a></li>
                                <li><a href="<?php echo home_url('/cat/bag/chanel'); ?>">・シャネル</a></li>
                                <li><a href="<?php echo home_url('/cat/bag/saint_laurent'); ?>">・サンローラン</a></li>
                                <li><a href="<?php echo home_url('/cat/bag/celine'); ?>">・セリーヌ</a></li>
                                <li><a href="<?php echo home_url('/cat/bag/dior'); ?>">・ディオール</a></li>
                                <li><a href="<?php echo home_url('/cat/bag/ferragamo'); ?>">・フェラガモ</a></li>
                                <li><a href="<?php echo home_url('/cat/bag/fendi'); ?>">・フェンディ</a></li>
                                <li><a href="<?php echo home_url('/cat/bag/bottegaveneta'); ?>">・ボッテガ</a></li>
                                <li><a href="<?php echo home_url('/cat/bag/loewe'); ?>">・ロエベ</a></li>

                            </ul>
                        </div>


                    </li>
                    <li><a href="<?php echo home_url('cat/outfit'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-category-outfit.png"　alt="洋服・毛皮"></a>




                    </li>
                    <li><a href="<?php echo home_url('cat/wallet'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-category-wallet.png"　alt="ブランド財布"></a>
                        <div>
                            <ul>
                                <li><a href="<?php echo home_url('/cat/wallet/louisvuitton'); ?>">・ヴィトン</a></li>
                                <li><a href="<?php echo home_url('/cat/bag/chanel'); ?>">・シャネル</a></li>
                                <li><a href="<?php echo home_url('/cat/bag/hermes'); ?>">・エルメス</a></li>
                                <li><a href="<?php echo home_url('/cat/bag/prada'); ?>">・プラダ</a></li>
                                <li><a href="<?php echo home_url('/cat/bag/bottegaveneta'); ?>">・ボテッガ</a></li>
                            </ul>
                        </div>


                    </li>
                    <li><a href="<?php echo home_url('cat/shoes'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-category-shoes.png"　alt="靴"></a></li>
                    <li><a href="<?php echo home_url('cat/diamond'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-category-diamond.png"　alt="ダイヤモンド"></a>
                        <div>
                            <ul>
                                <li><a href="<?php echo home_url('/cat/gem/tiffany'); ?>">・ティファニー</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/cartier'); ?>">・カルティエ</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/harrywinston'); ?>">・ハリーウィンストン</a></li>
                            </ul>
                        </div>

                    </li>
                </ul>
            </nav>
        </section>


        <section id="side-categorylist">
            <h3><img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/side-title-br_list.png"></h3>
            <?php if (is_parent_slug() === 'patek-philippe') { ?>

<ul class="side_br_list">
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/calatrava_pilot'); ?>">カラトラバ・パイロット</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/calatrava'); ?>">カラトラバ</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/p_chronograph'); ?>">クロノグラフ</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/grand-complication'); ?>">グランド・コンプリケーション</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/complication'); ?>">コンプリケーション</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/gondolo'); ?>">ゴンドーロ</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/twenty_four'); ?>">トゥエンティーフォー</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/retrograde'); ?>">レトログラード永久カレンダー</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/world_time'); ?>">ワールドタイム</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/annualcalendar'); ?>">年次カレンダー</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/forever'); ?>">永久カレンダー</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/forever_chrono'); ?>">永遠カレンダークロノグラフ</a></li>

                <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
            </ul>
            
<?php } elseif (is_parent_slug() === 'franckmuller') { ?>
<ul class="side_br_list">
                <li><a href="<?php echo home_url('/cat/watch/franckmuller/casablanca'); ?>">カサブランカ</a></li>
                <li><a href="<?php echo home_url('/cat/watch/franckmuller/color_dream'); ?>">カラードリーム</a></li>
                <li><a href="<?php echo home_url('/cat/watch/franckmuller/kureijiawas'); ?>">クレイジーアワーズ</a></li>
                <li><a href="<?php echo home_url('/cat/watch/franckmuller/conquistador_cortez'); ?>">コンキスタドールコルテス</a></li>
                <li><a href="<?php echo home_url('/cat/watch/franckmuller/conquistador'); ?>">コンキスタドール</a></li>
                <li><a href="<?php echo home_url('/cat/watch/franckmuller/tonneau_car_becks'); ?>">トノーカーベックス</a></li>
                <li><a href="<?php echo home_url('/cat/watch/franckmuller/master_square'); ?>">マスタースクエア</a></li>
                <li><a href="<?php echo home_url('/cat/watch/franckmuller/master_bunker'); ?>">マスターバンカー</a></li>
                <li><a href="<?php echo home_url('/cat/watch/franckmuller/long_island'); ?>">ロングアイランド</a></li>
                <li><a href="<?php echo home_url('/cat/watch/franckmuller/vegas'); ?>">ヴェガス</a></li>
                <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>

            </ul>
            
<?php } elseif (is_parent_slug() === 'harrywinston') { ?>
<ul class="side_br_list">
                <li><a href="<?php echo home_url('/cat/gem/harrywinston/hw_ring'); ?>">HWリング</a></li>
                <li><a href="<?php echo home_url('/cat/gem/harrywinston/with_love_from'); ?>">ウィズラブフロムハリーウィンストン</a></li>
                <li><a href="<?php echo home_url('/cat/gem/harrywinston/geometric'); ?>">ジオメトリック</a></li>
                <li><a href="<?php echo home_url('/cat/gem/harrywinston/solitaire'); ?>">ソリティア</a></li>
                <li><a href="<?php echo home_url('/cat/gem/harrywinston/traffic'); ?>">トラフィック</a></li>
                <li><a href="<?php echo home_url('/cat/gem/harrywinston/trist'); ?>">トリスト</a></li>
                <li><a href="<?php echo home_url('/cat/gem/harrywinston/bell'); ?>">ベル</a></li>
                <li><a href="<?php echo home_url('/cat/gem/harrywinston/micropave'); ?>">マイクロパヴェ</a></li>
                <li><a href="<?php echo home_url('/cat/gem/harrywinston/lily_cluster'); ?>">リリークラスター</a></li>
<li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
            </ul>     
            
<?php } elseif (is_parent_slug() === 'piaget') { ?>
<ul class="side_br_list">
                <li><a href="<?php echo home_url('/cat/gem/piaget/upstream'); ?>">アップストリーム</a></li>
                <li><a href="<?php echo home_url('/cat/gem/piaget/altiplano'); ?>">アルティプラノ</a></li>
                <li><a href="<?php echo home_url('/cat/gem/piaget/tanagra'); ?>">タナグラ</a></li>
                <li><a href="<?php echo home_url('/cat/gem/piaget/dancer'); ?>">ダンサー</a></li>
                <li><a href="<?php echo home_url('/cat/gem/piaget/poseshon'); ?>">ポセション</a></li>
                <li><a href="<?php echo home_url('/cat/gem/piaget/limelight'); ?>">ライムライト</a></li>
<li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
            </ul>    
            
<?php } elseif (is_parent_slug() === 'bulgari') { ?>
<ul class="side_br_list">
                <li><a href="<?php echo home_url('/cat/gem/bulgari/b-zero1'); ?>">B-ZERO1</a></li>
                <li><a href="<?php echo home_url('/cat/gem/bulgari/asutorare'); ?>">アストラーレ</a></li>
                <li><a href="<?php echo home_url('/cat/gem/bulgari/arubeare'); ?>">アルベアーレ</a></li>
                <li><a href="<?php echo home_url('/cat/gem/bulgari/corona'); ?>">コロナ</a></li>
                <li><a href="<?php echo home_url('/cat/gem/bulgari/serpenti'); ?>">セルペンティ</a></li>
                <li><a href="<?php echo home_url('/cat/gem/bulgari/cheruki'); ?>">チェルキ</a></li>
                <li><a href="<?php echo home_url('/cat/gem/bulgari/tondo'); ?>">トンド</a></li>
                <li><a href="<?php echo home_url('/cat/gem/bulgari/passodoppio'); ?>">パッソドッピオ</a></li>
                <li><a href="<?php echo home_url('/cat/gem/bulgari/parenteshi'); ?>">パレンテシ</a></li>
                <li><a href="<?php echo home_url('/cat/gem/bulgari/piramidering'); ?>">ピラミデリング</a></li>
                <li><a href="<?php echo home_url('/cat/gem/bulgari/flower_collection'); ?>">フラワーコレクション</a></li>
                <li><a href="<?php echo home_url('/cat/gem/bulgari/prestige'); ?>">プレステージ</a></li>
                <li><a href="<?php echo home_url('/cat/gem/bulgari/horoscope'); ?>">ホロスコープ</a></li>
                <li><a href="<?php echo home_url('/cat/gem/bulgari/marimi'); ?>">マリーミー</a></li>
                <li><a href="<?php echo home_url('/cat/gem/bulgari/marquiscross'); ?>">マーキスクロス</a></li>
                <li><a href="<?php echo home_url('/cat/gem/bulgari/latincross'); ?>">ラテンクロス</a></li>
<li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
            </ul>            
            
            
       
<?php } else { ?>
<ul class="side_br_list">
<li><a href="<?php echo home_url('cat/gem/vancleefarpels'); ?>">ヴァンクリーフアーペル</a></li>
<li><a href="<?php echo home_url('cat/gem/emerald'); ?>">エメラルド</a></li>
<li><a href="<?php echo home_url('cat/gem/opal'); ?>">オパール</a></li>
<li><a href="<?php echo home_url('cat/gem/cartier'); ?>">カルティエ</a></li>
<li><a href="<?php echo home_url('cat/gem/sapphire'); ?>">サファイア</a></li>
<li><a href="<?php echo home_url('cat/gem/tiffany'); ?>">ティファニー</a></li>
<li><a href="<?php echo home_url('cat/gem/tourmaline'); ?>">トルマリン</a></li>
<li><a href="<?php echo home_url('cat/gem/paraibatourmaline'); ?>">パライバトルマリン</a></li>
<li><a href="<?php echo home_url('cat/gem/harrywinston'); ?>">ハリーウィンストン</a></li>
<li><a href="<?php echo home_url('cat/gem/piaget'); ?>">ピアジェ</a></li>
<li><a href="<?php echo home_url('cat/gem/hisui'); ?>">ヒスイ</a></li>
<li><a href="<?php echo home_url('cat/gem/boucheron'); ?>">ブシュロン</a></li>
<li><a href="<?php echo home_url('cat/gem/bulgari'); ?>">ブルガリ</a></li>
<li><a href="<?php echo home_url('cat/gem/ruby'); ?>">ルビー</a></li> 
<li><a href="<?php echo home_url('cat/watch/rolex'); ?>">ロレックス</a></li>                    
<li><a href="<?php echo home_url('cat/watch/iwc'); ?>">IWC</a></li>
<li><a href="<?php echo home_url('cat/watch/vacheron'); ?>">ヴァシュロンコンスタンタン</a></li>
<li><a href="<?php echo home_url('cat/watch/hublot'); ?>">ウブロ</a></li>
<li><a href="<?php echo home_url('cat/watch/audemarspiguet'); ?>">オーデマピゲ</a></li>
<li><a href="<?php echo home_url('cat/watch/omega'); ?>">オメガ</a></li>
<li><a href="<?php echo home_url('cat/watch/patek-philippe'); ?>">パテックフィリップ</a></li>
<li><a href="<?php echo home_url('cat/watch/panerai'); ?>">パネライ</a></li>
<li><a href="<?php echo home_url('cat/watch/franckmuller'); ?>">フランクミューラー</a></li>
<li><a href="<?php echo home_url('cat/watch/breguet'); ?>">ブレゲ</a></li>
<li><a href="<?php echo home_url('cat/watch/alange-soehne'); ?>">ランゲ&amp;ゾーネ</a></li>                   
<li><a href="<?php echo home_url('cat/bag/hermes'); ?>">エルメス</a></li>
<li><a href="<?php echo home_url('cat/bag/gucci'); ?>">グッチ</a></li>
<li><a href="<?php echo home_url('cat/bag/prada'); ?>">プラダ</a></li>
<li><a href="<?php echo home_url('cat/wallet/louisvuitton'); ?>">ルイヴィトン</a></li>
<li><a href="<?php echo home_url('cat/bag/chanel'); ?>">シャネル</a></li>
<li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
</ul>
<?php } ?>

            
            
            
        </section>

        <section id="side-categorylist">
            <h3><img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/side-title-contents.png"></h3>
            <ul class="list-unstyled">
                <li>
                    <a href="<?php echo home_url('voice'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-voice.png"　alt="お客様の声"></a>
                    <p>利用された皆様から嬉しいお言葉を沢山いただきました。</p>
                </li>
                <li>
                    <a href="<?php echo home_url('about-purchase'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-kaitoriabout.png"　alt="買取について"></a>
                    <p>初めての方でも不安無く、安心してご利用いただく為に。</p>
                </li>
                <!--<li>
        <a href="<?php echo home_url('media'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-infomedia.png"　alt="メディア情報"></a>
        <p>雑誌やテレビにて当店の商品が掲載されました。</p>
      </li>-->
                <li>
                    <a href="<?php echo home_url('faq'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-faq.png"　alt="よくある質問"></a>
                    <p>ご質問とその回答。不明な点はこちらから。</p>
                </li>
                <li>
                    <a href="<?php echo home_url('ginza'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-tenpo.png"　alt="店舗案内"></a>
                    <p>銀座駅徒歩30秒の好立地。
                        <br>最寄り駐車場案内。</p>
                </li>
            </ul>
        </section>

        <section>
            <ul class="list-unstyled">
                <li class="aboutsatei"><p id="phone_number_holder">0120-970-060</p><a href="<?php echo home_url('purchase1'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-satei.png" alt="今すぐ査定申込"></a></li>
                <li><a href="<?php echo home_url('line'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/sidebar/btn-line.png" alt="楽しい！簡単！LINE査定はじめました！"></a></li>
            </ul>
        </section>


        <section id="blog-newlist">
            <?php
      // 新着記事4件取得
      $query = array(
        'posts_per_page' => 4,
        'post_type'     => 'blog',
        'post_status'   => 'publish',
        'orderby'       => 'post_modified',
        'order'         => 'DESC',
      );
      $result = new WP_Query($query);
      
      // ログインユーザーのみ閲覧可
      //if(current_user_can('read_private_pages')) :
        echo '<h3>新着コラム</h3>';
        echo '<p class="bloglink"><a href="/blog"> > 一覧を見る</a></p>';
        echo '<ul class="clearfix">';
        while ($result->have_posts()) {
          $result->the_post();
          $cat = get_the_terms(get_the_ID(), 'blog-cat');
          $cat = $cat[0];
          $catName = $cat->name;
          if(!empty($catName)) {
            $catName = '【'.$catName.'】';
          }
          
          echo '<li class="postlist">';
          echo '<a href="'.get_the_permalink().'">';
          the_post_thumbnail();
          echo '<p class="date">'.get_the_time("Y年m月d日（D）").'</p>';
          echo '<h4>'.$catName.get_the_title().'</h4>';
          echo '</a>';
          echo '</li>';
        }
        echo '</ul>';
      //endif;
      wp_reset_postdata();
    ?>
        </section>

    </div>
    <!-- #secondary -->