<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  '001.jpg::エブリンⅢ::トリヨンクレマンス　R刻印::164,000::160,000::170,000::10,000',
  '002.jpg::プラダ::2WAYハンドバッグ::70,000::67,000::74,000::7,000',
  '003.jpg::グッチ::2WAYハンドバッグ::84,000::82,000::90,000::8,000',
  '004.jpg::ルイヴィトン::ダミエ　モンスリGM::110,000::100,000::115,000::15,000',
  '005.jpg::エルメス::ミニケリー　○I刻印::1,200,000::1,180,000::1,236,000::56,000',
  '006.jpg::デカマトラッセ::Ｗフラップ　Ｗチェーン　Ｇ金具::364,000::358,000 ::378,000::20,000',
  '007.jpg::ルイヴィトン::ダミエ　スピーディバンドリエール::118,000::114,000::123,000::9,000',
  '008.jpg::セリーヌ::トラペーズ・スモール::174,000::170,000::180,000::10,000',
  '009.jpg::エルメス::ピコタンロック　エクラPM　X刻印::242,000::230,000::250,000::20,000',
  '010.jpg::クリスチャンディオール::レディディオール::220,000::216,000::240,000::24,000',
  '011.jpg::フェンディ::プチトゥジュール　モンスター　２WAYバッグ::156,000::150,000::160,000::10,000',
  '012.jpg::プラダ::カナパ　ビジュー付き::68,000::65,000::72,000::7,000',
);


get_header(); ?>

<p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
<p class="main_bottom">どこよりも高価買取いたします！ブランドバッグ・鞄買取特別強化中！</p>
<div id="primary" class="cat-page content-area">
    <main id="main" class="site-main" role="main">
        <div id="lp_head" class="bag_ttl">
            <div>
                <p>銀座で最高水準の査定価格・サービス品質をご体験ください。</p>
                <h2>あなたの高級ブランドバッグ<br />
                    どんな物でもお売り下さい！！</h2>
            </div>
        </div>
        <p id="catchcopy">BRANDREVALUE(ブランドリバリュー)では様々な種類のブランドバッグの買取を行っております。人気の高いルイ・ヴィトンやエルメス、コーチ、シャネル、グッチ、プラダなどのブランドバッグ・鞄が査定対象です。ひと昔前のデザインのバッグでもメンテナンス状態がよければ高額での買取査定が可能ですので、ぜひご自宅に使わなくなったバッグがありましたらBRANDREVALUE(ブランドリバリュー)までお持ちください。<br>
            傷があったり、ベルトが切れてしまっているバッグについても、お気軽にご相談ください。<br>
            他店では値段がほとんどつかなかったようなブランドバッグでも、BRAND REVALUEなら納得の高額査定で買い取りができるケースもございます。</p>
        <section id="hikaku" class="bag_hikaku">
            <h3 class="text-hide">他社の買取額と比較すると</h3>
            <p class="hikakuName">HERMES<br>
                バーキン30</p>
            <p class="hikakuText">※状態が良い場合や当店で品薄の場合などは<br>
                　特に高価買取致します。</p>
            <p class="hikakuPrice1"><span class="red">A社</span>：1,300,000円</p>
            <p class="hikakuPrice2"><span class="blue">B社</span>：1,600,000円</p>
            <p class="hikakuPrice3">1,900,000<span class="small">円</span></p>
        </section>

        <section class="kaitori_cat">
            <ul>
                <li>
                    <a href="https://kaitorisatei.info/brandrevalue/blog/doburock"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/12/caf61dc6779ec6137c0ab58dfe3a550d.jpg" alt="どぶろっく"></a>
                </li>
            </ul>
        </section>                   
        <section id="cat-point">
            <h3>高価買取のポイント</h3>
            <ul class="list-unstyled">
                <li>
                    <p class="pt_tl">商品情報が明確だと査定がスムーズ</p>
                    <p class="pt_tx">ブランド名、モデル名が明確だと査定がスピーディに出来、買取価格にもプラスに働きます。また、新作や人気モデル、人気ブランドであれば買取価格がプラスになります。</p>
                </li>
                <li>
                    <p class="pt_tl">数点まとめての査定だと <br>
                        キャンペーンで高価買取が可能</p>
                    <p class="pt_tx">数点まとめての査定依頼ですと、買取価格をプラスさせていただきます。</p>
                </li>
                <li>
                    <p class="pt_tl">品物の状態がよいほど <br>
                        高価買取が可能</p>
                    <p class="pt_tx">お品物の状態が良ければ良いほど、買取価格もプラスになります。</p>
                </li>
            </ul>
            <p>ブランドバッグ・鞄を高く売るためには、日頃のメンテナンスが重要です。よく手入れをすることや気をつけて使うことでバッグは長持ちしますし、売却する際の査定の評価も良いものとなります。また、バッグのブランド名、モデル名、購入した時期などをお伝えいただくことでスピーディーな査定を行うことができます。<br>
                バッグの保証書や箱、付属品の小物やベルトなどがありましたら必ず一緒にご持参ください。付属品が一式そろっていることで、査定金額に反映することができます。 </p>
        </section>
        <h3 class="mid">ブランドバッグ 一覧</h3>
        <ul class="mid_link">
            <li>
            <a href="<?php echo home_url('/cat/bag/hermes/hermes-bag'); ?>">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/01.jpg" alt="エルメス買取">
            <p class="mid_ttl">エルメス</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/celine'); ?>">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/02.jpg" alt="セリーヌ買取">
            <p class="mid_ttl">セリーヌ</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/wallet/louisvuitton/louisvuitton-bag'); ?>">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/03.jpg" alt="ルイヴィトン買取">
            <p class="mid_ttl">ルイヴィトン</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/chanel/chanel-bag'); ?>">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/04.jpg" alt="シャネル買取">
            <p class="mid_ttl">シャネル</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/gucci/gucci-bag'); ?>">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/05.jpg" alt="グッチ買取">
            <p class="mid_ttl">グッチ</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('cat/gem/cartier/cartier-bag'); ?>">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/06.jpg" alt="カルティエ買取">
            <p class="mid_ttl">カルティエ</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/gem/tiffany/tiffany-bag'); ?>">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/07.jpg" alt="ティファニー買取">
            <p class="mid_ttl">ティファニー</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/prada'); ?>">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/09.jpg" alt="プラダ買取">
            <p class="mid_ttl">プラダ</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/fendi/fendi-bag'); ?>">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/10.jpg" alt="フェンディ買取">
            <p class="mid_ttl">フェンディ</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/dior/dior-bag'); ?>">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/08.jpg" alt="ディオール買取">
            <p class="mid_ttl">ディオール</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/saint_laurent'); ?>">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/12.jpg" alt="サンローラン買取">
            <p class="mid_ttl">サンローラン</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/bottegaveneta'); ?>">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/13.jpg" alt="ボッテガ・ヴェネタ買取">
            <p class="mid_ttl">ボッテガ・ヴェネタ</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/ferragamo'); ?>">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/14.jpg" alt="フェラガモ買取">
            <p class="mid_ttl">フェラガモ</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/loewe'); ?>">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/15.jpg" alt="ロエベ買取">
            <p class="mid_ttl">ロエベ</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/berluti'); ?>">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/16.jpg" alt="ベルルッティ">
            <p class="mid_ttl">ベルルッティ</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/goyal'); ?>">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/17.jpg" alt="ゴヤール">
            <p class="mid_ttl">ゴヤール</p>
            </a>
            </li>
            <li>
            <a href="<?php echo home_url('/cat/bag/rimowa'); ?>">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/18.jpg" alt="リモワ">
            <p class="mid_ttl">リモワ</p>
            </a>
            </li>           
            <li>
            <a href="<?php echo home_url('/cat/bag/globe-trotter'); ?>">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/19.jpg" alt="グローブトロッター">
            <p class="mid_ttl">グローブトロッター</p>
            </a>
            </li>  
            <li>
            <a href="<?php echo home_url('/cat/bag/valextra'); ?>">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/bag/20.jpg" alt="ヴァレクストラ">
            <p class="mid_ttl">ヴァレクストラ</p>
            </a>
            </li>             
            </ul>
        <section id="lp-cat-jisseki">
            <h3 class="text-hide">買取実績</h3>
            <ul id="box-jisseki" class="list-unstyled clearfix">
                <?php
            foreach($resultLists as $list):
            // :: で分割
            $listItem = explode('::', $list);
          
          ?>
                <li class="box-4">
                    <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/lp/<?php echo $listItem[0]; ?>" alt="">
                        <p class="itemName"><?php echo $listItem[1]; ?><br>
                            <?php echo $listItem[2]; ?></p>
                        <hr>
                        <p> <span class="red">A社</span>：<?php echo $listItem[3]; ?>円<br>
                            <span class="blue">B社</span>：<?php echo $listItem[4]; ?>円 </p>
                    </div>
                    <div class="box-jisseki-cat">
                        <h3>買取価格例</h3>
                        <p class="price"><?php echo $listItem[5]; ?><span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                        <p><span class="small">買取差額“最大”</span><?php echo $listItem[6]; ?>円</p>
                    </div>
                </li>
                <?php endforeach; ?>
            </ul>
        </section>
        <section id="list-brand" class="clearfix">
            <!--<h3>ブランドリスト</h3>
            <ul class="list-unstyled">
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/hermes'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch33.jpg" alt="エルメス"></dd>
                                <dt><span>Hermes</span>エルメス</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/celine'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch29.jpg" alt="セリーヌ"></dd>
                                <dt><span>CELINE</span>セリーヌ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/wallet/louisvuitton'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch23.jpg" alt="ルイヴィトン"></dd>
                                <dt><span>LOUIS VUITTON</span>ルイヴィトン</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/chanel'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag01.jpg" alt="シャネル"></dd>
                                <dt><span>CHANEL</span>シャネル</dt>
                            </a>
                        </dl>
                    </li>                   
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/gucci'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch25.jpg" alt="グッチ"></dd>
                                <dt><span>GUCCI</span>グッチ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/prada'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag02.jpg" alt="プラダ"></dd>
                                <dt><span>PRADA</span>プラダ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/fendi'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag03.jpg" alt="プラダ"></dd>
                                <dt><span>FENDI</span>フェンディ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/dior'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag04.jpg" alt="ディオール"></dd>
                                <dt><span>Dior</span>ディオール</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                        <a href="<?php echo home_url('/cat/bag/saint_laurent'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag05.jpg" alt="サンローラン"></dd>
                                <dt><span>Saint Laurent</span>サンローラン</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/bottegaveneta'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch32.jpg" alt="ボッテガヴェネタ"></dd>
                                <dt><span>Bottegaveneta</span>ボッテガヴェネタ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/ferragamo'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch31.jpg" alt="フェラガモ"></dd>
                                <dt><span>Ferragamo</span>フェラガモ</dt>
                            </a>
                        </dl>
                    </li>                   
            </ul>-->
        </section>
        <section id="lp-cat-jisseki" class="clearfix">
            <div id="konna" class="konna_bag">
                <p class="example1">■粘つき・ひび割れ</p>
                <p class="example2">■化粧のシミ</p>
                <p class="example3">■日焼け</p>
                <p class="text">その他：汚れ・角すれ等の状態でもお買取りいたします。</p>
            </div>
        </section>
        <section id="about_kaitori" class="clearfix">
            <?php
        // 買取について
        get_template_part('_widerange');
                get_template_part('_widerange2');
      ?>
        </section>
        <section> <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/bn_matomegai.png"> </section>
        <?php
        // 買取基準
        get_template_part('_criterion');

        // NGアイテム
        get_template_part('_no_criterion');
        
                // カテゴリーリンク
        get_template_part('_category');
            ?>
        <section id="kaitori_genre">
            <h3 class="text-hide">その他買取可能なジャンル</h3>
            <p>【買取ジャンル】バッグ/ウエストポーチ/セカンドバック/トートバッグ/ビジネスバッグ/ボストンバッグ/クラッチバッグ/トランクケース/ショルダーバッグ/ポーチ/財布/カードケース/パスケース/キーケース/手帳/腕時計/ミュール/サンダル/ビジネスシューズ/パンプス/ブーツ/ペアリング/リング/ネックレス/ペンダント/ピアス/イアリング/ブローチ/ブレスレット/ライター/手袋/傘/ベルト/ペン/リストバンド/アンクレット/アクセサリー/サングラス/帽子/マフラー/ハンカチ/ネクタイ/ストール/スカーフ/バングル/カットソー/アンサンブル/ジャケット/コート/ブルゾン/ワンピース/ニット/シャツメンズ/毛皮/Tシャツ/キャミソール/タンクトップ/パーカー/ベスト/ポロシャツ/ジーンズ/スカート/スーツなど</p>
        </section>
        <?php
        // 買取方法
        get_template_part('_purchase');
      ?>
        <section id="user_voice">
            <h3>ご利用いただいたお客様の声</h3>
            <p class="user_voice_text1">ちょうど家の整理をしていたところ、家のポストにチラシが入っていたので、ブランドリバリューに電話してみました。今まで買取店を利用したことがなく、不安でしたがとても丁寧な電話の対応とブランドリバリューの豊富な取り扱い品目を魅力に感じ、出張買取を依頼することにしました。
                絶対に売れないだろうと思った、動かない時計や古くて痛んだバッグ、壊れてしまった貴金属のアクセサリーなども高額で買い取っていただいて、とても満足しています。古紙幣や絵画、食器なども買い取っていただけるとのことでしたので、また家を整理するときにまとめて見てもらおうと思います。</p>
            <h4>鑑定士からのコメント</h4>
            <div class="clearfix">
                <p class="user_voice_text2">家の整理をしているが、何を買い取ってもらえるか分からないから一回見に来て欲しいとのことでご連絡いただきました。
                    買取店が初めてで不安だったが、丁寧な対応に非常に安心しましたと笑顔でおっしゃって頂いたことを嬉しく思います。
                    物置にずっとしまっていた時計や、古いカバン、壊れてしまったアクセサリーなどもしっかりと価値を見極めて高額で提示させて頂きましたところ、お客様もこんなに高く買取してもらえるのかと驚いておりました。
                    これからも家の不用品を整理するときや物の価値判断ができないときは、すぐにお電話しますとおっしゃって頂きました。</p>
                <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/photo-user-voice.jpg" class="right"> </div>
        </section>
        <section>
            <div class="cont_widerange">
                <h4>バッグの買取で取扱いブランドは？<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
                <ul>
                    <li>ブランド買取の代表アイテムと言えば、有名ブランドバッグを思い浮かべる人も多いでしょう。ブランドバッグに興味がないという人も、もしかすると家にひとつは持っているという人もいるかもしれません。これまではブランドバッグと言えばレディースファッションブランドが多いイメージでしたが、最近ではメンズブランドからもクラッチバッグやバックパック、ビジネスバッグなどたくさんのバッグコレクションやモデルが発表されているため、男性からも関心のたかいアイテムとなっています。それだけ日本人にとってブランドバッグは、高いデザイン性と優れた機能性を持ち合わせたとても身近なファッションアイテムと言えるでしょう。<br>
                        「プレゼントでもらったブランドバッグだけどどうも好みに合わない。できれば買取ってもらいたい」、「最近使わなくなったバッグ、買取ってもらえるかな？」といったバッグがあれば、まず気になるのがそのバッグのブランドが買取で取扱ブランドになっているかという点でしょう。有名インポートブランドの場合はほとんど取扱いブランドとなっていますが、どうしても心配な時は買取店へ直接問い合わせても良いでしょう。<br>
                        バッグ買取ではルイヴィトン、シャネル、エルメスが三大バッグブランドを中心にグッチ、プラダ、コーチ、ボッテガヴェネタなどの多くの有名インポートブランドが代表的な取扱いブランドとなっています。もちろん、ドメスティックブランドでも買取取扱ブランドとなっているものもありますが、中古市場でもヴィトンやシャネル、エルメスなどのインポートブランドの人気がとても高いので、買取店によっては取扱いブランドとなっていない場合もあります。できるだけ査定に出す前に買取店に問い合わせてみるのも良いかもしれません。<br>
                        新しいコレクションや新品であれば高額買取になる場合が多いですが、取扱いブランドであれば古いデザインやコレクションのバッグでも買取対象となるので家で眠っているだけのバッグであれば買取ってもらった方が得策と言えるでしょう。<br>
                        <br>
                    </li>
                </ul>
            </div>
            <div class="cont_widerange">
                <h4>高額査定が付くバッグの特徴<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
                <ul>
                    <li>ブランドバッグをせっかく買取ってもらうのなら、少しでも高額査定をしてもらって高く買取ってもらいたいと思うのは当然のことです。しかし、その高額査定をしてもらうポイントは素人ではなかなか分かりにくいものですが、査定に出す前にちょっとした一手間で査定額が上がる可能性もあるので、ぜひ高額査定になるバッグの特徴をご紹介していきましょう。<br>
                        まずブランドバッグの買取で高額査定が付くバッグの大きな特徴として、ブランドブティックだけでなく中古市場でも人気や稀少性が高く、需要が高いバッグであることが挙げられます。特に、直営ブランドブティックで完売してしまったり、生産数が極端に少なくなかなか手には入らないようなレアなブランドバッグや、爆発的な人気となっているモデルのブランドバッグ、中古市場でも安定した人気を誇る定番のブランドバッグなどが高額査定になりやすいと言えるでしょう。<br>
                        また中古市場でも人気ブランドバッグの最新コレクションも、高額査定となりやすいバッグの特徴です。発売直後で注目されているため早い回転が見込まれてはいても、中古市場にまだ流通量が少ないため高額査定となる場合が多いのです。さらに、最新モデルの場合は未使用の新品のまま買取店に持ち込まれるケースが多く、商品の状態が「新品」の最高ランクになることも高額査定に繋がるからなのです。ブランドバッグを購入したものの「使わないなあ」と思ったら、早めに査定買取に出すのも高額査定になるポイントなのです。<br>
                        その他には使用したブランドバッグの状態が査定には大きく影響してきますが、丁寧な取扱い方や日頃からのお手入れによって良い状態を維持することができるので手元にある間に気を付けておくと良いでしょう。また査定時にはブランドバッグの付属品の有無によって査定価格が変わってくる場合があります。購入時に付いてくるギャランティーカードや箱、カデナ、などの付属品を揃えっているブランドバッグは高額査定になる場合もあるので、査定前に付属品も必ず確認しておきましょう。<br>
                        <br>
                    </li>
                </ul>
            </div>
            <div class="cont_widerange">
                <h4>高額買取になる人気ブランドバッグ<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
                <ul>
                    <li>ブランドバッグと言えば、やはり人気ブランドのバッグが欲しくなりますよね。バッグは本来物を入れるための道具なので機能性やデザインから選ぶ人も多いですが、なかにはまず人気のブランドからバッグを探す人もいるでしょう。たとえば、「フォーマルなバッグが欲しいけど、シャネルなら上品なバッグがあるかもしれない」、「軽くて丈夫なトートバッグが欲しいから、もしかするとルイヴィトンで探してみよう」といった探し方をする人もいるかもしれません。その時に気にするポイントとして人気ブランドなのか、また人気のあるブランドバッグなのかといった点ではないでしょうか？<br>
                        ブランドバッグの魅力はその品質の高さはもちろんですが、なんといってもブランドバッグのステータスの他なりません。そのため、多少高額であっても有名ブランドでバッグを選ぶ人は多いものです。もちろん、中古市場でブランドバッグを探す時もそのバッグが放つブランドステータスを含めて、人気ブランドバッグを探していることが多いものです。そのため、人気ブランドのバッグは中古市場でも必然的に人気が高く、買取も高額な価格になることが多いのです。言い換えれば、ブランドの人気の高さも査定に含まれるといっても良いでしょう。<br>
                        気になるバッグ買取の人気ブランドはルイヴィトン、シャネル、エルメスの３大ブランドとなっており、その他にグッチ、プラダなどのブランドが続きます。買取されるブランドバッグのおよそ60％はルイヴィトンのバッグとも言われており、ルイヴィトンの中でも定番のモノグラムやダミエは特に人気の高いラインとなっています。また、人気ブランドの中でも他のブランドと一線を画しているエルメスは女性を中心に人気が高く、特にバーキンは絶対的な人気があるバッグであることから、革の種類やサイズによりますが100万円以上の買取価格になることも少なくありません。<br>
                        このように人気ブランドバッグであれば査定でもその人気や価値もしっかり評価の対象となるため、全体的に高額買取となる可能性が高いと言えるでしょう。 </li>
                </ul>
            </div>
        </section>
    </main>
    <!-- #main --> 
</div>
<!-- #primary -->

<?php
get_sidebar();
get_footer();

