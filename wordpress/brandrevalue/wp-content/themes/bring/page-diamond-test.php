<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
  //'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  '001.jpg::ルースダイヤ　1.098ct　G　SI2　G　NONE::330,000::335,000::340,000::10,000',
  '002.jpg::ダイヤネックレス　Pt850　0.81ct　MD0.23ct::340,000::330,000::350,000 ::17,000',
  '003.jpg::ヴァンクリーフ&アーペル　クロアミニ　アチュールクロス　ダイヤネックレス::117,000::105,000::110,000 ::5,000',
  '004.jpg::ダイヤリング　Pt900　1.00ｃｔ　0.4ｃｔ::62,000::66,000::70,000::8,000',
  '005.jpg::ティファニー　Pt900　ダイヤリング::30,000::27,000::32,000::5,000',
  '006.jpg::ルースダイヤ　1.006ct　H　VS2　VG　NONE　::440,000::445,000::450,000::1,000',
  '007.jpg::ダイヤリング　Pt900　0.51ct　::45,000::47,800::50,000::5,000',
  '008.jpg::ルースダイヤ　1.793ct　J　SI2　G　NONE　::620,000::611,000::630,000::19,000',
  '009.jpg::ダイヤリング　K18　1.48ｃｔ　::58,000::60,000::63,000::5,000',
  '010.jpg::ブルガリ　B-ZERO1　ダイヤネネックレス　K18WG::220,000::215,000::225,000::10,000',
  '011.jpg::ブシュロン　ディアマン　ダイヤネックレス　750::225,000::219,000::230,000::11,000',
  '012.jpg::カルティエ　ハーフダイヤラブリング::97,000::93,000::103,000::10,000',
);


get_header(); ?>

<p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
<p class="main_bottom">ダイヤモンド買取！！業界最高峰の買取価格をお約束いたします。</p>
<div id="primary" class="cat-page content-area">
	<main id="main" class="site-main" role="main">
		<div id="lp_head" class="dia_ttl">
			<div>
				<p>銀座で最高水準の査定価格・サービス品質をご体験ください。</p>
				<h2>あなたのダイヤモンド<br />
					どんな物でもお売り下さい！！</h2>
			</div>
		</div>
		<p id="catchcopy">BRAND REVALUEがモットーとしているのは「お客様に利益を還元する店舗づくり」。路面店ではなくあえて家賃の安い空中階に店舗を構え、人件費・広告費を抑えているのも、削減した経費の分査定額を高めるためです。ダイヤモンドの買取においても、「鑑定書がないダイヤモンド」、「ノンブランドのダイヤモンド」、「極小粒のダイヤモンド（メレ）」などどんなものでもご満足いただける価格で買い取らせていただきます。新しい店舗ではありますが、経験豊かな鑑定士が適切に査定を行いますので、どうかお気軽にご相談ください。</p>
		<section id="hikaku" class="dia_hikaku">
			<h3 class="text-hide">他社の買取額と比較すると</h3>
			<p class="hikakuName">ルースダイヤ　1.793ct　J　SI2　G　NONE</p>
			<p class="hikakuText">※状態が良い場合や当店で品薄の場合などは特に高価買取致します。 </p>
			<p class="hikakuPrice1"><span class="red">A社</span>：620,000円</p>
			<p class="hikakuPrice2"><span class="blue">B社</span>：611,000円</p>
			<p class="hikakuPrice3">630,000<span class="small">円</span></p>
		</section>
		<section id="cat-point">
			<h3>高価買取のポイント</h3>
			<ul class="list-unstyled">
				<li>
					<p class="pt_tl">商品情報が明確だと査定がスムーズ</p>
					<p class="pt_tx">ブランド名、モデル名が明確だと査定がスピーディに出来、買取価格にもプラスに働きます。また、新作や人気モデル、人気ブランドであれば買取価格がプラスになります。</p>
				</li>
				<li>
					<p class="pt_tl">数点まとめての査定だと <br>
						キャンペーンで高価買取が可能</p>
					<p class="pt_tx">数点まとめての査定依頼ですと、買取価格をプラスさせていただきます。</p>
				</li>
				<li>
					<p class="pt_tl">品物の状態がよいほど高価買取が可能</p>
					<p class="pt_tx">お品物の状態が良ければ良いほど、買取価格もプラスになります。</p>
				</li>
			</ul>
			<p>天然の鉱物の中で最も硬度が高いといわれるダイヤモンドですが、実は瞬間的な衝撃には弱く、欠けてしまうことも珍しくありません。 <br>
				そのため、査定のためお持ちいただく際にも傷がつかないよう、緩衝材等で保護いただくことをお勧めしております。また、ご自宅で軽く布でぬぐうなどして汚れを落としておいた方が、査定額は高くなります。ときどきリングやペンダントからダイヤモンドを外して持ってこられる方もいらっしゃいますが、取り外しの際に傷がつく可能性があるので、そのままお持ちください。もし購入時に入手した鑑別書があれば、BRANDREVALUE(ブランドリバリュー)へぜひお持ちください。査定がスムーズになり、買取価格も上がりやすくなります。もちろん鑑定書等がなくてもご心配なく。経験豊かな鑑定士が専用の機器を使い、迅速かつ正確に鑑定を行います。 </p>
		</section>
		<section class="gb_point">
			<h3>良い点・悪い点・保存方法</h3>
			<ul>
				<li>
					<p class="gb_tl"><span>1</span>ダイヤモンドの良い点</p>
					<p>ダイヤモンドはメレダイヤと呼ばれる小さなものから価格が付き、 ダイヤモンドと人工石を判別する方法も存在するため、他の宝石と違い査定にかかる時間も短くなることが多いです。
						貴金属と違い日にちの単位で相場が大きく動くことは少ないため、安定した査定金額を提供することが出来ます。</p>
				</li>
				<li>
					<p class="gb_tl"><span>2</span>ダイヤモンドの悪い点</p>
					<p>カラットが大きくなるほどグレードの違いによる価格の差が大きくなるため、 同じカラット数でも1ctを超えるものであれば査定金額が10万円単位で差が出ることも御座います。
						また、鑑定書にもグレードがあり評価の低い鑑定書、 評価基準が古い鑑定書の場合は鑑定書の内容どおりの査定とはならないことが多く御座います。</p>
				</li>
				<li>
					<p class="gb_tl"><span>3</span>ダイヤモンドの保存方法</p>
					<p>キズが付かないように保管して頂くために、 ダイヤモンドの付いているアクセサリーは専用のケースかケースの無い場合は緩衝材に包み、 他のアクセサリーなどと直接当たることの無いように保管をしましょう。
						また、ダイヤモンド自体に汚れが付着することも多いため掃除をすることも綺麗なダイヤモンドを保たせます。</p>
				</li>
			</ul>
		</section>
		<!--買取評価チェックコンテンツ-->
		
		<section id="check_cont">
			<h3>4C評価を入れて買取相場をチェック</h3>
			<form action="" name="sel_form" id="sel_form">
				<table class="kakaku_input">
					<tr>
						<th><span>●</span>カラー / 色</th>
						<td><select id="color_id" name="color_id">
								<option value="">選択して下さい</option>
								<option value="1">D</option>
								<option value="2">E</option>
								<option value="3">F</option>
								<option value="4">G</option>
								<option value="5">H</option>
								<option value="6">I</option>
								<option value="7">J</option>
							</select></td>
						<th><span>●</span>カット / 総合評価</th>
						<td><select id="cut_id" name="cut_id">
								<option value="">選択して下さい</option>
								<option value="1">Excellent(優秀)</option>
								<option value="2">Verygood(優良)</option>
								<option value="3">Good(良好)</option>
							</select></td>
					</tr>
					<tr>
						<th><span>●</span>クラリティ /内包物</th>
						<td><select id="clarity_id" name="clarity_id">
								<option value="">選択して下さい</option>
								<option value="1">VVS1</option>
								<option value="2">VVS2</option>
								<option value="3">VS1</option>
								<option value="4">VS2</option>
								<option value="5">SI1</option>
								<option value="6">SI2</option>
							</select></td>
						<th><span>●</span>カラット /重量</th>
						<td><input name="carat_value" id="carat_value" type="text" style="ime-mode:disabled;"></td>
					</tr>
				</table>
				<div id="kakaku_btn">
					<input type="button" class="hyouka_btn" id="estimation_btn" value="買取価格を調べる">
				</div>
				<div class="kakaku_box">
					<table>
						<tr>
							<th>買取価格：</th>
							<td id="purchase_price"></td>
						</tr>
					</table>
				</div>
			</form>
		</section>
		<section id="lp-cat-jisseki">
			<h3 class="text-hide">買取実績</h3>
			<ul id="box-jisseki" class="list-unstyled clearfix">
				
				<!-- 1 
          <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/dia/lp/001.jpg" alt="">
            <p class="itemName">ダイヤルース1.098ct</p>
            <!-- p class="itemdetail">カラット：1.003ct<br>
              カラー:E<br>
              クラリティ:SI1<br>
              カット:EXCELLENT<br>
              形状:ラウンドブリリアントカット</p>
            <hr>
            <p><span class="red">A社</span>：142,000円<br>
              <span class="blue">B社</span>：140,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price">145,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>5,000円</p>
          </div>
        </li>
             
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/dia/lp/002.jpg" alt="">
            <p class="itemName">DR0.81ct　MD0.23ct　Pt850ネックレス</p>
            <hr>
            <p> <span class="red">A社</span>：65,000円<br>
              <span class="blue">B社</span>：64,300円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price">69,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>4,700円</p>
          </div>
        </li>
         
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/dia/lp/003.jpg" alt="">
            <p class="itemName">ヴァンクリーフ＆アーペル　クロアミニ　K18WG</p>
            <hr>
            <p> <span class="red">A社</span>：85,000円<br>
              <span class="blue">B社</span>：82,000円</p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price">87,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>5,000円</p>
          </div>
        </li>
        
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/dia/lp/004.jpg" alt="">
            <p class="itemName">Pt900　ダイヤリング</p>
            <hr>
            <p> <span class="red">A社</span>：25,000円<br>
              <span class="blue">B社</span>：23,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price">27,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>4,000円</p>
          </div>
        </li>
        
         
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/dia/lp/005.jpg" alt="">
            <p class="itemName">ティファニー　Pt900　ダイヤリング</p>
            <hr>
            <p> <span class="red">A社</span>：30,000円<br>
              <span class="blue">B社</span>：27,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price">32,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>5,000円</p>
          </div>
        </li>
        
          
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/dia/lp/006.jpg" alt="">
            <p class="itemName">ダイヤルース1.006ct</p>
            <hr>
            <p> <span class="red">A社</span>：159,000円<br>
              <span class="blue">B社</span>：158,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price">166,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>8,000円</p>
          </div>
        </li>
        
        
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/dia/lp/007.jpg" alt="">
            <p class="itemName">MD1.0ct　Pt900リング</p>
            <hr>
            <p> <span class="red">A社</span>：40,000円<br>
              <span class="blue">B社</span>：39,800円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price">42,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>2,200円</p>
          </div>
        </li>
        
        
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/dia/lp/008.jpg" alt="">
            <p class="itemName">ダイヤルース　1.79ct</p>
            <hr>
            <p> <span class="red">A社</span>：576,000円<br>
              <span class="blue">B社</span>：568,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price">583,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>15,000円</p>
          </div>
        </li>
          
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/dia/lp/009.jpg" alt="">
            <p class="itemName">MD1.48ct　K18リング</p>
            <hr>
            <p> <span class="red">A社</span>：49,000円<br>
              <span class="blue">B社</span>：46,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price">51,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>5,000円</p>
          </div>
        </li>
        
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/dia/lp/010.jpg" alt="">
            <p class="itemName">ブルガリ　B-ZERO1　K18WG　MD</p>
            <hr>
            <p> <span class="red">A社</span>：186,000円<br>
              <span class="blue">B社</span>：182,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price">192,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>10,000円</p>
          </div>
        </li>
        
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/dia/lp/011.jpg" alt="">
            <p class="itemName">ブシュロン　10Pダイヤネックレス</p>
            <hr>
            <p> <span class="red">A社</span>：200,000円<br>
              <span class="blue">B社</span>：194,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price">205,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>11,000円</p>
          </div>
        </li>
          
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/dia/lp/012.jpg" alt="">
            <p class="itemName">カルティエ　ハーフダイヤラブリング</p>
            <hr>
            <p> <span class="red">A社</span>：97,000円<br>
              <span class="blue">B社</span>：93,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price">103,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>10,000円</p>
          </div>
        </li>
        
 end -->
				
				<?php
            foreach($resultLists as $list):
            // :: で分割
            $listItem = explode('::', $list);
          
          ?>
				<li class="box-4">
					<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/dia/lp/<?php echo $listItem[0]; ?>" alt="">
						<p class="itemName"><?php echo $listItem[1]; ?></p>
						<hr>
						<p> <span class="red">A社</span>：<?php echo $listItem[2]; ?>円<br>
							<span class="blue">B社</span>：<?php echo $listItem[3]; ?>円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price"><?php echo $listItem[4]; ?><span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span><?php echo $listItem[5]; ?>円</p>
					</div>
				</li>
				<?php endforeach; ?>
			</ul>
			<section id="list-brand" class="clearfix">
				<h3>ブランドリスト</h3>
				<ul class="list-unstyled">
					<li>
						<dl>
							<a href="<?php echo home_url('/cat/gold/ingot'); ?>">
							<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold05.jpg" alt="インゴット"></dd>
							<dt>インゴット</dt>
							</a>
						</dl>
					</li>
					<li>
						<dl>
							<a href="<?php echo home_url('/cat/gold/gold-coin'); ?>">
							<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold11.jpg" alt="金貨"></dd>
							<dt>金貨</dt>
							</a>
						</dl>
					</li>
					<li>
						<dl>
							<a href="<?php echo home_url('/cat/gold/14k'); ?>">
							<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold01.jpg" alt="14金"></dd>
							<dt>14金</dt>
							</a>
						</dl>
					</li>
					<li>
						<dl>
							<a href="<?php echo home_url('/cat/gold/18k'); ?>">
							<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold02.jpg" alt="18金"></dd>
							<dt>18金</dt>
							</a>
						</dl>
					</li>
					<li>
						<dl>
							<a href="<?php echo home_url('/cat/gold/22k'); ?>">
							<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold03.jpg" alt="22金"></dd>
							<dt>22金</dt>
							</a>
						</dl>
					</li>
					<li>
						<dl>
							<a href="<?php echo home_url('/cat/gold/24k'); ?>">
							<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold04.jpg" alt="24金"></dd>
							<dt>24金</dt>
							</a>
						</dl>
					</li>
					<li>
						<dl>
							<a href="<?php echo home_url('/cat/gold/white-gold'); ?>">
							<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold06.jpg" alt="ホワイトゴールド"></dd>
							<dt>ホワイトゴールド</dt>
							</a>
						</dl>
					</li>
					<li>
						<dl>
							<a href="<?php echo home_url('/cat/gold/pink-gold'); ?>">
							<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold07.jpg" alt="ピンクゴールド"></dd>
							<dt>ピンクゴールド</dt>
							</a>
						</dl>
					</li>
					<li>
						<dl>
							<a href="<?php echo home_url('/cat/gold/yellow-gold'); ?>">
							<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold08.jpg" alt="イエローゴールド"></dd>
							<dt>イエローゴールド</dt>
							</a>
						</dl>
					</li>
					<li>
						<dl>
							<a href="<?php echo home_url('/cat/gold/platinum'); ?>">
							<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold09.jpg" alt="プラチナ"></dd>
							<dt>プラチナ</dt>
							</a>
						</dl>
					</li>
					<li>
						<dl>
							<a href="<?php echo home_url('/cat/gem/cartier'); ?>">
							<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem01.jpg" alt="カルティエ"></dd>
							<dt>Cartier<span></span>カルティエ</dt>
							</a>
						</dl>
					</li>
					<li>
						<dl>
							<a href="<?php echo home_url('/cat/gem/tiffany'); ?>">
							<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem02.jpg" alt="ティファニー"></dd>
							<dt><span>Tiffany</span>ティファニー</dt>
							</a>
						</dl>
					</li>
					<li>
						<dl>
							<a href="<?php echo home_url('/cat/gem/harrywinston'); ?>">
							<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem03.jpg" alt="ハリーウィンストン"></dd>
							<dt><span>Harrywinston</span>ハリーウィンストン</dt>
							</a>
						</dl>
					</li>
					<li>
						<dl>
							<a href="<?php echo home_url('/cat/gem/piaget'); ?>">
							<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch22.jpg" alt="ピアジェ"></dd>
							<dt><span>PIAGET</span>ピアジェ</dt>
							</a>
						</dl>
					</li>
					<li>
						<dl>
							<a href="<?php echo home_url('/cat/gem/vancleefarpels'); ?>">
							<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch30.jpg" alt="ヴァンクリーフ&アーペル"></dd>
							<dt><span>Vanleefarpels</span>ヴァンクリーフ&アーペル</dt>
							</a>
						</dl>
					<li>
						<dl>
							<a href="<?php echo home_url('/cat/gem/bulgari'); ?>">
							<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch27.jpg" alt="ブルガリ"></dd>
							<dt><span>BVLGALI</span>ブルガリ</dt>
							</a>
						</dl>
					</li>
					<li>
						<dl>
							<a href="<?php echo home_url('/cat/gem/boucheron'); ?>">
							<dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gem/gem06.jpg" alt="ブシュロン"></dd>
							<dt><span>Boucheron</span>ブシュロン</dt>
							</a>
						</dl>
					</li>
				</ul>
			</section>
			<div class="more_buy dia">
				<div class="mr_tx_wrap">
					<p class="mr_tl">傷・汚れが着かない保存</p>
					<p class="mr_tx"> ダイヤモンドは天然の鉱物で最も固いとされておりますが、硬いが故に 瞬間的な衝撃でヒビ、欠けが出来てしまい、その度合いによって評価は下がってしまいます。
						普段のご使用はもちろん、当店にお持ちになる際にもダイヤモンドにキズ等が付かないように緩衝材等で保護した状態でお持ち下さることをお勧め致します。<br>
						<br>
						また、ダイヤモンドの評価基準としてクラリティ（内包物の有無）が御座いますので、 可能な限り汚れを取り除いておかれたほうが汚れと内包物を混同されることなく評価されますので高価買取に繋がります。
						リフォームや貴金属とダイヤを外して別々にされますと、高い確率でダイヤモンドにキズが付いてしまいますので ダイヤモンドの付いているアクセサリーはそのままでお持ちいただくことをお勧め致します。 </p>
				</div>
			</div>
		</section>
		<section class="clearfix" id="lp-cat-jisseki">
			<div id="konna" class="konna_dia">
				<p class="example1">■デザインが古い</p>
				<p class="example2">■メレダイヤ</p>
				<p class="example3">■石のみ</p>
				<p class="text">その他：鑑定書無し等でもお買取りいたします。</p>
			</div>
		</section>
		<section id="about_kaitori" class="clearfix">
			<?php
        // 買取について
        get_template_part('_widerange');
				get_template_part('_widerange2');
      ?>
		</section>
		<section> <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/bn_matomegai.png"> </section>
		<?php
        // 買取基準
        get_template_part('_criterion');

        // NGアイテム
        get_template_part('_no_criterion');
        
				// カテゴリーリンク
        get_template_part('_category');
			?>
		<section id="kaitori_genre">
			<h3 class="text-hide">その他買取可能なジャンル</h3>
			<p>【買取ジャンル】バッグ/ウエストポーチ/セカンドバック/トートバッグ/ビジネスバッグ/ボストンバッグ/クラッチバッグ/トランクケース/ショルダーバッグ/ポーチ/財布/カードケース/パスケース/キーケース/手帳/腕時計/ミュール/サンダル/ビジネスシューズ/パンプス/ブーツ/ペアリング/リング/ネックレス/ペンダント/ピアス/イアリング/ブローチ/ブレスレット/ライター/手袋/傘/ベルト/ペン/リストバンド/アンクレット/アクセサリー/サングラス/帽子/マフラー/ハンカチ/ネクタイ/ストール/スカーフ/バングル/カットソー/アンサンブル/ジャケット/コート/ブルゾン/ワンピース/ニット/シャツメンズ/毛皮/Tシャツ/キャミソール/タンクトップ/パーカー/ベスト/ポロシャツ/ジーンズ/スカート/スーツなど</p>
		</section>
		<?php
        // 買取方法
        get_template_part('_purchase');
      ?>
		<section id="user_voice">
			<h3>ご利用いただいたお客様の声</h3>
			<p class="user_voice_text1">ちょうど家の整理をしていたところ、家のポストにチラシが入っていたので、ブランドリバリューに電話してみました。今まで買取店を利用したことがなく、不安でしたがとても丁寧な電話の対応とブランドリバリューの豊富な取り扱い品目を魅力に感じ、出張買取を依頼することにしました。 絶対に売れないだろうと思った、動かない時計や古くて痛んだバッグ、壊れてしまった貴金属のアクセサリーなども高額で買い取っていただいて、とても満足しています。古紙幣や絵画、食器なども買い取っていただけるとのことでしたので、また家を整理するときにまとめて見てもらおうと思います。 </p>
			<h4>鑑定士からのコメント</h4>
			<div class="clearfix">
				<p class="user_voice_text2">家の整理をしているが、何を買い取ってもらえるか分からないから一回見に来て欲しいとのことでご連絡いただきました。 買取店が初めてで不安だったが、丁寧な対応に非常に安心しましたと笑顔でおっしゃって頂いたことを嬉しく思います。 物置にずっとしまっていた時計や、古いカバン、壊れてしまったアクセサリーなどもしっかりと価値を見極めて高額で提示させて頂きましたところ、お客様もこんなに高く買取してもらえるのかと驚いておりました。 これからも家の不用品を整理するときや物の価値判断ができないときは、すぐにお電話しますとおっしゃって頂きました。 </p>
				<img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/photo-user-voice.jpg" class="right"> </div>
		</section>
		<section>
			<div class="cont_widerange">
				<h4>ダイヤモンドの買取には鑑定書など付属品をお忘れなく<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
				<ul>
					<li>ダイヤモンドは「宝石の王様」と呼ばれ、その永遠に輝き続ける美しさから婚約指輪や結婚指輪に使用されることが多い世界で最高の人気を誇る宝石です。宝石言葉として「永遠の絆」「不屈」を持つため、結婚などの時には世界中の女性が永遠に憧れる宝石のひとつでしょう。４月の誕生石としても有名ですが、自分の誕生石でなくても好んでダイヤモンドジュエリーを集める女性も多くいます。<br>
						その美しさとは合わせて、ダイヤモンドは宝石の中で最も硬い宝石としても有名で、硬度を表すレベルでもモース硬度10とされています。この硬度によって加工がしやすく、小さなかけらとなってもカットや研磨によってその輝きを失うことはないため、ジュエリーには欠かせない宝石となっています。確かに、シンプルな結婚指輪に小さなダイヤモンドがセットされているだけでも、その指輪の印象は華やかなものになり、ルビーやサファイア、エメラルドといった主石の周りにセットされるだけでもジュエリーとして一層ゴージャス感が増します。<br>
						ダイヤモンドを購入したり贈られたりするタイミングは、婚約指輪や結婚指輪、スウィートテンなど、たとえファッションジュエリーでもやはり何か人生のイベントとなることが多いものです。そのため、ダイヤモンドのジュエリーを売却するには、手放す人それぞれに理由があるのかもしれません。価値のあるダイヤモンドはジュエリーとしてだけでなく、宝石としても大いに価値のある石なので、できれば宝石やジュエリーをきちんと鑑定できる専門鑑定士がいるお店や業者で買取をしてもらいましょう。ダイヤモンドの買取をしてもらう時は、ダイヤモンドの石やジュエリーを購入した際に発行される購入年月日やダイヤモンドの情報、４Ｃランクがしっかりと記載されている鑑定書や品質保証書、保存専用ボックスや保存袋、ブランドジュエリーの場合はギャランティーカードやブランドの刻印あるショッパー袋などの付属品をしっかり揃えておくとダイヤモンドの品質が明確なだけに査定価格が高額になる可能性があります。付属品もしっかり準備しておきましょう。<br>
						<br>
					</li>
				</ul>
			</div>
			<div class="cont_widerange">
				<h4>知っておこう！ダイヤモンドの種類<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
				<ul>
					<li>無色透明の石から放たれるブリリアンスやファイアの美しさ、燦然と輝くダイヤモンド。元来ダイヤモンドの語源はギリシャ語の「アマダス」に由来し、「侵されざる物」という意味を持っています。長い年月と自然の力だけによって創り上げられるその美しさは人の手が到底及ばない天然の産物であることへの敬意が表れているのかもしれません。だからこそ、宝石の中の王様とも呼ばれるダイヤモンド、ダイヤモンドジュエリーには他の宝石とは一線を画すほどの価値があるのでしょう。<br>
						素人の見た目には分かりませんが、ダイヤモンドには様々な種類があり、ダイヤモンドの評価基準がしっかり決まっているのです。ダイヤモンドを購入する時、そして売却する時も、ダイヤモンドの種類や評価基準によって価格が決まります。<br>
						まずダイヤモンドの種類についてもっとも一般の人にも分かりやすいのが、カラーの種類でしょう。ダイヤモンドの中でも最もポピュラーで価値が高いのが無色透明のものになります。光の屈折によって放たれるブリリアンスやファイアの輝きは無色であるがためにより一層増します。稀少なカラーダイヤモンドとして、ブルーダイヤモンドやピンクダイヤモンド、レッドダイヤモンドが挙げられ、その稀少性から価格も高価な石となっています。比較的価格が安価なカラーダイヤモンドになるとブラウンダイヤモンド、イエローダイヤモンドとなり、カラーによってその価値は大きく変わってきます。<br>
						その他、ダイヤモンドの価値評価には、４Ｃと呼ばれるダイヤモンドだけに適用される品質基準が使用されます。４ＣはColor カラー(色)、Clarity クラリティ(透明度)、Carat カラット(重量)、Cut カット(カット)といった４つの評価基準の頭文字の「Ｃ」を表したものです。これらの４つの各評価基準に照らし合わせ、一石一石を鑑定して総合点がその石の価値となり、価格が決められていきます。購入時に発行される鑑定書には必ずそのダイヤモンドの評価が記載されているので、査定買取に出す前に一度確認してみましょう。<br>
						<br>
					</li>
				</ul>
			</div>
			<div class="cont_widerange">
				<h4>ブランドは関係する？ダイヤモンドアクセサリーの買取<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
				<ul>
					<li>ダイヤモンドジュエリーを有名ブランドで選ぶとしたら、どんなブランドのダイヤモンドジュエリーを思い浮かべますか？女性から絶大な人気を誇る代表的なジュエリーブランドには、ハリーウィンストン、ヴァンクリーフ＆アーペル、ティファニー、カルティエ、ピエジェなどの名前が挙がるのではないでしょうか？なかには、シャネルのように、ファインジュエリーの分野からダイヤモンドジュエリーを発表しているクチュールブランドもあります。このように、美しくデザインされたダイヤモンドジュエリーは、一般的な宝飾市場だけでなく、ブランド市場でも多く発表、販売されています。ブランドジュエリーの場合はそのブランドの専属デザイナーがブランドのテーマに沿ってデザインしているため、時には多くの女性から絶大な人気を得るようなブランドアイコンとも称されるダイヤモンドファッションジュエリーも数多くあります。<br>
						高額な価格のブランドダイヤモンドジュエリーですが、憧れのブランドの刻印があるダイヤモンドジュエリーを手にした時の嬉しさや幸せ感が格別なものでしょう。できれば少しでも長く手元に置いておきたいものですが、様々な理由からダイヤモンドジュエリーを手放す人もいます。せっかく手放すのなら、ダイヤモンド自体の価値とブランド価値をきちんと鑑定ができる買取店で少しでも高額査定をしてもらいたいものです。そのため、有名ブランドのダイヤモンドジュエリーの場合は、普通の宝石専門買取店よりも、ブランド知識や人気の動向など情報が豊富なブランド買取店で査定してもらう方が高額査定になることが多いようです。<br>
						ダイヤモンドジュエリーの場合、ダイヤモンドの価値だけでなくブランド価値、人気なども含まれるため、ノーブランドのダイヤモンドジュエリーよりも高額な買取価格がつくことがほとんどです。そのため査定に出す時にはブランドから発行される鑑定書も必ず一緒に出すようにしましょう。ブランドによってダイヤモンドの品質はしっかり保証されていますが、より詳しい石の情報は鑑定書によって確認することが多くより適正な鑑定となるでしょう。</li>
				</ul>
			</div>
		</section>
	</main>
	<!-- #main --> 
</div>
<!-- #primary -->
<script src="//kaitorisatei.info/brandrevalue/wp-content/themes/bring/js/gaisan.js" type="text/javascript"></script>

<?php
get_sidebar();
get_footer();




