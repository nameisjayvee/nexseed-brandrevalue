<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  'watch/top/001.jpg::パテックフィリップ::カラトラバ 3923::646,000::640,000::650,000::10,000',
  'watch/top/002.jpg::パテックフィリップ::アクアノート 5065-1A::1,367,000::1,350,000::1,400,000::50,000',
  'watch/top/003.jpg::オーデマピゲ::ロイヤルオーク・オフショア 26170ST::1,200,000::1,195,000::1,210,000::15,000',
  'watch/top/004.jpg::ROLEX::サブマリーナ 116610LN ランダム品番::720,000::700,000::740,000::40,000',
  'watch/top/005.jpg::ROLEX::デイトナ 116505 ランダム品番 コスモグラフ::1,960,000::1,950,000::2,000,000::50,000',
  'watch/top/006.jpg::ブライトリング::クロノマット44::334,000::328,000::340,000::12,000',
  'watch/top/007.jpg::パネライ::ラジオミール 1940::594,000::590,000::600,000::10,000',
  'watch/top/008.jpg::ボールウォッチ::ストークマン ストームチェイサープロ CM3090C::128,000::125,000::130,000::5,000',
  'watch/top/009.jpg::ブレゲ::クラシックツインバレル 5907BB12984::615,000::610,000::626,000::16,000',
  'watch/top/010.jpg::ウブロ::ビッグバン ブラックマジック::720,000::695,000::740,000::45,000',
  'watch/top/011.jpg::オメガ::シーマスター プラネットオーシャン 2208-50::192,000::190,000::200,000::10,000',
  'watch/top/012.jpg::ディオール::シフルルージュ クロノグラフ CD084612 M001::190,000::180,000::200,000::20,000',
);

get_header(); ?>

    <div id="primary" class="cat-page content-area">
        <main id="main" class="site-main" role="main">
            <section id="mainVisual" style="background:url(<?php echo get_s3_template_directory_uri() ?>/img/shop/center_mv.png)">
                <h2 class="text-hide">買取センター</h2>
            </section>
            <section id="catchcopy">
                <h3>ブランド買取店をお探しのお客様へ。ブランド買取なら高価買取のBRAND REVALUE(ブランドリバリュー)へ。</h3>
                <p>最寄り駅は、「原宿駅」、「渋谷駅」、千代田線・副都心線「明治神宮前駅」。BRAND REVALUE買取センターは、完全予約制のプライベート買取センターとなります。「買取時に他の人の目が気になる」「落ち着いて査定を受けたい」というお客様の要望からプライベート買取ブースを設置しております。 BRAND REVALUE買取センターでは、お客様が快適に過ごして頂ける空間と最上級のサービスでおもてなし致します。<br>
                    <br>
                </p>
            </section>

            <section class="shop_gal">
                <h2 class="shop_ttl">ブランドリバリュー買取センター(完全予約制)店舗案内</h2>
                <!--<div class="link_gr ">
                    <a href="https://www.google.co.jp/maps/uv?hl=ja&pb=!1s0x60188ca8910581f1%3A0xd786dbaeae332c68!2m22!2m2!1i80!2i80!3m1!2i20!16m16!1b1!2m2!1m1!1e1!2m2!1m1!1e3!2m2!1m1!1e5!2m2!1m1!1e4!2m2!1m1!1e6!3m1!7e115!4shttps%3A%2F%2Flh5.googleusercontent.com%2Fp%2FAF1QipOmyveOoV7RqpKLtUPT8VNgqg4tmkfCVWi6zOUw%3Dw190-h192-n-k-no!5z44OW44Op44Oz44OJ44Oq44OQ44Oq44Ol44O8IOa4i-iwt-W6lyAtIEdvb2dsZSDmpJzntKI&imagekey=!1e10!2sAF1QipOc0zSWFVPL1GRGLSuTd_1ayv8rFcWzkYvkkJxC" target="_blank" class="hiragino">店舗内をみる<i class="fas fa-caret-right"></i></a>

                </div>-->
                <ul class="shop_main">
                    <li class="item1"><img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center/center01.png" alt="買取センター"></li>
                    <li class="item2"><img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center/center02.png" alt="買取センター"></li>
                    <li class="item3"><img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center/center03.png" alt="買取センター"></li>
                    <li class="item4"><img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center/center04.png" alt="買取センター"></li>
                </ul>
                <ul class="thumb">
                    <li class="thumb1"><img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center/center01.png" alt="買取センター"></li>
                    <li class="thumb2"><img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center/center02.png" alt="買取センター"></li>
                    <li class="thumb3"><img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center/center03.png" alt="買取センター"></li>
                    <li class="thumb4"><img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center/center04.png" alt="買取センター"></li>
                </ul>
            </section>

            <section class="shop_page ovf">
                <div class="map_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center/center_map.jpg" alt="買取センター"></div>
                <table class="info_table">
                    <tr>
                        <th>住所</th>
                        <td class="shop_map">東京都渋谷区神宮前6-23-3<br>第9SYビル5階</td>
                    </tr>
                    <tr>
                        <th>営業時間</th>
                        <td>11：00～21：00</td>
                    </tr>
                    <tr>
                        <th>電話番号</th>
                        <td><a href="tel:0120-970-060">0120-970-060</a></td>
                    </tr>
                    <tr>
                        <th>駐車場</th>
                        <td>近隣にコインパーキングあり</td>
                    </tr>
                </table>
                <div class="link_map ">
                    <a href="https://goo.gl/maps/FZZd38QRaPn" target="_blank" class="hiragino">MAPを見る<i class="fas fa-caret-right"></i></a>
                </div>
            </section>
            <section>
                <h3 class="mb30">買取センターまでのアクセス</h3>
                <ul class="map_root">
                    <li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_center01.png" alt="原宿駅 表参道口" />
                        <div class="root_tx_bx">
                            <p class="root_ttl"><span>1</span>原宿駅 表参道口</p>
                            <p>原宿駅から、ラフォーレ方面へ向かいます。交差点を右折し、明治通りへ出ます。</p>
                        </div>
                    </li>
                    <li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_center02.png" alt="A5出口" />
                        <div class="root_tx_bx">
                            <p class="root_ttl"><span>2</span>神宮前交差点を右折</p>
                            <p>右手に明治神宮前駅の7番出口があります。地下鉄をご利用の方は、この出口をご利用ください。</p>
                        </div>
                    </li>
                    <li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_center03.png" alt="明治通りを渋谷方向に直進" />
                        <div class="root_tx_bx">
                            <p class="root_ttl"><span>3</span>明治通りを渋谷方向に直進</p>
                            <p>5分ほど明治通りを直進していただき、Right-on奥の歩道橋まで進みます。</p>
                        </div>
                    </li>
                    <li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/root_center04.png" alt="第9SYビル(エントランス)" />
                        <div class="root_tx_bx">
                            <p class="root_ttl"><span>4</span>第9SYビル(エントランス)</p>
                            <p>１FにEDIFICE様がある第9SYビルの5階がブランドリバリュー買取センターです。</p>
                        </div>
                    </li>
                </ul>
            </section>
            <section class="shop_page">
                <h2 class="mb30">駐車場案内</h2>
                <p class="ant_ttl">NPC24H原宿第２パーキング</p>
                <table class="parking">
                    <tr>
                        <th>住所</th>
                        <td>東京都渋谷区神宮前6丁目25−14</td>
                    </tr>
                    <tr>
                        <th>営業時間<br>料金</th>
                        <td>24時間営業</td>
                    </tr>
                </table>
                <p class="ant_ttl">ポケットパーク 神宮前駐車場</p>
                <table class="parking">
                    <tr>
                        <th>住所</th>
                        <td>東京都渋谷区神宮前6丁目12−17</td>
                    </tr>
                    <tr>
                        <th>営業時間<br>料金</th>
                        <td>24時間営業</td>
                    </tr>

                </table>
                <p class="ant_ttl">京セラ原宿ビルコイン 駐車場</p>
                <table class="parking">
                    <tr>
                        <th>住所</th>
                        <td>東京都渋谷区神宮前6丁目27−8</td>
                    </tr>
                    <tr>
                        <th>営業時間<br>料金</th>
                        <td>24時間営業</td>
                    </tr>

                </table>
            </section>

            <div class="takuhai_cvbox tentou">
                <p class="cv_tl">ブランドリバリュー買取センターで<br>プライベート買取を予約する</p>
                <div class="custom_tel takuhai_tel">
                    <a href="tel:0120-970-060">
                        <div class="tel_wrap">
                            <div class="telbox01"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/customer/telico.png" alt="お申し込みはこちら">
                                <p>お電話からでもお申込み可能！<br>ご不明な点は、お気軽にお問合せ下さい。 </p>
                            </div>
                            <div class="telbox02"> <span class="small_tx">【受付時間】11:00 ~ 21:00</span><span class="ted_tx"> 年中無休</span>
                                <p>0120-970-060</p>
                            </div>
                        </div>
                    </a>
                </div>
                <p><a href="<?php echo home_url('purchase/visit-form'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/tentou_cv01.png" alt="店頭買取お申し込み"></a></p>
            </div>





            <section class="mb50">
                <h3 class="mb30">買取センター　店内紹介・内装へのこだわり</h3>
                <div class="shop_comm"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center_kodawari01.png" alt="店内" />
                    <p>ブランドリバリュー買取センターでは、お客さまをお待たせすることなくサービスを受けて頂きたく完全予約制とさせて頂いております。<br>プライベートな空間と最上級のサービスでおもてなし致します。</p>
                </div>
                <div class="shop_comm"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center_kodawari02.png" alt="店内" />
                    <p>各お客さまへ専属バイヤーが対応させて頂きます。経験豊富で信頼の置ける当店屈指の専属バイヤーは超高額品やレア商品など専門知識の必要なお品物も丁寧に査定させて頂きます。</p>
                </div>
                <div class="shop_comm"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/center_kodawari03.png" alt="店内" />
                    <p>お客さまが人目を気にせず安心して査定して頂けるように完全個室の査定ブースを設けさせて頂きました。持ち込み頂いたお品物についてのご質問やご相談・要望などありましたら遠慮なく専属バイヤーへお申し付け下さい。</p>
                </div>
            </section>
            <section>
                <h3>買取センター　鑑定士のご紹介</h3>
                <div class="staff_bx"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/staff01.png" alt="査定士 森" />
                    <p class="staff_name">森 雄大 鑑定士</p>
                    <h4>仕事における得意分野</h4>
                    <p>私の得意分野は、オールジャンルです。特に、買取店によって査定金額に差が出てしまう、エルメスやシャネル、ロレックス、宝石に関しては、頻繁に業者間オークションに参加し、知識向上に努めております。</p>
                    <h4>自身の強み</h4>
                    <p>私の強みは、高額査定にあります。長年、古物業界に携わることにより、独自の販売ルート　を構築して参りました。その為、査定金額には、自信があります。業界最高峰の査定金額でお客様にご満足いただけることをお約束いたします。 </p>
                    <h4>仕事にかける思いと心がけ</h4>
                    <p>お客様に「ありがとう」と笑顔で言っていただけることが、私の喜びです。お客様にご満足いただけるよう、高品質のサービスをご提供できるよう店舗運営・スタッフ教育に努めてまいります。</p>
                </div>
                <div class="staff_bx"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/staff02.png" alt="佐藤 昂太 鑑定士" />
                    <p class="staff_name">佐藤 昂太 鑑定士</p>
                    <h4>仕事における得意分野</h4>
                    <p>私の得意分野は、オールジャンルですが、特に相場の変動が激しいジュエリー関連と時計関連です。知識向上のため買取品だけでなく、経済や相場も含め日々勉強しております。</p>
                    <h4>自身の強み</h4>
                    <p>どんな良いブランドでも日々相場は変動しお買取り金額にも影響致します。日々変動するお買取り相場からお客様に損させることなく最高水準でのお買取り金額のご案内ができますよう弊社独自の販売ルートの確保をさせていただいております。</p>
                    <h4>仕事にかける思いと心がけ</h4>
                    <p>ご売却される理由は様々ございますが、お客様の大切なお品物を決して損させる事なくお買取りさせていただいております。どこよりも高くお買取りができるというのが私のできる最大のサービスであると考えております。 </p>
                </div>
            </section>
            <section id="top-jisseki">
                <h3>
                    買取センターの買取実績
                </h3>
                <div class="full_content">

                    <!-- ブランド買取 -->
                    <div class="menu hover"><span class="glyphicon glyphicon-tag"></span> ブランド買取</div>
                    <div class="content">
                        <!-- box -->
                        <ul id="box-jisseki" class="list-unstyled clearfix">
                            <!-- brand1 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/001.jpg" alt="">
                                    <p class="itemName">オーデマピゲ　ロイヤルオークオフショアクロノ 26470OR.OO.A002CR.01 ゴールド K18PG</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：3,120,000円<br>
                                        <span class="blue">B社</span>：3,100,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">3,150,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>50,000円</p>
                                </div>
                            </li>

                            <!-- brand2 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/002.jpg" alt="">
                                    <p class="itemName">パテックフィリップ　コンプリケーテッド ウォッチ 5085/1A-001</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：1,400,000円<br>
                                        <span class="blue">B社</span>：1,390,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">1,420,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>30,000円</p>
                                </div>
                            </li>

                            <!-- brand3 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/003.jpg" alt="">
                                    <p class="itemName">パテックフィリップ　コンプリケーション 5130G-001 WG</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：2,970,000円<br>
                                        <span class="blue">B社</span>：2,950,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">3,000,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>50,000円</p>
                                </div>
                            </li>

                            <!-- brand4 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/004.jpg" alt="">
                                    <p class="itemName">パテックフィリップ　ワールドタイム 5130R-001</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：3,000,000円<br>
                                        <span class="blue">B社</span>：2,980,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">3,050,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>70,000円</p>
                                </div>
                            </li>

                            <!-- brand5 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/005.jpg" alt="">
                                    <p class="itemName">シャネル　ラムスキン　マトラッセ　二つ折り長財布</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：69,000円<br>
                                        <span class="blue">B社</span>：68,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">70,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>2,000円</p>
                                </div>
                            </li>

                            <!-- brand6 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/006.jpg" alt="">
                                    <p class="itemName">エルメス　ベアンスフレ　ブラック</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：200,000円<br>
                                        <span class="blue">B社</span>：196,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">211,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>15,000円</p>
                                </div>
                            </li>

                            <!-- brand7 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/007.jpg" alt="">
                                    <p class="itemName">エルメス　バーキン30　トリヨンクレマンス　マラカイト　SV金具</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：1,220,000円<br>
                                        <span class="blue">B社</span>：1,200,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">1,240,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>40,000円</p>
                                </div>
                            </li>

                            <!-- brand8 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/008.jpg" alt="">
                                    <p class="itemName">セリーヌ　ラゲージマイクロショッパー</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：150,000円<br>
                                        <span class="blue">B社</span>：147,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">155,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>8,000円</p>
                                </div>
                            </li>

                            <!--brand9 -->

                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/009.jpg" alt="">
                                    <p class="itemName">ルイヴィトン　裏地ダミエ柄マッキントッシュジャケット</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：31,000円<br>
                                        <span class="blue">B社</span>：30,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">32,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>2,000円</p>
                                </div>
                            </li>

                            <!-- brand10 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/010.jpg" alt="">
                                    <p class="itemName">シャネル　ココマーク　ラパンファーマフラー</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：68,000円<br>
                                        <span class="blue">B社</span>：65,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">71,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>6,000円</p>
                                </div>
                            </li>

                            <!-- brand11 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/011.jpg" alt="">
                                    <p class="itemName">ルイヴィトン　ダミエ柄サイドジップレザーブーツ</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：49,000円<br>
                                        <span class="blue">B社</span>：48,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">51,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>3,000円</p>
                                </div>
                            </li>

                            <!-- brand12 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/012.jpg" alt="">
                                    <p class="itemName">サルバトーレフェラガモ　ヴェラリボンパンプス</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：9,000円<br>
                                        <span class="blue">B社</span>：8,500円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">10,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>1,500円</p>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <!-- 金買取 -->
                    <div class="menu"><span class="glyphicon glyphicon-bookmark"></span> 金買取</div>
                    <div class="content">
                        <!-- box -->
                        <ul id="box-jisseki" class="list-unstyled clearfix">
                            <!-- 1 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/001.jpg" alt="">
                                    <p class="itemName">K18　ダイヤ0.11ctリング</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：23,700円<br>
                                        <span class="blue">B社</span>：23,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">
                                        <!-- span class="small" >120g</span -->25,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>2,000円</p>
                                </div>
                            </li>
                            <!-- 2 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/002.jpg" alt="">
                                    <p class="itemName">K18　メレダイヤリング</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：38,000円<br>
                                        <span class="blue">B社</span>：37,500円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">
                                        <!-- span class="small" >120g</span -->39,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>1,500円</p>
                                </div>
                            </li>
                            <!-- 3 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/003.jpg" alt="">
                                    <p class="itemName">K18/Pt900　メレダイアリング</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：14,200円<br>
                                        <span class="blue">B社</span>：14,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">16,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>2,000円</p>
                                </div>
                            </li>
                            <!-- 4 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/004.jpg" alt="">
                                    <p class="itemName">Pt900　メレダイヤリング</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：22,600円<br>
                                        <span class="blue">B社</span>：21,200円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">23,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>1,800円</p>
                                </div>
                            </li>
                            <!-- 5 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/005.jpg" alt="">
                                    <p class="itemName">K18WG　テニスブレスレット</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：24,600円<br>
                                        <span class="blue">B社</span>：23,800円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">26,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>2,200円</p>
                                </div>
                            </li>
                            <!-- 6 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/006.jpg" alt="">
                                    <p class="itemName">K18/K18WG　メレダイヤリング</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：22,400円<br>
                                        <span class="blue">B社</span>：22,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">23,600<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>1,600円</p>
                                </div>
                            </li>
                            <!-- 7 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/007.jpg" alt="">
                                    <p class="itemName">K18ブレスレット</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：18,100円<br>
                                        <span class="blue">B社</span>：17,960円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">20,100<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>2,140円</p>
                                </div>
                            </li>
                            <!-- 8 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/008.jpg" alt="">
                                    <p class="itemName">K14WGブレスレット</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：23,600円<br>
                                        <span class="blue">B社</span>：22,800円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">24,300<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>1,500円</p>
                                </div>
                            </li>
                            <!-- 9 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/009.jpg" alt="">
                                    <p class="itemName">Pt850ブレスレット</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：23,600円<br>
                                        <span class="blue">B社</span>：23,200円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">24,700<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>1,500円</p>
                                </div>
                            </li>
                            <!-- 10 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/010.jpg" alt="">
                                    <p class="itemName">Pt900/Pt850メレダイヤネックレス</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：29,600円<br>
                                        <span class="blue">B社</span>：28,400円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">30,500<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>2,100円</p>
                                </div>
                            </li>
                            <!-- 11 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/011.jpg" alt="">
                                    <p class="itemName">K18/Pt900/K24ネックレストップ</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：43,600円<br>
                                        <span class="blue">B社</span>：43,100円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">45,900<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>2,800円</p>
                                </div>
                            </li>
                            <!-- 12 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/012.jpg" alt="">
                                    <p class="itemName">K24インゴットメレダイヤネックレストップ</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：53,700円<br>
                                        <span class="blue">B社</span>：52,900円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">56,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>3,100円</p>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <!-- 宝石買取 -->
                    <div class="menu"><span class="glyphicon glyphicon-magnet"></span> 宝石買取</div>
                    <div class="content">
                        <!-- box -->
                        <ul id="box-jisseki" class="list-unstyled clearfix">
                            <!--<li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem001.jpg" alt="">
            <p class="itemName">エメラルド</p>
            <hr>
            <p> <span class="red">A社</span>：190,000円<br>
              <span class="blue">B社</span>：194,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>200,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>7,500円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem002.jpg" alt="">
            <p class="itemName">サファイア</p>
            <hr>
            <p> <span class="red">A社</span>：145,500円<br>
              <span class="blue">B社</span>：142,500円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>150,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>10,000円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem003.jpg" alt="">
            <p class="itemName">ルビー</p>
            <hr>
            <p> <span class="red">A社</span>：97,000円<br>
              <span class="blue">B社</span>：95,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>100,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>5,000円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem004.jpg" alt="">
            <p class="itemName">アレキサンドライト</p>
            <hr>
            <p> <span class="red">A社</span>：67,900円<br>
              <span class="blue">B社</span>：66,500円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>70,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>4,500円</p>
          </div>
        </li>-->

                            <!-- 1 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/001.jpg" alt="">
                                    <p class="itemName">ダイヤルース</p>
                                    <p class="itemdetail">カラット：1.003ct<br> カラー：G
                                        <br> クラリティ：VS-2
                                        <br> カット：Good
                                        <br> 蛍光性：FAINT
                                        <br> 形状：ラウンドブリリアント
                                    </p>
                                    <hr>
                                    <p> <span class="red">A社</span>：215,000円<br>
                                        <span class="blue">B社</span>：210,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">
                                        <!-- span class="small">地金＋</span -->221,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>11,000円</p>
                                </div>
                            </li>

                            <!-- 2 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/002.jpg" alt="">
                                    <p class="itemName">Pt900 DR0.417ctリング</p>
                                    <p class="itemdetail"> カラー：F<br> クラリティ：SI-1
                                        <br> カット：VERY GOOD <br> 蛍光性：FAINT
                                        <br> 形状：ラウンドブリリアント
                                    </p>
                                    <hr>
                                    <p> <span class="red">A社</span>：42,000円<br>
                                        <span class="blue">B社</span>：41,300円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">44,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>2,700円</p>
                                </div>
                            </li>

                            <!-- 3 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/003.jpg" alt="">
                                    <p class="itemName">Pt900　DR0.25ctリング</p>
                                    <p class="itemdetail"> カラー：H<br> クラリティ:VS-1
                                        <br> カット：Good
                                        </br>
                                        蛍光性：MB<br> 形状：ラウンドブリリアント
                                        <br>

                                        <hr>
                                        <p> <span class="red">A社</span>：67,400円<br>
                                            <span class="blue">B社</span>：67,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">68,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>1,000円</p>
                                </div>
                            </li>

                            <!-- 4 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/004.jpg" alt="">
                                    <p class="itemName">K18　DR0.43ct　MD0.4ctネックレストップ</p>
                                    <p class="itemdetail"> カラー：I<br> クラリティ：VS-2
                                        <br> カット：Good
                                        <br> 蛍光性：WB
                                        <br> 形状：ラウンドブリリアント
                                    </p>
                                    <hr>
                                    <p> <span class="red">A社</span>：50,000円<br>
                                        <span class="blue">B社</span>：49,500円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">51,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>1,500円</p>
                                </div>
                            </li>

                            <!-- 5 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/005.jpg" alt="">
                                    <p class="itemName">ダイヤルース</p>
                                    <p class="itemdetail">カラット：0.787ct<br> カラー：E
                                        <br> クラアリティ：VVS-2
                                        <br> カット：Good
                                        <br> 蛍光性：FAINT
                                        <br> 形状：ラウンドブリリアント
                                    </p>
                                    <hr>
                                    <p> <span class="red">A社</span>：250,000円<br>
                                        <span class="blue">B社</span>：248,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">257,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>9,000円</p>
                                </div>
                            </li>

                            <!-- 6 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/006.jpg" alt="">
                                    <p class="itemName">Pt950　MD0.326ct　0.203ct　0.150ctネックレス</p>
                                    <p class="itemdetail"> カラー：F<br> クラリティ：SI-2
                                        <br> カット：Good
                                        <br> 蛍光性：FAINT
                                        <br> 形状：ラウンドブリリアント
                                    </p>
                                    <hr>
                                    <p> <span class="red">A社</span>：55,000円<br>
                                        <span class="blue">B社</span>：54,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">57,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>3,000円</p>
                                </div>
                            </li>

                            <!-- 7 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/007.jpg" alt="">
                                    <p class="itemName">ダイヤルース</p>
                                    <p class="itemdetail">カラット：1.199ct<br> カラー：K
                                        <br> クラリティ：SI-2
                                        <br> カット：評価無
                                        <br> 蛍光性：NONE
                                        <br> 形状：パビリオン
                                        <br>
                                    </p>
                                    <hr>
                                    <p> <span class="red">A社</span>：58,000円<br>
                                        <span class="blue">B社</span>：56,800円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">60,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>3,200円</p>
                                </div>
                            </li>

                            <!-- 8 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/008.jpg" alt="">
                                    <p class="itemName">ダイヤルース</p>
                                    <p class="itemdetail">カラット：8.1957ct<br> カラー：LIGTH YELLOW<br> クラリティ：VS-1
                                        <br> カット：VERY GOOD<br> 蛍光性：FAINT
                                        <br> 形状：ラウンドブリリアント
                                    </p>
                                    <hr>
                                    <p> <span class="red">A社</span>：6,540,000円<br>
                                        <span class="blue">B社</span>：6,500,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">6,680,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>180,000円</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="menu"><span class="glyphicon glyphicon-time"></span> 時計買取</div>
                    <div class="content">
                        <ul id="box-jisseki" class="list-unstyled clearfix">
                            <?php
                    foreach($resultLists as $list):
                    // :: で分割
                    $listItem = explode('::', $list);
                  
                  ?>
                                <li class="box-4">
                                    <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/<?php echo $listItem[0]; ?>" alt="">
                                        <p class="itemName">
                                            <?php echo $listItem[1]; ?><br>
                                            <?php echo $listItem[2]; ?>
                                        </p>
                                        <hr>
                                        <p> <span class="red">A社</span>：
                                            <?php echo $listItem[3]; ?>円<br>
                                            <span class="blue">B社</span>：
                                            <?php echo $listItem[4]; ?>円 </p>
                                    </div>
                                    <div class="box-jisseki-cat">
                                        <h3>買取価格例</h3>
                                        <p class="price">
                                            <?php echo $listItem[5]; ?><span class="small">円</span></p>
                                    </div>
                                    <div class="sagaku">
                                        <p><span class="small">買取差額“最大”</span>
                                            <?php echo $listItem[6]; ?>円</p>
                                    </div>
                                </li>
                                <?php endforeach; ?>
                        </ul>
                    </div>

                    <!-- バッグ買取 -->
                    <div class="menu"><span class="glyphicon glyphicon-briefcase"></span> バッグ買取</div>
                    <div class="content">
                        <ul id="box-jisseki" class="list-unstyled clearfix">
                            <!-- 1 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/001.jpg" alt="">
                                    <p class="itemName">エルメス　エブリンⅢ　トリヨンクレマンス</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：176,000円<br>
                                        <span class="blue">B社</span>：172,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">180,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>8,000円</p>
                                </div>
                            </li>
                            <!-- 2 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/002.jpg" alt="">
                                    <p class="itemName">プラダ　シティトート2WAYバッグ</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：128,000円<br>
                                        <span class="blue">B社</span>：125,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">132,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>7,000円</p>
                                </div>
                            </li>
                            <!-- 3 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/003.jpg" alt="">
                                    <p class="itemName">バンブーデイリー2WAYバッグ</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：83,000円<br>
                                        <span class="blue">B社</span>：82,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">85,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>3,000円</p>
                                </div>
                            </li>
                            <!-- 4 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/004.jpg" alt="">
                                    <p class="itemName">ルイヴィトン　モノグラムモンスリGM</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：64,000円<br>
                                        <span class="blue">B社</span>：63,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">66,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>3,000円</p>
                                </div>
                            </li>
                            <!-- 5 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/005.jpg" alt="">
                                    <p class="itemName">エルメス　バーキン30</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：1,150,000円<br>
                                        <span class="blue">B社</span>：1,130,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">1,200,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>70,000円</p>
                                </div>
                            </li>
                            <!-- 6 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/006.jpg" alt="">
                                    <p class="itemName">シャネル　マトラッセダブルフラップダブルチェーンショルダーバッグ</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：252,000円<br>
                                        <span class="blue">B社</span>：248,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">260,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>12,000円</p>
                                </div>
                            </li>
                            <!-- 7 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/007.jpg" alt="">
                                    <p class="itemName">ルイヴィトン　ダミエネヴァーフルMM</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：96,000円<br>
                                        <span class="blue">B社</span>：94,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">98,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>4,000円</p>
                                </div>
                            </li>
                            <!-- 8 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/008.jpg" alt="">
                                    <p class="itemName">セリーヌ　ラゲージマイクロショッパー</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：150,000円<br>
                                        <span class="blue">B社</span>：148,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">155,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>7,000円</p>
                                </div>
                            </li>
                            <!-- 9 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/009.jpg" alt="">
                                    <p class="itemName">ロエベ　アマソナ23</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：86,000円<br>
                                        <span class="blue">B社</span>：82,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">90,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>8,000円</p>
                                </div>
                            </li>
                            <!-- 10 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/010.jpg" alt="">
                                    <p class="itemName">グッチ　グッチシマ　ビジネスバッグ</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：50,000円<br>
                                        <span class="blue">B社</span>：48,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">53,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>5,000円</p>
                                </div>
                            </li>
                            <!-- 11 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/011.jpg" alt="">
                                    <p class="itemName">プラダ　サフィアーノ2WAYショルダーバッグ</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：115,000円<br>
                                        <span class="blue">B社</span>：110,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">125,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>15,000円</p>
                                </div>
                            </li>
                            <!-- 12 -->
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/012.jpg" alt="">
                                    <p class="itemName">ボッテガヴェネタ　カバMM　ポーチ付き</p>
                                    <hr>
                                    <p> <span class="red">A社</span>：150,000円<br>
                                        <span class="blue">B社</span>：146,000円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">155,000<span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>9,000円</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="kaitori_voice clearfix shop_page mb50">
                <h3 class="mb30">買取センターのお客様の声</h3>
                <ul>
                    <li>
                        
                        <h4>大満足です。</h4>
                        <p class="voice_txt">昔に購入したリングやネックレスなどの査定に、ブランドリバリューさんの買取センターを利用しました。なぜ買取センターにしたかというと、大勢いらっしゃるお買い物のお客さんと顔を合わせたくなかったから。落ち着いた雰囲気のブースでじっくりと査定してほしかったからです。
「査定士さんが怖い人だったらどうしよう?」そんな風にも思っていたのですが、対応してくれた方の人柄もよく安心。十分な広さのブースは密室感もなかったので、リラックスして査定してもらうこともできました。
そして出された査定結果は、こちらの期待を良い意味で大きく裏切るもの!!　金額に納得できなければ持ち帰る気満々だったのですが、デザインも考慮してくれたことが良い結果につながったようです。

                        </p>
                    </li>
                    <li>
                       
                        <h4>安心して利用することができました。</h4>
                        <p class="voice_txt">ロレックスやエルメス、シャネルなど……。ブランド好きだった親戚が亡くなったので、全てを現金にする必要があり、ブランドリバリューさんにお願いしました。理由は時計をはじめとして高価なものを持ち込むだけに、人目に触れたくなかったから。
その点、ブランドリバリューさんの買取センターは完全予約制だと聞いていましたし、査定専用のブースも完備。プライベートが守られますから、安心して利用することができました。
そして、査定士さんと買取り価格について、じっくりとつめられるのもよかった点。他のお客さんを待たせているのが気になって、落ち着かないまま査定が進められることもなく、納得できる買取価格を出してもらうことができました。
叔母の遺品、次の方も大切に使っていただけることを願っています。

                        </p>
                    </li>
                    
                </ul>
            </section>
            <section>
                <h3 class="mb30">買取センター　立地へのこだわり</h3>
                <p class="line_h16">BRAND REVALUE買取センターでは、最上級のサービスでおもてなしをするために「プライベートコンシェルジュ」サービスを導入しております。お客様一人ひとりに専属バイヤーがつきます。豊富な経験と確かな実績がある専属バイヤーは、超高額品やレア商品まで幅広く対応可能です。お客様が大切にしてきたお品物を最適な方法でご売却できるよう様々なご提案をさせていただきます。<br><br>お昼休みやちょっとした空き時間やお仕事帰り、渋谷・原宿でのショッピングついでなどでも、ブランド品、時計、宝石、貴金属などの買取が可能ですので、どうぞお気軽にお立ち寄りください。売りたい商品の現物をお持ちいただく前に、お電話や店頭にて仮査定にておおよそのお値段をお伝えする仮査定も可能です。 また、来店買取だけではなく、宅配買取、出張買取などお客様のニーズに応えるべく様々な買取サービスをご提供致します。長年この業界でブランド品、時計、宝石、貴金属などの買取に携わってきたプロフェッショナルであるバイヤーが、よりお客様にご満足いただけるよう、お客様の気持ちに親身になり他店の査定額より上回る高価査定にてご要望にお答え致します。 ブランド品買取ならBRAND REVALUE買取センターへ。 お客さま満足度No1のブランド品買取店を目指し、精進して参ります。買取センタースタッフ一同、皆様のご来店心よりお待ち申し上げております。 ブランド品買取なら高価買取のBRAND REVALUE(ブランドリバリュー)へ。
                </p>

            </section>
            <div class="takuhai_cvbox tentou">
                <p class="cv_tl">ブランドリバリュー買取センターで<br>プライベート買取を予約する</p>
                <div class="custom_tel takuhai_tel">
                    <a href="tel:0120-970-060">
                        <div class="tel_wrap">
                            <div class="telbox01"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/customer/telico.png" alt="お申し込みはこちら">
                                <p>お電話からでもお申込み可能！<br>ご不明な点は、お気軽にお問合せ下さい。 </p>
                            </div>
                            <div class="telbox02"> <span class="small_tx">【受付時間】11:00 ~ 21:00</span><span class="ted_tx"> 年中無休</span>
                                <p>0120-970-060</p>
                            </div>
                        </div>
                    </a>
                </div>
                <p><a href="<?php echo home_url('purchase/visit-form'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/tentou_cv01.png" alt="店頭買取お申し込み"></a></p>
            </div>
            <?php
        // アクションポイント
        get_template_part('_action');
        
        // 買取方法
        get_template_part('_purchase');
      ?>
        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->

    <?php
get_sidebar();
get_footer();
