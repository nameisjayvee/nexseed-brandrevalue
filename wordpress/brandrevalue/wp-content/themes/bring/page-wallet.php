<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  '001.jpg::CHANEL（シャネル）::キャビアスキン　マトラッセチェーンウォレット::100,000::96,000::105,000::9,000',
  '002.jpg::LOUIS VUITTON（ルイヴィトン）::モノグラム　ポルトフォイユクレマンス::45,000::38,000::48,000::10,000',
  '003.jpg::HERMES（エルメス）::ドゴンＧＭ　T刻印::125,000::115,000::150,000::35,000',
  '004.jpg::PRADA（プラダ）::サフィアーノ　長財布::32,000::28,000::36,000::8,000',
  '005.jpg::BOTTEGA VENETA（ボッテガヴェネタ）::ヴェネタ　イントレチャートラウンドファスナー長財布::42,000::39,500::44,000::4,500',
  '006.jpg::BOTTEGA VENETA（ボッテガヴェネタ）::ヴェネタ　イントレチャート長財布::38,000::35,000::40,000::5,000',
  '007.jpg::LOUIS VUITTON（ルイヴィトン）::モノグラム　ポルトフォイユインターナショナル::25,000::10,000::30,000::20,000',
  '008.jpg::HERMES（エルメス）::ベアンスフレ　T刻印::140,000::125,000::170,000::45,000',
  '009.jpg::LOUIS VUITTON（ルイヴィトン）::ダミエ　ジッピーウォレット::63,000::58,000::70,000::12,000',
  '010.jpg::GUCCI（グッチ）::ダブルG　長財布::32,000::30,000::35,000::5,000',
  '011.jpg::CHANEL（シャネル）::キャビアスキン　マトラッセラウンドファスナー長財布::90,000::87,000::95,000::8,000',
  '012.jpg::LOUIS VUITTON（ルイヴィトン）::モノグラム　ポルトフォイユサラ::58,000::54,000::60,000::6,000',
);


get_header(); ?>

<p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
<p class="main_bottom">満足度No1宣言！ブランド財布買取のブランドリバリューへ！</p>
<div id="primary" class="cat-page content-area">
<main id="main" class="site-main" role="main">
<div id="lp_head" class="wallet_ttl">
<div>
<p>銀座で最高水準の査定価格・サービス品質をご体験ください。</p>
<h2>あなたの高級ブランド財布<br />どんな物でもお売り下さい！！</h2>
</div>
</div>

      <p id="catchcopy">BRAND REVALUEはまだ新しい買取ショップですが、財布買取に精通したスタッフがそろっており再販ルートも確立しております。<br>
      お客様の大切な財布の価値を見逃すことなく高く査定させていただいていることから、オープン後財布の買取実績も急速に伸びております。また、BRANDREVALUE(ブランドリバリュー)は銀座の中心地という利便性の高い地域に店舗を構えていますが、路面店と比べて格段に家賃の安い空中階に店舗があるため、他店と比べて圧倒的に経費を安く抑えることができています。<br>
      <br>
      その他、広告費、人件費等も可能な限り削減しており、その分査定額を高めに設定してお客様に利益を還元しております。</p>
      
      <section id="hikaku" class="wallet_hikaku">
        <h3 class="text-hide">他社の買取額と比較すると</h3>
        <p class="hikakuName">CHANEL<br>マトラッセ 長財布</p>
        <p class="hikakuText">※状態が良い場合や当店で品薄の場合などは<br>
        　特に高価買取致します。</p>
        <p class="hikakuPrice1"><span class="red">A社</span>：60,000円</p>
        <p class="hikakuPrice2"><span class="blue">B社</span>：80,000円</p>
        <p class="hikakuPrice3">100,000<span class="small">円</span></p>
      </section>
      <section class="kaitori_cat">
          <ul>
              <li>
                  <a href="https://kaitorisatei.info/brandrevalue/blog/doburock"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/12/caf61dc6779ec6137c0ab58dfe3a550d.jpg" alt="どぶろっく"></a>
              </li>
          </ul>
      </section>      
      
      <section id="cat-point">
        <h3>高価買取のポイント</h3>
        <ul class="list-unstyled">
                    <li>
                        <p class="pt_tl">商品情報が明確だと査定がスムーズ</p>
                        <p class="pt_tx">ブランド名、モデル名が明確だと査定がスピーディに出来、買取価格にもプラスに働きます。また、新作や人気モデル、人気ブランドであれば買取価格がプラスになります。</p>
                    </li>
                    <li>
                        <p class="pt_tl">数点まとめての査定だと
                            <br>キャンペーンで高価買取が可能</p>
                        <p class="pt_tx">数点まとめての査定依頼ですと、買取価格をプラスさせていただきます。</p>
                    </li>
                    <li>
                        <p class="pt_tl">品物の状態がよいほど
                            <br>高価買取が可能</p>
                        <p class="pt_tx">お品物の状態が良ければ良いほど、買取価格もプラスになります。</p>
                    </li>
                </ul>
        




        <p>財布の査定額は、ちょっとした工夫で高くなることがあるのをご存知でしょうか。<br>
				それは「ご自宅で簡単にメンテナンスをしてから査定に出す」というもの。モノ自体がよくても、埃がたまっているなど汚れが目立つ財布は査定額が下がってしまう場合があるのです。ご自宅でできる範囲で表面の汚れをふき取り、カード入れや小銭入れ等の小さなポケットにたまった埃をとるだけでも評価額が上がるので、ぜひお試しください。<br>その他、購入時の箱や袋、保証書等の付属品が一式そろっていると、査定金額が上がります。</p>
      </section>
      
      <section id="lp-cat-jisseki">
        <h3 class="text-hide">買取実績</h3>
            
        <ul id="box-jisseki" class="list-unstyled clearfix">
          <?php
            foreach($resultLists as $list):
            // :: で分割
            $listItem = explode('::', $list);
          
          ?>
          <li class="box-4">
            <div class="title">
              <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/wallet/lp/<?php echo $listItem[0]; ?>" alt="">
              <p class="itemName"><?php echo $listItem[1]; ?><br><?php echo $listItem[2]; ?></p>
              <hr>
              <p>
                <span class="red">A社</span>：<?php echo $listItem[3]; ?>円<br>
                <span class="blue">B社</span>：<?php echo $listItem[4]; ?>円
              </p>
            </div>
            <div class="box-jisseki-cat">
              <h3>買取価格例</h3>
              <p class="price"><?php echo $listItem[5]; ?><span class="small">円</span></p>
            </div>
            <div class="sagaku">
              <p><span class="small">買取差額“最大”</span><?php echo $listItem[6]; ?>円</p>
            </div>
          </li>
          <?php endforeach; ?>
        </ul>
        </section>
<section id="list-brand" class="clearfix">
<h3>ブランドリスト</h3>
			<ul class="list-unstyled">
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/hermes'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch33.jpg" alt="エルメス"></dd>
                                <dt><span>Hermes</span>エルメス</dt>
                            </a>
                        </dl>
                    </li>
					<li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/celine'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch29.jpg" alt="セリーヌ"></dd>
                                <dt><span>CELINE</span>セリーヌ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/wallet/louisvuitton'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch23.jpg" alt="ルイヴィトン"></dd>
                                <dt><span>LOUIS VUITTON</span>ルイヴィトン</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/chanel'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag01.jpg" alt="シャネル"></dd>
                                <dt><span>CHANEL</span>シャネル</dt>
                            </a>
                        </dl>
                    </li>					
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/gucci'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch25.jpg" alt="グッチ"></dd>
                                <dt><span>GUCCI</span>グッチ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/prada'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag02.jpg" alt="プラダ"></dd>
                                <dt><span>PRADA</span>プラダ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/fendi'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag03.jpg" alt="プラダ"></dd>
                                <dt><span>FENDI</span>フェンディ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/dior'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag04.jpg" alt="ディオール"></dd>
                                <dt><span>Dior</span>ディオール</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
						<a href="<?php echo home_url('/cat/bag/saint_laurent'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag05.jpg" alt="サンローラン"></dd>
                                <dt><span>Saint Laurent</span>サンローラン</dt>
                            </a>
                        </dl>
                    </li>
					<li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/bottegaveneta'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch32.jpg" alt="ボッテガヴェネタ"></dd>
                                <dt><span>Bottegaveneta</span>ボッテガヴェネタ</dt>
                            </a>
                        </dl>
                    </li>
					<li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/ferragamo'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch31.jpg" alt="フェラガモ"></dd>
                                <dt><span>Ferragamo</span>フェラガモ</dt>
                            </a>
                        </dl>
                    </li>					
			</ul>
			</section>

<section id="lp-cat-jisseki" class="clearfix">
        <div id="konna" class="konna_wallet">
        	<p class="example1">■汚れ</p>
          <p class="example2">■型くずれ</p>
          <p class="example3">■角スレ</p>
          <p class="text">その他：変色等されている状態でもお買取りいたします。</p>
        </div>
      </section>
      
      <section id="about_kaitori" class="clearfix">
      	<?php
        // 買取について
        get_template_part('_widerange');
				get_template_part('_widerange2');
      ?>
      </section>
      
      
      
      <section>
          <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/bn_matomegai.png">
      </section>
      
      <?php
        // 買取基準
        get_template_part('_criterion');

        // NGアイテム
        get_template_part('_no_criterion');
        
				// カテゴリーリンク
        get_template_part('_category');
			?>
      <section id="kaitori_genre">
        <h3 class="text-hide">その他買取可能なジャンル</h3>
        <p>【買取ジャンル】バッグ/ウエストポーチ/セカンドバック/トートバッグ/ビジネスバッグ/ボストンバッグ/クラッチバッグ/トランクケース/ショルダーバッグ/ポーチ/財布/カードケース/パスケース/キーケース/手帳/腕時計/ミュール/サンダル/ビジネスシューズ/パンプス/ブーツ/ペアリング/リング/ネックレス/ペンダント/ピアス/イアリング/ブローチ/ブレスレット/ライター/手袋/傘/ベルト/ペン/リストバンド/アンクレット/アクセサリー/サングラス/帽子/マフラー/ハンカチ/ネクタイ/ストール/スカーフ/バングル/カットソー/アンサンブル/ジャケット/コート/ブルゾン/ワンピース/ニット/シャツメンズ/毛皮/Tシャツ/キャミソール/タンクトップ/パーカー/ベスト/ポロシャツ/ジーンズ/スカート/スーツなど</p>
      </section>
      
      <?php
        // 買取方法
        get_template_part('_purchase');
      ?>
      
      <section id="user_voice">
      	<h3>ご利用いただいたお客様の声</h3>
        
        <p class="user_voice_text1">ちょうど家の整理をしていたところ、家のポストにチラシが入っていたので、ブランドリバリューに電話してみました。今まで買取店を利用したことがなく、不安でしたがとても丁寧な電話の対応とブランドリバリューの豊富な取り扱い品目を魅力に感じ、出張買取を依頼することにしました。
絶対に売れないだろうと思った、動かない時計や古くて痛んだバッグ、壊れてしまった貴金属のアクセサリーなども高額で買い取っていただいて、とても満足しています。古紙幣や絵画、食器なども買い取っていただけるとのことでしたので、また家を整理するときにまとめて見てもらおうと思います。</p>

			<h4>鑑定士からのコメント</h4>
      <div class="clearfix">
      	<p class="user_voice_text2">家の整理をしているが、何を買い取ってもらえるか分からないから一回見に来て欲しいとのことでご連絡いただきました。
買取店が初めてで不安だったが、丁寧な対応に非常に安心しましたと笑顔でおっしゃって頂いたことを嬉しく思います。
物置にずっとしまっていた時計や、古いカバン、壊れてしまったアクセサリーなどもしっかりと価値を見極めて高額で提示させて頂きましたところ、お客様もこんなに高く買取してもらえるのかと驚いておりました。
これからも家の不用品を整理するときや物の価値判断ができないときは、すぐにお電話しますとおっしゃって頂きました。</p>
			
      	<img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/photo-user-voice.jpg" class="right">
      </div>
      </section>

<section>
  <div class="cont_widerange">
              <h4>ブランド財布買取での取扱いブランドとは？<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
              <ul>
                  <li>あなたは、今どんな財布を使っていますか？財布は毎日使用する身近なレザーアイテムなので、丈夫さや実用性を重視して財布を選ぶ人ももちろん多いですが、ほとんどの人がお気に入りのブランド財布を使っているのではないでしょうか。さらに最近では、財布を一年毎に新調するというこだわりを持つ人も多くなってきたので、ブランド財布の回転がとても早くなっているのです。当然ブランド財布は直営ブランドブティックでも、中古市場でも、代表的な人気レザーアイテムとなっています。<br>
「この財布、そろそろ買い替えようかな」と思ってはいてもブランド財布は品質も高いので見た目はまだまだきれいな状態の美品が多いため、簡単に処分はしたくないものです。特に人気の高いブランド財布であれば、なおさら「どこかブランド買取店で高く買取ってもらえれば…」と誰もが思うものでしょう。そうなると、買取店ではとんなブランドだったら買取取扱いブランドとして査定しもらえるのか気になってきますよね。せっかく買取ってもらおうと買取店に持ち込んだものの、買取取扱いブランドではないからと査定してもらえなかったり、ノーブランドの財布のように安く買取られるのも良い気分はしません。<br>
ブランド財布の買取で一般的に取扱ブランドとなっているのは、レザーブランドとして人気の高いエルメスやルイヴィトン、グッチ、ボッテガヴェネタ、コーチをはじめ、シャネル、プラダ、カルティエ、ブルガリ、クロムハーツなど女性男性を問わず人気の有名ブランドがずらりと並びます。その他にも取扱いブランドは買取店によっても異なってくるので、あまりメジャーではないブランドの場合は査定を依頼しようと思う買取店に事前に直接問い合わせてみるようにしましょう。<br>
ブランド財布は財布そのものの価値を査定してもらえますがブランドネームによる価値も査定に大きく影響してきます。そのため、できるだけ取扱いブランドが豊富な買取店で、ブランド財布として査定買取をしてもらいましょう。<br><br>
</li>
              </ul>
          </div>
  <div class="cont_widerange">
              <h4>高額査定になるブランド財布とは？<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
              <ul>
                  <li>財布は一番身近で使用頻度の高いアイテムだからこそ、誰でも自分の好みに一番しっくりくる財布を選びたいものです。しかし、時にはプレゼントで贈られた財布がどうしても自分に合わないテイストだった場合は、どう考えても使うことはありませんよね。そんな時は、新品の状態のままで買取店に持ち込んで買取ってもらう人も増えてきました。このような購入してから間もない未使用の財布が、人気のブランド財布だった場合は高額査定になることも多いようです。<br>
もちろん、先にご紹介したようなプレゼントでもらった財布ばかりが買取店に持ち込まれる訳ではなく、長く愛用してきたブランド財布を査定に出すケースの方が多いかもしれません。手元にあるブランド財布を買取店で買取ってもらうのなら、できれば少しでも高額査定をしてもらいたいですよね。そこでブランド財布が少しでも高額査定になるポイントをご紹介しておきましょう。<br>
まずブランド財布買取の際の査定ではどのようなポイントが査定基準となるかご存知でしょうか？ほとんどの場合、その財布本体の状態と中古市場での人気や需要の高さが査定価格の基準となるようです。<br>
最も気になるブランド財布の状態ですが、まず各買取店が定める買取可能な状態であることが最低条件とはなります。最近では型崩れや角擦れ、多少の汚れがあるものでも買取対象になるものが多いようです。ただし美品になればなるほど高額査定となるため、先にご紹介したような未使用の人気ブランド財布の場合はかなりの高額査定額が見込めるはずです。一方、使用感のあるブランド財布であっても査定前にメンテナンスをしておく、汚れをきれいにふき取っておく、財布の内側や小銭入れにたまっているホコリを取り除いておくだけでも査定額が上がる場合があります。<br>
また人気の高いブランド財布の場合は、中古市場でも需要が高く、すぐに再販の目途が立つため高額査定になる場合が多いようです。また買取ったブランド財布を再販できる独自の販売ルートを持つ買取店で査定してもらうと、諸経費がかからない分高額査定がとなるケースが多いようです。<br><br>
</li>
              </ul>
          </div>
  <div class="cont_widerange">
              <h4>中古のブランド財布でも人気ブランドとは？<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
              <ul>
                  <li>何気にバッグから取り出した財布を見て、一緒にいた人から「あのブランドの財布使ってるんだね」と言われると悪い気はしないものですよね。財布は一番よく使うレザーアイテムであると同時に、周囲の人から最もチェックされているアイテムのひとつなのです。だからこそ、選ぶ財布は使いやすさよりもブランドにこだわる人が多いのかもしれません。<br>
ブランド財布は欲しいけれど、完売になっているモデルやすでに生産中止になっているコレクションの財布、定価が高額過ぎて直営ブランドブティックではなかなか手が届かないブランド財布だとする時はよくあります。でも中古市場なら探していたモデルやコレクションのブランド財布が見つかったり、なかなか定価では手が届かなかった高級ブランド財布も中古でもまだきれいな状態のものを定価より抑えられた価格で手に入れることができかもしれません。そのため中古市場で流通している人気のブランド財布をチェックしている人は意外にも多いのです。<br>
一方、ブランド財布を売却したい人にとっても、いま買取ってもらいたいブランド財布が人気のブランドや人気の高いブランド財布であれば高額査定が付きやすいので、中古市場で人気のあるブランド財布の情報はやはり気になるところでしょう。<br>
ブランド財布として中古市場でも絶大な人気を誇るブランドは、何と言ってもルイヴィトンでしょう。特に、流行や新作コレクションの人気に左右されず、常に安定した人気を誇るモノグラムやダミエのジップアラウンドの長財布は中古市場でも絶対的な人気となっています。見た目にも傷がつきにくく、使用される革も丈夫なので、中古のブランド財布として購入しても全く違和感なく使用できることも大きな魅力となっています。またメンズのブランド財布として人気の高いのは、ボッテガヴェネタのイントレチャートのラウンドジップの長財布や二つ折り長財布です。使用感が付きにくく良い状態の財布として中古市場でも再販されやすく回転の早い人気ブランドとなっています。
</li>
              </ul>
          </div>
</section>
      
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
