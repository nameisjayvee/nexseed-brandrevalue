<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>

	<div id="primary" class="introduction content-area">
		<main id="main" class="site-main" role="main">
      <section id="mainVisual" style="background:url(<?php echo get_s3_template_directory_uri() ?>/img/mv/introduction.png)">
        <h2 class="text-hide">買取実績</h2>
      </section>
      
      <section id="top-jisseki">
        <h2 class="text-hide">買取実績</h2>
        <h3>ブランド買取</h3>
            <ul id="box-jisseki" class="list-unstyled clearfix">
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/item001.png" alt="">
                  <p class="itemName">ロイヤルオーク オフショア</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：2,630,000円<br>
                        <span class="blue">B社</span>：2,750,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">2,900,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>270,000円</p>
                </div>
              </li>
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/item002.png" alt="">
                  <p class="itemName">コンプリケーテッド</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：4,559,000円<br>
                        <span class="blue">B社</span>：4,465,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">4,700,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>235,000円</p>
                </div>
              </li>
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/item003.png" alt="">
                  <p class="itemName">コンプリケーション</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：3,977,000円<br>
                        <span class="blue">B社</span>：3,895,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">4,100,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>205,000円</p>
                </div>
              </li>
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand001.jpg" alt="">
                  <p class="itemName">パテックフィリップ　ワールドタイム　5130P-001</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：3,700,000円<br>
                        <span class="blue">B社</span>：3,650,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">3,900,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>250,000円</p>
                </div>
              </li>
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/item004.png" alt="">
                  <p class="itemName">マトラッセ 長財布</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：97,000円<br>
                        <span class="blue">B社</span>：95,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">100,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>5,000円</p>
                </div>
              </li>
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/item005.png" alt="">
                  <p class="itemName">ベアンスフレ</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：223,100円<br>
                        <span class="blue">B社</span>：218,500円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">230,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>11,500円</p>
                </div>
              </li>
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/item006.png" alt="">
                  <p class="itemName">HERMES バーキン30</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：1,450,000円<br>
                        <span class="blue">B社</span>：1,520,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">1,600,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>150,000円</p>
                </div>
              </li>
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/item007.png" alt="">
                  <p class="itemName">CELINE バッグ</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：1,805,000円<br>
                        <span class="blue">B社</span>：1,843,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">1,900,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>57,000円</p>
                </div>
              </li>
              
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand002.jpg" alt="">
                  <p class="itemName">グッチ　レザー　ライダースジャケット</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：70,000円<br>
                        <span class="blue">B社</span>：65,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">76,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>11,000円</p>
                </div>
              </li>
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand003.jpg" alt="">
                  <p class="itemName">グッチ  ウール×アンゴラ　ハーフコート</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：27,000円<br>
                        <span class="blue">B社</span>：23,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">30,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>7,000円</p>
                </div>
              </li>
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand004.jpg" alt="">
                  <p class="itemName">サンローラン　チェーンハーネスブーツ</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：77,500円<br>
                        <span class="blue">B社</span>：76,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">80,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>4,000円</p>
                </div>
              </li>
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/brand005.jpg" alt="">
                  <p class="itemName">フェラガモ　スタッズ　フラットシューズ</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：15,500円<br>
                        <span class="blue">B社</span>：15,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">16,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>1,000円</p>
                </div>
              </li>
            </ul>
        <h3><a href="http://kaitorisatei.info/brandrevalue/cat/gold" style="color:#ffffff; text-decoration:none;">金・プラチナ買取</a></h3>
            <ul id="box-jisseki" class="list-unstyled clearfix">
<!-- 1 -->
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold001.jpg" alt="">
                  <p class="itemName">K18</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：424,860円<br>
                        <span class="blue">B社</span>：416,100円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price"><span class="small">120g</span> 438,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>219,000円</p>
                </div>
              </li>
<!-- 2 -->      
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold002.jpg" alt="">
                  <p class="itemName">Pt900</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：308,848円<br>
                        <span class="blue">B社</span>：302,480円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price"><span class="small">80g</span> 318,400<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>15,920円</p>
                </div>
              </li>
<!-- 3 -->      
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold003.jpg" alt="">
                  <p class="itemName">ダイヤリング　Pt900　8.0g</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：247,500円<br>
                        <span class="blue">B社</span>：242,500円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">250,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>7,500円</p>
                </div>
              </li>
<!-- 4 -->      
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold004.jpg" alt="">
                  <p class="itemName">メイプルリーフ金貨ダイヤネックレストップ　K18×K24 11.8g</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：49,500円<br>
                        <span class="blue">B社</span>：48,500円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">50,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>1,500円</p>
                </div>
              </li>
<!-- 5 -->
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold005.jpg" alt="">
                  <p class="itemName">K22コイン色石付きネックレス　23.3ｇ</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：94,700円<br>
                        <span class="blue">B社</span>：94,400円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">95,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>600円</p>
                </div>
              </li>
<!-- 6 -->      
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold006.jpg" alt="">
                  <p class="itemName">Ｋ18ＰＧダイヤリング　10.2ｇ</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：62,000円<br>
                        <span class="blue">B社</span>：61,500円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">63,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>63,000円</p>
                </div>
              </li>
<!-- 7 -->      
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold007.jpg" alt="">
                  <p class="itemName">プラチナリング　Pt950　3.5g</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：15,840円<br>
                        <span class="blue">B社</span>：15,520円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">16,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>480円</p>
                </div>
              </li>
<!-- 8 -->      
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold008.jpg" alt="">
                  <p class="itemName">ダイヤインゴットネックレストップ　枠K18　22.2g</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：90,160円<br>
                        <span class="blue">B社</span>：88,320円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">92,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>3,680円</p>
                </div>
              </li> 
<!-- 9 -->
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold009.jpg" alt="">
                  <p class="itemName">K18ダイヤリング　5g</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：18,600円<br>
                        <span class="blue">B社</span>：18,500円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">19,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>500円</p>
                </div>
              </li> 
<!-- 10 -->                  
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold010.jpg" alt="">
                  <p class="itemName">ダイヤリング　K24　6.8g</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：34,650円<br>
                        <span class="blue">B社</span>：34,300円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">35,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>700円</p>
                </div>
              </li>
<!-- 11 -->                
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold011.jpg" alt="">
                  <p class="itemName">K18WG　8.4g</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：29,179円<br>
                        <span class="blue">B社</span>：28,577円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">30,081<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>1,504円</p>
                </div>
              </li>  
<!-- 12 -->                
              <li class="box-4">
                <div class="title">
                  <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gold012.jpg" alt="">
                  <p class="itemName">K14パールブローチ　6.9g</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：17,600円<br>
                        <span class="blue">B社</span>：17,200円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">18,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>800円</p>
                </div>
              </li>
            </ul>
        <h3><a href="http://kaitorisatei.info/brandrevalue/cat/gem" style="color:#ffffff; text-decoration:none;">宝石買取</a></h3>
                <ul id="box-jisseki" class="list-unstyled clearfix">
        <li class="box-4">
          <div class="title"> <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem005.jpg" alt="">
            <p class="itemName">ダイヤモンド　ルース</p>
            <p class="itemdetail">カラット：1.003ct<br>
              カラー:E<br>
              クラリティ:SI1<br>
              カット:EXCELLENT<br>
              形状:ラウンドブリリアントカット</p>
            <hr>
            <p> <span class="red">A社</span>：194,000円<br>
              <span class="blue">B社</span>：190,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>480,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>10,000円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem006.jpg" alt="">
            <p class="itemName">K18YG<br>
              ダイヤモンドリング</p>
            <p class="itemdetail">カラット(主石)：0.54ct<br>
              カラー：E<br>
              クラリティ：VS２<br>
              カット：VERY GOOD <br>
              形状：ラウンドブリリアント<br>
              メレダイヤモンド0.7ct<br>
              地金：18金イエローゴールド　5ｇ</p>
            <hr>
            <p> <span class="red">A社</span>：145,500円<br>
              <span class="blue">B社</span>：142,500円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>150,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>7,500円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem007.jpg" alt="">
            <p class="itemName">ティファニー<br>
              ソレスト　ダイヤモンドリング</p>
            <p class="itemdetail">PT950<br>
              カラット：0.31ct<br>
              カラー：E<br>
              クラリティ:VVS２<br>
              カット：EXCELLENT</p>
            <hr>
            <p> <span class="red">A社</span>：320,100円<br>
              <span class="blue">B社</span>：313,500円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>330,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>16,500円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem008.jpg" alt="">
            <p class="itemName">ハートシェイプ<br>
              ピンクダイヤモンド　ルース</p>
            <p class="itemdetail">カラット：2ct<br>
              カラー：VERY LIGHT PINK<br>
              クラリティ：VS２<br>
              形状：ハートシェイプ</p>
            <hr>
            <p> <span class="red">A社</span>：4,850,000円<br>
              <span class="blue">B社</span>：4,750,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>5,000,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>250,000円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem009.jpg" alt="">
            <p class="itemName">ペアシェイプ<br>
              ダイヤモンドルース</p>
            <p class="itemdetail">カラット：5ct<br>
              カラー：F<br>
              クラアリティ：SI2<br>
              形状：ペアシェイプ</p>
            <hr>
            <p> <span class="red">A社</span>：3,395,500円<br>
              <span class="blue">B社</span>：3,325,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>3,500,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>175,000円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem010.jpg" alt="">
            <p class="itemName">カルティエ　バレリーナ　ハーフエタニティダイヤリング</p>
            <p class="itemdetail">PT950<br>
              カラット：0.51ct<br>
              カラー：F<br>
              クラリティ：VVS１<br>
              カット：VERY　GOOD</p>
            <hr>
            <p> <span class="red">A社</span>：388,000円<br>
              <span class="blue">B社</span>：380,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>400,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>20,000円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem011.jpg" alt="">
            <p class="itemName">ハリーウィンストン<br>
              マドンナクロスネックレス</p>
            <p class="itemdetail">Pt950<br>
              カラット：1.0ct<br>
              カラー：D<br>
              クラリティ：VVS2<br>
              カット：EXCELLENT</p>
            <hr>
            <p> <span class="red">A社</span>：970,000円<br>
              <span class="blue">B社</span>：950,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>1,000,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>50,000円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/gem012.jpg" alt="">
            <p class="itemName">ピアジェ　パッション<br>
              ダイヤリング</p>
            <p class="itemdetail">Pt950<br>
              カラット：0.30ct<br>
              カラー：E<br>
              クラリティ：VVS2<br>
              カット：VG</p>
            <hr>
            <p> <span class="red">A社</span>：200,000円<br>
              <span class="blue">B社</span>：210,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>230,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>30,000円</p>
          </div>
        </li>
                </ul>
        <h3><a href="http://kaitorisatei.info/brandrevalue/cat/watch" style="color:#ffffff; text-decoration:none;">時計買取</a></h3>
              <ul id="box-jisseki" class="list-unstyled clearfix">
                            <li class="box-4">
                    <div class="title">
                      <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch009.png" alt="">
                      <p class="itemName">PATEK PHILIPPE<br>ノーチラス Ref5712</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：3,201,000円<br>
                        <span class="blue">B社</span>：3,135,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">3,300,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>165,000円</p>
                    </div>
                  </li>
                                    <li class="box-4">
                    <div class="title">
                      <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch010.png" alt="">
                      <p class="itemName">BREITLING<br>ナビタイマー ref. A022B01NP</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：388,000円<br>
                        <span class="blue">B社</span>：380,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">400,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>20,000円</p>
                    </div>
                  </li>
                                    <li class="box-4">
                    <div class="title">
                      <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch011.png" alt="">
                      <p class="itemName">PANERAI<br>ルミノール クロノデイライト</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：582,000円<br>
                        <span class="blue">B社</span>：570,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">600,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>30,000円</p>
                    </div>
                  </li>
                                    <li class="box-4">
                    <div class="title">
                      <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch012.png" alt="">
                      <p class="itemName">ボールウォッチ<br>エンジニア ハイドロカーボン</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：77,600円<br>
                        <span class="blue">B社</span>：76,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">80,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>4,000円</p>
                    </div>
                  </li>
                                    <li class="box-4">
                    <div class="title">
                      <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch001.jpg" alt="">
                      <p class="itemName">ROLEX<br>サブマリーナ Ref.116610LN</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：620,000円<br>
                        <span class="blue">B社</span>：670,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">710,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>90,000円</p>
                    </div>
                  </li>
                                    <li class="box-4">
                    <div class="title">
                      <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch002.jpg" alt="">
                      <p class="itemName">AUDEMARS PIGUET<br>ロイヤルオーク オフショア</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：2,630,000円<br>
                        <span class="blue">B社</span>：2,750,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">2,900,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>270,000円</p>
                    </div>
                  </li>
                                    <li class="box-4">
                    <div class="title">
                      <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch003.jpg" alt="">
                      <p class="itemName">PATEK PHILIPPE<br>コンプリケーテッド</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：3,900,000円<br>
                        <span class="blue">B社</span>：4,100,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">4,700,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>800,000円</p>
                    </div>
                  </li>
                                    <li class="box-4">
                    <div class="title">
                      <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch004.jpg" alt="">
                      <p class="itemName">BREGUET<br>コンプリケーション</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：3,400,000円<br>
                        <span class="blue">B社</span>：3,600,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">4,100,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>700,000円</p>
                    </div>
                  </li>
                                    <li class="box-4">
                    <div class="title">
                      <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch012.jpg" alt="">
                      <p class="itemName">HUBLOT<br>クラシックフュージョン 521.NX.1170.R</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：600,000円<br>
                        <span class="blue">B社</span>：580,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">620,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>40,000円</p>
                    </div>
                  </li>
                                    <li class="box-4">
                    <div class="title">
                      <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch013.jpg" alt="">
                      <p class="itemName">OMEGA<br>スピードマスター レーシング コーアクシャル</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：194,000円<br>
                        <span class="blue">B社</span>：193,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">200,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>7,000円</p>
                    </div>
                  </li>
                                    <li class="box-4">
                    <div class="title">
                      <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch014.jpg" alt="">
                      <p class="itemName">ディオール<br>シフルルージュ ブラックタイム クロノ</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：150,000円<br>
                        <span class="blue">B社</span>：120,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">165,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>45,000円</p>
                    </div>
                  </li>
                                    <li class="box-4">
                    <div class="title">
                      <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/watch015.jpg" alt="">
                      <p class="itemName">ロレックス<br>デイトナ 116505 黒文字盤</p>
                      <hr>
                      <p>
                        <span class="red">A社</span>：2,250,000円<br>
                        <span class="blue">B社</span>：2,200,000円
                      </p>
                    </div>
                    <div class="box-jisseki-cat">
                      <h3>買取価格例</h3>
                      <p class="price">2,300,000<span class="small">円</span></p>
                    </div>
                    <div class="sagaku">
                      <p><span class="small">買取差額“最大”</span>100,000円</p>
                    </div>
                  </li>
                                  </ul>

        <h3><a href="http://kaitorisatei.info/brandrevalue/cat/bag" style="color:#ffffff; text-decoration:none;">バッグ買取</a></h3>
                 <ul id="box-jisseki" class="list-unstyled clearfix">
<!-- 1 -->
                        <li class="box-4">
                            <div class="title">
                              <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag009.png" alt="">
                              <p class="itemName">HERMES<br>エブリン3PM</p>
                              <hr>
                              <p>
                                <span class="red">A社</span>：184,300円<br>
                                <span class="blue">B社</span>：180,500円
                              </p>
                            </div>
                            <div class="box-jisseki-cat">
                              <h3>買取価格例</h3>
                              <p class="price">190,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                              <p><span class="small">買取差額“最大”</span>9,500円</p>
                            </div>
                          </li>
<!-- 2 -->
                            <li class="box-4">
                            <div class="title">
                              <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag010.png" alt="">
                              <p class="itemName">PRADA<br>ハンドバッグ</p>
                              <hr>
                              <p>
                                <span class="red">A社</span>：116,400円<br>
                                <span class="blue">B社</span>：114,000円
                              </p>
                            </div>
                            <div class="box-jisseki-cat">
                              <h3>買取価格例</h3>
                              <p class="price">120,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                              <p><span class="small">買取差額“最大”</span>6,000円</p>
                            </div>
                          </li>
<!-- 3 -->
                          <li class="box-4">
                            <div class="title">
                              <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag011.png" alt="">
                              <p class="itemName">GUCCI<br>バンブーバッグ</p>
                              <hr>
                              <p>
                                <span class="red">A社</span>：29,100円<br>
                                <span class="blue">B社</span>：28,500円
                              </p>
                            </div>
                            <div class="box-jisseki-cat">
                              <h3>買取価格例</h3>
                              <p class="price">30,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                              <p><span class="small">買取差額“最大”</span>1,500円</p>
                            </div>
                          </li>
<!-- 4 -->
                           <li class="box-4">
                            <div class="title">
                              <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag012.png" alt="">
                              <p class="itemName">LOUIS VUITTON<br>モンスリGM</p>
                              <hr>
                              <p>
                                <span class="red">A社</span>：58,200円<br>
                                <span class="blue">B社</span>：57,000円
                              </p>
                            </div>
                            <div class="box-jisseki-cat">
                              <h3>買取価格例</h3>
                              <p class="price">60,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                              <p><span class="small">買取差額“最大”</span>3,000円</p>
                            </div>
                          </li>
<!-- 5 -->
                           <li class="box-4">
                            <div class="title">
                              <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag001.jpg" alt="">
                              <p class="itemName">HERMES<br>バーキン30</p>
                              <hr>
                              <p>
                                <span class="red">A社</span>：1,450,000円<br>
                                <span class="blue">B社</span>：1,520,000円
                              </p>
                            </div>
                            <div class="box-jisseki-cat">
                              <h3>買取価格例</h3>
                              <p class="price">1,600,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                              <p><span class="small">買取差額“最大”</span>150,000円</p>
                            </div>
                          </li>
<!-- 6 -->
                           <li class="box-4">
                            <div class="title">
                              <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag002.jpg" alt="">
                              <p class="itemName">CHANEL<br>マトラッセ チェーンバッグ</p>
                              <hr>
                              <p>
                                <span class="red">A社</span>：100,000円<br>
                                <span class="blue">B社</span>：130,000円
                              </p>
                            </div>
                            <div class="box-jisseki-cat">
                              <h3>買取価格例</h3>
                              <p class="price">150,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                              <p><span class="small">買取差額“最大”</span>50,000円</p>
                            </div>
                          </li>
<!-- 7 -->
                           <li class="box-4">
                            <div class="title">
                              <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag003.jpg" alt="">
                              <p class="itemName">LOUIS VUITTON<br>ダミエ ネヴァーフルMM</p>
                              <hr>
                              <p>
                                <span class="red">A社</span>：92,000円<br>
                                <span class="blue">B社</span>：104,000円
                              </p>
                            </div>
                            <div class="box-jisseki-cat">
                              <h3>買取価格例</h3>
                              <p class="price">110,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                              <p><span class="small">買取差額“最大”</span>18,000円</p>
                            </div>
                          </li>
<!-- 8 -->
                           <li class="box-4">
                            <div class="title">
                              <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag004.jpg" alt="">
                              <p class="itemName">CELINE<br>ラゲージマイクロショッパーバッグ</p>
                              <hr>
                              <p>
                                <span class="red">A社</span>：160,000円<br>
                                <span class="blue">B社</span>：175,000円
                              </p>
                            </div>
                            <div class="box-jisseki-cat">
                              <h3>買取価格例</h3>
                              <p class="price">185,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                              <p><span class="small">買取差額“最大”</span>25,000円</p>
                            </div>
                          </li>
<!-- 9 --> 
                           <li class="box-4">
                            <div class="title">
                              <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag013.jpg" alt="">
                              <p class="itemName">ロエベ<br>レザー　2WAYバッグ</p>
                              <hr>
                              <p>
                                <span class="red">A社</span>：23,000円<br>
                                <span class="blue">B社</span>：22,000円
                              </p>
                            </div>
                            <div class="box-jisseki-cat">
                              <h3>買取価格例</h3>
                              <p class="price">25,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                              <p><span class="small">買取差額“最大”</span>3,000円</p>
                            </div>
                          </li>
<!-- 10 -->
                           <li class="box-4">
                            <div class="title">
                              <img src="https://img.kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag014.jpg" alt="">
                              <p class="itemName">グッチ<br>グッチシマ　レザートートバッグ　オレンジ</p>
                              <hr>
                              <p>
                                <span class="red">A社</span>：38,000円<br>
                                <span class="blue">B社</span>：36,000円
                              </p>
                            </div>
                            <div class="box-jisseki-cat">
                              <h3>買取価格例</h3>
                              <p class="price">40,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                              <p><span class="small">買取差額“最大”</span>4,000円</p>
                            </div>
                          </li>
<!-- 11 -->
                           <li class="box-4">
                            <div class="title">
                              <img src="http://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag015.jpg" alt="">
                              <p class="itemName">プラダ<br>キャンバス×サフィアーノ　2Wayダブルバッグ</p>
                              <hr>
                              <p>
                                <span class="red">A社</span>：120,000円<br>
                                <span class="blue">B社</span>：113,000円
                              </p>
                            </div>
                            <div class="box-jisseki-cat">
                              <h3>買取価格例</h3>
                              <p class="price">126,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                              <p><span class="small">買取差額“最大”</span>13,000円</p>
                            </div>
                          </li>
<!-- 12 -->
                           <li class="box-4">
                            <div class="title">
                              <img src="http://kaitorisatei.info/brandrevalue/wp-content/themes/bring/img/item/bag016.jpg" alt="">
                              <p class="itemName">ボッテガヴェネタ<br>イントレチャート　カバ　トートバッグ</p>
                              <hr>
                              <p>
                                <span class="red">A社</span>：198,000円<br>
                                <span class="blue">B社</span>：190,000円
                              </p>
                            </div>
                            <div class="box-jisseki-cat">
                              <h3>買取価格例</h3>
                              <p class="price">200,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                              <p><span class="small">買取差額“最大”</span>10,000円</p>
                            </div>
                          </li>
                      </ul>

      </section>
            
      <?php
        // アクションポイント
        get_template_part('_action');
        
        // 買取方法
        get_template_part('_purchase');
        
        // 店舗案内
        get_template_part('_shopinfo');
      ?>
      
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
