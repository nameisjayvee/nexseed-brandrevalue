
<?php
    /**
     * The template for displaying all pages.
     *
     * This is the template that displays all pages by default.
     * Please note that this is the WordPress construct of pages
     * and that other 'pages' on your WordPress site may use a
     * different template.
     *
     * @link https://codex.wordpress.org/Template_Hierarchy
     *
     * @package BRING
     */

    get_header();

    $categoryLinkArray = array(
        '時計' => array('url' => home_url('cat/watch'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_watch.jpg', 'alt' => '時計買取実績', 'text' => 'その他のブランド時計参考価格公開中'),
        'バッグ' => array('url' => home_url('cat/bag'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_bag.jpg', 'alt' => '', 'text' => 'その他のブランドバッグ参考価格公開中'),
        'BJ' => array('url' => home_url('cat/brandjewelery'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_jewelry.jpg', 'alt' => '', 'text' => 'その他のブランドジュエリー参考価格公開中'),
        '財布' => array('url' => home_url('cat/wallet'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_bag.jpg', 'alt' => '', 'text' => 'その他のブランド財布参考価格公開中'),
        '金' => array('url' => home_url('cat/gold'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_bag.jpg', 'alt' => '', 'text' => 'その他の金・プラチナ参考価格公開中'),
        '洋服・毛皮' => array('url' => home_url('cat/outfit'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_bag.jpg', 'alt' => '', 'text' => 'その他の洋服・毛皮参考価格公開中'),
        '宝石' => array('url' => home_url('cat/gem'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_bag.jpg', 'alt' => '', 'text' => 'その他のジュエリー参考価格公開中'),
        '骨董品' => array('url' => home_url('introduction/antique'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_bag.jpg', 'alt' => '', 'text' => 'その他の骨董品参考価格公開中'),
        '靴' => array('url' => home_url('cat/shoes'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_bag.jpg', 'alt' => '', 'text' => 'その他のブランド靴参考価格公開中'),
        'アンティークロレックス' => array('url' => home_url('watch/antique_rolex'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_bag.jpg', 'alt' => '', 'text' => 'その他のアンティークロレックス参考価格公開中'),
        'ダイヤモンド' => array('url' => home_url('cat/diamond'), 'image' => get_s3_template_directory_uri() . '/img/ranking/ranking_cate_bag.jpg', 'alt' => '', 'text' => 'その他のダイヤモンド参考価格公開中'));

    $brandNameArray = array(
        'エルメス' => 'HERMES<span>エルメス</span>',
        'ロレックス' => 'ROLEX<span>ロレックス</span>',
        'オーデマピゲ' => 'AUDEMARS PIGUET<span>オーデマピゲ</span>',
        'ハリーウィンストン' => 'HARRY WINSTON<span>ハリーウィンストン</span>',
        '小判' => '小判',
        'カルティエ' => 'Cartier<span>カルティエ</span>',
        'ルイヴィトン' => 'LOUIS VUITTON<span>ルイヴィトン</span>',
        'リシャールミル' => 'RICHARD MILLE<span>リシャールミル</span>');

    // 毎月変える部分
    $yymm = '2019';
    $rankTopImageName = "ranking_mv{$yymm}.jpg";

    $spreadSheetData = '1	時計	ロレックス	サブマリーナ デイト  116610LV 	¥1,360,000	¥1,460,000	¥1,620,000	1740
2	BJ	ハリーウィンストン	リリークラスター ドロップピアス	¥1,300,000	¥1,350,000	¥1,400,000	2221
3	時計	ロレックス	コスモグラフ デイトナ  116500LN	¥2,500,000	¥2,650,000	¥2,800,000	0008
4	バッグ	エルメス	バーキン30 オーストリッチ パーシュマン	¥2,300,000	¥2,500,000	¥2,720,000	0053
5	BJ	カルティエ	ジュストアンクル 	¥498,000	¥520,000	¥550,000	2241
6	時計	パテックフィリップ	パテック・フィリップ ワールドタイム  5130	¥3,160,000	¥3,300,000	¥3,500,000	2526
7	BJ	ハリーウィンストン	マイクロパヴェダイヤリング	¥2,300,000	¥2,400,000	¥2,450,000	2215
8	バッグ	ルイヴィトン	ダミエ ネヴァーフル MM N41358	¥115,000	¥128,000	¥140,000	0109
9	時計	オメガ	デ・ヴィル プレステージ コーアクシャル	¥330,000	¥360,000	¥400,000	1494
10	バッグ	シャネル	ラムスキン トレンディ CC	¥250,000	¥280,000	¥300,000	1886

';

    $pickupBrand = 'ヴァンクリーフ＆アーペル';
    $pickupLink = home_url('cat/gem/vancleefarpels');

    $pickupData = 'アルハンブラ パピヨン ターコイズ ネックレス	\140,000
マジックアルハンブラ シェルネックレス	\300,000
スイートアルハンブラ ピアス マザーオブパール	\180,000
ヴィンテージアルハンブラ ブレスレット オニキス5Ｐ	\300,000
    ';
    // 毎月変える部分ここまで

    $ignoreRegExp = '/[\\\\¥,]/';
    $spreadSheetDataArray = explode("\n", $spreadSheetData);
    $spreadSheetDataArray = array_map(function($data) use ($ignoreRegExp) {
        $data = explode("\t", $data);
        $data = array(
            'rank' => $data[0],
            'category' => $data[1],
            'brand' => $data[2],
            'item_name' => $data[3],
            'a_price' => (int) preg_replace($ignoreRegExp, '', $data[4]),
            'b_price' => (int) preg_replace($ignoreRegExp, '', $data[5]),
            'j_price' => (int) preg_replace($ignoreRegExp, '', $data[6]),
        );
        return $data;
    }, $spreadSheetDataArray);
    $pickupDataArray = explode("\n", $pickupData);
    $pickupDataArray = array_map(function($data) use ($ignoreRegExp) {
        $data = explode("\t", $data);
        $data = array(
            'item_name' => $data[0],
            'price' => (int) preg_replace($ignoreRegExp, '', $data[1])
        );
        return $data;
    }, $pickupDataArray);
?>

<div class="ranking_page">
    <h2><img src="<?php echo get_s3_template_directory_uri() ?>/img/ranking/<?php echo $rankTopImageName ?>" alt="ブランドリバリュー買取注目ランキング"></h2>
    <ul class="rankingbox_top">
        <?php for ($i = 0; $i < 4; $i++) : ?>
            <?php
                $rank = $i + 1;
                $didit2Rank = sprintf("%02d", $rank);
                $categoryData = $categoryLinkArray[$spreadSheetDataArray[$i]['category']];
                if (!empty($brandNameArray[$spreadSheetDataArray[$i]['brand']])) {
                    $brandName = $brandNameArray[$spreadSheetDataArray[$i]['brand']];
                } else {
                    $brandName = $spreadSheetDataArray[$i]['brand'];
                }
            ?>
            <li>
                <div class="ranking_box_wrap">
                    <div class="ranking_img_box">
                        <p class="ranking_ico">
                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/ranking/badge<?php echo $didit2Rank ?>.png">
                        </p>
                        <p class="ranking_item">
                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/ranking/ranking_item<?php echo $didit2Rank ?>.png?<?php echo $yymm ?>" alt="<?php echo $spreadSheetDataArray[$i]['item_name'] ?>">
                        </p>
                    </div>
                    <div class="ranking_info_box">
                        <p class="itemname_top"><?php echo $brandName  ?></p>
                        <p class="itemname_btm"><?php echo $spreadSheetDataArray[$i]['item_name'] ?></p>
                        <div class="price_top">
                            <p><span>A社</span><?php echo number_format($spreadSheetDataArray[$i]['a_price']) ?>円</p>
                            <p><span>B社</span><?php echo number_format($spreadSheetDataArray[$i]['b_price']) ?>円</p>
                        </div>
                        <div class="price_btm">
                            <p>BRAND REVALUE 買取価格</p>
                            <p>~<?php echo number_format($spreadSheetDataArray[$i]['j_price']) ?>円</p>
                        </div>
                    </div>
                </div>
                <?php if ($rank < 4) : ?>
                    <a href="<?php echo $categoryData['url'] ?>"><img src="<?php echo $categoryData['image'] ?>" alt="<?php echo $categoryData['alt'] ?>"></a>
                <?php elseif ($rank === 4) : ?>
                    <p class="link"><a href="<?php echo $categoryData['url'] ?>"><?php echo $categoryData['text'] ?></a></p>
                <?php endif; ?>
            </li>
        <?php endfor; ?>
    </ul>
</div>

<ul class="rankingbox_btm">
    <?php for ($i = 4; $i < 10; $i++) : ?>
        <?php
            $rank = $i + 1;
            $didit2Rank = sprintf("%02d", $rank);
            $categoryData = $categoryLinkArray[$spreadSheetDataArray[$i]['category']];
            if (!empty($brandNameArray[$spreadSheetDataArray[$i]['brand']])) {
                $brandName = $brandNameArray[$spreadSheetDataArray[$i]['brand']];
            } else {
                $brandName = $spreadSheetDataArray[$i]['brand'];
            }
        ?>
        <li>
            <p class="ranking_ico">
                <img src="<?php echo get_s3_template_directory_uri() ?>/img/ranking/badge<?php echo $didit2Rank ?>.png">
            </p>
            <p class="ranking_item">
                <img src="<?php echo get_s3_template_directory_uri() ?>/img/ranking/ranking_item<?php echo $didit2Rank ?>.png?<?php echo $yymm ?>" alt="<?php echo $spreadSheetDataArray[$i]['item_name'] ?>">
            </p>
            <div class="ranking_info_box">
                <p class="itemname_top"><?php echo $brandName ?></p>
                <p class="itemname_btm"><?php echo $spreadSheetDataArray[$i]['item_name'] ?></p>
                <div class="price_top">
                    <p><span>A社：</span><?php echo number_format($spreadSheetDataArray[$i]['a_price']) ?>円</p>
                    <p><span>B社：</span><?php echo number_format($spreadSheetDataArray[$i]['b_price']) ?>円</p>
                </div>
                <div class="price_btm">
                    <p>BRAND REVALUE 買取価格</p>
                    <p>~<?php echo number_format($spreadSheetDataArray[$i]['j_price']) ?>円</p>
                </div>
            </div>
            <p class="link"><a href="<?php echo $categoryData['url'] ?>"><?php echo $categoryData['text'] ?></a></p>
        </li>
    <?php endfor; ?>
</ul>

<div class="ranking-pickup">
    <div class="ranking-pickup-title">
        <h2><img src="<?php echo get_s3_template_directory_uri() . '/img/pickup/pickup_head.jpg' ?>" alt="今月の買取強化PICK UPブランド <?php echo $pickupBrand ?>" /></h2>
    </div>
    <ul class="archive_purchase">
        <?php for ($i = 0; $i < 4; $i++) : ?>
            <?php $digit2 = sprintf("%02d", $i+1); ?>
            <li>
                <p class="kaitori-image"><img src="<?php echo get_s3_template_directory_uri() ?>/img/pickup/pickup_<?php echo $digit2 ?>.jpg" alt=""></p>
                <p class="kaitori-title"><?php echo $pickupDataArray[$i]['item_name'] ?></p>
                <p class="kaitori-price-header">買取価格</p>
                <P class="kaitori-price"> ¥<?php echo number_format($pickupDataArray[$i]['price']) ?></P>
            </li>
        <?php endfor; ?>
    </ul>
    <p>※価格は買取時の商品の状態や付属品の有無に基づいており、最高価格の目安ではありません。（古品、付属品の不足した商品も含まれています。）</p>
    <div class="ranking-pickup-link">
        <a href="<?php echo $pickupLink ?>">その他、<?php echo $pickupBrand ?>の買取実績はコチラ</a>
    </div>
</div>

<div class="ranking_contnt">
    <?php get_template_part('_category'); ?>
    <section id="shop-point">
        <h2 class="mincho"><span>ブランドリバリュー</span>当店の<span class="size44">3</span><span>つ</span>の<span>ポイント</span></h2>
        <ul>
            <li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/top/point01.png" alt="業界再速級">
                <div>
                    <h3 class="mincho">業界最速級！</h3>
                    <p>査定は当日、<span>翌日振込み</span></p>
                    <p>査定の速さはトップクラス</p>
                </div>
            </li>
            <li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/top/point02.png" alt="すべて無料">
                <div>
                    <h3 class="mincho">すべて無料</h3>
                    <p>査定料も送料も、<span>全て0円</span></p>
                    <p>買取にかかる費用は完全無料</p>
                </div>
            </li>
            <li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/top/point03.png" alt="即現金払い">
                <div>
                    <h3 class="mincho">即現金払い</h3>
                    <p><span>お急ぎ</span>の方でも大丈夫</p>
                    <p>出張・店頭買取なら即現金払い</p>
                </div>
            </li>
        </ul>
    </section>
    <?php
        // 3つの買取方法
        get_template_part('_action');
        get_template_part('_shopinfo');
    ?>
</div>
</div>
<!--//page-ranking-->

<?php
    get_footer();
