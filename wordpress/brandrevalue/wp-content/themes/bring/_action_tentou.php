
<section class="cv_parts_action">
<h2>「ブランドリバリュー」のスタイル別買取方法</h2>
<p class="cv02_tx">来店するのが面倒、時間がないという方はこちら！</p>
<ul class="custom_cv_box02 page_cv">
	<li><a href="<?php echo home_url('purchase/tentou'); ?>">
		<p>駅前だから便利!</p>
		<p>店頭買取</p>
		</a></li>
	<li><a href="<?php echo home_url('purchase/takuhai'); ?>">
		<p>忙しい方にオススメ!</p>
		<p>宅配買取</p>
		</a></li>
	<li><a href="<?php echo home_url('purchase/syutchou'); ?>">
		<p>点数が多い方にぴったり!</p>
		<p>出張買取</p>
		</a></li>
</ul>
</section>
