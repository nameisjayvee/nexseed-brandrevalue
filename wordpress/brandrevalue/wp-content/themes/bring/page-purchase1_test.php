<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header('test'); ?>

	<div id="primary" class="form-purchase content-area">
		<main id="main" class="site-main" role="main">
      <section id="mainVisual" style="background:url(<?php echo get_s3_template_directory_uri() ?>/img/mv/purchase.png)">
        <h2 class="text-hide">テストテスト査定希望お申し込み</h2>
      </section>
      
		  <div class="purchase1-bnr-box"><p class="purchase1-bnr-Txt"><span id="phone_number_holder_5">0120-970-060</span></p></div>
		  <p><a href="http://kaitorisatei.info/brandrevalue/line"><img src="<?php echo get_s3_template_directory_uri() ?>/img/mv/purchase1_bnr02.jpg" alt="LINE査定"></a></p>
      

      <section id="mailform">
        <div class="">
          <?php echo do_shortcode('[contact-form-7 id="22346" title="買取査定（通常）_test"]'); ?>
        </div>
      </section>
<div class="textarea">
※メールでのお問い合わせ・査定は、原則的に１営業日以内にご連絡を差し上げるよう努めております。
多くのお問い合わせを頂戴している場合や店舗の混雑状況により、ご回答が遅れる可能性がございます。<br>
当店から返信がない場合には、お手数ではございますが、電話またはＬＩＮＥにてお問い合わせくださいませ。<br><br>
</div>
      <?php
        // アクションポイント
        get_template_part('_action');
        
        // 買取方法
        get_template_part('_purchase');
        
        // 店舗案内
        get_template_part('_shopinfo');
      ?>
      
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer('test');
