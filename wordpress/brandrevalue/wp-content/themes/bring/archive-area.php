<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

 // 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  'bag009.png::HERMES::エブリン3PM::184,300::180,500::190,000::9,500',
  'bag010.png::PRADA::ハンドバッグ::116,400::114,000::120,000::6,000',
  'bag011.png::GUCCI::バンブーバッグ::29,100::28,500::30,000::30,000',
  'bag012.png::LOUIS VUITTON::モンスリGM::58,200::57,000::60,000::3,000',
  'bag001.jpg::HERMES::バーキン30::1,450,000::1,520,000::1,600,000::150,000',
  'bag002.jpg::CHANEL::マトラッセ チェーンバッグ::100,000::130,000::150,000::50,000',
  'bag003.jpg::LOUIS VUITTON::ダミエ ネヴァーフルMM::92,000::104,000::110,000::18,000',
  'bag004.jpg::CELINE::ラゲージマイクロショッパーバッグ::160,000::175,000::1850,000::25,000',
);

// 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  'watch/top/001.jpg::パテックフィリップ::カラトラバ 3923::646,000::640,000::650,000::10,000',
  'watch/top/002.jpg::パテックフィリップ::アクアノート 5065-1A::1,367,000::1,350,000::1,400,000::50,000',
  'watch/top/003.jpg::オーデマピゲ::ロイヤルオーク・オフショア 26170ST::1,200,000::1,195,000::1,210,000::15,000',
  'watch/top/004.jpg::ROLEX::サブマリーナ 116610LN ランダム品番::720,000::700,000::740,000::40,000',
  'watch/top/005.jpg::ROLEX::デイトナ 116505 ランダム品番 コスモグラフ::1,960,000::1,950,000::2,000,000::50,000',
  'watch/top/006.jpg::ブライトリング::クロノマット44::334,000::328,000::340,000::12,000',
  'watch/top/007.jpg::パネライ::ラジオミール 1940::594,000::590,000::600,000::10,000',
  'watch/top/008.jpg::ボールウォッチ::ストークマン ストームチェイサープロ CM3090C::128,000::125,000::130,000::5,000',
  'watch/top/009.jpg::ブレゲ::クラシックツインバレル 5907BB12984::615,000::610,000::626,000::16,000',
  'watch/top/010.jpg::ウブロ::ビッグバン ブラックマジック::720,000::695,000::740,000::45,000',
  'watch/top/011.jpg::オメガ::シーマスター プラネットオーシャン 2208-50::192,000::190,000::200,000::10,000',
  'watch/top/012.jpg::ディオール::シフルルージュ クロノグラフ CD084612 M001::190,000::180,000::200,000::20,000',
);

get_header(); ?>

	<div id="primary" class="about-purchase content-area">
		<main id="main" class="site-main" role="main">
		<div class="area_mv">
		<h2>買取エリア</h2>
		<h3><span>日本全国どこでも高額買取いたします。</span><br />
お気軽にお問合せください。</h3>
		<p>BRAND REVARUEは日本全国からの買取お申込みに対応しております。<br />
※往復送料・梱包キット・キャンセル料は、全て無料です。（お客様費用負担0円）<br />
※ご不明な点はお気軽に当社オペレーターまでお問い合わせくださいませ。</p>

		</div>
		<section class="area_box">
		<h2>買取エリアを選ぶ</h2>
		<h3>北海道・東北エリア</h3>
		<ul class="area_list">
		<li> <a href="<?php echo home_url('/area/hokkaido'); ?>">北海道</a></li>
		<li><a href="<?php echo home_url('/area/aomori'); ?>">青森県</a></li>
		<li><a href="<?php echo home_url('/area/iwate'); ?>">岩手県</a></li>
		<li><a href="<?php echo home_url('/area/miyagi'); ?>">宮城県</a></li>
		<li><a href="<?php echo home_url('/area/akita'); ?>">秋田県</a></li>
		<li><a href="<?php echo home_url('/area/yamagata'); ?>">山形県</a></li>
		<li><a href="<?php echo home_url('/area/fukushima'); ?>">福島県</a></li>
		</ul>
		<h3>関東エリア</h3>
		<ul class="area_list">
		<li><a href="<?php echo home_url('/area/tokyo'); ?>">東京都</a></li>
		<li><a href="<?php echo home_url('/area/kanagawa'); ?>">神奈川県</a></li>
		<li><a href="<?php echo home_url('/area/chiba'); ?>">千葉県</a></li>
		<li><a href="<?php echo home_url('/area/saitama'); ?>">埼玉県</a></li>
		<li><a href="<?php echo home_url('/area/ibaraki'); ?>">茨城県</a></li>
		<li><a href="<?php echo home_url('/area/tochigi'); ?>">栃木県</a></li>
		<li><a href="<?php echo home_url('/area/gunma'); ?>">群馬県</a></li>
		</ul>
		<h3>北陸・甲信越エリア</h3>
		<ul class="area_list">
		<li><a href="<?php echo home_url('/area/niigata'); ?>">新潟県</a></li>
		<li><a href="<?php echo home_url('/area/toyama'); ?>">富山県</a></li>
		<li><a href="<?php echo home_url('/area/ishikawa'); ?>">石川県</a></li>
		<li><a href="<?php echo home_url('/area/fukui'); ?>">福井県</a></li>
		<li><a href="<?php echo home_url('/area/yamanashi'); ?>">山梨県</a></li>
		<li><a href="<?php echo home_url('/area/nagano'); ?>">長野県</a></li>
		</ul>
		<h3>東海エリア</h3>
		<ul class="area_list">
		<li><a href="<?php echo home_url('/area/gifu'); ?>">岐阜県</a></li>
		<li><a href="<?php echo home_url('/area/shizuoka'); ?>">静岡県</a></li>
		<li><a href="<?php echo home_url('/area/aichi'); ?>">愛知県</a></li>
		<li><a href="<?php echo home_url('/area/mie'); ?>">三重県</a></li>

		</ul>
		<h3>関西エリア</h3>
		<ul class="area_list">
		<li><a href="<?php echo home_url('/area/osaka'); ?>">大阪府</a></li>
		<li><a href="<?php echo home_url('/area/kyoto'); ?>">京都府</a></li>
		<li><a href="<?php echo home_url('/area/shiga'); ?>">滋賀県</a></li>
		<li><a href="<?php echo home_url('/area/hyogo'); ?>">兵庫県</a></li>
		<li><a href="<?php echo home_url('/area/nara'); ?>">奈良県</a></li>
		<li><a href="<?php echo home_url('/area/wakayama'); ?>">和歌山県</a></li>
		</ul>
		<h3>中国エリア</h3>
		<ul class="area_list">
		<li><a href="<?php echo home_url('/area/tottori'); ?>">鳥取県</a></li>
		<li><a href="<?php echo home_url('/area/shimane'); ?>">島根県</a></li>
		<li><a href="<?php echo home_url('/area/okayama'); ?>">岡山県</a></li>
		<li><a href="<?php echo home_url('/area/hiroshima'); ?>">広島県</a></li>
		<li><a href="<?php echo home_url('/area/yamaguchi'); ?>">山口県</a></li>
		</ul>
		<h3>四国エリア</h3>
		<ul class="area_list">
		<li><a href="<?php echo home_url('/area/tokushima'); ?>">徳島県</a></li>
		<li><a href="<?php echo home_url('/area/kagawa'); ?>">香川県</a></li>
		<li><a href="<?php echo home_url('/area/ehime'); ?>">愛媛県</a></li>
		<li><a href="<?php echo home_url('/area/kochi'); ?>">高知県</a></li>
		</ul>
		<h3>九州・沖縄エリア</h3>
		<ul class="area_list">
		<li><a href="<?php echo home_url('/area/fukuoka'); ?>">福岡県</a></li>
		<li><a href="<?php echo home_url('/area/saga'); ?>">佐賀県</a></li>
		<li><a href="<?php echo home_url('/area/nagasaki'); ?>">長崎県</a></li>
		<li><a href="<?php echo home_url('/area/kumamoto'); ?>">熊本県</a></li>
		<li><a href="<?php echo home_url('/area/oita'); ?>">大分県</a></li>
		<li><a href="<?php echo home_url('/area/miyazaki'); ?>">宮崎県</a></li>
		<li><a href="<?php echo home_url('/area/kagoshima'); ?>">鹿児島県</a></li>
		<li><a href="<?php echo home_url('/area/okinawa'); ?>">沖縄県</a></li>

		</ul>

		</section>


      <section>
<img src="<?php echo get_s3_template_directory_uri() ?>/img/mv/takuhai_mv.png" alt="宅配買取全て無料で対応致します。">
      </section>
	  <ul class="takuhai_topbnr">
	  <li><a href="tel:0120970060"><img src="<?php echo get_s3_template_directory_uri() ?>/img/takuhai_top_bnr01.png" alt="宅配買取全て無料で対応致します。"></a></li>
	  <li><a href="<?php echo home_url('purchase3') ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/takuhai_top_bnr02.png" alt="無料宅配キット申し込み"></a></li>

	  </ul>

<section class="kaitori_voice">
<h3>お客様の声</h3>
<ul>
<li>
<p class="kaitori_tab takuhai_tab">宅配買取</p>
<h4>東京都杉並区 | 40代 | 女性</h4>
<p class="voice_txt">クローゼットの整理をしていると、もう使わなくなったエルメスのバーキン30が出てきたので、ブランドリバリューの宅配買取をお願いしました。宅配キットを無料で準備して頂けて、その箱にアイテムを詰めて送るだけなので、杉並区から重たい荷物を店頭に持ち込むことなく、とても手軽に買取査定を利用することができました。査定額も期待以上に高額で、すぐに買取をお願いしたほどです。対応も早く、とても信頼できる買取店だと思います</p>
</li>

<li>
<p class="kaitori_tab takuhai_tab">宅配買取</p>
<h4>神奈川県鎌倉市 | 30代 | 男性</h4>
<p class="voice_txt">このたび、ブランドリバリューの宅配買取で、長い間愛用してきたROLEXのデイトナ 116520　ランダム品番 黒文字盤を納得の買取額で売却することができました。結婚資金の一部に充てたかったので、誰にも知られず、また鎌倉にある買取店よりも高額で買取してもらえて本当に感謝しています。ロレックス買取を強化しているブランドリバリューだからこその買取査定だと満足しています。今後は宅配買取だけなく、機会があれば店頭も利用してみたいと思います。		</p>
</li>
<li>
<p class="kaitori_tab takuhai_tab">宅配買取</p>
<h4>兵庫県姫路市 | 20代 | 女性</h4>
<p class="voice_txt">お店のお客様から頂いたカルティエのマイヨンパンテール　フルダイヤリングを、宅配買取でブランドリバリューに買取してもらいました。お客様も姫路市内の方なので地元の買取店は利用したくなくて、遠方の買取店をネットで探していたところ、ブランドリバリューを見つけました。高額なブランドジュエリーの買取実績も多く安心して査定をしてもらえそうだったので、利用してみることにしました。利用してみると、自宅にながら査定から買取、振込をしてもらえるので、店頭に行くよりも便利だと思いました。		</p>
</li>
</ul>
</section>






		<section id="top-jisseki">
			<h2 class="text-hide">買取実績</h2>
			<div class="full_content">

				<!-- ブランド買取 -->
				<div class="menu hover"><span class="glyphicon glyphicon-tag"></span> ブランド買取</div>
				<div class="content">
					<!-- box -->
					<ul id="box-jisseki" class="list-unstyled clearfix">
						<!-- brand1 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/001.jpg" alt="">
								<p class="itemName">オーデマピゲ　ロイヤルオークオフショアクロノ 26470OR.OO.A002CR.01 ゴールド K18PG</p>
								<hr>
								<p> <span class="red">A社</span>：3,120,000円<br>
									<span class="blue">B社</span>：3,100,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">3,150,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>50,000円</p>
							</div>
						</li>

						<!-- brand2 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/002.jpg" alt="">
								<p class="itemName">パテックフィリップ　コンプリケーテッド ウォッチ 5085/1A-001</p>
								<hr>
								<p> <span class="red">A社</span>：1,400,000円<br>
									<span class="blue">B社</span>：1,390,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">1,420,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>30,000円</p>
							</div>
						</li>

						<!-- brand3 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/003.jpg" alt="">
								<p class="itemName">パテックフィリップ　コンプリケーション 5130G-001 WG</p>
								<hr>
								<p> <span class="red">A社</span>：2,970,000円<br>
									<span class="blue">B社</span>：2,950,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">3,000,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>50,000円</p>
							</div>
						</li>

						<!-- brand4 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/004.jpg" alt="">
								<p class="itemName">パテックフィリップ　ワールドタイム 5130R-001</p>
								<hr>
								<p> <span class="red">A社</span>：3,000,000円<br>
									<span class="blue">B社</span>：2,980,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">3,050,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>70,000円</p>
							</div>
						</li>

						<!-- brand5 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/005.jpg" alt="">
								<p class="itemName">シャネル　ラムスキン　マトラッセ　二つ折り長財布</p>
								<hr>
								<p> <span class="red">A社</span>：69,000円<br>
									<span class="blue">B社</span>：68,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">70,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,000円</p>
							</div>
						</li>

						<!-- brand6 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/006.jpg" alt="">
								<p class="itemName">エルメス　ベアンスフレ　ブラック</p>
								<hr>
								<p> <span class="red">A社</span>：200,000円<br>
									<span class="blue">B社</span>：196,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">211,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>15,000円</p>
							</div>
						</li>

						<!-- brand7 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/007.jpg" alt="">
								<p class="itemName">エルメス　バーキン30　トリヨンクレマンス　マラカイト　SV金具</p>
								<hr>
								<p> <span class="red">A社</span>：1,220,000円<br>
									<span class="blue">B社</span>：1,200,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">1,240,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>40,000円</p>
							</div>
						</li>

						<!-- brand8 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/008.jpg" alt="">
								<p class="itemName">セリーヌ　ラゲージマイクロショッパー</p>
								<hr>
								<p> <span class="red">A社</span>：150,000円<br>
									<span class="blue">B社</span>：147,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">155,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>8,000円</p>
							</div>
						</li>

						<!--brand9 -->

						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/009.jpg" alt="">
								<p class="itemName">ルイヴィトン　裏地ダミエ柄マッキントッシュジャケット</p>
								<hr>
								<p> <span class="red">A社</span>：31,000円<br>
									<span class="blue">B社</span>：30,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">32,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,000円</p>
							</div>
						</li>

						<!-- brand10 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/010.jpg" alt="">
								<p class="itemName">シャネル　ココマーク　ラパンファーマフラー</p>
								<hr>
								<p> <span class="red">A社</span>：68,000円<br>
									<span class="blue">B社</span>：65,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">71,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>6,000円</p>
							</div>
						</li>

						<!-- brand11 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/011.jpg" alt="">
								<p class="itemName">ルイヴィトン　ダミエ柄サイドジップレザーブーツ</p>
								<hr>
								<p> <span class="red">A社</span>：49,000円<br>
									<span class="blue">B社</span>：48,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">51,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>3,000円</p>
							</div>
						</li>

						<!-- brand12 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/012.jpg" alt="">
								<p class="itemName">サルバトーレフェラガモ　ヴェラリボンパンプス</p>
								<hr>
								<p> <span class="red">A社</span>：9,000円<br>
									<span class="blue">B社</span>：8,500円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">10,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,500円</p>
							</div>
						</li>
					</ul>
				</div>

				<!-- 金買取 -->
				<div class="menu"><span class="glyphicon glyphicon-bookmark"></span> 金買取</div>
				<div class="content">
					<!-- box -->
					<ul id="box-jisseki" class="list-unstyled clearfix">
						<!-- 1 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/001.jpg" alt="">
								<p class="itemName">K18　ダイヤ0.11ctリング</p>
								<hr>
								<p> <span class="red">A社</span>：23,700円<br>
									<span class="blue">B社</span>：23,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price"><!-- span class="small" >120g</span -->25,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,000円</p>
							</div>
						</li>
						<!-- 2 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/002.jpg" alt="">
								<p class="itemName">K18　メレダイヤリング</p>
								<hr>
								<p> <span class="red">A社</span>：38,000円<br>
									<span class="blue">B社</span>：37,500円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price"><!-- span class="small" >120g</span -->39,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,500円</p>
							</div>
						</li>
						<!-- 3 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/003.jpg" alt="">
								<p class="itemName">K18/Pt900　メレダイアリング</p>
								<hr>
								<p> <span class="red">A社</span>：14,200円<br>
									<span class="blue">B社</span>：14,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">16,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,000円</p>
							</div>
						</li>
						<!-- 4 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/004.jpg" alt="">
								<p class="itemName">Pt900　メレダイヤリング</p>
								<hr>
								<p> <span class="red">A社</span>：22,600円<br>
									<span class="blue">B社</span>：21,200円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">23,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,800円</p>
							</div>
						</li>
						<!-- 5 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/005.jpg" alt="">
								<p class="itemName">K18WG　テニスブレスレット</p>
								<hr>
								<p> <span class="red">A社</span>：24,600円<br>
									<span class="blue">B社</span>：23,800円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">26,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,200円</p>
							</div>
						</li>
						<!-- 6 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/006.jpg" alt="">
								<p class="itemName">K18/K18WG　メレダイヤリング</p>
								<hr>
								<p> <span class="red">A社</span>：22,400円<br>
									<span class="blue">B社</span>：22,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">23,600<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,600円</p>
							</div>
						</li>
						<!-- 7 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/007.jpg" alt="">
								<p class="itemName">K18ブレスレット</p>
								<hr>
								<p> <span class="red">A社</span>：18,100円<br>
									<span class="blue">B社</span>：17,960円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">20,100<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,140円</p>
							</div>
						</li>
						<!-- 8 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/008.jpg" alt="">
								<p class="itemName">K14WGブレスレット</p>
								<hr>
								<p> <span class="red">A社</span>：23,600円<br>
									<span class="blue">B社</span>：22,800円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">24,300<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,500円</p>
							</div>
						</li>
						<!-- 9 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/009.jpg" alt="">
								<p class="itemName">Pt850ブレスレット</p>
								<hr>
								<p> <span class="red">A社</span>：23,600円<br>
									<span class="blue">B社</span>：23,200円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">24,700<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,500円</p>
							</div>
						</li>
						<!-- 10 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/010.jpg" alt="">
								<p class="itemName">Pt900/Pt850メレダイヤネックレス</p>
								<hr>
								<p> <span class="red">A社</span>：29,600円<br>
									<span class="blue">B社</span>：28,400円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">30,500<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,100円</p>
							</div>
						</li>
						<!-- 11 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/011.jpg" alt="">
								<p class="itemName">K18/Pt900/K24ネックレストップ</p>
								<hr>
								<p> <span class="red">A社</span>：43,600円<br>
									<span class="blue">B社</span>：43,100円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">45,900<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,800円</p>
							</div>
						</li>
						<!-- 12 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/012.jpg" alt="">
								<p class="itemName">K24インゴットメレダイヤネックレストップ</p>
								<hr>
								<p> <span class="red">A社</span>：53,700円<br>
									<span class="blue">B社</span>：52,900円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">56,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>3,100円</p>
							</div>
						</li>
					</ul>
				</div>

				<!-- 宝石買取 -->
				<div class="menu"><span class="glyphicon glyphicon-magnet"></span> 宝石買取</div>
				<div class="content">
					<!-- box -->
					<ul id="box-jisseki" class="list-unstyled clearfix">
						<!--<li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem001.jpg" alt="">
            <p class="itemName">エメラルド</p>
            <hr>
            <p> <span class="red">A社</span>：190,000円<br>
              <span class="blue">B社</span>：194,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>200,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>7,500円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem002.jpg" alt="">
            <p class="itemName">サファイア</p>
            <hr>
            <p> <span class="red">A社</span>：145,500円<br>
              <span class="blue">B社</span>：142,500円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>150,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>10,000円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem003.jpg" alt="">
            <p class="itemName">ルビー</p>
            <hr>
            <p> <span class="red">A社</span>：97,000円<br>
              <span class="blue">B社</span>：95,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>100,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>5,000円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem004.jpg" alt="">
            <p class="itemName">アレキサンドライト</p>
            <hr>
            <p> <span class="red">A社</span>：67,900円<br>
              <span class="blue">B社</span>：66,500円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>70,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>4,500円</p>
          </div>
        </li>-->

						<!-- 1 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/001.jpg" alt="">
								<p class="itemName">ダイヤルース</p>
								<p class="itemdetail">カラット：1.003ct<br>
									カラー：G<br>
									クラリティ：VS-2<br>
									カット：Good<br>
									蛍光性：FAINT<br>
									形状：ラウンドブリリアント</p>
								<hr>
								<p> <span class="red">A社</span>：215,000円<br>
									<span class="blue">B社</span>：210,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price"><!-- span class="small">地金＋</span -->221,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>11,000円</p>
							</div>
						</li>

						<!-- 2 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/002.jpg" alt="">
								<p class="itemName">Pt900 DR0.417ctリング</p>
								<p class="itemdetail"> カラー：F<br>
									クラリティ：SI-1<br>
									カット：VERY GOOD <br>
									蛍光性：FAINT<br>
									形状：ラウンドブリリアント</p>
								<hr>
								<p> <span class="red">A社</span>：42,000円<br>
									<span class="blue">B社</span>：41,300円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">44,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,700円</p>
							</div>
						</li>

						<!-- 3 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/003.jpg" alt="">
								<p class="itemName">Pt900　DR0.25ctリング</p>
								<p class="itemdetail"> カラー：H<br>
									クラリティ:VS-1<br>
									カット：Good</br>
									蛍光性：MB<br>
									形状：ラウンドブリリアント<br>

								<hr>
								<p> <span class="red">A社</span>：67,400円<br>
									<span class="blue">B社</span>：67,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">68,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,000円</p>
							</div>
						</li>

						<!-- 4 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/004.jpg" alt="">
								<p class="itemName">K18　DR0.43ct　MD0.4ctネックレストップ</p>
								<p class="itemdetail"> カラー：I<br>
									クラリティ：VS-2<br>
									カット：Good<br>
									蛍光性：WB<br>
									形状：ラウンドブリリアント</p>
								<hr>
								<p> <span class="red">A社</span>：50,000円<br>
									<span class="blue">B社</span>：49,500円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">51,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,500円</p>
							</div>
						</li>

						<!-- 5 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/005.jpg" alt="">
								<p class="itemName">ダイヤルース</p>
								<p class="itemdetail">カラット：0.787ct<br>
									カラー：E<br>
									クラアリティ：VVS-2<br>
									カット：Good<br>
									蛍光性：FAINT<br>
									形状：ラウンドブリリアント</p>
								<hr>
								<p> <span class="red">A社</span>：250,000円<br>
									<span class="blue">B社</span>：248,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">257,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>9,000円</p>
							</div>
						</li>

						<!-- 6 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/006.jpg" alt="">
								<p class="itemName">Pt950　MD0.326ct　0.203ct　0.150ctネックレス</p>
								<p class="itemdetail"> カラー：F<br>
									クラリティ：SI-2<br>
									カット：Good<br>
									蛍光性：FAINT<br>
									形状：ラウンドブリリアント</p>
								<hr>
								<p> <span class="red">A社</span>：55,000円<br>
									<span class="blue">B社</span>：54,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">57,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>3,000円</p>
							</div>
						</li>

						<!-- 7 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/007.jpg" alt="">
								<p class="itemName">ダイヤルース</p>
								<p class="itemdetail">カラット：1.199ct<br>
									カラー：K<br>
									クラリティ：SI-2<br>
									カット：評価無<br>
									蛍光性：NONE<br>
									形状：パビリオン<br>
								</p>
								<hr>
								<p> <span class="red">A社</span>：58,000円<br>
									<span class="blue">B社</span>：56,800円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">60,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>3,200円</p>
							</div>
						</li>

						<!-- 8 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/008.jpg" alt="">
								<p class="itemName">ダイヤルース</p>
								<p class="itemdetail">カラット：8.1957ct<br>
									カラー：LIGTH YELLOW<br>
									クラリティ：VS-1<br>
									カット：VERY GOOD<br>
									蛍光性：FAINT<br>
									形状：ラウンドブリリアント</p>
								<hr>
								<p> <span class="red">A社</span>：6,540,000円<br>
									<span class="blue">B社</span>：6,500,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">6,680,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>180,000円</p>
							</div>
						</li>
					</ul>
				</div>
				<div class="menu"><span class="glyphicon glyphicon-time"></span> 時計買取</div>
				<div class="content">
					<ul id="box-jisseki" class="list-unstyled clearfix">
						<?php
                    foreach($resultLists as $list):
                    // :: で分割
                    $listItem = explode('::', $list);

                  ?>
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/<?php echo $listItem[0]; ?>" alt="">
								<p class="itemName"><?php echo $listItem[1]; ?><br>
									<?php echo $listItem[2]; ?></p>
								<hr>
								<p> <span class="red">A社</span>：<?php echo $listItem[3]; ?>円<br>
									<span class="blue">B社</span>：<?php echo $listItem[4]; ?>円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price"><?php echo $listItem[5]; ?><span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span><?php echo $listItem[6]; ?>円</p>
							</div>
						</li>
						<?php endforeach; ?>
					</ul>
				</div>

				<!-- バッグ買取 -->
				<div class="menu"><span class="glyphicon glyphicon-briefcase"></span> バッグ買取</div>
				<div class="content">
					<ul id="box-jisseki" class="list-unstyled clearfix">
						<!-- 1 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/001.jpg" alt="">
								<p class="itemName">エルメス　エブリンⅢ　トリヨンクレマンス</p>
								<hr>
								<p> <span class="red">A社</span>：176,000円<br>
									<span class="blue">B社</span>：172,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">180,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>8,000円</p>
							</div>
						</li>
						<!-- 2 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/002.jpg" alt="">
								<p class="itemName">プラダ　シティトート2WAYバッグ</p>
								<hr>
								<p> <span class="red">A社</span>：128,000円<br>
									<span class="blue">B社</span>：125,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">132,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>7,000円</p>
							</div>
						</li>
						<!-- 3 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/003.jpg" alt="">
								<p class="itemName">バンブーデイリー2WAYバッグ</p>
								<hr>
								<p> <span class="red">A社</span>：83,000円<br>
									<span class="blue">B社</span>：82,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">85,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>3,000円</p>
							</div>
						</li>
						<!-- 4 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/004.jpg" alt="">
								<p class="itemName">ルイヴィトン　モノグラムモンスリGM</p>
								<hr>
								<p> <span class="red">A社</span>：64,000円<br>
									<span class="blue">B社</span>：63,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">66,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>3,000円</p>
							</div>
						</li>
						<!-- 5 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/005.jpg" alt="">
								<p class="itemName">エルメス　バーキン30</p>
								<hr>
								<p> <span class="red">A社</span>：1,150,000円<br>
									<span class="blue">B社</span>：1,130,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">1,200,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>70,000円</p>
							</div>
						</li>
						<!-- 6 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/006.jpg" alt="">
								<p class="itemName">シャネル　マトラッセダブルフラップダブルチェーンショルダーバッグ</p>
								<hr>
								<p> <span class="red">A社</span>：252,000円<br>
									<span class="blue">B社</span>：248,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">260,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>12,000円</p>
							</div>
						</li>
						<!-- 7 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/007.jpg" alt="">
								<p class="itemName">ルイヴィトン　ダミエネヴァーフルMM</p>
								<hr>
								<p> <span class="red">A社</span>：96,000円<br>
									<span class="blue">B社</span>：94,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">98,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>4,000円</p>
							</div>
						</li>
						<!-- 8 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/008.jpg" alt="">
								<p class="itemName">セリーヌ　ラゲージマイクロショッパー</p>
								<hr>
								<p> <span class="red">A社</span>：150,000円<br>
									<span class="blue">B社</span>：148,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">155,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>7,000円</p>
							</div>
						</li>
						<!-- 9 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/009.jpg" alt="">
								<p class="itemName">ロエベ　アマソナ23</p>
								<hr>
								<p> <span class="red">A社</span>：86,000円<br>
									<span class="blue">B社</span>：82,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">90,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>8,000円</p>
							</div>
						</li>
						<!-- 10 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/010.jpg" alt="">
								<p class="itemName">グッチ　グッチシマ　ビジネスバッグ</p>
								<hr>
								<p> <span class="red">A社</span>：50,000円<br>
									<span class="blue">B社</span>：48,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">53,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>5,000円</p>
							</div>
						</li>
						<!-- 11 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/011.jpg" alt="">
								<p class="itemName">プラダ　サフィアーノ2WAYショルダーバッグ</p>
								<hr>
								<p> <span class="red">A社</span>：115,000円<br>
									<span class="blue">B社</span>：110,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">125,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>15,000円</p>
							</div>
						</li>
						<!-- 12 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/012.jpg" alt="">
								<p class="itemName">ボッテガヴェネタ　カバMM　ポーチ付き</p>
								<hr>
								<p> <span class="red">A社</span>：150,000円<br>
									<span class="blue">B社</span>：146,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">155,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>9,000円</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</section>
<section> <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/bn_matomegai.png"> </section>
<?php

        // 買取方法
        get_template_part('_purchase');

        // 店舗案内
        get_template_part('_shopinfo');
      ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
