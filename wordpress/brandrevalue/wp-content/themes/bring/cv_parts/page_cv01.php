<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

?>

<section class="cv_parts01">
	<h2><span>簡単！便利！</span>無料査定・問合せのご案内</h2>
	<ul class="cv_box01 page_cv">
		<li><a href="tel:0120970060">
			<p class="cv01_ttl">お電話でのお問い合わせ</p>
			<p>0120-970-060</p>
			<p>【受付時間】11:00 ~ 21:00 ※年中無休</p>
			</a></li>
		<li><a href="<?php echo home_url('line'); ?>">
			<p class="cv01_ttl">LINEでカンタン査定!</p>
			<p>最短30秒でのお申し込み!</p>
			<p>LINE査定についてはこちら<i class="fas fa-caret-right fa-lg faa-horizontal "></i></p>
			</a></li>
		<li><a href="<?php echo home_url('purchase1'); ?>">
			<p class="cv01_ttl">メールで簡単査定</p>
			<p>パソコン・スマホから24時間受付 !</p>
			<p><img src="<?php echo get_s3_template_directory_uri() ?>/img/cv01/btn.png" alt="お申し込みはこちら"></p>
			</a></li>
	</ul>
</section>
