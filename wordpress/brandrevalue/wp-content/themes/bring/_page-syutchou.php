<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>

	<div id="primary" class="about-purchase content-area">
		<main id="main" class="site-main" role="main">
      <section id="mainVisual" style="background:url(<?php echo get_s3_template_directory_uri() ?>/img/mv/about-purchase-syutchou.png)">
        <h2 class="text-hide">出張買取　〜連絡して、待つだけ〜</h2>
      </section>
      
      <section id="catchcopy">
        <h3>家から一歩も出ずにスピード査定を受けられる、楽ちんサービス！</h3>
        <p>東京近郊にお住まいで、「品物を店舗まで運んだり、郵送の手続きをする時間がない」という方にお勧めしたいのが、出張買取サービスです。お申込み後「最短即日」でBBRANDREVALUE(ブランドリバリュー)の鑑定スタッフがお客様のご自宅までお伺いし、その場でお品物をスピード査定。金額にご納得いただければその場で現金をお渡しいたします。出張買取は家から一歩も出ることなく、店舗と同様のスピーディな買取サービスをご利用いただける、「一番楽ちん」なスタイルです。</p>
      </section>
      
      
      <section id="about-purchase-feature1" class="clearfix">
        <h3><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/syutchou-feature1.png"></h3>
        <div>
          <img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/syutchou01.png" class="pull-left">
          <p>出張買取サービスのメリットは、何と言っても「家にいながらにしてお品物の査定を受けられる」こと。店舗まで移動する必要がない上に、「郵送中に商品が破損したり紛失したりするかも」といった不安も無用です。さらに、商品の査定を行う鑑定スタッフと直接顔を合わせることができるので、わからないことや不安なこともすぐお気軽にお問い合わせいただけます。時間を大切にしたい方にこそ、出張買取は最適なサービスと言えるでしょう。</p>
        </div>
      </section>
      
      <section id="about-purchase-feature2">
        <h3><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/syutchou-feature2.png"></h3>
        <div>
          <img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/syutchou02.png" class="pull-left">
          <p>出張買取サービスをご希望の方は、まずは当WEBサイトの申し込みフォーム（http://brand.kaitorisatei.info/）またはお電話（0120-970-060）よりお申込みください。出張買取の対象エリアは原則東京23区内となっておりますので、それ以外の場合は別途お問い合わせください。ご希望のご訪問日を調整後、最短即日で鑑定スタッフがご自宅へ伺います。査定は1品約3分とスピーディに行い、金額にご納得いただければその場で現金をお支払いいたします。</p>
        </div>
      </section>
      
      <section id="about-purchase-feature3">
        <h3><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/syutchou-feature3.png"></h3>
        <div>
          <img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/tentou02.png" class="pull-left">
          <p>店頭買取サービスをご利用いただく際には、お品物の査定を依頼するご本人の身分証明書をご用意いただく必要がございます。身分証明書としては、運転免許証、パスポート、保険証、住民基本台帳カード（住基カード）のいずれかをお送りください。いずれもコピーでOKです。お振込み先情報につきましては、BRANDREVALUE(ブランドリバリュー)にて用紙を用意いたしますので、そちらにご記入いただきお品物とご同梱ください。なお、腕時計等の壊れやすいお品物につきましては、郵送中の故障・破損を未然に防ぐため、緩衝材でしっかりお包みください。また、お品物の点数に誤りがないよう、用紙にご記入いただきますよう、お願いいたします。</p>
        
          <table class="table table-bordered">
            <caption>下記をご用意下さい。</caption>
            <tr><th>お品物</th><td>買取して欲しい商品をご用意ください。</td></tr>
            <tr><th>身分証明書</th><td>運転免許証、保険証、パスポートなど身分を証明できるもの</td></tr>
          </table>
          <p>
            ※買い取り希望ブランドアイテムの保証書、鑑定書、箱や付属品などもあわせて持ってきてください。<br>
            ※未成年の方に関しましては、保護者からの同意書、もしくは同伴が必要です。
          </p>
        </div>
      </section>
      
      <?php
        // アクションポイント
        get_template_part('_action');
        
        // 買取方法
        get_template_part('_purchase');
        
        // 店舗案内
        get_template_part('_shopinfo');
      ?>
      
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
