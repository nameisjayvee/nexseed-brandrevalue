<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  '001.jpg::::ルイヴィトン　リバーシブルブルゾン::57,000::55,000::61,000::6,000',
  '002.jpg::::モンクレール　マヤ　迷彩柄ダウンジャケット::120,000::117,000::126,000::9,000',
  '003.jpg::::シャネル　ツイードジャケット::118,000::115,000::122,000::7,000',
  '004.jpg::::ドルチェ＆ガッバーナ　スタッズ付きダメージデニムパンツ::18,000::15,000::21,000::6,000',
  '005.jpg::::グッチ　ムートンジャケット::46,000::40,000::50,000::10,000',
  '006.jpg::::エルメス　レザージャケット::94,000::90,000::100,000::10,000',
  '007.jpg::::リアルマッコイ　ムートンジャケット::80,000::75,000::86,000::11,000',
  '008.jpg::::クロムハーツ　デニムジャケット::54,000::50,000::58,000::8,000',
  '009.jpg::::シャネル　セットアップ::90,000::87,000::93,000::6,000',
  '010.jpg::::エルメス　エルミエンヌ　ダウンコート::90,000::86,000::92,000::6,000',
  '011.jpg::::エルメス　メンズラムスキンレザージャケット::74,000::72,000::80,000::8,000',
  '012.jpg::::シャネル　ツイードジャケット::58,000::55,000::61,000::6,000',
);


get_header(); ?>

<p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
<p class="main_bottom">ブランドリバリューは洋服・毛皮の買取実績多数！</p>
<div id="primary" class="cat-page content-area">
<main id="main" class="site-main" role="main">
<div id="lp_head" class="outfit_ttl">
<div>
<p>銀座で最高水準の査定価格・サービス品質をご体験ください。</p>
<h2>あなたの毛皮・洋服<br />どんな物でもお売り下さい！！</h2>
</div>
</div>
      <p id="catchcopy">洋服・毛皮等のアパレル品は、品目が多くチェックポイントも複雑なため、一般的なブランド・貴金属買取ショップでは適切な査定が難しいこともあります。しかし、BRAND REVALUEは系列店で培ってきた豊富な古着買取の経験があり、熟練の鑑定士がスピーディかつ正確に査定を行うことが可能です。
      <br>
      他店では値段がほとんどつかなかったような洋服・毛皮でも、BRAND REVALUEなら納得の高額査定で買い取らせていただくケースがございます。</p>
      
      <section id="hikaku" class="outfit_hikaku">
        <h3 class="text-hide">他社の買取額と比較すると</h3>
        <p class="hikakuName">モンクレール<br>マヤ　迷彩柄ダウンジャケット</p>
        <p class="hikakuText">※状態が良い場合や当店で品薄の場合などは<br>
        　特に高価買取致します。</p>
        <p class="hikakuPrice1"><span class="red">A社</span>：120,000円</p>
        <p class="hikakuPrice2"><span class="blue">B社</span>：117,000円</p>
        <p class="hikakuPrice3">126,000<span class="small">円</span></p>
      </section>
      
      <section class="kaitori_cat">
          <ul>
              <li>
                  <a href="https://kaitorisatei.info/brandrevalue/blog/doburock"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/12/caf61dc6779ec6137c0ab58dfe3a550d.jpg" alt="どぶろっく"></a>
              </li>
          </ul>
      </section>      
      <section id="cat-point">
        <h3>高価買取のポイント</h3>
        <ul class="list-unstyled">
                    <li>
                        <p class="pt_tl">商品情報が明確だと査定がスムーズ</p>
                        <p class="pt_tx">ブランド名、モデル名が明確だと査定がスピーディに出来、買取価格にもプラスに働きます。また、新作や人気モデル、人気ブランドであれば買取価格がプラスになります。</p>
                    </li>
                    <li>
                        <p class="pt_tl">数点まとめての査定だと
                            <br>キャンペーンで高価買取が可能</p>
                        <p class="pt_tx">数点まとめての査定依頼ですと、買取価格をプラスさせていただきます。</p>
                    </li>
                    <li>
                        <p class="pt_tl">品物の状態がよいほど
                            <br>高価買取が可能</p>
                        <p class="pt_tx">お品物の状態が良ければ良いほど、買取価格もプラスになります。</p>
                    </li>
                </ul>
        <p>もし購入時に同封されていた品質保証の証明書等がある場合は、査定時にお持ちいただくことをおすすめします。<br>
        スムーズかつ高額な査定をしやすくなります。その他、購入時のブランド製紙袋、箱、その他小物の換え部品等の付属品も一緒にお持ちいただくと、査定額がかなりアップすることがあります。<br>
流行のデザインの洋服などの場合、目安として発売後1年以内のお品を高額査定対象とさせていただいておりますので、お早目に査定を行われることをおすすめいたします。その他、品質タグがついた洋服や、事前にクリーニングしていただいた洋服も、査定額がアップします。</p>
      </section>
    <h3 class="mid">洋服・毛皮 一覧</h3>
    <ul class="mid_link">
      <li>
      <a href="<?php echo home_url('/cat/bag/chanel/chanel-apparel'); ?>">
      <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/fit/01.jpg" alt="シャネル買取">
      <p class="mid_ttl">シャネル</p>
      </a>
      </li>
      <li>
      <a href="<?php echo home_url('/cat/bag/gucci/gucci-apparel'); ?>">
      <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/fit/02.jpg" alt="グッチ買取">
      <p class="mid_ttl">グッチ</p>
      </a>
      </li>
      <li>
      <a href="<?php echo home_url('/cat/bag/hermes/hermes-apparel'); ?>">
      <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/fit/03.jpg" alt="エルメス買取">
      <p class="mid_ttl">エルメス</p>
      </a>
      </li>
      <li>
      <a href="<?php echo home_url('/cat/wallet/louisvuitton/louisvuitton-apparel'); ?>">
      <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/fit/04.jpg" alt="ルイヴィトン買取">
      <p class="mid_ttl">ルイヴィトン</p>
      </a>
      </li>
    </ul>
      
      <section id="lp-cat-jisseki">
        <h3 class="text-hide">買取実績</h3>
            
        <ul id="box-jisseki" class="list-unstyled clearfix">
          <?php
            foreach($resultLists as $list):
            // :: で分割
            $listItem = explode('::', $list);
          
          ?>
          <li class="box-4">
            <div class="title">
              <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/outfit/lp/<?php echo $listItem[0]; ?>" alt="">
              <p class="itemName"><?php echo $listItem[1]; ?><br><?php echo $listItem[2]; ?></p>
              <hr>
              <p>
                <span class="red">A社</span>：<?php echo $listItem[3]; ?>円<br>
                <span class="blue">B社</span>：<?php echo $listItem[4]; ?>円
              </p>
            </div>
            <div class="box-jisseki-cat">
              <h3>買取価格例</h3>
              <p class="price"><?php echo $listItem[5]; ?><span class="small">円</span></p>
            </div>
            <div class="sagaku">
              <p><span class="small">買取差額“最大”</span><?php echo $listItem[6]; ?>円</p>
            </div>
          </li>
          <?php endforeach; ?>
        </ul>
      <section id="list-brand" class="clearfix">
      <!--<h3>ブランドリスト</h3>
      <ul class="list-unstyled">
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/hermes'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch33.jpg" alt="エルメス"></dd>
                                <dt><span>Hermes</span>エルメス</dt>
                            </a>
                        </dl>
                    </li>
          <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/celine'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch29.jpg" alt="セリーヌ"></dd>
                                <dt><span>CELINE</span>セリーヌ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/wallet/louisvuitton'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch23.jpg" alt="ルイヴィトン"></dd>
                                <dt><span>LOUIS VUITTON</span>ルイヴィトン</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/chanel'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag01.jpg" alt="シャネル"></dd>
                                <dt><span>CHANEL</span>シャネル</dt>
                            </a>
                        </dl>
                    </li>         
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/gucci'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch25.jpg" alt="グッチ"></dd>
                                <dt><span>GUCCI</span>グッチ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/prada'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag02.jpg" alt="プラダ"></dd>
                                <dt><span>PRADA</span>プラダ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/fendi'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag03.jpg" alt="プラダ"></dd>
                                <dt><span>FENDI</span>フェンディ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/dior'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag04.jpg" alt="ディオール"></dd>
                                <dt><span>Dior</span>ディオール</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
            <a href="<?php echo home_url('/cat/bag/saint_laurent'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag05.jpg" alt="サンローラン"></dd>
                                <dt><span>Saint Laurent</span>サンローラン</dt>
                            </a>
                        </dl>
                    </li>
          <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/bottegaveneta'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch32.jpg" alt="ボッテガヴェネタ"></dd>
                                <dt><span>Bottegaveneta</span>ボッテガヴェネタ</dt>
                            </a>
                        </dl>
                    </li>
          <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/ferragamo'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch31.jpg" alt="フェラガモ"></dd>
                                <dt><span>Ferragamo</span>フェラガモ</dt>
                            </a>
                        </dl>
                    </li>         
      </ul>
      </section> -->      
        <div id="konna" class="konna_out">
          <p class="example1">■デザインが古い</p>
          <p class="example2">■破れている</p>
          <p class="example3">■毛が出ている</p>
          <p class="text">その他、状態についてのご質問はお気軽にご連絡下さい。</p>
        </div>
      </section>
      
      <section id="about_kaitori" class="clearfix">
        <?php
        // 買取について
        get_template_part('_widerange');
        get_template_part('_widerange2');
      ?>
      </section>
      

      
      <section>
          <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/bn_matomegai.png">
      </section>
      
      <?php
        // 買取基準
        get_template_part('_criterion');

        // NGアイテム
        get_template_part('_no_criterion');
        
        // カテゴリーリンク
        get_template_part('_category');
      ?>
      <section id="kaitori_genre">
        <h3 class="text-hide">その他買取可能なジャンル</h3>
        <p>【買取ジャンル】バッグ/ウエストポーチ/セカンドバック/トートバッグ/ビジネスバッグ/ボストンバッグ/クラッチバッグ/トランクケース/ショルダーバッグ/ポーチ/財布/カードケース/パスケース/キーケース/手帳/腕時計/ミュール/サンダル/ビジネスシューズ/パンプス/ブーツ/ペアリング/リング/ネックレス/ペンダント/ピアス/イアリング/ブローチ/ブレスレット/ライター/手袋/傘/ベルト/ペン/リストバンド/アンクレット/アクセサリー/サングラス/帽子/マフラー/ハンカチ/ネクタイ/ストール/スカーフ/バングル/カットソー/アンサンブル/ジャケット/コート/ブルゾン/ワンピース/ニット/シャツメンズ/毛皮/Tシャツ/キャミソール/タンクトップ/パーカー/ベスト/ポロシャツ/ジーンズ/スカート/スーツなど</p>
      </section>
      
      <?php
        // 買取方法
        get_template_part('_purchase');
      ?>
      
      <section id="user_voice">
        <h3>ご利用いただいたお客様の声</h3>
        
        <p class="user_voice_text1">ちょうど家の整理をしていたところ、家のポストにチラシが入っていたので、ブランドリバリューに電話してみました。今まで買取店を利用したことがなく、不安でしたがとても丁寧な電話の対応とブランドリバリューの豊富な取り扱い品目を魅力に感じ、出張買取を依頼することにしました。
絶対に売れないだろうと思った、動かない時計や古くて痛んだバッグ、壊れてしまった貴金属のアクセサリーなども高額で買い取っていただいて、とても満足しています。古紙幣や絵画、食器なども買い取っていただけるとのことでしたので、また家を整理するときにまとめて見てもらおうと思います。</p>

      <h4>鑑定士からのコメント</h4>
      <div class="clearfix">
        <p class="user_voice_text2">家の整理をしているが、何を買い取ってもらえるか分からないから一回見に来て欲しいとのことでご連絡いただきました。
買取店が初めてで不安だったが、丁寧な対応に非常に安心しましたと笑顔でおっしゃって頂いたことを嬉しく思います。
物置にずっとしまっていた時計や、古いカバン、壊れてしまったアクセサリーなどもしっかりと価値を見極めて高額で提示させて頂きましたところ、お客様もこんなに高く買取してもらえるのかと驚いておりました。
これからも家の不用品を整理するときや物の価値判断ができないときは、すぐにお電話しますとおっしゃって頂きました。</p>
      
        <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/photo-user-voice.jpg" class="right">
      </div>
      </section>

<section>
  <div class="cont_widerange">
              <h4>洋服・毛皮の買取取扱いブランド<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
              <ul>
                  <li>家のクローゼットを開けると、もう着ることがなくなった洋服・毛皮がクローゼットのほとんどの空間を占めていたということはありませんか？なかには、引越しの時に洋服を整理したら、昔購入した高価な毛皮やブランド服が大量に出てきたという人もいるかもしれません。特に、高額な定価でも奮発して購入した有名ブランドや人気ブランドのジャケットやアウター、高級ブランドの毛皮になると簡単に捨てることもできず処分に困っている人も多いはずです。そんな時は、ブランド買取を専門にしている買取店や古着買取を経験している鑑定士やスタッフがいる買取店で売却するのが、最もスマートでお得な処分方法ではないでしょうか？<br>
しかし気になるのが、買取対象になる取扱いブランドはどんなブランドなのかという点でしょう。一般的に、洋服・毛皮といったアパレル品は、国内外を問わずブランド、ジャンルも幅広く、さらに販売されるアイテム品目も多いため適切な査定をされるのが難しいとされていますが、古着買取などを専門していた経験を持つ買取店であればアパレルブランドの知識を豊富にあるので取扱いブランドも比較的多く、洋服・毛皮の状態やブランドの応じて適正に査定してもらえます。<br>
洋服買取で取扱いブランドとして挙げられるのは、やはり人気の高いファッションブランドが多いようです。モードなデザインとその高い品質から3大ブランドと呼ばれるルイヴィトン、シャネル、エルメスをはじめ、グッチやマックスマーラ、プラダ、バーバリー、クロエ、フォクシーなど安定した人気のあるブランドであればブランド名やコレクションが明確なので、買取では高額になる場合が多いです。また、毛皮のブランドでは、サガ・ファー、エンバ、アメリカンレジェンド、カナダマジェスティックなど世界の有名ブランドが取扱いブランドとしている買取店も多いです。<br>
もし「このブランドは買取してもらえるのかな」と思ったら、まずは古着買取もしているブランド買取店に売却したい洋服のブランドの取扱いがあるのか問い合わせてみると良いでしょう。<br><br>
</li>
              </ul>
          </div>
  <div class="cont_widerange">
              <h4>洋服・毛皮を高額査定してもらうコツとは<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
              <ul>
                  <li>家のクローゼットの大半のスペースを埋めてしまっている毛皮やアウターやコート、スーツなど洋服が高額査定で高く買取ってくれるのなら、ちょっとした臨時のお小遣いにもなりますし新しい洋服を買う軍資金にもなりますよね。だからこそ、せっかく売却するなら、高額査定になるポイントをしっかり押さえて高額買取してもらいましょう。<br>
まず、洋服や毛皮を査定に出す前に、洋服の状態をしっかり確認しましょう。汗ジミやホコリ、汚れがついたままになっていたり、シワが付いたままの洋服は、高額査定がつきにくくなります。そのため、査定前に一度クリーニングに出したり適切な温度で服を傷めないようにアイロンを掛けたり、見た目を整えておくことが大切です。また毛皮の場合は、毛並の艶や品質を保つために定期的な手入れやメンテナンスが必要となるので、保管している間はお手入れをしっかりしておくようにしましょう。<br>
洋服・毛皮にブランドタグや品質タグがきれいな状態で付いているかも査定の前に一度確認しておきましょう。タグにはブランド名はもちろん素材、コレクション名、時期などが記載されており、大切な査定情報になっています。また毛皮には、購入時の品質保証書や証明書が発行されるので、残っている場合は査定時に一緒に毛皮と一緒に出すと査定が高くなることがあります。<br>
また、換えボタンやベルトといった小物の付属品はもちろん、高級ブランドの洋服になると購入時にブランドハンガーやカバー、箱、ショッパーバッグなどの付属品が付いてきます。小さな小物や付属品もしっかりと揃えて査定に出すと査定額がアップして、思ったよりも高額査定になる場合があります。<br>
最後に洋服・毛皮には流行があるため、できるだけそのコレクションの発売後はシーズンの変わり目などのタイミングを見ながら早めに査定に出すようにしましょう。特に流行デザインの場合は中古市場でも需要度が変わりやすいため、発売後1年以内を目安に査定に出すと高額になる場合もあります。「もう着ないな」と思ったら早めに査定に出すようにしましょう。<br><br>
</li>
              </ul>
          </div>
  <div class="cont_widerange">
              <h4>洋服・毛皮の便利な買取方法<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
              <ul>
                  <li>家のクローゼットを整理したり、引越しの準備で洋服を整理したら、思った以上に大量の洋服・毛皮が出てきたという時、どうやって大量の洋服を処分したらいいか悩む人もいるでしょう。もういらないと思った洋服・毛皮の中に人気ブランドのコートやアウター、ジャケットやスーツ、毛皮などあると、そのまま捨てるのもなんだか気が進みませんよね。そんな時は、ブランド服を取扱っている買取店で高額で買取ってもらいましょう。<br>
洋服・毛皮の買取方法には、主に3つの買取方法があります。洋服・毛皮を買取店に直接持ち込んで査定買取してもらう「店頭買取」、買取店の査定スタッフが自宅まで来てくれてその場で査定買取をしてくれる「出張買取」、そして洋服・毛皮を箱に詰めて宅配で買取店に送る「宅配買取」があります。<br>
店頭買取は店頭で査定してもらうと即現金で買取ってもらうことができるため、自分の都合の良い時に買取をしてもらえることが最大のメリットです。しかし、買取店の受付時間中に店頭まで行くことができる、買取ってもらいたい洋服・毛皮の量が自分で運ぶことができる程度といったデメリットもあります。<br>
一方、出張買取は店頭まで聞く手間が省けるため、売却したい洋服・毛皮が多い人におすすめの買取方法です。自宅で査定後はほとんどの場合即現金で買取ってもらうことができます。さらに、出張買取では買取店のスタッフが買取った洋服・毛皮を全てその場で引き取ってくれるので、一気に処分することができます。ただし、買取店の都合によって、希望する日時に出張に来てくれるとは限りません。<br>
そして、なかなか買取店まで行けないという忙しい人には、宅配買取が最も便利な買取方法になります。買取ってもらいたい洋服・毛皮を箱に詰めて送料無料で送るだけなので、時間に縛られずに査定、買取をしてもらうことができます。ただし発送から買取金額の入金までに少し時間がかかることが唯一のデメリットと言えるでしょう。<br>
持ち運びにかさばりやすい洋服・毛皮だからこそ、自分に合った便利な買取方法で買取ってもらうと良いでしょう。
</li>
              </ul>
          </div>
</section>
      
    </main><!-- #main -->
  </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
