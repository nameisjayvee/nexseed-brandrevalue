<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BRING
 */

// リダイレクト設定
$url = $_SERVER["REQUEST_URI"];
$url = str_replace(home_url(), "", $url);
//var_dump($url);

$ua = $_SERVER['HTTP_USER_AGENT'];
//var_dump($ua);

if(is_page() || is_front_page()) {
  // 固定ページのみridairekuto
if(
  (strpos($ua, 'Android') !== false) &&
  (strpos($ua, 'Mobile') !== false) ||
  (strpos($ua, 'iPhone') !== false) ||
  (strpos($ua, 'Windows Phone') !== false)) {
    // スマートフォンからアクセスされた場合
    header("Location: /sp".$url);
    exit();

} elseif (
  (strpos($ua, 'Android') !== false) ||
  (strpos($ua, 'iPad') !== false)) {
    // タブレットからアクセスされた場合
    header("Location: /sp".$url);
    exit();

} elseif (
  (strpos($ua, 'DoCoMo') !== false) ||
  (strpos($ua, 'KDDI') !== false) ||
  (strpos($ua, 'SoftBank') !== false) ||
  (strpos($ua, 'Vodafone') !== false) ||
  (strpos($ua, 'J-PHONE') !== false)) {
    // 携帯からアクセスされた場合
    header("Location: /sp".$url);
    exit();

} else {
    // その他（PC）からアクセスされた場合はなにもしない
}
}

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/slide.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/reset.css">
<?php get_template_part('template-parts/gtm', 'head') ?>
<?php wp_head(); ?>

<!-- modal -->
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jquery.leanModal.min.js"></script>
<script type="text/javascript">
$(function() {
    $( 'a[rel*=leanModal]').leanModal({
        top: 50,                     // モーダルウィンドウの縦位置を指定
        overlay : 0.5,               // 背面の透明度
        closeButton:".modal_close"  // 閉じるボタンのCSS classを指定
    });
});
</script>

</head>

<body <?php body_class(); ?>>
<?php get_template_part('template-parts/gtm', 'body') ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'bring' ); ?></a>
        <div class="head_tab">
            <ul>
                <li class="tab01">時計・ブランド高額買取</li>
                <a href="http://kaitorisatei.info/"><li class="tab02">メンズブランド古着買取</li></a>

            </ul>
        </div>

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding container">
			<h1><a href="<?php echo home_url(''); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/logo.png" alt="高額ブランド買取専門サイトBRAND REVALUE"></a></h1>
			<div class="headerAction">
  			<a href="tel:0120-970-060" class="adgain_tel"><p class="lower">
            <span>携帯･スマホからもOK!土日･祝日も承ります</span><br>
            <span id="phone_number_holder">0120-970-060</span><br>
            <span>【受付時間】　11:00~21:00 ※年中無休</span>
          </p></a>
  			<p><a href="<?php echo home_url('line'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/action-line.png" alt="LINE査定"></a></p>
  			<p><a href="<?php echo home_url('contact'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/action-app.png"　alt="今すぐ査定申込"></a></p>
			</div>
		</div><!-- .site-branding -->

		<nav id="gNav">
			<ul class="list-inline container">
  			   <li><a href="<?php echo home_url('service'); ?>">サービスについて<span class="mincho">SERVICE</span></a></li><!--
  			--><li><a href="<?php echo home_url('purchase'); ?>">買取方法<span class="mincho">PURCHASE</span></a></li><!--
  			--><li><a href="<?php echo home_url('brand'); ?>">取扱ブランド<span class="mincho">BRAND</span></a></li><!--
  			--><li><a href="<?php echo home_url('cat'); ?>">取扱カテゴリ<span class="mincho">CATEGORY</span></a></li><!--
  			--><li><a href="<?php echo home_url('introduction'); ?>">実績紹介<span class="mincho">INTRODUCTION</span></a></li><!--
  			--><li><a href="<?php echo home_url('voice'); ?>">お客様の声<span class="mincho">VOICE</span></a></li>
			</ul>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<?php if(is_front_page() && is_home()): ?>


    <div class="wideslider">
    <ul>
        <li><img src="<?php echo get_s3_template_directory_uri() ?>/img/slider/slide01.png" alt="銀座店　NEWOPEN"></li>
        <li><a href="<?php echo home_url('about-purchase/syutchou'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/slider/slide02.png" alt="出張買取"></a></li>
        <li><a href="<?php echo home_url('about-purchase/tentou'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/slider/slide03.png" alt="店頭買取"></a></li>
        <li><a href="<?php echo home_url('about-purchase/takuhai'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/slider/slide04.png" alt="宅配買取"></a></li>
        <li><a href="<?php echo home_url('about-purchase/tebura'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/slider/slide05.png" alt="店頭買取"></a></li>
        <li><a href="<?php echo home_url('service'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/slider/slide06.png" alt="“業界最高峰”の買取価格"></a></li>
    </ul>
    </div>

  <?php else: ?>
    <!-- パンくず -->
    <div class="breadcrumbs">
      <?php if(function_exists('bcn_display')) {
        bcn_display();
      }?>
    </div>
  <?php endif; ?>
