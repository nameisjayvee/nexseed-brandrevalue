<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>

	<div id="primary" class="cat-page content-area">
		<main id="main" class="site-main" role="main">
      <section id="mainVisual" style="background:url(<?php echo get_template_directory_uri() ?>/img/mv/voice.png)">
        <h2 class="text-hide">お客様の声</h2>
      </section>
      
      <section id="catchcopy">
      </section>
            <section class="voice">
      <ul>
      <li>
      <div class="voice_tl wacth">
      <h4>愛着があるものなので高値で買い取りしてくれる店を探してました。</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">時計を扱っている買取店は様々ある中で、ブランドリバリューはどの店よりも高値で時計を買取してくれるとのことだったので、査定をお願いしてみました。店まではタクシー代が出るとのことだったので、東京駅からタクシーで店まで伺いました。清潔感のあるお店で、査定は個室で行われました。査定にはそれほど時間もかからず、内容まで丁寧に説明してくれました。接客してくれたお店のスタッフも清潔感があり良かったです。価格も想定していた値段より高値で買い取りしてもらったので、とても満足して時計を売却することができました。ありがとうございました。</p>
      
      </div>
      
      </li>
            <li>
      <div class="voice_tl wacth">
      <h4>安心して買取してくれるお店を探してました</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">私は時計が好きで何本も所有しているのですが、つい最近持っている時計と同じものをプレゼントで貰いました。2つ同じものを所有していても仕方ないので、自分で購入したものをブランドリバリューで買い取り査定してもらいました。来店してすぐに査定してもらい、長年使用していたものなのですが、新しい時計を買える位の値段を付けてもらえました。予約をして為、予約特典としてプラス1000円もいただいて何だか得をした気分になりました。安心して納得できる査定価格を出してもらえるお店です。また利用したいと思いました。</p>
      
      </div>
      
      </li>
            <li>
      <div class="voice_tl wacth">
      <h4>手ばなすのを悩んでいたのですが、高値提示で売却を決断しました</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">新しいモデルの時計の購入を考えていて、あまり使わなくなった時計を売却して新しく購入する為の足しにしたいと思っていました。インターネットで調べて、どこよりも高い値段で買い取りをしてくれるとのことで、あまり使わない時計を3つ持って店に伺いました。その中でパテック・フィリップコンプリケーションという時計の査定評価が良く、一番納得のいく買取値段だったので、その場で決断してパテック・フィリップコンプリケーションだけ売却することを決めました。査定は早かったのですが、説明は丁寧で分かりやすい印象を受けました。これで新しいモデルの時計が買えるので今から楽しみです。他のアクセサリーの買取も他の店の査定より高値で買い取りをしてくれるということだったので、また利用したいと思いました。</p>
      
      </div>
      
      </li>
<li>
      <div class="voice_tl wacth">
      <h4>時計を買取してくれるお店の多さにビックリ</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">オメガシーマスターアクアテラという時計を売却したいと思い、インターネットで時計買い取りの専門店や、時計査定店を調べたのですが、思いのほか多くて店の選定に時間がかかりました。そんな中で、BRANDREVALUEで買い取り査定をお願いしたのは、2点ポイントがあります。1つは予約買取は1000円プラスで支払いしてくれるという点。もう一つはどの買取査定店よりも高値で買い取りをしてくれるという2点です。どうしてもオメガシーマスターアクアテラを売却する必要があったので、実際査定してもらって自分の希望より高く買い取ってもらえたので嬉しかったです。その場ですぐに査定してくれて現金払いしてくれるので、急にお金が必要だった僕には助かりました。ありがとうございます。</p>
      
      </div>
      
      </li>
<li>
      <div class="voice_tl wacth">
      <h4>安心で評価の高い買取専門店</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">買取専門店は沢山お店がありますが、その中でBRANDREVALUEさんは評価が良く、安心して買取がお願いできるお店です。理由は取り扱いの品数が多く、査定時間はスピーディーなのに、査定の説明は細かく丁寧に分かりやすくしてくれるのでありがたいです。新しいモデルを常に身に着けたい人にとっては、売る時のことを考えて使用しているので、査定ポイントを教えてもらえると今後の参考にできてとても助かります。また、どんなお店よりも高額買取してくれるのも評価が高いポイントで、安心して売却できるのもありがたいです。</p>
      
      </div>
      </li>
      <li>
      <div class="voice_tl bag">
      <h4>高値買取で満足できました</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">同じエルメスのバーキンを2つ所有していたのですが、他のバッグが欲しくなったので、エルメスバーキンを買取してもらうことにしました。高値で買取してくれると評判のブランドリバリューさんにお願いすることにしました。ブランドリバリューさんにお願いした理由は1つ『どこよりも高く買取』してくれるとのことでしたので安心して査定を依頼することができました。エルメスのバーキンは高価ですし、とても大事にしていたものなのですが、どうしても他のバッグが欲しくなってしまって持ち込みで査定していただきました。想像以上に高値をつけていだだき、その後に欲しかった新しいブランドバッグも手に入ったので、大満足の一日でした。</p>
      
      </div>
      </li>
            <li>
      <div class="voice_tl bag">
      <h4>セリーヌバッグを買取してもらいました</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">セリーヌは老舗のブランドなのですが、昔から好きで愛用しているブランドの一つです。新しいブランドラインが出ると毎回売却をして新しいセリーヌを買いに行くのですが、今回もブランドリバリューさんで買取査定してもらい新しいシーズン限定のセリーヌのバックを購入しました。どこよりも高く査定買取していただけるお店なので安心して査定をお願いできます。今回も自分の希望していた金額を上回る査定金額を提示していただけたので、納得して売却できました。もう一つブランドリバリューさんのいいところは、現金ですぐ買取をしてくれるところです。次回もまたお願いします。</p>
      
      </div>
      </li>
            <li>
      <div class="voice_tl bag">
      <h4>ヴィトンを売るならブランドリバリューがオススメ！</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">いつの間にか大切にしていたヴィトンのバッグにカビが生えてダメになっていたことがきっかけで、クローゼットの掃除や整理を心がけるようになりました。そうすると毎回決まってこれは使わないとか、使ったことがないとかが出てくるので、それらは全てブランドリバリューにお願いして出張買い取り査定をしてもらっています。ヴィトンは人気のあるブランドなので、私が希望する買取価格よりいつも上回る高額買取をしてくれます。そして出張買取をしてくれるというのもオススメポイントで、大きな袋を抱えて街を歩くのは気が引けるので、出張買取は重宝しています。今後ともよろしくお願いします！</p>
      
      </div>
      </li>
            <li>
      <div class="voice_tl bag">
      <h4>シャネラーの私がシャネルグッズを唯一買取お願いしてます。</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">シャネラーの私は新作が出るたびにシャネルのグッズを購入します。買うばかりでシャネルのグッズは増えていく一方でした。使わないものや、使ってないシャネルのグッズが沢山あったので、どうにかならないかとインターネットで検索したところブランドリバリューさんを見つけて、早速電話しました。品数が多いので宅配査定でお願いしたところ、後日宅配セットが届けられその中に入るだけとりあえず入れて、ブランドリバリューさんへ送り返しました。この際の送料も負担してくれるので安心でした。メールで丁寧に査定金額の返信が送られてきました。きちんとした査定評価をいただいたので、納得してシャネルとお別れできました。まだまだあるので、これからも利用したいです。</p>
      
      </div>
      </li>
                  <li>
      <div class="voice_tl bag">
      <h4>プラダを売りたい時に見つけた査定してくれるお店</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">つい最近までプラダが大好きで色々集めていたのですが、あることがきっかけで違うブランドにハマってしまって、今まで持っていたプラダがいらなくなりました。友達にあげるのも何か上から目線で嫌だったので、ブランドリバリューさんに出張買取お願いしました。全部で45点位残っていて、他は違う査定会社にお願いして売却済だったのですが、ブランドリバリューは査定早くて、説明も分かりやすく、高値で買取してくれることが分かりました。始めからブランドリバリューに買取査定お願いすればよかったと思う位評価高いです！アクセサリーも買取しているので、次はアクセサリーも査定してもらってブランドリバリューに売りたいと思っております。またよろしくお願いします。</p>
      
      </div>
      </li>
      </ul>
      <ul class="voice_pager">
      <li><a href="<?php echo home_url('voice'); ?>">1</a></li>
      <li class="ac"><a href="#">2</a></li>
      <li><a href="<?php echo home_url('voice3'); ?>">3</a></li>
      </ul>
      </section>
      
      <?php
        // 買取基準
        get_template_part('_criterion');
        
        // アクションポイント
        get_template_part('_action');
        
        // 買取方法
        get_template_part('_purchase');
        
        // 店舗案内
        get_template_part('_shopinfo');
      ?>
      
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
