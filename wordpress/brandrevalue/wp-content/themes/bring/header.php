<?php
    /**
     * The header for our theme.
     *
     * This is the template that displays all of the <head> section and everything up until <div id="content">
     *
     * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
     *
     * @package BRING
     */

    // リダイレクト設定
    $url = $_SERVER[ "REQUEST_URI" ];
    $url = str_replace( home_url(), "", $url );
    //var_dump($url);

    $ua = $_SERVER[ 'HTTP_USER_AGENT' ];
    //var_dump($ua);

    if ( is_page() || is_front_page() ) {
        // 固定ページのみridairekuto
        if (
            ( strpos( $ua, 'Android' ) !== false ) &&
            ( strpos( $ua, 'Mobile' ) !== false ) ||
            ( strpos( $ua, 'iPhone' ) !== false ) ||
            ( strpos( $ua, 'Windows Phone' ) !== false ) ) {
            // スマートフォンからアクセスされた場合
            header( "Location: /sp" . $url );
            exit();

        } elseif (
            ( strpos( $ua, 'Android' ) !== false ) ||
            ( strpos( $ua, 'iPad' ) !== false ) ) {
            // タブレットからアクセスされた場合
            header( "Location: /sp" . $url );
            exit();

        } elseif (
            ( strpos( $ua, 'DoCoMo' ) !== false ) ||
            ( strpos( $ua, 'KDDI' ) !== false ) ||
            ( strpos( $ua, 'SoftBank' ) !== false ) ||
            ( strpos( $ua, 'Vodafone' ) !== false ) ||
            ( strpos( $ua, 'J-PHONE' ) !== false ) ) {
            // 携帯からアクセスされた場合
            header( "Location: /sp" . $url );
            exit();

        } else {
            // その他（PC）からアクセスされた場合はなにもしない
        }
    }

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <link rel="icon" href="<?php echo get_template_directory_uri() ?>/img/favicon.ico">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/slide.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/reset.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/kakakuhyou.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/slick-theme.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/slick.css">
        <link href="<?php echo get_template_directory_uri(); ?>/css/customstyle.css" rel="stylesheet ">
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
        <!--追加アニメーションオプション　-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome-animation/0.0.10/font-awesome-animation.css" type="text/css" media="all" />
        <?php wp_head(); ?>

        <!-- modal -->
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jquery.leanModal.min.js"></script>
        <script type="text/javascript">
            $(function() {
                $('a[rel*=leanModal]').leanModal({
                    top: 50, // モーダルウィンドウの縦位置を指定
                    overlay: 0.5, // 背面の透明度
                    closeButton: ".modal_close" // 閉じるボタンのCSS classを指定
                });
            });

        </script>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/js/dropdown.js"></script>
        <script src="//statics.a8.net/a8sales/a8sales.js"></script>
        <?php get_template_part('template-parts/gtm', 'head') ?>
    </head>

    <body <?php body_class(); ?>>
        <?php get_template_part('template-parts/gtm', 'body') ?>
        <div id="page" class="site">
            <a class="skip-link screen-reader-text" href="#content">
                <?php esc_html_e( 'Skip to content', 'bring' ); ?>
            </a>
            <div class="head_tab">
                <ul>
                    <li class="tab01">時計・ブランド高額買取</li>
                    <a href="//kaitorisatei.info/">
                        <li class="tab02">メンズブランド古着買取</li>
                    </a>
                    <li class="tab03">
                        <h1>
                            <?php wp_title(); ?>
                        </h1>
                    </li>
                </ul>
            </div>
            <header id="masthead" class="site-header" role="banner">
                <div class="site-branding container">
                    <div class="logo"><a href="<?php echo home_url(''); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/logo.png" alt="高額ブランド買取専門サイトBRAND REVALUE"></a> </div>
                    <div class="headerAction">
                        <a href="tel:0120-970-060" class="adgain_tel">
                            <p class="lower"> <span>携帯･スマホからもOK!土日･祝日も承ります</span><br>
                                <span id="phone_number_holder">0120-970-060</span><br>
                                <span>【受付時間】　11:00~21:00 ※年中無休</span> </p>
                        </a>
                        <p><a href="<?php echo home_url('line'); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/action-line.png" alt="LINE査定"></a> </p>
                        <p><a href="<?php echo home_url('customer'); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/action-app.png"　alt="今すぐ査定申込"></a> </p>
                    </div>
                </div>
                <!-- .site-branding -->
                <!--<nav id="gNav">
                     <ul class="list-inline container">
                     <li><a href="<?php echo home_url('service'); ?>">サービスについて<span class="mincho">SERVICE</span></a></li>
                     <li><a href="<?php echo home_url('purchase'); ?>">買取方法<span class="mincho">PURCHASE</span></a></li>
                     <li><a href="<?php echo home_url('brand'); ?>">取扱ブランド<span class="mincho">BRAND</span></a></li>
                     <li><a href="<?php echo home_url('cat'); ?>">取扱カテゴリ<span class="mincho">CATEGORY</span></a></li>
                     <li><a href="<?php echo home_url('introduction'); ?>">実績紹介<span class="mincho">INTRODUCTION</span></a></li>
                     <li><a href="<?php echo home_url('voice'); ?>">お客様の声<span class="mincho">VOICE</span></a></li>
                     </ul>
                     </nav>-->
                <!-- #site-navigation -->
                <div id="gNav">
                    <nav id="dd-mega">
                        <ul class="main_menu">
                            <li><a href="<?php echo home_url('service'); ?>">サービスについて<span class="mincho">SERVICE</span></a> </li>
                            <li><a href="<?php echo home_url('purchase'); ?>">買取方法<span class="mincho">PURCHASE</span></a>
                                <ul class="drop">
                                    <li>
                                        <p class="tum"><img src="<?php echo get_template_directory_uri() ?>/img/nav/mgk01.png"> </p>
                                        <div class="link_area">
                                            <h3><a href="<?php echo home_url('/purchase/takuhai'); ?>">宅配買取</a></h3>
                                            <p>お品物を郵送するだけで、スピード査定・現金化が可能に！</p>
                                        </div>
                                    </li>
                                    <li>
                                        <p class="tum"><img src="<?php echo get_template_directory_uri() ?>/img/nav/mgk02.png"> </p>
                                        <div class="link_area">
                                            <h3><a href="<?php echo home_url('/purchase/syutchou'); ?>">出張買取</a></h3>
                                            <p>家から一歩も出ずにスピード査定が受けられる、楽ちんサービス！</p>
                                        </div>
                                    </li>
                                    <li>
                                        <p class="tum"><img src="<?php echo get_template_directory_uri() ?>/img/nav/mgk03.png"> </p>
                                        <div class="link_area">
                                            <h3><a href="<?php echo home_url('/purchase/tentou'); ?>">店頭買取</a></h3>
                                            <p>「即日で現金を手に入れたい」「顔を見て売りたい」そんな方にピッタリ！</p>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="<?php echo home_url('brand'); ?>">取扱ブランド<span class="mincho">BRAND</span></a> </li>
                            <li><a href="<?php echo home_url('cat'); ?>">取扱カテゴリ<span class="mincho">CATEGORY</span></a>
                                <ul class="drop">
                                    <?php get_template_part('_category'); ?>
                                </ul>
                            </li>
                            <li><a href="<?php echo home_url('introduction'); ?>">実績紹介<span class="mincho">INTRODUCTION</span></a> </li>
                            <li><a href="<?php echo home_url('voice'); ?>">お客様の声<span class="mincho">VOICE</span></a> </li>
                            <li><a href="<?php echo home_url('shop'); ?>">店舗案内<span class="mincho">SHOP</span></a>
                                <ul class="drop top_shopmenu">
                                    <li><a href="<?php echo home_url('ginza'); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/shop/shop01.png" alt="銀座店"/>
                                        <p>銀座店</p>
                                        <p>〒104-0061<br />
                                            東京都中央区銀座5-8-3 四谷学院ビル5階</p>
                                        <p>詳しくはこちら »</p>
                                    </a></li>
                                    <li><a href="<?php echo home_url('shibuya'); ?>"> <img src="<?php echo get_template_directory_uri() ?>/img/shop/shop02.png" alt="渋谷店"/>
                                        <p>渋谷店</p>
                                        <p>〒150-0041<br />
                                            東京都渋谷区神南1-12-16 和光ビル4階</p>
                                        <p>詳しくはこちら »</p>
                                    </a></li>
                                    <li><a href="<?php echo home_url('shinjuku'); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/shop/shop03.png" alt="新宿店"/><p>新宿店</p>
                                        <p>〒160-0022<br />東京都新宿区新宿3丁目31-1大伸第２ビル3階</p>
                                        <p>詳しくはこちら »</p>
                                    </a></li>
                                    <li>
                                        <a href="<?php echo home_url('shinsaibashi'); ?>">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/shop/shop_com.png" alt="心斎橋店"/><p>心斎橋店</p>
                                            <p>〒542-0081<br />大阪市中央区南船場4-4-8 クリエイティブ心斎橋8階</p>
                                            <p>詳しくはこちら »</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo home_url('center'); ?>">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/shop/shop05.png" alt="買取センター"/><p>買取センター</p>
                                            <p>〒150-0001<br />東京都渋谷区神宮前6-23-3<br>第9SYビル5階</p>
                                            <p>詳しくはこちら »</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!-- /#dd-mega -->
                    </nav>
                </div>
            </header>
            <!-- #masthead -->
            <?php if(is_front_page() && is_home()): ?>
                <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/slide-video.css">
                <script src="<?php echo get_template_directory_uri() ?>/js/slide-video.js"></script>
                <div class="wideslider">
                    <ul>

                        <li><a href="<?php echo home_url('customer'); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/slider/slide_satei.jpg" alt="出張買取"><p class="txt_y">全国対応！スピード無料査定で価値をお伝えします</p></a></li>
                        <li><a href="<?php echo home_url('cat/watch'); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/slider/slide_watch.jpg" alt="“業界最高峰”の買取価格"><p class="txt_y">高級腕時計・バッグ・貴金属・宝石の高額買取なら「ブランドリバリュー」へ</p></a></li>
                        <li><a href="<?php echo home_url('cat/bag'); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/slider/slide_bag.jpg" alt="“業界最高峰”の買取価格"><p class="txt_y">バッグ・財布・洋服・毛皮の高額買取なら「ブランドリバリュー」へ</p></a></li>
                        <li><a href="<?php echo home_url('cat/brandjewelery'); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/slider/slide_jewelry.jpg" alt="店頭買取"><p class="txt_y">ブランドジュエリー・宝石・ダイヤの高額買取なら「ブランドリバリュー」へ</p></a></li>
                        <li><a href="<?php echo home_url('purchase'); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/slider/slide_kaitori.jpg" alt="宅配買取"><p class="txt_y">お客様負担額0円！信頼と実績の鑑定士が買取査定致します</p></a></li>
                        <li><a href="<?php echo home_url('shinsaibashi'); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/slider/slide_shop_shinsaibashi.jpg" alt="心斎橋店　NEWOPEN"><p class="txt_y">関西エリア1号店！西日本でNo.1の高価買取を目指します</p></a></li>
                        <li><a href="<?php echo home_url('shop'); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/slider/slide_shop.jpg" alt="店舗情報"><p class="txt_y">各店アクセス良好！業界最高峰の買取価格は「ブランドリバリュー」</p></a></li>
                        <li><a href="javascript:void(0);" id="slide-video"><img src="<?php echo get_template_directory_uri() ?>/video/image_thumb.jpg" alt="動画サムネイル"><p class="txt_y">「ブランドリバリュー」の買取はなぜ安心か？動画でご確認ください</p></a></li>
                    </ul>
                </div>
                <div id="mv_bottom">
                    <ul>
                        <li><a href="<?php echo home_url('cat/bag/hermes'); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/mv/brand_mv_hermes.jpg" alt="HERMES(エルメス)"></a></li>
                        <li><a href="<?php echo home_url('cat/bag/chanel'); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/mv/brand_mv_chanel.jpg" alt="シャネル(CHANEL)"></a></li>
                        <li><a href="<?php echo home_url('cat/wallet/louisvuitton'); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/mv/brand_mv_louisvuitton.jpg" alt="ルイ・ヴィトン(LOUIS VUITON)"></a></li>
                    </ul>
                </div>
                <div id="slide-video-popup"><video controls preload="none"><source src="<?php echo get_template_directory_uri() ?>/video/image.mp4"> </video></div>
                <div id="slide-video-popup-filter"></div>
            <?php else: ?>
                <!-- パンくず -->
                <div class="breadcrumbs">
                    <?php if(function_exists('bcn_display')) {
                        bcn_display();
                        }?>
                </div>
            <?php endif; ?>
            <?php if(is_page( 'watch' )): ?>
                <section id="lp_mainVisual">
                    <div id="lp_mainVisual"> <img src="<?php echo get_template_directory_uri() ?>/img/mv/cat_watch_main.jpg" alt="あなたの使用していない時計をお売りください！ "> </div>
                </section>
            <?php endif; ?>
            <?php if(is_page( 'wallet' )): ?>
            <section id="lp_mainVisual">
                <div id="lp_mainVisual"> <img src="<?php echo get_template_directory_uri() ?>/img/mv/cat_wallet_main.jpg" alt="あなたの使用していない財布をお売りください！ "> </div>
            </section>
            <?php endif; ?>
            <?php if(is_page( 'shoes' )): ?>
                <section id="lp_mainVisual">
                    <div id="lp_mainVisual"> <img src="<?php echo get_template_directory_uri() ?>/img/mv/cat_shoes_main.jpg" alt="あなたの使用していない靴をお売りください！ "> </div>
                </section>
            <?php endif; ?>
            <?php if(is_page( 'bag' )): ?>
                <section id="lp_mainVisual">
                    <div id="lp_mainVisual"> <img src="<?php echo get_template_directory_uri() ?>/img/mv/cat_bag_main.jpg" alt="あなたの使用していないバッグをお売りください！ "> </div>
                </section>
            <?php endif; ?>
            <?php if(is_page( 'outfit' )): ?>
                <section id="lp_mainVisual">
                    <div id="lp_mainVisual"> <img src="<?php echo get_template_directory_uri() ?>/img/mv/cat_outfitlp_main.jpg" alt="あなたの使用していない洋服・毛皮をお売りください！ "> </div>
                </section>
            <?php endif; ?>
            <?php if(is_page( 'gem' )): ?>
                <section id="lp_mainVisual">
                    <div id="lp_mainVisual"> <img src="<?php echo get_template_directory_uri() ?>/img/mv/cat_gem_main.jpg" alt="あなたの使用していない宝石をお売りください！ "> </div>
                </section>
            <?php endif; ?>
            <?php if(is_page( 'diamond' )): ?>
                <section id="lp_mainVisual">
                    <div id="lp_mainVisual"> <img src="<?php echo get_template_directory_uri() ?>/img/mv/cat_diamond_main.jpg" alt="あなたの使用していないダイアモンドをお売りください！ "> </div>
                </section>
            <?php endif; ?>
            <?php if(is_page( 'gold' )): ?>
                <section id="lp_mainVisual">
                    <div id="lp_mainVisual"> <img src="<?php echo get_template_directory_uri() ?>/img/mv/cat_gold_main.jpg" alt="あなたの使用していないゴールド・プラチナをお売りください！ "> </div>
                </section>
            <?php endif; ?>
            <?php if(is_page( 'antique' )): ?>
                <section id="lp_mainVisual">
                    <div id="lp_mainVisual"> <img src="<?php echo get_template_directory_uri() ?>/img/mv/cat_antique_main.jpg" alt="あなたの使用していない骨董品をお売り下さい"> </div>
                </section>
            <?php endif; ?>
            <?php if(is_page( 'liquor' )): ?>
                <section id="lp_mainVisual">
                    <div id="lp_mainVisual"> <img src="<?php echo get_template_directory_uri() ?>/img/mv/cat_liquor_main.png" alt="お酒の買取はお任せ下さい。"> </div>
                </section>
            <?php endif; ?>
            <?php if(is_page( 'kimono' )): ?>
                <section id="lp_mainVisual">
                    <div id="lp_mainVisual"> <img src="<?php echo get_template_directory_uri() ?>/img/lp/kimono/mv.jpg" alt="着物の買取はお任せ下さい。"> </div>
                </section>
            <?php endif; ?>
                <?php
                    //if(is_page( 'rolexlp' )):
                    //if(is_page_template('theme-lp.php')):
                    $lpMainVis = SCF::get( 'lp-mainVis' );
                    if ( !empty( $lpMainVis ) ):
                ?>
                    <section id="lp_mainVisual">
                        <div id="lp_mainVisual">
                            <!--<img src="<?php echo get_template_directory_uri() ?>/img/mv/rolex_main.png" alt="あなたの使用していないロレックスをお売り下さい">-->
                            <?php echo wp_get_attachment_image($lpMainVis, 'full'); ?> </div>
                    </section>
                <?php endif; ?>

                <div id="content" class="site-content container">
