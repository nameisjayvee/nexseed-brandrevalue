<section>
  <div class="cont_widerange">
      <h4><img src="<?php echo get_s3_template_directory_uri() ?>/img/top/allbrand_ttl.png" alt="ブランドバッグ、時計、アクセサリーなど、幅広いお品物を買い取ります。"></h4>
      <ul>
          <li class="left_tx">時計、アクセサリー、バッグ、財布、アクセサリー、貴金属（金・ダイヤモンド等）、毛皮、財布、靴、衣類など――<br>
                    BRANDREVALUE(ブランドリバリュー)では幅広いお品物を、どこよりも高い査定額にて買い取らせていただきます。
                    幅広いブランドの買取りに対応しており、エルメス、ルイ・ヴィトン、プラダ、グッチ、オメガ、ロレックスといった代表的なブランドはもちろん、それ以外の価値ある品も積極的に取り扱っております。
                    古いお品物や傷ついたお品物も高額に査定いたしますし、1000万円以上の超高級品の買取も可能。<br>
          「自宅に使わなくなったブランド品があるけれど、愛着もある高価な品物だから信頼できるお店でなるべく高く売りたい」
          そんな慎重で欲張りな方にも、BRAND REVALUEの買取りサービスは必ずご納得いただけると思います。</li>
          <li class="right_ph"><img src="<?php echo get_s3_template_directory_uri() ?>/img/top/allbrand_ph.png"></li>
      </ul>
  </div>
</section>