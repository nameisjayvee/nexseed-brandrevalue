<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>

	<div id="primary" class="cat-page content-area">
		<main id="main" class="site-main" role="main">
      <section id="mainVisual" style="background:url(<?php echo get_template_directory_uri() ?>/img/mv/voice.png)">
        <h2 class="text-hide">お客様の声</h2>
      </section>
      
      <section id="catchcopy">
      </section>
            <section class="voice">
      <ul>
      <li>
      <div class="voice_tl gem">
      <h4>指輪を思ったより高額買取してもらえました！</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">使わなくなった指輪をネットでブランドリバリューを探して買取してもらいました。前もってネット予約して、店頭買取してもらいました。予約すると特典で査定額1点ごとに1000円UPするみたいです。更に他の店舗の査定額より必ず高く買い取ってくれるのがとても魅力的です。今まで愛用していた指輪なので、買い取ってくれるのであれば少しでも高い値段で買い取ってもらえる方が嬉しいのでブランドリバリューさんにお願いして良かったです。店内は個室になっていて、清潔感のある個室でプライバシー管理もされていました。指輪などのジュエリーは大好きで沢山持っているので、使わないものは買取してもらって新しいものを買いたいと思います。</p>
      
      </div>
      
      </li>
            <li>
      <div class="voice_tl gem">
      <h4>壊れたアクセサリーも買い取ってもらえるなんて知りませんでした！</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">いつもアクセサリーは壊れてしまうと直さずにそのまま放置してしまっていました。引っ越しをするのに部屋を掃除していたら壊れたネックレスがいくつも出てきて、結局12点くらいは全て壊れていました。処分しようと思って壊れているアクセサリーでも買い取ってもらえるお店を検索していたら、ブランドリバリューをみつけました。先にお店へ郵送しておいて、引っ越しが完了してからお店へ伺いました。これは買い取ってもらえないかな？というタイプのネックレスもブランド品だったので買取していただけるということで、結局全部値段がついて買取してもらえました！そのお金で引っ越しのお祝いのディナーは美味しくいただけました。</p>
      
      </div>
      
      </li>
            <li>
      <div class="voice_tl gem">
      <h4>安心して買取をしてくれるお店が良かったのでここにしました。</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">買取査定をしてもらいたいユーザーからすると、買取査定してもらうお店の基準は「安心」、「高額査定」、「高評価」、「丁寧」、「親切」なサービスを提供してくれるお店です。やっぱり自分が欲しくて使用していたものなので、気持ちの良いお店にいつもお願いしています。ブランドリバリューで買い取りしてもらったのは、来店してすぐに買取可能だったことと、買取価格も他店より上の金額で買い取ってくれるということからでした。様々なブランド品やジュエリーやアクセサリーなどの買取を取り扱っているのも、商品知識が豊富な証拠だと感じました。ブランドリバリューは安心して次回もまた利用したいと思えるお店でした。お友達にも紹介したいと思います。</p>
      
      </div>
      
      </li>
<li>
      <div class="voice_tl gem">
      <h4>孫の出産祝いにプラチナインゴットを現金化</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">娘が赤ん坊を出産したのでお祝いに何かプレゼントしてあげようと思いましたが、これからのことを考えるとお金がいいかと思い、投資代わりに買っていたプラチナインゴットを売ろうと思いました。買取をしてもらうのは始めてだったので、店に持って行ってすぐに個室に通されて、査定をしていただきました。査定というのは時間がかかるのかと思っていましたが、時間もかからなくすぐその場で現金で買い取ってくれたのでびっくりしました。こんなに早く査定ができるのは便利だと感じました。これで出産祝いが渡せるので良かったです。アクセサリーや宝石も買い取ってくれるみたいなので、また機会があれば利用したいです。</p>
      
      </div>
      
      </li>
<li>
      <div class="voice_tl gem">
      <h4>金のインゴットを買い取っていただきました。</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">投資用に何年か前に金を購入していました。買いたいものができたので、金を売ろうと思いブランドリバリューを利用しました。好きなものを購入するのと、別の投資用にお金は使う予定です。BRANDREVALUEは他の買取専門店より高値で買い取ってくれるということだったので、事前に他の買取専門店へ出向いて査定してもらった上で、BRANDREVALUEへ予約来店しました。予約来店すると1品1000円プラスの査定をしてくれるのもBRANDREVALUEで査定をお願いしたくなるポイントです。査定額も他の店舗より高く買い取ってくれたので、納得できる買取りをしてもらえてよかったです。</p>
      
      </div>
      </li>
      <li>
      <div class="voice_tl neck">
      <h4>BRANDREVALUEで査定してもらってよかった点</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">色付きの宝石は購入した時の価格より、売却する時の価格はほとんど値段が付かないと良く聞いていたので売るかそのまま持っているか悩んでいました。でも一度査定はしてもらおうとBRANDREVALUEでエメラルドの指輪をみてもらいました。購入した価格とは全く違いますが、しっかり値段を付けてくれたのでそのまま買い取ってもらうことにしました。エメラルドの石と指輪の台になっている部分にそれぞれ値段がついたようなので考えていたより高値で納得のいく買取だったと思います。デザインが古いものでも買取をしてくれる様なので、次回も使わなくなった宝石を手放す時はBRANDREVALUEで買い取り査定お願いしたいと思います。</p>
      
      </div>
      </li>
            <li>
      <div class="voice_tl neck">
      <h4>真珠のネックレスをブランドリバリューで査定してもらいました</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">真珠は何かとフォーマルな服装には使い勝手がいいので、ついつい良いものがあったりすると買ってしまい、家にはいつのまにかかなりの数の真珠のネックレスがありました。持っていてもなかなか使わないものがあるので、思い切ってここは断捨離しました。1つだけ気に入ったものだけ残して、他のすべては査定してもらうことにしました。真珠のネックレスの査定相場も分からなかったのですが、きちんとお店の人が説明してくれて全て買取していただくことに決めました。何も分からない私でも、納得して査定買取してもらえてブランドリバリューさんでお願いしてよかったなと思います。また、機会があれば利用させていただきたいと思います。ありがとうございました。</p>
      
      </div>
      </li>
            <li>
      <div class="voice_tl neck">
      <h4>サファイアネックレスを高額買取してもらえました！</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">サファイヤのネックレスだったのですが、もう何年も前に購入したもので最近全く使わなくなってしまったので、査定をブランドリバリューさんにお願いしました。お店まで行く時間がもったいなかったので、今回は出張査定を利用しました。電話で相談して私の予定に合わせてもらって自宅まできていただきました。査定時間もあっという間で、即現金買取してくれました。時間をかけるのが好きではないので、電話で出張買取をお願いして買取までスムーズでとても丁寧でした。買い取っていただいたサファイアのネックレスは気に入っていたものだったので高額買取りしていただき大満足です。次回もまたお願いしたいと思います。</p>
      
      </div>
      </li>
            <li>
      <div class="voice_tl neck">
      <h4>ルビーを売りたくてブランドリバリュー</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">かなり前に買ってもらったルビーの指輪を使わなくなってしまったので、ブランドリバリューさんに買い取りをお願いしました。今回は時間がなかったので、「宅配買取」をお願いしました。WEBで宅配買取をお願いすると宅配キットが送られてきて、そのまま指輪を梱包して着払いで郵送しました。その後査定額がメールで送られてきました。希望していた金額よりも上の値段での買取をしてくれるとのことでありがたかったです。また、納得いかない時は返送してくれるので、安心して査定依頼ができる点も好感が持てます。</p>
      
      </div>
      </li>
                  <li>
      <div class="voice_tl neck">
      <h4>安心して買取査定してくれるお店見つけました</h4>
      </div>
      <div class="voice_tx">
      <p><img src="<?php echo get_template_directory_uri() ?>/img/voice_use.png" alt="ユーザー">買取査定はお店によって価格がまちまちで、売り手からすると値段相場の分からない宝石やアクセサリーは売却する時に不安がつきまといます。中には分からないまま値段を付けてくるお店もあるので気を付けています。ブランドリバリューさんはインターネットで探して評価がいいお店に買い取りをお願いしたいと探しました。安心して売ることができたのは、査定が早く説明が丁寧で分かりやすかったことです。説明をしてくれないお店もあるので、その点は本当に親切だったと感じました。買取してもらい、すぐに現金を受け取ることができたのも魅力の一つです。今後急きょ現金が必要になった時は、ブランドリバリューさんにお願いしようと思いました。ありがとうございました。</p>
      
      </div>
      </li>
      </ul>
      <ul class="voice_pager">
      <li class="ac"><a href="#">1</a></li>
      <li><a href="<?php echo home_url('voice2'); ?>">2</a></li>
      <li><a href="<?php echo home_url('voice3'); ?>">3</a></li>
      </ul>
      </section>
      
      <?php
        // 買取基準
        get_template_part('_criterion');
        
        // アクションポイント
        get_template_part('_action');
        
        // 買取方法
        get_template_part('_purchase');
        
        // 店舗案内
        get_template_part('_shopinfo');
      ?>
      
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
