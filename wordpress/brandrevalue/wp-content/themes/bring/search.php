<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package BRING
 */

get_header(); ?>
<style>
#lp_mainVisual{
	display:none;
}
	
</style>
<section id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<h3 class="obi_tl">『<?php echo get_search_query(); ?>』での検索結果一覧</h3>
		<?php if(have_posts()): ?>
		<?php while(have_posts()): the_post(); ?>
		<h4 class="frm_ttl"><a href="<?php the_permalink(); ?>">
			<?php the_title(); ?>
			</a></h4>
		
		<?php endwhile; ?>
		<?php else : ?>
		<p>検索条件にヒットした記事がありませんでした。</p>
		<?php endif; ?>
	</main>
	<!-- #main --> 
</section>
<!-- #primary -->

<?php
get_sidebar();
get_footer();

