<?php
    /*
       Template Name: 遺品テンプレート
     */
?>

<!DOCTYPE html>
<html lang="ja">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=640,user-scalable=yes" />
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/vendor/slider-pro/css/slider-pro.min.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/mement.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/validation.css">
        <link rel="icon" href="<?php echo get_template_directory_uri() ?>/img/favicon.ico">

        <title>思い出が詰まった大切な遺品をお買い取りいたします。| 遺品買取のブランドリバリュー</title>
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <?php get_template_part('template-parts/gtm', 'head') ?>
    </head>

    <body>
        <?php get_template_part('template-parts/gtm', 'body') ?>
        <article>

            <header class="global">
                <div class="container">
                    <a class="logo" href="<?php site_url('/') ?>/"><img src="<?php echo get_template_directory_uri() ?>/imgs/header_logo.png" alt="ブランド・時計・貴金属・ダイヤ宝石買取専門店 BRAND VALUE - ブランドバリュー -"></a>
                    <a class="tel" href="tel:0120-970-060"><div class="tel_number">0120-970-060</div></a>
                    <a class="form" href="#form"><img src="<?php echo get_template_directory_uri() ?>/imgs/header_form.png" alt="お問い合わせ"></a>
                </div>
            </header>

            <div id="fv">
                <div class="container">
                    <h1><img src="<?php echo get_template_directory_uri() ?>/imgs/fv_h1.png" alt="思い出が詰まった大切な遺品をお買い取りいたします。" /></h1>
                    <ul class="points">
                        <li>
                            <picture>
                                <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/fv_points_01_sm.png">
                                <img src="<?php echo get_template_directory_uri() ?>/imgs/fv_points_01.png" alt="出張料無料">
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/fv_points_02_sm.png">
                                <img src="<?php echo get_template_directory_uri() ?>/imgs/fv_points_02.png" alt="査定料無料">
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/fv_points_03_sm.png">
                                <img src="<?php echo get_template_directory_uri() ?>/imgs/fv_points_03.png" alt="キャンセル料無料">
                            </picture>
                        </li>
                    </ul>
                </div>
            </div>

            <section id="sec01">
                <div class="container">
                    <h2>
                        <img class="lg" src="<?php echo get_template_directory_uri() ?>/imgs/sec01_h2.png" alt="遺品＝不用品ではありません。" />
                        <img class="sm" src="<?php echo get_template_directory_uri() ?>/imgs/sec01_h2_sm.png" alt="遺品＝不用品ではありません。" />
                    </h2>
                    <p>遺品買取のブランドリバリューでは、<br class="lg" />大切な遺品を心を込めてお買取り致します。</p>
                    <img class="items" src="<?php echo get_template_directory_uri() ?>/imgs/sec01_items.png" alt="">
                </div>
            </section>

            <section id="sec02">
                <div class="container">
                    <h2><img src="<?php echo get_template_directory_uri() ?>/imgs/sec02_h2.png" alt="多くの方が、「遺品整理」に多額の費用を請求されております。" /></h2>
                    <table>
                        <caption><img src="<?php echo get_template_directory_uri() ?>/imgs/sec02_table_caption.png" alt="こんなにもお得なんです！" /></caption>
                        <thead>
                            <tr>
                                <th>
                                    <picture>
                                        <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/sec02_th_01_sm.png">
                                        <img src="<?php echo get_template_directory_uri() ?>/imgs/sec02_th_01.png" alt="遺品買取の「ブランドリバリュー」へ依頼" />
                                    </picture>
                                </th>
                                <th>
                                    <picture>
                                        <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/sec02_th_02_sm.png">
                                        <img src="<?php echo get_template_directory_uri() ?>/imgs/sec02_th_02.png" alt="遺品回収業者へ依頼" />
                                    </picture>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="td01">故人様の大切な遺品を<br />買取金額<strong>100</strong>万円<br class="sm">でご成約</td>
                                <td class="td02">故人様の大切な遺品を回収・処分</td>
                            </tr>
                            <tr>
                                <td class="td03" colspan="2">遺品回収業者へ<em>30</em>万円のお支払い</td>
                            </tr>
                            <tr>
                                <td class="td04">遺品に適正な価値を<br class="sm">付けてもらい<br class="lg">回収・<br class="sm">処分費用を支払っても<br /><strong>70</strong>万円が相続金と<br class="sm">して手元に残る</td>
                                <td class="td05">回収・処分され費用として<br /><em>30</em>万円の出費</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>

            <section id="sec03" class="secInquiry">
                <div class="container">
                    <p>故人様の大切な遺品を心を込めて買い取ります。</p>
                    <h2>
                        <picture>
                            <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/secInquiry_h2_sm.png">
                            <img src="<?php echo get_template_directory_uri() ?>/imgs/secInquiry_h2.png" alt="遺品整理業者に連絡をする前に、遺品買取のブランドリバリューにご連絡下さい！" />
                        </picture>
                    </h2>
                    <picture class="copy">
                        <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/secInquiry_copy_sm.png">
                        <img src="<?php echo get_template_directory_uri() ?>/imgs/secInquiry_copy.png" alt="出張料・査定料 キャンセル料無料！" />
                    </picture>
                    <a class="tel" href="tel:0120-970-060">
                        <picture>
                            <div class="tel_block_sp"><img src="<?php echo get_template_directory_uri() ?>/imgs/secInquiry_tel_sm.png"></div>
                            <div class="tel_block"><div class="tel_number">0120-970-060</div></div>
                        </picture>
                    </a>
                    <a class="form" href="#form">
                        <picture>
                            <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/secInquiry_form_sm.png">
                            <img src="<?php echo get_template_directory_uri() ?>/imgs/secInquiry_form.png" alt="お問い合わせ">
                        </picture>
                    </a>
                </div>
            </section>

            <section id="sec04">
                <div class="container">
                    <h2>
                        <picture>
                            <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/sec04_h2_sm.png">
                            <img src="<?php echo get_template_directory_uri() ?>/imgs/sec04_h2.png" alt="遺品買取ブランドリバリューの3つの強み" />
                        </picture>
                    </h2>
                    <div class="clearfix sections">
                        <section>
                            <h3>
                                <picture>
                                    <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/sec04_h3_01_sm.png">
                                    <img src="<?php echo get_template_directory_uri() ?>/imgs/sec04_h3_01.png" alt="遺品買取免許を取得した鑑定士が対応致します。" />
                                </picture>
                            </h3>
                            <p>遺品買取ブランドリバリューでは、遺品買取の免許を取得したスタッフが正しい知識を活用し故人様の大切な遺品をお買取りさせて頂きます。</p>
                        </section>
                        <section>
                            <h3>
                                <picture>
                                    <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/sec04_h3_02_sm.png">
                                    <img src="<?php echo get_template_directory_uri() ?>/imgs/sec04_h3_02.png" alt="出張・査定・ キャンセルは 完全無料で ご対応致します。" />
                                </picture>
                            </h3>
                            <p>遺品買取ブランドリバリューでは、出張費・査定・ご相談は完全無料でご対応させて頂いておりますので、お客様の費用負担は一切ございません。お気軽にお問合せ下さい。</p>
                        </section>
                        <section>
                            <h3><img src="<?php echo get_template_directory_uri() ?>/imgs/sec04_h3_03.png" alt="即日現金で ご対応致します。" /></h3>
                            <p>査定が完了しお客様の合意を頂ければ、その場で現金でお買取り致します。</p>
                        </section>
                    </div>
                </div>
            </section>

            <section id="sec05">
                <div class="container">
                    <h2>
                        <picture>
                            <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/sec05_h2_sm.png">
                            <img src="<?php echo get_template_directory_uri() ?>/imgs/sec05_h2.png" alt="遺品買取（買取〜整理まで）の流れ" />
                        </picture>
                    </h2>
                    <ul class="clearfix flow">
                        <li>
                            <picture>
                                <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/sec05_flow_01_sm.png">
                                <img src="<?php echo get_template_directory_uri() ?>/imgs/sec05_flow_01.png" alt="お問い合わせ" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/sec05_flow_02_sm.png">
                                <img src="<?php echo get_template_directory_uri() ?>/imgs/sec05_flow_02.png" alt="指定訪問先にお伺い" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/sec05_flow_03_sm.png">
                                <img src="<?php echo get_template_directory_uri() ?>/imgs/sec05_flow_03.png" alt="遺品を査定" />
                            </picture>
                        </li>
                        <li>
                            <picture>
                                <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/sec05_flow_04_sm.png">
                                <img src="<?php echo get_template_directory_uri() ?>/imgs/sec05_flow_04.png" alt="お見積のお渡し" />
                            </picture>
                        </li>
                    </ul>
                </div>
            </section>

            <section id="sec06">
                <div class="container">
                    <h2><img src="<?php echo get_template_directory_uri() ?>/imgs/sec06_h2.png" alt="買取商品のご紹介" /></h2>
                    <div class="clearfix" id="itemSlider">
                        <ul class="clearfix items sp-slides">
                            <li>
                                <img src="<?php echo get_template_directory_uri() ?>/imgs/sec06_items_01.png" alt="" />
                                <div class="name">油彩画</div>
                                <p>静物<br />宮本　三郎</p>
                                <div class="price">
                                    <div class="label">買取金額</div>
                                    <div class="value">¥400,000</div>
                                </div>
                            </li>
                            <li>
                                <img src="<?php echo get_template_directory_uri() ?>/imgs/sec06_items_02.png" alt="" />
                                <div class="name">日本画等軸装</div>
                                <p><br />川端 龍子</p>
                                <div class="price">
                                    <div class="label">買取金額</div>
                                    <div class="value">¥300,000</div>
                                </div>
                            </li>
                            <li>
                                <img src="<?php echo get_template_directory_uri() ?>/imgs/sec06_items_03.png" alt="" />
                                <div class="name">彫刻</div>
                                <p>福神<br />圓鍔 勝三</p>
                                <div class="price">
                                    <div class="label">買取金額</div>
                                    <div class="value">¥300,000</div>
                                </div>
                            </li>
                            <li>
                                <img src="<?php echo get_template_directory_uri() ?>/imgs/sec06_items_04.png" alt="" />
                                <div class="name">彫刻</div>
                                <p>獅子頭<br />横山 一夢</p>
                                <div class="price">
                                    <div class="label">買取金額</div>
                                    <div class="value">¥30,000</div>
                                </div>
                            </li>
                            <li>
                                <img src="<?php echo get_template_directory_uri() ?>/imgs/sec06_items_05.png" alt="" />
                                <div class="name">現代版画</div>
                                <p>聖マリアたち<br />アントニ・クラーベ </p>
                                <div class="price">
                                    <div class="label">買取金額</div>
                                    <div class="value">¥15,000</div>
                                </div>
                            </li>
                            <li>
                                <img src="<?php echo get_template_directory_uri() ?>/imgs/sec06_items_06.png" alt="" />
                                <div class="name">花瓶、茶道具、<br />着物　各種</div>
                                <div class="price">
                                    <div class="label">買取金額</div>
                                    <div class="value">〜ASK</div>
                                </div>
                            </li>
                            <li>
                                <img src="<?php echo get_template_directory_uri() ?>/imgs/sec06_items_07.png" alt="" />
                                <div class="name">懐中時計</div>
                                <p>ウォルサム<br />素材：K18</p>
                                <div class="price">
                                    <div class="label">買取金額</div>
                                    <div class="value">¥40,000</div>
                                </div>
                            </li>
                            <li>
                                <img src="<?php echo get_template_directory_uri() ?>/imgs/sec06_items_08.png" alt="" />
                                <div class="name">ネックレス</div>
                                <p>宝石</p>
                                <div class="price">
                                    <div class="label">買取金額</div>
                                    <div class="value">¥200,000</div>
                                </div>
                            </li>
                            <li>
                                <img src="<?php echo get_template_directory_uri() ?>/imgs/sec06_items_09.png" alt="" />
                                <div class="name">バッグ</div>
                                <p>バビロン<br />ヴィトン</p>
                                <div class="price">
                                    <div class="label">買取金額</div>
                                    <div class="value">¥80,000</div>
                                </div>
                            </li>
                            <li>
                                <img src="<?php echo get_template_directory_uri() ?>/imgs/sec06_items_10.png" alt="" />
                                <div class="name">腕時計</div>
                                <p>デイトジャスト69178<br />ROLEX</p>
                                <div class="price">
                                    <div class="label">買取金額</div>
                                    <div class="value">¥500,000</div>
                                </div>
                            </li>
                            <li>
                                <img src="<?php echo get_template_directory_uri() ?>/imgs/sec06_items_11.png" alt="" />
                                <div class="name">宝石</div>
                                <p>ダイヤリング<br />ダイヤモンド</p>
                                <div class="price">
                                    <div class="label">買取金額</div>
                                    <div class="value">¥50,000</div>
                                </div>
                            </li>
                            <li>
                                <img src="<?php echo get_template_directory_uri() ?>/imgs/sec06_items_12.png" alt="" />
                                <div class="name">貴金属</div>
                                <p>ブランド：なし</p>
                                <div class="price">
                                    <div class="label">買取金額</div>
                                    <div class="value">¥10,000〜ASK</div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>

            <section id="sec07" class="secInquiry">
                <div class="container">
                    <p>故人様の大切な遺品を心を込めて買い取ります。</p>
                    <h2>
                        <picture>
                            <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/secInquiry_h2_sm.png">
                            <img src="<?php echo get_template_directory_uri() ?>/imgs/secInquiry_h2.png" alt="遺品整理業者に連絡をする前に、遺品買取のブランドリバリューにご連絡下さい！" />
                        </picture>
                    </h2>
                    <picture class="copy">
                        <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/secInquiry_copy_sm.png">
                        <img src="<?php echo get_template_directory_uri() ?>/imgs/secInquiry_copy.png" alt="出張料・査定料 キャンセル料無料！" />
                    </picture>
                    <a class="tel" href="tel:0120-970-060">
                        <picture>
                            <div class="tel_block_sp"><img src="<?php echo get_template_directory_uri() ?>/imgs/secInquiry_tel_sm.png"></div>
                            <div class="tel_block"><div class="tel_number">0120-970-060</div></div>
                        </picture>
                    </a>
                    <a class="form" href="#form">
                        <picture>
                            <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/secInquiry_form_sm.png">
                            <img src="<?php echo get_template_directory_uri() ?>/imgs/secInquiry_form.png" alt="お問い合わせ">
                        </picture>
                    </a>
                </div>
            </section>

            <section id="sec08">
                <div class="container">
                    <h2>
                        <picture>
                            <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/sec08_h2_sm.png">
                            <img src="<?php echo get_template_directory_uri() ?>/imgs/sec08_h2.png" alt="状態に関係なく高価買取を致します！" />
                        </picture>
                    </h2>
                    <ul class="clearfix points">
                        <li>キズがある</li>
                        <li class="long">刻印がない、変形している</li>
                        <li>カバンにカビがある</li>
                        <li>デザインが古い</li>
                        <li>時計が動いていない</li>
                        <li>鑑定書がない</li>
                        <li>劣化している</li>
                        <li>指輪の主石がない</li>
                        <li>一部破損している</li>
                        <li class="long">ダメージや持ち手が汚れている</li>
                    </ul>
                </div>
            </section>

            <section id="sec09">
                <div class="container">
                    <h2><img src="<?php echo get_template_directory_uri() ?>/imgs/sec09_h2.png" alt="お客様の声" /></h2>
                    <div id="voiceSlider">
                        <div class="sp-slides sections clearfix">
                            <div class="slide">
                                <section>
                                    <h3>千葉県　Aさん　男性(30代)</h3>
                                    <img src="<?php echo get_template_directory_uri() ?>/imgs/sec09_sec1.png" alt="" />
                                    <p>ブランドリバリューを利用する前に他の業者様にも当たってみましたがプロの鑑定士がいない為、高額な物は引き取れないと言われてしまいました。私は、足腰も弱く専門の買い取り店へ行く事が難しい為、どこか良いとこはないか探していたときネットでブランドリバリューさんを見つけました。ブランドリバリューでは引き取れないと言われてしまったものでも資格を持ったプロの方に一つ一つしっかり鑑定して頂き誠実な対応で納得のいく金額、説明を受け感動しました。</p>
                                </section>
                            </div>
                            <div class="slide">
                                <section>
                                    <h3>京都府　Bさん　男性(60代)</h3>
                                    <img src="<?php echo get_template_directory_uri() ?>/imgs/sec09_sec2.png" alt="" />
                                    <p>亡くなった妻の遺品整理で利用させてもらいました。どこのホームページを見ても良いことばかりしか書いてないので正直半信半疑ではありましたが、ホームページ通りのことばかりでびっくりしました。特に一番驚いた事は、対応の誠実さです。些細な事でも親身になって対応して頂けたので安心して任せられました。</p>
                                </section>
                            </div>
                            <div class="slide">
                                <section>
                                    <h3>東京都　Cさん　男性(40代)</h3>
                                    <img src="<?php echo get_template_directory_uri() ?>/imgs/sec09_sec3.png" alt="" />
                                    <p>震災の影響を受けた家の整理をしようと3社ほど見積もりを取りましたが、2日はかかると言われたり、あしらうような対応だったりとあまり良い業者様に出会えず、ブランドリバリューさんにお願いしたところ快く引き受けて下さり、もの凄いチームワークで綺麗に片付けて頂きました。とても大変だったと思いますがほんとに感謝してます。</p>
                                </section>
                            </div>
                            <div class="slide">
                                <section>
                                    <h3>大阪府　Dさん　女性(50代)</h3>
                                    <img src="<?php echo get_template_directory_uri() ?>/imgs/sec09_sec4.png" alt="" />
                                    <p>東京で一人暮らしの祖母が急逝してしまい困っていたところ数ある遺品整理業者様からブランドリバリューさんにお願い致しました。金額はもう少し安いところもありましたが、対応力や人柄などを考えると、安い金額ではないかと思います。供養から遺品の査定、家具の発送まで丁寧に対応して頂きました。良い会社に出会えてほんとによかったです。ありがとうございました。</p>
                                </section>

                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="form">
                <div class="container">
                    <h2><img src="<?php echo get_template_directory_uri() ?>/imgs/secForm_h2.png" alt="お問い合わせフォーム" /></h2>
                    <form action="<?php echo get_template_directory_uri() ?>/mail.php" method="POST" id="inquiryForm" novalidate>
                        <table class="form_tbl">
                            <tr>
                                <th class="form_th">お名前<span style="font-size:12px;color:#ffe400;">　※必須</span></th>
                                <td><span class="with-validate"><input type="text" name="お名前" placeholder="お名前を入力してください" class="form_b" /></span></td>
                            </tr>
                            <tr>
                                <th class="form_th">フリガナ<span style="font-size:12px;color:#ffe400;">　※必須</span></th>
                                <td><span class="with-validate"><input type="text" name="フリガナ" placeholder="フリガナを入力してください" class="form_b" /></span></td>
                            </tr>
                            <tr>
                                <th class="form_th">住所</th>
                                <td><input type="text" name="住所" placeholder="ご住所を入力してください" class="form_b" /></td>
                            </tr>
                            <tr>
                                <th class="form_th">電話番号<span style="font-size:12px;color:#ffe400;">　※必須</span></th>
                                <td><span class="with-validate"><input type="tel" name="電話番号" placeholder="電話番号を入力してください" class="form_b" /></span></td>
                            </tr>
                            <tr>
                                <th class="form_th">メールアドレス<span style="font-size:12px;color:#ffe400;">　※必須</span></th>
                                <td><span class="with-validate"><input type="email" name="メールアドレス" placeholder="メールアドレスを入力してください" class="form_b" /></span></td>
                            </tr>
                            <tr>
                                <th class="form_th">個人情報保護方針に<br>同意する<span style="font-size:12px;color:#ffe400;">　※必須</span></th>
                                <td><span class="with-validate"><input type="checkbox" name="同意チェックボックス" style="width:initial;" /></span>個人情報保護方針をご確認のうえチェックをいれてください</td>
                            </tr>
                            <tr>
                                <th class="form_th">個人情報保護方針</th>
                                <td>
                                    <pre style="height: 15em; max-width: 60em; overflow: scroll;">
1.株式会社STAYGOLD当社は、お客様の個人情報を業務の遂行上必要な範囲内において以下の目的で利用いたします。
※当社ウェブページからのお申込、お問い合わせによるお申込、メール・電話によるお問い合わせを問わず、お客様から明示された特定の個人を識別できる情報 (以下「個人情報」) について下記のとおり取り扱うものとします。
※個人情報とはお客様を識別できる情報のことで、氏名、住所、電話番号、メールアドレスなどをいいます。
※当社が個人情報を収集する場合は、収集目的を明らかにし、必要な範囲内の個人情報を収集いたします。
※当社は取得した個人情報について適切な管理に努めると共に個人情報の漏洩、改ざん、不正な侵入の防止に努めます。
※当社は取得した個人情報を次の各項の場合を除いて、原則として第三者に提供、開示などいたしません。
(1)お容様からお問い合せへの対応、アフターサービス、FC業務取引に関する一切の手続実施のため
(2)当社、当社グループ会社または当社と提携する他社サービス、商品、各種企画に関するご案内のため
(3)アンケート調査、キャンペーン実施等、当社事業活動に関するご案内のため
(4)FC取引状況管理、与信管理、償権管理のため
(5)お容様の本人確認のため
(6)不正な取引の防止のため
(7)マーケティング調査及び分析のため
(8)経営分析のための続計数値作成及び分新結果の利用のため
(9)お客様との電話対応、 接客の際、 オペレーターまたはスタッフの的確な対応と品質維持向上のため
(10)その他法令に基づく対応等を含めた取引等に必要な業務のため
※当社は、当社が保有する個人情報に関して適用される日本の法令、その他規範を遵守するとともに、本ポリシーの内容を適宜見直し、その改善に努めます。
2. 個人情報の取り扱いについては、 当社の「<a href="<?php echo home_url('/privacy') ?>">プライバシーポリシー</a>」に従って適切に取り扱います。
【個人情報保護管理者】株式会社STAYGOLD TEL.03-6427-1153 (電話受付時間平日10:00~17:00)
                                    </pre>
                                </td>
                            </tr>
                            <tr>
                                <th class="comment form_th">備考</th>
                                <td><textarea name="備考"></textarea></td>
                            </tr>
                        </table>
                        <div class="submitContainer">
                            <input value="確認して送信する" type="submit" />
                        </div>
                    </form>
                </div>
            </section>

            <section id="sec10">
                <div class="container">
                    <h2><img src="<?php echo get_template_directory_uri() ?>/imgs/sec10_h2.png" alt="スタッフ紹介" /></h2>
                    <div class="clearfix sections">
                        <section>
                            <img src="<?php echo get_template_directory_uri() ?>/imgs/sec10_stuff_01.png" alt="" />
                            <p>私が遺品整理士を志したきっかけは、鑑定士の能力を活かして「ご遺族の役に立ちたい」という想いからでした。遺品の多くは、ご遺族の方では価値が分からないことが多く、遺品＝処分と考えている遺族の方が多くいます。私は、遺品の価値を見極め、ご遺族の方に還元するということは、生前の故人様の想いを汲み取ることにも繋がるのではないかと考えています。遺品整理士として、ご遺族の方に真の思いやりと心からの親切をモットーに少しでもお役に立てたらと思います。<br /><strong>遺品整理・遺品買取ならブランドリバリュー銀座店にお任せ下さい！</strong><br />皆様のお問合せ心よりお持ちしております。</p>
                            <h3 class="name">森 鑑定士</h3>
                        </section>
                        <section>
                            <img src="<?php echo get_template_directory_uri() ?>/imgs/sec10_stuff_02.png" alt="" />
                            <p>遺品整理では、ご遺族の思いが様々ございます。私共の基準ではなくご遺族様、ご依頼者様のご希望一つ一つに添えるよう努めるのが私たちの責務だと思います。私は、これまでの経験を活かしきっと皆様のご期待に添えるまごころをご提供出来る自信があると思い遺品整理士を志しました。遺品整理・遺品買取は、ブランドリバリューにお任せ下さい。<br />私たちに何が出来るか常に考え、より良いご提案をする事を心がけ大切な思い出がお客様に戻ってくるよう、丁寧な作業をお約束致します。<br /><strong>どんな些細な事でも結構です！皆様のお問合せ心よりお持ちしております！</strong></p>
                            <h3 class="name">佐藤 鑑定士</h3>
                        </section>
                    </div>
                </div>
            </section>

            <section id="sec11" class="column">
                <div class="container">
                    <h2>遺品整理の現状</h2>
                    <p>現代では、日本の高齢化や核家族化、ライフスタイルの変化により「遺品整理」という仕事に注目が集まっております。今後も日本の高齢化は進展すると考えられており、遺品整理の需要はさらに増加していくことが予測されます。以前は、ご遺族が遺品整理を行うことが一般的でしたが、ライフスタイルの変化に伴い遺品を整理する時間の確保が困難だというご遺族の方も増えてきました。当社では、現代のニーズに応えられるよう遺品整理の事業展開を開始致しました。故人様の大切な遺品と真摯に向き合いたい、少しでも残された遺族のお役に立ちたいと考えております。</p>
                    <p>現在、高齢者の独身世帯が増加し、高齢者の孤立死が社会問題化しております。高齢者の孤立死が増加しているという事は、それだけ家族関係が希薄になっているとも言えます。生前に資産や貴重品などの場所を把握する機会もなく、残された遺品の価値が分からないというご遺族も少なくありません。ブランドリバリューでは、プロの鑑定士が大切な遺品の価値をしっかりと見定めてお買取りさせて頂きます。遺品整理でよくあるトラブルとして不正買取が代表的なトラブル事例として挙げられますが、当店では、業界に長く携わってきたプロの鑑定士が誠心誠意ご対応致しますのでご安心下さいませ。</p>
                    <p>ブランドリバリューは、ブランドアパレル買取販売店BRINGを母体とし、時計、バッグ、貴金属、ジュエリー、骨董品、絵画等をメインに様々なジャンルの買取を行っております。アンティークの時計や骨董品、絵画は、ご自身の想像以上に価値が付くことがございます。見た目はボロボロでホコリが被っているような時計でも数百万円の価値がある時計だったということも珍しくありません。<br />査定料・キャンセル料等は一切ございませんので、査定だけでもいかがでしょうか。時計、バッグ、貴金属、ジュエリーなどは、経済情勢、為替相場の影響により相場が変動いしてしまいます。鑑定士もいつ相場がどうなるかはわかりません。もし、遺品の整理に困っているようであれば、お早めにご売却することをおすすめ致します。店頭に行く時間がないという方は、宅配買取、出張買取をご利用下さいませ。メールやLINEでお写真を頂き査定を行うことも可能です。スタッフ一同、皆さまのご来店、お問合せ心よりお待ち申し上げます。</p>
                    <h2>遺品整理士とは</h2>
                    <p>遺品整理士とは、一般社団法人遺品整理士認定協会が認定している資格でございます。資格取得までに、遺品整理士の心構えや法規制などを学び、遺品整理に必要な知識向上に取り組んでまいりました。現在、遺品整理の需要が拡大する中、遺品整理にまつわるトラブルが急増しているのも事実です。トラブル増加により、遺品整理に対し悪いイメージを持っている方や不安を抱いてしまう方も少なくありません。</p>
                    <p>当社では、遺品整理の相談をする際に少しでも安心して欲しいという想いから、資格取得を行い、遺品整理士のあるべき姿を学んでまいりました。大切な方を亡くされて「何から手を付けたらいいのか分からない」という方は、お気軽にご相談下さいませ。</p>
                    <h2>ありがちな遺品整理でのトラブルとは</h2>
                    <p>遺品整理でのトラブルは、以下のようなものが独立行政法人国民センターをはじめ、一般社団法人遺整理士認定協会に対し報告されているため、消費も注意が必要なようです。</p>
                    <section class="cases">
                        <h3>【事例1：不当な買取に対してのトラブル】</h3>
                        <p>かなり低い価格での買取も、遺品整理業者とのトラブルとして多いことがあげられます。<br />貴金属類（アクセサリーやブランド物、時計）などといったものを価値があるにもかかわらず、中古市場での相場と比べ、明らかに低い価格で見積もり買取をさせるというケースや、中古市場での相場自体をわざと安く偽らせ、正しい価格を提示しているかのようにし買取成立を促すというようなトラブルもあげられます。<br />上記だけではなく、手数料に対しても、普通では考えられないような手数料を発生させるということもあるようです。<br />これだけならまだしも、低い価格を提示するだけで売却価格を伝えない、といったトラブルのケースも出ています。<br />これらは特に、遺族側からすると判断しにくいのはもちろんのこと、そもそも被害に自分があっている状態であるということも気づかない遺族の方もいることから、報告に上がっていないトラブルも存在することが考えられます。
                            <br />例をだしてみるならば、亡くなった世帯主が数十万円で購入した腕時計を無理に数万円程度で買い取られた、というような場合もあげられるでしょう。</p>
                    </section>
                    <section class="cases">
                        <h3>【事例2：不当な請求に対してのトラブル】</h3>
                        <p>より悪質なトラブルで、費用を不当に請求される場合もあるようです。<br />遺品整理の依頼をし、費用見積もりをしてもらった際に、きちんと遺品整理をしてもらう場所の状況をチェックしてもらうことなく高額な見積もりを出してくるケースもあるようです。<br />
                            遺品整理の見積もりが高額なことからキャンセルする旨を伝えると、手配を済ませてしまっていることからキャンセル料を請求されたと言うケースが該当します。<br />
                            これ以外にも、一番費用が安い業者に依頼するために、そう見積もりを取り、業者を選んだつもりが、作業を進めていく上で追加料金が加算されてしまい、最終的にはかなりの費用が掛かってしまった、という場合もあるようです。</p>
                    </section>
                    <section class="cases">
                        <h3>【事例3：無理な契約等に対してのトラブル】</h3>
                        <p>見積もりは無料ですといわれていた業者に対し、見積もりを実際に取ってもらったものの見積もりを見て依頼を断った際、見積もりが無料なのは依頼された場合だけなどというようないわゆる見積もりの費用を取られたというような場合。<br />またこれだけではなく、キャンセルをする際にもかなりの額のキャンセルに対する費用が発生してしまった場合もあげられます。<br />さらに、ご遺族の方が契約自体を拒んだにもかかわらず、無理に遺品整理を開始してしまう、といった強引なケースもあります。</p>
                    </section>
                    <p>上記にあげたようなトラブルを抱え込まないようにするためには、やはり前もってきちんと見積額を出してもらい、この見積額に対しての説明をきちんとしてもらうことができる業者に遺品整理を依頼することが重要だといえるでしょう。<br />遺品整理の業者や評判に対し、インターネットを活用し、トラブルなどが発生していないかをチェックしてみたり、それぞれの業者がいつ設立されたかの年度のチェック（こまめに業者名を変えている場合もあるため）などもしっかりとチェックすることも大切だといえるでしょう。<br />そもそも、遺品整理業に対しての公的な資格自体は存在しないものですが、「一般廃棄物収集運搬」の認可が不用品処分業には必要ですし、「古物商」の許可（こちらは古物営業法に規定される業者であり、都道府県公安委員会が審査をするもの。）がリサイクル品買取業には必要なのです。<br />ですからこれらに対しきちんと登録されているのかといった状況をきちんと把握することも重要だといえます。<br />ですが最終的には、買い取り業者に対し、遺品処理に対しては処理をする業者も、こちら処理したものを持ち込む、ということになります。<br />なぜかといいますと、特に必要でもない中間手数料を支払う必要もなく直接的に持ち込むことで、買取を最高価格ですることができるからです。<br />業者の方や一般のお客様、どちらも価格には違いがありませんし、どのような物や量でも、どのような方でも、しっかりと最高価格を出させていただきますよ。<br />ちなみに、生前から遺言等やエンディングノートできちんと資産に対して準備をしていただいているのであれば、「万が一」の時は、ご遺族の方がより的確な判断をすることができる基準にもなると思っております。<br />特にこれらに対してのより詳しい判断については、公認会計士や税理士、行政書士などといったいわゆるそれ専門のエキスパートな方々に相談してもらいたいと思っておりますが、こちらも常識的に必要とされている知識をあげさせていただきたいなと考えております。</p>
                    <ul class="points">
                        <li>成年後見人や任意後見人が有るのか、無いのか。</li>
                        <li>遺言やエンディングノートが有るのか、無いのか。</li>
                        <li>もし遺言が存在するのなら、自筆証明遺言、公正証書遺言または秘密証明遺言のどれなのか。</li>
                        <li>法定相続人の範囲についてのチェック。</li>
                        <li>相続の遺留分についてのチェック。</li>
                        <li>形見分けの範囲に対してのチェック。</li>
                    </ul>
                    <p>また、遺言やエンディングノートを用意したのならば、これ自体がきちんと存在しているということを周りの人たちに伝えておくことも重要です。<br />自筆証明遺言の場合（自分で作った遺言）は、特に法的にみてしっかりと根拠を満たすことが出来ているものなのかはもちろんのこと、実際に発見されずに後から発見され、トラブルに発展しないように気を付けることも意識しておくことが大切です。</p>
                    <h2>遺品整理士の理想像とは</h2>
                    <p>ブランドリバリューでは、遺品整理を行う際の心構えとして、「遺品」は不要品ではないという共通認識を徹底しております。新規事業者の参入により、遺品整理業者の質が低下し遺品を不用品をとして段ボールに投げ入れる行為を行う心もとない業者も存在します。当社では、そういったことがないよう「遺品」は不要品でななく故人様が残された大切な思い出として扱うように心がけております。また、新規参入業者が増える中、遺品整理にまつわるトラブルが増加しているのが実情です。トラブルの事例としては、不当な買取や請求、強引な契約、など多岐に渡ります。遺品整理業者を選ぶ際は、遺品整理士の認定資格を持っているか、会社情報をHP上で開示しているかなどを確認し、選択することが重要です。ブランドリバリューのスタッフは、一流の鑑定士、遺品整理士です。お客様のご希望に添い、質の高いサービスをご提供いたします。少しでもご遺族の方のお役に立てたら幸いです。</p>
                </div>
            </section>

            <footer class="global">
                <div class="container">
                    <a href="<?php site_url('/') ?>/"><img src="<?php echo get_template_directory_uri() ?>/imgs/footer_banner.png" alt="ブランド・時計・貴金属・ダイヤ宝石買取専門店 BRAND VALUE - ブランドバリュー -　詳しくはこちら" /></a>
                    <a class="form" href="#form">
                        <picture>
                            <source media="(max-width: 640px)" srcset="<?php echo get_template_directory_uri() ?>/imgs/footer_button_sm.png">
                            <img src="<?php echo get_template_directory_uri() ?>/imgs/footer_button.png" alt="無料！！ 出張見積り依頼" />
                        </picture>
                    </a>
                    <!--p class="auths">大阪府公安委員会許可    第621120152181号　愛知県公安委員会許可    第541161509800号　　東京都公安委員会許可    第301061604141号<br />広島県公安委員会許可    第731021600014号　神奈川県公安委員会許可 第451310004957号　福岡県公安委員会許可    第901021510096号<br />京都府公安委員会許可    第611241630018号</p-->
                    <br />
                    <p class="links"><a href="<?php site_url('/') ?>/company">会社概要</a>　|　<a href="<?php site_url('/') ?>/privacy">プライバシーポリシー</a></p>
                </div>
                <p class="copyright">Copyrights © <?php echo date('Y') ?> 高額ブランド買取専門サイト BRAND REVALUE all rights reserved</p>
            </footer>

            <div class="footnav sm">
                <a class="phone" href="tel:0120970060"><img src="<?php echo get_template_directory_uri() ?>/imgs/footnav_tel.png" alt="お電話はこちら（0120-970-060）"></a>
                <a class="form" href="#form"><img src="<?php echo get_template_directory_uri() ?>/imgs/footnav_form.png" alt="お問い合わせ"></a>
            </div>

        </article>


        <script src="<?php echo get_template_directory_uri() ?>/vendor/jquery/js/jquery-3.1.1.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/vendor/bootstrap/js/tether.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/vendor/slider-pro/js/jquery.sliderPro.min.js"></script>

        <script src="<?php echo get_template_directory_uri() ?>/js/mement/jquery.validate.min.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/js/mement/for-validate.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/js/mement/app.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/js/mement/smoothscroll.js"></script>

        <script>
            $(document).ready(function($) {
                var ww = $(window).width();

                if (ww < 641) {
                    $('#itemSlider li').addClass('sp-slide');
                    $('#itemSlider').sliderPro({
                        width: 154,
                        height: 410,
                        autoSlideSize: false,
                        autoplay: true,
                        arrows: false,
                        buttons: false,
                        visibleSize: '100%',
                    });

                    $('#voiceSlider .slide').addClass('sp-slide');
                    $('#voiceSlider').sliderPro({
                        width: 570,
                        height: 530,
                        autoplay: false,
                        arrows: false,
                        buttons: false,
                        visibleSize: '100%',
                    });
                }
            });
        </script>
    </body>

</html>
