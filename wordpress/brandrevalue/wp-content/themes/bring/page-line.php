<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>
	<div id="primary" class="brandlist content-area">
		<main id="main" class="site-main" role="main">

      <section id="catchcopy">
        <img src="<?php echo get_s3_template_directory_uri() ?>/img/mv/line_mv.png">
            <div class="line_mv">
                <img src="<?php echo get_s3_template_directory_uri() ?>/img/line/line_ttl.png" alt="ご来店も郵送も不要！スマートフォンひとつで、スピード査定が可能に">
                <p>「クローゼットの中に使わなくなったブランド品があるけど、どれぐらいの金額になるのか見当がつかないし、お店に行く時間もない」そんな方にお勧めしたいのがLINE査定です。
                携帯電話の内蔵カメラでお品物を写し、その画像をLINEやメールでお送りいただくだけでOK。<br>
                24時間以内に簡易査定を行い、目安となる買取金額をお伝えいたします。</p>
            </div>
      </section>

      <section class="line_step">
            <h3 class="text-center"><img src="<?php echo get_s3_template_directory_uri() ?>/img/line/line_stepttl.png" alt="登録はかんたん！たったの3ステップ"></h3>
                <div class="step_box">
                    <dl>
                        <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/line/line_step1.png" alt="STEP1"></dt>
                        <dd>LINEを開き「その他」メニューから「友だち追加」を選択して下さい。</dd>
                    </dl>
                    <dl>
                        <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/line/line_step2.png" alt="STEP2"></dt>
                        <dd>
                        <p class="qr"><img src="<?php echo get_s3_template_directory_uri() ?>/img/line/qr.png" alt="QR"></p>
                        <p class="qr_tx">@brandrevalueで検索して下さい。
                        <a href="https://line.me/ti/p/%40otv4506b"><img width="80" border="0" alt="友だち追加数" src="http://biz.line.naver.jp/line_business/img/btn/addfriends_ja.png"></a></p></dd>
                    </dl>
                    <dl>
                        <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/line/line_step3.png" alt="STEP3"></dt>
                        <dd>「BRAND REVALUE」を 友だちリストに追加して下さい。</dd>
                    </dl>
                </div>
      </section>
      <section>
        <p class="text-center"><img src="<?php echo get_s3_template_directory_uri() ?>/img/line/flow.png" alt="受付は24時間実施中！LINE査定の流れ"></p>
      </section>
      <section>
        <p class="text-center"><img src="<?php echo get_s3_template_directory_uri() ?>/img/line/attention.png" alt="LINE査定に関する注意事項"></p>
      </section>

      <section class="line_tech text-center">
          <dl>
              <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/line_tech_bar.png" alt="こうすれば査定額がより正確に！LINE査定の高額査定テクニック"></dt>
              <dd>
                  <p>LINE査定はあくまで「目安となる買取金額をお伝えする簡易査定」ですが、以下の点にご注意いただければ、より正確な金額をお伝えしやすくなります。
                  ぜひ、ご参考にしてみてください。</p>
                  <ul>
                      <li>写真は複数枚撮影し、外観全体が見えるようにしてください。<br>
                      ダメージがある箇所、型番等の表記はアップで写すとより効果的です。</li>
                      <li>購入先、購入時期、型番等、おわかりになる範囲で情報をご明記ください。</li>
                      <li>お品物のサイズ（寸法）もあわせてご明記ください。同じ形状でサイズが異なる商品もございます。</li>
                  </ul>
                  <p>その他、ご質問等ございましたら、LINEまたはメールにてお気軽にお伝えください。</p>
              </dd>
          </dl>
      </section>

      <?php
        // 買取基準
        //get_template_part('_criterion');

        // アクションポイント
        get_template_part('_action');

        // 買取方法
        get_template_part('_purchase');

        // 店舗案内
        get_template_part('_shopinfo');
      ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
