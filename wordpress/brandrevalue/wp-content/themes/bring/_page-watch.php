<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
    //'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
    '001.jpg::パテックフィリップ::カラトラバ PG 手巻き::1,480,000::1,420,000::1,510,000::90,000',
    '002.jpg::ブライトリング::アナビタイマー AB0120::370,000::364,000::380,000::16,000',
    '003.jpg::パネライ::PAM00088 GMT::370,000::364,000::380,000::16,000',
    '004.jpg::ボールウォッチ::ストークマン ストームチェイサー::128,000::124,000::131,000::7,000',
    '005.jpg::ROLEX::サブマリーナ 116610LN::570,000::560,000::600,000::40,000',
    '006.jpg::オーデマピゲ::ミレネリー::1,000,000::987,000::1,024,000::37,000',
    '007.jpg::パテックフィリップ::ゴンドーロ 手巻き::1,100,000::1,000,000::1,140,000::140,000',
    '008.jpg::ブレゲ::トランスアトランティック::1,012,000::1,000,000::1,020,000 ::20,000',
    '009.jpg::ブライトリング::ベントレーGT A13362::362,000::358,000::376,000::18,000',
    '010.jpg::コルム::アドミラルズカップ チャレンジ44 スプリット::310,000::306,000::314,000::8,000',
    '011.jpg::オメガ::スピードマスター クロノグラフ::115,000::110,000::121,000::11,000',
    '012.jpg::パネライ::ラジオミール 750WG ダイヤインデックス::1,760,000::1,750,000::1,800,000::50,000',
);


get_header(); ?>

<p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
   <p class="main_bottom">他社を圧倒！ブランドリバリューはブランド時計の買取実績多数！</p>
    <div id="primary" class="cat-page content-area">
        <main id="main" class="site-main" role="main">
        <div id="lp_head" class="watch_ttl">
<div>
<p>銀座で最高水準の査定価格・サービス品質をご体験ください。</p>
<h2>あなたの高級ブランド腕時計<br />どんな物でもお売り下さい！！</h2>
</div>
</div>
<p id="catchcopy">BRANDREVALUE(ブランドリバリュー)では独自の中古時計再販ルートを構築しているため、幅広いブランドの腕時計を高く査定し、買い取ることが可能となっております。
                <br> また、ブランド腕時計の買取において豊富な経験を積んだスタ ッフが在籍しており、故障した時計、電池切れで動かない時計、傷ついた時計等も価値を見落とすことなく最大限高く査定。
                <br>
                <br> 「他店で査定を断られたような時計が、BRANDREVALUE(ブランドリバリュー)では
                <br> 驚くほどの高額買取の対象となった」
                <br> ――そのようなことも全く珍しくはありません。
            </p>

            <section id="hikaku" class="watch_hikaku">
                <h3 class="text-hide">他社の買取額と比較すると</h3>
                <p class="hikakuName">AUDEMARS PIGUET
                    <br>ロイヤルオーク オフショア</p>
                <p class="hikakuText">※状態が良い場合や当店で品薄の場合などは
                    <br> 　特に高価買取致します。
                </p>
                <p class="hikakuPrice1"><span class="red">A社</span>：2,630,000円</p>
                <p class="hikakuPrice2"><span class="blue">B社</span>：2,750,000円</p>
                <p class="hikakuPrice3">2,900,000<span class="small">円</span></p>
            </section>

            <section id="lp_history">
                <h3 class="text-hide">有名ブランドの歴史</h3>
                <ul class="clearfix">
                    <li><a rel="leanModal" href="#div_point01"><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/btn_history01.jpg" alt="ロレックス"></a></li>
                    <li><a rel="leanModal" href="#div_point02"><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/btn_history02.jpg" alt="パテックフィリップ"></a></li>
                    <li><a rel="leanModal" href="#div_point03"><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/btn_history03.jpg" alt="ウブロ"></a></li>
                    <li><a rel="leanModal" href="#div_point04"><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/btn_history04.jpg" alt="オーデマピゲ"></a></li>
                    <li><a rel="leanModal" href="#div_point05"><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/btn_history05.jpg" alt="ブライトリング"></a></li>
                    <li><a rel="leanModal" href="#div_point06"><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/btn_history06.jpg" alt="ブレゲ"></a></li>
                </ul>

                <div id="div_point01" class="div_point">
                    <a class="modal_close" href="#"></a>
                    <h5>ROLEX（ロレックス）とは</h5>
                    <p>ROLEX（ロレックス）は1905年にウィルスドルフ＆デイビスという名前の時計商社として設立され
                        <br> 幾度と名前の変更を経て、現在のROLEX（ロレックス）となりました。
                    </p>
                    <p>ROLEX（ロレックス）と聞けば王冠マークをすぐに思い浮かべられるほど誰もが知る有名高級時計です。
                        <br> それほどの知名度を築くに至ったのもROLEX（ロレックス）が独自に開発をし、現在では多くの
                        <br> 自動巻き時計に採用されている数々の特許技術でしょう。
                    </p>
                    <p>まずはパーペチュアル機構です。それ以前にも自動巻きの機構は存在しましたが、ROLEX（ロレックス）の
                        <br> 開発したパーペチュアル機構（全回転機構）により自動巻き時計は完成となり、ROLEX（ロレックス）は
                        <br> ブランド自動巻き時計の第一号となったのです。
                    </p>
                    <p>その後もオイスターケースという防水仕様のケースの開発、デイトジャストと呼ばれる日付が0時ちょうどで
                        <br> 切り替わる機構など、数々の革新的な技術の開発を行ってきました。
                    </p>
                    <p>また、機構の開発だけに留まらず時計そのものとしてのクオリティーも細部に至るまでこだわり作られている
                        <br> こともROLEX（ロレックス）が現在の地位を獲得した要因でしょう。
                    </p>
                    <p>10年に1度はオーバーホールが必要ではありますが、きちんとしたメンテナンスを行っていれば正に一生ものと
                        <br> 呼ぶにふさわしい時計です。
                    </p>
                    <p>有名なモデルとして、デイトジャスト、サブマリーナ、デイトナ、GMTマスター、エクスプローラーⅠ、 デイトジャスト、ヨットマスター、ターノグラフ、エアキング、シードュエラ、ミルガウスなど
                    </p>
                </div>

                <div id="div_point02" class="div_point">
                    <a class="modal_close" href="#"></a>

                    <h5>PATEK PHILIPPE（パテックフィリップ）とは</h5>
                    <p>PATEK PHILIPPE（パテックフィリップ）は誰もが知るブランド時計では御座いません。
                        <br> しかし、世界三大高級時計として世界各国で愛されている最高級の時計ブランドです。
                    </p>
                    <p>PATEK PHILIPPE（パテックフィリップ）はカルティエやティファニー、ギュブラン、宝石店向けに製品を
                        <br> 納品していたこともあり、世界一高級な時計を販売するマニュファクチュールとして知られています。
                    </p>
                    <p>PATEK PHILIPPE（パテックフィリップ）はどんなに古い商品でも自社の物であれば修理可能と永久修理保証と
                        <br> 宣伝している為、PATEK PHILIPPE（パテックフィリップ）の時計は一生ものというイメージを確立しております。</p>
                    <p>有名なモデルとして、カラトラバ、コンプリケーテッド、グランドコンプリケオーション、アクアノート、ゴンドーロ、 トゥエンティー・フォー、ノーチラス、ゴールデンイリプスなど
                    </p>
                </div>

                <div id="div_point03" class="div_point">
                    <a class="modal_close" href="#"></a>

                    <h5>HUBLOT（ウブロ）とは</h5>
                    <p>HUBLOT（ウブロ）は1980年に誕生し、独特なベゼル、ケースデザインやベルトにラバーを採用するなど
                        <br> 独自の世界観で多くの時計ファンに衝撃を与えた高級時計ブランドです。
                    </p>
                    <p>ユニークなデザインの為、クラシックなものと違い派手さ、華やかさから多くのスポーツ選手や芸能人が
                        <br> 愛用しております。
                    </p>
                    <p>有名なモデルにクラシックフュージョン、ビッグバン、ビッグバンキング、キングパワーなど</p>
                </div>

                <div id="div_point04" class="div_point">
                    <a class="modal_close" href="#"></a>

                    <h5>AUDEMARS　PIGUET（オーデマピゲ）とは</h5>
                    <p>AUDEMARS　PIGUET（オーデマピゲ）は1875年ジュール＝ルイ・オーデマと
                        <br> 友人のエドワール＝オーギュスト・ピゲにより設立された高級時計メーカーです。
                    </p>
                    <p>お互いの技術力の高さから複雑時計の開発を始め、1889年に第10回パリ万博で
                        <br> スプリットセコンドグラフや永久カレンダーなどを装備する、グランド・コンプリカシオン
                        <br> 懐中時計を発表。1892年に世界で初めてのミニッツリピーターを搭載した腕時計の
                        <br> 独創的な製品を発表し「複雑度系のオーデマピゲ」として名声を獲得しております。
                    </p>
                    <p>現在でも、創業者一族が経営を引き継いでおり高い情熱と技術が継承されております。</p>
                    <p>厚さ1.62mmの世界最薄手巻きムーブメントや、1972年にはラグジュアリースポーツウォッチと
                        <br> いつ新カテゴリーを築いたロイヤルオークを発表し、誕生から40年をヘタ現在も高い人気を誇ります。
                    </p>
                    <p>有名なモデルにロイヤルオーク オフショア、ロイヤルオーク、ジュール・オーデマ、エドワール・ピゲ、ミレネリーなど</p>
                </div>

                <div id="div_point05" class="div_point">
                    <a class="modal_close" href="#"></a>

                    <h5>BREITLING（ブライトリング）とは</h5>
                    <p>BREITLING（ブライトリング）の歴史はクロノグラフの進化の歴史といっても過言ではありません。</p>
                    <p>創業者のレオン・ブライトリングは計測機器の開発に特化しており、各分野のプロ用クロノグラフを
                        <br> 製造。息子ガストンが会社を継ぎ、1915年に独立した専用プッシュボタンを備えた世界初の腕時計型
                        <br> クロノグラフの開発に成功。1934年にリセットボタンを開発し、現在のクロノグラフの基礎を築いた。
                    </p>
                    <p>そして、1936年にイギリス空軍とコックピットクロックの供給契約を皮切りに航空業界で勢力を拡大。
                        <br> 1952年に航空用回転計算尺を搭載した「ナビタイマー」によって、航空時計ナンバーワンブランドの
                        <br> 地位を確立した。
                    </p>
                    <p>有名なモデルにクロノマット、スーパーオーシャン、トランスオーシャン、ナビタイマー、ウィンドライダー、スカイレーサーなど</p>
                </div>

                <div id="div_point06" class="div_point">
                    <a class="modal_close" href="#"></a>

                    <h5>BREGUET（ブレゲ）とは</h5>
                    <p>BREGUET（ブレゲ）は1775年、パリにオープンした工房から始まった。</p>
                    <p>1780年にはブラビアン＝ルイ・ブレゲが自動巻き機構を開発。衝撃吸収装置、ブレゲヒゲゼンマイ、
                        <br> トゥールビヨン機構、スプリットセコンドクロノグラフなど、次々と画期的な機構を開発、ブレゲ針や
                        <br> ブレゲ数字、文字盤のギョ―シェ彫りもブレゲの発案によるものだ。
                    </p>
                    <p>天才と言われる彼の技術はマリー・アントワネット妃の注文により作られた超複雑時計No.160
                        <br> 「マリー・アントワネット」に集約されている。
                    </p>
                    <p>1823年にブレゲは死去するが、その技術は弟子たちに受け継がれ、経営がブラウン家、宝石商ショーメと
                        <br> 移るが、その間も数多くの複雑度系を世に送り出した。
                    </p>
                    <p>現在はスウォッチ・グループの傘下となり、安定した経営基盤を獲得し、自社ムーブメントの開発や
                        <br> シリコン素材の導入など、新境地を開き続けている。
                    </p>
                    <p>有名なモデルにタイプトゥエンティー、マリーン、クイーン・オブ・ネイプルズ、クラシック、トラディション、ヘリテージ、コンプリケーションなど</p>
                </div>

            </section>

            <section id="cat-point">
                <h3>高価買取のポイント</h3>
                <ul class="list-unstyled">
                    <li>
                        <p class="pt_tl">商品情報が明確だと査定がスムーズ</p>
                        <p class="pt_tx">ブランド名、モデル名が明確だと査定がスピーディに出来、買取価格にもプラスに働きます。また、新作や人気モデル、人気ブランドであれば買取価格がプラスになります。</p>
                    </li>
                    <li>
                        <p class="pt_tl">数点まとめての査定だと
                            <br>買取がスムーズ</p>
                        <p class="pt_tx">数点まとめての査定依頼ですと、買取価格をプラスさせていただきます。</p>
                    </li>
                    <li>
                        <p class="pt_tl">品物の状態がよいほど
                            <br>査定がスムーズ</p>
                        <p class="pt_tx">お品物の状態が良ければ良いほど、買取価格もプラスになります。</p>
                    </li>
                </ul>
                <p>腕時計の査定の際にぜひお持ちいただきたいのは、購入時に同封されていた保証書や明細書。
                    <br> そのモデルの種類、購入価格等がわかればよりスムーズに査定を行うことができます。購入時の箱や付属品のベルト等、付属品がそろっていれば、より査定金額が高くなるでしょう。
                    <br> もちろん、保証書や箱などの付属品がなくても時計そのものの価値は変わりませんのでご心配なく。ベルトがボロボロになっている場合なども、時計本体の状態によっては高い査定価格のご提示も可能です。
                    <br> 手軽に利用できるLINE査定などもありますので、ぜひお気軽にご相談ください。
                </p>
            </section>

            <section id="lp-cat-jisseki">
                <h3 class="text-hide">買取実績</h3>
                <!--<ul id="jisseki-type" class="clearfix">
<li><a href="">ブランド時計</a></li>
<li><a href="">高級装飾時計</a></li>
<li><a href="">懐中時計</a></li>
<li><a href="">置時計</a></li>
<li><a href="">アンティーク時計</a></li>
</ul>-->
                <ul id="box-jisseki" class="list-unstyled clearfix">
                    <?php
    foreach($resultLists as $list):
                // :: で分割
                $listItem = explode('::', $list);

                ?>
                        <li class="box-4">
                            <div class="title">
                                <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/watch/lp/<?php echo $listItem[0]; ?>" alt="">
                                <p class="itemName">
                                    <?php echo $listItem[1]; ?>
                                        <br>
                                        <?php echo $listItem[2]; ?>
                                </p>
                                <hr>
                                <p>
                                    <span class="red">A社</span>：
                                    <?php echo $listItem[3]; ?>円
                                        <br>
                                        <span class="blue">B社</span>：
                                        <?php echo $listItem[4]; ?>円
                                </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">
                                    <?php echo $listItem[5]; ?><span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>
                                    <?php echo $listItem[6]; ?>円</p>
                            </div>
                        </li>
                        <?php endforeach; ?>
                </ul>
            </section>

            <section id="list-brand" class="clearfix">
                <h3>ブランドリスト</h3>
                <ul class="list-unstyled">
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/rolex'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat1.jpg" alt="ロレックス"></dd>
                                <dt><span>ROLEX</span>ロレックス</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/breitling'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat2.jpg" alt="ブライトリング"></dd>
                                <dt><span>Breitling</span>ブライトリング</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/franckmuller'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat3.jpg" alt="フランクミューラー"></dd>
                                <dt><span>Franckmuller</span>フランクミューラー</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/iwc'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat4.jpg" alt="アイダブルシー"></dd>
                                <dt><span>IWC</span>アイダブルシー</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/hublot'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat5.jpg" alt="ウブロ"></dd>
                                <dt><span>HUBLOT</span>ウブロ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/omega'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat6.jpg" alt="オメガ"></dd>

                                <dt><span>OMEGA</span>オメガ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/breguet'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat7.jpg" alt="ブレゲ"></dd>
                                <dt><span>Breguet</span>ブレゲ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/gem/bulgari'); ?>">

                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat8.jpg" alt="ブルガリ"></dd>
                                <dt><span>BVLGALI</span>ブルガリ</dt>
                            </a>
                        </dl>

                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/gem/cartier'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat9.jpg" alt="カルティエ"></dd>
                                <dt><span>Cartier</span>カルティエ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat10.jpg" alt="ジラールペラゴ"></dd>
                            <dt><span>Girard-Perregaux</span>ジラールペラゴ</dt>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/audemarspiguet'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat11.jpg" alt="オーデマピゲ"></dd>
                                <dt><span>Audemarspiguet</span>オーデマピゲ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/alange-soehne'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat12.jpg" alt="ランゲ&ゾーネ"></dd>
                                <dt><span>A.lange-soehne</span>ランゲ&ゾーネ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/patek-philippe'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat13.jpg" alt="パテックフィリップ"></dd>
                                <dt><span>Patek Philippe</span>パテックフィリップ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/jaeger-lecoultre'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat14.jpg" alt="ジャガールクルト"></dd>
                                <dt><span>Jaeger Lecoultre</span>ジャガールクルト</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/gagamilano'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat15.jpg" alt="ガガミラノ"></dd>
                                <dt><span>GaGa MILANO</span>ガガミラノ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/rogerdubuis'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat16.jpg" alt="ロジェ・デュブイ"></dd>
                                <dt><span>RogerDubuis</span>ロジェ・デュブイ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/richardmille'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat17.jpg" alt="リシャール・ミル"></dd>
                                <dt><span>RichardMille</span>リシャール・ミル</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/vacheron'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat21.jpg" alt="ヴァシュロンコンスタンタン"></dd>
                                <dt style="font-size:9px;"><span>Vacheron Constatin</span>ヴァシュロンコンスタンタン</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/jacob'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat31.jpg" alt="ジェイコブ"></dd>
                                <dt><span>JACOB CO</span>ジェイコブ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/tagheuer'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat32.jpg" alt="タグホイヤー"></dd>
                                <dt><span>Tagheuer</span>タグホイヤー</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/hamilton'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat33.jpg" alt="ハミルトン"></dd>
                                <dt><span>Hamilton</span>ハミルトン</dt>
                            </a>
                        </dl>
                    </li>
<li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/panerai'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat34.jpg" alt="パネライ"></dd>
                                <dt><span>Panerai</span>パネライ</dt>
                            </a>
                        </dl>
                    </li>
<li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/blancpain'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat35.jpg" alt="ブランパン"></dd>
                                <dt><span>Blancpain</span>ブランパン</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat18.jpg" alt="ロンジン"></dd>
                            <dt><span>LONGINES</span>ロンジン</dt>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat19.jpg" alt="グラハム"></dd>
                            <dt><span>GRAHAM</span>グラハム</dt>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat20.jpg" alt="コルム"></dd>
                            <dt><span>CORUM</span>コルム</dt>
                        </dl>
                    </li>

                    <li>
                        <dl>
                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat22.jpg" alt="ジン"></dd>
                            <dt><span>SINN</span>ジン</dt>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/gem/boucheron'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat23.jpg" alt="ブシュロン"></dd>
                                <dt><span>Boucheron</span>ブシュロン</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/watch/seiko'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat25.jpg" alt="セイコー"></dd>
                                <dt><span>SEIKO</span>セイコー</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat26.jpg" alt="ジェラルドジェンダ"></dd>
                            <dt><span>Gerald Genta</span>ジェラルドジェンダ</dt>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat27.jpg" alt="ゼニス"></dd>
                            <dt><span>ZENITH</span>ゼニス</dt>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat28.jpg" alt="チュードル"></dd>
                            <dt><span>Tudor</span>チュードル</dt>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat29.jpg" alt="ボーム＆メルシェ"></dd>
                            <dt><span>Baume Mercier</span>ボーム＆メルシェ</dt>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat30.jpg" alt="ショパール"></dd>
                            <dt><span>Chopard</span>ショパール</dt>
                        </dl>
                    </li>

                </ul>
            </section>
            <section id="lp-cat-jisseki">

                <div id="konna">
                    <p class="example1">■風防割れ</p>
                    <p class="example2">■ベルトのよれ、劣化</p>
                    <p class="example3">■文字盤焼け</p>
                    <p class="text">その他：箱、ギャランティー、付属品無し、電池切れや故障による不動品でもお買取りいたします。</p>
                </div>
            </section>

            <section id="about_kaitori" class="clearfix">
                <?php
    // 買取について
    get_template_part('_widerange');
                                     get_template_part('_widerange2');
            ?>
            </section>



            <section class="clearfix">
                <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/bn_matomegai.png">
            </section>

            <?php
    // 買取基準
    get_template_part('_criterion');

                 // NGアイテム
                 get_template_part('_no_criterion');

                 // カテゴリーリンク
                 get_template_part('_category');
        ?>
                <section id="kaitori_genre">
                    <h3 class="text-hide">その他買取可能なジャンル</h3>
                    <p>【買取ジャンル】バッグ/ウエストポーチ/セカンドバック/トートバッグ/ビジネスバッグ/ボストンバッグ/クラッチバッグ/トランクケース/ショルダーバッグ/ポーチ/財布/カードケース/パスケース/キーケース/手帳/腕時計/ミュール/サンダル/ビジネスシューズ/パンプス/ブーツ/ペアリング/リング/ネックレス/ペンダント/ピアス/イアリング/ブローチ/ブレスレット/ライター/手袋/傘/ベルト/ペン/リストバンド/アンクレット/アクセサリー/サングラス/帽子/マフラー/ハンカチ/ネクタイ/ストール/スカーフ/バングル/カットソー/アンサンブル/ジャケット/コート/ブルゾン/ワンピース/ニット/シャツメンズ/毛皮/Tシャツ/キャミソール/タンクトップ/パーカー/ベスト/ポロシャツ/ジーンズ/スカート/スーツなど</p>
                </section>

                <?php
        // 買取方法
        get_template_part('_purchase');
        ?>

                    <section id="user_voice">
                        <h3>ご利用いただいたお客様の声</h3>

                        <p class="user_voice_text1">ちょうど家の整理をしていたところ、家のポストにチラシが入っていたので、ブランドリバリューに電話してみました。今まで買取店を利用したことがなく、不安でしたがとても丁寧な電話の対応とブランドリバリューの豊富な取り扱い品目を魅力に感じ、出張買取を依頼することにしました。 絶対に売れないだろうと思った、動かない時計や古くて痛んだバッグ、壊れてしまった貴金属のアクセサリーなども高額で買い取っていただいて、とても満足しています。古紙幣や絵画、食器なども買い取っていただけるとのことでしたので、また家を整理するときにまとめて見てもらおうと思います。
                        </p>

                        <h4>鑑定士からのコメント</h4>
                        <div class="clearfix">
                            <p class="user_voice_text2">家の整理をしているが、何を買い取ってもらえるか分からないから一回見に来て欲しいとのことでご連絡いただきました。 買取店が初めてで不安だったが、丁寧な対応に非常に安心しましたと笑顔でおっしゃって頂いたことを嬉しく思います。 物置にずっとしまっていた時計や、古いカバン、壊れてしまったアクセサリーなどもしっかりと価値を見極めて高額で提示させて頂きましたところ、お客様もこんなに高く買取してもらえるのかと驚いておりました。 これからも家の不用品を整理するときや物の価値判断ができないときは、すぐにお電話しますとおっしゃって頂きました。
                            </p>

                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/photo-user-voice.jpg" class="right">
                        </div>
                    </section>

<section>
  <div class="cont_widerange">
              <h4>時計の買取取扱ブランドとは？<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
              <ul>
                  <li>時計はただの時間を知るためのツールではなく、その時計を身に着ける人の人生も一緒に刻む大切なツールでもあります。そのため、時計を選ぶ時は、誰もが「一生モノの時計に出会いたい」と思いながら探すことも少なくありません。そのため、一生モノの時計として世界的にも有名時計ブランドから発表されているコレクションやデザインを選ぶ人も多いでしょう。しかし、時計は世界屈指に挙げられる時計ブランドはもちろん、使用される材質やデザインなどによって提示される定価が大変高額になる場合も多く、時には1000万円単位の超高額時計もあります。超高額な時計には誰もが憧れるブランド時計が多いのですが、誰でも気軽に購入できる価格ではありません。そのため、どうしてもその時計を手に入れたい人は、中古市場で探すこともあるのです。<br>
中古市場には数多くのブランド時計が流通しており、ブランド、デザイン、コレクション、ヴァリエーションも多く幅広い品揃えとなっています。その理由として、時計のサイクルは意外にも早く、着ける年代や好み、ライフスタイルの変化によって手放す人が多いことが挙げられます。そのため、中古市場には、すでに廃盤になったレアな高級ブランド時計が流通していることもあり、時計コレクターだけでなく欲しかった時計モデルが見つかる場合もあるのです。<br>
一方、時計を売却したい人にとっては、いま手元にあるブランド時計は買取取扱いブランドかといった点は気になるところでしょう。もっとも代表的な取扱いブランドは世界三大高級腕時計ブランドとしてその名を知られている老舗ブランドのパテック・フィリップ、ヴァシュロン・コンスタンタン、オーデマ・ピゲをはじめ、IWC、ロレックス、フランクミュラー、プレゲ、タグ・ホイヤー、オメガ、ウブロなどが時計の買取取扱ブランドは幅広くあります。そのほかジュエリーブランドのピアジェやカルティエ、ファッションブランドのシャネルやエルメス、グッチの時計も人気が高く、時計の買取取扱いブランドとなっています。<br><br>
</li>
              </ul>
          </div>
  <div class="cont_widerange">
              <h4>気になる！時計の高額査定となるコツとは？<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
              <ul>
                  <li>世界にも名前が知られているほどの有名時計ブランドの時計は高額な定価のものが多く、それなりの値段を覚悟して購入することがほとんどです。しかし、時計を新調したり、好みや年代の変化などによって身に付ける頻度も少なくなってくると、愛用してきた時計を手放す人もいます。その場合、ブランド買取店で査定してもらうとかなりの高額査定になることも多いため持ち込む人も多いようです。<br>
ブランド時計を査定するには、時計に関してだけではなくブランドに関する歴史や人気コレクションなどの知識、そして中古市場の動向や人気の傾向などの情報が必要になってきます。ブランド時計の買取を取扱うブランド買取ショップには、知識や情報、そして取扱い経験が豊富な専門の鑑定士がおり、買取希望の時計を丁寧に、そして適正に査定するため、そのブランド時計の価値を査定金額で証明してくれます。<br>
時計を売却する人にとっては少しでも高額査定をしてもらって、高くかいとってもらいたいものです。しかし高額査定してもらうためには、売却する人もそれなりの準備が必要となってきます。<br>
高額査定となる大前提として、時計に大きな傷や痛みはないか、正常に時を刻んでいるか、メンテナンスはきちんとしているかといったコンディション面は査定に出す前にしっかりチェックしておく必要があります。何よりコンディションの良さは高額査定に直接繋がりやすいため、しっかり確認しておきましょう。電池切れなどでも問題ないと言う買取店もありますが、一見して状態良く動いているだけでも高額査定のポイントになるので、簡単なメンテナンスは事前にしておくと良いでしょう。<br>
また、購入時に発行されたギャランティーカードやオーバーホールの保証書、過去の修理完了などの証明書、ボックス、ブレスのコマ、替えのベルトなど細かな付属品に至るまでしっかりと揃えておくと高額査定になる場合もあります。また、中古時計にも人気やブームがあるため、使わなくなったら早めに査定買取に出すことも高額査定してもらうコツと言えるでしょう。<br><br>
</li>
              </ul>
          </div>
  <div class="cont_widerange">
              <h4>時計買取で人気ブランドとは？<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
              <ul>
                  <li>「時計との出会いは運命みたいなものだ」と言われることもある位、時計を選ぶということはとても意味があるのです。一度にいくつもの時計を身に着けることはあまりないからこそ、１本の時計への思い入れは大きくなるのかもしれません。<br>
時計は時を刻むツール以上に、常に手元に着けておくツールであるだけに人は自分の人生の一部のように感じることのできるツールでもあるのです。だからこそ時計を選ぶ時は自分の相棒を選ぶように、慎重に、そして「これ」と決めた運命の一本を選ぶのだと例えられるのでしょう。また、時計にはそのブランド身に着ける人のステータスを表すともいわれています。そのため、フォーマルシーンやビジネスシーンでは一定の知名度のある高級腕時計は欠かせないものであり、社交ツールとしても不可欠と言えるでしょう。<br>
中古市場にも有名時計はたくさん流通していますが、中古市場での時計人気ブランドとなるとやはり一定の人気ブランドに偏る傾向があるようです。世界三大時計ブランドと言われるパテック・フィリップ、ヴァシュロン・コンスタンタン、オーデマ・ピゲの３大老舗ブランドを頂点に、ロレックス、フランクミュラー、IWC、プレゲ、タグ・ホイヤー、オメガ、ウブロ、ガガミラノなどが比較的新しい時計ブランドも人気ブランドとして、その名を連ねています。特に、長年に渡り国内におけるロレックスの人気は、他の国々に比べて高いと言えるでしょう。一度は手に入れてみたい時計として人気ブランドなのではないでしょうか。<br>
その他にも時計を専門としたブランドの他にも人気ブランドは数多くあります。たとえばジュエリーブランドの時計やファッションブランドから発表されている時計も人気ブランドとして高い支持を得ています。ピアジェやヴァンクリーフ＆アーペル、カルティエ、ブルガリなどのジュエリーブランド、シャネル、エルメス、グッチなどのファッションブランドの時計も人気が高く、中古市場でも人気ブランドとして知られています。
</li>
              </ul>
          </div>
</section>

        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->

    <?php
    get_sidebar();
                     get_footer();