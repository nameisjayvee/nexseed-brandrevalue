<?php
    /**
     * The sidebar containing the main widget area.
     *
     * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
     *
     * @package BRING
     */
?>

<div id="secondary" class="widget-area" role="complementary">
    <section id="side-purchase">
        <ul class="list-unstyled kaitori_bnr">
            <li>
                <a href="<?php echo home_url('about-purchase/takuhai'); ?>">
                    <p>最短で翌日ご連絡&amp;ご入金</p>
                    <p>宅配買取</p>
                    <p>詳しくはこちら&nbsp;&nbsp;></p>
                </a>
            </li>
            <li>
                <a href="<?php echo home_url('about-purchase/syutchou'); ?>">
                    <p>その場で査定、即現金払い</p>
                    <p>出張買取</p>
                    <p>詳しくはこちら&nbsp;&nbsp;></p>
                </a>
            </li>
            <li>
                <a href="<?php echo home_url('about-purchase/tentou'); ?>">

                    <p>その場で査定、即現金払い</p>
                    <p>店頭買取</p>
                    <p>詳しくはこちら&nbsp;&nbsp;></p>
                </a>
            </li>
            <!--                 <li>
                 <a href="<?php echo home_url('about-purchase/tebura'); ?>">

                 <p>先に郵送、来店でお支払い</p>
                 <p>店頭郵送買取</p>
                 <p>詳しくはこちら&nbsp;&nbsp;></p>
                 </a>
                 </li> -->
        </ul>
        <div class="side_cvitem">
            <a href="<?php echo home_url('customer'); ?>">
                <p>0120-970-060</p>
            </a>
        </div>
    </section>
    <section class="side_cvbox">
        <h2>無料査定のご案内</h2>
        <ul class="side_cvlist">
            <li><a href="<?php echo home_url('customer'); ?>">電話査定</a></li>
            <li><a href="<?php echo home_url('line'); ?>">LINE査定</a></li>
            <li><a href="<?php echo home_url('purchase1'); ?>">メール査定</a></li>
        </ul>

    </section>
    <section id="side-categorylist">
        <h3 class="side_ttl">取り扱いカテゴリー</h3>
        <nav class="side_on" id="side_on">
            <ul class="list-unstyled">
                <li class="new_side gold">
                    <a href="<?php echo home_url('cat/gold'); ?>">
                        <p>金・プラチナ<span>GOLD・PLATINUM</span></p>
                    </a>
                    <div>
                        <ul>
                            <li><a href="<?php echo home_url('/cat/gold'); ?>">・金貨</a></li>
                            <li><a href="<?php echo home_url('/cat/gold/14k'); ?>">・14金</a></li>
                            <li><a href="<?php echo home_url('/cat/gold/18k'); ?>">・18金</a></li>
                            <li><a href="<?php echo home_url('/cat/gold/22k'); ?>">・22金</a></li>
                            <li><a href="<?php echo home_url('/cat/gold/24k'); ?>">・24金</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/ingot'); ?>">・インゴット</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/white-gold'); ?>">・ホワイトゴールド</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/pink-gold'); ?>">・ピンクゴールド</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/yellow-gold'); ?>">・イエローゴールド</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/platinum'); ?>">・プラチナ</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/silver'); ?>">・シルバー</a></li>
                        </ul>
                    </div>
                </li>
                <li class="new_side gem">
                    <a href="<?php echo home_url('cat/gem'); ?>">
                        <p>宝石<span>GEM</span></p>
                    </a>
                    <div>
                        <ul>
                            <li><a href="<?php echo home_url('/cat/gem/emerald'); ?>">・エメラルド</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/opal'); ?>">・オパール</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/sapphire'); ?>">・サファイア</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/hisui'); ?>">・ヒスイ</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/cartier'); ?>">・カルティエ</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/tourmaline'); ?>">・トルマリン</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/paraibatourmaline'); ?>">・パライバトルマリン</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/piaget'); ?>">・ピアジェ</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/boucheron'); ?>">・ブシュロン</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/ruby'); ?>">・ルビー</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/vancleefarpels'); ?>">・ヴァンクリーフ&amp;アーペル</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/harrywinston'); ?>">・ハリーウィンストン</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/bulgari'); ?>">・ブルガリ</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/tiffany'); ?>">・ティファニー</a></li>
                        </ul>
                    </div>
                </li>
                <li class="new_side watch">
                    <a href="<?php echo home_url('cat/watch'); ?>">
                        <p>時計<span>BRAND WATCH</span></p>
                    </a>
                    <div>
                        <ul>
                            <li><a href="<?php echo home_url('/cat/watch/rolex'); ?>">・ロレックス</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/iwc'); ?>">・IWC</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/hublot'); ?>">・HUBLOT</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/omega'); ?>">・オメガ</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/panerai'); ?>">・パネライ</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/breitling'); ?>">・ブライトリング</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/franckmuller'); ?>">・フランクミュラー</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/breguet'); ?>">・ブレゲ</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/audemarspiguet'); ?>">・オーデマピゲ</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/patek-philippe'); ?>">・パテックフィリップ</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/jaeger-lecoultre'); ?>">・ジャガールクルト</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/jacob'); ?>">・ジェイコブ</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/vacheron'); ?>">・ヴァシュロンコンスタンタン</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/seiko'); ?>">・セイコー</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/gagamilano'); ?>">・ガガミラノ</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/alange-soehne'); ?>">・ランゲ&amp;ゾーネ</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/richardmille'); ?>">・リシャール・ミル</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/rogerdubuis'); ?>">・ロジェ・デュブイ</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/tagheuer'); ?>">・タグホイヤー</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/hamilton'); ?>">・ハルミトン</a></li>
                            <li><a href="<?php echo home_url('/cat/watch/blancpain'); ?>">・ブランパン</a></li>
                        </ul>
                    </div>
                </li>
                <li class="new_side bag">
                    <a href="<?php echo home_url('cat/bag'); ?>">
                        <p>バッグ<span>BAG</span></p>
                    </a>
                    <div>
                        <ul>
                            <li><a href="<?php echo home_url('/cat/bag/hermes'); ?>">・エルメス</a></li>
                            <li><a href="<?php echo home_url('/cat/wallet/louisvuitton'); ?>">・ルイヴィトン</a></li>
                            <li><a href="<?php echo home_url('/cat/bag/gucci'); ?>">・グッチ</a></li>
                            <li><a href="<?php echo home_url('/cat/bag/prada'); ?>">・プラダ</a></li>
                            <li><a href="<?php echo home_url('/cat/bag/chanel'); ?>">・シャネル</a></li>
                            <li><a href="<?php echo home_url('/cat/bag/saint_laurent'); ?>">・サンローラン</a></li>
                            <li><a href="<?php echo home_url('/cat/bag/celine'); ?>">・セリーヌ</a></li>
                            <li><a href="<?php echo home_url('/cat/bag/dior'); ?>">・ディオール</a></li>
                            <li><a href="<?php echo home_url('/cat/bag/ferragamo'); ?>">・フェラガモ</a></li>
                            <li><a href="<?php echo home_url('/cat/bag/fendi'); ?>">・フェンディ</a></li>
                            <li><a href="<?php echo home_url('/cat/bag/bottegaveneta'); ?>">・ボッテガ</a></li>
                            <li><a href="<?php echo home_url('/cat/bag/loewe'); ?>">・ロエベ</a></li>
                        </ul>
                    </div>
                </li>
                <li class="new_side outfit">
                    <a href="<?php echo home_url('cat/outfit'); ?>">
                        <p>洋服・毛皮<span>OUTFIT</span></p>
                    </a>
                </li>
                <li class="new_side wallet">
                    <a href="<?php echo home_url('cat/wallet'); ?>">
                        <p>ブランド財布<span>BRAND WALLET</span></p>
                    </a>
                    <div>
                        <ul>
                            <li><a href="<?php echo home_url('/cat/wallet/louisvuitton'); ?>">・ヴィトン</a></li>
                            <li><a href="<?php echo home_url('/cat/bag/chanel'); ?>">・シャネル</a></li>
                            <li><a href="<?php echo home_url('/cat/bag/hermes'); ?>">・エルメス</a></li>
                            <li><a href="<?php echo home_url('/cat/bag/prada'); ?>">・プラダ</a></li>
                            <li><a href="<?php echo home_url('/cat/bag/bottegaveneta'); ?>">・ボテッガ</a></li>
                        </ul>
                    </div>
                </li>
                <li class="new_side shoes">
                    <a href="<?php echo home_url('cat/shoes'); ?>">
                        <p>ブランド靴<span>SHOES</span></p>
                    </a>
                </li>
                <li class="new_side dia">
                    <a href="<?php echo home_url('cat/diamond'); ?>">
                        <p>ダイヤモンド<span>DIAMOND</span></p>
                    </a>
                    <div>
                        <ul>
                            <li><a href="<?php echo home_url('/cat/gem/tiffany'); ?>">・ティファニー</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/cartier'); ?>">・カルティエ</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/harrywinston'); ?>">・ハリーウィンストン</a></li>
                        </ul>
                    </div>
                </li>
                <li class="new_side kimono">
                    <a href="<?php echo home_url('kimono'); ?>">
                        <p>着物<span>KIMONO</span></p>
                    </a>
                </li>
                <li class="new_side antique">
                    <a href="<?php echo home_url('introduction/antique'); ?>">
                        <p>骨董品<span>ANTIQUE</span></p>
                    </a>
                </li>
                <li class="new_side memento">
                    <a href="http://kaitorisatei.info/brandrevalue/cat/mement">
                        <p>遺品買取<span>Memento</span></p>
                    </a>
                </li>
                <li class="new_side bj">
                    <a href="<?php echo home_url('cat/brandjewelery'); ?>">
                        <p>ブランドジュエリー
                            <span>Brand jewelry</span></p>
                    </a>
                </li>
                <li class="new_side anti_rolex">
                    <a href="<?php echo home_url('cat/antique_rolex'); ?>">
                        <p>アンティークロレックス<span>Antique ROLEX</span></p>
                    </a>
                </li>
            </ul>
        </nav>
    </section>
    <section id="side-categorylist">

        <?php if (is_parent_slug() === 'patek-philippe'|| is_page('patek-philippe')) { ?>
            <h3 class="side_ttl">ﾊﾟﾃｯｸﾌｨﾘｯﾌﾟ買取強化モデル</h3>
            <ul class="side_br_list">
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/aquanote'); ?>">アクアノート</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/calatrava_pilot'); ?>">カラトラバ・パイロット</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/calatrava'); ?>">カラトラバ</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/p_chronograph'); ?>">クロノグラフ</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/grand-complication'); ?>">グランド・コンプリケーション</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/complication'); ?>">コンプリケーション</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/gondolo'); ?>">ゴンドーロ</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/twenty_four'); ?>">トゥエンティーフォー</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/nautilus'); ?>">ノーチラス</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/retrograde'); ?>">レトログラード永久カレンダー</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/world_time'); ?>">ワールドタイム</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/annualcalendar'); ?>">年次カレンダー</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/forever'); ?>">永久カレンダー</a></li>
                <li><a href="<?php echo home_url('/cat/watch/patek-philippe/forever_chrono'); ?>">永遠カレンダークロノグラフ</a></li>
                <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
            </ul>
            <?php } elseif (is_parent_slug() === 'franckmuller'|| is_page('franckmuller')) { ?>
                <h3 class="side_ttl">フランクミュラー買取強化モデル</h3>
                <ul class="side_br_list">
                    <li><a href="<?php echo home_url('/cat/watch/franckmuller/casablanca'); ?>">カサブランカ</a></li>
                    <li><a href="<?php echo home_url('/cat/watch/franckmuller/color_dream'); ?>">カラードリーム</a></li>
                    <li><a href="<?php echo home_url('/cat/watch/franckmuller/kureijiawas'); ?>">クレイジーアワーズ</a></li>
                    <li><a href="<?php echo home_url('/cat/watch/franckmuller/conquistador_cortez'); ?>">コンキスタドールコルテス</a></li>
                    <li><a href="<?php echo home_url('/cat/watch/franckmuller/conquistador'); ?>">コンキスタドール</a></li>
                    <li><a href="<?php echo home_url('/cat/watch/franckmuller/tonneau_car_becks'); ?>">トノーカーベックス</a></li>
                    <li><a href="<?php echo home_url('/cat/watch/franckmuller/master_square'); ?>">マスタースクエア</a></li>
                    <li><a href="<?php echo home_url('/cat/watch/franckmuller/master_bunker'); ?>">マスターバンカー</a></li>
                    <li><a href="<?php echo home_url('/cat/watch/franckmuller/long_island'); ?>">ロングアイランド</a></li>
                    <li><a href="<?php echo home_url('/cat/watch/franckmuller/vegas'); ?>">ヴェガス</a></li>
                    <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
                </ul>
            <?php } elseif (is_parent_slug() === 'vancleefarpels'|| is_page('vancleefarpels')) { ?>
                    <h3 class="side_ttl">ｳﾞｧﾝｸﾘｰﾌ&amp;ｱｰﾍﾟﾙ買取強化ライン</h3>
                    <ul class="side_br_list">
                        <li><a href="<?php echo home_url('/cat/gem/vancleefarpels/alhambra'); ?>">アルハンブラ</a></li>
                        <li><a href="<?php echo home_url('/cat/gem/vancleefarpels/icone'); ?>">イコーヌ</a></li>
                        <li><a href="<?php echo home_url('/cat/gem/vancleefarpels/vintage_alhambra'); ?>">ヴィンテージアルハンブラ</a></li>
                        <li><a href="<?php echo home_url('/cat/gem/vancleefarpels/suite_alhambra'); ?>">スイートアルハンブラ</a></li>
                        <li><a href="<?php echo home_url('/cat/gem/vancleefarpels/treffle'); ?>">トレフル</a></li>
                        <li><a href="<?php echo home_url('/cat/gem/vancleefarpels/frievor'); ?>">フリヴォル</a></li>
                        <li><a href="<?php echo home_url('/cat/gem/vancleefarpels/bonheur'); ?>">ボヌール</a></li>
                        <li><a href="<?php echo home_url('/cat/gem/vancleefarpels/lucky_alhambra'); ?>">ラッキーアルハンブラ</a></li>
                        <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
                    </ul>
            <?php } elseif (is_parent_slug() === 'harrywinston'|| is_page('harrywinston')) { ?>
                        <h3 class="side_ttl">ﾊﾘｰｳｨﾝｽﾄﾝ買取強化ライン</h3>
                        <ul class="side_br_list">
                            <li><a href="<?php echo home_url('/cat/gem/harrywinston/hw_ring'); ?>">HWリング</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/harrywinston/with_love_from'); ?>">ウィズラブフロムハリーウィンストン</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/harrywinston/geometric'); ?>">ジオメトリック</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/harrywinston/solitaire'); ?>">ソリティア</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/harrywinston/traffic'); ?>">トラフィック</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/harrywinston/trist'); ?>">トリスト</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/harrywinston/bell'); ?>">ベル</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/harrywinston/micropave'); ?>">マイクロパヴェ</a></li>
                            <li><a href="<?php echo home_url('/cat/gem/harrywinston/lily_cluster'); ?>">リリークラスター</a></li>
                            <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
                        </ul>
            <?php } elseif (is_parent_slug() === 'piaget' || is_page('piaget')) { ?>
                            <h3 class="side_ttl">ピアジェ買取強化ライン</h3>
                            <ul class="side_br_list">
                                <li><a href="<?php echo home_url('/cat/gem/piaget/upstream'); ?>">アップストリーム</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/piaget/altiplano'); ?>">アルティプラノ</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/piaget/tanagra'); ?>">タナグラ</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/piaget/dancer'); ?>">ダンサー</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/piaget/poseshon'); ?>">ポセション</a></li>
                                <li><a href="<?php echo home_url('/cat/gem/piaget/limelight'); ?>">ライムライト</a></li>
                                <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
                            </ul>
            <?php } elseif (is_parent_slug() === 'bulgari'|| is_page('bulgari')) { ?>
                                <h3 class="side_ttl">ブルガリ買取強化ライン</h3>
                                <ul class="side_br_list">
                                    <li><a href="<?php echo home_url('/cat/gem/bulgari/b-zero1'); ?>">B-ZERO1</a></li>
                                    <li><a href="<?php echo home_url('/cat/gem/bulgari/asutorare'); ?>">アストラーレ</a></li>
                                    <li><a href="<?php echo home_url('/cat/gem/bulgari/arubeare'); ?>">アルベアーレ</a></li>
                                    <li><a href="<?php echo home_url('/cat/gem/bulgari/corona'); ?>">コロナ</a></li>
                                    <li><a href="<?php echo home_url('/cat/gem/bulgari/serpenti'); ?>">セルペンティ</a></li>
                                    <li><a href="<?php echo home_url('/cat/gem/bulgari/cheruki'); ?>">チェルキ</a></li>
                                    <li><a href="<?php echo home_url('/cat/gem/bulgari/tondo'); ?>">トンド</a></li>
                                    <li><a href="<?php echo home_url('/cat/gem/bulgari/passodoppio'); ?>">パッソドッピオ</a></li>
                                    <li><a href="<?php echo home_url('/cat/gem/bulgari/parenteshi'); ?>">パレンテシ</a></li>
                                    <li><a href="<?php echo home_url('/cat/gem/bulgari/piramidering'); ?>">ピラミデ</a></li>
                                                   <li><a href="<?php echo home_url('/cat/gem/bulgari/flower_collection'); ?>">フラワーコントレール</a></li>
                                    <li><a href="<?php echo home_url('/cat/gem/bulgari/prestige'); ?>">プレステージ</a></li>
                                    <li><a href="<?php echo home_url('/cat/gem/bulgari/horoscope'); ?>">ホロスコープ</a></li>
                                    <li><a href="<?php echo home_url('/cat/gem/bulgari/marimi'); ?>">マリーミー</a></li>
                                    <li><a href="<?php echo home_url('/cat/gem/bulgari/latincross'); ?>">ラテンクロス</a></li>
                                    <li><a href="<?php echo home_url('/cat/gem/bulgari/tubogasu'); ?>">トゥボガス</a></li>
                                    <li><a href="<?php echo home_url('/cat/gem/bulgari/doppio_bachurato'); ?>">ドッピオバチェラート</a></li>
                                    <li><a href="<?php echo home_url('/cat/gem/bulgari/torcello'); ?>">トルチェッロ</a></li>
                                    <li><a href="<?php echo home_url('/cat/gem/bulgari/troncet'); ?>">トロンケット</a></li>
                                    <li><a href="<?php echo home_url('/cat/gem/bulgari/bulgari-bulgari'); ?>">ブルガリブルガリ</a></li>
                                    <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
                                </ul>
            <?php } elseif (is_parent_slug() === 'tiffany'|| is_page('tiffany')) { ?>
                                    <h3 class="side_ttl">ティファニー買取強化ライン</h3>
                                    <ul class="side_br_list">
                                        <li><a href="<?php echo home_url('/cat/gem/tiffany/t-narrow-chain'); ?>">Tナローチェーン</a></li>
                                        <li><a href="<?php echo home_url('/cat/gem/tiffany/t_wire'); ?>">Tワイヤー・Tスクウェア</a></li>
                                        <li><a href="<?php echo home_url('/cat/gem/tiffany/atlas'); ?>">アトラス</a></li>
                                        <li><a href="<?php echo home_url('/cat/gem/tiffany/solitaire'); ?>">ソリテール</a></li>
                                        <li><a href="<?php echo home_url('/cat/gem/tiffany/harmony'); ?>">ハーモニー</a></li>
                                        <li><a href="<?php echo home_url('/cat/gem/tiffany/baizayado'); ?>">バイザヤード</a></li>
                                        <li><a href="<?php echo home_url('/cat/gem/tiffany/victoria'); ?>">ビクトリア</a></li>
                                        <li><a href="<?php echo home_url('/cat/gem/tiffany/ribbondiamond'); ?>">リボンダイヤ</a></li>
                                        <li><a href="<?php echo home_url('/cat/gem/tiffany/teardrop'); ?>">ティアドロップ</a></li>
                                        <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
                                    </ul>
            <?php } elseif (is_parent_slug() === 'cartier'|| is_page('cartier')) { ?>
                                        <h3 class="side_ttl">カルティエ買取強化ライン</h3>
                                        <ul class="side_br_list">
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-essential'); ?>">エッセンシャルライン</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-etoriru'); ?>">エトリール</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier-caresse-dorchidees-par-cartier'); ?>">カレスドルキデパルカルティエ</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-juste'); ?>">ジュストアンクル</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-sucre'); ?>">スクレドゥブドゥワール</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-solitaire'); ?>">ソリテール</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-diadea'); ?>">ディアデア</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-nijeria'); ?>">ニジェリア</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-ballerina'); ?>">バレリーナ</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-bamboo'); ?>">バンブー</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-paris-nouvelle-vague'); ?>">ヌーベルバーグ</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-panther'); ?>">パンサー</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-panthere'); ?>">パンテール</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-penelope'); ?>">ペネロープ</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-maiyon'); ?>">マイヨンパンテール</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-yourmine'); ?>">ユアマイン</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-lacaruda'); ?>">ラカルダ</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-lovebrace'); ?>">ラブブレス</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-lovering'); ?>">ラブリング</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-dragonring'); ?>">ベゼデュドラゴンリング</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-lanieres'); ?>">ラニエール</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-trinity'); ?>">トリニティ</a></li>
                                            <li><a href="<?php echo home_url('/cat/gem/cartier/cartier-diamanlegacycartier'); ?>">ディアマンレジェドゥカルティエ</a></li>
                                            <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
                                        </ul>
            <?php } elseif (is_parent_slug() === 'omega'|| is_page('omega')) { ?>
                                            <h3 class="side_ttl">オメガ買取強化モデル</h3>
                                            <ul class="side_br_list">
                                                <li><a href="<?php echo home_url('/cat/watch/omega/omega-constellatio'); ?>">コンステレーション</a></li>
                                                <li><a href="<?php echo home_url('/cat/watch/omega/omega-seamaster'); ?>">シーマスター</a></li>
                                                <li><a href="<?php echo home_url('/cat/watch/omega/omega-speedmaster'); ?>">スピードマスター</a></li>
                                                <li><a href="<?php echo home_url('/cat/watch/omega/omega-de-ville'); ?>">デヴィル</a></li>
                                                <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
                                            </ul>
            <?php } elseif (is_parent_slug() === 'zenith'|| is_page('zenith')) { ?>
                                                <h3 class="side_ttl">ゼニス買取強化モデル</h3>
                                                <ul class="side_br_list">
                                                    <li><a href="<?php echo home_url('/cat/watch/zenith/zenith-chronomaster'); ?>">クロノマスター</a></li>
                                                    <li><a href="<?php echo home_url('/cat/watch/zenith/zenith-pilot'); ?>">パイロット</a></li>
                                                    <li><a href="<?php echo home_url('/cat/watch/zenith/zenith-elite-classic'); ?>">エリート クラシック</a></li>
                                                    <li><a href="<?php echo home_url('/cat/watch/zenith/zenith-elite-captain'); ?>">エリート キャプテン</a></li>
                                                    <li><a href="<?php echo home_url('/cat/watch/zenith/zenith-elite-ultrathin'); ?>">エリート ウルトラシン</a></li>
                                                    <li><a href="<?php echo home_url('/cat/watch/zenith/zenith-defy'); ?>">デファイ</a></li>
                                                    <li><a href="<?php echo home_url('/cat/watch/zenith/zenith-elprimero'); ?>">エルプリメロ</a></li>
                                                    <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
                                                </ul>
            <?php } elseif (is_parent_slug() === 'rolex'|| is_page('rolex')) { ?>
                                                    <h3 class="side_ttl">ロレックス買取強化モデル</h3>
                                                    <ul class="side_br_list">
                                                        <li><a href="<?php echo home_url('/cat/watch/rolex/rolex-gmt-master2'); ?>">GMTマスターⅡ</a></li>
                                                        <li><a href="<?php echo home_url('/cat/watch/rolex/rolex-explorer'); ?>">エクスプローラーⅠ</a></li>
                                                        <li><a href="<?php echo home_url('/cat/watch/rolex/rolex-explorer2'); ?>">エクスプローラーⅡ</a></li>
                                                        <li><a href="<?php echo home_url('/cat/watch/rolex/rolex-oyster-perpetual-date'); ?>">オイスター・パーペチュアル・デイト</a></li>
                                                        <li><a href="<?php echo home_url('/cat/watch/rolex/rolex-submariner'); ?>">サブマリーナ</a></li>
                                                        <li><a href="<?php echo home_url('/cat/watch/rolex/rolex-seadweller'); ?>">シードゥエラー</a></li>
                                                        <li><a href="<?php echo home_url('/cat/watch/rolex/rolex-sky-dweller'); ?>">スカイドゥエラー</a></li>
                                                        <li><a href="<?php echo home_url('/cat/watch/rolex/rolex-datejust2'); ?>">デイトジャストⅡ</a></li>
                                                        <li><a href="<?php echo home_url('/cat/watch/rolex/rolex-thunderbird'); ?>">デイトジャスト　サンダーバード</a></li>
                                                        <li><a href="<?php echo home_url('/cat/watch/rolex/rolex-datejust'); ?>">デイトジャスト</a></li>
                                                        <li><a href="<?php echo home_url('/cat/watch/rolex/rolex-datejust-turn-o-graph'); ?>">デイトジャスト　ターノグラフ</a></li>
                                                        <li><a href="<?php echo home_url('/cat/watch/rolex/rolex-daytona'); ?>">デイトナ</a></li>
                                                        <li><a href="<?php echo home_url('/cat/watch/rolex/rorex-milgauss'); ?>">ミルガウス</a></li>
                                                        <li><a href="<?php echo home_url('/cat/watch/rolex/rolex-yacht-master'); ?>">ヨットマスター</a></li>
                                                        <li><a href="<?php echo home_url('/cat/watch/rolex/rolex-yacht-master2'); ?>">ヨットマスターⅡ</a></li>
                                                        <li><a href="<?php echo home_url('/cat/watch/rolex/rolex-airkings'); ?>">エアキング</a></li>
                                                        <li><a href="<?php echo home_url('/cat/watch/rolex/daydate'); ?>">デイデイト</a></li>
                                                        <li><a href="<?php echo home_url('/cat/watch/rolex/daydate2'); ?>">デイデイトⅡ</a></li>
                                                        <li><a href="<?php echo home_url('/cat/watch/rolex/cellini'); ?>">チェリーニ</a></li>
                                                        <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
                                                    </ul>
            <?php } elseif (is_page(array('watch', 'iwc', 'hublot','panerai','breguet','audemarspiguet','jaeger-lecoultre','jacob','vacheron','seiko','gagamilano','alange-soehne','richardmille','rogerdubuis','tagheuer','hamilton','blancpain'))) { ?>
                                                        <h3 class="side_ttl">時計の買取強化ブランド</h3>
                                                        <ul class="side_br_list">
                                                            <li><a href="<?php echo home_url('/cat/watch/rolex'); ?>">ロレックス</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/iwc'); ?>">IWC</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/hublot'); ?>">HUBLOT</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/omega'); ?>">オメガ</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/panerai'); ?>">パネライ</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/breitling'); ?>">ブライトリング</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/franckmuller'); ?>">フランクミュラー</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/breguet'); ?>">ブレゲ</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/audemarspiguet'); ?>">オーデマピゲ</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/patek-philippe'); ?>">パテックフィリップ</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/jaeger-lecoultre'); ?>">ジャガールクルト</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/jacob'); ?>">ジェイコブ</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/vacheron'); ?>">ヴァシュロンコンスタンタン</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/seiko'); ?>">セイコー</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/gagamilano'); ?>">ガガミラノ</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/alange-soehne'); ?>">ランゲ&amp;ゾーネ</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/richardmille'); ?>">リシャール・ミル</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/rogerdubuis'); ?>">ロジェ・デュブイ</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/tagheuer'); ?>">タグホイヤー</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/hamilton'); ?>">ハルミトン</a></li>
                                                            <li><a href="<?php echo home_url('/cat/watch/blancpain'); ?>">ブランパン</a></li>
                                                            <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
                                                        </ul>
            <?php } elseif (is_page(array('bag', 'fendi-bag', 'dior-bag','saint_laurent','bottegaveneta','ferragamo','loewe','berluti','goyal','rimowa','globe-trotter','valextra'))) { ?>
                                                            <ul class="side_br_list">
                                                                <h3 class="side_ttl">バッグの買取強化ブランド</h3>
                                                                <li><a href="<?php echo home_url('/cat/bag/hermes'); ?>">エルメス</a></li>
                                                                <li><a href="<?php echo home_url('/cat/bag/celine'); ?>">セリーヌ</a></li>
                                                                <li><a href="<?php echo home_url('cat/bag/gucci'); ?>">グッチ</a></li>
                                                                <li><a href="<?php echo home_url('cat/bag/prada'); ?>">プラダ</a></li>
                                                                <li><a href="<?php echo home_url('cat/wallet/louisvuitton'); ?>">ルイヴィトン</a></li>
                                                                <li><a href="<?php echo home_url('cat/bag/chanel'); ?>">シャネル</a></li>
                                                                <li><a href="<?php echo home_url('cat/gem/cartier/cartier-bag'); ?>">カルティエ</a></li>
                                                                <li><a href="<?php echo home_url('cat/gem/tiffany/tiffany-bag'); ?>">ティファニー</a></li>
                                                                <li><a href="<?php echo home_url('cat/bag/prada'); ?>">プラダ</a></li>
                                                                <li><a href="<?php echo home_url('cat/bag/fendi/fendi-bag'); ?>">フェンディ</a></li>
                                                                <li><a href="<?php echo home_url('cat/bag/dior/dior-bag'); ?>">ディオール</a></li>
                                                                <li><a href="<?php echo home_url('cat/bag/saint_laurent'); ?>">サンローラン</a></li>
                                                                <li><a href="<?php echo home_url('cat/bag/bottegaveneta'); ?>">ボッテガ・ヴェネタ</a></li>
                                                                <li><a href="<?php echo home_url('cat/bag/ferragamo'); ?>">フェラガモ</a></li>
                                                                <li><a href="<?php echo home_url('cat/bag/loewe'); ?>">ロエベ</a></li>
                                                                <li><a href="<?php echo home_url('cat/bag/berluti'); ?>">ベルルッティ</a></li>
                                                                <li><a href="<?php echo home_url('cat/bag/goyal'); ?>">ゴヤール</a></li>
                                                                <li><a href="<?php echo home_url('cat/bag/rimowa'); ?>">リモワ</a></li>
                                                                <li><a href="<?php echo home_url('cat/bag/globe-trotter'); ?>">グローブトロッター</a></li>
                                                                <li><a href="<?php echo home_url('cat/bag/valextra'); ?>">ヴァレクストラ</a></li>

                                                                <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
                                                            </ul>
            <?php } elseif (is_parent_slug() === 'hermes'|| is_page('hermes')) { ?>
                                                                <h3 class="side_ttl">エルメス買取強化ライン</h3>
                                                                <ul class="side_br_list">
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-halzan'); ?>">アルザン</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-evelyne'); ?>">エブリン</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-herbag'); ?>">エールバッグ</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-herline'); ?>">エールライン</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/haut-a-courroies'); ?>">オータクロア</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-calvi'); ?>">カルヴィ</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-cannes'); ?>">カンヌ</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-garden-party'); ?>">ガーデンパーティ</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-kelly'); ?>">ケリー</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-constance'); ?>">コンスタンス</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-saxo'); ?>">サクソ</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-sac_a_depeche'); ?>">サックアデペッシュ</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-jypsiere'); ?>">ジプシエール</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-dogon'); ?>">ドゴン</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-douville'); ?>">ドーヴィル</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-birkin'); ?>">バーキン</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-himalaya'); ?>">ヒマラヤ</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-picotin-lock'); ?>">ピコタンロック</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-picotin'); ?>">ピコタン</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-fourretout'); ?>">フールトゥ</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-bearnsoufflet'); ?>">ベアンスフレ</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-beant'); ?>">ベアン</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-bolide'); ?>">ボリード</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-lindy'); ?>">リンディ</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-victoria'); ?>">ヴィクトリア</a></li>
                                                                    <li><a href="<?php echo home_url('/cat/bag/hermes/hermes-vespa'); ?>">ヴェスパ</a></li>
                                                                    <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
                                                                </ul>
            <?php } elseif (is_parent_slug() === 'gucci'|| is_page('gucci')) { ?>
                                                                    <h3 class="side_ttl">グッチ買取強化ライン</h3>
                                                                    <ul class="side_br_list">
                                                                        <li><a href="<?php echo home_url('/cat/bag/gucci/gucci-ggimprime'); ?>">GGインプリメ</a></li>
                                                                        <li><a href="<?php echo home_url('/cat/bag/gucci/gucci-ggcanvas'); ?>">GGキャンバス</a></li>
                                                                        <li><a href="<?php echo home_url('/cat/bag/gucci/gucci-ggcrystal'); ?>">GGクリスタル</a></li>
                                                                        <li><a href="<?php echo home_url('/cat/bag/gucci/gucci-ggplus'); ?>">GGプラス</a></li>
                                                                        <li><a href="<?php echo home_url('/cat/bag/gucci/gucci-wavingline'); ?>">ウェビングライン</a></li>
                                                                        <li><a href="<?php echo home_url('/cat/bag/gucci/gucci-old'); ?>">オールドグッチ</a></li>
                                                                        <li><a href="<?php echo home_url('/cat/bag/gucci/gucci-sshima'); ?>">グッチシマ</a></li>
                                                                        <li><a href="<?php echo home_url('/cat/bag/gucci/gucci-jackie'); ?>">ジャッキー</a></li>
                                                                        <li><a href="<?php echo home_url('/cat/bag/gucci/gucci-sukey'); ?>">スーキー</a></li>
                                                                        <li><a href="<?php echo home_url('/cat/bag/gucci/gucci-soho'); ?>">ソーホー</a></li>
                                                                        <li><a href="<?php echo home_url('/cat/bag/gucci/gucci-tattooheart'); ?>">タトゥーハート</a></li>
                                                                        <li><a href="<?php echo home_url('/cat/bag/gucci/gucci-diamante'); ?>">ディアマンテ</a></li>
                                                                        <li><a href="<?php echo home_url('/cat/bag/gucci/gucci-bamboo'); ?>">バンブー</a></li>
                                                                        <li><a href="<?php echo home_url('/cat/bag/gucci/gucci-pricysh'); ?>">プリンシー</a></li>
                                                                        <li><a href="<?php echo home_url('/cat/bag/gucci/gucci-mayfair'); ?>">メイフェア</a></li>
                                                                        <li><a href="<?php echo home_url('/cat/bag/gucci/gucci-lovery'); ?>">ラブリー</a></li>
                                                                        <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
                                                                    </ul>
            <?php } elseif (is_parent_slug() === 'chanel'|| is_page('chanel')) { ?>
                                                                        <h3 class="side_ttl">シャネル買取強化ライン</h3>
                                                                        <ul class="side_br_list">
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-2-55'); ?>">2.55ライン</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-vstitch'); ?>">Vステッチ</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-wflap'); ?>">Wフラップ</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-icon'); ?>">アイコン</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-icecube'); ?>">アイスキューブ</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-un-limited'); ?>">アンリミテッド</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-window'); ?>">ウィンドウライン</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-ultrastitch'); ?>">ウルトラステッチ</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-camellia'); ?>">カメリア</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-cambonline'); ?>">カンボンライン</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-calfskin'); ?>">カーフスキン</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-caviarskin'); ?>">キャビアスキン</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-cocococoon'); ?>">コココクーン</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-cocobutton'); ?>">ココボタン</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-cosmoline'); ?>">コスモライン</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-single-chain'); ?>">シングルチェーン</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-singleflap'); ?>">シングルフラップ</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-sportsline'); ?>">スポーツライン</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-central'); ?>">セントラルステーション</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-double-chain'); ?>">ダブルチェーン</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-chain-shoulder'); ?>">チェーンショルダー</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-chain-bag'); ?>">チェーンバッグ</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-chocobar'); ?>">チョコバー</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-denim-line'); ?>">デニムライン</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-deauville'); ?>">ドーヴィルライン</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-newtravelline'); ?>">ニュートラベルライン</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-bysee'); ?>">バイシー</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-bcamellia'); ?>">バタフライカメリア</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-parisbiarritz'); ?>">パリビアリッツ</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-parisbyzance'); ?>">パリビザンス</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-paris-shanghai'); ?>">パリ上海</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-bicolore'); ?>">ビコローレ</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-frenchriviera'); ?>">フレンチリビエラ</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-boy-chanel'); ?>">ボーイシャネル</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-matelasser'); ?>">マトラッセ</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-mademoiselle'); ?>">マドモワゼル</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-makeup'); ?>">メイクアップライン</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-luxury'); ?>">ラグジュアリーライン</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-lambskin'); ?>">ラムスキン</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-wildstitch'); ?>">ワイルドステッチ</a></li>
                                                                            <li><a href="<?php echo home_url('/cat/bag/chanel/chanel-travelline'); ?>">旧トラベルライン</a></li>
                                                                            <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
                                                                        </ul>
            <?php } elseif (is_parent_slug() === 'gold'|| is_page('gold')) { ?>
                                                                            <h3 class="side_ttl">金・プラチナ買取強化商品</h3>
                                                                            <ul class="side_br_list">
                                                                                <li><a href="<?php echo home_url('/cat/gold'); ?>">金貨</a></li>
                                                                                <li><a href="<?php echo home_url('/cat/gold/14k'); ?>">14金</a></li>
                                                                                <li><a href="<?php echo home_url('/cat/gold/18k'); ?>">18金</a></li>
                                                                                <li><a href="<?php echo home_url('/cat/gold/22k'); ?>">22金</a></li>
                                                                                <li><a href="<?php echo home_url('/cat/gold/24k'); ?>">24金</a></li>
                                                                                <li><a href="<?php echo home_url('/cat/gem/ingot'); ?>">インゴット</a></li>
                                                                                <li><a href="<?php echo home_url('/cat/gem/white-gold'); ?>">ホワイトゴールド</a></li>
                                                                                <li><a href="<?php echo home_url('/cat/gem/pink-gold'); ?>">ピンクゴールド</a></li>
                                                                                <li><a href="<?php echo home_url('/cat/gem/yellow-gold'); ?>">イエローゴールド</a></li>
                                                                                <li><a href="<?php echo home_url('/cat/gem/platinum'); ?>">プラチナ</a></li>
                                                                                <li><a href="<?php echo home_url('/cat/gem/silver'); ?>">シルバー</a></li>
                                                                                <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
                                                                            </ul>
            <?php } elseif (is_parent_slug() === 'breitling'|| is_page('breitling')) { ?>
                                                                                <h3 class="side_ttl">ブライトリング買取強化モデル</h3>
                                                                                <ul class="side_br_list">
                                                                                    <li><a href="<?php echo home_url('/cat/watch/breitling/avenger'); ?>">アベンジャー</a></li>
                                                                                    <li><a href="<?php echo home_url('/cat/watch/breitling/avenger_2_gmt'); ?>">アベンジャーⅡＧＭＴ</a></li>
                                                                                    <li><a href="<?php echo home_url('/cat/watch/breitling/avenger_blackbird44'); ?>">アベンジャーブラックバード44</a></li>
                                                                                    <li><a href="<?php echo home_url('/cat/watch/breitling/chrono_super_ocean'); ?>">クロノスーパーオーシャン</a></li>
                                                                                    <li><a href="<?php echo home_url('/cat/watch/breitling/chronomat'); ?>">クロノマット</a></li>
                                                                                    <li><a href="<?php echo home_url('/cat/watch/breitling/chronomat44'); ?>">クロノマット44</a></li>
                                                                                    <li><a href="<?php echo home_url('/cat/gem/vancleefarpels/chronomat44_airborne'); ?>">クロノマット44エアボーン</a></li>
                                                                                    <li><a href="<?php echo home_url('/cat/watch/breitling/chronomat_before97'); ?>">クロノマット９７年以前</a></li>
                                                                                    <li><a href="<?php echo home_url('/cat/watch/breitling/chronomat_airborne'); ?>">クロノマットエアボーン</a></li>
                                                                                    <li><a href="<?php echo home_url('/cat/watch/breitling/chronomat_evolution'); ?>">クロノマットエボリューション</a></li>
                                                                                    <li><a href="<?php echo home_url('/cat/watch/breitling/shadowflyback'); ?>">シャドーフライバック</a></li>
                                                                                    <li><a href="<?php echo home_url('/cat/watch/breitling/superocean'); ?>">スーパーオーシャン</a></li>
                                                                                    <li><a href="<?php echo home_url('/cat/watch/breitling/spaciograph'); ?>">スパシオグラフ</a></li>
                                                                                    <li><a href="<?php echo home_url('/cat/watch/breitling/spaciograph2'); ?>">スパシオグラフⅡ</a></li>
                                                                                    <li><a href="<?php echo home_url('/cat/watch/breitling/navitimer'); ?>">ナビタイマー</a></li>
                                                                                    <li><a href="<?php echo home_url('/cat/watch/breitling/navitimer_cosmonotes'); ?>">ナビタイマーコスモノート</a></li>
                                                                                    <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
                                                                                </ul>
            <?php } elseif (is_parent_slug() === 'louisvuitton'|| is_page('louisvuitton')) { ?>
                                                                                    <h3 class="side_ttl">ルイヴィトン買取強化ライン</h3>
                                                                                    <ul class="side_br_list">
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/eyeall-2'); ?>">アイオール</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/love_monogram'); ?>">ラブ・モノグラム</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/antia'); ?>">アンティア</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/antigua'); ?>">アンティグア</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/empreinte'); ?>">アンプラント</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/ikat_flower'); ?>">イカット・フラワー</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/idylle'); ?>">イディール</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/epi_metallic'); ?>">エピメタリック</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/epi'); ?>">エピ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/kabaanburu'); ?>">カバ・アンブル</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/kyuiru-trione'); ?>">キュイール トリヨン</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/groom'); ?>">グルーム</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/globe_shopper'); ?>">グローブショッパー</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/cosmic-blossom'); ?>">コスミック ブラッサム</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/kontodufe'); ?>">コント・ドゥ・フェ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/cyberepi'); ?>">サイバーエピ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/thats-love'); ?>">ザッツラブ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/chicago'); ?>">シカゴ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/suite-monogram'); ?>">スイートモノグラム</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/suhari'); ?>">スハリ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/sauvage'); ?>">ソバージュ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/taiga'); ?>">タイガ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/damier-infini'); ?>">ダミエ・アンフェニ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/damier-color'); ?>">ダミエ・カラー</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/damier_gurase'); ?>">ダミエ・グラセ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/damier-cobalt'); ?>">ダミエ コバルト</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/damie_vernis'); ?>">ダミエ・ヴェルニ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/damier'); ?>">ダミエ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/cherryblossom'); ?>">チェリーブロッサム</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/trunk-lock'); ?>">トランク＆ロック</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/tromploeil'); ?>">トロンプイユ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/naxos'); ?>">ナクソス</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/nomad'); ?>">ノマド</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/bran-soleil'); ?>">ブランソレイユ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/bray'); ?>">ブレイディッド・レザー</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/mahina'); ?>">マヒナ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/multicolor-b'); ?>">マルチカラー・ブロン</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/multicolor-n'); ?>">マルチカラー・ノワール</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/multi-color-mink'); ?>">マルチカラー・ミンク</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/miroir'); ?>">ミロワール</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/azure'); ?>">モノグラム・アズール</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/m-4'); ?>">モノグラム・ウォーターカラー</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/m-5'); ?>">モノグラム・エクリプス</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/monogram-camouflage'); ?>">モノグラム・カモフラージュ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/m-6'); ?>">モノグラム・グラセ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/monogram-graffiti'); ?>">モノグラム・グラフティ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/m-7'); ?>">モノグラム・サテン</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/simmer'); ?>">モノグラム・シマー</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/m-8'); ?>">モノグラム・ジョークス</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/monogram-stone'); ?>">モノグラム・ストーン</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/monogram-tahitiennes'); ?>">モノグラム・タイシエンヌ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/danteru'); ?>">モノグラム・ダンテェル</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/m-3'); ?>">モノグラム・チェリー</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/m-charm'); ?>">モノグラム・チャーム</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/monogram-denim'); ?>">モノグラム・デニム</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/m-21'); ?>">モノグラム・フルリ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/m-22'); ?>">モノグラム・ペルフォ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/m-11'); ?>">モノグラム・ボンボン</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/macassar'); ?>">モノグラム・マカサー</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/m-24'); ?>">モノグラム・マット</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/minigurase'); ?>">モノグラム・ミニグラセ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/monogram-mini'); ?>">モノグラム・ミニ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/m-12'); ?>">モノグラム・ミラージュ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/m-13'); ?>">モノグラム・ミンク</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/m-26'); ?>">モノグラム・メッシュ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/monogram-rayures'); ?>">モノグラム・レイユール</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/shine'); ?>">モノグラム・シャイン</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/pulp'); ?>">モノグラム・パルプ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/m-20'); ?>">モノグラム・パンダ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/monogram_leopard'); ?>">モノグラム・レオパード</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/monogram'); ?>">モノグラム</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/leopard-2'); ?>">レオパード</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/yayoi-kusama'); ?>">ヤヨイクサマコレクション</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/utah'); ?>">ユタ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/vernis_fluoride'); ?>">ヴェルニ・フルオ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/vernis_fleur'); ?>">ヴェルニ・フルール</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/vernis'); ?>">ヴェルニ</a></li>
                                                                                        <li><a href="<?php echo home_url('/cat/wallet/louisvuitton/takashi_murakami'); ?>">村上隆MOCA限定</a></li>
                                                                                        <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
                                                                                    </ul>
        <?php } else { ?>
            <h3 class="side_ttl">注目の強化買取ブランド</h3>
            <ul class="side_br_list">
                <li><a href="<?php echo home_url('cat/watch/rolex'); ?>">ロレックス</a></li>
                <li><a href="<?php echo home_url('cat/wallet/louisvuitton'); ?>">ルイヴィトン</a></li>
                <li><a href="<?php echo home_url('cat/bag/chanel'); ?>">シャネル</a></li>
                <li><a href="<?php echo home_url('cat/bag/hermes'); ?>">エルメス</a></li>
                <li><a href="<?php echo home_url('cat/watch/omega'); ?>">オメガ</a></li>
                <li><a href="<?php echo home_url('cat/gem/cartier'); ?>">カルティエ</a></li>
                <li><a href="<?php echo home_url('cat/gem/tiffany'); ?>">ティファニー</a></li>
                <li><a href="<?php echo home_url('cat/gem/bulgari'); ?>">ブルガリ</a></li>
                <li><a href="<?php echo home_url('cat/bag/gucci'); ?>">グッチ</a></li>
                <li><a href="<?php echo home_url('cat/bag/prada'); ?>">プラダ</a></li>
                <li><a href="<?php echo home_url('/cat/bag/celine'); ?>">セリーヌ</a></li>
                <li><a href="<?php echo home_url('/cat/bag/bottegaveneta'); ?>">ボッテガヴェネタ</a></li>
                <li><a href="<?php echo home_url('cat/gem/harrywinston'); ?>">ハリーウィンストン</a></li>
                <li><a href="<?php echo home_url('cat/gem/vancleefarpels'); ?>">ヴァンクリーフ&amp;アーペル</a></li>
                <li><a href="<?php echo home_url('cat/watch/audemarspiguet'); ?>">オーデマピゲ</a></li>
                <li><a href="<?php echo home_url('cat/watch/patek-philippe'); ?>">パテックフィリップ</a></li>
                <li><a href="<?php echo home_url('cat/watch/hublot'); ?>">ウブロ</a></li>
                <li><a href="<?php echo home_url('cat/watch/franckmuller'); ?>">フランクミューラー</a></li>
                <li><a href="<?php echo home_url('cat/watch/tudor'); ?>">チュードル</a></li>
                <li><a href="<?php echo home_url('cat/gem/piaget'); ?>">ピアジェ</a></li>
                <li><a href="<?php echo home_url('brand'); ?>">その他取り扱いブランドはコチラ</a></li>
            </ul>
        <?php } ?>
    </section>
    <section id="side-categorylist">
        <h3 class="side_ttl">CONTENTS</h3>
        <ul class="list-unstyled">
            <li class="side_cont voice_bn"> <a href="<?php echo home_url('voice'); ?>">お客様の声</a>
                <p>利用された皆様から嬉しいお言葉を沢山いただきました。</p>
            </li>
            <li class="side_cont kaitori_bn"> <a href="<?php echo home_url('about-purchase'); ?>">買取について</a>
                <p>初めての方でも不安無く、安心してご利用いただく為に。</p>
            </li>
            <li class="side_cont qa_bn"> <a href="<?php echo home_url('faq'); ?>">よくある質問</a>
                <p>ご質問とその回答。不明な点はこちらから。</p>
            </li>
            <li class="side_cont shop_bn"> <a href="<?php echo home_url('shop'); ?>">店舗案内</a>
                <p>銀座駅徒歩30秒の好立地。 最寄り駐車場案内。
                </p>
            </li>
            <li class="side_cont rec_bn"> <a href="http://www.staygold-sg.com/recruit" target="_blank">採用情報</a> </li>
        </ul>
    </section>
    <!-- <section>
         <ul class="list-unstyled">
         <li class="aboutsatei adgain_tel">
         <p id="phone_number_holder">0120-970-060</p>
         <a href="<?php echo home_url('purchase1'); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/sidebar/btn-satei.png" alt="今すぐ査定申込"></a></li>
         <li><a href="<?php echo home_url('line'); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/sidebar/btn-line.png" alt="楽しい！簡単！LINE査定はじめました！"></a></li>
         </ul>
         </section>-->
    <section class="side_cvbox">
        <h2>無料査定のご案内</h2>
        <ul class="side_cvlist">
            <li><a href="<?php echo home_url('customer'); ?>">電話査定</a></li>
            <li><a href="<?php echo home_url('line'); ?>">LINE査定</a></li>
            <li><a href="<?php echo home_url('purchase1'); ?>">メール査定</a></li>
        </ul>

    </section>
    <section id="blog-newlist">
        <?php
            // 新着記事4件取得
            $query = array(
                'posts_per_page' => 4,
                'post_type'     => 'blog',
                'post_status'   => 'publish',
                'orderby'       => 'post_modified',
                'order'         => 'DESC',
            );
            $result = new WP_Query($query);

            // ログインユーザーのみ閲覧可
            //if(current_user_can('read_private_pages')) :
            echo '<h3>新着コラム</h3>';
            echo '<p class="bloglink"><a href="http://kaitorisatei.info/brandrevalue/blog"> > 一覧を見る</a></p>';
            echo '<ul class="clearfix">';
            while ($result->have_posts()) {
                $result->the_post();
                $cat = get_the_terms(get_the_ID(), 'blog-cat');
                $cat = $cat[0];
                $catName = $cat->name;
                if(!empty($catName)) {
                    $catName = '【'.$catName.'】';
                }

                echo '<li class="postlist">';
                echo '<a href="'.get_the_permalink().'">';
                the_post_thumbnail();
                echo '<p class="date">'.get_the_time("Y年m月d日（D）").'</p>';
                echo '<h4>'.$catName.get_the_title().'</h4>';
                echo '</a>';
                echo '</li>';
            }
            echo '</ul>';
            //endif;
            wp_reset_postdata();
        ?>
        </section>
    </div>
    <!-- #secondary -->
