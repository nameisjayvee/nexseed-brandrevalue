<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>
    <div id="primary" class="about-purchase content-area">
        <main id="main" class="site-main" role="main">
            <section>
                <img src="<?php echo get_s3_template_directory_uri() ?>/img/mv/takuhai_mv.png" alt="宅配買取全て無料で対応致します。">
            </section>

            <video controls
    src="<?php echo get_s3_template_directory_uri() ?>/video/mov.mp4"
    poster="<?php echo get_s3_template_directory_uri() ?>/video/mov_img.png"
    width="100%">

すみませんが、このブラウザーは埋め込み映像に対応していません。</video>
            <section id="catchcopy">
                <h3>お品物を郵送するだけで、スピード査定・現金化が可能に</h3>
                <p>「品物を査定してほしいけど、銀座まで行くのは遠くて大変……」「売りたいものがたくさんあって、お店まで持っていけない！」そんな方もどうかご心配なく。BRANDREVALUE(ブランドリバリュー)では、ご自宅から郵送でお品物をお送りいただき、店頭にお越しいただくことなく査定・買取を行う宅配買取サービスを行っています。もちろん配送料は無料。お品物を箱詰めするだけで簡単に梱包できる「宅配キット」をお送りいたしますので、手間をかけずにお品物をお売りいただけます。</p>
            </section>
            <section class="takuhai_flow">
                <div class="takuhai_flow_box">
                    <h2>宅配買取の流れ</h2>
                    <ul class="flow_list">
                        <li>
                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/flow01.png" alt="お申し込み">
                            <p>ご依頼を頂きましたら、当社から専用宅配キット<br> (段ボール等・必要書類）を発送させて頂きます。</p>
                        </li>
                        <li>
                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/flow02.png" alt="梱包して発送">
                            <p>宅配キット到着後お品物を梱包し、必要書類にご記入頂き身分証のコピーと合わせてお品物を梱包下さい。</p>
                        </li>
                        <li>
                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/flow03.png" alt="査定結果のご連絡">
                            <p>お品物が当店に到着致しましたら、到着連絡をさせて頂き1日～3日以内にご連絡させて頂きます。</p>
                        </li>
                        <li>
                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/flow04.png" alt="同意後、即入金">
                            <p>査定額にご納得して頂きましたら、ご指定口座にお振込みを致します。※店頭でのお支払いも可能です。<br>キャンセルの場合でも無料でご返送させて頂きます。</p>
                        </li>
                    </ul>
                </div>
                <p style="margin-top:20px;"><a href="<?php echo home_url('area'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/takuhai_bnr.jpg" alt="買取エリアはこちら"></a></p>
                <div class="flow_step">
                    <div class="stpe01">
                        <h3><span>STEP1</span>お申し込み</h3>
                        <div class="in">
                            <p>当ホームページの「宅配キットお申込み」から必要事項を記入して頂ければ、無料宅配キットを発送いたします。</p>

                        </div>
                        <div class="takuhai_cvbox">
                            <p><a href="<?php echo home_url('purchase3'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/takuhai_cv01.png" alt="宅配買取お申し込み"></a></p>
                            <div class="custom_tel takuhai_tel">
                                <a href="tel:0120-970-060">
                                    <div class="tel_wrap">
                                        <div class="telbox01"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/customer/telico.png" alt="お申し込みはこちら">
                                            <p>お電話からでもお申込み可能！<br>ご不明な点は、お気軽にお問合せ下さい。 </p>
                                        </div>
                                        <div class="telbox02"> <span class="small_tx">【受付時間】11:00 ~ 21:00</span><span class="ted_tx"> 年中無休</span>
                                            <p>0120-970-060</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="stpe02">
                        <h3><span>STEP2</span>梱包して発送</h3>
                        <div class="in">
                            <p>宅配キットが到着後、査定商品を梱包し、佐川運輸に集荷依頼を行い着払いでお送りください。<br>またお近くのコンビニ、宅配業者からでも着払いで発送することも可能です。<br>また、高額商品につきましては、運送保険サービスをご用意しております。<br>ご不明点は、フリーダイヤル<a href="tel:0120-970-060">0120-970-060</a>までお電話ください。
                            </p>
                        </div>
                    </div>
                    <div class="stpe03">
                        <h3><span>STEP3</span>査定結果のご連絡</h3>
                        <div class="in">
                            <p>お送りいただいたお品物の到着確認後、1日～3日以内に査定金額をご連絡させていただきます。
                            </p>
                            <dl>
                                <dt>買取金額に同意の場合</dt>
                                <dd>ご指定口座へ即日または翌営業日にお振込いたします。※店頭でのお支払いも可能です。</dd>
                            </dl>
                            <dl>
                                <dt>キャンセルの場合</dt>
                                <dd>弊社で送料を負担し、お品物をご返却いたします。</dd>
                            </dl>
                            <ul>
                                <li><span>※</span>環境配慮に基づきお送り頂いた梱包資材でご返送させていただきます。ご了承ください。</li>
                                <li><span>※</span>全て無料でご利用いただけます。</li>
                                <li><span>※</span>運送保険サービスについてはこちらから フリーダイヤル<a href="tel:0120-970-060">0120-970-060 </a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="takuhai_cvbox">
                        <p><a href="<?php echo home_url('purchase3'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/takuhai_cv01.png" alt="宅配買取お申し込み"></a></p>
                        <div class="custom_tel takuhai_tel">
                            <a href="tel:0120-970-060">
                                <div class="tel_wrap">
                                    <div class="telbox01"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/customer/telico.png" alt="お申し込みはこちら">
                                        <p>お電話からでもお申込み可能！<br>ご不明な点は、お気軽にお問合せ下さい。 </p>
                                    </div>
                                    <div class="telbox02"> <span class="small_tx">【受付時間】11:00 ~ 21:00</span><span class="ted_tx"> 年中無休</span>
                                        <p>0120-970-060</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </section>
            <section class="takuhai_chara">
                <h2 class="takihai_ttl">ブランドリバリュー　宅配買取の特徴</h2>
                <div class="chara01">
                    <h3>交通費も送料も、面倒な梱包の準備も必要ありません</h3>
                    <p>「店頭まで行くのは面倒だし、無駄な交通費をかけたくない」――そんな効率重視の方にピッタリな宅配買取。BRANDREVALUE(ブランドリバリュー)の宅配買取は、お品物を郵送でお送りいただくだけで、査定・買取を行う便利なサービスです。しかも、送料は当社が全額負担いたしますし、郵送に必要な箱等の宅配セットもご希望の方には無料でご提供！　無駄な時間もお金も全く使わずに品物を納得の価格で換金することができる、人気の買取方法です。</p>
                </div>
                <div class="chara02">
                    <h3>運送保険サービス</h3>
                    <p>ブランドリバリューの宅配買取は、お客様の不安を取り除くために、「運送保険サービス」をご用意しております。 「時計などの精密機器を送るは不安・・・」　「宝石がかけてしまったらどうしよう」などといったお客様の不安を解消する為に、最大5000万円の運送保険サービスをご用意しております。 運送時の万が一の破損や紛失事故に備えております為、運送事故が起きても全額保証致します。
                    </p>
                    <p class="bol">高額なお品物でも安心してご利用頂けるよう、宅配保険サービスをご用意しております。</p>
                    <dl class="hoken_box">
                        <dt class="hoken_at">必須条件1</dt>
                        <dd>
                            <p>事前査定<br />メール・LINE・電話にて事前に査定を受けて下さい。</p>
                        </dd>
                    </dl>
                    <dl class="hoken_box">
                        <dt class="hoken_at">必須条件2</dt>
                        <dd>
                            <p>当店からお送りする宅配梱包キット<br />（梱包材・着払い伝票）をご使用ください。</p>
                        </dd>
                    </dl>
                    <p class="small_txt">※事前査定の金額が補償金額の上限となります。事前査定がない場合は、配送会社の規定に準じます。<br />※ご不明点がございましたら直接お電話にてお問合せくださいませ。</p>
                </div>
                <div class="chara03">
                    <h3>特殊梱包資材</h3>
                    <p>運送保険だけでは不安！といったお客様の声にお応えするために、ブランドリバリューでは特殊梱包資材をご用意しており、多くのお客様に安心してご利用いただいております。<br>
                        <span>※</span>特殊梱包資材は、●●万円以上の高額時計、宝石に限ります。</p>
                    <p class="img_center"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/takuhai_example.png" alt=""></p>
                    <p><span>※</span>特殊梱包資材は高額で箱等付属品がないお品物や弊社で必要と判断された場合にご利用いただけます。<br>詳しくは一度お電話下さいませ。</p>
                </div>
                <div class="takuhai_cvbox">
                    <p><a href="<?php echo home_url('purchase3'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/takuhai_cv01.png" alt="宅配買取お申し込み"></a></p>
                    <div class="custom_tel takuhai_tel">
                        <a href="tel:0120-970-060">
                            <div class="tel_wrap">
                                <div class="telbox01"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/customer/telico.png" alt="お申し込みはこちら">
                                    <p>お電話からでもお申込み可能！<br>ご不明な点は、お気軽にお問合せ下さい。 </p>
                                </div>
                                <div class="telbox02"> <span class="small_tx">【受付時間】11:00 ~ 21:00</span><span class="ted_tx"> 年中無休</span>
                                    <p>0120-970-060</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </section>
            <section class="takuhai_voice">
                <h2 class="takihai_ttl">お客様の声</h2>
                <ul>
                    <li>
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/voice_item01.png" alt="ロレックス GMTマスターⅡ 116758SARU">
                        <div>
                            <h3>ロレックス GMTマスターⅡ 116758SARU</h3>
                            <p>お買取価格:850万円</p>
                            <p>S.Y様　保険サービス有</p>
                            <p>遠方の為、宅配買取の利用に抵抗がありましたが保険サービスがあるとの事でお願いしました。事前に電話で問い合わせた際の金額で買い取って頂けたので良かったです。</p>
                        </div>
                    </li>
                    <li>
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/voice_item02.png" alt="エルメス バーキン25 ローズサクラ ヴォースイフト">
                        <div>
                            <h3>エルメス バーキン25 ローズサクラ ヴォースイフト</h3>
                            <p>お買取価格:165万円</p>
                            <p>愛知県　A.T様　保険サービス有</p>
                            <p>初めての買取りで不安はありましたが、分かりやすく説明頂き希望以上の金額で売却出来ました。また宜しくお願いします。</p>
                        </div>
                    </li>
                    <li>
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/voice_item03.png" alt="ダイヤリング 2.03ct D VVS2 EX">
                        <div>
                            <h3>ダイヤリング 2.03ct D VVS2 EX</h3>
                            <p>お買取価格:850万円</p>
                            <p>福島県　SK様　保険サービス有　特殊梱包資材有</p>
                            <p>運送上何かトラブルがないか不安でしたが、高額商品専用の、特殊梱包材があるという事でしたので利用致しました。対応も親切で、お買取り金額も仮査定以上で取引できたので嬉しかったです。</p>
                        </div>
                    </li>

                </ul>
            </section>



            <section id="lp-cat-jisseki" class="takuhai_jisseki">
                <h2 class="takihai_ttl">宅配買取の買取実績</h2>
                <ul id="box-jisseki" class="list-unstyled clearfix">
                    <li class="box-4">
                        <div class="title"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/1.png" alt="ロレックス デイトナ 116506">
                            <p class="itemName">ロレックス デイトナ 116506</p>
                            <hr /><span class="red">A社</span>：6,450,000円<br /><span class="blue">B社</span>：6,400,000円</div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">6,500,000<span class="small">円</span></p>
                        </div>
                        <div class="hitokoto">別の時計の買い替え資金とのことでロレックス　デイトナ　116506をお買取りさせて頂きました。</div>
                    </li>
                    <li class="box-4">
                        <div class="title"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/2.png" alt="フランクミュラー ロングアイランド 902QZCD1R">
                            <p class="itemName">フランクミュラー ロングアイランド 902QZCD1R</p>
                            <hr /><span class="red">A社</span>：550,000円<br /><span class="blue">B社</span>：¥530,000円</div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">580,000<span class="small">円</span></p>
                        </div>
                        <div class="hitokoto">コレクションの整理にとフランクミュラー　ロングアイランド 902QZCD1Rをお買取りさせて頂きました。</div>
                    </li>
                    <li class="box-4">
                        <div class="title"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/3.png" alt="ロレックス デイトナ 116506">
                            <p class="itemName">リシャールミル フェリペ マッサ RM011</p>
                            <hr /><span class="red">A社</span>：14,750,000<br /><span class="blue">B社</span>：14,500,000円</div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">15,000,000<span class="small">円</span></p>
                        </div>
                        <div class="hitokoto">急遽お金が必要になったとのことで、リシャールミル フェリペ マッサ RM011をお買取りさせて頂きました。</div>
                    </li>
                    <li class="box-4">
                        <div class="title"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/4.png" alt="ウブロ ビッグバンエボリューション 301.PX.1180.GR.1104 ">
                            <p class="itemName">ウブロ ビッグバンエボリューション 301.PX.1180.GR.1104 </p>
                            <hr /><span class="red">A社</span>：1,920,000円<br /><span class="blue">B社</span>：1,880,000円</div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">2,000,000<span class="small">円</span></p>
                        </div>
                        <div class="hitokoto">お車の購入資金のためにと、ウブロ ビッグバンエボリューション 301.PX.1180.GR.1104をお買取りさせて頂きました。</div>
                    </li>
                    <li class="box-4">
                        <div class="title"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/5.png" alt="ロレックス デイトナ 116506">
                            <p class="itemName">エルメス バーキン30 ヒマラヤ</p>
                            <hr /><span class="red">A社</span>：8,500,000円<br /><span class="blue">B社</span>：8,200,000円</div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">9,000,000<span class="small">円</span></p>
                        </div>
                        <div class="hitokoto">投資目的で購入された　エルメス　バーキン30　ヒマラヤをお買取りさせて頂きました。</div>
                    </li>
                    <li class="box-4">
                        <div class="title"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/6.png" alt="エルメス バーキン30 ヒマラヤ">
                            <p class="itemName">シャネル ボーイシャネル ショルダーバッグ</p>
                            <hr /><span class="red">A社</span>：530,000円<br /><span class="blue">B社</span>：500,000円</div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">580,000<span class="small">円</span></p>
                        </div>
                        <div class="hitokoto">頂いたのだが、ご使用されないという事で　シャネル　ボーイシャネル　ショルダーバッグをお買取りさせて頂きました。</div>
                    </li>
                    <li class="box-4">
                        <div class="title"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/7.png" alt="パームスプリングスミニ M41562">
                            <p class="itemName">パームスプリングスミニ M41562</p>
                            <hr /><span class="red">A社</span>：200,000円<br /><span class="blue">B社</span>：180,000円</div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">220,000<span class="small">円</span></p>
                        </div>
                        <div class="hitokoto">新しいバッグを購入したとのことで、パームスプリングスミニ M41562をお買取りさせて頂きました。</div>
                    </li>
                    <li class="box-4">
                        <div class="title"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/8.png" alt="ロレックス デイトナ 116506">
                            <p class="itemName">グッチ ディオニュソス 2WAYバッグ</p>
                            <hr /><span class="red">A社</span>：205,000円<br /><span class="blue">B社</span>：180,000円</div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">230,000<span class="small">円</span></p>
                        </div>
                        <div class="hitokoto">購入したもののあまり使う機会がないとのことで、グッチ ディオニュソス 2WAYバッグをお買取りさせて頂きました。</div>
                    </li>
                    <li class="box-4">
                        <div class="title"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/9.png" alt="カルティエ ジュストアンクル ブレスレット">
                            <p class="itemName">カルティエ ジュストアンクル ブレスレット</p>
                            <hr /><span class="red">A社</span>：3,000,000円<br /><span class="blue">B社</span>：2,950,000円</div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">3,150,000<span class="small">円</span></p>
                        </div>
                        <div class="hitokoto">大切にご使用されていたというカルティエ ジュストアンクル ブレスレットをお買取りさせて頂きました。</div>
                    </li>
                    <li class="box-4">
                        <div class="title"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/10.png" alt="ロレックス デイトナ 116506">
                            <p class="itemName">ヴァンクリーフ＆アーペル アルハンブラネックレス</p>
                            <hr /><span class="red">A社</span>：700,000円<br /><span class="blue">B社</span>：680,000円</div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">750,000<span class="small">円</span></p>
                        </div>
                        <div class="hitokoto">趣味が変わってしまったとのことで、ヴァンクリーフ＆アーペル アルハンブラネックレスをお買取りさせて頂きました。</div>
                    </li>
                    <li class="box-4">
                        <div class="title"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/11.png" alt="ロレックス デイトナ 116506">
                            <p class="itemName">パライバトルマリン ダイヤモンドリング　</p>
                            <hr /><span class="red">A社</span>：1,350,000円<br /><span class="blue">B社</span>：1,300,000円</div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">1,500,000<span class="small">円</span></p>
                        </div>
                        <div class="hitokoto">お母様から頂いたというパライバトルマリン ダイヤモンドリングをお買い取りさせて頂きました。</div>
                    </li>
                    <li class="box-4">
                        <div class="title"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/12.png" alt="ロレックス デイトナ 116506">
                            <p class="itemName">サファイアダイヤリング　12.3ｃｔ</p>
                            <hr /><span class="red">A社</span>：650,000円<br /><span class="blue">B社</span>：620,000円</div>
                        <div class="box-jisseki-cat">
                            <h3>買取価格例</h3>
                            <p class="price">700,000<span class="small">円</span></p>
                        </div>
                        <div class="hitokoto">遺品整理の為　サファイアダイヤリングをお買取りさせて頂きました。</div>
                    </li>
                </ul>
            </section>

            <section class="takuhai_possible">
                <h2 class="takihai_ttl">お買取りできるもの</h2>
                <ul class="possible_list01">
                    <li>
                        <p>バッグ</p>
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible01.png" alt="バッグ">
                    </li>
                    <li>
                        <p>時計</p>
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible02.png" alt="バッグ">
                    </li>
                    <li>
                        <p>ダイヤ</p>
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible03.png" alt="バッグ">
                    </li>
                    <li>
                        <p>宝石</p>
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible04.png" alt="バッグ">
                    </li>
                    <li>
                        <p>ブランドジュエリー</p>
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible05.png" alt="バッグ">
                    </li>
                    <li>
                        <p>財布</p>
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible06.png" alt="バッグ">
                    </li>
                    <li>
                        <p>靴</p>
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible07.png" alt="バッグ">
                    </li>
                    <li>
                        <p>毛皮</p>
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible08.png" alt="バッグ">
                    </li>
                    <li>
                        <p>ブランドアパレル</p>
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible09.png" alt="バッグ">
                    </li>
                    <li>
                        <p>お酒</p>
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible10.png" alt="バッグ">
                    </li>
                </ul>
                <h3><span class="cr01">「傷」</span>や<span class="cr02">「汚れ」</span>があってもお買い取りさせて下さい！<br> どんな状態であっても必ずお買取りさせて頂きます。
                </h3>
                <ul class="possible_list02">
                    <li>

                        <h4>ボロボロになったバッグ</h4>
                        <div>
                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/state_ex01.png" alt="ボロボロになったバッグ">
                            <p>・シミ / 汚れ<br>・破れ / 日焼け</p>
                        </div>
                    </li>
                    <li>

                        <h4>壊れてしまった時計</h4>
                        <div>
                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/state_ex02.png" alt="壊れてしまった時計">
                            <p>・不動<br>・パーツ破損</p>
                        </div>
                    </li>
                    <li>

                        <h4>破損した貴金属、宝石</h4>
                        <div>
                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/state_ex03.png" alt="破損した貴金属、宝石">
                            <p>・千切れてしまった<br>・石が取れてしまった<br>・イニシャル入り</p>
                        </div>
                    </li>

                    <li>

                        <h4>ルース(裸石)</h4>
                        <div>
                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/state_ex04.png" alt="破損した貴金属、宝石">
                            <p>・枠から外れてしまった<br>・鑑定書なし
                            </p>
                        </div>
                    </li>


                </ul>



            </section>

            <section class="kaitori_not">
                <h3><span class="ng">NG</span>買取できないもの<span class="at">※ 店頭または出張買取は対応しております。</span></h3>
                <div class="not_cont">
                    <p class="img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/not_image.png" alt="買取できないもの" /></p>
                    <div class="cont_left">
                        <ul class="not_list">
                            <li>食器</li>
                            <li>陶器</li>
                            <li>ガラス製品</li>
                        </ul>
                        <div class="cont_g">
                            <p class="not_txt">その他、<span>割れや欠けなどの破損の可能性がある
お品物に関しては宅配買取をご利用なさらないよう</span>お願い申し上げます。</p>
                            <p class="not_txt_s">宅配買取対応商品や買取可能かお悩みの場合はお問い合わせフォーム またはお電話にてご確認ください。
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <div class="custom_cv_box01_wrap">
                <p class="tx_center"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/ttl.png" alt="お申し込みはこちら"></p>
                <ul class="custom_cv_box01">
                    <li>
                        <a href="tel:0120-970-060">
                            <p>電話査定</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo home_url('line'); ?>">
                            <p>LINE査定</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo home_url('purchase1'); ?>">
                            <p>メール査定</p>
                        </a>
                    </li>
                </ul>
                <p class="tx_center tx_green">最新情報をもとにおおよその査定金額をご案内いたします。<br /> ささいな疑問やご相談にもお答えいたします。 </p>
                <ul class="cv_box01_list">
                    <li>店舗の混雑状況は？</li>
                    <li>買取の申し込み方法は？</li>
                    <li>保証書や状態について</li>
                    <li>店舗はどこにあるの？</li>
                    <li>どんな買取方法があるの？</li>
                </ul>
            </div>

            <?php
        // アクションポイント
        get_template_part('_action');
		?>
                <?php
        // 買取方法
        get_template_part('_purchase');

        // 店舗案内
        get_template_part('_shopinfo');
      ?>

        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->

    <?php
get_sidebar();
get_footer();
