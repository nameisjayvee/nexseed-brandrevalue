<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  'watch009.png::PATEK PHILIPPE::ノーチラス Ref5712::3,201,000::3,135,000::3,300,000::165,000',
  'watch010.png::BREITLING::ナビタイマー ref. A022B01NP::388,000::380,000::400,000::20,000',
  'watch011.png::PANERAI::ルミノール クロノデイライト::582,000::570,000::600,000::30,000',
  'watch012.png::ボールウォッチ::エンジニア ハイドロカーボン::77,600::76,000::80,000::4,000',
  'watch001.jpg::ROLEX::サブマリーナ::480,000::510,000::580,000::100,000',
  'watch002.jpg::AUDEMARS PIGUET::ロイヤルオーク オフショア::3,900,000::4,100,000::470,000::800,000',
  'watch003.jpg::PATEK PHILIPPE::コンプリケーテッド::3,900,000::4,100,000::4,700,000::800,000',
  'watch004.jpg::BREGUET::コンプリケーション::3,400,000::3,600,000::4,100,000::700,000',
  'watch005.jpg::BREITLING(ブライトリング)::ナビタイマー::400,000::410,000::470,000::70,000',
  'watch006.jpg::CORUM(コルム)::アドミラルズカップ コンペティション::300,000::340,000::380,000::80,000',
  'watch007.jpg::OMEGA(オメガ)::シーマスターアクアテラ::250,000::270,000::300,000::50,000',
  'watch008.jpg::PANERAI(パネライ)::ルミノール::1,200,000::1,450,000::1,750,000::550,000',
);


get_header(); ?>

	<div id="primary" class="cat-page content-area">
		<main id="main" class="site-main" role="main">
      <p id="catchcopy">BRANDREVALUE(ブランドリバリュー)では独自の中古時計再販ルートを構築しているため、幅広いブランドの腕時計を高く査定し、買い取ることが可能となっております。<br>
      また、ブランド腕時計の買取において豊富な経験を積んだスタ ッフが在籍しており、故障した時計、電池切れで動かない時計、傷ついた時計等も価値を見落とすことなく最大限高く査定。<br>
      <br>
      「他店で査定を断られたような時計が、BRANDREVALUE(ブランドリバリュー)では<br>
      驚くほどの高額買取の対象となった」<br>
      ――そのようなことも全く珍しくはありません。</p>
      
      <section id="hikaku" class="watch_hikaku">
        <h3 class="text-hide">他社の買取額と比較すると</h3>
        <p class="hikakuName">ROLEX ジャスデイト ジャスⅡ</p>
        <p class="hikakuText">※状態が良い場合や当店で品薄の場合などは<br>
        　特に高価買取致します。</p>
        <p class="hikakuPrice1"><span class="red">A社</span>：68,000円</p>
        <p class="hikakuPrice2"><span class="blue">B社</span>：64,000円</p>
        <p class="hikakuPrice3">68,000<span class="small">円</span></p>
      </section>
      
      <section id="lp_history">
      	<h3 class="text-hide">有名ブランドの歴史</h3>
        <ul class="clearfix">
        	<li><a href=""><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/btn_history01.jpg" alt="ロレックス"></a></li>
          <li><a href=""><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/btn_history02.jpg" alt="パテックフィリップ"></a></li>
          <li><a href=""><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/btn_history03.jpg" alt="ウブロ"></a></li>
          <li><a href=""><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/btn_history04.jpg" alt="オーデマピゲ"></a></li>
          <li><a href=""><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/btn_history05.jpg" alt="ブライトリング"></a></li>
          <li><a href=""><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/btn_history06.jpg" alt="ブレゲ"></a></li>
        </ul>
      </section>
      
      <section id="cat-point">
        <h3>高価買取のポイント</h3>
        <ul class="list-unstyled">
          <li>
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/catitem-point1.png"
            alt="Point1. 商品情報が明確だと査定がスムーズ ブランド名、モデル名が明確だと査定がスピーディに出来、買取価格にもプラスに働きます。また、新作や人気モデル、人気ブランドであれば買取価格がプラスになります。">
          </li>
          <li>
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/catitem-point2.png"
            alt="Point2. 数点まとめての査定だと買取がスムーズ 数点まとめての査定依頼ですと、買取価格をプラスさせていただきます。">
          </li>
          <li>
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/catitem-point3.png"
            alt="Point3. 品物の状態がよいほど査定がスムーズ お品物の状態が良ければ良いほど、買取価格もプラスになります。">
          </li>
        </ul>
        <p>腕時計の査定の際にぜひお持ちいただきたいのは、購入時に同封されていた保証書や明細書。<br>
				そのモデルの種類、購入価格等がわかればよりスムーズに査定を行うことができます。購入時の箱や付属品のベルト等、付属品がそろっていれば、より査定金額が高くなるでしょう。<br>
				もちろん、保証書や箱などの付属品がなくても時計そのものの価値は変わりませんのでご心配なく。ベルトがボロボロになっている場合なども、時計本体の状態によっては高い査定価格のご提示も可能です。<br>
				手軽に利用できるLINE査定などもありますので、ぜひお気軽にご相談ください。</p>
      </section>
      
      <section id="lp-cat-jisseki">
        <h3 class="text-hide">買取実績</h3>
            <ul id="jisseki-type" class="clearfix">
            	<li><a href="">ブランド時計</a></li>
            	<li><a href="">高級装飾時計</a></li>
              <li><a href="">懐中時計</a></li>
              <li><a href="">置時計</a></li>
              <li><a href="">アンティーク時計</a></li>
            </ul>
        <ul id="box-jisseki" class="list-unstyled clearfix">
          <?php
            foreach($resultLists as $list):
            // :: で分割
            $listItem = explode('::', $list);
          
          ?>
          <li class="box-4">
            <div class="title">
              <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/<?php echo $listItem[0]; ?>" alt="">
              <p class="itemName"><?php echo $listItem[1]; ?><br><?php echo $listItem[2]; ?></p>
              <hr>
              <p>
                <span class="red">A社</span>：<?php echo $listItem[3]; ?>円<br>
                <span class="blue">B社</span>：<?php echo $listItem[4]; ?>円
              </p>
            </div>
            <div class="box-jisseki-cat">
              <h3>買取価格例</h3>
              <p class="price"><?php echo $listItem[5]; ?><span class="small">円</span></p>
            </div>
            <div class="sagaku">
              <p><span class="small">買取差額“最大”</span><?php echo $listItem[6]; ?>円</p>
            </div>
          </li>
          <?php endforeach; ?>
        </ul>
        
        <div id="konna">
        	<p class="example1">■風防割れ</p>
          <p class="example2">■ベルトのよれ、劣化</p>
          <p class="example3">■文字盤焼け</p>
          <p class="text">その他：箱、ギャランティー、付属品無し、電池切れや故障による不動品でもお買取りいたします。</p>
        </div>
      </section>
      
      <section id="about_kaitori" class="clearfix">
      	<?php
        // 買取について
        get_template_part('_widerange');
				get_template_part('_widerange2');
      ?>
      </section>
      
      <section id="list-brand" class="clearfix">
        <h3>ブランドリスト</h3>
        <ul class="list-unstyled">
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat1.jpg" alt="ロレックス"></dd>
              <dt>ロレックス</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat2.jpg" alt="ブライトリング"></dd>
              <dt>ブライトリング</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat3.jpg" alt="フランクミューラー"></dd>
              <dt>フランクミューラー</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat4.jpg" alt="IWC"></dd>
              <dt>IWC</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat5.jpg" alt="ウブロ"></dd>
              <dt>ウブロ</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat6.jpg" alt="オメガ"></dd>
              <dt>オメガ</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat7.jpg" alt="ブレゲ></dd>
              <dt>ブレゲ</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat8.jpg" alt="ブルガリ"></dd>
              <dt>ブルガリ</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat9.jpg" alt="カルティエ"></dd>
              <dt>カルティエ</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat10.jpg" alt="ジラールペラゴ"></dd>
              <dt>ジラールペラゴ</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat11.jpg" alt="オーデマピゲ"></dd>
              <dt>オーデマピゲ</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat12.jpg" alt="ECW"></dd>
              <dt>ECW</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat13.jpg" alt="パテックフィリップ"></dd>
              <dt>パテックフィリップ</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat14.jpg" alt="ジャガールクルト"></dd>
              <dt>ジャガールクルト</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat15.jpg" alt="カシオ"></dd>
              <dt>カシオ</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat16.jpg" alt="ブティ"></dd>
              <dt>ブティ</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat17.jpg" alt="オリス"></dd>
              <dt>オリス</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat18.jpg" alt="ロンジン"></dd>
              <dt>ロンジン</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat19.jpg" alt="グラハム"></dd>
              <dt>グラハム</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat20.jpg" alt="コルム"></dd>
              <dt>コルム</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat21.jpg" alt="ヴァシュロンコンスタンタン"></dd>
              <dt style="font-size:10px;">ヴァシュロンコンスタンタン</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat22.jpg" alt="ジン"></dd>
              <dt>ジン</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat23.jpg" alt="ブシュロン"></dd>
              <dt>ブシュロン</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat24.jpg" alt="ロジェデュブイ"></dd>
              <dt>ロジェデュブイ</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat25.jpg" alt="セイコー"></dd>
              <dt>セイコー</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat26.jpg" alt="ジェラルドジェンダ"></dd>
              <dt>ジェラルドジェンダ</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat27.jpg" alt="ゼニス"></dd>
              <dt>ゼニス</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat28.jpg" alt="チュードル"></dd>
              <dt>チュードル</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat29.jpg" alt="ボーム＆メルシェ"></dd>
              <dt>ボーム＆メルシェ</dt>
            </dl>
          </li>
          <li>
            <dl>
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/watch/t-watchcat30.jpg" alt="ショパール"></dd>
              <dt>ショパール</dt>
            </dl>
          </li>
        </ul>
			</section>
      
      <section>
          <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/bn_matomegai.png">
      </section>
      
      <?php
        // 買取基準
        get_template_part('_criterion');

        // NGアイテム
        get_template_part('_no_criterion');
        
        // アクションポイント
        get_template_part('_action');
        
        // 買取方法
        get_template_part('_purchase');
      ?>
      
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
