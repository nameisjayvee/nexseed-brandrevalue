<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>

	<div id="primary" class="introduction content-area">
		<main id="main" class="site-main" role="main">
      <section id="mainVisual" style="background:url(<?php echo get_s3_template_directory_uri() ?>/img/mv/introduction.png)">
        <h2 class="text-hide"><?php echo get_the_title(); ?></h2>
      </section>
      
      <section id="top-jisseki">
        <h2 class="text-hide"><?php echo get_the_title(); ?></h2>
        <ul id="box-jisseki" class="list-unstyled clearfix">
        <?php 
		$paged = get_query_var('paged') ? get_query_var('paged') : 1;
		$args = array(
				'post_type' => 'introduction',
				'posts_per_page' => 20,
				'category_name' => 'wallet',
				'paged' => $paged,
			);
			$loop = new WP_Query($args);
			if(have_posts()) :
			while($loop->have_posts()) : $loop->the_post(); ?>
          <li class="box-4">
            <div class="title">
              <?php echo wp_get_attachment_image(get_post_meta($post->ID, 'item_image', true),'item_images'); ?>
              <p class="itemName"><?php echo get_post_meta($post->ID,'item_name',true); ?></p>
            </div>
            <div class="">
              <h3>買取実績金額</h3>
              <p class="price">¥<?php echo get_post_meta($post->ID,'item_price',true); ?>-</p>
            </div>
          </li>
          <?php endwhile; endif; ?>
        </ul>
        
        <?php 
		if(function_exists('wp_pagenavi')){
			wp_pagenavi(array('query'=>$loop));
		}
		wp_reset_postdata(); ?>
      
      </section>
            
      <?php
        // アクションポイント
        get_template_part('_action');
        
        // 買取方法
        get_template_part('_purchase');
        
        // 店舗案内
        get_template_part('_shopinfo');
      ?>
      
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
