<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  'watch/top/001.jpg::パテックフィリップ::カラトラバ 3923::646,000::640,000::650,000::10,000',
  'watch/top/002.jpg::パテックフィリップ::アクアノート 5065-1A::1,367,000::1,350,000::1,400,000::50,000',
  'watch/top/003.jpg::オーデマピゲ::ロイヤルオーク・オフショア 26170ST::1,200,000::1,195,000::1,210,000::15,000',
  'watch/top/004.jpg::ROLEX::サブマリーナ 116610LN ランダム品番::720,000::700,000::740,000::40,000',
  'watch/top/005.jpg::ROLEX::デイトナ 116505 ランダム品番 コスモグラフ::1,960,000::1,950,000::2,000,000::50,000',
  'watch/top/006.jpg::ブライトリング::クロノマット44::334,000::328,000::340,000::12,000',
  'watch/top/007.jpg::パネライ::ラジオミール 1940::594,000::590,000::600,000::10,000',
  'watch/top/008.jpg::ボールウォッチ::ストークマン ストームチェイサープロ CM3090C::128,000::125,000::130,000::5,000',
  'watch/top/009.jpg::ブレゲ::クラシックツインバレル 5907BB12984::615,000::610,000::626,000::16,000',
  'watch/top/010.jpg::ウブロ::ビッグバン ブラックマジック::720,000::695,000::740,000::45,000',
  'watch/top/011.jpg::オメガ::シーマスター プラネットオーシャン 2208-50::192,000::190,000::200,000::10,000',
  'watch/top/012.jpg::ディオール::シフルルージュ クロノグラフ CD084612 M001::190,000::180,000::200,000::20,000',
);

get_header(); ?>

    <div id="primary" class="cat-page content-area">
        <main id="main" class="site-main" role="main">
            <section id="mainVisual" style="background:url(<?php echo get_s3_template_directory_uri() ?>/img/mv/ginza_mv.png)">
                <h2 class="text-hide">銀座店</h2>
            </section>
            <section id="catchcopy">
                <h3>銀座でジュエリー・時計・バッグの買取店をお探しの皆様へ。<br> ブランド買取なら高額査定のBRAND REVALUEへ。</h3>
                <p>最寄り駅は、丸ノ内線・銀座線・日比谷線「銀座駅」のA5出口。<br> BRANDREVALUE(ブランドリバリュー)銀座店は、東京のシンボル三越銀座店の正面に位置する四谷学院ビル5階に店舗を構えております。
                    <br>
                    <br>
                </p>
            </section>
            <section id="company">
                <table class="table table-bordered">
                    <tr>
                        <th>住所</th>
                        <td class="shop_map">〒104-0061<br> 東京都中央区銀座5-8-3 四谷学院ビル5階<a class="map_btn" href="https://goo.gl/maps/P7Hh7E9ayG22" target="_blank">Google MAP</a></td>
                    </tr>
                    <tr>
                        <th>営業時間</th>
                        <td>11：00～21：00</td>
                    </tr>
                    <tr>
                        <th>電話番号</th>
                        <td><a href="tel:0120-970-060">0120-970-060</a></td>
                    </tr>
                    <tr>
                        <th>駐車場</th>
                        <td> - </td>
                    </tr>
                </table>
                <div class="shop_more"><a href="<?php echo home_url('ginza'); ?>">詳しくはこちら</a></div>


            </section>
            <section id="mainVisual" style="background:url(<?php echo get_s3_template_directory_uri() ?>/img/mv/shibuya_mv.png)">
                <h2 class="text-hide">渋谷店</h2>
            </section>
            <section id="catchcopy">
                <h3>渋谷で時計・バッグ・ジュエリー・貴金属の買取店をお探しの皆様へ。 ブランド買取なら高額査定のBRAND REVALUEへ。</h3>
                <p>最寄り駅は、JR・地下鉄・私鉄各線が通る「渋谷駅」。BRANDREVALUE(ブランドリバリュー)渋谷店は、渋谷口のハチ公口を出て、東京のシンボルの一つであるタワーレコードのほぼ正面に位置する和光ビル4階に店舗を構えております。 渋谷駅から徒歩3分と、非常にアクセスしやすい場所にあり、原宿・表参道にお越しの際にも便利にご利用頂けます。<br>
                    <br>
                </p>
            </section>
            <section id="company">
                <table class="table table-bordered">
                    <tr>
                        <th>住所</th>
                        <td class="shop_map">
                            東京都渋谷区神南1-12-16 和光ビル4階<a class="map_btn" href="https://goo.gl/maps/2i5igG64gFp" target="_blank">Google MAP</a></td>
                    </tr>
                    <tr>
                        <th>営業時間</th>
                        <td>11：00～21：00</td>
                    </tr>
                    <tr>
                        <th>電話番号</th>
                        <td><a href="tel:0120-970-060">0120-970-060</a></td>
                    </tr>
                    <tr>
                        <th>駐車場</th>
                        <td> - </td>
                    </tr>
                </table>
                <div class="shop_more"><a href="<?php echo home_url('shibuya'); ?>">詳しくはこちら</a></div>

            </section>
            <section id="mainVisual" style="background:url(<?php echo get_s3_template_directory_uri() ?>/img/shop/shinjuku_mv.png)">
                <h2 class="text-hide">新宿店</h2>
            </section>
            <section id="catchcopy">
                <h3>新宿でジュエリー・時計・バッグなどのブランド品買取先をお探しの皆様へ。ブランド買取なら高額査定のBRAND REVALUEへ。</h3>
                <p>最寄り駅は、JR・地下鉄・私鉄各線が通る、「JR新宿駅」「東京メトロ新宿三丁目駅」。<br>BRANDREVALUE(ブランドリバリュー)新宿店は、新宿地下道A5出口を出て、東京のシンボルの一つである新宿伊勢丹の向かいに立つUFJ銀行の真裏に位置する大伸第２ビル3階び店舗を構えております。<br>最寄り駅出口から徒歩3分と、非常にアクセスしやすい場所にあり、近くには伊勢丹、ビックロ、マルイ等、お買い物、お食事、ついででお越しの際にも便利にご利用頂けます。
                </p>
            </section>
            <section id="company">
                <table class="table table-bordered">
                    <tr>
                        <th>住所</th>
                        <td class="shop_map">
                            〒160-0022<br>東京都新宿区新宿3丁目31-1大伸第２ビル3階<a class="map_btn" href="https://goo.gl/maps/dYjcme5kcj72" target="_blank">Google MAP</a></td>
                    </tr>
                    <tr>
                        <th>営業時間</th>
                        <td>11：00～21：00</td>
                    </tr>
                    <tr>
                        <th>電話番号</th>
                        <td><a href="tel:0120-970-060">0120-970-060</a></td>
                    </tr>
                    <tr>
                        <th>駐車場</th>
                        <td> - </td>
                    </tr>
                </table>
                <div class="shop_more last"><a href="<?php echo home_url('shinjuku'); ?>">詳しくはこちら</a></div>

            </section>
            
            
            <section id="mainVisual" style="background:url(<?php echo get_s3_template_directory_uri() ?>/img/shop/shinsaibashi_mv.png)">
                <h2 class="text-hide">心斎橋店</h2>
            </section>
            <section id="catchcopy">
                <h3> 大阪・心斎橋でジュエリー・時計・バッグなどのブランド品買取先をお探しの皆様へ。ブランド買取なら高額査定のBRAND REVALUEへ。</h3>
                <p>銀座・渋谷・新宿で高価買取実績のブランドリバリュー（BRAND REVALUE）がついに大阪心斎橋でオープン！<br>最寄り駅は、地下鉄御堂筋線「心斎橋駅」。ルイヴィトンの目の前という好立地で心斎橋駅3番出口徒歩3秒のビルに店舗を構えております。駅スグなので商店街を通ることもなく商品の持ち運びはラクラク。お気軽にご利用いただけます。
                </p>
            </section>
            <section id="company">
                <table class="table table-bordered">
                    <tr>
                        <th>住所</th>
                        <td class="shop_map">
                            〒542-0081<br>大阪府大阪市中央区南船場4-4-8 クリエイティブ心斎橋8階<a class="map_btn" href="https://goo.gl/maps/3JQXWvJ3idF2" target="_blank">Google MAP</a></td>
                    </tr>
                    <tr>
                        <th>営業時間</th>
                        <td>11：00～21：00</td>
                    </tr>
                    <tr>
                        <th>電話番号</th>
                        <td><a href="tel:0120-970-060">0120-970-060</a></td>
                    </tr>
                    <tr>
                        <th>駐車場</th>
                        <td> - </td>
                    </tr>
                </table>
                <div class="shop_more last"><a href="<?php echo home_url('shinsaibashi'); ?>">詳しくはこちら</a></div>

            </section>








            <?php
        // アクションポイント
        get_template_part('_action');
        
        // 買取方法
        get_template_part('_purchase');
      ?>
        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->

    <?php
get_sidebar();
get_footer();
