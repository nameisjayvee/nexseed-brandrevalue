<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */


// 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  '001.jpg::::ベルルッティ　アニバーサリー・カリグラフ・アレッサンドロ::142,000::136,000::150,000::14,000',
  '002.jpg::::シャネル　ココマーク　レザースニーカー::38,000::32,000::41,000::9,000',
  '003.jpg::::グッチ　モンクストラップレザーシューズ::8,500::8,000::10,000::2,000',
  '004.jpg::::グッチ　ホースビットローファー::13,500::12,800::14,000::1,200',
  '005.jpg::::サルバトーレフェラガモ　ビットローファー::17,200::16,500::18,000::1,500',
  '006.jpg::::クリスチャンルブタン　メッシュヒールパンプス::27,000::24,000::31,000::7,000',
  '007.jpg::::サルバトーレフェラガモ　ヴェラパンプス::11,500::10,000::13,000::3,000',
  '008.jpg::::シャネル　マトラッセヒールブーツ::46,000::41,000::50,000::9,000',
  '009.jpg::::ボッテガヴェネタ　イントレチャートフラットシューズ::18,000::15,000::21,000::6,000',
  '010.jpg::::ジミーチュウ　スタッズムートンブーツ::23,000::20,000::27,000::7,000',
  '011.jpg::::ミュウミュウ　ヒールパンプス::13,000::14,000::15,000::2,000',
  '012.jpg::::エルメス　レザースニーカー::35,000::38,000::40,000::5,000',
);


get_header(); ?>

<p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
<p class="main_bottom">ブランド靴”高価買取”宣言！満足価格で買取ります！</p>
<div id="primary" class="cat-page content-area">
<main id="main" class="site-main" role="main">
<div id="lp_head" class="shoes_ttl">
<div>
<p>銀座で最高水準の査定価格・サービス品質をご体験ください。</p>
<h2>あなたの高級ブランド靴<br />どんな物でもお売り下さい！！</h2>
</div>
</div>

<p id="catchcopy">多くのブランド買取ショップでは貴金属やジュエリーには力を入れている一方、洋服や靴といったアパレル品の査定のノウハウが乏しいケースも多いと言われています。こうした場合、せっかくのお値打ち品もびっくりするような安い買取額になってしまう可能性も。その点BRAND REVALUEは新しい店舗ではありますが、系列店で長年アパレル買取の実績を積み重ねています。
      <br>
      ブランド靴の買取に精通したプロの鑑定士が古くなった靴でも適正に価値を査定いたしますので、他店に負けない高額査定が可能となっております。</p>
      
      <section id="hikaku" class="shoes_hikaku">
        <h3 class="text-hide">他社の買取額と比較すると</h3>
        <p class="hikakuName">シャネル　マトラッセヒールブーツ</p>
        <p class="hikakuText">※状態が良い場合や当店で品薄の場合などは<br>特に高価買取致します。</p>
        <p class="hikakuPrice1"><span class="red">A社</span>：46,000円</p>
        <p class="hikakuPrice2"><span class="blue">B社</span>：41,000円</p>
        <p class="hikakuPrice3">50,000<span class="small">円</span></p>
      </section>
      <section class="kaitori_cat">
          <ul>
              <li>
                  <a href="https://kaitorisatei.info/brandrevalue/blog/doburock"><img src="https://img.kaitorisatei.info/brandrevalue/wp-content/uploads/2018/12/caf61dc6779ec6137c0ab58dfe3a550d.jpg" alt="どぶろっく"></a>
              </li>
          </ul>
      </section>      
      
      <section id="cat-point">
        <h3>高価買取のポイント</h3>
        <ul class="list-unstyled">
                    <li>
                        <p class="pt_tl">商品情報が明確だと査定がスムーズ</p>
                        <p class="pt_tx">ブランド名、モデル名が明確だと査定がスピーディに出来、買取価格にもプラスに働きます。また、新作や人気モデル、人気ブランドであれば買取価格がプラスになります。</p>
                    </li>
                    <li>
                        <p class="pt_tl">数点まとめての査定だと
                            <br>キャンペーンで高価買取が可能</p>
                        <p class="pt_tx">数点まとめての査定依頼ですと、買取価格をプラスさせていただきます。</p>
                    </li>
                    <li>
                        <p class="pt_tl">品物の状態がよいほど
                            <br>高価買取が可能</p>
                        <p class="pt_tx">お品物の状態が良ければ良いほど、買取価格もプラスになります。</p>
                    </li>
                </ul>
        <p>ブランド靴の査定額は、ちょっとした工夫で高くなることがあるのをご存知でしょうか。<br>
        それは「ご自宅で簡単にメンテナンスをしてから査定に出す」というもの。モノ自体がよくても、埃がたまっているなど汚れが目立つブランド靴は査定額が下がってしまう場合があるのです。ご自宅でできる範囲で表面の汚れをふき取り、中敷きや縫い目などの細かい部分にたまった埃をとるだけでも評価額が上がるので、ぜひお試しください。<br>その他、購入時の箱や袋、保証書等の付属品が一式そろっていると、査定金額が上がります。</p>
      </section>
      
      <section id="lp-cat-jisseki">
        <h3 class="text-hide">買取実績</h3>
            
        <ul id="box-jisseki" class="list-unstyled clearfix">
          <?php
            foreach($resultLists as $list):
            // :: で分割
            $listItem = explode('::', $list);
          
          ?>
          <li class="box-4">
            <div class="title">
              <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/shoes/lp/<?php echo $listItem[0]; ?>" alt="">
              <p class="itemName"><?php echo $listItem[1]; ?><br><?php echo $listItem[2]; ?></p>
              <hr>
              <p>
                <span class="red">A社</span>：<?php echo $listItem[3]; ?>円<br>
                <span class="blue">B社</span>：<?php echo $listItem[4]; ?>円
              </p>
            </div>
            <div class="box-jisseki-cat">
              <h3>買取価格例</h3>
              <p class="price"><?php echo $listItem[5]; ?><span class="small">円</span></p>
            </div>
            <div class="sagaku">
              <p><span class="small">買取差額“最大”</span><?php echo $listItem[6]; ?>円</p>
            </div>
          </li>
          <?php endforeach; ?>
        </ul>
      <section id="list-brand" class="clearfix">
       <h3>ブランドリスト</h3>
      <ul class="list-unstyled">
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/hermes'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch33.jpg" alt="エルメス"></dd>
                                <dt><span>Hermes</span>エルメス</dt>
                            </a>
                        </dl>
                    </li>
          <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/celine'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch29.jpg" alt="セリーヌ"></dd>
                                <dt><span>CELINE</span>セリーヌ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/wallet/louisvuitton'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch23.jpg" alt="ルイヴィトン"></dd>
                                <dt><span>LOUIS VUITTON</span>ルイヴィトン</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/chanel'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag01.jpg" alt="シャネル"></dd>
                                <dt><span>CHANEL</span>シャネル</dt>
                            </a>
                        </dl>
                    </li>         
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/gucci'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch25.jpg" alt="グッチ"></dd>
                                <dt><span>GUCCI</span>グッチ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/prada'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag02.jpg" alt="プラダ"></dd>
                                <dt><span>PRADA</span>プラダ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/fendi'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag03.jpg" alt="プラダ"></dd>
                                <dt><span>FENDI</span>フェンディ</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/dior'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag04.jpg" alt="ディオール"></dd>
                                <dt><span>Dior</span>ディオール</dt>
                            </a>
                        </dl>
                    </li>
                    <li>
                        <dl>
            <a href="<?php echo home_url('/cat/bag/saint_laurent'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/bag/bag05.jpg" alt="サンローラン"></dd>
                                <dt><span>Saint Laurent</span>サンローラン</dt>
                            </a>
                        </dl>
                    </li>
          <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/bottegaveneta'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch32.jpg" alt="ボッテガヴェネタ"></dd>
                                <dt><span>Bottegaveneta</span>ボッテガヴェネタ</dt>
                            </a>
                        </dl>
                    </li>
          <li>
                        <dl>
                            <a href="<?php echo home_url('/cat/bag/ferragamo'); ?>">
                                <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/watch/watch31.jpg" alt="フェラガモ"></dd>
                                <dt><span>Ferragamo</span>フェラガモ</dt>
                            </a>
                        </dl>
                    </li>         
      </ul>
      </section>        <div id="konna" class="konna_shoes">
          <p class="example1">■ヒール釘が出ている</p>
          <p class="example2">■傷がついている</p>
          <p class="example3">■中・外の汚れ</p>
          <p class="text">その他：裏の汚れ等がある状態でもお買取りいたします。</p>
        </div>
      </section>
      
      <section id="about_kaitori" class="clearfix">
        <?php
        // 買取について
        get_template_part('_widerange');
        get_template_part('_widerange2');
      ?>
      </section>
      

      
      <section>
          <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/bn_matomegai.png">
      </section>
      
      <?php
        // 買取基準
        get_template_part('_criterion');

        // NGアイテム
        get_template_part('_no_criterion');
        
        // カテゴリーリンク
        get_template_part('_category');
      ?>
      <section id="kaitori_genre">
        <h3 class="text-hide">その他買取可能なジャンル</h3>
        <p>【買取ジャンル】バッグ/ウエストポーチ/セカンドバック/トートバッグ/ビジネスバッグ/ボストンバッグ/クラッチバッグ/トランクケース/ショルダーバッグ/ポーチ/財布/カードケース/パスケース/キーケース/手帳/腕時計/ミュール/サンダル/ビジネスシューズ/パンプス/ブーツ/ペアリング/リング/ネックレス/ペンダント/ピアス/イアリング/ブローチ/ブレスレット/ライター/手袋/傘/ベルト/ペン/リストバンド/アンクレット/アクセサリー/サングラス/帽子/マフラー/ハンカチ/ネクタイ/ストール/スカーフ/バングル/カットソー/アンサンブル/ジャケット/コート/ブルゾン/ワンピース/ニット/シャツメンズ/毛皮/Tシャツ/キャミソール/タンクトップ/パーカー/ベスト/ポロシャツ/ジーンズ/スカート/スーツなど</p>
      </section>
      
      <?php
        // 買取方法
        get_template_part('_purchase');
      ?>
      
      <section id="user_voice">
        <h3>ご利用いただいたお客様の声</h3>
        
        <p class="user_voice_text1">ちょうど家の整理をしていたところ、家のポストにチラシが入っていたので、ブランドリバリューに電話してみました。今まで買取店を利用したことがなく、不安でしたがとても丁寧な電話の対応とブランドリバリューの豊富な取り扱い品目を魅力に感じ、出張買取を依頼することにしました。
絶対に売れないだろうと思った、動かない時計や古くて痛んだバッグ、壊れてしまった貴金属のアクセサリーなども高額で買い取っていただいて、とても満足しています。古紙幣や絵画、食器なども買い取っていただけるとのことでしたので、また家を整理するときにまとめて見てもらおうと思います。</p>

      <h4>鑑定士からのコメント</h4>
      <div class="clearfix">
        <p class="user_voice_text2">家の整理をしているが、何を買い取ってもらえるか分からないから一回見に来て欲しいとのことでご連絡いただきました。
買取店が初めてで不安だったが、丁寧な対応に非常に安心しましたと笑顔でおっしゃって頂いたことを嬉しく思います。
物置にずっとしまっていた時計や、古いカバン、壊れてしまったアクセサリーなどもしっかりと価値を見極めて高額で提示させて頂きましたところ、お客様もこんなに高く買取してもらえるのかと驚いておりました。
これからも家の不用品を整理するときや物の価値判断ができないときは、すぐにお電話しますとおっしゃって頂きました。</p>
      
        <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/photo-user-voice.jpg" class="right">
      </div>
      </section>
      
<section>
  <div class="cont_widerange">
              <h4>靴買取で人気の取扱ブランド<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
              <ul>
                  <li>靴は、その日のファションを仕上げる最後のマストアイテムです。その日の気分や着こなしで様々なデザインやスタイル、ジャンルから選べるのも、靴ならではの魅力と言えるでしょう。そのため、玄関やシューズボックスにはたくさんの靴があふれているという人も少なくはないでしょう。<br>
もう履かなくなったブランド靴や服の好みが変わったために出番が少なくなったブランドの靴は、ブランド専門に扱う買取店で買取ってもらってみましょう。ブランド靴の価値を適正に査定してくれるので、なかには高額買取になる靴もあるかもしれません。買取ってもらったお金をもとに、新しいブランド靴を買う軍資金にするなど買取システムを有効に活用してみることをおすすめします。<br>
履いた時に脚のラインがきれいに見える、長い時間履いていても疲れない、自分の足の形に沿ってくれるから靴擦れなどになりにくいなど、ブランド靴にはメリットがたくさんあります。そのため、一度履くと同じブランドでリピートする人も少なくありませんが、中には廃盤になってしまった靴のモデルもあります。そんなモデルの靴を中古市場で探している人がいることも多く、買取や中古市場でも人気のブランド靴は常に需要が高いのです。<br>
靴の買取で取扱いブランドとなるのは一般的に人気ブランドであることが多く、フェラガモ、セルジオロッシ、ジミーチュウ、クリスチャンルブタン、グッチ、エルメス、シャネル、ボッテガヴェネタなど世界屈指のシューメーカーやレザーブランドを中心に、有名ファッションブランドが取扱いブランドとして買取取扱いされることが多いようです。しかし、その他にも靴を発表している有名ブランドはたくさんあり、買取店によって取扱いブランドはことなるため、査定依頼をする前に取扱いブランドについて直接問い合わせるようにしましょう。また査定買取を依頼する買取店は、ブランド買取を専門にしていると同時に、古着や靴など中古アパレル買取経験が豊富な買取店をまず選ぶようにしましょう。取扱いブランドが豊富でしっかり靴の価値を査定してくれる場合が多いようですよ。<br><br>
</li>
              </ul>
          </div>
  <div class="cont_widerange">
              <h4>高額査定になる靴の特徴<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
              <ul>
                  <li>靴はバッグなどのレザーアイテム比べると比較的購入しやすい価格のものが多いため、ついつい衝動買いしてしまうファッションアイテムです。靴が好きな人の中には、玄関やシューズボックス、クローゼットに溢れるほどの数の靴を持っている人もいるでしょう。しかし、好んで履く靴は一部の靴に偏ってしまうことも多く、ブティックで一目惚れして購入したまま一度も履かずにいるブランド靴や、シーズンによって出番が多くなったり少なくなったりする靴もありますよね。あまりにも靴が増えすぎてしまったら、履かなくなった靴を整理してみると良いでしょう。「新しく靴を買っても、もう置き場所がない」と思っていても意外と新しい靴を片付けるスペースがまた空くかもしれません。<br>
いらなくなった靴の中には高級ブランド靴も含まれていることもあります。ブランド靴は、ブランド買取店で高額査定がつくことがあるので、ぜひ一度査定依頼をしてみましょう。せっかく高い定価で購入したブランド靴なのですから、できれば高額査定をしてもらいたいですよね。そこで売却したいブランド靴を少しでも高額査定にするポイントを、査定に出す前にしっかり押さえておきましょう。<br>
まずブランド靴の査定は、靴の状態と、中古市場での人気や需要が大きく影響します。言うまでもなく購入したままで一度も履いていない人気のブランド靴であれば高額査定になることは間違いありませんが、何度か履いた靴であっても、査定前にメンテナンスに出したり、汚れやホコリをきれいに落としたりしておくと査定額が上がる場合があるので、ぜひ査定前に一手間をかけるようにしましょう。<br>
また査定に出すブランド靴が季節に左右されるタイプ、例えばミュールやサンダル、ブーツなどの場合は、そのシーズンに入る前に査定してもらうと。中古市場の需要が高くなることを見越して高額査定になる場合もあるので、季節の変わり目などの査定に出すタイミングもチェックしておくようにしましょう。<br><br>
</li>
              </ul>
          </div>
  <div class="cont_widerange">
              <h4>【スニーカーも可！】買取で人気のメンズ靴、レディース靴の種類<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
              <ul>
                  <li>靴には、様々なジャンルやデザインがあります。もちろんメンズ、レディースを問わず有名な人気ブランドからも、フォーマル、エレガント、カジュアル、スポーティなど豊富なコレクションやモデルの種類の靴が発表、販売されています。服のテイストやジャンルによって選ぶ靴も変えることができるので、靴は何足あっても困らない最強のファッションアイテムです。そのため、人気のブランドブティックではもちろん中古市場でも、ブランド靴はオシャレなファッショニスタから常にチェックされているアイテムと言ってもよいでしょう。<br>
靴好きな人に限らず、増え過ぎたブランド靴を整理するためにもブランド買取店に買取ってもらおうと検討している人もいるでしょう。せっかく手放す決心をしたブランド靴でも、いったいどんな種類の靴であれば買取してもらえるのか気になりますよね。基本的に、どんな種類の靴でも買取してもらえます。もちろん、人気ブランドであればカジュアルなスニーカーも可という買取店もたくさんあります。<br>
買取で人気のレディースブランド靴の種類は、どんなシーンにもジャンルにも使えるパンプスやヒールがもっとも人気が高く、ミュール、サンダル、ブーツ、フラットシューズなど幅広く取扱いされているようです。もちろん、取扱いはスニーカーでも可となっています。一方、メンズブランド靴の買取になる種類には、通勤にも使えるトラップシューズやモカシン、ローファー、レースアップシューズ、レザースニーカー、シンプルなスリッポンをはじめ、ブーツ、サンダルなどのシーズンシューズ、カジュアルなハイカットスニーカーもあります。<br>
このようなブランド靴の査定には、靴の状態はもちろん、付属品の有無、中古市場での人気や需要が査定基準になります。そのため、靴の状態を査定前にチェックする時に靴の表面やソールの汚れやホコリを拭き取ったり、靴の箱やシューズバッグ、ギャランティーカードなどの付属品が揃っているかチェックするようにしましょう。</li>
              </ul>
          </div>
</section>

    </main><!-- #main -->
  </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
