<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package BRING
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h2 class="page-title"><?php esc_html_e( '404 - Page Not Found', 'bring' ); ?></h2>
				</header><!-- .page-header -->

				<div class="page-content">
					<p>お探しのページは見つかりませんでした。<br>URLが変更になったか削除された可能性があります。</p>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->
			
			<?php
        // アクションポイント
        get_template_part('_action');
        
        // 買取方法
        get_template_part('_purchase');
        
        // 店舗案内
        get_template_part('_shopinfo');
      ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
