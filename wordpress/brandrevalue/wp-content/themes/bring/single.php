<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package BRING
 */

get_header(); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_format() );

			the_post_navigation();



		endwhile; // End of the loop.
		?>
<?php


        // 幅広いお品物を・・
        get_template_part('_widerange');
        
        // 買取対応の基準
        get_template_part('_criterion');
        
        // NG
        get_template_part('_no_criterion');
        
        // 銀座で最高水準の・・・
        get_template_part('_ginza');
        
        // アクションポイント
        get_template_part('_action');
        
       
        
        // 店舗案内
        get_template_part('_shopinfo');



      ?>
		</main><!-- #main -->
	</div><!-- #primary -->
	

<?php
get_sidebar();
get_footer();
