
/* ベーシック
   ========================================================================== */
$(function(){
	$("#dd li ul").hide();
	$("#dd > ul > li").hover(function() {
		$("ul", this).stop(true, true).slideDown("fast");
		$("> a", this).addClass("active");
	},
	function() {
		$("ul", this).stop(true, true).slideUp("fast");
		$("> a", this).removeClass("active");
	});
});


/* コンテンツ領域 100%
   ========================================================================== */
$(function() {
	$("#dd-mega li > ul").hide();
	$("#dd-mega > ul > li").hover(function() {
		$("ul", this).stop(true, true).slideDown("fast");
		$("> a", this).addClass("active");
	},
	function() {
		$("ul", this).stop(true, true).slideUp("fast");
		$("> a", this).removeClass("active");
	});
});


/* ブラウザ幅 100%
   ========================================================================== */
$(function() {
	$("#dd-full li > ul").hide();
	$("#dd-full > ul > li").hover(function() {
		$("ul", this).stop(true, true).slideDown("fast");
		$("> a", this).addClass("active");
	},
	function() {
		$("ul", this).stop(true, true).slideUp("fast");
		$("> a", this).removeClass("active");
	});
});

$(function() {
        var $header = $('#blog_nav');
        $(window).scroll(function() {
            if ($(window).scrollTop() > 100) {
                $header.addClass('fixed');
            } else {
                $header.removeClass('fixed');
            }
        });
    });
