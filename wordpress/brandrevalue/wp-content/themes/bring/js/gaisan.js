//var $caratNames   = ["0.20ct～0.29ct","0.30ct～0.39ct","0.40ct～0.49ct","0.50ct～0.59ct","0.60ct～0.69ct","0.70ct～0.79ct","0.80ct～0.89ct","0.90ct～0.99ct","1.00ct～1.49ct","1.50ct～1.99ct","2.00ct～2.99ct","3.00ct～3.99ct","4.00ct～4.99ct","5.00ct～5.99ct","6.00ct～6.99ct","7.00ct～7.99ct","8.00ct～8.99ct","9.00ct～9.99ct","10.00ct～10.99ct"];
//var $clarityNames = ["VVS1","VVS2","VS1","VS2","SI1","SI2"];
//var $colorNames   = ["D","E","F","G","H","I","J"];
//var $cutNames     = ["Excellent","Verygood","Good"];
var $caratRanges  = {
	"1" : [0.20,  0.30],
	"2" : [0.30,  0.40],
	"3" : [0.40,  0.50],
	"4" : [0.50,  0.60],
	"5" : [0.60,  0.70],
	"6" : [0.70,  0.80],
	"7" : [0.80,  0.90],
	"8" : [0.90,  1.00],
	"9" : [1.00,  1.50],
	"10": [1.50,  2.00],
	"11": [2.00,  3.00],
	"12": [3.00,  4.00],
	"13": [4.00,  5.00],
	"14": [5.00,  6.00],
	"15": [6.00,  7.00],
	"16": [7.00,  8.00],
	"17": [8.00,  9.00],
	"18": [9.00,  10.00],
	"19": [10.00, 11.00]
};

function separate(num){
	return String(num).replace( /(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
}

function validation() {
	
	if ($('#color_id').val() == '') {
		alert('カラーを選択してください');
		return false;
	}
	
	if ($('#clarity_id').val() == '') {
		alert('クラリティを選択してください');
		return false;
	}
	
	if ($('#cut_id').val() == '') {
		alert('カットを選択してください');
		return false;
	}
	
	var $caratValue = $('#carat_value').val();
	
	if (!$caratValue.match( /^\d+(\.\d{1,3})?$/ )) {
		alert('半角の整数 もしくは 少数（第３位まで）の数字でカラットを入力してください');
		return false;
	}
	
	if ($caratValue < 0.2 || $caratValue >= 11) {
		alert('0.2 以上 11 未満の数字でカラットを入力してください');
		return false;
	}

	return true;
}

function getCaratID($caratValue) {
	
	var $caratID = false;
	
	$.each($caratRanges, function($caratIndex, $caratRange) {
	
		if ($caratValue >= $caratRange[0] && $caratValue < $caratRange[1]) {
			
			$caratID = $caratIndex;
			return false;
		}
		
		
	});
	
	return $caratID;
}
/*
$(function() {
	
	$.getJSON("/diamond/files/price_list.txt" , function($priceList) {
	
		var $priceListDom = '';
		
		$.each($priceList, function($caratIndex, $clarities) {
			
			$priceListDom += '<p class="carat_title" id="carat' + $caratIndex + '">' + $caratNames[($caratIndex - 1)] + '&emsp;買取相場</p>';
			
			$.each($clarities, function($clarityIndex, $colors) {
			
				$priceListDom += '<p class="clarity_title" id="clarity' + $caratIndex + '-' + $clarityIndex + '">' + $clarityNames[($clarityIndex - 1)] + '&emsp;(&nbsp;' + $caratNames[($caratIndex - 1)] + '&nbsp;)</p>';
				
				$priceListDom += '<table class="price_list_table"><tbody id="table' + $caratIndex + '-' + $clarityIndex + '"><tr><th class="color_title"></th>';
				
				$.each($cutNames, function($i, $cutName) {
					$priceListDom += '<th>' + $cutName + '</th>';
				});
				
				$priceListDom += '</tr>';
				
				$.each($colors, function($colorIndex, $cuts) {
				
					$priceListDom += '<tr><td class="color_title">' + $colorNames[($colorIndex - 1)] + '</td>';
					
					$.each($cuts, function($cutIndex, $price) {
					
						$priceListDom += '<td class="price_cell" id="cell' + $caratIndex + '-' + $clarityIndex + '-' + $colorIndex + '-' + $cutIndex + '">&yen;' + separate($price) + '</td>';
					
					});
					
					$priceListDom += '</tr>';
				});
				
				$priceListDom += '</tbody></table>';
			});
		});
		
		$('#price_list').append($priceListDom);
	});
});
*/
$('#estimation_btn').click(function() {
	/*
	if ($('#input_carat_id').val() && $('#input_clarity_id').val() && $('#input_color_id').val() && $('#input_cut_id').val()) {
		$('#cell' + $('#input_carat_id').val() + '-' + $('#input_clarity_id').val() + '-' + $('#input_color_id').val() + '-' + $('#input_cut_id').val()).removeClass('switch');
	}
	*/
	if (validation()) {
		
		var $caratValue = $('#carat_value').val();
		
		var $clarityID = $('#clarity_id').val();
		
		var $colorID = $('#color_id').val();
		
		var $cutID = $('#cut_id').val();
		
		$caratID = getCaratID($caratValue);
		
		$.getJSON("/brandrevalue/wp-content/themes/bring/price_list.txt" , function($priceList) {
		
			if ($priceList[$caratID][$clarityID][$colorID][$cutID]) {
				
				var $price = $priceList[$caratID][$clarityID][$colorID][$cutID];
				
				$price = ($price * $caratValue);
				
				$('#purchase_price').html('&yen;' + separate($price));
				/*
				var $currentCell = $('#cell' + $caratID + '-' + $clarityID + '-' + $colorID + '-' + $cutID);
				
				$currentCell.addClass('switch');
				
				var $target = $('#clarity' + $caratID + '-' + $clarityID).offset().top - $('#price_list_wrapper').offset().top + $('#price_list_wrapper').scrollTop();
				
				$('#price_list_wrapper').animate({scrollTop: $target}, 'fast');
				
				$('#input_carat_id').val($caratID);
				$('#input_clarity_id').val($clarityID);
				$('#input_color_id').val($colorID);
				$('#input_cut_id').val($cutID);
				*/
			}
		});
	}
});