$(function() {
  $('#slide-video').on('click', function() {
    clearInterval(wsSetTimer);
    $('#slide-video-popup, #slide-video-popup-filter').show();
    $('#slide-video-popup video').get(0).play();
  });

  $('#slide-video-popup-filter').on('click', function() {
    $('#slide-video-popup video').get(0).pause();
    $('#slide-video-popup, #slide-video-popup-filter').hide();
  });
});
