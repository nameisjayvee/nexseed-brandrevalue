
/* ベーシック
   ========================================================================== */
$(function(){
	$("#dd li ul").hide();
	$("#dd > ul > li").hover(function() {
		$("ul", this).stop(true, true).slideDown("fast");
		$("> a", this).addClass("active");
	},
	function() {
		$("ul", this).stop(true, true).slideUp("fast");
		$("> a", this).removeClass("active");
	});
});


/* コンテンツ領域 100%
   ========================================================================== */
$(function() {
	$("#dd-mega li > ul").hide();
	$("#dd-mega > ul > li").hover(function() {
		$("ul", this).stop(true, true).slideDown("fast");
		$("> a", this).addClass("active");
	},
	function() {
		$("ul", this).stop(true, true).slideUp("fast");
		$("> a", this).removeClass("active");
	});
});


/* ブラウザ幅 100%
   ========================================================================== */
$(function() {
	$("#dd-full li > ul").hide();
	$("#dd-full > ul > li").hover(function() {
		$("ul", this).stop(true, true).slideDown("fast");
		$("> a", this).addClass("active");
	},
	function() {
		$("ul", this).stop(true, true).slideUp("fast");
		$("> a", this).removeClass("active");
	});
});

$(function() {
        var $header = $('#blog_nav');
        $(window).scroll(function() {
            if ($(window).scrollTop() > 100) {
                $header.addClass('fixed');
            } else {
                $header.removeClass('fixed');
            }
        });
    });


$(function() {
     //初期表示
     $(".liquor_content").hide();//全ての.tab_contentを非表示
     $("ul.liquor_tabs li:first").addClass("active").show();//tabs内最初のliに.activeを追加
     $(".liquor_content:first").show();//最初の.tab_contentを表示
     //タブクリック時
     $("ul.liquor_tabs li").click(function() {
          $("ul.liquor_tabs li").removeClass("active");//.activeを外す
          $(this).addClass("active");//クリックタブに.activeを追加
          $(".liquor_content").hide();//全ての.tab_contentを非表示
          var activeTab = $(this).find("a").attr("href");//アクティブタブコンテンツ
          $(activeTab).fadeIn();//アクティブタブコンテンツをフェードイン
          return false;
     });
});

