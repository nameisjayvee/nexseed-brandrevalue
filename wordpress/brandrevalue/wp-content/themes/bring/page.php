<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>


<style type="text/css">
  .sp-only {
    display: none;
  }
</style>
    <p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
   <?php
   $lp_mainVisBottom_txt = SCF::get( 'lp_mainVisBottom_txt' );
?>
<p class="main_bottom"><?php echo nl2br( $lp_mainVisBottom_txt ); ?></p>
<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();
        $lpMainVis = SCF::get('lp-mainVis');
        if(!empty($lpMainVis)) {
          get_template_part( 'template-parts/content-lp', 'page' );
        } else {
          get_template_part( 'template-parts/content', 'page' );
        }

				// If comments are open or we have at least one comment, load up the comment template.
				/*if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;*/

			endwhile; // End of the loop.
			
 // 店舗案内
        get_template_part('_shopinfo');
            ?>
            
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
