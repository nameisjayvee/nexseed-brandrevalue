<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>

	<div id="primary" class="form-purchase content-area">
		<main id="main" class="site-main" role="main">
      <section id="mainVisual" style="background:url(<?php echo get_s3_template_directory_uri() ?>/img/mv/purchase-kit.png)">
        <h2 class="text-hide">宅配キットお申し込み</h2>
      </section>
      <section id="about-purchase-feature3">
       <div class="takuhai_purchase">
          <dl>
              <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/flow-takuhai01.png" alt="1 お申し込み"></dt>
              <dd>当ホームページの「宅配キットお申し込み」から必要事項を記入していただければ、宅配キットを発送させていただきます。</dd>
          </dl>
          <dl>
              <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/flow-takuhai02.png" alt="2 宅配キット到着"></dt>
              <dd>宅配キットには、段ボール箱、買取査定を希望しているアイテムを梱包するための梱包材と着払い伝票、必要書類等が同封されています。</dd>
          </dl>
          <dl>
              <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/flow-takuhai03.png" alt="3 梱包"></dt>
              <dd>お客様の買い取り希望、査定を希望しているブランドアイテムを査定申込書と身分書を段ボールの箱に同封し、着払い伝票を貼ってください。</dd>
          </dl>
          <dl>
              <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/flow-takuhai04.png" alt="4 発送"></dt>
              <dd>梱包していただいたブランドアイテムを宅配業者にご連絡いただければ集荷にお伺いさせていただきます。日にちや時間してもできますので、ご都合に合わせてご利用ください。また、お近くに佐川急便の店舗がある場合はそちらから持ち込み発送もできます。</dd>
          </dl>
          <dl>
              <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/flow-takuhai05.png" alt="5 査定結果のご連絡"></dt>
              <dd>お客様が発送していただいたブランドアイテムが到着してから1日から3日以内に査定額をFAXまたはメールでご連絡させていただきます。</dd>
          </dl>
          <dl>
              <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/flow-takuhai06.png" alt="6 同意後、即入金"></dt>
              <dd>査定額にご納得いただけたら詳細をメールにてお送りさせていただきます。査定額にご納得いただけなかったらご返答を頂いてからすぐにお送りいただいたブランドアイテムをご返送させていただきます。</dd>
          </dl>
          </div>
       </section>
       
      <section class="kaitori_hoken">
	  <h3>宅配保険サービス</h3>
	  <p class="bol">高額なお品物でも安心してご利用頂けるよう、宅配保険サービスをご用意しております。</p>
	  <dl class="hoken_box">
	  <dt class="hoken_at">必須条件1</dt>
	  <dd><p>事前査定<br />メール・LINE・電話にて事前に査定を受けて下さい。</p></dd>
	  </dl>
	  <dl class="hoken_box">
	  <dt class="hoken_at">必須条件2</dt>
	  <dd><p>当店からお送りする宅配梱包キット<br />（梱包材・着払い伝票）をご使用ください。</p></dd>
	  </dl>
	  <p class="small_txt">※事前査定の金額が補償金額の上限となります。事前査定がない場合は、配送会社の規定に準じます。<br />※ご不明点がございましたら直接お電話にてお問合せくださいませ。</p>
	  
	  
	  </section>

     
      <section id="mailform">
        <div class="">
          <?php echo do_shortcode('[mwform_formkey key="23163"]'); ?>
        </div>
      </section>
      
      <?php
        // アクションポイント
        get_template_part('_action');
        
        // 買取方法
        get_template_part('_purchase');
        
        // 店舗案内
        get_template_part('_shopinfo');
      ?>
      
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
