<?php
    /**
     * The template for displaying all pages.
     *
     * This is the template that displays all pages by default.
     * Please note that this is the WordPress construct of pages
     * and that other 'pages' on your WordPress site may use a
     * different template.
     *
     * @link https://codex.wordpress.org/Template_Hierarchy
     *
     * @package BRING
     */

    get_header(); ?>

<div id="primary" class="about-purchase content-area">
    <main id="main" class="site-main" role="main">
        <div class="mv_area ">
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/tentou_mv.png " alt="その場で即お支払い店頭買取">
        </div>
        <ul class="tentou_bnr">
            <li><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/point01.png" alt="タクシー代お支払い"></li>
            <li><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/point02.png" alt="タクシー代お支払い"></li>
        </ul>
        <h2 class="ttl_blue">ブランドリバリュー店舗ご案内</h2>
        <p>ブランドリバリューは、全店舗駅から徒歩圏内の好立地に店舗を構えております。 お客様が安心して快適に過ごせるように内装にもこだわり、お客様のプライバシーを配慮した個室ブースをご用意しております。
        </p>
        <section id="shopinfo_tentou">
            <div class="shopinfo_wrap">
                <div class="box-shop">
                    <a href="<?php echo home_url('ginza'); ?>">
                        <div class="wrapperShopinfo">
                            <div class="text">
                                <h3>BRANDREVALUE銀座店</h3>
                                <img src="<?php echo get_s3_template_directory_uri() ?>/img/top/top-shop-harajuku.png" alt="BRANDREVALUE(ブランドリバリュー)銀座店" class="shopimage">
                                <p>〒104-0061<br>東京都中央区銀座5-8-3<p>四谷学院ビル5階</p>
                                    <p>TEL 0120-970-060</p>
                                    <p class="shop_link"> 詳しく見る<i class="fas fa-arrow-circle-right"></i></p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="box-shop">
                    <a href="<?php echo home_url('shibuya'); ?>">
                        <div class="wrapperShopinfo">
                            <div class="text">
                                <h3>BRANDREVALUE渋谷店</h3>
                                <img src="<?php echo get_s3_template_directory_uri() ?>/img/top/top-shop-newshibuya.png" alt="BRANDREVALUE(ブランドリバリュー)銀座店" class="shopimage">
                                <p>〒150-0041<br>東京都渋谷区神南1-12-16<p>和光ビル4階</p>
                                    <p>TEL 0120-970-060</p>
                                    <p class="shop_link">詳しく見る<i class="fas fa-arrow-circle-right"></i></p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="box-shop">
                    <a href="<?php echo home_url('shinjuku'); ?>">
                        <div class="wrapperShopinfo">
                            <div class="text">
                                <h3>BRANDREVALUE新宿店</h3>
                                <img src="<?php echo get_s3_template_directory_uri() ?>/img/top/top-shop-nshinjuku.png" alt="BRANDREVALUE(ブランドリバリュー)銀座店" class="shopimage">
                                <p>〒160-0022<br>東京都新宿区新宿3丁目31-1<p>大伸第２ビル3階</p>
                                    <p>TEL 0120-970-060</p>
                                    <p class="shop_link">詳しく見る<i class="fas fa-arrow-circle-right"></i></p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="box-shop">
                    <a href="<?php echo home_url('shinsaibashi'); ?>">
                        <div class="wrapperShopinfo">
                            <div class="text">
                                <h3>BRANDREVALUE心斎橋店</h3>
                                <img src="<?php echo get_s3_template_directory_uri() ?>/img/top/top-shop-00.png" alt="BRANDREVALUE(ブランドリバリュー)心斎橋店" class="shopimage">
                                <p>〒542-0081<br>大阪府大阪市中央区南船場4-4-8<p>クリエイティブ心斎橋8階</p>
                                    <p>TEL 0120-970-060</p>
                                    <p class="shop_link"> 詳しく見る<i class="fas fa-arrow-circle-right"></i></p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </section>

        <div class="takuhai_cvbox tentou">
            <p class="arw_ttl">予約特典で査定金額15％UP中</p>
            <div class="custom_tel takuhai_tel">
                <a href="tel:0120-970-060">
                    <div class="tel_wrap">
                        <div class="telbox01"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/customer/telico.png" alt="お申し込みはこちら">
                            <p>お電話からでもお申込み可能！<br>ご不明な点は、お気軽にお問合せ下さい。 </p>
                        </div>
                        <div class="telbox02"> <span class="small_tx">【受付時間】11:00 ~ 21:00</span><span class="ted_tx"> 年中無休</span>
                            <p>0120-970-060</p>
                        </div>
                    </div>
                </a>
            </div>
            <p><a href="<?php echo home_url('purchase/visit-form'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/tentou_cv01.png" alt="店頭買取お申し込み"></a></p>
        </div>
        <h2 class="ttl_blue">買取の際にご用意いただきもの</h2>
        <p><span class="ft_bold">＜ 本人確認書類 ＞</span> 運転免許証　パスポート　住民基本台帳カード　健康保険証　在留カード　個人番号カード etc…
            <span class="ft_small">※未成年の方に関しましては、保護者からの同意書もしくは同伴が必要です。</span>
        </p>
        <section class="tentou_chara">
            <div class="charabox01">
                <h3>高額商材でも即金対応！業界最高峰の買取金額！</h3>
                <p>「即日、現金が手に入る」「スタッフの顔を見て査定を受けられる」－店頭買取にはさまざななメリットがありますが、 一番大きなメリットは、1,000万円以上を超えるような高額なお品物や一般的に査定が難しいと言われているアンティーク時計などもブランドリバリューでは、即日現金にてご対応が可能です。ブランドリバリューでは、徹底的なコストカットにより実現した「業界最高値」の査定を目指します。

                </p>
            </div>
            <div class="charabox02">
                <h3>全店駅から徒歩5分圏内だから便利！</h3>
                <p>東京都内に4店舗（銀座・新宿・渋谷・神宮前）、大阪に1店舗の合計5店舗を展開しています。ブランドリバリューは、全店駅から徒歩5分圏内の好立地に店舗を構えております。また、ご来店いただいたお客様がストレスなくご利用いただけるように「サロンのような内装」というコンセプトのもと店舗作りをしております。もし店舗の場所がわからない場合は0120-970-060までお問い合わせください。道順を説明いたします。入店後は、研修カリキュラムを修了した査定士が丁寧懇切にご要望にお応えいたします。
                </p>
            </div>
            <div class="charabox03">
                <h3>最速査定！10分～15分でお取引完了！</h3>
                <p>ブランドリバリューは、「ホスピタリティ」＝「おもてなし」の心を大切にお客様のお時間を無駄にしない迅速な査定を心がけております。最短で10分～15分で完了します。
                </p>
            </div>
        </section>
        <section id="about-purchase-feature3">
            <div class="tentou_purchase">
                <dl>
                    <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/flow-tentou01.png" alt="1 店舗までご来店下さい。"></dt>
                    <dd>店舗買取にご来店の際は事前にご連絡を頂ければ幸いです。お待たせすることなく迅速にお客様の買取希望のブランド品を査定させていただきます。</dd>
                </dl>
                <dl>
                    <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/flow-tentou02.png" alt="2 鑑定士が丁寧に査定"></dt>
                    <dd>熟練された経験豊富な鑑定士がご安心できるようお客様のブランドアイテムを目の前で鑑定させていただきます。<br>
                        <br>
                    </dd>
                </dl>
                <dl>
                    <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/flow-tentou03.png" alt="3 査定金額を提示"></dt>
                    <dd>熟練された経験豊富な鑑定士がご安心できるようお客様のブランドアイテムを目の前で鑑定させていただきます。</dd>
                </dl>
                <dl>
                    <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/flow-tentou04.png" alt="4 その場で現金にてお支払い"></dt>
                    <dd>買取成立の場合は、たとえ数百万円単位、一千万円でもその場で店舗買取させていだきます。</dd>
                </dl>
            </div>
            <div class="takuhai_cvbox tentou">
                <p class="arw_ttl">予約特典で査定金額15％UP中</p>
                <div class="custom_tel takuhai_tel">
                    <a href="tel:0120-970-060">
                        <div class="tel_wrap">
                            <div class="telbox01"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/customer/telico.png" alt="お申し込みはこちら">
                                <p>お電話からでもお申込み可能！<br>ご不明な点は、お気軽にお問合せ下さい。 </p>
                            </div>
                            <div class="telbox02"> <span class="small_tx">【受付時間】11:00 ~ 21:00</span><span class="ted_tx"> 年中無休</span>
                                <p>0120-970-060</p>
                            </div>
                        </div>
                    </a>
                </div>
                <p><a href="<?php echo home_url('purchase/visit-form'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/page_tentou/tentou_cv01.png" alt="店頭買取お申し込み"></a></p>
            </div>
        </section>
        <?php
            // お問い合わせ
            get_template_part('_action_tentou');
        ?>
        <section class="top_riyuu">
            <h2><span class="ttl_small">ブランドリバリューが、</span><br>
                選ばれている<span class="tx_red">6つの理由</span></h2>
            <ul class="reason_list">
                <li>
                    <h3><span class="tx_red">最高峰</span>の高額買取</h3>
                    <p>市場調査の徹底や販売ルートの確立により他社よりも高額で買取できる仕組みを整えております。</p>
                </li>
                <li>
                    <h3><span class="tx_red">超スピード</span>買取査定</h3>
                    <p>経験豊富なコンシェルジュが持つブランド知識で正確且つスピーディーな査定でお客様へ査定結果をお知らせ。</p>
                </li>
                <li>
                    <h3>送料・査定料<span class="tx_red">0円</span></h3>
                    <p>宅配買取時の送料・キャンセル料・振込手数料・宅配キット代金・査定料など全て0円で承ります。</p>
                </li>
                <li>
                    <h3><span class="tx_small tx_red">買取額1000万円以上</span><span class="tx_small">でも</span><br>
                        <span class="tx_red">即現金</span>でお支払い</h3>
                    <p>買取金額が1000万円を超える場合でも即現金でお支払い可能。<br>
                        ※お振り込みになる場合もございます。 </p>
                </li>
                <li>
                    <h3><span class="tx_small tx_red">1万点以上の</span><br>
                        取扱ブランド数</h3>
                    <p>1万点以上のブランドを買取対象としているので他社で断られたブランド商品でもお気軽にご相談下さい。</p>
                </li>
                <li>
                    <h3>全店舗<span class="tx_red">駅徒歩圏内</span></h3>
                    <p>ブランドリバリューは全店舗、駅から徒歩圏内の好立地に店舗を構えております。お気軽にお越しください。</p>
                </li>
            </ul>
        </section>

        <section class="takuhai_possible">
            <h2 class="takihai_ttl">お買取りできるもの</h2>
            <ul class="possible_list01">
                <li>
                    <p>バッグ</p>
                    <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible01.png" alt="バッグ">
                </li>
                <li>
                    <p>時計</p>
                    <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible02.png" alt="バッグ">
                </li>
                <li>
                    <p>ダイヤ</p>
                    <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible03.png" alt="バッグ">
                </li>
                <li>
                    <p>宝石</p>
                    <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible04.png" alt="バッグ">
                </li>
                <li>
                    <p>ブランドジュエリー</p>
                    <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible05.png" alt="バッグ">
                </li>
                <li>
                    <p>財布</p>
                    <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible06.png" alt="バッグ">
                </li>
                <li>
                    <p>靴</p>
                    <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible07.png" alt="バッグ">
                </li>
                <li>
                    <p>毛皮</p>
                    <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible08.png" alt="バッグ">
                </li>
                <li>
                    <p>ブランドアパレル</p>
                    <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible09.png" alt="バッグ">
                </li>
                <li>
                    <p>お酒</p>
                    <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/possible10.png" alt="バッグ">
                </li>
            </ul>
            <h3><span class="cr01">「傷」</span>や<span class="cr02">「汚れ」</span>があってもお買い取りさせて下さい！<br> どんな状態であっても必ずお買取りさせて頂きます。
            </h3>
            <ul class="possible_list02">
                <li>

                    <h4>ボロボロになったバッグ</h4>
                    <div>
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/state_ex01.png" alt="ボロボロになったバッグ">
                        <p>・シミ / 汚れ<br>・破れ / 日焼け</p>
                    </div>
                </li>
                <li>

                    <h4>壊れてしまった時計</h4>
                    <div>
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/state_ex02.png" alt="壊れてしまった時計">
                        <p>・不動<br>・パーツ破損</p>
                    </div>
                </li>
                <li>

                    <h4>破損した貴金属、宝石</h4>
                    <div>
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/state_ex03.png" alt="破損した貴金属、宝石">
                        <p>・千切れてしまった<br>・石が取れてしまった<br>・イニシャル入り</p>
                    </div>
                </li>

                <li>

                    <h4>ルース(裸石)</h4>
                    <div>
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/page_takuhai/state_ex04.png" alt="破損した貴金属、宝石">
                        <p>・枠から外れてしまった<br>・鑑定書なし
                        </p>
                    </div>
                </li>


            </ul>



        </section>


        <section class="kaitori_voice">
            <h2 class="ttl_edit01">店頭買取をご利用いただいたお客様の声</h2>
            <ul>
                <li>
                    <p class="kaitori_tab tentou_tab">店頭買取</p>
                    <h4>パネライ　時計　</h4>
                    <p class="voice_txt">新しい時計が気になり、今まで愛用していたパネライの腕時計を買取してらもらうことにしました。<br /> 手元に保管しておくという手もあったのですが、やはり使わずに時計を手元に置いておくよりは、買取資金の足しにした方がいいのかなと思ったので・・・。
                        <br /> 通勤するときに、ブランドリバリューの近くを通るので、通勤ついでにブランドリバリューで試しで査定をしてもらうことにしたのですが、本当にこちらを利用してよかったです。
                        <br /> 私もブランドに詳しい自信がありましたが、査定士さんはそれ以上の知識があり、きちんとブランドの価値をわかってくださいました。本当に買取額に対して満足することができただけではなく、安心して買取手続きをすることができました。
                        <br /> やはり買取査定はブランドリバリューのように、きちんとブランド知識があるところでお願いしないと満足できないですよね。
                    </p>
                </li>
                <li>
                    <p class="kaitori_tab tentou_tab">店頭買取</p>
                    <h4>シャネル　バッグ　</h4>
                    <p class="voice_txt">シャネルのバッグの査定をしてもらいました。<br /> 店頭買取をしてもらったのですが、実はブランドリバリューに出向く前に他社比較もしようと思い、他の買取店でも査定をしてもらっていたんです。 ですが、他社比較の結果、断然！ブランドリバリューは高値の査定を出してくれました！
                        <br /> ブランドリバリューは、きちんと店頭買取をすることができる、いわば実際の店舗が存在するブランド買取店でもあるので、この安心さも兼ねそろえているのが個人的に安心でした。
                        <br /> また、他にもブランドバッグで買取をしてほしいものが出てきたら、絶対にブランドリバリューを利用するつもりです。 もう他社比較はしません！笑
                    </p>
                </li>
                <li>
                    <p class="kaitori_tab tentou_tab">店頭買取</p>
                    <h4>ボッテガヴェネタ　財布</h4>
                    <p class="voice_txt">好みのブランドから、新作のお財布が販売されたので、今使っているボッテガヴェネタの財布を買取してもらおうと思い、ブランドリバリューを利用しました。<br /> 店舗の位置をチェックしてみると、銀座駅からもめっちゃ近かったので、店頭買取を利用しての買取です。
                        <br /> 今回は、ボッテガヴェネタの財布１つだけの買取だったので、このお財布１つだけで、買取査定を依頼するのは何だか申し訳ないなと思ったりもしたのですが、担当の方もとても丁寧で気持ちのいい対応をとってくださったので、本当によかったです。
                        <br /> 査定額も満足することができたので、機会があればまた利用したいと思っています。
                    </p>
                </li>
            </ul>
        </section>
        <?php

            // 買取方法
            get_template_part('_purchase');

        ?>
    </main>
    <!-- #main -->
</div>
<!-- #primary -->

<?php
    get_sidebar();
    get_footer();
