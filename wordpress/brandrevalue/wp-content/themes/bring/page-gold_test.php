<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */




get_header(); ?>

<?php

	/************/
	/* 定数定義 */
	/************/
	const DAY_BORDER_HOUR   = 10;                   // システム内の日付変更時間
	const JSON_GET_URL      = 'https://www.net-japan.co.jp/system/upload/netjapan/export/'; // JSON取得先
	const JSON_GET_PREFIX   = 'price_';             // JSON取得先ファイルの接頭語
	const JSON_GET_SAFFIX   = '_0930.json';         // JSON取得先ファイルの接尾語
	const PRICES_DIR        = '/prices/';           // サーバー内の金額ファイルの設置ディレクトリ
	const JSON_LOCAL_SAFFIX = '.json';              // サーバー内の金額ファイルの接尾語
	const MAX_DATE_GOBACK   = 14;                   // 遡る最大日数
	
	/************/
	/* 関数定義 */
	/************/
	// URLで指定したページ情報取得
	function getApiDataCurl($url)
	{
		$option = array(
			CURLOPT_RETURNTRANSFER => true, //文字列として返す
			CURLOPT_TIMEOUT		=> 3, // タイムアウト時間
		);

		$ch = curl_init($url);
		curl_setopt_array($ch, $option);

		$json	= curl_exec($ch);
		$info	= curl_getinfo($ch);
		$errorNo = curl_errno($ch);

		// OK以外はエラーなので空白を返す
		if ($errorNo !== CURLE_OK) {
			// 詳しくエラーハンドリングしたい場合はerrorNoで確認
			// タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
			return '';
		}

		// 200以外のステータスコードは失敗とみなし空白を返す
		if ($info['http_code'] !== 200) {
			return '';
		}

		// 文字列から変換
		//$jsonArray = json_decode($json, true);

		return $json;
	}
	
	// 金額取得関数
	function getPrices($fileNameBase) {
		
		// サーバー内の金額ファイル名生成
		$localFileName = __DIR__ . PRICES_DIR . $fileNameBase . JSON_LOCAL_SAFFIX;
		
		$json = '';
		
		if (!file_exists($localFileName)) {
		// サーバー内に金額ファイルがない場合、JSON取得して作成
		
			// JSON取得先のURL生成
			$apiFileName = JSON_GET_URL . JSON_GET_PREFIX . $fileNameBase . JSON_GET_SAFFIX;
			
			// JSONを取得できた場合、サーバー内に金額ファイル保存
			if ($json = getApiDataCurl($apiFileName)) {
				// ファイル保存（失敗時はfalseを返却)
				if (!file_put_contents($localFileName, $json)) {
					return FALSE;
				}
			}
		} else {
		// サーバー内に金額ファイルがある場合、読み込む
			if (is_readable($localFileName)) $json = file_get_contents($localFileName);
		}
		
		return $json;
	}
	
	$time = new DateTime();
	$time->setTimestamp(time())->setTimezone(new DateTimeZone('Asia/Tokyo'));
	$hour = $time->format('G'); // 現在時
	
	$data = array();   // データ
	
	for ($dateCnt = 0; $dateCnt < MAX_DATE_GOBACK; $dateCnt++) {
	
		if ($hour < DAY_BORDER_HOUR) {
		// 日付変更時より前なら一日前分
			$fileNameBase = date('Ymd', strtotime('-'.($dateCnt + 1).' day'));
		} else {
		// 日付変更時以降なら当日分
			$fileNameBase = date('Ymd', strtotime('-'.$dateCnt.' day'));
		}
		
		// jsonを取得できたらデコードして繰り返し終了
		if ($json = getPrices($fileNameBase)) {
			$data = json_decode($json, true);
			break;
		}
	}
	
	$market = array();
	$price  = array();
	
	if (!empty($data['data']['market'])) $market = $data['data']['market'];
	if (!empty($data['data']['nj_buy'])) $price  = $data['data']['nj_buy'];
?>
<script>
	// 整数判定関数
	function isInteger(x) {
		x = parseFloat(x);
		return Math.round(x) === x;
	}
	
	// 計算結果表示
	function autoCalc() {
	
		if ($('#unit_price').text() !== '-' && $('#copy_metal_weight').text() !== '-') {
		
			var $result = parseFloat($('#unit_price').text().replace(/,/g, '')) * parseFloat($('#copy_metal_weight').text());
			
			$('#calculation_result').text($result.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
		}
	}
	
	var $prices = new Object();
	$prices['au'] = <?= json_encode($price['au_scrap'], JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT); ?>;
	$prices['pt'] = <?= json_encode($price['pt_scrap'], JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT); ?>;
	$prices['ag'] = <?= json_encode($price['ag_scrap'], JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT); ?>;
	
	$(function(){
		
		// 品種選択時、品位のプルダウン作成
		$("input[name='radio_kind']").change(function() {
			
			$('#dignity option').remove();
			
			$kind = $(this).data('kind');
			
			$options = $.map($prices[$kind], function ($arr) {
				$option = $('<option>', {value: $arr['price'].replace(/,/g, ''), text: $arr['name']});
				return $option;
			});
			
			$options.unshift($('<option>', {value: '-', text: '選択してください'}));
			
			$('#dignity').append($options);
			
			$('#unit_price').text('-');
			$('#calculation_result').text('-');
		});
		
		// 品位選択時、単価と自動計算
		$("#dignity").change(function() {
			
			if ($(this).val() === '-') {
				$('#unit_price').text('-');
				$('#calculation_result').text('-');
			} else {
				$('#unit_price').text($(this).val().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
				autoCalc();
			}
		});
		
		// 重量入力時、重量と自動計算
		$("#input_metal_weight").keyup(function() {
			
			var $input_value = $(this).val().replace(/[０-９]/g, function(s) {
				return String.fromCharCode(s.charCodeAt(0) - 65248);
			});
			
			if ($input_value == '') {
			// 入力が空の場合
				$('#copy_metal_weight').text('-');
				$('#calculation_result').text('-');
				$('#input_metal_weight_err').hide();
				
			} else if (isInteger($input_value)) {
			// 入力が整数の場合
				$('#copy_metal_weight').text($input_value);
				autoCalc();
				$('#input_metal_weight_err').hide();
			} else {
			// 上記以外の場合
				$('#copy_metal_weight').text('-');
				$('#calculation_result').text('-');
				$('#input_metal_weight_err').show();
			}
		});
	});
</script>

<p class="bottom_sub">BRANDREVALUEは、最高額の買取をお約束致します。</p>
<p class="main_bottom">満足価格で買取！金・プラチナ買取ならブランドリバリュー</p>
<div id="primary" class="cat-page content-area">
	<main id="main" class="site-main" role="main">
		<div id="lp_head" class="gold_ttl">
			<div>
				<p>銀座で最高水準の査定価格・サービス品質をご体験ください。</p>
				<h2>あなたの金・プラチナ<br />
					どんな物でもお売り下さい！！</h2>
			</div>
		</div>
		<p id="catchcopy">BRAND REVALUEにはブランド品・貴金属買取に精通したスタッフがそろっており、金（ゴールド）の再販ルートを確立しております。 壊れたアクセサリーに含まれた金などを含め、多少状態に難があっても素材の価値を高く査定し、 お客様にご納得いただける査定額をご提示するのがBRANDREVALUE(ブランドリバリュー)の方針です。 <br>
			さらに、BRANDREVALUE(ブランドリバリュー)はまだ新しい買取店ですので、利益以上にお客様に認知され、愛される店づくりを目指しています。 そのため、「赤字覚悟の高額査定」を実現することができているのです。 </p>
		<section id="hikaku" class="gold_hikaku">
			<h3 class="text-hide">他社の買取額と比較すると</h3>
			<p class="hikakuName">アメリカンイーグル金貨1oz</p>
			<p class="hikakuText">※状態が良い場合や当店で品薄の場合などは <br>
				　特に高価買取致します。 </p>
			<p class="hikakuPrice1"><span class="red">A社</span>：125,832円</p>
			<p class="hikakuPrice2"><span class="blue">B社</span>：123,237円</p>
			<p class="hikakuPrice3">129,724<span class="small">円</span></p>
		</section>
		<section id="cat-point">
			<h3>高価買取のポイント</h3>
			<ul class="list-unstyled">
				<li>
					<p class="pt_tl">商品情報が明確だと査定がスムーズ</p>
					<p class="pt_tx">ブランド名、モデル名が明確だと査定がスピーディに出来、買取価格にもプラスに働きます。また、新作や人気モデル、人気ブランドであれば買取価格がプラスになります。</p>
				</li>
				<li>
					<p class="pt_tl">数点まとめての査定だと <br>
						キャンペーンで高価買取が可能</p>
					<p class="pt_tx">数点まとめての査定依頼ですと、買取価格をプラスさせていただきます。</p>
				</li>
				<li>
					<p class="pt_tl">品物の状態がよいほど <br>
						高価買取が可能</p>
					<p class="pt_tx">お品物の状態が良ければ良いほど、買取価格もプラスになります。</p>
				</li>
			</ul>
			<p>同じゴールド製品であっても、実は「査定の出し方」によって評価額はかなり変化するもの。せっかくなら一円でも高く売りたいという方に、ぜひ知っておいていただきたいポイントがあります。それは、製品が収まっていた「箱・保存袋・ギャランティーカード」等、購入時の付属品はできる限りよい状態で保管し、製品と一緒に査定に出すことです。また、金が含まれる時計・アクセサリー等のトレンドが推移する前に査定すれば、評価額は一層高くなります。</p>
		</section>
		<h2 class="obi_tl_gold">本日の価格情報 <span class="gold_date"><?= $market['price_date'] ?>（10:00更新）現在</span></h2>
		<ul class="gold_box_top">
			<li>
				<h4>金<span>-GOLD-</span></h4>
				<p class="gold_mp"><?= $price['ingod']['au']['price'] ?>円<span>(前日比 <span class="f_red"><?= $price['ingod']['au']['diff'] ?></span>円)</span></p>
			</li>
			<li>
				<h4>銀<span>-SILVER-</span></h4>
				<p class="gold_mp"><?= $price['ingod']['ag']['price'] ?>円<span>(前日比 <span class="f_red"><?= $price['ingod']['ag']['diff'] ?></span>円)</span></p>
			</li>
			<li>
				<h4>Pt<span>-PLATINUM-</span></h4>
				<p class="gold_mp"><?= $price['ingod']['pt']['price'] ?>円<span>(前日比 <span class="f_red"><?= $price['ingod']['pt']['diff'] ?></span>円)</span></p>
			</li>
			<li>
				<h4>Pd<span>-PALLADIUM-</span></h4>
				<p class="gold_mp"><?= $price['ingod']['pd']['price'] ?>円<span>(前日比 <span class="f_red"><?= $price['ingod']['pd']['diff'] ?></span>円)</span></p>
			</li>
		</ul>
		<div class="gold_container">
			<div class="gold_box_wrap01 gft_l">
				<h4>金<span>-GOLD-</span></h4>
				<ul class="gold_inner_list">
<?php foreach ($price['au_scrap'] as $p): ?>
					<li<?php if($p['bold']): ?> class="pick_list"<?php endif; ?>><?= str_replace(' ', '<br>', $p['name']) ?><span><?= $p['price'] ?>円/g</span></li>
<?php endforeach; ?>
				</ul>
			</div>
			<div class="gold_box_wrap02 gft_l">
				<h4>金<span>-GOLD-</span>Pt<span>-PLATINUM-</span></h4>
				<ul class="gold_inner_list ">
<?php foreach ($price['pk_combi'] as $p): ?>
					<li<?php if($p['bold']): ?> class="pick_list"<?php endif; ?>><?= str_replace(' ', '<br>', $p['name']) ?><span><?= $p['price'] ?>円/g</span></li>
<?php endforeach; ?>
				</ul>
			</div>
			<div class="gold_box_wrap03 gft_l">
				<h4>Pt<span>-PLATINUM-</span></h4>
				<ul class="gold_inner_list">
<?php foreach ($price['pt_scrap'] as $p): ?>
					<li<?php if($p['bold']): ?> class="pick_list"<?php endif; ?>><?= str_replace(' ', '<br>', $p['name']) ?><span><?= $p['price'] ?>円/g</span></li>
<?php endforeach; ?>
				</ul>
				<h4 class="gl_sliv">銀<span>-PLATINUM-</span></h4>
				<ul class="gold_inner_list gl_sliv">
<?php foreach ($price['ag_scrap'] as $p): ?>
					<li<?php if($p['bold']): ?> class="pick_list"<?php endif; ?>><?= str_replace(' ', '<br>', $p['name']) ?><span><?= $p['price'] ?>円/g</span></li>
<?php endforeach; ?>
				</ul>
			</div>
		</div>
		<div class="gold_bnr"><img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/gold/gold_bnr.jpg" alt="イエローゴールド" ></div>
		
		<!--買取評価チェックコンテンツ-->
		
		<div class="gold_check">
			<h3>本日の相場を自動計算</h3>
			<form action="" name="sel_form" id="sel_form" class="mark_wrap">
				<ul class="gold_mark">
					<li>
						<p class="gold_input_ttl"><span>●</span>貴金属製品の品種をお選びください。</p>
						<label>
							<input type="radio" name="radio_kind" class="radio01-input" data-kind="au">
							<span class="radio01-parts">金</span> </label>
						<label>
							<input type="radio" name="radio_kind" class="radio01-input" data-kind="pt">
							<span class="radio01-parts">プラチナ</span> </label>
						<label>
							<input type="radio" name="radio_kind" class="radio01-input" data-kind="ag">
							<span class="radio01-parts">銀</span> </label>
					</li>
					<li>
						<p class="gold_input_ttl"><span>●</span>貴金属製品の部位をお選びください。</p>
						<select id="dignity" name="color_id">
							<option value="">選択して下さい</option>
						</select>
					</li>
					<li>
						<p class="gold_input_ttl"><span>●</span>貴金属製品の重量を入力してください。</p>
						<input type="text" id="input_metal_weight" style="width: 100px;" />g
						&nbsp;<span id="input_metal_weight_err" style="color:red; display:none;">整数で入力してください。</span>
					</li>
				</ul>
				<div class="mark_result">
				<p>本日の相場×重量を自動計算</p>
				<table>
				<tr>
				<th width="30%"><span class="ico_fix01">¥</span><span id="unit_price">-</span></th>
				<td>×</td>
				<th width="30%"><span id="copy_metal_weight">-</span>&ensp;<span class="ico_fix02">g</span></th>
				<td>=</td>
				<th width="30%"><span class="ico_fix01">¥</span><span id="calculation_result">-</span></th>
				</tr>
				</table>
				
				
				</div>
				
			</form>
		</div>
		<h3 class="mid">金・プラチナ 一覧</h3>
		<ul class="mid_link">
			<li><a href="<?php echo home_url('/cat/gold/gold-coin'); ?>"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/01.jpg" alt="金貨">
				<p class="mid_ttl">金貨</p>
				</a> </li>
			<li><a href="<?php echo home_url('/cat/gold/14k'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/02.jpg" alt="14K">
				<p class="mid_ttl">14K</p>
				</a> </li>
			<li><a href="<?php echo home_url('/cat/gold/18k'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/03.jpg" alt="18K">
				<p class="mid_ttl">18K</p>
				</a> </li>
			<li><a href="<?php echo home_url('/cat/gold/22k'); ?>"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/04.jpg" alt="22K">
				<p class="mid_ttl">22K</p>
				</a> </li>
			<li><a href="<?php echo home_url('/cat/gold/24k'); ?>"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/05.jpg" alt="24K">
				<p class="mid_ttl">22K</p>
				</a> </li>
			<li><a href="<?php echo home_url('/cat/gold/ingot'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/06.jpg" alt="インゴット">
				<p class="mid_ttl">インゴット</p>
				</a> </li>
			<li><a href="<?php echo home_url('/cat/gold/white-gold'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/07.jpg" alt="ホワイトゴールド">
				<p class="mid_ttl">ホワイトゴールド</p>
				</a> </li>
			<li><a href="<?php echo home_url('/cat/gold/pink-gold'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/08.jpg" alt="ピンクゴールド">
				<p class="mid_ttl">ピンクゴールド</p>
				</a> </li>
			<li><a href="<?php echo home_url('/cat/gold/yellow-gold'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/09.jpg" alt="イエローゴールド">
				<p class="mid_ttl">イエローゴールド</p>
				</a> </li>
			<li><a href="<?php echo home_url('/cat/gold/platinum'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/10.jpg" alt="プラチナ">
				<p class="mid_ttl">プラチナ</p>
				</a> </li>
			<li><a href="<?php echo home_url('/cat/gold/silver'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/lp/mid/gold/11.jpg" alt="シルバー">
				<p class="mid_ttl">シルバー</p>
				</a> </li>
		</ul>
		<section id="lp-cat-jisseki">
			<h3 class="text-hide">買取実績</h3>
			<ul id="box-jisseki" class="list-unstyled clearfix">
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold01.jpg" alt=""> </p>
						<p class="itemName">金急須</p>
						<hr>
						<p> <span class="red">A社</span>：1,261,000円<br>
							<span class="blue">B社</span>：1,235,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">1,300,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>65,000円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold02.jpg" alt=""> </p>
						<p class="itemName">金プラチナコンビネックレスチェーン</p>
						<hr>
						<p> <span class="red">A社</span>：87,300円<br>
							<span class="blue">B社</span>：85,500円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">90,000円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>4,500円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold03.jpg" alt=""> </p>
						<p class="itemName">金　置物</p>
						<hr>
						<p> <span class="red">A社</span>：717,800円<br>
							<span class="blue">B社</span>：703,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">740,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>37,000円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold04.jpg" alt=""> </p>
						<p class="itemName">破損・ノーブランドの貴金属アクセサリー　金　プラチナ製</p>
						<hr>
						<p> <span class="red">A社</span>：1,406,500円<br>
							<span class="blue">B社</span>：1,377,500円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">1,450,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>72,500円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold05.jpg" alt=""> </p>
						<p class="itemName">プラチナ　プラチナメイプルリーフコイン</p>
						<hr>
						<p> <span class="red">A社</span>：109,610円<br>
							<span class="blue">B社</span>：107,350円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">113,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>5,650円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold06.jpg" alt=""> </p>
						<p class="itemName">ウィーン金貨ハーモニー</p>
						<hr>
						<p> <span class="red">A社</span>：133,860円<br>
							<span class="blue">B社</span>：131,100円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">138,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>6,900円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold07.jpg" alt=""> </p>
						<p class="itemName">金　ネックレス</p>
						<hr>
						<p> <span class="red">A社</span>：504,400円<br>
							<span class="blue">B社</span>：494,000円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">520,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>26,000円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold08.jpg" alt=""> </p>
						<p class="itemName">金　リング</p>
						<hr>
						<p> <span class="red">A社</span>：87,300円<br>
							<span class="blue">B社</span>：85,500円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">90,000<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>4,500円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold09.jpg" alt=""> </p>
						<p class="itemName">ツバルホース金貨1/2oz</p>
						<hr>
						<p> <span class="red">A社</span>：65,766円<br>
							<span class="blue">B社</span>：64,410円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">67,800<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>3,390円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold10.jpg" alt=""> </p>
						<p class="itemName">アメリカンイーグル金貨1oz</p>
						<hr>
						<p> <span class="red">A社</span>：125,832円<br>
							<span class="blue">B社</span>：123,237円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">129,724<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>6,478円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold12.jpg" alt=""> </p>
						<p class="itemName">足金リング</p>
						<hr>
						<p> <span class="red">A社</span>：50,449円<br>
							<span class="blue">B社</span>：49,409円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">52,010<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>2,601円</p>
					</div>
				</li>
				<li class="box-4">
					<div class="title">
						<p class="bx_img"><img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold11.jpg" alt=""> </p>
						<p class="itemName"><a href="/cat/gold/24k">K24</a>　マン島キャット金貨1/5oz</p>
						<hr>
						<p> <span class="red">A社</span>：50,449円<br>
							<span class="blue">B社</span>：49,409円 </p>
					</div>
					<div class="box-jisseki-cat">
						<h3>買取価格例</h3>
						<p class="price">52,010<span class="small">円</span></p>
					</div>
					<div class="sagaku">
						<p><span class="small">買取差額“最大”</span>2,601円</p>
					</div>
				</li>
			</ul>
		</section>
		<section id="list-brand" class="clearfix"> 
			<!--<h3>ブランドリスト</h3>
        <ul class="list-unstyled">
          <li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/ingot'); ?>">
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold05.jpg" alt="インゴット"></dd>
              <dt>インゴット</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/gold-coin'); ?>">
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold11.jpg" alt="金貨"></dd>
              <dt>金貨</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/14k'); ?>">
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold01.jpg" alt="14金"></dd>
              <dt>14金</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/18k'); ?>">
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold02.jpg" alt="18金"></dd>
              <dt>18金</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/22k'); ?>">
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold03.jpg" alt="22金"></dd>
              <dt>22金</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/24k'); ?>">
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold04.jpg" alt="24金"></dd>
              <dt>24金</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/white-gold'); ?>">
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold06.jpg" alt="ホワイトゴールド"></dd>
              <dt>ホワイトゴールド</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/pink-gold'); ?>">
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold07.jpg" alt="ピンクゴールド"></dd>
              <dt>ピンクゴールド</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/yellow-gold'); ?>">
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold08.jpg" alt="イエローゴールド"></dd>
              <dt>イエローゴールド</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/platinum'); ?>">
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold09.jpg" alt="プラチナ"></dd>
              <dt>プラチナ</dt>
              </a>
            </dl>
          </li>
<li>
            <dl>
            <a href="<?php echo home_url('/cat/gold/silver'); ?>">
              <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/link_cat/gold/gold10.jpg" alt="シルバー"></dd>
              <dt>シルバー</dt>
              </a>
            </dl>
          </li>

          
                    
         
        </ul>--> 
		</section>
		<section id="lp-cat-jisseki" class="clearfix">
			<div id="konna" class="konna_gold">
				<p class="example1">■海外物、刻印無し</p>
				<p class="example2">■金　コイン</p>
				<p class="example3">■刻印無しインゴット</p>
				<p class="text">その他：金具破損、眼鏡、台だけの指輪等でもお買取りいたします。</p>
			</div>
		</section>
		<section id="about_kaitori" class="clearfix">
			<?php
        // 買取について
        get_template_part('_widerange');
				get_template_part('_widerange2');
      ?>
		</section>
		<!--<section id="list-brand" class="clearfix">
                <h3>ブランドリスト</h3>
                <ul class="list-unstyled">
                    <li>
                        <dl>
                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/shoes/t-bagcat1.jpg" alt="エルメス"></dd>
                            <dt>エルメス</dt>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/shoes/t-bagcat3.jpg" alt="ルイヴィトン"></dd>
                            <dt>ルイヴィトン</dt>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/shoes/t-bagcat5.jpg" alt="プラダ"></dd>
                            <dt>プラダ</dt>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/shoes/t-bagcat7.jpg" alt="シャネル"></dd>
                            <dt>シャネル</dt>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/shoes/t-bagcat9.jpg" alt="エルメス"></dd>
                            <dt>グッチ</dt>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dd><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/shoes/t-bagcat14.jpg" alt="イヴ・サンローラン"></dd>
                            <dt>イヴ・サンローラン</dt>
                        </dl>
                    </li>
                </ul>
            </section>-->
		<section> <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/bn_matomegai.png"> </section>
		<?php
        // 買取基準
        get_template_part('_criterion');

        // NGアイテム
        get_template_part('_no_criterion');
        
				// カテゴリーリンク
        get_template_part('_category');
			?>
		<section id="kaitori_genre">
			<h3 class="text-hide">その他買取可能なジャンル</h3>
			<p>【買取ジャンル】バッグ/ウエストポーチ/セカンドバック/トートバッグ/ビジネスバッグ/ボストンバッグ/クラッチバッグ/トランクケース/ショルダーバッグ/ポーチ/財布/カードケース/パスケース/キーケース/手帳/腕時計/ミュール/サンダル/ビジネスシューズ/パンプス/ブーツ/ペアリング/リング/ネックレス/ペンダント/ピアス/イアリング/ブローチ/ブレスレット/ライター/手袋/傘/ベルト/ペン/リストバンド/アンクレット/アクセサリー/サングラス/帽子/マフラー/ハンカチ/ネクタイ/ストール/スカーフ/バングル/カットソー/アンサンブル/ジャケット/コート/ブルゾン/ワンピース/ニット/シャツメンズ/毛皮/Tシャツ/キャミソール/タンクトップ/パーカー/ベスト/ポロシャツ/ジーンズ/スカート/スーツなど</p>
		</section>
		<?php
        // 買取方法
        get_template_part('_purchase');
      ?>
		<section id="user_voice">
			<h3>ご利用いただいたお客様の声</h3>
			<p class="user_voice_text1">ちょうど家の整理をしていたところ、家のポストにチラシが入っていたので、ブランドリバリューに電話してみました。今まで買取店を利用したことがなく、不安でしたがとても丁寧な電話の対応とブランドリバリューの豊富な取り扱い品目を魅力に感じ、出張買取を依頼することにしました。 絶対に売れないだろうと思った、動かない時計や古くて痛んだバッグ、壊れてしまった貴金属のアクセサリーなども高額で買い取っていただいて、とても満足しています。古紙幣や絵画、食器なども買い取っていただけるとのことでしたので、また家を整理するときにまとめて見てもらおうと思います。 </p>
			<h4>鑑定士からのコメント</h4>
			<div class="clearfix">
				<p class="user_voice_text2">家の整理をしているが、何を買い取ってもらえるか分からないから一回見に来て欲しいとのことでご連絡いただきました。 買取店が初めてで不安だったが、丁寧な対応に非常に安心しましたと笑顔でおっしゃって頂いたことを嬉しく思います。 物置にずっとしまっていた時計や、古いカバン、壊れてしまったアクセサリーなどもしっかりと価値を見極めて高額で提示させて頂きましたところ、お客様もこんなに高く買取してもらえるのかと驚いておりました。 これからも家の不用品を整理するときや物の価値判断ができないときは、すぐにお電話しますとおっしゃって頂きました。 </p>
				<img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/photo-user-voice.jpg" class="right"> </div>
		</section>
		<section>
			<div class="cont_widerange">
				<h4>知っておこう！金・プラチナの種類と価格の違い<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
				<ul>
					<li>貴金属といえば、金・プラチナ・シルバーなどが思い浮かびますが、近年の貴金属相場の値上がりで金・プラチナの売却を考える人も多くなってきましたが、「本当にきちんと査定してもらえるのか」と金・プラチナ買取をしてもらうことに不安を感じている人もいるかもしれません。<br>
						そんな時は金・プラチナの種類や価格の違いを少し把握しておきましょう。金・プラチナの種類を知っておくだけで、査定が適正にされているか見極めることができるので買取をしてもらう基準を判断することができます。<br>
						金は純度によって、その価格に違いがあります。24金が金の純度100％と価格が最も高く、22金、18金、14金と純度ランクが下がってきます。18金が最もポピュラーな純度で扱いやすいとされ、アクセサリーによく用いられています。また14金は金の純度が60％と低く最も安い価格で取引されることになります。<br>
						また、カラーゴールドと呼ばれる種類によって分けられます。今人気のホワイトゴールド、その上品な人気のピンクゴールド、そして金本来のイエローゴールドがあります。なかでもホワイトゴールドはイエローゴールドに稀少性の高いパラジウムを混ぜることによりイエロー色を抑えたものですが、パラジウムの稀少性によってイエローゴールドよりも高価となっています。ピンクゴールドはイエローゴールドに銅をメインに他の種類の地金を混ぜたものになります。その優しく上品なカラーが最大の魅力で、最近ではイエローゴールドよりもピンクゴールドが使われる時計やジュエリーも多くなってきています。<br>
						一方、プラチナは純度によって価格に違いがあります。プラチナの純度が高い順にPt1000、Pt950、Pt900、Pt850とランク分けされおり、純度が刻印されていうこともあります。プラチナは大変柔らかい金属であるため、アクセサリーなどには他の金属と混ぜたプラチナ純度90％のPt900がよく使われているようです。もちろん純度の高いほど価格も高くなるなど、価格の違いはプラチナの中にもあります。<br>
						<br>
					</li>
				</ul>
			</div>
			<div class="cont_widerange">
				<h4>金・プラチナの高額査定してもらうポイント<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
				<ul>
					<li>最近、高額査定や高額買取となることから何かと注目される金・プラチナなどの貴金属を売ろうと思っている人は多いでしょう。手元に眠っている金・プラチナのアクセサリーやジュエリーがあるのなら、ぜひ金・プラチアの買取をしている買取専門店に査定してもらってはいかがでしょうか？<br>
						せっかく売却するなら少しでも高額査定で高く買取ってもらいたいと誰もが思うところでしょう。金・プラチナ、シルバーなど各貴金属の相場の値上がりによって、思った以上に高額査定になる場合もあります。実は、高額査定してもらうためのちょっとしたポイントがあるのでご紹介していきましょう。<br>
						まず手元にある金・プラチナなどのアクセサリーやジュエリーを購入した時に発行された鑑定書や保証書などがあれば、必ず揃えて査定に出す時に一緒に出しましょう。それだけでも一定の高額査定が可能になります。<br>
						また金・プラチナなどの貴金属は、取引価格の基準となる相場（レート）があることを把握しておきましょう。日本で取引される金・プラチナの相場は田中貴金属が発表しているレートが基準となっています。このレートは、田中貴金属の公式サイトに掲載されているので、金・プラチナを扱う業者だけでなく一般の人も簡単に見ることができます。<br>
						レートは他の為替と同じように毎日変動しているため、買取価格も毎日変動しています。そのためレートが値上がった日に金・プラチナの買取店で査定してもらうと必然的に高額査定となり、逆にレートが値下がった時に査定してもらうと高額査定にはなりません。<br>
						ただし各買取店によって買取価格を設定するため、実際の買取価格は買取店によって異なってきます。たとえば、ある買取店ではレートの90％を買取価格に設定していても、他の買取店では85％を買取価格にしている場合もあります。そのため、金・プラチナの高額査定をしてもらうためにはレートに対して高い割合（％）の買取価格を設定している買取店を選ぶようにしましょう。ただし、様々な理由で手数料を取ろうとする買取店は買取金額が低くなってしまうので注意が必要です。<br>
						<br>
					</li>
				</ul>
			</div>
			<div class="cont_widerange">
				<h4>金・プラチナの買取可能な状態は？<img src="http://brand.kaitorisatei.info/wp-content/uploads/2016/11/brandkaitori_ttl_00-1.png" alt=""></h4>
				<ul>
					<li>家に眠っている金・プラチナのアクセサリーやジュエリーを買取ってもらおうと思っている人の中には、金・プラチナのどんな状態のものが買取ってもらえるのか不安に思っている人もいるかもしれません。買取と言うからには、傷やダメージのない金・プラチナのジュエリーでなくてはいけないのかと疑問に感じたり、いま売ろうとしている金・プラチナは買取不可として突き返されてしまったり、ダメージが大きいと買取価格を大幅に安く叩かれてしまうかもしれないと思うと不安になりますよね。<br>
						そこで手元にある金・プラチナを査定、買取に出す前に、どのような金・プラチナ状態が買取可能な状態なのかしっかり把握しておけば、事前に買取不可となるアクセサリーを査定に出してガッカリすることありません。さらに安く買取ろうとする買取店をしっかりと見極めることもできるため、買取可能な状態の基準はしっかり知っておく必要があるのです。せっかく貴金属の中でも高い価値のある金・プラチナなのですから、貴金属製品を取扱った経験豊富な鑑定士がいる良心的な買取店でその価値の分をしっかりと高額買取で金額に反映してもらうようにしましょう。<br>
						基本的に買取可能な状態の金・プラチナは、その材質でできているものであればジュエリーやアクセサリーに関わらず形は問題になりません。そのため、金やプラチナのインゴットをはじめ金貨、ベルトバックル、置物、金製品、金やプラチナでできた工芸品、なかには金歯に至るまで、刻印がなくてもその材質に問題がなければ買取可能な状態と言えます。そのため、古いデザインのジュエリーやちぎれたネックレスやブレスレット、壊れていたり片方しかないピアスやイヤリング、宝石が取れて金やプラチナ台だけになっている指輪などジュエリー全般でも全く問題がありません。<br>
						買取価格は通常1g単位で設定されているため、売却したい重量が多ければ多いほど買取総額は高くなるので思った以上に高額買い取りになる場合もあります。ぜひこれはと思う金製品、プラチナ製品があればまずは買取可能な状態かを確認して査定依頼をしてみましょう。 </li>
				</ul>
			</div>
		</section>
	</main>
	<!-- #main --> 
</div>
<!-- #primary -->

<?php
get_sidebar();
get_footer();





