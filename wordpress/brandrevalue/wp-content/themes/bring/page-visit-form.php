<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */
   // 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  'bag009.png::HERMES::エブリン3PM::184,300::180,500::190,000::9,500',
  'bag010.png::PRADA::ハンドバッグ::116,400::114,000::120,000::6,000',
  'bag011.png::GUCCI::バンブーバッグ::29,100::28,500::30,000::30,000',
  'bag012.png::LOUIS VUITTON::モンスリGM::58,200::57,000::60,000::3,000',
  'bag001.jpg::HERMES::バーキン30::1,450,000::1,520,000::1,600,000::150,000',
  'bag002.jpg::CHANEL::マトラッセ チェーンバッグ::100,000::130,000::150,000::50,000',
  'bag003.jpg::LOUIS VUITTON::ダミエ ネヴァーフルMM::92,000::104,000::110,000::18,000',
  'bag004.jpg::CELINE::ラゲージマイクロショッパーバッグ::160,000::175,000::1850,000::25,000',
);

// 買取実績リスト
$resultLists = array(
//'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
  'watch/top/001.jpg::パテックフィリップ::カラトラバ 3923::646,000::640,000::650,000::10,000',
  'watch/top/002.jpg::パテックフィリップ::アクアノート 5065-1A::1,367,000::1,350,000::1,400,000::50,000',
  'watch/top/003.jpg::オーデマピゲ::ロイヤルオーク・オフショア 26170ST::1,200,000::1,195,000::1,210,000::15,000',
  'watch/top/004.jpg::ROLEX::サブマリーナ 116610LN ランダム品番::720,000::700,000::740,000::40,000',
  'watch/top/005.jpg::ROLEX::デイトナ 116505 ランダム品番 コスモグラフ::1,960,000::1,950,000::2,000,000::50,000',
  'watch/top/006.jpg::ブライトリング::クロノマット44::334,000::328,000::340,000::12,000',
  'watch/top/007.jpg::パネライ::ラジオミール 1940::594,000::590,000::600,000::10,000',
  'watch/top/008.jpg::ボールウォッチ::ストークマン ストームチェイサープロ CM3090C::128,000::125,000::130,000::5,000',
  'watch/top/009.jpg::ブレゲ::クラシックツインバレル 5907BB12984::615,000::610,000::626,000::16,000',
  'watch/top/010.jpg::ウブロ::ビッグバン ブラックマジック::720,000::695,000::740,000::45,000',
  'watch/top/011.jpg::オメガ::シーマスター プラネットオーシャン 2208-50::192,000::190,000::200,000::10,000',
  'watch/top/012.jpg::ディオール::シフルルージュ クロノグラフ CD084612 M001::190,000::180,000::200,000::20,000',
);


get_header(); ?>

<div id="primary" class="about-purchase content-area">
	<main id="main" class="site-main" role="main">
		<section> <img src="<?php echo get_s3_template_directory_uri() ?>/img/about-purchase/visit/mv.png">
		</section>

		<section id="visit_box">
			<h3 class="ttl01">待ち時間短縮、査定基準は全国一律！</h3>
				<div><p>ご予約のお客様を優先的にご案内！混みあってしまった場合でも、お待ち頂かずにご案内可能です。<br />また、査定基準の統一により「場所によって金額に差があるのでは…」「〇〇の店舗にしか行けない…」等といったお客様の不安を解消いたします。</p>
			</div>
		</section>
		<section id="visit_box">
			<h3 class="ttl02">予約で金額アップ！</h3>
			<div>
				<p>ご来店予約でさらに査定金額アップが可能となります。ご成約の場合には、さらにタクシー代の負担もさせていただきます。<br />
※一部対象外エリアがございます。ご希望のお客様は、事前にカスタマーサポートセンターまでお問合せください。
</p>
			</div>
		</section>
		
		<section id="visit_box">
			<h3 class="ttl03">手ぶらでもOK！</h3>
			<div>
				<p>「大きなお荷物、たくさんの商品を売りたいけど持って行くのが大変…」<br />
「しっかり対面で説明を受けたい」そんなお客様にお勧めさせて頂くサービスです。<br />
お買取り希望のお品物を先に店頭へ宅配して頂いた後、お客様は手ぶらでご来店頂きます。<br />
来店されましたら、当店査定士がお買取りをさせて頂きます。<br />
宅配用キットは弊社でご用意させて頂きますので、ご安心下さいませ。

</p>
			</div>
		</section>

		<section>
			<h2 class="obi_tl mgnone">来店お申込みフォーム</h2>
			<div class="mailform_new02"> <?php echo do_shortcode('[mwform_formkey key="23535"]'); ?> </div>
		</section>
		<?php
        // アクションポイント
        get_template_part('_action');
		?>
		<section id="top-jisseki">
			<h2 class="text-hide">買取実績</h2>
			<div class="full_content"> 
				
				<!-- ブランド買取 -->
				<div class="menu hover"><span class="glyphicon glyphicon-tag"></span> ブランド買取</div>
				<div class="content"> 
					<!-- box -->
					<ul id="box-jisseki" class="list-unstyled clearfix">
						<!-- brand1 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/001.jpg" alt="">
								<p class="itemName">オーデマピゲ　ロイヤルオークオフショアクロノ 26470OR.OO.A002CR.01 ゴールド K18PG</p>
								<hr>
								<p> <span class="red">A社</span>：3,120,000円<br>
									<span class="blue">B社</span>：3,100,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">3,150,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>50,000円</p>
							</div>
						</li>
						
						<!-- brand2 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/002.jpg" alt="">
								<p class="itemName">パテックフィリップ　コンプリケーテッド ウォッチ 5085/1A-001</p>
								<hr>
								<p> <span class="red">A社</span>：1,400,000円<br>
									<span class="blue">B社</span>：1,390,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">1,420,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>30,000円</p>
							</div>
						</li>
						
						<!-- brand3 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/003.jpg" alt="">
								<p class="itemName">パテックフィリップ　コンプリケーション 5130G-001 WG</p>
								<hr>
								<p> <span class="red">A社</span>：2,970,000円<br>
									<span class="blue">B社</span>：2,950,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">3,000,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>50,000円</p>
							</div>
						</li>
						
						<!-- brand4 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/004.jpg" alt="">
								<p class="itemName">パテックフィリップ　ワールドタイム 5130R-001</p>
								<hr>
								<p> <span class="red">A社</span>：3,000,000円<br>
									<span class="blue">B社</span>：2,980,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">3,050,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>70,000円</p>
							</div>
						</li>
						
						<!-- brand5 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/005.jpg" alt="">
								<p class="itemName">シャネル　ラムスキン　マトラッセ　二つ折り長財布</p>
								<hr>
								<p> <span class="red">A社</span>：69,000円<br>
									<span class="blue">B社</span>：68,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">70,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,000円</p>
							</div>
						</li>
						
						<!-- brand6 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/006.jpg" alt="">
								<p class="itemName">エルメス　ベアンスフレ　ブラック</p>
								<hr>
								<p> <span class="red">A社</span>：200,000円<br>
									<span class="blue">B社</span>：196,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">211,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>15,000円</p>
							</div>
						</li>
						
						<!-- brand7 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/007.jpg" alt="">
								<p class="itemName">エルメス　バーキン30　トリヨンクレマンス　マラカイト　SV金具</p>
								<hr>
								<p> <span class="red">A社</span>：1,220,000円<br>
									<span class="blue">B社</span>：1,200,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">1,240,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>40,000円</p>
							</div>
						</li>
						
						<!-- brand8 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/008.jpg" alt="">
								<p class="itemName">セリーヌ　ラゲージマイクロショッパー</p>
								<hr>
								<p> <span class="red">A社</span>：150,000円<br>
									<span class="blue">B社</span>：147,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">155,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>8,000円</p>
							</div>
						</li>
						
						<!--brand9 -->
						
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/009.jpg" alt="">
								<p class="itemName">ルイヴィトン　裏地ダミエ柄マッキントッシュジャケット</p>
								<hr>
								<p> <span class="red">A社</span>：31,000円<br>
									<span class="blue">B社</span>：30,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">32,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,000円</p>
							</div>
						</li>
						
						<!-- brand10 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/010.jpg" alt="">
								<p class="itemName">シャネル　ココマーク　ラパンファーマフラー</p>
								<hr>
								<p> <span class="red">A社</span>：68,000円<br>
									<span class="blue">B社</span>：65,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">71,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>6,000円</p>
							</div>
						</li>
						
						<!-- brand11 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/011.jpg" alt="">
								<p class="itemName">ルイヴィトン　ダミエ柄サイドジップレザーブーツ</p>
								<hr>
								<p> <span class="red">A社</span>：49,000円<br>
									<span class="blue">B社</span>：48,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">51,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>3,000円</p>
							</div>
						</li>
						
						<!-- brand12 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/012.jpg" alt="">
								<p class="itemName">サルバトーレフェラガモ　ヴェラリボンパンプス</p>
								<hr>
								<p> <span class="red">A社</span>：9,000円<br>
									<span class="blue">B社</span>：8,500円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">10,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,500円</p>
							</div>
						</li>
					</ul>
				</div>
				
				<!-- 金買取 -->
				<div class="menu"><span class="glyphicon glyphicon-bookmark"></span> 金買取</div>
				<div class="content"> 
					<!-- box -->
					<ul id="box-jisseki" class="list-unstyled clearfix">
						<!-- 1 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/001.jpg" alt="">
								<p class="itemName">K18　ダイヤ0.11ctリング</p>
								<hr>
								<p> <span class="red">A社</span>：23,700円<br>
									<span class="blue">B社</span>：23,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price"><!-- span class="small" >120g</span -->25,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,000円</p>
							</div>
						</li>
						<!-- 2 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/002.jpg" alt="">
								<p class="itemName">K18　メレダイヤリング</p>
								<hr>
								<p> <span class="red">A社</span>：38,000円<br>
									<span class="blue">B社</span>：37,500円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price"><!-- span class="small" >120g</span -->39,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,500円</p>
							</div>
						</li>
						<!-- 3 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/003.jpg" alt="">
								<p class="itemName">K18/Pt900　メレダイアリング</p>
								<hr>
								<p> <span class="red">A社</span>：14,200円<br>
									<span class="blue">B社</span>：14,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">16,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,000円</p>
							</div>
						</li>
						<!-- 4 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/004.jpg" alt="">
								<p class="itemName">Pt900　メレダイヤリング</p>
								<hr>
								<p> <span class="red">A社</span>：22,600円<br>
									<span class="blue">B社</span>：21,200円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">23,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,800円</p>
							</div>
						</li>
						<!-- 5 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/005.jpg" alt="">
								<p class="itemName">K18WG　テニスブレスレット</p>
								<hr>
								<p> <span class="red">A社</span>：24,600円<br>
									<span class="blue">B社</span>：23,800円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">26,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,200円</p>
							</div>
						</li>
						<!-- 6 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/006.jpg" alt="">
								<p class="itemName">K18/K18WG　メレダイヤリング</p>
								<hr>
								<p> <span class="red">A社</span>：22,400円<br>
									<span class="blue">B社</span>：22,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">23,600<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,600円</p>
							</div>
						</li>
						<!-- 7 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/007.jpg" alt="">
								<p class="itemName">K18ブレスレット</p>
								<hr>
								<p> <span class="red">A社</span>：18,100円<br>
									<span class="blue">B社</span>：17,960円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">20,100<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,140円</p>
							</div>
						</li>
						<!-- 8 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/008.jpg" alt="">
								<p class="itemName">K14WGブレスレット</p>
								<hr>
								<p> <span class="red">A社</span>：23,600円<br>
									<span class="blue">B社</span>：22,800円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">24,300<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,500円</p>
							</div>
						</li>
						<!-- 9 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/009.jpg" alt="">
								<p class="itemName">Pt850ブレスレット</p>
								<hr>
								<p> <span class="red">A社</span>：23,600円<br>
									<span class="blue">B社</span>：23,200円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">24,700<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,500円</p>
							</div>
						</li>
						<!-- 10 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/010.jpg" alt="">
								<p class="itemName">Pt900/Pt850メレダイヤネックレス</p>
								<hr>
								<p> <span class="red">A社</span>：29,600円<br>
									<span class="blue">B社</span>：28,400円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">30,500<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,100円</p>
							</div>
						</li>
						<!-- 11 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/011.jpg" alt="">
								<p class="itemName">K18/Pt900/K24ネックレストップ</p>
								<hr>
								<p> <span class="red">A社</span>：43,600円<br>
									<span class="blue">B社</span>：43,100円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">45,900<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,800円</p>
							</div>
						</li>
						<!-- 12 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/012.jpg" alt="">
								<p class="itemName">K24インゴットメレダイヤネックレストップ</p>
								<hr>
								<p> <span class="red">A社</span>：53,700円<br>
									<span class="blue">B社</span>：52,900円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">56,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>3,100円</p>
							</div>
						</li>
					</ul>
				</div>
				
				<!-- 宝石買取 -->
				<div class="menu"><span class="glyphicon glyphicon-magnet"></span> 宝石買取</div>
				<div class="content"> 
					<!-- box -->
					<ul id="box-jisseki" class="list-unstyled clearfix">
						<!--<li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem001.jpg" alt="">
            <p class="itemName">エメラルド</p>
            <hr>
            <p> <span class="red">A社</span>：190,000円<br>
              <span class="blue">B社</span>：194,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>200,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>7,500円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem002.jpg" alt="">
            <p class="itemName">サファイア</p>
            <hr>
            <p> <span class="red">A社</span>：145,500円<br>
              <span class="blue">B社</span>：142,500円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>150,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>10,000円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem003.jpg" alt="">
            <p class="itemName">ルビー</p>
            <hr>
            <p> <span class="red">A社</span>：97,000円<br>
              <span class="blue">B社</span>：95,000円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>100,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>5,000円</p>
          </div>
        </li>
        <li class="box-4">
          <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem004.jpg" alt="">
            <p class="itemName">アレキサンドライト</p>
            <hr>
            <p> <span class="red">A社</span>：67,900円<br>
              <span class="blue">B社</span>：66,500円 </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price"><span class="small">地金＋</span>70,000<span class="small">円</span></p>
          </div>
          <div class="sagaku">
            <p><span class="small">買取差額“最大”</span>4,500円</p>
          </div>
        </li>--> 
						
						<!-- 1 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/001.jpg" alt="">
								<p class="itemName">ダイヤルース</p>
								<p class="itemdetail">カラット：1.003ct<br>
									カラー：G<br>
									クラリティ：VS-2<br>
									カット：Good<br>
									蛍光性：FAINT<br>
									形状：ラウンドブリリアント</p>
								<hr>
								<p> <span class="red">A社</span>：215,000円<br>
									<span class="blue">B社</span>：210,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price"><!-- span class="small">地金＋</span -->221,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>11,000円</p>
							</div>
						</li>
						
						<!-- 2 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/002.jpg" alt="">
								<p class="itemName">Pt900 DR0.417ctリング</p>
								<p class="itemdetail"> カラー：F<br>
									クラリティ：SI-1<br>
									カット：VERY GOOD <br>
									蛍光性：FAINT<br>
									形状：ラウンドブリリアント</p>
								<hr>
								<p> <span class="red">A社</span>：42,000円<br>
									<span class="blue">B社</span>：41,300円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">44,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>2,700円</p>
							</div>
						</li>
						
						<!-- 3 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/003.jpg" alt="">
								<p class="itemName">Pt900　DR0.25ctリング</p>
								<p class="itemdetail"> カラー：H<br>
									クラリティ:VS-1<br>
									カット：Good</br>
									蛍光性：MB<br>
									形状：ラウンドブリリアント<br>
								
								<hr>
								<p> <span class="red">A社</span>：67,400円<br>
									<span class="blue">B社</span>：67,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">68,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,000円</p>
							</div>
						</li>
						
						<!-- 4 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/004.jpg" alt="">
								<p class="itemName">K18　DR0.43ct　MD0.4ctネックレストップ</p>
								<p class="itemdetail"> カラー：I<br>
									クラリティ：VS-2<br>
									カット：Good<br>
									蛍光性：WB<br>
									形状：ラウンドブリリアント</p>
								<hr>
								<p> <span class="red">A社</span>：50,000円<br>
									<span class="blue">B社</span>：49,500円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">51,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>1,500円</p>
							</div>
						</li>
						
						<!-- 5 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/005.jpg" alt="">
								<p class="itemName">ダイヤルース</p>
								<p class="itemdetail">カラット：0.787ct<br>
									カラー：E<br>
									クラアリティ：VVS-2<br>
									カット：Good<br>
									蛍光性：FAINT<br>
									形状：ラウンドブリリアント</p>
								<hr>
								<p> <span class="red">A社</span>：250,000円<br>
									<span class="blue">B社</span>：248,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">257,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>9,000円</p>
							</div>
						</li>
						
						<!-- 6 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/006.jpg" alt="">
								<p class="itemName">Pt950　MD0.326ct　0.203ct　0.150ctネックレス</p>
								<p class="itemdetail"> カラー：F<br>
									クラリティ：SI-2<br>
									カット：Good<br>
									蛍光性：FAINT<br>
									形状：ラウンドブリリアント</p>
								<hr>
								<p> <span class="red">A社</span>：55,000円<br>
									<span class="blue">B社</span>：54,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">57,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>3,000円</p>
							</div>
						</li>
						
						<!-- 7 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/007.jpg" alt="">
								<p class="itemName">ダイヤルース</p>
								<p class="itemdetail">カラット：1.199ct<br>
									カラー：K<br>
									クラリティ：SI-2<br>
									カット：評価無<br>
									蛍光性：NONE<br>
									形状：パビリオン<br>
								</p>
								<hr>
								<p> <span class="red">A社</span>：58,000円<br>
									<span class="blue">B社</span>：56,800円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">60,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>3,200円</p>
							</div>
						</li>
						
						<!-- 8 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/008.jpg" alt="">
								<p class="itemName">ダイヤルース</p>
								<p class="itemdetail">カラット：8.1957ct<br>
									カラー：LIGTH YELLOW<br>
									クラリティ：VS-1<br>
									カット：VERY GOOD<br>
									蛍光性：FAINT<br>
									形状：ラウンドブリリアント</p>
								<hr>
								<p> <span class="red">A社</span>：6,540,000円<br>
									<span class="blue">B社</span>：6,500,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">6,680,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>180,000円</p>
							</div>
						</li>
					</ul>
				</div>
				<div class="menu"><span class="glyphicon glyphicon-time"></span> 時計買取</div>
				<div class="content">
					<ul id="box-jisseki" class="list-unstyled clearfix">
						<?php
                    foreach($resultLists as $list):
                    // :: で分割
                    $listItem = explode('::', $list);
                  
                  ?>
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/<?php echo $listItem[0]; ?>" alt="">
								<p class="itemName"><?php echo $listItem[1]; ?><br>
									<?php echo $listItem[2]; ?></p>
								<hr>
								<p> <span class="red">A社</span>：<?php echo $listItem[3]; ?>円<br>
									<span class="blue">B社</span>：<?php echo $listItem[4]; ?>円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price"><?php echo $listItem[5]; ?><span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span><?php echo $listItem[6]; ?>円</p>
							</div>
						</li>
						<?php endforeach; ?>
					</ul>
				</div>
				
				<!-- バッグ買取 -->
				<div class="menu"><span class="glyphicon glyphicon-briefcase"></span> バッグ買取</div>
				<div class="content">
					<ul id="box-jisseki" class="list-unstyled clearfix">
						<!-- 1 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/001.jpg" alt="">
								<p class="itemName">エルメス　エブリンⅢ　トリヨンクレマンス</p>
								<hr>
								<p> <span class="red">A社</span>：176,000円<br>
									<span class="blue">B社</span>：172,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">180,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>8,000円</p>
							</div>
						</li>
						<!-- 2 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/002.jpg" alt="">
								<p class="itemName">プラダ　シティトート2WAYバッグ</p>
								<hr>
								<p> <span class="red">A社</span>：128,000円<br>
									<span class="blue">B社</span>：125,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">132,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>7,000円</p>
							</div>
						</li>
						<!-- 3 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/003.jpg" alt="">
								<p class="itemName">バンブーデイリー2WAYバッグ</p>
								<hr>
								<p> <span class="red">A社</span>：83,000円<br>
									<span class="blue">B社</span>：82,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">85,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>3,000円</p>
							</div>
						</li>
						<!-- 4 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/004.jpg" alt="">
								<p class="itemName">ルイヴィトン　モノグラムモンスリGM</p>
								<hr>
								<p> <span class="red">A社</span>：64,000円<br>
									<span class="blue">B社</span>：63,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">66,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>3,000円</p>
							</div>
						</li>
						<!-- 5 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/005.jpg" alt="">
								<p class="itemName">エルメス　バーキン30</p>
								<hr>
								<p> <span class="red">A社</span>：1,150,000円<br>
									<span class="blue">B社</span>：1,130,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">1,200,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>70,000円</p>
							</div>
						</li>
						<!-- 6 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/006.jpg" alt="">
								<p class="itemName">シャネル　マトラッセダブルフラップダブルチェーンショルダーバッグ</p>
								<hr>
								<p> <span class="red">A社</span>：252,000円<br>
									<span class="blue">B社</span>：248,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">260,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>12,000円</p>
							</div>
						</li>
						<!-- 7 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/007.jpg" alt="">
								<p class="itemName">ルイヴィトン　ダミエネヴァーフルMM</p>
								<hr>
								<p> <span class="red">A社</span>：96,000円<br>
									<span class="blue">B社</span>：94,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">98,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>4,000円</p>
							</div>
						</li>
						<!-- 8 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/008.jpg" alt="">
								<p class="itemName">セリーヌ　ラゲージマイクロショッパー</p>
								<hr>
								<p> <span class="red">A社</span>：150,000円<br>
									<span class="blue">B社</span>：148,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">155,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>7,000円</p>
							</div>
						</li>
						<!-- 9 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/009.jpg" alt="">
								<p class="itemName">ロエベ　アマソナ23</p>
								<hr>
								<p> <span class="red">A社</span>：86,000円<br>
									<span class="blue">B社</span>：82,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">90,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>8,000円</p>
							</div>
						</li>
						<!-- 10 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/010.jpg" alt="">
								<p class="itemName">グッチ　グッチシマ　ビジネスバッグ</p>
								<hr>
								<p> <span class="red">A社</span>：50,000円<br>
									<span class="blue">B社</span>：48,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">53,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>5,000円</p>
							</div>
						</li>
						<!-- 11 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/011.jpg" alt="">
								<p class="itemName">プラダ　サフィアーノ2WAYショルダーバッグ</p>
								<hr>
								<p> <span class="red">A社</span>：115,000円<br>
									<span class="blue">B社</span>：110,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">125,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>15,000円</p>
							</div>
						</li>
						<!-- 12 -->
						<li class="box-4">
							<div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/012.jpg" alt="">
								<p class="itemName">ボッテガヴェネタ　カバMM　ポーチ付き</p>
								<hr>
								<p> <span class="red">A社</span>：150,000円<br>
									<span class="blue">B社</span>：146,000円 </p>
							</div>
							<div class="box-jisseki-cat">
								<h3>買取価格例</h3>
								<p class="price">155,000<span class="small">円</span></p>
							</div>
							<div class="sagaku">
								<p><span class="small">買取差額“最大”</span>9,000円</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</section>
		<section> <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/bn_matomegai.png"> </section>
		<section class="kaitori_voice">
			<h3>お客様の声</h3>
			<ul>
				<li>
					<p class="kaitori_tab tentou_tab">店頭買取</p>
					<h4>パネライ　時計　</h4>
					<p class="voice_txt">新しい時計が気になり、今まで愛用していたパネライの腕時計を買取してらもらうことにしました。<br />
						手元に保管しておくという手もあったのですが、やはり使わずに時計を手元に置いておくよりは、買取資金の足しにした方がいいのかなと思ったので・・・。<br />
						通勤するときに、ブランドリバリューの近くを通るので、通勤ついでにブランドリバリューで試しで査定をしてもらうことにしたのですが、本当にこちらを利用してよかったです。<br />
						私もブランドに詳しい自信がありましたが、査定士さんはそれ以上の知識があり、きちんとブランドの価値をわかってくださいました。本当に買取額に対して満足することができただけではなく、安心して買取手続きをすることができました。<br />
						やはり買取査定はブランドリバリューのように、きちんとブランド知識があるところでお願いしないと満足できないですよね。</p>
				</li>
				<li>
					<p class="kaitori_tab tentou_tab">店頭買取</p>
					<h4>シャネル　バッグ　</h4>
					<p class="voice_txt">シャネルのバッグの査定をしてもらいました。<br />
						店頭買取をしてもらったのですが、実はブランドリバリューに出向く前に他社比較もしようと思い、他の買取店でも査定をしてもらっていたんです。
						ですが、他社比較の結果、断然！ブランドリバリューは高値の査定を出してくれました！<br />
						ブランドリバリューは、きちんと店頭買取をすることができる、いわば実際の店舗が存在するブランド買取店でもあるので、この安心さも兼ねそろえているのが個人的に安心でした。<br />
						また、他にもブランドバッグで買取をしてほしいものが出てきたら、絶対にブランドリバリューを利用するつもりです。
						もう他社比較はしません！笑</p>
				</li>
				<li>
					<p class="kaitori_tab tentou_tab">店頭買取</p>
					<h4>ボッテガヴェネタ　財布</h4>
					<p class="voice_txt">好みのブランドから、新作のお財布が販売されたので、今使っているボッテガヴェネタの財布を買取してもらおうと思い、ブランドリバリューを利用しました。<br />
						店舗の位置をチェックしてみると、銀座駅からもめっちゃ近かったので、店頭買取を利用しての買取です。<br />
						今回は、ボッテガヴェネタの財布１つだけの買取だったので、このお財布１つだけで、買取査定を依頼するのは何だか申し訳ないなと思ったりもしたのですが、担当の方もとても丁寧で気持ちのいい対応をとってくださったので、本当によかったです。<br />
						査定額も満足することができたので、機会があればまた利用したいと思っています。</p>
				</li>
			</ul>
		</section>
		<?php
        
        // 買取方法
        get_template_part('_purchase');
        
        // 店舗案内
        get_template_part('_shopinfo');
      ?>
	</main>
	<!-- #main --> 
</div>
<!-- #primary -->

<script src="http://code.jquery.com/jquery-3.3.1.slim.min.js"></script> 
<script type="text/javascript">
(function () {

    "use strict";

    /**
     * Pardot form handler assist script.
     * Copyright (c) 2018 toBe marketing, inc.
     * Released under the MIT license
     * http://opensource.org/licenses/mit-license.php
     */

    // == 設定項目ここから ==================================================================== //

    /*
     * ＠データ取得方式
     */
    var useGetType = 1;

    /*
     * ＠フォームハンドラーエンドポイントURL
     */
    var pardotFHUrl="//go.staygold.shop/l/436102/2018-05-02/f3jb4y";

    /*
     * ＠フォームのセレクタ名
     */
    var parentFormName = '#mw_wp_form_mw-wp-form-23535 > form';

    /*
     * ＠送信ボタンのセレクタ名
     */
    var submitButton = 'input[type=submit][name=confirm]';

    /*
     * ＠項目マッピング
     */

    var defaultFormJson=[
    { "tag_name": "sei",               "x_target": 'sei'               },
    { "tag_name": "mei",               "x_target": 'mei'               },
    { "tag_name": "kana-sei",          "x_target": 'kana-sei'          },
    { "tag_name": "kana-mei",          "x_target": 'kana-mei'          },
    { "tag_name": "email",             "x_target": 'email'             },
    { "tag_name": "tel",               "x_target": 'tel'               },
    { "tag_name": "request_store",     "x_target": 'request_store'     },
    { "tag_name": "request_date1",     "x_target": 'request_date1'     },
    { "tag_name": "request_time1",     "x_target": 'request_time1'     },
    { "tag_name": "request_date2",     "x_target": 'request_date2'     },
    { "tag_name": "request_time2",     "x_target": 'request_time2'     },
    { "tag_name": "request_date3",     "x_target": 'request_date3'     },
    { "tag_name": "request_time3",     "x_target": 'request_time3'     },
    { "tag_name": "inquiry",           "x_target": 'inquiry'           },
    { "tag_name": "inquiry_item",      "x_target": 'inquiry_item'      },
    { "tag_name": "mailing",           "x_target": 'mailing'           }
    ];

    // == 設定項目ここまで ==================================================================== //

    // 区切り文字設定
    var separateString = ',';

    var iframeFormData = '';
    for (var i in defaultFormJson) {
        iframeFormData = iframeFormData + '<input id="pd' + defaultFormJson[i]['tag_name'] + '" type="text" name="' + defaultFormJson[i]['tag_name'] + '"/>';
    }

    var iframeHeadSrc =
        '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';

    var iframeSrcDoc =
        '<form id="shadowForm" action="' + pardotFHUrl + '" method="post" accept-charset=\'UTF-8\'>' +
        iframeFormData +
        '<input id="shadowSubmit" type="submit" value="send" onclick="document.charset=\'UTF-8\';"/>' +
        '</form>';

    var shadowSubmited = false;
    var isError = false;
    var pdHiddenName = 'pdHiddenFrame';

    /**
     * エスケープ処理
     * @param val
     */
    function escapeSelectorString(val) {
        return val.replace(/[ !"#$%&'()*+,.\/:;<=>?@\[\\\]^`{|}~]/g, "\\$&");
    }

    $(function () {

        $(submitButton).click(function () {
            // Submitボタンを無効にする。
            $(submitButton).prop("disabled", true);
        });

        $('#' + pdHiddenName).remove();

        $('<iframe>', {
            id: pdHiddenName
        })
            .css({
                display: 'none'
            })
            .on('load', function () {

                if (shadowSubmited) {
                    if (!isError) {

                        shadowSubmited = false;

                        // 送信ボタンを押せる状態にする。
                        $(submitButton).prop("disabled", false);

                        // 親フォームを送信
                        $(parentFormName).submit();
                    }
                } else {
                    $('#' + pdHiddenName).contents().find('head').prop('innerHTML', iframeHeadSrc);
                    $('#' + pdHiddenName).contents().find('body').prop('innerHTML', iframeSrcDoc);

                    $(submitButton).click(function () {
                        shadowSubmited = true;
                        try {
                            for (var j in defaultFormJson) {
                                var tmpData = '';

                                // NANE値取得形式
                                if (useGetType === 1) {
                                    $(parentFormName + ' [name="' + escapeSelectorString(defaultFormJson[j]['x_target']) + '"]').each(function () {

                                        //checkbox,radioの場合、未選択項目は送信除外
                                        if (["checkbox", "radio"].indexOf($(this).prop("type")) >= 0) {
                                            if ($(this).prop("checked") == false) {
                                                return true;
                                            }

                                        }

                                        if (tmpData !== '') {
                                            tmpData += separateString;
                                        }

                                        // 取得タイプ 1 or Null(default) :val()方式, 2:text()形式
                                        if (defaultFormJson[j]['x_type'] === 2) {
                                            tmpData += $(this).text().trim();
                                        } else {
                                            tmpData += $(this).val().trim();
                                        }
                                    });
                                }

                                // セレクタ取得形式
                                if (useGetType === 2) {
                                    $(defaultFormJson[j]['x_target']).each(function () {
                                        if (tmpData !== '') {
                                            tmpData += separateString;
                                        }

                                        // 取得タイプ 1 or Null(default) :val()方式, 2:text()形式
                                        if (defaultFormJson[j]['x_type'] === 2) {
                                            tmpRegexData = $(this).text().trim();
                                        } else {
                                            tmpRegexData = $(this).val().trim();
                                        }
                                    });
                                }

                                $('#' + pdHiddenName).contents().find('#pd' + escapeSelectorString(defaultFormJson[j]['tag_name'])).val(tmpData);

                            }

                            $('#' + pdHiddenName).contents().find('#shadowForm').submit();

                        } catch (e) {
                            isError = true;

                            $(submitButton).prop("disabled", false);

                            $(parentFormName).submit();
                            shadowSubmited = false;
                        }
                        return false;
                    });
                }
            })
            .appendTo('body');
    });
})();
</script>
<?php
get_sidebar();
get_footer();

