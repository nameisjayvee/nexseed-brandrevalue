<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>

<!--campaign_page-->

<div class="campaign_page">

    <img src="<?php echo get_s3_template_directory_uri() ?>/img/campaign/cont01.jpg" alt="ブランドリバリューLINE査定キャンペーン">
    <a href="https://line.me/ti/p/%40otv4506b"><img src="<?php echo get_s3_template_directory_uri() ?>/img/campaign/cont02.jpg" alt="ブランドリバリューLINE査定キャンペーン"></a>
    <img src="<?php echo get_s3_template_directory_uri() ?>/img/campaign/cont03.jpg" alt="ブランドリバリューLINE査定キャンペーン">
    <img src="<?php echo get_s3_template_directory_uri() ?>/img/campaign/cont04.jpg" alt="ブランドリバリューLINE査定キャンペーン">
    <img src="<?php echo get_s3_template_directory_uri() ?>/img/campaign/cont05.jpg" alt="ブランドリバリューLINE査定キャンペーン">
    <img src="<?php echo get_s3_template_directory_uri() ?>/img/campaign/cont06.jpg" alt="ブランドリバリューLINE査定キャンペーン">
    <img src="<?php echo get_s3_template_directory_uri() ?>/img/campaign/cont07.jpg" alt="ブランドリバリューLINE査定キャンペーン">
    <img src="<?php echo get_s3_template_directory_uri() ?>/img/campaign/cont08.jpg" alt="ブランドリバリューLINE査定キャンペーン">
    <img src="<?php echo get_s3_template_directory_uri() ?>/img/campaign/cont09.jpg" alt="ブランドリバリューLINE査定キャンペーン">
    <a href="https://line.me/ti/p/%40otv4506b"><img src="<?php echo get_s3_template_directory_uri() ?>/img/campaign/cont11.jpg" alt="ブランドリバリューLINE査定キャンペーン"></a> 

    <div class="text-center" style="margin:20px 0;">
    <img src="<?php echo get_s3_template_directory_uri() ?>/img/campaign/cont10.jpg" alt="ブランドリバリューLINE査定キャンペーン">
    <img src="<?php echo get_s3_template_directory_uri() ?>/img/line/flow.png" alt="受付は24時間実施中！LINE査定の流れ">
    <img src="<?php echo get_s3_template_directory_uri() ?>/img/line/attention.png" alt="LINE査定に関する注意事項"></div>
    
      <section class="line_tech text-center">
          <dl>
              <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/line_tech_bar.png" alt="こうすれば査定額がより正確に！LINE査定の高額査定テクニック"></dt>
              <dd>
                  <p>LINE査定はあくまで「目安となる買取金額をお伝えする簡易査定」ですが、以下の点にご注意いただければ、より正確な金額をお伝えしやすくなります。
                  ぜひ、ご参考にしてみてください。</p>
                  <ul>
                      <li>写真は複数枚撮影し、外観全体が見えるようにしてください。<br>
                      ダメージがある箇所、型番等の表記はアップで写すとより効果的です。</li>
                      <li>購入先、購入時期、型番等、おわかりになる範囲で情報をご明記ください。</li>
                      <li>お品物のサイズ（寸法）もあわせてご明記ください。同じ形状でサイズが異なる商品もございます。</li>
                  </ul>
                  <p>その他、ご質問等ございましたら、LINEまたはメールにてお気軽にお伝えください。</p>
              </dd>
          </dl>
      </section>
    <?php
        // 買取基準
        //get_template_part('_criterion');
        
        // アクションポイント
        get_template_part('_action');
        
    
        
        // 店舗案内
        get_template_part('_shopinfo');
      ?>
    
      
  
  

</div>
<!--//campaign_page-->

<?php
get_footer();

