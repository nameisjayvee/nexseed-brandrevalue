<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>
<?php /*
<span id="a8sales"></span>
<script>
a8sales({
"pid": "s00000017941001",
"order_number": <?php echo do_shortcode('[mwform_last_tracking_number key="23163"]'); ?>,
"currency": "JPY",
"items": [
{
"code": "a8",
"price": 4000,
"quantity": 1
},
],
"total_price": 4000,
});
</script>
*/ ?>
<!-- Google Code for &#23429;&#37197;&#36023;&#21462; Conversion Page -->

<script type="text/javascript">

/* <![CDATA[ */

var google_conversion_id = 858195022;

var google_conversion_language = "en";

var google_conversion_format = "3";

var google_conversion_color = "ffffff";

var google_conversion_label = "hkeACOeUt28QzoicmQM";

var google_remarketing_only = false;

/* ]]> */

</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">

</script>

<noscript>
<div style="display:inline;"> <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/858195022/?label=hkeACOeUt28QzoicmQM&amp;guid=ON&amp;script=0"/> </div>
</noscript>

<!-- Yahoo Code for your Conversion Page -->
<script type="text/javascript">
    /* <![CDATA[ */
    var yahoo_conversion_id = 1000385394;
    var yahoo_conversion_label = "algVCMHgxHAQ97GbmQM";
    var yahoo_conversion_value = 0;
    /* ]]> */
</script>
<script type="text/javascript" src="//s.yimg.jp/images/listing/tool/cv/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//b91.yahoo.co.jp/pagead/conversion/1000385394/?value=0&label=algVCMHgxHAQ97GbmQM&guid=ON&script=0&disvt=true"/>
    </div>
</noscript>

<div id="primary" class="cat-page content-area">
	<main id="main" class="site-main" role="main">
		<div class="contact_end">
			<h3>宅配買取申し込みの送信完了</h3>
			<p>お問合せ誠にありがとうございました。<br>
				<br>
				送信いただいた内容は、担当者が確認した後、折り返しご連絡をさしあげます。<br>
				お問合せ内容の確認メールを、自動配信でお送りしております。<br>
				メールが届かない場合は、送信が完了していないことがございます。<br>
				お手数ではございますが、下記メールアドレス宛までご連絡ください。<br>
				<br>
				E-Mail:<a href="mailto:info@shopbring.jp">info@brandrevalue.jp</a></p>
		</div>
		<div class="purchase1-bnr-box">
			<p class="purchase1-bnr-Txt"><span id="phone_number_holder_5">0120-970-060</span></p>
		</div>
		<?php echo do_shortcode('[mwform_formkey key="23163"]'); ?>
		<?php
        // 買取基準
        get_template_part('_criterion');
        
        // アクションポイント
        get_template_part('_action');
        
        // 買取方法
        get_template_part('_purchase');
        
        // 店舗案内
        get_template_part('_shopinfo');
      ?>
	</main>
	<!-- #main --> 
</div>
<!-- #primary -->

<?php
get_sidebar();
get_footer();




