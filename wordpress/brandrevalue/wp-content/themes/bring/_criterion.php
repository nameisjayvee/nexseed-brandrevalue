<link href="css/style.css" rel="stylesheet" type="text/css" />
<section>
    <div class="cont_criterion">
        <h2 class="ttl_edit02">ブランドリバリュー買取対応の基準</h2>
        <dl>
            <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/purchase_kijun_bar.png" alt="古い物、傷物も高く査定いたします！"></dt>
            <dd>傷のついたお品物、古くなったお品物も、コスト削減を徹底し高額買取りを<br> 可能としているBRAND REVALUEでは水準以上の価格で査定しています。<br> 「どうせ高くは売れないから……」とあきらめる前に、ぜひ一度ご相談ください。
                <br> 動かなくなった時計やストラップが壊れたバッグなど、ワケあり品も高額に
                <br> 査定できる可能性は十分にあります。もちろん査定自体は無料ですし、
                <br> 1品あたり3分程度で終わります。宅配買取、出張買取も行っていますので、
                <br> こちらもお気軽にご利用ください。
            </dd>
        </dl>
        <div class="cp_tab">
            <input type="radio" name="cp_tab" id="tab1_1" aria-controls="first_tab01" checked>
            <label for="tab1_1">カバン買取</label>
            <input type="radio" name="cp_tab" id="tab1_2" aria-controls="second_tab01">
            <label for="tab1_2">ブランド買取</label>
            <input type="radio" name="cp_tab" id="tab1_3" aria-controls="third_tab01">
            <label for="tab1_3">金買取</label>
            <input type="radio" name="cp_tab" id="tab1_4" aria-controls="force_tab01">
            <label for="tab1_4">時計買取</label>
            <input type="radio" name="cp_tab" id="tab1_5" aria-controls="five_tab01">
            <label for="tab1_5">宝石買取</label>
            <div class="cp_tabpanels">
                <div id="first_tab01" class="cp_tabpanel">
                    <ul class="wake_list">
                        <li>
                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/purchase_wake/bag01.png">
                            <p>カビがあっても買取OK</p>
                        </li>
                        <li>
                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/purchase_wake/bag02.png">
                            <p>シミがあっても買取OK</p>
                        </li>
                        <li>
                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/purchase_wake/bag03.png">
                            <p>ダメージがあっても買取OK</p>
                        </li>
                        <li>
                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/purchase_wake/bag04.png">
                            <p>ダメージがあっても買取OK</p>
                        </li>
                        <li>
                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/purchase_wake/bag05.png">
                            <p>持ち手が黒ずんでてても買取OK</p>
                        </li>
                    </ul>
                </div>
                <div id="second_tab01" class="cp_tabpanel">
                    <ul>
                        <li>
                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/purchase_wake/brand01.png">
                            <p>キズがついてても買取OK</p>
                        </li>
                        <li><img src="<?php echo get_s3_template_directory_uri() ?>/img/purchase_wake/brand02.png">
                            <p>動いてなくても買取OK</p>
                        </li>
                        <li><img src="<?php echo get_s3_template_directory_uri() ?>/img/purchase_wake/brand03.png">
                            <p>劣化していても買取OK</p>
                        </li>
                    </ul>
                </div>
                <div id="third_tab01" class="cp_tabpanel">
                    <ul>

                        <li>
                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/purchase_wake/gold01.png">
                            <p>刻印がなくても買取OK</p>
                        </li>


                        <li><img src="<?php echo get_s3_template_directory_uri() ?>/img/purchase_wake/gold02.png">
                            <p>少量でも買取OK</p>
                        </li>

                        <li><img src="<?php echo get_s3_template_directory_uri() ?>/img/purchase_wake/gold03.png">
                            <p>変形した金でも買取OK</p>
                        </li>
                    </ul>
                </div>
                <div id="force_tab01" class="cp_tabpanel">
                    <ul>

                        <li><img src="<?php echo get_s3_template_directory_uri() ?>/img/purchase_wake/watch01.png" alt="" width="370" height="270">
                            <p>キズがついてても買取OK</p>
                        </li>

                        <li><img src="<?php echo get_s3_template_directory_uri() ?>/img/purchase_wake/watch02.png" alt="" width="370" height="270">
                            <p>ベルトが破損してても買取OK</p>
                        </li>

                        <li><img src="<?php echo get_s3_template_directory_uri() ?>/img/purchase_wake/watch03.png" alt="" width="370" height="270">
                            <p>動いてなくても買取OK</p>
                        </li>
                    </ul>
                </div>
                <div id="five_tab01" class="cp_tabpanel">
                    <ul>

                        <li><img src="<?php echo get_s3_template_directory_uri() ?>/img/purchase_wake/gem01.png">
                            <p>デザインが古くても買取OK</p>
                        </li>
                        <li>

                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/purchase_wake/gem02.png">
                            <p>デザインが古くても買取OK</p>
                        </li>
                        <li>

                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/purchase_wake/gem03.png">
                            <p>壊れてても買取OK</p>
                        </li>
                        <li>

                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/purchase_wake/gem04.png">
                            <p>鑑定書がなくても買取OK</p>
                        </li>

                        <li><img src="<?php echo get_s3_template_directory_uri() ?>/img/purchase_wake/gem05.png">
                            <p>主石がなくても買取OK</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
