<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>



	<div id="primary" class="cat-page content-area">
		<main id="main" class="site-main" role="main">
          <section id="mainVisual" style="background:url(<?php echo get_s3_template_directory_uri() ?>/img/mv/purchase_mv.png)">
            <h2 class="text-hide">買取方法</h2>
        </section>
      
      <section id="catchcopy">
        <h2>「売り方」のスタイルに合わせて選べる、3つの買取方法！</h2>
        <p>「不用品を売りたいけど、銀座まで行くのは遠くて大変」という方は、お品物を自宅から送って査定を待つ「宅配買取サービス」を。「郵送するのも面倒」という方には「出張買取サービス」を。「高価なブランド物を売るのだから、ちゃんとお店で顔を見て売りたい」という方には「店頭買取」を。あなたにピッタリの「売り方」を、自由に選べるシステムをBRANDREVALUE(ブランドリバリュー)は用意しています。</p>
      </section>
      
      <section>
          <ul class="purchase_btn">
              <li><a href="#takuhai"><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/purchase_btn01.png" alt="宅配買取"></a></li>
              <li><a href="#syutchou"><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/purchase_btn02.png" alt="出張買取"></a></li>
              <li><a href="#tentou"><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/purchase_btn03.png" alt="店頭買取"></a></li>
          </ul>
      </section>
      
      <section>
          <div class="kaitorihouhou" id="takuhai">
              <p><a href="<?php echo home_url('about-purchase/takuhai'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/purchase_takuhai_ph.png" alt="宅配買取"></a></p>
              <dl>
                  <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/purchase_takuhai_ttl.png" alt="お品物を郵送するだけで、スピード査定・現金化が可能に"></dt>
                  <dd>「品物を査定してほしいけど、銀座まで行くのは遠くて大変……」<br>
                  「売りたいものがたくさんあって、お店まで持っていけない！」そんな方もどうかご心配なく。<br>
                  BRAND REVALUEでは、ご自宅から郵送でお品物をお送りいただき、店頭にお越しいただくことなく査定・買取を行う宅配買取サービスを行っています。
                  もちろん配送料は無料。お品物を箱詰めするだけで簡単に梱包できる「宅配キット」をお送りいたしますので、手間をかけずにお品物をお売りいただけます。</dd>
                  <dd><a href="<?php echo home_url('about-purchase/takuhai'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/purchase_takuhai_btn.png" alt="「宅配買取」詳細はこちら"></a></dd>
              </dl>
          </div>

          <div class="kaitorihouhou" id="syutchou">
              <p><a href="<?php echo home_url('about-purchase/syutchou'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/purchase_syutchou_ph.png" alt="出張買取"></a></p>
              <dl>
                  <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/purchase_syutchou_ttl.png" alt="家から一歩も出ずにスピード査定を受けられる、楽ちんサービス！"></dt>
                  <dd>東京近郊にお住まいで、「品物を店舗まで運んだり、郵送の手続きをする時間がない」という方にお勧めしたいのが、出張買取サービスです。
                  お申込み後「最短即日」でBRAND REVALUEの鑑定スタッフがお客様のご自宅までお伺いし、その場でお品物をスピード査定。金額にご納得いただければその場で現金をお渡しいたします。
                  家から一歩も出ることなく、店舗と同様のスピーディな買取サービスをご利用いただける、「一番楽ちん」なスタイルです。</dd>
                  <dd><a href="<?php echo home_url('about-purchase/syutchou'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/purchase_syutchou_btn.png" alt="「出張買取」詳細はこちら"></a></dd>
              </dl>
          </div>

          <div class="kaitorihouhou" id="tentou">
              <p><a href="<?php echo home_url('about-purchase/tentou'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/purchase_tentou_ph.png" alt="店頭買取"></a></p>
              <dl>
                  <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/purchase_tentou_ttl.png" alt="「即日で現金を手に入れたい」「顔を見て売りたい」そんな方にピッタリ！"></dt>
                  <dd>「大事な品物だから、郵送ではなく自分の手で店舗まで運びたい」――そんな慎重な方には、店頭買取がお勧め。<br>
                  銀座店に常駐するプロの鑑定スタッフがその場でお品物を査定し、金額にご納得いただければその場で現金をお受け取りいただけます。
                  店舗の様子をご覧いただくことができるだけではなく、ご不明な点や気になることがあればその場でスタッフにお問い合わせいただけるので、とにかく安心してご利用いただける買取スタイルです。</dd>
                  <dd><a href="<?php echo home_url('about-purchase/tentou'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/purchase_tentou_btn.png" alt="「店頭買取」詳細はこちら"></a></dd>
              </dl>
          </div>
          <div class="cont_criterion">
          <h2 class="ttl_edit02">ブランドリバリュー買取対応の基準</h2>
              <dl>
                <dt><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/purchase_kijun_bar.png" alt="古い物、傷物も高く査定いたします！"></dt>
                  <dd>傷のついたお品物、古くなったお品物も、コスト削減を徹底し高額買取りを<br>
                    可能としているBRAND REVALUEでは水準以上の価格で査定しています。<br>
「どうせ高くは売れないから……」とあきらめる前に、ぜひ一度ご相談ください。<br>
動かなくなった時計やストラップが壊れたバッグなど、ワケあり品も高額に<br>
査定できる可能性は十分にあります。もちろん査定自体は無料ですし、<br>
1品あたり3分程度で終わります。宅配買取、出張買取も行っていますので、<br>
こちらもお気軽にご利用ください。</dd>
              </dl>
              <p><img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/purchase_kijun_ph.png" alt=""></p>
          </div>
      
      </section>
      
      
      
      <?php
        // 買取方法
        get_template_part('_purchase');
        
        // 買取基準
        //get_template_part('_criterion');
        
        // アクションポイント
        get_template_part('_action');
        
        // 店舗案内
        get_template_part('_shopinfo');
      ?>
      
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
