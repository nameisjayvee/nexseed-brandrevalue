<?php
    /**
     * The main template file.
     *
     * This is the most generic template file in a WordPress theme
     * and one of the two required files for a theme (the other being style.css).
     * It is used to display a page when nothing more specific matches a query.
     * E.g., it puts together the home page when no home.php file exists.
     *
     * @link https://codex.wordpress.org/Template_Hierarchy
     *
     * @package BRING
     */

    // 買取実績リスト
    $resultLists = array(
        //'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
        'bag009.png::HERMES::エブリン3PM::184,300::180,500::190,000::9,500',
        'bag010.png::PRADA::ハンドバッグ::116,400::114,000::120,000::6,000',
        'bag011.png::GUCCI::バンブーバッグ::29,100::28,500::30,000::30,000',
        'bag012.png::LOUIS VUITTON::モンスリGM::58,200::57,000::60,000::3,000',
        'bag001.jpg::HERMES::バーキン30::1,450,000::1,520,000::1,600,000::150,000',
        'bag002.jpg::CHANEL::マトラッセ チェーンバッグ::100,000::130,000::150,000::50,000',
        'bag003.jpg::LOUIS VUITTON::ダミエ ネヴァーフルMM::92,000::104,000::110,000::18,000',
        'bag004.jpg::CELINE::ラゲージマイクロショッパーバッグ::160,000::175,000::1850,000::25,000',
    );

    // 買取実績リスト
    $resultLists = array(
        //'画像名::ブランド名::アイテム名::A価格::B価格::価格例::sagaku',
        'watch/top/001.jpg::パテックフィリップ::カラトラバ 3923::850,000::750,000::1,000,000::250,000',
        'watch/top/002.jpg::パテックフィリップ::アクアノート 5065-1A::1,700,000::1,500,000::2,000,000::500,000',
        'watch/top/003.jpg::オーデマピゲ::ロイヤルオーク・オフショア 26170ST::1,487,500::1,312,500::1,750,000::437,500',
        'watch/top/004.jpg::ROLEX::サブマリーナ 116610LV ランダム品番::1,105,000::975,000::1,300,000::325,000',
        'watch/top/005.jpg::ROLEX::デイトナ 116505 ランダム品番 コスモグラフ::2,422,500::2,137,500::2,850,000::712,500',
        'watch/top/006.jpg::ブライトリング::クロノマット44::289,000::255,000::340,000::85,000',
        'watch/top/007.jpg::パネライ::ラジオミール 1940::510,000::450,000::600,000::150,000',
        'watch/top/008.jpg::ボールウォッチ::ストークマン ストームチェイサープロ CM3090C::110,500::97,500::130,000::32,500',
        'watch/top/009.jpg::ブレゲ::クラシックツインバレル 5907BB12984::595,000::525,000::700,000::175,000',
        'watch/top/010.jpg::ウブロ::ビッグバン ブラックマジック::739,500::652,500::870,000::217,500',
        'watch/top/011.jpg::オメガ::シーマスター プラネットオーシャン 2208-50::212,500::187,500::250,000::62,500',
        'watch/top/012.jpg::ディオール::シフルルージュ クロノグラフ CD084612 M001::170,000::150,000::200,000::50,000',
    );

    // ランキング更新時はここだけ弄る
    $rankTopImageName = 'toppage_ranking_pc2019.jpg'; // img/ranking directory

    get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="top_shop">
            <ul>
                <li>
                    <a href="<?php echo home_url('ginza'); ?>">
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shop01.png" alt="銀座店"/>
                        <div class="top_info_box">
                            <p>銀座店</p>
                            <p>〒104-0061<br />東京都中央区銀座5-8-3<br>四谷学院ビル5階</p>
                            <p>詳しくはこちら »</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="<?php echo home_url('shibuya'); ?>">
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shop02.png" alt="渋谷店"/>
                        <div class="top_info_box">
                            <p>渋谷店</p>
                            <p>〒150-0041<br />東京都渋谷区神南1-12-16<br>和光ビル4階</p>
                            <p>詳しくはこちら »</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="<?php echo home_url('shinjuku'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shop03.png" alt="新宿店"/>
                        <div class="top_info_box">
                            <p>新宿店</p>
                            <p>〒160-0022<br />東京都新宿区新宿3丁目31-1<br>大伸第２ビル3階</p>
                            <p>詳しくはこちら »</p>
                        </div>
                    </a>
                </li>
                <li style="margin-top:10px;">
                    <a href="<?php echo home_url('shinsaibashi'); ?>">
                        <img src="<?php echo get_s3_template_directory_uri() ?>/img/shop/shop_com.png" alt="心斎橋店"/>
                        <div class="top_info_box">
                            <p>心斎橋店</p>
                            <p>〒542-0081<br />大阪府大阪市中央区南船場4-4-8<br>クリエイティブ心斎橋8階</p>
                            <p>詳しくはこちら »</p>
                        </div>
                    </a>
                </li>
            </ul>
        </div>

        <section class="kaitori_cat">
            <ul>
                <li>
                    <a href="<?php echo home_url('blog/doburock'); ?>"><img src="https://kaitorisatei.info/brandrevalue/wp-content/uploads/2018/12/caf61dc6779ec6137c0ab58dfe3a550d.jpg" alt="どぶろっく"></a>
                </li>
            </ul>
        </section>
        <div class="infomation_box">
            <h3 class="content_ttl">お知らせ</h3>
            <?php
                $posts = get_posts(array(
                    'posts_per_page' => 3, // 表示件数
                    'category' => '181' // カテゴリIDもしくはスラッグ名
                ));
            ?>
            <ul class="information_list">
                <?php if($posts): foreach($posts as $post): setup_postdata($post); ?>
                    <li><a href="<?php the_permalink() ?>"><span class="date">
                        <?php the_time('Y/m/d') ?>
                    </span>
                    <?php the_title(); ?>
                    </a></li>
                <?php endforeach; endif; ?>
            </ul>
        </div>
        <div class="top_search_box">
            <p class="mincho">買取実績 40億円を突破!</p>
            <p class="mincho">お手持ちのブランド買取実績が、すぐに検索できます。</p>
            <form method="get" class="searchform_top" action="<?php echo esc_url( home_url('/') ); ?>">
                <input type="search" placeholder="ブランド名でさがす" name="s" class="searchfield_top" value="" />
                <input type="image" src="<?php bloginfo('template_directory'); ?>/img/search_btn.png" alt="検索" class="searchsubmit_top" />
            </form>
            <div class="search_text">検索</div>
            <a class="search_link" href="<?php echo home_url('/brand'); ?>">取扱ブランド一覧からさがす<i class="fas fa-arrow-circle-right"></i></a>
        </div>
        <?php get_template_part('_category'); ?>

        <section class="top_jisseki">
            <h3 class="content_ttl">最新の買取実績はコチラ</h3>

            <ul class="archive_purchase slider">

                <?php query_posts(
                    array(
                        'post_type' => 'purchase_item',
                        'taxonomy' => 'purchase_item',
                        'posts_per_page' => 8,
                        'paged' => $paged,
                    ));
                    if (have_posts()): while ( have_posts() ) : the_post();
                ?>
                    <li><a href="<?php the_permalink(); ?>">
                        <p class="kaitori-date"><?php the_field('kaitori-date'); ?></p>
                        <p class="kaitori-image"><img src="<?php the_field('kaitori-img'); ?>" alt="<?php the_title(); ?>"></p>
                        <p class="kaitori-title"><?php the_title(); ?></p>
                        <p class="kaitori-price-header">買取価格</p>
                        <P class="kaitori-price"> ¥<?php the_field('kaitori-price'); ?></P>
                        <p class="kaitori-method"><?php the_field('kaitori-method'); ?></p>
                    </a></li>
                <?php endwhile; endif; wp_reset_query(); ?>
            </ul>
            <a href="<?php echo home_url('purchase_items'); ?>" class="purchase_more">買取実績一覧はコチラ<i class="fas fa-caret-right"></i></a>
        </section>
        <section class="kaitori_cat">
            <ul>
                <li>
                    <a href="<?php echo home_url('ranking'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/ranking/<?php echo $rankTopImageName ?>" alt="買取ランキング"></a>
                </li>
            </ul>
        </section>

        <section id="shop-point">
            <h2 class="mincho"><span>ブランドリバリュー</span>当店の<span class="size44">3</span><span>つ</span>の<span>ポイント</span></h2>
            <ul>
                <li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/top/point01.png" alt="業界再速級">
                    <div>
                        <h3 class="mincho">業界最速級！</h3>
                        <p>査定は当日、<span>翌日振込み</span></p>
                        <p>査定の速さはトップクラス</p>
                    </div>
                </li>
                <li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/top/point02.png" alt="すべて無料">
                    <div>
                        <h3 class="mincho">すべて無料</h3>
                        <p>査定料も送料も、<span>全て0円</span></p>
                        <p>買取にかかる費用は完全無料</p>
                    </div>
                </li>
                <li> <img src="<?php echo get_s3_template_directory_uri() ?>/img/top/point03.png" alt="即現金払い">
                    <div>
                        <h3 class="mincho">即現金払い</h3>
                        <p><span>お急ぎ</span>の方でも大丈夫</p>
                        <p>出張・店頭買取なら即現金払い</p>
                    </div>
                </li>
            </ul>
        </section>
        <?php
            // 3つの買取方法
            get_template_part('cv_parts/page_cv01');
        ?>
        <section class="top_riyuu">
            <h2><span class="ttl_small">ブランドリバリューが、</span><br> 選ばれている
                <span class="tx_red">6つの理由</span></h2>
            <ul class="reason_list">
                <li>
                    <h3><span class="tx_red">最高峰</span>の高額買取</h3>
                    <p>市場調査の徹底や販売ルートの確立により他社よりも高額で買取できる仕組みを整えております。</p>
                </li>
                <li>
                    <h3><span class="tx_red">超スピード</span>買取査定</h3>
                    <p>経験豊富なコンシェルジュが持つブランド知識で正確且つスピーディーな査定でお客様へ査定結果をお知らせ。</p>
                </li>
                <li>
                    <h3>送料・査定料<span class="tx_red">0円</span></h3>
                    <p>宅配買取時の送料・キャンセル料・振込手数料・宅配キット代金・査定料など全て0円で承ります。</p>
                </li>
                <li>
                    <h3><span class="tx_small tx_red">買取額1000万円以上</span><span class="tx_small">でも</span><br>
                        <span class="tx_red">即現金</span>でお支払い</h3>
                    <p>買取金額が1000万円を超える場合でも即現金でお支払い可能。<br> ※お振り込みになる場合もございます。 </p>
                </li>
                <li>
                    <h3><span class="tx_small tx_red">1万点以上の</span><br> 取扱ブランド数
                    </h3>
                    <p>1万点以上のブランドを買取対象としているので他社で断られたブランド商品でもお気軽にご相談下さい。</p>
                </li>
                <li>
                    <h3>全店舗<span class="tx_red">駅徒歩圏内</span></h3>
                    <p>ブランドリバリューは全店舗、駅から徒歩圏内の好立地に店舗を構えております。お気軽にお越しください。</p>
                </li>
            </ul>
            <h2><span class="ttl_small">ブランドリバリューに来られるお客様だからこそ</span><br>
                <span class="tx_red">当店だから、出来るサービス</span></h2>
            <ul class="serv_list">
                <li>
                    <div class="taxi">
                        <h3><span class="tx_small">ご来店頂く際に、ご利用された</span><br>
                            <span class="tx_red">タクシー代金</span>お支払い</h3>
                        <p>お店までお越し頂いたお客様のタクシー代金の一部を<br> 弊社がお支払いいたします。
                        </p>
                        <div class="reason_img">
                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/top/reason_taxi.png"" />
                        </div>
                    </div>
                    <div class="privacy">
                        <h3><span class="tx_small">プライバシーを重視した</span><br> 安心の
                            <span class="tx_red">完全個室制</span></h3>
                        <p>お客様のプライバシーを配慮した個室ブースをご用意しております。
                        </p>
                        <div class="reason_img">
                            <img src="<?php echo get_s3_template_directory_uri() ?>/img/top/reason_privacy.png"" />
                        </div>
                    </div>
                    <div>
                        <p class="taxi_btn"><a href="<?php echo home_url('about-purchase/tentou'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/btn-more-gold.png" alt="詳しくはこちら"></a></p>
                    </div>
                </li>
            </ul>
        </section>
        <section id="top-jisseki">
            <h2 class="ttl_edit02">ブランドリバリューの買取実績</h2>
            <div class="full_content">

                <!-- ブランド買取 -->
                <div class="menu hover"><span class="glyphicon glyphicon-tag"></span> ブランド買取</div>
                <div class="content">
                    <!-- box -->
                    <ul id="box-jisseki" class="list-unstyled clearfix">
                        <!-- brand1 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/001.jpg" alt="">
                                <p class="itemName">オーデマピゲ　ロイヤルオークオフショアクロノ 26470OR.OO.A002CR.01 ゴールド K18PG</p>
                                <hr>
                                <p> <span class="red">A社</span>：3,080,000円<br>
                                    <span class="blue">B社</span>：3,000,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">3,200,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>200,000円</p>
                            </div>
                        </li>

                        <!-- brand2 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/002.jpg" alt="">
                                <p class="itemName">パテックフィリップ　コンプリケーテッド ウォッチ 5085/1A-001</p>
                                <hr>
                                <p> <span class="red">A社</span>：1,470,000円<br>
                                    <span class="blue">B社</span>：1,380,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">1,650,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>270,000円</p>
                            </div>
                        </li>

                        <!-- brand3 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/003.jpg" alt="">
                                <p class="itemName">パテックフィリップ　コンプリケーション 5130G-001 WG</p>
                                <hr>
                                <p> <span class="red">A社</span>：2,630,000円<br>
                                    <span class="blue">B社</span>：2,320,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">3,100,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>780,000円</p>
                            </div>
                        </li>

                        <!-- brand4 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/004.jpg" alt="">
                                <p class="itemName">パテックフィリップ　ワールドタイム 5130R-001</p>
                                <hr>
                                <p> <span class="red">A社</span>：2,800,000円<br>
                                    <span class="blue">B社</span>：2,500,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">3,300,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>800,000円</p>
                            </div>
                        </li>

                        <!-- brand5 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/005.jpg" alt="">
                                <p class="itemName">シャネル　ラムスキン　マトラッセ　二つ折り長財布</p>
                                <hr>
                                <p> <span class="red">A社</span>：85,000円<br>
                                    <span class="blue">B社</span>：75,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">100,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>25,000円</p>
                            </div>
                        </li>

                        <!-- brand6 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/006.jpg" alt="">
                                <p class="itemName">エルメス　ベアンスフレ　ブラック</p>
                                <hr>
                                <p> <span class="red">A社</span>：180,000円<br>
                                    <span class="blue">B社</span>：157,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">210,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>53,000円</p>
                            </div>
                        </li>

                        <!-- brand7 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/007.jpg" alt="">
                                <p class="itemName">エルメス　バーキン30　トリヨンクレマンス　マラカイト　SV金具</p>
                                <hr>
                                <p> <span class="red">A社</span>：1,200,000円<br>
                                    <span class="blue">B社</span>：1,050,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">1,350,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>300,000円</p>
                            </div>
                        </li>

                        <!-- brand8 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/008.jpg" alt="">
                                <p class="itemName">セリーヌ　ラゲージマイクロショッパー</p>
                                <hr>
                                <p> <span class="red">A社</span>：210,000円<br>
                                    <span class="blue">B社</span>：180,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">240,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>60,000円</p>
                            </div>
                        </li>

                        <!--brand9 -->

                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/009.jpg" alt="">
                                <p class="itemName">ルイヴィトン　裏地ダミエ柄マッキントッシュジャケット</p>
                                <hr>
                                <p> <span class="red">A社</span>：34,000円<br>
                                    <span class="blue">B社</span>：30,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">40,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>10,000円</p>
                            </div>
                        </li>

                        <!-- brand10 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/010.jpg" alt="">
                                <p class="itemName">シャネル　ココマーク　ラパンファーマフラー</p>
                                <hr>
                                <p> <span class="red">A社</span>：100,000円<br>
                                    <span class="blue">B社</span>：90,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">120,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>30,000円</p>
                            </div>
                        </li>

                        <!-- brand11 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/011.jpg" alt="">
                                <p class="itemName">ルイヴィトン　ダミエ柄サイドジップレザーブーツ</p>
                                <hr>
                                <p> <span class="red">A社</span>：45,000円<br>
                                    <span class="blue">B社</span>：40,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">55,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>15,000円</p>
                            </div>
                        </li>

                        <!-- brand12 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/brand/top/012.jpg" alt="">
                                <p class="itemName">サルバトーレフェラガモ　ヴェラリボンパンプス</p>
                                <hr>
                                <p> <span class="red">A社</span>：8,500円<br>
                                    <span class="blue">B社</span>：7,500円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">10,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>,500円</p>
                            </div>
                        </li>
                    </ul>
                </div>

                <!-- 金買取 -->
                <div class="menu"><span class="glyphicon glyphicon-bookmark"></span> 金買取</div>
                <div class="content">
                    <!-- box -->
                    <ul id="box-jisseki" class="list-unstyled clearfix">
                        <!-- 1 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/001.jpg" alt="">
                                <p class="itemName">K18　ダイヤ0.11ctリング</p>
                                <hr>
                                <p> <span class="red">A社</span>：29,750円<br>
                                    <span class="blue">B社</span>：26,250円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">
                                    <!-- span class="small" >120g</span -->35,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>8,750円</p>
                            </div>
                        </li>
                        <!-- 2 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/002.jpg" alt="">
                                <p class="itemName">K18　メレダイヤリング</p>
                                <hr>
                                <p> <span class="red">A社</span>：38,250円<br>
                                    <span class="blue">B社</span>：33,750円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">
                                    <!-- span class="small" >120g</span -->45,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>11,250円</p>
                            </div>
                        </li>
                        <!-- 3 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/003.jpg" alt="">
                                <p class="itemName">K18/Pt900　メレダイアリング</p>
                                <hr>
                                <p> <span class="red">A社</span>：15,300円<br>
                                    <span class="blue">B社</span>：13,500円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">18,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>4,500円</p>
                            </div>
                        </li>
                        <!-- 4 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/004.jpg" alt="">
                                <p class="itemName">Pt900　メレダイヤリング</p>
                                <hr>
                                <p> <span class="red">A社</span>：20,400円<br>
                                    <span class="blue">B社</span>：18,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">24,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>6,000円</p>
                            </div>
                        </li>
                        <!-- 5 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/005.jpg" alt="">
                                <p class="itemName">K18WG　テニスブレスレット</p>
                                <hr>
                                <p> <span class="red">A社</span>：34,000円<br>
                                    <span class="blue">B社</span>：30,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">40,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>10,000円</p>
                            </div>
                        </li>
                        <!-- 6 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/006.jpg" alt="">
                                <p class="itemName">K18/K18WG　メレダイヤリング</p>
                                <hr>
                                <p> <span class="red">A社</span>：25,500円<br>
                                    <span class="blue">B社</span>：22,500円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">30,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>7,500円</p>
                            </div>
                        </li>
                        <!-- 7 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/007.jpg" alt="">
                                <p class="itemName">K18ブレスレット</p>
                                <hr>
                                <p> <span class="red">A社</span>：23,800円<br>
                                    <span class="blue">B社</span>：21,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">28,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>7,000円</p>
                            </div>
                        </li>
                        <!-- 8 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/008.jpg" alt="">
                                <p class="itemName">K14WGブレスレット</p>
                                <hr>
                                <p> <span class="red">A社</span>：52,700円<br>
                                    <span class="blue">B社</span>：46,500円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">62,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>15,500円</p>
                            </div>
                        </li>
                        <!-- 9 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/009.jpg" alt="">
                                <p class="itemName">Pt850ブレスレット</p>
                                <hr>
                                <p> <span class="red">A社</span>：23,800円<br>
                                    <span class="blue">B社</span>：21,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">28,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>8,000円</p>
                            </div>
                        </li>
                        <!-- 10 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/010.jpg" alt="">
                                <p class="itemName">Pt900/Pt850メレダイヤネックレス</p>
                                <hr>
                                <p> <span class="red">A社</span>：29,750円<br>
                                    <span class="blue">B社</span>：26,250円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">35,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>8,750円</p>
                            </div>
                        </li>
                        <!-- 11 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/011.jpg" alt="">
                                <p class="itemName">K18/Pt900/K24ネックレストップ</p>
                                <hr>
                                <p> <span class="red">A社</span>：50,150円<br>
                                    <span class="blue">B社</span>：44,250円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">59,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>14,750円</p>
                            </div>
                        </li>
                        <!-- 12 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gold/top/012.jpg" alt="">
                                <p class="itemName">K24インゴットメレダイヤネックレストップ</p>
                                <hr>
                                <p> <span class="red">A社</span>：51,000円<br>
                                    <span class="blue">B社</span>：45,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">60,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>15,000円</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- 宝石買取 -->
                <div class="menu"><span class="glyphicon glyphicon-magnet"></span> 宝石買取</div>
                <div class="content">
                    <!-- box -->
                    <ul id="box-jisseki" class="list-unstyled clearfix">
                        <!-- 1 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/001.jpg" alt="">
                                <p class="itemName">ダイヤルース</p>
                                <p class="itemdetail">カラット：1.003ct<br> カラー：G
                                    <br> クラリティ：VS-2
                                    <br> カット：Good
                                    <br> 蛍光性：FAINT
                                    <br> 形状：ラウンドブリリアント
                                </p>
                                <hr>
                                <p> <span class="red">A社</span>：380,000円<br>
                                    <span class="blue">B社</span>：335,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">
                                    <!-- span class="small">地金＋</span -->450,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>115,000円</p>
                            </div>
                        </li>

                        <!-- 2 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/002.jpg" alt="">
                                <p class="itemName">Pt900 DR0.417ctリング</p>
                                <p class="itemdetail"> カラー：F<br> クラリティ：SI-1
                                    <br> カット：VERY GOOD <br> 蛍光性：FAINT
                                    <br> 形状：ラウンドブリリアント
                                </p>
                                <hr>
                                <p> <span class="red">A社</span>：51,000円<br>
                                    <span class="blue">B社</span>：45,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">60,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>15,000円</p>
                            </div>
                        </li>

                        <!-- 3 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/003.jpg" alt="">
                                <p class="itemName">Pt900　DR0.25ctリング</p>
                                <p class="itemdetail"> カラー：H<br> クラリティ:VS-1
                                    <br> カット：Good
                                    <br> 蛍光性：MB
                                    <br> 形状：ラウンドブリリアント
                                    <br>

                                    <hr>
                                    <p> <span class="red">A社</span>：58,000円<br>
                                        <span class="blue">B社</span>：51,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">68,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>17,000円</p>
                            </div>
                        </li>
                        <!-- 4 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/004.jpg" alt="">
                                <p class="itemName">K18　DR0.43ct　MD0.4ctネックレストップ</p>
                                <p class="itemdetail"> カラー：I<br> クラリティ：VS-2
                                    <br> カット：Good
                                    <br> 蛍光性：WB
                                    <br> 形状：ラウンドブリリアント
                                </p>
                                <hr>
                                <p> <span class="red">A社</span>：60,000円<br>
                                    <span class="blue">B社</span>：52,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">70,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>18,000円</p>
                            </div>
                        </li>

                        <!-- 5 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/005.jpg" alt="">
                                <p class="itemName">ダイヤルース</p>
                                <p class="itemdetail">カラット：0.787ct<br> カラー：E
                                    <br> クラアリティ：VVS-2
                                    <br> カット：Good
                                    <br> 蛍光性：FAINT
                                    <br> 形状：ラウンドブリリアント
                                </p>
                                <hr>
                                <p> <span class="red">A社</span>：220,000円<br>
                                    <span class="blue">B社</span>：190,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">260,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>70,000円</p>
                            </div>
                        </li>

                        <!-- 6 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/006.jpg" alt="">
                                <p class="itemName">Pt950　MD0.326ct　0.203ct　0.150ctネックレス</p>
                                <p class="itemdetail"> カラー：F<br> クラリティ：SI-2
                                    <br> カット：Good
                                    <br> 蛍光性：FAINT
                                    <br> 形状：ラウンドブリリアント
                                </p>
                                <hr>
                                <p> <span class="red">A社</span>：100,000円<br>
                                    <span class="blue">B社</span>：90,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">120,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>30,000円</p>
                            </div>
                        </li>

                        <!-- 7 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/007.jpg" alt="">
                                <p class="itemName">ダイヤルース</p>
                                <p class="itemdetail">カラット：1.199ct<br> カラー：K
                                    <br> クラリティ：SI-2
                                    <br> カット：評価無
                                    <br> 蛍光性：NONE
                                    <br> 形状：パビリオン
                                    <br>
                                </p>
                                <hr>
                                <p> <span class="red">A社</span>：127,000円<br>
                                    <span class="blue">B社</span>：110,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">150,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>40,000円</p>
                            </div>
                        </li>

                        <!-- 8 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/gem/top/008.jpg" alt="">
                                <p class="itemName">ダイヤルース</p>
                                <p class="itemdetail">カラット：8.1957ct<br> カラー：LIGTH YELLOW<br> クラリティ：VS-1
                                    <br> カット：VERY GOOD<br> 蛍光性：FAINT
                                    <br> 形状：ラウンドブリリアント
                                </p>
                                <hr>
                                <p> <span class="red">A社</span>：6,000,000円<br>
                                    <span class="blue">B社</span>：5,500,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">6,700,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>1,200,000円</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="menu"><span class="glyphicon glyphicon-time"></span> 時計買取</div>
                <div class="content">
                    <ul id="box-jisseki" class="list-unstyled clearfix">
                        <?php
                            foreach($resultLists as $list):
                            // :: で分割
                            $listItem = explode('::', $list);

                        ?>
                            <li class="box-4">
                                <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/<?php echo $listItem[0]; ?>" alt="">
                                    <p class="itemName">
                                        <?php echo $listItem[1]; ?><br>
                                        <?php echo $listItem[2]; ?>
                                    </p>
                                    <hr>
                                    <p> <span class="red">A社</span>：
                                        <?php echo $listItem[3]; ?>円<br>
                                        <span class="blue">B社</span>：
                                        <?php echo $listItem[4]; ?>円 </p>
                                </div>
                                <div class="box-jisseki-cat">
                                    <h3>買取価格例</h3>
                                    <p class="price">
                                        <?php echo $listItem[5]; ?><span class="small">円</span></p>
                                </div>
                                <div class="sagaku">
                                    <p><span class="small">買取差額“最大”</span>
                                        <?php echo $listItem[6]; ?>円</p>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>

                <!-- バッグ買取 -->
                <div class="menu"><span class="glyphicon glyphicon-briefcase"></span> バッグ買取</div>
                <div class="content">
                    <ul id="box-jisseki" class="list-unstyled clearfix">
                        <!-- 1 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/001.jpg" alt="">
                                <p class="itemName">エルメス　エブリンⅢ　トリヨンクレマンス</p>
                                <hr>
                                <p> <span class="red">A社</span>：230,000円<br>
                                    <span class="blue">B社</span>：200,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">270,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>70,000円</p>
                            </div>
                        </li>
                        <!-- 2 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/002.jpg" alt="">
                                <p class="itemName">プラダ　シティトート2WAYバッグ</p>
                                <hr>
                                <p> <span class="red">A社</span>：110,000円<br>
                                    <span class="blue">B社</span>：95,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">132,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>37,000円</p>
                            </div>
                        </li>
                        <!-- 3 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/003.jpg" alt="">
                                <p class="itemName">バンブーデイリー2WAYバッグ</p>
                                <hr>
                                <p> <span class="red">A社</span>：100,000円<br>
                                    <span class="blue">B社</span>：90,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">120,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>30,000円</p>
                            </div>
                        </li>
                        <!-- 4 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/004.jpg" alt="">
                                <p class="itemName">ルイヴィトン　モノグラムモンスリGM</p>
                                <hr>
                                <p> <span class="red">A社</span>：110,000円<br>
                                    <span class="blue">B社</span>：90,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">120,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>30,000円</p>
                            </div>
                        </li>
                        <!-- 5 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/005.jpg" alt="">
                                <p class="itemName">エルメス　バーキン30</p>
                                <hr>
                                <p> <span class="red">A社</span>：1,360,000円<br>
                                    <span class="blue">B社</span>：1,200,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">1,600,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>400,000円</p>
                            </div>
                        </li>
                        <!-- 6 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/006.jpg" alt="">
                                <p class="itemName">シャネル　マトラッセダブルフラップダブルチェーンショルダーバッグ</p>
                                <hr>
                                <p> <span class="red">A社</span>：220,000円<br>
                                    <span class="blue">B社</span>：195,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">260,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>65,000円</p>
                            </div>
                        </li>
                        <!-- 7 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/007.jpg" alt="">
                                <p class="itemName">ルイヴィトン　ダミエネヴァーフルMM</p>
                                <hr>
                                <p> <span class="red">A社</span>：110,000円<br>
                                    <span class="blue">B社</span>：96,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">130,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>34,000円</p>
                            </div>
                        </li>
                        <!-- 8 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/008.jpg" alt="">
                                <p class="itemName">セリーヌ　ラゲージマイクロショッパー</p>
                                <hr>
                                <p> <span class="red">A社</span>：200,000円<br>
                                    <span class="blue">B社</span>：180,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">240,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>60,000円</p>
                            </div>
                        </li>
                        <!-- 9 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/009.jpg" alt="">
                                <p class="itemName">ロエベ　アマソナ23</p>
                                <hr>
                                <p> <span class="red">A社</span>：75,000円<br>
                                    <span class="blue">B社</span>：68,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">90,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>22,000円</p>
                            </div>
                        </li>
                        <!-- 10 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/010.jpg" alt="">
                                <p class="itemName">グッチ　グッチシマ　ビジネスバッグ</p>
                                <hr>
                                <p> <span class="red">A社</span>：51,000円<br>
                                    <span class="blue">B社</span>：45,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">60,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>15,000円</p>
                            </div>
                        </li>
                        <!-- 11 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/011.jpg" alt="">
                                <p class="itemName">プラダ　サフィアーノ2WAYショルダーバッグ</p>
                                <hr>
                                <p> <span class="red">A社</span>：100,000円<br>
                                    <span class="blue">B社</span>：93,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">130,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>37,000円</p>
                            </div>
                        </li>
                        <!-- 12 -->
                        <li class="box-4">
                            <div class="title"> <img src="<?php echo get_s3_template_directory_uri() ?>/img/item/bag/top/012.jpg" alt="">
                                <p class="itemName">ボッテガヴェネタ　カバMM　ポーチ付き</p>
                                <hr>
                                <p> <span class="red">A社</span>：136,000円<br>
                                    <span class="blue">B社</span>：120,000円 </p>
                            </div>
                            <div class="box-jisseki-cat">
                                <h3>買取価格例</h3>
                                <p class="price">160,000<span class="small">円</span></p>
                            </div>
                            <div class="sagaku">
                                <p><span class="small">買取差額“最大”</span>40,000円</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section> <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/bn_matomegai.png"> </section>

        <?php

            // 3つの買取方法
            get_template_part('_action');

            // 幅広いお品物を・・
            get_template_part('_widerange');

            // 買取対応の基準
            get_template_part('_criterion');

            // NG
            get_template_part('_no_criterion');

            // 銀座で最高水準の・・・
            get_template_part('_ginza');

            // 店舗案内
            get_template_part('_shopinfo');
            // ご来店が難しい方はこちら
            get_template_part('cv_parts/page_cv02');

            // ブランド買取について
            get_template_part('_aboutbrand');

        ?>
    </main>
    <!-- #main -->
</div>
<!-- #primary -->

<?php
    get_sidebar();
    get_footer();
