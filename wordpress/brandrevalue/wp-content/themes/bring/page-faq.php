<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>

	<div id="primary" class="cat-page content-area">
		<main id="main" class="site-main" role="main">
      <section id="mainVisual" style="background:url(<?php echo get_s3_template_directory_uri() ?>/img/mv/faq.png)">
        <h2 class="text-hide">よくある質問</h2>
      </section>
      
      <section id="faq">
        <dl>
          <dt>チェーンが切れたネックレスや破損した指輪等も買取可能ですか？</dt>
          <dd>買取可能です。金、プラチナ等の貴金属は溶かして再利用することができるため、破損していても査定額が大きく下がることはありません。片方しかないピアスや、留め具だけになったピアスもOK。指輪から外れたダイヤモンド等の宝石類も高く査定することが可能です。</dd>
        </dl>
        <dl>
          <dt>古い財布・バッグの内側がベタベタになっていても、買取可能ですか？</dt>
          <dd>外側の革や布部分にダメージがなければ、高額で買取可能です。</dd>
        </dl>
        <dl>
          <dt>身分証としてシルバーパスや定期券を利用することは可能ですか？</dt>
          <dd>申し訳ありませんが、できません。身分証としてご提示いただけるのは、免許証、パスポート、保険証、住民基本台帳カード（住基カード）のいずれかとなっております。</dd>
        </dl>
        <dl>
          <dt>査定だけ依頼することは可能ですか？　その場合に料金はかからないのですか？</dt>
          <dd>査定は完全無料で行っておりますので、ご安心ください。もちろん、「お品物の価値を知りたいので査定だけまず行いたい」「複数の店舗で相見積もりをとりたい」というご要望も大歓迎です。</dd>
        </dl>
        <dl>
          <dt>宅配買取の送料は無料ですか？</dt>
          <dd>はい、無料です！<br>もし査定額にご納得いただけなかった場合も無料で返送いたしますので、どうぞお気軽にご利用ください。宅配が面倒な場合は、LINE査定や出張買取もお勧めです。</dd>
        </dl>
        <dl>
          <dt>鑑定書、鑑別書のないダイヤモンド、宝石も買い取ってもらえますか？</dt>
          <dd>もちろんです！　専用の検査装置を使って真贋や品質を鑑定し、傷や状態等を加味して可能な限り高く買い取らせていただきます。傷が入ったダイヤモンド、サイズが小さいダイヤモンドもOKです。</dd>
        </dl>
        <dl>
          <dt>宅配買取りの場合、商品を梱包後どうやって送ればいいですか？　送料はかかりますか？</dt>
          <dd>最寄りの佐川急便へご連絡ください。ドライバーがお荷物を引き取りに伺います。その際は着払いにてお送りください。送料は弊社が全額負担いたします。</dd>
        </dl>
        <dl>
          <dt>家が銀座から遠いのですが、利用できますか？</dt>
          <dd>もちろんです！　BRAND REVALUEでは店頭買取の他に、東京23区でご利用いただける「出張買取」や全国でご利用いただける「宅配買取」、さらにLINEで簡易査定ができる「LINE査定」も行っております。</dd>
        </dl>
        <dl>
          <dt>どうしてBRAND REVALUEは他店より高額に買取ができているのですか？</dt>
          <dd>お客様に利益を還元するため、徹底したコスト削減を行っているからです。店舗は銀座の一等地にありますが、あえて家賃が高い1階・路面店ではなく家賃の安い空中階に店舗を構え、スペース・人員も最小限に抑えております。その分、通常の路面店よりも高い水準で査定を行うことが可能となりました。</dd>
        </dl>
        <dl>
          <dt>古着や家電製品、書籍等の買取は可能ですか？</dt>
          <dd>申し訳ございません、BRAND REVALUEでは中古ブランド品をメインとした買取サービスを行っており、家電製品、書籍等の古物は取り扱っておりません。古着につきましては、ブランド品の衣類であれば査定可能ですが、それ以外のお品物は査定の対象外となります。</dd>
        </dl>
      </section>
      
      <?php
        // アクションポイント
        get_template_part('_action');
        
        // 買取方法
        get_template_part('_purchase');
      ?>
      
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
