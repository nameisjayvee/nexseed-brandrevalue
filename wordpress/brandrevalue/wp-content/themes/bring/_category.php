<section class="kaitori_cat">
    <ul class="top_kaitori_bn">
        <li class="gold">
            <a href="<?php echo home_url('cat/gold'); ?>">
                <h3>金・プラチナ<span>GOLD・PLATINUM</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="jewelry">
            <a href="<?php echo home_url('cat/gem'); ?>">
                <h3>宝石<span>GEM</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="watch">
            <a href="<?php echo home_url('cat/watch'); ?>">
                <h3>時計<span>BRAND WATCH</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="bag">
            <a href="<?php echo home_url('cat/bag'); ?>">
                <h3>バッグ<span>BAG</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="outfit">
            <a href="<?php echo home_url('cat/outfit'); ?>">
                <h3>洋服・毛皮<span>OUTFIT</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="wallet">
            <a href="<?php echo home_url('cat/wallet'); ?>">
                <h3>ブランド財布<span>BRAND WALLET</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="shoes">
            <a href="<?php echo home_url('cat/shoes'); ?>">
                <h3>靴<span>SHOES</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="diamond">
            <a href="<?php echo home_url('cat/diamond'); ?>">
                <h3>ダイヤモンド<span>DIAMOND</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="kimono">
            <a href="<?php echo home_url('kimono'); ?>">
                <h3>着物<span>KIMONO</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="antique">
            <a href="<?php echo home_url('cat/antique'); ?>">
                <h3>骨董品<span>ANTIQUE</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="add bj_bnr">
            <a href="<?php echo home_url('cat/brandjewelery'); ?>">
                <h3><span class="lh">ブランド<br>ジュエリー</span><span>Brand jewelery</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li class="add an_rolex">
            <a href="<?php echo home_url('cat/antique_rolex'); ?>">
                <h3><span class="lh">アンティーク<br>ロレックス</span><span>Antique ROLEX</span></h3>
                <p>買取</p>
            </a>
        </li>
        <li><a href="<?php echo home_url('cat/mement'); ?>"><img src="<?php echo get_s3_template_directory_uri() ?>/img/top/top-category-memento.png" alt="遺品 買取"></a></li>
    </ul>
</section>
