<?php

/*
Template Name: LP用テンプレート
*/

get_header(lp); ?>

<?php if(have_posts()): while(have_posts()): the_post(); ?>
 
    <?php the_post_thumbnail('thumbnail'); ?>
 
<?php endwhile; endif; ?>
    
<div id="content" class="site-content container">

    <div id="primary" class="cat-page content-area">
        <main id="main" class="site-main" role="main">
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
  </main>
        <!-- #main -->
    </div>
    <!-- #primary -->

    <?php
get_sidebar();
get_footer();