<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 *
 * Template Name: 特別買取強化LP用テンプレート
 */

get_header(); ?>

<div id="primary" class="cat-page content-area">
  <main id="main" class="site-main" role="main">
    <p><?php echo wp_get_attachment_image(SCF::get('lp-mainVIsHeadline'), 'full'); ?></p>
    
    <p id="catchcopy"><?php echo nl2br(SCF::get('lp-catchcopy')); ?></p>
    
    <section id="hikaku" class="lorex_hikaku" style="background-image:url(<?php echo wp_get_attachment_image_url(SCF::get('lp-resultPickupImage'), 'full'); ?>);">
      <h3 class="text-hide">他社の買取額と比較すると</h3>
      <p class="hikakuName"><?php echo nl2br(SCF::get('lp-resultPickupName')); ?></p>
      <p class="hikakuText">※状態が良い場合や当店で品薄の場合などは<br> 　特に高価買取致します。</p>
      <p class="hikakuPrice1"><span class="red">A社</span>：<?php echo number_format(SCF::get('lp-resultPickupPriceA')); ?>円</p>
      <p class="hikakuPrice2"><span class="blue">B社</span>：<?php echo number_format(SCF::get('lp-resultPickupPriceB')); ?>円</p>
      <p class="hikakuPrice3"><?php echo number_format(SCF::get('lp-resultPickupPrice')); ?><span class="small">円</span></p>
    </section>
    
    <section id="cat-point">
      <h3>高価買取のポイント</h3>
      <ul class="list-unstyled">
        <li>
          <p class="pt_tl">商品情報が明確だと査定がスムーズ</p>
          <p class="pt_tx">ブランド名、モデル名が明確だと査定がスピーディに出来、買取価格にもプラスに働きます。また、新作や人気モデル、人気ブランドであれば買取価格がプラスになります。</p>
        </li>
        <li>
          <p class="pt_tl">数点まとめての査定だと<br>買取がスムーズ</p>
          <p class="pt_tx">数点まとめての査定依頼ですと、買取価格をプラスさせていただきます。</p>
        </li>
        <li>
          <p class="pt_tl">品物の状態がよいほど<br>査定がスムーズ</p>
          <p class="pt_tx">お品物の状態が良ければ良いほど、買取価格もプラスになります。</p>
        </li>
      </ul>
      <p><?php echo nl2br(SCF::get('lp-resultPickupText')); ?></p>
    </section>
    
    
    <section id="lp-cat-jisseki">
      <h3 class="text-hide">買取実績</h3>
      <ul id="jisseki-type" class="clearfix">
        <li><a href="">ブランド時計</a></li>
        <li><a href="">高級装飾時計</a></li>
        <li><a href="">懐中時計</a></li>
        <li><a href="">置時計</a></li>
        <li><a href="">アンティーク時計</a></li>
      </ul>
      
      <ul id="box-jisseki" class="list-unstyled clearfix">
        <?php
          $resultLists = SCF::get('resultLists');
          foreach($resultLists as $list):
        ?>
        
        <li class="box-4" style="height:450px;">
          <div class="title">
            <?php echo wp_get_attachment_image($list['resultImage'], 'full'); ?>
            <p class="itemName">
              <?php echo $list['resultName']; ?><br>
            </p>
            <hr>
            <p>
              <span class="red">A社</span>：
              <?php echo number_format($list['resultPriceA']); ?>円
              <br>
              <span class="blue">B社</span>：
              <?php echo number_format($list['resultPriceB']); ?>円
            </p>
          </div>
          <div class="box-jisseki-cat">
            <h3>買取価格例</h3>
            <p class="price">
              <?php echo number_format($list['resultPrice']); ?><span class="small">円</span></p>
          </div>
          <div class="hitokoto">
            <p><span class="small">一言コメント</span><br>
              <?php echo nl2br($list['resultComment']); ?></p>
          </div>
        </li>
        <?php endforeach; ?>
      </ul>
    </section>

    <section id="about_kaitori" class="clearfix">
      <?php
        // 買取について
        get_template_part('_widerange2');
      ?>
    </section>

    <section>
      <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/bn_matomegai.png">
    </section>

    <?php
      // 買取基準
      get_template_part('_criterion');

      // NGアイテム
      get_template_part('_no_criterion');
        
      // カテゴリーリンク
      get_template_part('_category');
    ?>
    
    <section id="kaitori_genre">
      <h3 class="text-hide">その他買取可能なジャンル</h3>
      <p>【買取ジャンル】バッグ/ウエストポーチ/セカンドバック/トートバッグ/ビジネスバッグ/ボストンバッグ/クラッチバッグ/トランクケース/ショルダーバッグ/ポーチ/財布/カードケース/パスケース/キーケース/手帳/腕時計/ミュール/サンダル/ビジネスシューズ/パンプス/ブーツ/ペアリング/リング/ネックレス/ペンダント/ピアス/イアリング/ブローチ/ブレスレット/ライター/手袋/傘/ベルト/ペン/リストバンド/アンクレット/アクセサリー/サングラス/帽子/マフラー/ハンカチ/ネクタイ/ストール/スカーフ/バングル/カットソー/アンサンブル/ジャケット/コート/ブルゾン/ワンピース/ニット/シャツメンズ/毛皮/Tシャツ/キャミソール/タンクトップ/パーカー/ベスト/ポロシャツ/ジーンズ/スカート/スーツなど</p>
    </section>

    <?php
      // 買取方法
      get_template_part('_purchase');
    ?>

    <section id="user_voice">
      <h3>ユーザーボイス</h3>
      <p class="user_voice_text1"><?php echo nl2br(SCF::get('voice-user')); ?></p>

      <h4>バイヤー目線</h4>
      <div class="clearfix">
        <p class="user_voice_text2"><?php echo nl2br(SCF::get('voice-buyer')); ?></p>
        <img src="<?php echo get_s3_template_directory_uri() ?>/img/cat/photo-user-voice_lp.png" class="right">
      </div>
    </section>

  </main>
  
  <!-- #main -->
  </div>
  <!-- #primary -->

<?php
  get_sidebar();
  get_footer();