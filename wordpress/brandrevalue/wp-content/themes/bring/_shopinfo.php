<section id="shopinfo">
    <h2 class="text-hide">BRANDREVALUE 店舗案内</h2>
    <div class="shopinfo_wrap">
    <div class="box-shop">
    <a href="<?php echo home_url('ginza'); ?>">
        <div class="wrapperShopinfo">
            <div class="text">
                <h3>BRANDREVALUE銀座店</h3>
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/top/top-shop-harajuku.png" alt="BRANDREVALUE(ブランドリバリュー)銀座店" class="shopimage">
                <p>〒104-0061<br>東京都中央区銀座5-8-3<p>四谷学院ビル5階</p>
                <p>TEL 0120-970-060</p>
                <p class="shop_link"> 詳しく見る<i class="fas fa-arrow-circle-right"></i></p>
            </div>
        </div>
    </a>
    </div>
    <div class="box-shop">
    <a href="<?php echo home_url('shibuya'); ?>">
        <div class="wrapperShopinfo">
            <div class="text">
                <h3>BRANDREVALUE渋谷店</h3>
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/top/top-shop-newshibuya.png" alt="BRANDREVALUE(ブランドリバリュー)銀座店" class="shopimage">
                <p>〒150-0041<br>東京都渋谷区神南1-12-16<p>和光ビル4階</p>
                <p>TEL 0120-970-060</p>
                <p class="shop_link">詳しく見る<i class="fas fa-arrow-circle-right"></i></p>
            </div>
        </div>
    </a>
    </div>
    <div class="box-shop">
        <a href="<?php echo home_url('shinjuku'); ?>">
        <div class="wrapperShopinfo">
            <div class="text">
                <h3>BRANDREVALUE新宿店</h3>
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/top/top-shop-nshinjuku.png" alt="BRANDREVALUE(ブランドリバリュー)銀座店" class="shopimage">
                <p>〒160-0022<br>東京都新宿区新宿3丁目31-1<p>大伸第２ビル3階</p>
                <p>TEL 0120-970-060</p>
                <p class="shop_link">詳しく見る<i class="fas fa-arrow-circle-right"></i></p>
            </div>
        </div>
        </a>
    </div>
    <div class="box-shop">
    <a href="<?php echo home_url('shinsaibashi'); ?>">
        <div class="wrapperShopinfo">
            <div class="text">
                <h3>BRANDREVALUE心斎橋店</h3>
            <img src="<?php echo get_s3_template_directory_uri() ?>/img/top/top-shop-00.png" alt="BRANDREVALUE(ブランドリバリュー)心斎橋店" class="shopimage">
                <p>〒542-0081<br>大阪府大阪市中央区南船場4-4-8<p>クリエイティブ心斎橋8階</p>
                <p>TEL 0120-970-060</p>
                <p class="shop_link"> 詳しく見る<i class="fas fa-arrow-circle-right"></i></p>
            </div>
        </div>
        </a>
    </div>
    </div>
</section>
