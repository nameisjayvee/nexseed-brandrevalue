<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BRING
 */

get_header(); ?>

  <div id="primary" class="cat-page content-area">
    <main id="main" class="site-main" role="main">
      <section id="mainVisual" style="background:url(<?php echo get_s3_template_directory_uri() ?>/img/mv/company.png)">
        <h2 class="text-hide">よくある質問</h2>
      </section>
      
      <section id="company">
        <table class="table table-bordered">
          <tr><th>会社名</th><td>株式会社STAY GOLD</td></tr>
          <tr><th>本社所在地</th><td>東京都渋谷区神宮前6丁目23-3第9SYビル5階</td></tr>
          <tr><th>店舗名</th><td>BRAND REVALUE(ブランドリバリュー)</td></tr>
          <tr><th>店舗所在地</th><td>店舗案内をご参照下さい。</td></tr>
          <tr><th>設立</th><td>平成26年4月14日</td></tr>
          <tr><th>役員</th><td>代表取締役　柏村 淳司</td></tr>
          <tr><th>資本金</th><td>500万円</td></tr>
          <tr><th>事業内容</th><td>衣料品、宝石、貴金属、アクセサリー等の買取、販売及び輸出入</td></tr>
          <tr><th>古物商許可番号</th><td>東京都公安委員会 第303311408927号 大阪府公安委員会 第621110181018号</td></tr>
          <tr><th>特定商取引法に基づく表記</th><td>買取方法：出張買取、宅配買取、店頭買取</td></tr>

        </table>
      </section>
      
      
      <section class="company_cont">

          <h3 class="ttl">業界最高水準の査定価格・サービス品質をご体験ください</h3>
          <p>BRAND REVALUEの店舗があるのは、銀座をはじめとした中古ブランド品の買取店がひしめく一等地。<br>
          私たちはこの激戦区で、他店に負けない「最高水準の査定価格」と「お客様のニーズに応える多彩なサービスメニュー」、そして「懇切丁寧・誠実な接客」を強みとして買取サービスを提供しております。</p>

          <h3 class="ttl">「売り方」のスタイルに合わせて選べる、3つの買取方法！</h3>
          <p>「店舗まで行くのは遠くて大変」という方は、お品物を自宅から送って査定を待つ「宅配買取サービス」を。<br>
          「郵送するのも面倒」という方には「出張買取サービス」を。<br>
          「ちゃんとお店で顔を見て売りたい」という方には「店頭買取」を。あなたにピッタリの「売り方」を、自由に選べるシステムをBRAND REVALUEは用意しています。</p>
      </section>
      
      <?php
        // アクションポイント
        get_template_part('_action');
        
        // 買取方法
        get_template_part('_purchase');
      ?>
      
    </main><!-- #main -->
  </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
